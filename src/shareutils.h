/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SHAREUTILS_H
#define SHAREUTILS_H

#include <QObject>
#include <QDebug>
#include <QPoint>
#include <QApplication>
#include <QClipboard>
#include <QOperatingSystemVersion>
#include <QDesktopServices>
#include <QUrl>

class PlatformShareUtils : public QObject
{
public:
    PlatformShareUtils(QObject *parent = nullptr)
        : QObject(parent) { }
    virtual ~PlatformShareUtils() { }
    virtual void share(const QString shareText, const QString email, QPoint pos)
    {
        Q_UNUSED(pos)        
        QString subject = shareText.split("\n").first();
        QString url = QString("mailto:?to=%1&subject=%2&body=%3").arg(email, subject, shareText);
        QDesktopServices::openUrl(QUrl(url, QUrl::TolerantMode));
    };
};

class ShareUtils : public QObject
{
    Q_OBJECT
public:
    explicit ShareUtils(QObject *parent = nullptr);

    Q_INVOKABLE void copyToClipboard(const QString shareText);
    Q_INVOKABLE void share(const QString shareText, const QString email, QPoint pos);

private:
    PlatformShareUtils *mPShareUtils;
};

#endif // SHAREUTILS_H
