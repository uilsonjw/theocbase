/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2019, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.10
import QtQuick.Layouts 1.3

Item {
    id: _numberSelector
    property int maxValue: 50
    property int selectedValue: 0
    property int boxSize: 30
    QtObject {
      id: internal
      property int neededBoxSize: boxSize + 5
    }
    width: 300
    height: Math.ceil( (maxValue+1) / Math.floor(width / internal.neededBoxSize ) ) * internal.neededBoxSize

    GridLayout {
        id: grid
        anchors.top: parent.top
        columns: _numberSelector.width / internal.neededBoxSize
        rowSpacing: 5
        columnSpacing: 5

        Repeater {
            model: maxValue+1
            Rectangle {
                id: rectangle
                Layout.preferredHeight: boxSize
                Layout.preferredWidth: boxSize
                color: index == selectedValue ? "black" : "grey"
                Text {
                    text: index
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    color: "white"
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        selectedValue = index
                    }
                }
            }
        }
    }
}


