#include "weekendmeetingavailabilitychecker.h"

using namespace tbAvailability;

namespace {
const QString DATA_SRC_OUTSIDE_SPEAKERS = "OutsideSpeakers";
const QString DATA_SRC_PUBLIC_MTGS = "PublicMeetings";
const QString DATA_SRC_HOST = "Hospitality";
const int LOOKBACK_WEEKS = 520; // number of historical weeks to analyse for "last used" date
}

// WeekendMeetingAvailabilityChecker
//=================================

WeekendMeetingAvailabilityChecker::WeekendMeetingAvailabilityChecker(const QDate &meetingDate, const QDate &weekCommencingDate)
    : AvailabilityChecker(meetingDate,
                          weekCommencingDate,
                          std::vector<person::UseFor>({ person::WtCondoctor, person::WtReader, person::Chairman, person::PublicTalk, person::Prayer }),
                          std::vector<QString>({ DATA_SRC_OUTSIDE_SPEAKERS, DATA_SRC_PUBLIC_MTGS }))
{
}

QString WeekendMeetingAvailabilityChecker::GenerateAssignedPersonsSql(QString dataSourceId)
{
    if (dataSourceId == DATA_SRC_OUTSIDE_SPEAKERS) {
        QString s =
                "SELECT speaker_id "
                "FROM outgoing "
                "WHERE active AND date = '%1'";

        return s.arg(weekCommencingDate_.toString(Qt::ISODate));
    }

    else if (dataSourceId == DATA_SRC_PUBLIC_MTGS) {
        // NOTE: db may benefit from an index on the "publicmeeting.date" column if table is large

        QString s =
                "SELECT date, speaker_id, wtreader_id, chairman_id, wt_conductor_id "
                "FROM publicmeeting "
                "WHERE active AND date <= '%1' "
                "ORDER BY date DESC LIMIT %2";

        return s.arg(weekCommencingDate_.toString(Qt::ISODate), QString::number(LOOKBACK_WEEKS));
    }

    return QString();
}

void WeekendMeetingAvailabilityChecker::PopulateAssignments(QString dataSourceId, AvailabilityItem &result, const sql_items &assignedPersons)
{
    int personId = result.Id;

    if (dataSourceId == DATA_SRC_OUTSIDE_SPEAKERS) {
        auto iterator = std::find_if(assignedPersons.begin(), assignedPersons.end(),
                                     [&personId](const sql_item &assignedPerson) { return assignedPerson.value("speaker_id").toInt() == personId; });

        result.OutsideSpeaker = iterator != assignedPersons.end();
    }

    else if (dataSourceId == DATA_SRC_PUBLIC_MTGS) {
        foreach (sql_item mtgAssigments, assignedPersons) {
            QDate dt = mtgAssigments.value("date").toDate();
            int speakerId = mtgAssigments.value("speaker_id").toInt();
            int readerId = mtgAssigments.value("wtreader_id").toInt();
            int chairmanId = mtgAssigments.value("chairman_id").toInt();
            int wtConductorId = mtgAssigments.value("wt_conductor_id").toInt();

            if (speakerId == personId) {
                result.AddHomeAssignment(dt, person::PublicTalk);
            }

            if (readerId == personId) {
                result.AddHomeAssignment(dt, person::WtReader);
            }

            if (chairmanId == personId) {
                result.AddHomeAssignment(dt, person::Chairman);
            }

            if (wtConductorId == personId) {
                result.AddHomeAssignment(dt, person::WtCondoctor);
            }
        }
    }
}

Availability WeekendMeetingAvailabilityChecker::GetPublicSpeaker(int congregationId /* = 0*/, int talkThemeId /* = 0*/)
{

    Availability result = Get(person::PublicTalk, congregationId);
    if (talkThemeId > 0) {
        result.FilterOnPersonIds(GetSpeakersForTheme(talkThemeId));
    }
    return result;
}

Availability WeekendMeetingAvailabilityChecker::GetLocalPublicSpeaker(int talkThemeId /* = 0*/)
{
    Availability result = GetLocal(person::PublicTalk);
    if (talkThemeId > 0) {
        result.FilterOnPersonIds(GetSpeakersForTheme(talkThemeId));
    }
    return result;
}

HostpitalityChecker::HostpitalityChecker(const QDate &meetingDate, const QDate &weekCommencingDate)
    : AvailabilityChecker(meetingDate,
                          weekCommencingDate,
                          std::vector<person::UseFor>({ person::WtCondoctor, person::WtReader, person::Chairman, person::PublicTalk, person::Hospitality }),
                          std::vector<QString>({ DATA_SRC_HOST }))
{
}

Availability HostpitalityChecker::GetHospitality()
{
    Availability result = Get(person::Hospitality, 0);
    return result;
}

QString HostpitalityChecker::GenerateAssignedPersonsSql(QString dataSourceId)
{
    if (dataSourceId == DATA_SRC_HOST) {
        QString s =
                "SELECT date, hospitality_id "
                "FROM publicmeeting "
                "WHERE active AND date <= '%1'";

        return s.arg(weekCommencingDate_.toString(Qt::ISODate));
    }
    return QString();
}

void HostpitalityChecker::PopulateAssignments(QString dataSourceId, AvailabilityItem &result, const sql_items &assignedPersons)
{
    if (dataSourceId == DATA_SRC_HOST) {
        int personId = result.Id;

        for (sql_item mtgAssigments : assignedPersons) {
            QDate dt = mtgAssigments.value("date").toDate();
            int hostId = mtgAssigments.value("hospitality_id").toInt();

            if (hostId == personId) {
                result.AddHomeAssignment(dt, person::Hospitality);
            }
        }
    }
}
