import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.1
import QtWebView 1.1
import QtQuick.Controls.Material 2.3

Rectangle {
    id: rectangle
    width: 480
    height: 640
    visible: true
    signal close()   

    Material.theme: Material.Dark

    signal newsFound()

    SwipeView {
        id: swipeView
        anchors.fill: parent
        interactive: false

        StartSliderPage1 {
            id: firstPage
            checkBox.onToggled: settings_ui.show_start_page = !checkBox.checked
            checkBox.checked: !settings_ui.show_start_page
            webView.url: "https://www.theocbase.net/appnews.html"
            webView.onLoadingChanged: {
                if (loadRequest.status === WebView.LoadSucceededStatus) {
                    if (settings_ui.latestArticle !== webView.title) {
                        settings_ui.latestArticle = webView.title
                        console.log("title saved " + webView.title)
                        newsFound()
                    }
                }
            }
        }
        StartSliderPage2 {
            id: secondPage
        }
        StartSliderPage3 {
            id: thirdPage
        }
    }

    /*
    RoundButton
    {
        id: minus
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.verticalCenter: parent.verticalCenter
        Material.background: "#254f8e"
        icon.source: "qrc:/startup/chevron_left.svg"
        icon.color: "white"
        MouseArea
        {
            anchors.fill:parent
            onClicked:{
                if(swipeView.currentIndex>0)
                    swipeView.currentIndex--
            }
        }
    }
    RoundButton
    {
        id: plus
        anchors.right:parent.right
        anchors.rightMargin: 10
        anchors.verticalCenter: parent.verticalCenter
        Material.background: "#254f8e"
        icon.source: "qrc:/startup/chevron_right.svg"
        icon.color: "white"
        MouseArea
        {
            anchors.fill:parent
            onClicked:{
                if(swipeView.currentIndex<3)
                    swipeView.currentIndex++
            }
        }
    }


    PageIndicator {
        id: indicator
        interactive: true
        count: swipeView.count
        currentIndex: swipeView.currentIndex
        anchors.bottom: swipeView.bottom
        anchors.bottomMargin: 10
        anchors.horizontalCenter: parent.horizontalCenter        
    }
    */

    RoundButton {
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.top: parent.top
        anchors.topMargin: 10
        icon.source: "qrc:/icons/close.svg"
        onClicked: close()
    }
}
