/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2016, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.5
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.1
import QtQuick.Window 2.1
import net.theocbase 1.0

Item {
    id: assignmentDialog
    property string title: "Congregation Bible Study"
    width: 300
    height: 350
    property LMM_Assignment currentAssignment

    AssignmentController { id: myController }

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 10

        RowLayout {
            ToolButton {
                icon.source: "qrc:/icons/title.svg"
                background: null
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                ToolTip.text: qsTr("Theme")
                ToolTip.visible: hovered
            }
            TextArea {
                id: labelTheme
                text: currentAssignment ? currentAssignment.theme : ""
                wrapMode: Text.WordWrap
                Layout.fillWidth: true
                font.pointSize: 11
                font.bold: true
                topPadding: 0
                bottomPadding: 0
                background: null
            }
        }

        RowLayout {
            ToolButton {
                icon.source: "qrc:/icons/wt_source.svg"
                background: null
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                ToolTip.text: qsTr("Source")
                ToolTip.visible: hovered
            }
            TextArea {
                Layout.fillWidth: true
                id: textareaSource
                text: currentAssignment ? currentAssignment.source : ""
                font.pointSize: 11
                readOnly: true
                wrapMode: Text.WordWrap
                topPadding: 0
                bottomPadding: 0
                background: null
            }
        }

        RowLayout {
            ToolButton {
                icon.source: "qrc:/icons/servant.svg"
                background: null
                ToolTip.text: qsTr("Conductor")
                ToolTip.visible: hovered
            }
            ComboBoxTable {
                currentText: currentAssignment && currentAssignment.speaker ? currentAssignment.speaker.fullname : ""
                Layout.fillWidth: true
                column3.title: qsTr("CBS conductor", "Dropdown column title")
                column4.title: qsTr("Any CL assignment", "Dropdown column title")
                onBeforeMenuShown: {
                    //                       if (typeof model === "undefined")
                    model = currentAssignment.getSpeakerList()
                    column2.resizeToContents()
                }
                onRowSelected: {
                    currentAssignment.speaker = id < 1 ? null : myController.getPublisherById(id)
                    currentAssignment.save()
                }
            }
        }

        RowLayout {
            ToolButton {
                icon.source: "qrc:/icons/reader.svg"
                background: null
                ToolTip.text: qsTr("Reader")
                ToolTip.visible: hovered
            }
            ComboBoxTable {
                currentText: currentAssignment && currentAssignment.assistant ? currentAssignment.assistant.fullname : ""
                Layout.fillWidth: true
                column4.visible: false
                onBeforeMenuShown: {
                    //                        if (typeof model === "undefined")
                    model = currentAssignment.getAssistantList()
                    column2.resizeToContents()
                }
                onRowSelected: {
                    currentAssignment.assistant = id < 1 ? null : myController.getPublisherById(id)
                    currentAssignment.save()
                }
            }
        }

        RowLayout {
            ToolButton {
                icon.source: "qrc:/icons/notes.svg"
                background: null
                ToolTip.text: qsTr("Note")
                ToolTip.visible: hovered
            }
            TextArea{
                id: texteditNote
                Layout.fillWidth: true
                wrapMode: Text.WordWrap
                selectByMouse: true
                text: currentAssignment ? currentAssignment.note : ""
                Layout.fillHeight: false
                font.pointSize: 11
                onEditingFinished: {
                    if (currentAssignment.note != text) {
                        currentAssignment.note = text
                        currentAssignment.save()
                    }
                }
            }
        }

        Item { Layout.fillHeight: true }

    }
}
