/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "printmidweekschedule.h"

PrintMidweekSchedule::PrintMidweekSchedule()
{
    currentTemplateData = QSharedPointer<TemplateData>(
            new TemplateData(TemplateData::MidweekScheduleTemplate, "schoolTemplate", "MW-Schedule_1.htm", "mw-"));
    qrCodeSupport = true;
}

QVariant PrintMidweekSchedule::fillTemplate()
{
    QString str = getTemplate();
    str = initLayout(str);
    str = ifthen.tokenize(str);
    str = fillTemplateBlockLMMOverall(str);
    fillWeekList(0);

    // titles for all meetings
    str.replace("!CONGREGATION_TITLE!", tr("%1 Congregation", "Congregation_Title Tag. %1 is Congregation Name").arg(myCongregation.name));
    str.replace("!CONGREGATION!", tr("Congregation"));
    str.replace("!CONGREGATION_NAME!", myCongregation.name);
    str.replace("!PT_TITLE!", tr("Public Talk"));
    str.replace("!WT_TITLE!", tr("Watchtower Study"));
    str.replace("!TREASURES_TITLE!", tr("Treasures from God's Word"));
    str.replace("!APPLY_TITLE!", tr("Apply Yourself to the Field Ministry"));
    str.replace("!LIVING_TITLE!", tr("Living as Christians"));

    QPair<QString, QString> sec = templateGetSection(str, "!REPEAT_START!", "!REPEAT_END!", "lmm");

    QString basepage = sec.first;
    QString section = sec.second;

    // Fill schedule for each week
    QString weekContext = "";
    for (weekInfo *w : weekList) {
        currentWeek = w;
        //QDate tempdate(w->week);
        QString oneweek(section);
        oneweek = fillTemplateBlockLMM(oneweek, false);
        weekContext.append(oneweek);
    }
    basepage.replace("<div class=\"lmm\"></div>", "<div class=\"lmm\">" + weekContext + "</div>");
    // Add QR Code
    // basepage = addQrCode(basepage);

    return (ifthen.compute(basepage));
}

QString PrintMidweekSchedule::fillTemplateBlockLMMOverall(QString context)
{
    context.replace("!TITLE!", sql->getSetting("print_mw_title", tr("Midweek Meeting")));
    context.replace("!WORKSHEET!", tr("Worksheet"));
    context.replace("!OPENING!", tr("Opening Comments", "See Workbook"));
    context.replace("!CONCLUSION!", tr("Concluding Comments", "See Workbook"));
    context.replace("!SONG!", tr("Song"));
    context.replace("!PRAYER!", tr("Prayer"));
    context.replace("!CHAIRMAN!", tr("Chairman"));
    context.replace("!COUNSELOR!", tr("Counselor"));
    context.replace("!CONDUCTOR!", tr("Conductor"));
    context.replace("!READER!", tr("Reader"));
    context.replace("!LM!", tr("Christian Life and Ministry Meeting"));
    context.replace("!GW!", tr("Treasures from God's Word"));
    context.replace("!FM!", tr("Apply Yourself to the Field Ministry"));
    context.replace("!CL!", tr("Living as Christians"));
    context.replace("!CBS!", tr("Congregation Bible Study"));
    context.replace("!CO!", tr("Circuit Overseer"));
    context.replace("!NO_MEETING!", tr("No regular meeting"));
    context.replace("!ASSISTANT!", tr("Assistant", "Assistant to student"));
    context.replace("!STUDY!", tr("Study", "Counsel point"));
    context.replace("!THEME!", tr("Theme", "Talk Theme description from workbook"));
    context.replace("!SOURCE!", tr("Source", "Source information from workbook"));
    context.replace("!EXERCISES!", tr("Exercises"));
    context.replace("!TIMING!", tr("Timing", "Assignment completed in time?"));
    context.replace("!NOTES!", tr("Notes"));
    context.replace(QRegularExpression("!NEXT_STUDY[A-Z|_]*!"), tr("Next study"));
    context.replace("!SETTING!", tr("Setting", "for sisters assignment"));
    context.replace("!SCHOOL1!", tr("Class") + " I");
    context.replace("!SCHOOL2!", tr("Class") + " II");
    context.replace("!SCHOOL3!", tr("Class") + " III");
    context.replace("!ACC!", tr("Auxiliary Classroom Counselor", "See S-140"));
    context.replace("!AC!", tr("Auxiliary Classroom", "See S-140"));
    context.replace("!MH!", tr("Main Hall", "See S-140"));
    context.replace("!MH_SHORT!", tr("MH", "Abbreviation for 'Main Hall'"));
    context.replace("!AUX_SHORT!", tr("A", "Abbreviation for 'Auxiliary Classroom'"));
    context.replace("!STUDENT!", tr("Student", "See S-140"));
    context.replace("!OC_TITLE!", sql->getSetting("print_mw_oc_title", tr("Opening Comments", "See Workbook")));
    context.replace("!CC_TITLE!", sql->getSetting("print_mw_cc_title", tr("Concluding Comments", "See Workbook"))); // TODO
    context.replace("!TODAY!", tr("Today"));
    context.replace("!NEXT_WEEK!", tr("Next week"));

    context.replace("!SECTION_TITLES!", currentTemplateData->optionValue("section-titles", "yes") == "yes" ? "yes" : "");

    return context;
}

QString PrintMidweekSchedule::fillTemplateBlockLMM(QString context, bool worksheet)
{
    QDate date(currentWeek->week);
    school s;
    bool showSongTitles = currentTemplateData->optionValue("song-titles") == "yes";
    bool showWorkbookNumber = currentTemplateData->optionValue("mwb-number") == "yes";
    bool useDuration = currentTemplateData->optionValue("duration") == "duration";

    ccongregation::exceptions ex = c.isException(date);
    int md = c.getMeetingDay(date, ccongregation::tms);

    QTime starttime = QTime::fromString(myCongregation.time_meeting1, "hh:mm");

    QDate d;
    if (ex == ccongregation::CircuitOverseersVisit) {
        d = date.addDays(md - 1);
    } else if (ex == ccongregation::Other) {
        if (md == 0) {
            // no meeting
            d = date.addDays(schoolday - 1);
        } else {
            d = date.addDays(md - 1);
        }
    } else {
        d = date.addDays(schoolday - 1);
    }
    context = replaceDateTag(context, "DATE", d);
    bool nomeeting(md == 0);
    context = fillCommonItems(context, nomeeting, ex);
    if (worksheet && nomeeting)
        return "";

    context = replaceTimeTag(context, "LM_STARTTIME", starttime);

    LMM_Meeting *mtg = new LMM_Meeting();
    if (!mtg->loadMeeting(date, true))
        return context;
    QList<LMM_Assignment *> prog = mtg->getAssignments();

    QString abcd = "ABCD";
#ifdef Q_OS_WIN
    QString checkmark = "√";
#else
    QString checkmark = "\u2713";
#endif
    if (useDuration)
        context.replace("!DURATION!", "Yes");
    else
        context.replace("!DURATION!", "");
    if (ex == ccongregation::CircuitAssembly || ex == ccongregation::RegionalConvention || md == 0) {
        /* Since we now have printing tags, we will skip this

        // no meeting because of convention or other reason
        exceptionText = mtg->bibleReading() + "\n";
        exceptionText += c.getExceptionText(date);
        // TODO: mene loppuun
        if (worksheet){
            context.replace("!BIBLE_READING!",exceptionText);
        }

        */
        context.replace("!LM_SOURCE!", mtg->bibleReading());
        context.replace("!LM_NOTES!", mtg->notes());
    } else {
        context.replace("!LM_SOURCE!", mtg->bibleReading());
        context.replace("!LM_NOTES!", mtg->notes());
        context.replace("!LM_CHAIRMAN!", mtg->chairman() ? mtg->chairman()->fullname() : "");
        context.replace("!COUNSELOR_A1!", mtg->counselor2() ? mtg->counselor2()->fullname() : "");
        context.replace("!COUNSELOR_A2!", mtg->counselor3() ? mtg->counselor3()->fullname() : "");

        context.replace("!CLASS_A_SCHEDULED!", mtg->chairman() ? "Yes" : "");
        context.replace("!CLASS_B_SCHEDULED!", mtg->counselor2() ? "Yes" : "");
        context.replace("!CLASS_C_SCHEDULED!", mtg->counselor3() ? "Yes" : "");

        context.replace("!SONG1_NO!", mtg->songBeginning() > 0 ? QVariant(mtg->songBeginning()).toString() : "");
        context.replace("!SONG2_NO!", mtg->songMiddle() > 0 ? QVariant(mtg->songMiddle()).toString() : "");
        context.replace("!SONG3_NO!", mtg->songEnd() > 0 ? QVariant(mtg->songEnd()).toString() : "");
        if (showSongTitles) {
            context.replace("!SONG1_NAME!", mtg->songBeginningTitle());
            context.replace("!SONG2_NAME!", mtg->songMiddleTitle());
            context.replace("!SONG3_NAME!", mtg->songEndTitle());
        } else {
            context.replace("!SONG1_NAME!", "");
            context.replace("!SONG2_NAME!", "");
            context.replace("!SONG3_NAME!", "");
        }
        context = replaceTimeTag(context, "SONG1_STARTTIME", starttime);
        context = replaceTimeTag(context, "SONG2_STARTTIME", starttime.addSecs(60 * 47));
        context = replaceTimeTag(context, "SONG3_STARTTIME", starttime.addSecs(60 * 100));
        context.replace("!PRAYER1_NAME!", mtg->prayerBeginning() ? mtg->prayerBeginning()->fullname() : "");
        context.replace("!PRAYER2_NAME!", mtg->prayerEnd() ? mtg->prayerEnd()->fullname() : "");

        context = replaceTimeTag(context, "OC_STARTTIME", starttime.addSecs(60 * 5));
        context.replace("!OC_CONTENT!", mtg->openingComments().replace("\n", "<br>"));
        context = replaceTimeTag(context, "CC_STARTTIME", starttime.addSecs(60 * ((ex == ccongregation::CircuitOverseersVisit) ? 67 : 97)));
        context.replace("!CC_CONTENT!", mtg->closingComments().replace("\n", "<br>"));

        int fmID(0);
        context.replace("!FM0_THEME!", "");
        int elapsedMinutes = date.year() < 2020 ? 8 : 6;
        for (LMM_Assignment *asgn : prog) {
            QString basetagname = "";
            QString lblspkrname = "";
            QString spkrname = "";
            QString lblasstname = "";
            QString asstname = "";
            QString lblsource = "";
            QString source(asgn->source());
            QString lbltheme = "";
            QString theme(asgn->theme());
            QString lblnote = "";
            QString note(asgn->note());
            QString time = "";
            QString lbltime = "";
            QString lblstarttime = "";
            LMM_Assignment_ex *studenttalk(nullptr);
            LMM_Assignment_ex *cbs(nullptr);
            int currentDuration(asgn->time());

            if (asgn->canCounsel())
                studenttalk = qobject_cast<LMM_Assignment_ex *>(asgn);
            else if (worksheet && d.year() < 2018) {
                if (asgn->talkId() == LMM_Schedule::TalkType_SampleConversationVideo) {
                    int len = asgn->theme().length();
                    context.replace("!PREPARE_PRESENTATIONS!", (len > 1) ? "Yes" : "");
                }
                // now worksheets only have student info
                // Chairman can have the normal schedule with him for other talks.
                continue;
            }
            if (asgn->talkId() == LMM_Schedule::TalkType_CBS)
                cbs = qobject_cast<LMM_Assignment_ex *>(asgn);

            if (asgn->speaker())
                spkrname = asgn->speaker()->fullname();
            if (studenttalk != nullptr && studenttalk->assistant())
                asstname = studenttalk->assistant()->fullname();
            else if (cbs && cbs->assistant())
                asstname = cbs->assistant()->fullname();

            switch (asgn->talkId()) {
            case LMM_Schedule::TalkType_Treasures:
                basetagname = "GW1";
                break;
            case LMM_Schedule::TalkType_Digging:
                basetagname = "GW2";
                currentDuration += 1;
                break;
            case LMM_Schedule::TalkType_BibleReading:
                basetagname = "GW3";
                currentDuration += 1;
                break;
            case LMM_Schedule::TalkType_SampleConversationVideo:
            case LMM_Schedule::TalkType_ApplyYourself:
            case LMM_Schedule::TalkType_InitialCall:
            case LMM_Schedule::TalkType_ReturnVisit1:
            case LMM_Schedule::TalkType_ReturnVisit2:
            case LMM_Schedule::TalkType_ReturnVisit3:
            case LMM_Schedule::TalkType_BibleStudy:
            case LMM_Schedule::TalkType_StudentTalk:
            case LMM_Schedule::TalkType_MemorialInvitation:
            case LMM_Schedule::TalkType_OtherFMVideoPart:
                if (asgn->theme().isEmpty()) {
                    basetagname = "Unused";
                } else {
                    if (asgn->classnumber() == 1) {
                        fmID++;
                        if (asgn->talkId() != LMM_Schedule::TalkType_SampleConversationVideo && asgn->talkId() != LMM_Schedule::TalkType_ApplyYourself)
                            currentDuration += 1;
                    }
                    basetagname = "FM" + QVariant(fmID).toString();
                }
                break;
            case LMM_Schedule::TalkType_LivingTalk1:
                elapsedMinutes = 52;
                basetagname = "CL1";
                break;
            case LMM_Schedule::TalkType_LivingTalk2:
                basetagname = "CL2";
                break;
            case LMM_Schedule::TalkType_LivingTalk3:
                basetagname = "CL3";
                break;
            case LMM_Schedule::TalkType_CBS:
                basetagname = "CBS";
                break;
            case LMM_Schedule::TalkType_COTalk:
                elapsedMinutes += 3; // Concluding comments
                basetagname = "CO";
                break;
            }
            QString classLetter(abcd.mid(asgn->classnumber() - 1, 1));
            lbltheme = "!" + basetagname + "_THEME!";
            lblsource = "!" + basetagname + "_SOURCE!";
            lblspkrname =
                    "!" + basetagname + (cbs ? "_CONDUCTOR" : "_SPEAKER") + (asgn->canMultiSchool() ? "_" + classLetter : "") + "!";
            lblasstname =
                    "!" + basetagname + (cbs ? "_READER" : "_ASSISTANT") + (asgn->canMultiSchool() ? "_" + classLetter : "") + "!";
            lblnote =
                    "!" + basetagname + "_NOTES" + (asgn->canMultiSchool() ? "_" + classLetter : "") + "!";
            lbltime = "!" + basetagname + "_TIME!";
            //! need to convert "time" from duration to actual TOD
            time = QString::number(asgn->time());
            lblstarttime = "!" + basetagname + "_STARTTIME!";

            /*
            qDebug() << "-------------------TAGS";
            qDebug() << "lbltheme" << lbltheme;
            qDebug() << "theme" << lbltheme;
            qDebug() << "lblsource" << lblsource;
            qDebug() << "source" << source;
            qDebug() << "lblspkrname" << lblspkrname;
            qDebug() << "spkrname" << spkrname;
            qDebug() << "lblasstname" << lblasstname;
            qDebug() << "asstname" << asstname;
            qDebug() << "lbltime" << lbltime;
            qDebug() << "time" << time;
            */

            context.replace(lbltheme, theme);
            context.replace(lblsource, source);
            context.replace(lblspkrname, spkrname);
            context.replace(lblasstname, asstname);
            context.replace(lblnote, note);
            context.replace(lbltime, time);
            context = replaceTimeTag(context, basetagname + "_STARTTIME", starttime.addSecs(60 * elapsedMinutes));
            if (asgn->classnumber() == 1)
                elapsedMinutes += currentDuration;
            if (asgn->canMultiSchool())
                context.replace(QString("!%1_SAMPLEVIDEO!").arg(basetagname), asgn->talkId() == LMM_Schedule::TalkType_SampleConversationVideo ? "Yes" : "");

            // work sheet
            QString studyname = "";
            QString studynumber = "";
            QString exercises = "";

            if (studenttalk) {
                context.replace(QString("!%1_TH_NO!").arg(basetagname), QVariant(asgn->study_number()).toString());
                context.replace(QString("!%1_TH_NAME!").arg(basetagname), asgn->study_name());

                if (asgn->date().year() > 2018) {
                    exercises = "";
                    studynumber = QVariant(asgn->study_number()).toString();
                    QString unknowncounselpoint;
                    schoolStudentStudy *study = studenttalk->getCounselPoint(unknowncounselpoint);
                    if (study)
                        studyname = study->getName();
                } else {
                    context.replace(QString("!%1_SETTING!").arg(basetagname), studenttalk->setting());
                    // student's study
                    if (studenttalk->speaker()) {
                        QString unknowncounselpoint;
                        schoolStudentStudy *study = studenttalk->getCounselPoint(unknowncounselpoint);
                        if (study) {
                            if (study->getExercise())
                                exercises = checkmark;
                            studynumber = QVariant(study->getNumber()).toString();
                            studyname = study->getName();
                            if (!unknowncounselpoint.isEmpty())
                                studyname = QString("%1 (%2)").arg(unknowncounselpoint, studyname);
                        }
                    }
                }
                context.replace(QString("!%1_SPEAKER_%2_EX_DONE!").arg(basetagname, classLetter), exercises);
                context.replace(QString("!%1_SPEAKER_%2_STUDYNAME!").arg(basetagname, classLetter), studyname);
                context.replace(QString("!%1_SPEAKER_%2_STUDY!").arg(basetagname, classLetter), studynumber);
            }
            context.replace("!NOTES!", asgn->note());

            // other school
            if (context.contains("!OTHER_SCHOOLS!")) {
                if (s.getClassesQty() == 1) {
                    context.remove("!OTHER_SCHOOLS!");
                    context.remove("!OTHER_SCHOOLS_NAMES!");
                } else {
                    context.replace("!OTHER_SCHOOLS!", tr("Other schools"));
                    QString otherschoolnames = "";
                    for (int schools2 = 2; schools2 < s.getClassesQty() + 1; schools2++) {
                        otherschoolnames.append(tr("Class"));
                        if (schools2 == 2) {
                            otherschoolnames.append(" II");
                        } else if (schools2 == 3) {
                            otherschoolnames.append(" III");
                        } else {
                            otherschoolnames.append(QVariant(schools2).toString());
                        }
                        otherschoolnames.append(": <br/>");
                        bool isFirst(true);
                        for (LMM_Assignment *otherasgn : prog) {
                            if (otherasgn->classnumber() != schools2)
                                continue;
                            if (isFirst)
                                isFirst = false;
                            else
                                otherschoolnames.append("<br/>");
                            LMM_Assignment_ex *otherstudenttalk(qobject_cast<LMM_Assignment_ex *>(otherasgn));
                            otherschoolnames.append(otherstudenttalk->talkName() + " ");
                            if (otherstudenttalk->speaker())
                                otherschoolnames.append(otherasgn->speaker()->fullname());
                            if (otherstudenttalk->assistant())
                                otherschoolnames.append(" / " + otherstudenttalk->assistant()->fullname());
                        }
                    }
                    context.replace("!OTHER_SCHOOLS_NAMES!", otherschoolnames);
                }
            }
        }
    }

    // next week
    if (context.contains("_NW!")) {
        QSharedPointer<LMM_Meeting> nw_mtg(new LMM_Meeting());
        if (nw_mtg->loadMeeting(date.addDays(7), true)) {
            context.replace("!LM_SOURCE_NW!", nw_mtg->bibleReading());
            QList<LMM_Assignment *> prog = nw_mtg->getAssignments();
            int fmID(0);
            for (LMM_Assignment *asgn : prog) {
                QString basetagname = "";
                switch (asgn->talkId()) {
                case LMM_Schedule::TalkType_Treasures:
                    basetagname = "GW1";
                    break;
                case LMM_Schedule::TalkType_Digging:
                    basetagname = "GW2";
                    break;
                case LMM_Schedule::TalkType_BibleReading:
                    basetagname = "GW3";
                    break;
                case LMM_Schedule::TalkType_SampleConversationVideo:
                case LMM_Schedule::TalkType_ApplyYourself:
                case LMM_Schedule::TalkType_InitialCall:
                case LMM_Schedule::TalkType_ReturnVisit1:
                case LMM_Schedule::TalkType_ReturnVisit2:
                case LMM_Schedule::TalkType_ReturnVisit3:
                case LMM_Schedule::TalkType_BibleStudy:
                case LMM_Schedule::TalkType_StudentTalk:
                case LMM_Schedule::TalkType_MemorialInvitation:
                case LMM_Schedule::TalkType_OtherFMVideoPart:
                    if (asgn->theme().isEmpty()) {
                        continue;
                    } else {
                        if (asgn->classnumber() == 1)
                            fmID++;
                        basetagname = "FM" + QVariant(fmID).toString();
                    }
                    break;
                case LMM_Schedule::TalkType_LivingTalk1:
                    basetagname = "CL1";
                    break;
                case LMM_Schedule::TalkType_LivingTalk2:
                    basetagname = "CL2";
                    break;
                case LMM_Schedule::TalkType_LivingTalk3:
                    basetagname = "CL3";
                    break;
                case LMM_Schedule::TalkType_CBS:
                    basetagname = "CBS";
                    break;
                case LMM_Schedule::TalkType_COTalk:
                    basetagname = "CO";
                    break;
                }
                QString lbltheme = "!" + basetagname + "_THEME_NW!";
                context.replace(lbltheme, asgn->theme());
                if (asgn->canCounsel()) {
                    QString classLetter(abcd.mid(asgn->classnumber() - 1, 1));
                    QString lblspkrname = "!" + basetagname + "_SPEAKER_" + classLetter + "_NW!";
                    QString spkrname = "";
                    if (asgn->speaker())
                        spkrname = asgn->speaker()->fullname();
                    context.replace(lblspkrname, spkrname);
                }
            }
        }
        context.replace("!CO_THEME_NW!", "");
        context.replace("!CBS_THEME_NW!", "");
    }

    // week after next week
    // TODO

    context.replace("!MWB_COLOR!", mtg->monthlyColor().name());
    context.replace("!MWB_RGB!", mtg->monthlyRGBColor());
    context.replace("!MWB_LIGHT_COLOR!", mtg->monthlyLightColor().name());

    if (showWorkbookNumber) {
        context.replace("!MWB_NO!", QVariant(mtg->date().month()).toString());
    } else {
        context.replace("!MWB_NO!", "");
    }
    context.remove(QRegularExpression("!SCHOOL[1-3]!"));
    context.remove(QRegularExpression("!H_[A-Z|a-z|_]*!"));
    context.remove(QRegularExpression("!NO[1-3]_[A-Z|a-z|_]*!"));
    context.remove(QRegularExpression("!NO[1-3]!"));
    context.remove(QRegularExpression("!OTHER_[A-Z|a-z|_]*!"));
    context.remove(QRegularExpression("!FM[0-4]_[A-Z|a-z|_]*!"));

    delete mtg;

    return context;
}
