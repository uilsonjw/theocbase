/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import net.theocbase 1.0

ScrollView {
    id: mwChairmanPanel
    property string title: "Midweek Meeting Chairman"
    width: 500
    height: 400
    clip: true

    property LMM_Meeting meeting
    onMeetingChanged: {
    }

    anchors.fill: parent

    function save(id, classnumber)
    {
        var b = id < 1 ? null : CPersons.getPerson(id)
        switch(classnumber){
        case 1:
            meeting.chairman = b
            break;
        case 2:
            meeting.counselor2 = b
            break;
        case 3:
            meeting.counselor3 = b
            break;
        }
        if (meeting.date.getFullYear() >= 2018) {
            // sample conversation video
            var video = meeting.getAssignment(LMM_Schedule.TalkType_SampleConversationVideo, 0, classnumber)
            if (video !== null) {
                video.speaker = b
                video.save()
            }
            // apply yourself to reading and teaching
            var apply = meeting.getAssignment(LMM_Schedule.TalkType_ApplyYourself, 0, classnumber)
            if (apply !== null) {
                apply.speaker = b
                apply.save()
            }
        }
        meeting.save()
    }

    ColumnLayout {
        id: layout
        x: 10
        y: 10
        width: mwChairmanPanel.width - 20

        // Chairman
        RowLayout {
            ToolButton {
                icon.source: "qrc:/icons/servant.svg"
                background: null
                Layout.alignment: Qt.AlignLeft
                ToolTip.text: qsTr("Chairman")
                ToolTip.visible: hovered
            }
            ComboBoxTable {
                id: comboChairman
                Layout.fillWidth: true
                currentText: meeting.chairman ? meeting.chairman.fullname : ""
                column4.visible: false
                onBeforeMenuShown: {
                    model = meeting.getChairmanList()
                    column2.resizeToContents()
                }
                onRowSelected: save(id, 1)
            }
        }
        // Counselor 2
        RowLayout {
            ToolButton {
                icon.source: "qrc:/icons/servant.svg"
                background: null
                Layout.alignment: Qt.AlignLeft
                ToolTip.text: qsTr("Auxiliary Classroom Counselor II")
                ToolTip.visible: hovered
                visible: meeting.classes > 1
            }

            ComboBoxTable {
                id: comboCounselor2
                Layout.fillWidth: true
                column4.visible: false
                visible: meeting.classes > 1
                currentText: meeting.counselor2 ? meeting.counselor2.fullname : ""
                onBeforeMenuShown: {
                    model = meeting.getChairmanList()
                    column2.resizeToContents()
                }
                onRowSelected: save(id, 2)
            }
        }
        RowLayout {
            ToolButton {
                icon.source: "qrc:/icons/servant.svg"
                background: null
                Layout.alignment: Qt.AlignLeft
                ToolTip.text: qsTr("Auxiliary Classroom Counselor III")
                ToolTip.visible: hovered
                visible: meeting.classes > 2
            }

            ComboBoxTable {
                id: comboCounselor3
                Layout.fillWidth: true
                column4.visible: false
                visible: meeting.classes > 2
                currentText: meeting.counselor3 ? meeting.counselor3.fullname : ""
                onBeforeMenuShown: {
                    model = meeting.getChairmanList()
                    column2.resizeToContents()
                }
                onRowSelected: save(id, 3)
            }
        }
    }
}
