/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.10
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.1
import net.theocbase 1.0

Rectangle {
    radius: 5
    id: lmmpage
    LMM_Meeting {
        id: myMeeting

        function updateOnCounselorChange() {
            listView.model = myMeeting.getAssignmentsVariant()
            // calculate listView height
            var listViewHeight = listView.model.length > 0 ? showSongTitles ? 180 : 170 : 0
            listViewHeight += listView.model.length * 40
            listView.height = listViewHeight
            listView.Layout.preferredHeight = listViewHeight
            lmmpage.height = listViewHeight + (showSongTitles ? 240 : 220)
        }

        onCounselor2Changed: updateOnCounselorChange()
        onCounselor3Changed: updateOnCounselorChange()
    }

    AssignmentController { id: myController }
    property string bibleReading
    property string cbsSource
    property int fontsize: defaultfontsize
    property int selectedTalkId
    property int selectedSequence
    property int selectedClass
    property bool noMultiSchool: myMeeting.date.getFullYear() < 2018 && myMeeting.date.getDate() < 7
    property bool editpossible: true
    property bool isLoading: false

    property string chairmanName: "ChairmanName"

    property string lastEditPage: ""
    property int lastEditAssignment: 0
    signal showEditPanel(var name, var args)
    signal showNotes(var name, var meeting, var openComments)

    function loadSchedule(currentdate){
        // load meeting data        
        isLoading = true
        myMeeting.loadMeeting(currentdate)
        listView.model = myMeeting.getAssignmentsVariant()       
        // calculate listView height
        var listViewHeight = listView.model.length > 0 ? (showSongTitles ? 180 : 170) : 0
        for(var i=0;i<listView.model.length;i++){
            listViewHeight += 40
        }

        listView.height = listViewHeight
        listView.Layout.preferredHeight = listViewHeight
        height = listViewHeight + (showSongTitles ? 240 : 220)
        Layout.preferredHeight = height

        chairmanRow.visible = (listView.model.length > 0)
        // song texts
        songBeginningRow.visible = (listView.model.length > 0)
        songEndRow.visible = (listView.model.length > 0)
        // comments
        openingCommentsRow.visible = (listView.model.length > 0)
        reviewCommentsRow.visible = (listView.model.length > 0)
        // bible reading
        bibleReading = myMeeting.bibleReading
        cbsSource = myMeeting.getAssignment(LMM_Schedule.TalkType_CBS) ? myMeeting.getAssignment(LMM_Schedule.TalkType_CBS).source : ""
        reviewCommentsRow.cotalkfound = myMeeting.getAssignment(LMM_Schedule.TalkType_COTalk) ? true : false        

        if (lastEditAssignment > 0) {
            if (lastEditAssignment == 100) {
                openingCommentsRow.clicked()
            } else if (lastEditAssignment == 200) {
                reviewCommentsRow.clicked()
            }else {
                var talkId = 0
                for(i=0;i<listView.model.length;i++){
                    if (listView.model[i].talkId === lastEditAssignment || listView.model[i].talkId > lastEditAssignment) {
                        editButtonClicked(listView.model[i].talkId, listView.model[i].sequence, 1)
                        break
                    }
                }
            }
        }
        isLoading = false
    }

    function getMonthlyColor(){
        return myMeeting.monthlyColor()
    }

    function editButtonClicked(index, sequence, classnumber){
        var dialogType = ""               
        switch(index){
        case LMM_Schedule.TalkType_BibleReading:
        case LMM_Schedule.TalkType_InitialCall:
        case LMM_Schedule.TalkType_ReturnVisit1:
        case LMM_Schedule.TalkType_ReturnVisit2:
        case LMM_Schedule.TalkType_ReturnVisit3:
        case LMM_Schedule.TalkType_BibleStudy:
        case LMM_Schedule.TalkType_StudentTalk:
        case LMM_Schedule.TalkType_MemorialInvitation:
            dialogType = "StudentAssignmentPanel.qml";
            break;
        case LMM_Schedule.TalkType_CBS:
            dialogType = "CBSPanel.qml";
            break;
        default:
            dialogType =  "AssignmentPanel.qml";
            break;
        }
        showEditPanel(dialogType, { "currentAssignment" : myMeeting.getAssignment(index, sequence, classnumber) })
        lastEditAssignment = index
    }

    Connections {
        target: openingCommentsRow
        function onClicked() {
            showEditPanel("LMMNotesPanel.qml", { "meeting" : myMeeting, "openComments" : true })
            lastEditAssignment = 100
        }
    }

    Connections {
        target: reviewCommentsRow
        function onClicked() {
            showEditPanel("LMMNotesPanel.qml", { "meeting" : myMeeting, "openComments" : false })
            lastEditAssignment = 200
        }
    }

    function getIconSource(index){
        switch(index){
        case LMM_Schedule.TalkType_Treasures: return "qrc:///icons/lmm-gw.svg"
        case LMM_Schedule.TalkType_SampleConversationVideo:
        case LMM_Schedule.TalkType_InitialCall:
        case LMM_Schedule.TalkType_ReturnVisit1:
        case LMM_Schedule.TalkType_ReturnVisit2:
        case LMM_Schedule.TalkType_ReturnVisit3:
        case LMM_Schedule.TalkType_BibleStudy:
        case LMM_Schedule.TalkType_ApplyYourself:
        case LMM_Schedule.TalkType_StudentTalk:
        case LMM_Schedule.TalkType_MemorialInvitation:
        case LMM_Schedule.TalkType_OtherFMVideoPart: return "qrc:///icons/lmm-fm.svg"
        case LMM_Schedule.TalkType_LivingTalk1: return "qrc:///icons/lmm-cl.svg"
        default: return ""
        }
    }

    function getTextColor(index){
        switch(index){
        case LMM_Schedule.TalkType_Treasures:
        case LMM_Schedule.TalkType_Digging:
        case LMM_Schedule.TalkType_BibleReading: return Qt.rgba(101/255, 97/255, 100/255, 1)
        case LMM_Schedule.TalkType_SampleConversationVideo:
        case LMM_Schedule.TalkType_InitialCall:
        case LMM_Schedule.TalkType_ReturnVisit1:
        case LMM_Schedule.TalkType_ReturnVisit2:
        case LMM_Schedule.TalkType_ReturnVisit3:
        case LMM_Schedule.TalkType_BibleStudy:
        case LMM_Schedule.TalkType_ApplyYourself:
        case LMM_Schedule.TalkType_StudentTalk:
        case LMM_Schedule.TalkType_MemorialInvitation:
        case LMM_Schedule.TalkType_OtherFMVideoPart: return Qt.rgba(199/255, 137/255, 9/255, 1)
        case LMM_Schedule.TalkType_LivingTalk1:
        case LMM_Schedule.TalkType_LivingTalk2:
        case LMM_Schedule.TalkType_LivingTalk3:
        case LMM_Schedule.TalkType_CBS: return Qt.rgba(153/255, 19/255, 30/255, 1)
        case LMM_Schedule.TalkType_COTalk: return "black"
        default: return "white"
        }
    }

    function getBackgroundColor(index){
        switch(index){
        case LMM_Schedule.TalkType_Treasures:
        case LMM_Schedule.TalkType_Digging:
        case LMM_Schedule.TalkType_BibleReading: return Qt.rgba(101/255, 97/255, 100/255, 0.1)
        case LMM_Schedule.TalkType_SampleConversationVideo:
        case LMM_Schedule.TalkType_InitialCall:
        case LMM_Schedule.TalkType_ReturnVisit1:
        case LMM_Schedule.TalkType_ReturnVisit2:
        case LMM_Schedule.TalkType_ReturnVisit3:
        case LMM_Schedule.TalkType_BibleStudy:
        case LMM_Schedule.TalkType_ApplyYourself:
        case LMM_Schedule.TalkType_StudentTalk:
        case LMM_Schedule.TalkType_MemorialInvitation:
        case LMM_Schedule.TalkType_OtherFMVideoPart: return Qt.rgba(199/255, 137/255, 9/255, 0.1)
        case LMM_Schedule.TalkType_LivingTalk1:
        case LMM_Schedule.TalkType_LivingTalk2:
        case LMM_Schedule.TalkType_LivingTalk3:
        case LMM_Schedule.TalkType_CBS: return Qt.rgba(153/255, 19/255, 30/255, 0.1)
        case LMM_Schedule.TalkType_COTalk: return "white"
        default:return "white"
        }
    }


    function getHeaderText(index){
        switch(index){
        case LMM_Schedule.TalkType_Treasures:
        case LMM_Schedule.TalkType_Digging:
        case LMM_Schedule.TalkType_BibleReading: return qsTr("TREASURES FROM GOD'S WORD")
        case LMM_Schedule.TalkType_SampleConversationVideo:
        case LMM_Schedule.TalkType_InitialCall:
        case LMM_Schedule.TalkType_ReturnVisit1:
        case LMM_Schedule.TalkType_ReturnVisit2:
        case LMM_Schedule.TalkType_ReturnVisit3:
        case LMM_Schedule.TalkType_BibleStudy:
        case LMM_Schedule.TalkType_ApplyYourself:
        case LMM_Schedule.TalkType_StudentTalk:
        case LMM_Schedule.TalkType_MemorialInvitation:
        case LMM_Schedule.TalkType_OtherFMVideoPart: return qsTr("APPLY YOURSELF TO THE FIELD MINISTRY")
        case LMM_Schedule.TalkType_LivingTalk1:
        case LMM_Schedule.TalkType_LivingTalk2:
        case LMM_Schedule.TalkType_LivingTalk3:
        case LMM_Schedule.TalkType_CBS:
        case LMM_Schedule.TalkType_COTalk: return qsTr("LIVING AS CHRISTIANS")
        default: return ""
        }
    }

    ColumnLayout {
        id: layout
        anchors.fill: parent
        anchors.margins: 10
        spacing: 0

        RowLayout {
            Layout.preferredHeight: 30
            Text {
                text: "●●○ " + Qt.locale().dayName(mwDate.getDay(), Locale.LongFormat) + ", " +
                      mwDate.toLocaleDateString(Qt.locale(), Locale.ShortFormat) + " | " + qsTr("Midweek Meeting").toUpperCase()
                font.pointSize: 12
                color: Qt.rgba(47/255, 72/255, 112/255, 1)
                Layout.preferredHeight: 30
                Layout.alignment: Qt.AlignVCenter
                verticalAlignment: Text.AlignVCenter
                Layout.fillWidth: true
                elide: Text.ElideRight
            }
            Item {
                Layout.fillHeight: true
                width: 30
                ToolButton {
                    icon.source: myMeeting.notes ? "qrc:/icons/notes-text.svg" : "qrc:/icons/notes.svg"
                    icon.color: Qt.rgba(47/255, 72/255, 112/255, 1)
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.right
                    ToolTip.text: qsTr("Notes", "Meeting Notes")
                    ToolTip.visible: hovered
                    visible: canViewMeetingNotes
                    onClicked: {
                        showEditPanel("MeetingNotes.qml", { "title" : qsTr("Midweek Meeting"), "meeting" : myMeeting, "editable" : canEditMeetingNotes })
                    }
                }
            }          
        }

        Label {
            id: importText
            visible : canEditMidweekMeetingSchedule && (listView.model === undefined || listView.model.length === 0) && editpossible && myMeeting.date.getFullYear() > 2015
            horizontalAlignment: Text.AlignHCenter
            text: qsTr("<html><a href='#'>" + qsTr("Import Schedule...") + "</a></html>")
            font.pointSize: fontsize
            onLinkActivated: {
                console.log("Import schedule link clicked!")
                root.importClicked()
            }

            DropArea {
                anchors.fill: parent
                onEntered: {
                    if (!drag.hasUrls || !drag.urls[0].endsWith(".epub"))
                        drag.accepted = false
                }
                onDropped: {
                    console.log("Dropped " + drop.urls)
                    if (drop.urls.length > 0)
                        root.fileDropped(drop.urls[0])
                }
            }
        }
        ScheduleRowItem {
            id: chairmanRow
            timeText.visible: false
            Layout.preferredHeight: 30
            clickable: true
            editable: canEditMidweekMeetingSchedule && editpossible
            RowLayout {                
                anchors.fill: parent
                anchors.leftMargin: noMultiSchool || myMeeting.classes == 1 ? 45 : 15
                anchors.rightMargin:  noMultiSchool || myMeeting.classes == 1 ? 30 : 0
                Repeater {
                    id: chairmanRepeater
                    model: noMultiSchool ? 1 : myMeeting.classes
                    Rectangle {
                        id: repeaterRect
                        color: Qt.rgba(0,0,0,0.1*index)
                        Layout.preferredHeight: 30
                        //property alias nametext: textChairman.text // chairmanCombo.currentText
                        Layout.fillWidth: true
                        RowLayout {
                            anchors.fill: parent
                            Rectangle {
                                Layout.leftMargin: 3
                                height: 24; width: 24; radius: 2
                                color: "#254f8e"
                                visible: myMeeting.classes > 1 && !noMultiSchool
                                Text {
                                    anchors.fill: parent
                                    text: index == 0 ? qsTr("MH","abbreviation for main hall") :
                                                       (index == 1 ? qsTr("A1", "abbreviation for auxiliary classroom 1") :
                                                                     qsTr("A2", "abbreviation for auxiliary classroom 2"))
                                    color: "white"
                                    horizontalAlignment: Text.AlignHCenter
                                    verticalAlignment: Text.AlignVCenter
                                    font.pointSize: fontsize
                                }
                            }
                            Text {
                                id: counselorTitle
                                text: index == 0 ? qsTr("Chairman") : qsTr("Counselor")
                                color: "grey"
                                font.pointSize: fontsize
                            }
                            Item { Layout.fillWidth: true }
                            Text {
                                id: textChairman
                                Layout.rightMargin: myMeeting.classes > 1 && !noMultiSchool ? 45 : 15
                                text: index == 0 ?
                                          myMeeting.chairman ?
                                              myMeeting.chairman.fullname : "" : index == 1 ?
                                                                   myMeeting.counselor2 ?
                                                      myMeeting.counselor2.fullname : "" : myMeeting.counselor3 ?
                                                          myMeeting.counselor3.fullname : ""
                                font.pointSize: fontsize
                            }
                        }
                    }
                }
            }
            onClicked: showEditPanel("MWMeetingChairmanPanel.qml", { "meeting" : myMeeting })
        }

        ScheduleRowItem {
            id: songBeginningRow
            Layout.preferredHeight: showSongTitles ? 40 : 30
            themeText.text: qsTr("Song %1 and Prayer").arg(myMeeting.songBeginning.toString()) +
                            (showSongTitles ? "\n" + myMeeting.songBeginningTitle : "")
            themeText.color: "grey"            
            timeText.font.pointSize: 12
            timeText.color: Qt.rgba(101/255, 97/255, 100/255, 1)
            timeText.text: "\u266B"
            nameText1.text: myMeeting ? myMeeting.prayerBeginning ? myMeeting.prayerBeginning.fullname : "" : ""
            timebox.color: "transparent"
            editable: canEditMidweekMeetingSchedule && editpossible
            onClicked: showEditPanel("MWMeetingPrayerPanel.qml", { "beginningPrayer" : true, "meeting" : myMeeting })
        }

        ScheduleRowItem {
            id: openingCommentsRow
            themeText.text: qsTr("Opening Comments")
            themeText.color: "grey"
            timeText.color: "white"
            timeText.text: myMeeting.date.getFullYear() < 2020 ? "3" : "1"
            timebox.color: Qt.rgba(101/255, 97/255, 100/255, 1)
            Layout.preferredHeight: 30
            Layout.bottomMargin: 10                        
            editable: canEditMidweekMeetingSchedule && editpossible
        }

        ListView {
            id: listView
            height: 100
            delegate: listRow
            Layout.fillWidth: true
            interactive: false
        }

        ScheduleRowItem {            
            id: reviewCommentsRow
            themeText.text: qsTr("Concluding Comments")
            themeText.color: "grey"
            timeText.color: "white"
            timeText.text: "3"
            timebox.color: Qt.rgba(153/255, 19/255, 30/255, 1)
            Layout.preferredHeight: 30                        
            editable: canEditMidweekMeetingSchedule && editpossible
            property bool cotalkfound: false
            // Adjust margins when CO visit -> review should be before service talk
            Layout.topMargin: cotalkfound ? - 30 : 10
            Layout.bottomMargin: cotalkfound ? 40 : 0
        }


        ScheduleRowItem {
            id: songEndRow
            Layout.preferredHeight: showSongTitles ? 40 : 30
            themeText.text: qsTr("Song %1 and Prayer").arg(myMeeting.songEnd.toString()) +
                            (showSongTitles ? "\n" + myMeeting.songEndTitle : "")
            themeText.color: "grey"
            timeText.font.pointSize: 12
            timeText.color: Qt.rgba(153/255, 19/255, 30/255, 1)
            timeText.text: "\u266B"
            nameText1.text: myMeeting ? myMeeting.prayerEnd ? myMeeting.prayerEnd.fullname : "" : ""
            timebox.color: "transparent"
            editable: canEditMidweekMeetingSchedule && editpossible
            onClicked: showEditPanel("MWMeetingPrayerPanel.qml", { "beginningPrayer" : false, "meeting" : myMeeting })
        }

        Item { Layout.fillHeight: true }
    }
    Component {
        id: listRow
        Rectangle {
            id: headerBackground
            height: hdr ? (model.index === 0 ? 80 : model.modelData.talkId === LMM_Schedule.TalkType_LivingTalk1 ? (showSongTitles ? 130 : 120) : 90) :
                          model.modelData.talkId === LMM_Schedule.TalkType_COTalk ? 80 : 40
            width: layout.width
            Component.onCompleted: {
                headerBackground.color = getBackgroundColor(model.modelData.talkId)
                rowIcon.source = getIconSource(model.modelData.talkId)
                hdr_text_color = getTextColor(model.modelData.talkId)
                headerText.text = getHeaderText(model.modelData.talkId)
            }

            property bool hdr: typeof model.modelData.talkId==="undefined" ?
                                   false :
                                   (model.modelData.talkId === LMM_Schedule.TalkType_Treasures ||
                                    (index > 0 && model.modelData.talkId !== listView.model[index-model.modelData.classnumber].talkId &&
                                     listView.model[index-model.modelData.classnumber].talkId === LMM_Schedule.TalkType_BibleReading) ||
                                    model.modelData.talkId === LMM_Schedule.TalkType_LivingTalk1) && (model.modelData.classnumber === 1)
            property string hdr_text_color: "white"

            ColumnLayout {
                anchors.fill: parent
                //anchors.margins: 5
                spacing: 0
                Rectangle {
                    Layout.fillWidth: true
                    height: 10
                    color: "white"
                    visible: hdr && model.index > 0
                }

                RowLayout {
                    Rectangle {
                        color: hdr_text_color
                        height: 40
                        width: 40
                        visible: hdr
                        Image {
                            id: rowIcon
                            anchors.fill: parent
                            anchors.margins: 5
                        }
                    }
                    // header for meeting part
                    ColumnLayout {
                        Text {
                            id: headerText
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            verticalAlignment: Text.AlignVCenter
                            elide: Text.ElideRight
                            font.bold: true
                            font.pointSize: 16
                            color: hdr_text_color
                            visible: hdr
                        }
                    }
                }
                ScheduleRowItem {
                    Layout.preferredHeight: showSongTitles ? 40 : 30
                    themeText.text: qsTr("Song %1").arg(myMeeting.songMiddle.toString()) +
                                    (showSongTitles ? "\n" + myMeeting.songMiddleTitle : "")
                    themeText.color: "grey"
                    timeText.font.pointSize: 12
                    timeText.color: hdr_text_color
                    timeText.text: "\u266B"
                    timebox.color: "transparent"
                    enabled: false                    
                    editable: canEditMidweekMeetingSchedule
                    visible: hdr && (model.modelData.talkId === LMM_Schedule.TalkType_LivingTalk1)
                }

                ScheduleRowItem {
                    timeText.text: modelData.time === null ? 0 : model.modelData.time
                    timeText.color: "white"
                    timebox.color: hdr_text_color
                    timebox.visible: model.modelData.classnumber === 1 ? true : false
                    themeText.text: model.modelData.classnumber === 1 ? model.modelData.theme : ""
                    themeText.color: model.modelData.classnumber === 1 ? "black" : "grey"

                    nameText1.text: model.modelData.volunteer ? ">" + model.modelData.volunteer.fullname : model.modelData.speakerFullName +
                                                                      (model.modelData.talkId === LMM_Schedule.TalkType_CBS ? " (" + qsTr("Conductor") + ")" : "")
                    nameText2.text: model.modelData.assistant ? model.modelData.assistant.fullname +
                                                                (model.modelData.talkId === LMM_Schedule.TalkType_CBS ? " (" + qsTr("Reader") + ")" : "")
                                                                : ""
                    nameText2.color: "grey"                    
                    Rectangle {
                        anchors.right: parent.right
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.rightMargin: 10
                        height: 24; width: 24; radius: 2
                        color: "#254f8e"
                        visible: !isLoading ? (myMeeting.classes > 1 && !noMultiSchool && model.modelData.canMultiSchool && !parent.rowHovered) : false
                        Text {
                            anchors.fill: parent
                            text: model.modelData.classnumber === 1 ? qsTr("MH","abbreviation for main hall") :
                                  model.modelData.classnumber === 2 ? qsTr("A1", "abbreviation for auxiliary classroom 1") :
                                                                      qsTr("A2", "abbreviation for auxiliary classroom 2")
                            color: "white"
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.pointSize: fontsize
                        }
                    }                    
                    editable: canEditMidweekMeetingSchedule && editpossible

                    onClicked: {
                        if (!editpossible) return
                        selectedTalkId = model.modelData.talkId
                        selectedSequence = model.modelData.sequence
                        selectedClass = model.modelData.classnumber
                        editButtonClicked(model.modelData.talkId, model.modelData.sequence, model.modelData.classnumber)
                    }
                }
            }
        }
    }
}

