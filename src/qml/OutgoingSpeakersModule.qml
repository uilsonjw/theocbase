/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.3
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import net.theocbase 1.0

Rectangle {
    radius: 5
    id: outgoingSpeakersEdit
    property string title: "Outgoing Speakers This Weekend"
    width: 600
    height: 200
    property date currentDate
    property int nameColWidth: 10
    property int lastEditIndex: -1

    signal showEditPanel(var name, var args)
    signal movedTodoList()

    function reload() {
        outModel.loadList(currentDate)
    }

    OutgoingSpeakersModel {
        id: outModel
        Component.onCompleted: outModel.loadList(currentDate)
        onModelChanged: {
            height = 40 + (outModel.rowCount()  * 40) + 20 + 40
            var nameTest = ""
            var i
            for (i = 0; i < outModel.rowCount(); i++) {
                if (outModel.get(i).speaker.length > nameTest.length)
                    nameTest = outModel.get(i).speaker
            }
            txtMetrics.text = nameTest
            nameColWidth = Math.max(50, txtMetrics.width + 10)
            // change edit page in the sidebar ?
            if (lastEditIndex > -1) {
                if (outModel.rowCount() -1 >= lastEditIndex) {
                    showEditPanel("OutgoingSpeakerEdit.qml", { "outModel": outModel, "modelIndex": lastEditIndex,
                                  "currentDate": pmController.date })
                } else {
                    showEditPanel("",{})
                    lastEditIndex = -1
                }
            }
        }
    }
    PublicMeetingController { id: pmController }
    onCurrentDateChanged: {
        pmController.date = currentDate
    }

    TextMetrics {
        id: txtMetrics
        font.pointSize: defaultfontsize
    }

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 10
        spacing: 0
        Rectangle {
            color: "#f7f4f4"
            Layout.fillWidth: true
            Layout.fillHeight: true
            ColumnLayout {
                anchors.fill: parent
                spacing: 0
                RowLayout {
                    Layout.fillWidth: true
                    Rectangle {
                        height: 40
                        width: 40
                        color: Qt.rgba(169/255, 146/255, 77/140, 1)
                        Image {
                            anchors.fill: parent
                            anchors.margins: 5
                            source: "qrc:/icons/speaker_outgoing.svg"
                            ColorOverlay {
                                anchors.fill: parent
                                source: parent
                                color: "white"
                            }
                        }
                    }
                    Text {
                        height: 40
                        text: qsTr("Outgoing speakers")
                        font.bold: true
                        font.pointSize: 16
                        font.capitalization: Font.AllUppercase
                        color: Qt.rgba(169/255, 146/255, 77/140, 1)
                        elide: Text.ElideRight
                        Layout.fillWidth: true
                    }
                }

                ListView {
                    id: listView
                    model: outModel
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    interactive: false
                    delegate: ScheduleRowItem {
                        width: listView.width
                        timeText.text: Qt.locale().dayName(date.getDay(), Locale.ShortFormat)
                        timebox.color: Qt.rgba(169/255, 146/255, 77/140, 1)
                        editable: canEditWeekendMeetingSchedule

                        RowLayout {
                            anchors.left: parent.left
                            anchors.leftMargin: 50
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.right: parent.right
                            anchors.rightMargin: 45
                            height: parent.height
                            Text {
                                text: speaker
                                font.pointSize: defaultfontsize
                                elide: Text.ElideRight
                                Layout.fillHeight: true
                                wrapMode: Text.WordWrap
                                verticalAlignment: Text.AlignVCenter
                                Layout.preferredWidth: nameColWidth
                            }
                            Text {
                                id: textTheme
                                Layout.fillWidth: true
                                Layout.fillHeight: true
                                font.pointSize: defaultfontsize
                                elide: Text.ElideRight
                                wrapMode: Text.WordWrap
                                verticalAlignment: Text.AlignVCenter
                                text: theme == "" ? "" : theme + " (" + themeNo + ")"
                            }
                            Text {
                                verticalAlignment: Text.AlignVCenter
                                Layout.fillHeight: true
                                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                                font.pointSize: defaultfontsize
                                text:  congregation
                            }
                        }
                        onClicked: {
                            if (!canEditWeekendMeetingSchedule)
                                return
                            showEditPanel("OutgoingSpeakerEdit.qml", { "outModel": outModel, "modelIndex": index,
                                          "currentDate": pmController.date })
                            lastEditIndex = index
                        }
                        button2icon: "qrc:/icons/move_to_todo.svg"
                        onClicked2: {
                            if (canEditWeekendMeetingSchedule && outModel.moveToTodo(index))
                                movedTodoList()
                        }

                        button3icon: "qrc:/icons/delete.svg"
                        onClicked3: {
                            if (canEditWeekendMeetingSchedule)
                                outModel.removeRow(index)
                        }
                    }
                }
            }
        }

        ScheduleRowItem {
            Layout.preferredHeight: 30
            Layout.topMargin: 10
            timebox.color: "transparent"
            timeText.text: ""
            themeText.text: outModel.length > 0 ?
                                qsTr("%1 speakers away this weekend","", outModel.length).arg(outModel.length) :
                                qsTr("No speakers away this weekend")
            button1icon: "qrc:/icons/add_circle.svg"
            editable: canEditWeekendMeetingSchedule
            onClicked: {
                outModel.addRow(-1,-1,-1)
                showEditPanel("OutgoingSpeakerEdit.qml", { "outModel": outModel, "modelIndex": outModel.rowCount()-1,
                              "currentDate": pmController.date })
                lastEditIndex = index                
            }
        }
    }
}
