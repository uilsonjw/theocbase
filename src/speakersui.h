/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPEAKERSUI_H
#define SPEAKERSUI_H

#include <QDialog>
#include <QTreeWidget>
#include <QGroupBox>
#include <QComboBox>
#include <QTimeEdit>
#include <QTreeWidgetItem>
#include <QListWidgetItem>
#include <QMouseEvent>
#include <QTimer>
#include <QQuickItem>
#include <QQmlContext>
#include <QSettings>
#include <memory>
#include "ccongregation.h"
#include "cpersons.h"
#include "cpublictalks.h"
#include "todo.h"

namespace Ui {
class speakersui;
}

class congregationTree : public QTreeWidget
{
    Q_OBJECT
public:
    congregationTree(QWidget * parent);
private:
    virtual void dropEvent(QDropEvent *event);
    virtual void dragMoveEvent(QDragMoveEvent *event);
signals:
    void changeSpeakerCongregation(int speakerindex, int congregationid, bool& done);
protected:
    void expandCircuit();
    QTimer expandTimer;
    QTreeWidgetItem* toExpandItem;
};

class speakersui : public QDialog
{
    Q_OBJECT
    
public:
    explicit speakersui(QWidget *parent = 0, int openingCongregation_id = 0);
    ~speakersui();

protected:
    void closeEvent(QCloseEvent *event);
    
private slots:

    void on_treeWidget_itemSelectionChanged();
    void on_treeWidget_itemClicked(QTreeWidgetItem *item, int column);
    void on_lineAddress_editingFinished();
    void on_buttonRemove_clicked();
    void on_buttonAddSpeaker_clicked();
    void on_buttonAddCongregation_clicked();
    void on_btnAddMultipleTalks_clicked();

    void on_buttonEditPublicTalks_clicked(bool checked);
    void changeCongregation(int index, int newcongid, bool& done);

    void on_btnBySpeaker_clicked();
    void on_btnByCongregation_clicked();
    void on_btnByCircuit_clicked();

    void on_filter_textChanged(const QString &arg1);

private:
    Ui::speakersui *ui;
    int ownCongregationId;

    enum class DisplayBy { Speaker, Congregation, Circuit } displayBy{ DisplayBy::Speaker };
    enum PublicTalkInOut { In, Out };

    QTreeWidgetItem* findCongregation(int congregId) const;
    QTreeWidgetItem* findSpeaker(int speakerId) const;

    void reloadSpeakers();
    void loadCongregations(QString current = "", bool showInfo = false);
    void loadCongregationInfo(int id);
    void setDayTimeCombo(QLabel*& label, QComboBox*& day, QTimeEdit*& time, ccongregation::meeting_dayandtime const& mdt);
    void loadSpeakerInfo();
    void showMap(QString address);
    void getPublicTalks();
    void clearPublicTalkSelection();
    int lang_id;
    bool save();
    bool SpeakerHasScheduledTalks(int speakerId, PublicTalkInOut inOut, QDate startDate);
    bool SpeakerHasScheduledTalks();
    person *GetSpeakerFromSpeakersList(int speakerid);
    void RemoveCongregation();
    void RemoveSpeaker();
    void RemoveSpeakerFromUI(int speakerId);
    int GetOwnCongId();
    bool IsOwnCong(int congId);

    QList<person*> speakerslist;
    ccongregation::congregation currentCongregation;
    person *currentSpeaker;
    sql_class *sql;
    QAction* actionFilterCircuit{ nullptr };
    QAction* actionFilterCongregation{ nullptr };
    QAction* actionFilterSpeaker{ nullptr };
signals:
    void updateScheduledTalks(int speakerId, int oldCongregationId, int newCongregationId, bool removed);
};

#endif // SPEAKERSUI_H
