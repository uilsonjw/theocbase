#include "midweekmeetingavailabilitychecker.h"

using namespace tbAvailability;


namespace
{
    const QString DATA_SRC_LMM_ASSIGNMENTS = "LMMAssignments";
    const QString DATA_SRC_LMM_MTG = "LMMMeeting";
    const int LOOKBACK_WEEKS = 25;   // number of historical weeks to analyse for "last used" date
}

// MidWeekMeetingAvailabilityChecker
//==================================

MidWeekMeetingAvailabilityChecker::MidWeekMeetingAvailabilityChecker(const QDate &meetingDate,
                                                                     const QDate &weekCommencingDate)
    : AvailabilityChecker(meetingDate,
                          weekCommencingDate,
                          std::vector<person::UseFor>({ person::LMM_Chairman, person::LMM_Treasures,
                                                        person::LMM_Digging, person::LMM_BibleReading,
                                                        person::LMM_PreparePresentations, person::LMM_InitialCall,
                                                        person::LMM_ReturnVisit, person::LMM_BibleStudy,
                                                        person::LMM_LivingTalks,
                                                        person::CBSReader, person::CBSConductor,
                                                        person::Assistant}),
                          std::vector<QString>({ DATA_SRC_LMM_ASSIGNMENTS, DATA_SRC_LMM_MTG }))
{


}

QString MidWeekMeetingAvailabilityChecker::GenerateAssignedPersonsSql(QString dataSourceId)
{
    if(dataSourceId == DATA_SRC_LMM_ASSIGNMENTS)
    {
        // NOTE: db may benefit from an index on the "lmm_assignment.date" column if table is large

        QString s =
            "SELECT a.date, a.assignee_id, a.assistant_id, s.talk_id"
            "FROM lmm_assignment a "
            "INNER JOIN lmm_schedule s "
            "on a.lmm_schedule_id = s.id "
            "WHERE s.active = 1 and date <= '%1' "
            "ORDER BY date DESC LIMIT %2";

        return s.arg(weekCommencingDate_.toString(Qt::ISODate), QString::number(LOOKBACK_WEEKS));
    }

    else if(dataSourceId == DATA_SRC_LMM_MTG)
    {
        // NOTE: db may benefit from an index on the "lmm_assignment.date" column if table is large

        QString s =
            "SELECT date, chairman, prayer_beginning, prayer_end, "
            "FROM lmm_meeting "
            "WHERE active AND date <= '%1' "
            "ORDER BY date DESC LIMIT %2";

        return s.arg(weekCommencingDate_.toString(Qt::ISODate), QString::number(LOOKBACK_WEEKS));
    }

    return QString();
}

person::UseFor MidWeekMeetingAvailabilityChecker::TalkIdToAssignment(int talkId)
{
    person::UseFor result;

    switch(talkId)
    {
        case 2:
            result = person::LMM_Treasures;
            break;

        case 3:
            result = person::LMM_Digging;
            break;

        case 4:
            result = person::LMM_BibleReading;
            break;

        case 5:
        case 17: // ApplyYourself.. it's practially the same thing.  No need to add a new usedfor
            result = person::LMM_PreparePresentations;
            break;

        case 6:
            result = person::LMM_InitialCall;
            break;

        case 7:
            result = person::LMM_ReturnVisit;
            break;

        case 8:
            result = person::LMM_BibleStudy;
            break;

        case 9:
        case 10:
            result = person::LMM_LivingTalks;
            break;

        case 12:
            result = person::CBSConductor; // or reader
            break;

        default:
            throw new UnknownTalkIException(talkId);
    }

    return result;
}

bool MidWeekMeetingAvailabilityChecker::IsStudentAssignment(person::UseFor assignment)
{
    switch(assignment)
    {
        case person::LMM_BibleReading:
        case person::LMM_InitialCall:
        case person::LMM_ReturnVisit:
        case person::LMM_BibleStudy:
            return true;
        default: return false;
    }
}

void MidWeekMeetingAvailabilityChecker::PopulateAssignments(QString dataSourceId, AvailabilityItem &result, const sql_items &assignedPersons)
{
    int personId = result.Id;

    if(dataSourceId == DATA_SRC_LMM_ASSIGNMENTS)
    {
        foreach(sql_item mtgAssigments, assignedPersons)
        {
            QDate dt = mtgAssigments.value("date").toDate();
            int assigneeId = mtgAssigments.value("assignee_id").toInt();
            int assistantId = mtgAssigments.value("assistant_id").toInt();
            int dbTalkId = mtgAssigments.value("talk_id").toInt();
            int talkId(0);
            int sequence(0);
            LMM_Schedule::splitDbTalkId(dbTalkId, talkId, sequence);

            person::UseFor assignment = TalkIdToAssignment(talkId);

            if(assigneeId == personId)
            {
                result.AddHomeAssignment(dt, assignment);
            }

            if(assistantId == personId)
            {
                if(IsStudentAssignment(assignment))
                {
                    result.AddHomeAssignment(dt, person::Assistant);
                }
                else if(assignment == person::CBSConductor)
                {
                    result.AddHomeAssignment(dt, person::CBSReader);
                }
                else
                {
                    Q_ASSERT(false); ; // shouldn't get here!
                }
            }
        }

        auto iterator = std::find_if(assignedPersons.begin(), assignedPersons.end(),
                                [&personId](const sql_item &assignedPerson) { return assignedPerson.value("speaker_id").toInt() == personId; });

        result.OutsideSpeaker = iterator != assignedPersons.end();
    }

    else if(dataSourceId == DATA_SRC_LMM_MTG)
    {
        foreach(sql_item mtgAssigments, assignedPersons)
        {
            QDate dt = mtgAssigments.value("date").toDate();
            int chairmanId = mtgAssigments.value("chairman").toInt();
            int prayerStartId = mtgAssigments.value("prayer_beginning").toInt();
            int prayerEndId = mtgAssigments.value("prayer_end").toInt();

            if(chairmanId == personId)
            {
                result.AddHomeAssignment(dt, person::LMM_Chairman);
            }

            if(prayerStartId == personId || prayerEndId == personId)
            {
                result.AddHomeAssignment(dt, person::Prayer);
            }
        }
    }
}


// UnknownTalkIException
//======================
UnknownTalkIException::UnknownTalkIException(int talkId)
    : std::invalid_argument(QObject::tr("Unknown talk ID = %1").arg(QString::number(talkId)).toLocal8Bit().constData())
{

}
