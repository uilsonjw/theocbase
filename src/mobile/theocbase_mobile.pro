TEMPLATE = app
QT += qml quick widgets sql network svg networkauth
TARGET = TheocBase
CONFIG += c++11
CONFIG -= bitcode

VERSION = 2021.06.0
DEFINES += APP_VERSION=\\\"$$VERSION\\\"

# C++ files
include(src/src.pri)

# Default rules for deployment.
include(deployment.pri)
# SMTP Client
include(../smtp/smtpclient.pri)

SOURCES +=
HEADERS +=

RESOURCES += \
    qml/qml.qrc \
    ../database.qrc \
    icons/icons.qrc \
    translations/translations_mobile.qrc \
    ../translations/translations.qrc \
    ../fonts/fonts.qrc

# var, prepend, append
defineReplace(prependAll) {
    for(a,$$1):result += $$2$${a}$$3
    return($$result)
}

lupdate_only {
    TR_EXCLUDE += $$PWD/../accesscontrol.*
    SOURCES = qml/*.qml \
        src/school_detail.cpp
}

# Supported languages
LANGUAGES = af bg cs da de en es et el fi fr gcf gn he hi hr ht hu hy it ka lt my ne nl no pl pt pt_BR ro ru sk sl sr sv th uk zh

# Available translations
TRANSLATIONS = $$prependAll(LANGUAGES, $$PWD/translations/theocbase_mobile_, .ts)

# Additional import path used to resolve QML modules in Qt Creator's code model
# QML_IMPORT_PATH =
