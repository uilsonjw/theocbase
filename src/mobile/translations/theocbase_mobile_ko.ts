<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ko">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>다음 연구에 임명하지 마십시오</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>현재 연구에 남아있기</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>잘못된 데이타</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>시간 더하기?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>주제</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>근거 자료</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>학생</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>보조</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>결과</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>완료</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>자원자</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>자원자 선택하기</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>현재 연구</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>연습 문제 완료</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>다음 연구</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>다음 연구 선택하기</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>시간</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>노트</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>학교 세부점</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>스탑워치 시작</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>스탑워치 멈춤</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>장면</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>장면 선택하기</translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>No exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit overseer&#x27;s visit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit assembly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Regional convention</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Memorial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Other exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>주제</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>근거 자료</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>사회자</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>연사</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>낭독자</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>노트</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <comment>Page title</comment>
        <translation>비고</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>성경에 담긴 보물</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>야외 봉사에 힘쓰십시오</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>그리스도인 생활</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>사회자</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>조언자</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>노래 %1 와 기도</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>노래</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>사회자</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>낭독자</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>소개말</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>기도</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>현재 연구에 남아있기</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>스탑워치 시작</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>스탑워치 멈춤</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>시간 더하기?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>주제</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>근거 자료</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>학생</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>결과</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>완료</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>시간</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>현재 연구</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>연습 문제 완료</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>다음 연구</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>다음 연구 선택하기</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>노트</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>보조자</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>자원자</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>스탑워치</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>보조자는 이성이어서는 안 된다.</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>세부점</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>당겨서 새로 고침</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>놓아서 새로 고침</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation>사용자 아이디</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>비밀번호</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>로그인</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>사용자 아이디 혹은 이메일</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>이메일</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>계정 만들기</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>비밀번호 재설정</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>이메일을 못찾겠습니다.</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>비밀번호 분실</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>로그인 페이지</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation>주 시작 %1</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>계획표</translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print</source>
        <translation>인쇄</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Text size</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>노래와 기도</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>노래 %1 와 기도</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>공개 강연</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>파수대 연구</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>노래 %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>사회자</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>낭독자</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Phone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>이름</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>성</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>형제</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>자매</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>종</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>가족</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>가족 관계</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>연락 정보</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>전화번호</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>전자메일</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>모든 학급</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>주된 학급만...</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>보조 학급만...</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>가장</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>보조</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>정보</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>활동적인</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>사회자</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>성경에 담긴 보물</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>성경 낭독</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>첫방문</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>재방문</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>성서 연구</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>그리스도인 생활 연설</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>회중 성서 연구</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>회중 성서 연구 낭독자</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>새 전도인</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>기도</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>전도인</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>셋팅</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>로그아웃</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>정보</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>버젼</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>TheocBase 홈페이지</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>제안</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>마지막 동기화: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>계획표</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>시간 표시</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>기간 표시</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>사용자 인터페이스</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>언어</translation>
    </message>
    <message>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Printing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>동기화 중...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Song</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>weeks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>똑같은 변화가 현재 사용기기 그리고 클라우드에서 발견되었습니다. (%1 rows). 현재 사용기기에서 변경을 유지하길 원합니까?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>다음 연구에 임명하지 마십시오</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>설정되지 않음</translation>
    </message>
</context></TS>