/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2016, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef REMINDERS_H
#define REMINDERS_H

#include <QDialog>
#include <QMessageBox>
#include <QProgressDialog>
#include <QHeaderView>
#include <QMouseEvent>
#include "schoolreminder.h"
#include "smtp/SmtpMime"
#include <QSettings>
#include "simplecrypt.h"
#include "constants.h"
#include "general.h"

namespace Ui {
class reminders;
class CheckBoxHeader;
}

class reminders : public QDialog
{
    Q_OBJECT

public:
    explicit reminders(QWidget *parent = nullptr);
    ~reminders();
    int remindersCount();

signals:
    void sent();
private slots:
    void on_buttonSend_clicked();
    void headerCheckBoxClicked(bool val, int column);
    void on_dateEditFrom_dateChanged(const QDate &date);
    void on_dateEditThru_dateChanged(const QDate &date);
    void on_tableWidget_doubleClicked(const QModelIndex &index);
    void on_tableWidget_cellChanged(int row, int column);

private:
    Ui::reminders *ui;
    QList<schoolreminder *> r;
    sql_class *sql;
    void updateList();
    bool updateList_;
    QSharedPointer<SmtpClient> smtp;
};

class CheckBoxHeader : public QHeaderView
{
    Q_OBJECT
public:
    CheckBoxHeader(Qt::Orientation orientation, const QHash<int, bool> &checkCols, QWidget *parent = nullptr)
        : QHeaderView(orientation, parent)
    {
        _checkedCols = checkCols;
    }
    bool isChecked(int column) const
    {
        return _checkedCols[column];
    }
    void setIsChecked(bool val, int column)
    {
        if (_checkedCols[column] != val) {
            _checkedCols[column] = val;
            redrawCheckBox();
        }
    }
signals:
    void checkBoxClicked(bool state, int column);

protected:
    void paintSection(QPainter *painter, const QRect &rect, int logicalIndex) const
    {
        //painter->save();
        QHeaderView::paintSection(painter, rect, logicalIndex);
        //painter->restore();
        if (_checkedCols.contains(logicalIndex)) {
            QStyleOptionButton option;
            option.rect = QRect(rect.x() + 1, 3, 20, 20);
            option.state = QStyle::State_Enabled | QStyle::State_Active;
            if (_checkedCols[logicalIndex])
                option.state |= QStyle::State_On;
            else
                option.state |= QStyle::State_Off;
            option.state |= QStyle::State_Off;
            this->style()->drawPrimitive(QStyle::PE_IndicatorCheckBox, &option, painter);
        }
    }
    void mousePressEvent(QMouseEvent *event)
    {
        int col = logicalIndexAt(event->pos());
        if (_checkedCols.contains(col)) {
            setIsChecked(!isChecked(col), col);
            emit checkBoxClicked(isChecked(col), col);
        } else {
            QHeaderView::mousePressEvent(event);
        }
    }

private:
    QHash<int, bool> _checkedCols;
    void redrawCheckBox()
    {
        viewport()->update();
    }
};

#endif // REMINDERS_H
