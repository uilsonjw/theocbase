import QtQuick 2.5
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Item {
    id: rowitem
    width: 400
    height: visible ? 40*dpi : 0
    Layout.fillWidth: true
    Layout.preferredHeight: visible ? 30*dpi : 0

    property alias timeText: textboxtext
    property alias timeboxColor: textbox.color
    property alias themeText: textTheme
    property alias nameText1: textSpeaker
    property alias nameText2: textAssistant
    property alias imageSource: image1.source
    property bool showTopLine: false
    property bool showBottomLine: true
    property bool clickable: false

    signal clicked()

    //property double dpi: 1.2

    GridLayout {
        id: gridLayout1
        columnSpacing: 5*dpi
        rowSpacing: 0
        rows: 2
        columns: 4
        anchors.fill: parent
        anchors.leftMargin: 5*dpi

        Rectangle {
            id: textbox
            color: "#841616"
            Layout.rowSpan: 2
            Layout.preferredHeight: 30*dpi
            Layout.preferredWidth: 30*dpi
            Text {
                id: textboxtext
                color: "white"
                text: "10"               
                anchors.fill: parent
                anchors.margins: 2*dpi
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.pointSize: app_info.fontsizeSmall
                minimumPointSize: 8
                fontSizeMode: Text.HorizontalFit
                wrapMode: Text.WordWrap
            }
        }

        Text {
            id: textTheme
            text: ""
            Layout.fillWidth: true
            Layout.rowSpan: 2
            font.pointSize: app_info.fontsizeSmall
            elide: Text.ElideRight
            Layout.preferredHeight: gridLayout1.height
            wrapMode: Text.WordWrap
            verticalAlignment: Text.AlignVCenter
        }

       ColumnLayout {
            Layout.rowSpan: 2
            Text {
                id: textSpeaker
                text:  ""
                verticalAlignment: Text.AlignVCenter
                Layout.fillHeight: true
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                font.pointSize: app_info.fontsizeSmall
                visible: text !== ""
            }
            Text {
                id: textAssistant
                text: ""
                verticalAlignment: Text.AlignVCenter
                Layout.fillHeight: true
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                font.pointSize: app_info.fontsizeSmall
                visible: text !== ""
            }
        }

        Image {
            id: image1
            Layout.rowSpan: 2
            Layout.preferredHeight: 20*dpi
            Layout.preferredWidth: 20*dpi
            source: ""
        }
    }
    Rectangle {
        anchors.fill: parent
        visible: clickable
        color: "grey"
        opacity: listRowMouseArea.pressed ? 0.1 : 0
        MouseArea {
            anchors.fill: parent
            id: listRowMouseArea
            onClicked: rowitem.clicked()
        }
    }    
    Rectangle { color: "lightgrey"; anchors.top: parent.top; width: parent.width; height: app_info.linewidth; visible: showTopLine }
    Rectangle { color: "lightgrey"; anchors.bottom: parent.bottom; width: parent.width; height: app_info.linewidth; visible: showBottomLine }
}
