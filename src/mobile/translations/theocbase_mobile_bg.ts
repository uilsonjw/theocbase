<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bg">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation>Име</translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation>Дата</translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Не назначавай следващия урок</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Да повтори урока</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Невалидни данни</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Искаш ли да добавиш време?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Тема</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Източник на материала</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Участник</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Помощник</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Резултат</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Изпълнено</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Доброволец</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Избери доброволец</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Възложен урок</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Упражненията са изпълнени</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Следващ урок</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Избери следващ урок</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Време</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Забележки</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Подробности за училището</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Пусни хронометъра</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Спри хронометъра</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Ситуация</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Избери ситуация</translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>No exception</source>
        <translation>Без изключение</translation>
    </message>
    <message>
        <source>Circuit overseer&#x27;s visit</source>
        <translation>Посещение на окръжния надзорник</translation>
    </message>
    <message>
        <source>Circuit assembly</source>
        <translation>Окръжен конгрес</translation>
    </message>
    <message>
        <source>Regional convention</source>
        <translation>Регионален конгрес</translation>
    </message>
    <message>
        <source>Memorial</source>
        <translation>Възпоменание</translation>
    </message>
    <message>
        <source>Other exception</source>
        <translation>Друго изключение</translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation>Седмицата от %1</translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation>Дни за събрания</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Делнично събрание</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Събрание в края на седмицата</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Тема</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Източник на материала</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Водещ</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Докладчик</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Четец</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Бележки</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Подробности</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <comment>Page title</comment>
        <translation>Забележки</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>СЪКРОВИЩА ОТ БОЖИЕТО СЛОВО</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>БЪДИ ПО–РЕЗУЛТАТЕН В СЛУЖБАТА</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>ХРИСТИЯНСКИ ЖИВОТ</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Председателстващ</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Съветник</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Песен %1 и молитва</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Песен</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Водещ</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Четец</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Встъпителни думи</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Молитва</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation>Импортирай програма...</translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation>ГЗ</translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation>ДЗ1</translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation>ДЗ2</translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation>Заключителни думи</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Делнично събрание</translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Да повтори урока</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Пусни хронометъра</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Спри хронометъра</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Искаш ли да добавиш време?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Тема</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Източник на материала</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Участник</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Резултат</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Изпълнено</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Време</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Възложен урок</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Упражненията са изпълнени</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Следващ урок</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Избери следващ урок</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Бележки</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Помощник</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Доброволец</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>Хронометър</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>Помощникът не бива да бъде от противоположния пол.</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>Урок</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Подробности</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>Издърпай за обновяване...</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Пусни за обновяване...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation>Потребителско име</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Парола</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Влез</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Потребителско име или имейл</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Имейл</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Регистрация</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>Смяна на парола</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>Имейл адресът не е намерен!</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Забравена парола</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>Страница за влизане</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation>Седмицата от %1</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation>Бележки</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation>ИЗЛИЗАЩИ ДОКЛАДЧИЦИ</translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>%1 излизащ докладчик този уикенд</numerusform>
            <numerusform>%1 излизащи докладчици този уикенд</numerusform>
        </translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation>Няма излизащи докладчици този уикенд</translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Програма</translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation>Работни листове</translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation>Програма за излизащи докладчици</translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Печат</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Делнично събрание</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Събрание в края на седмицата</translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation>Комбинация</translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation>Бланки със задачи</translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation>Шаблон</translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation>Бланки със задачи за помощниците</translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation>Отпечатай само назначените</translation>
    </message>
    <message>
        <source>Text size</source>
        <translation>Размер на текста</translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation>Индивидуални шаблони</translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Песен и молитва</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Песен %1 и молитва</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>ПУБЛИЧЕН ДОКЛАД</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>ИЗУЧАВАНЕ НА „СТРАЖЕВА КУЛА“</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Песен %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Водещ</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Четец</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation>Импортиране на СК...</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Събрание в края на седмицата</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Делнично събрание</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>Тема</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>Сбор</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Докладчик</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Моб. тел.</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Телефон</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Имейл</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Информация</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Домакин</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>Публичен доклад</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>Име</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Фамилия</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Брат</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Сестра</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Служител</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Семейство</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>Член на семейството, свързан с</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Информация за връзка</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Телефон</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Имейл</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>Всички класове</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Само основен клас</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Само допълнителнителните класове</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Глава на семейство</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Помощник</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Информация</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>активен</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Председателстващ</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>Съкровища от Божието Слово</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Четене на Библията</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Първо посещение</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Повторно посещение</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Библейско изучаване</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>Доклади от Християнски живот</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Изучаване на Библията в сбора</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Четец за Изуч. на Библията</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>Нов вестител</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Молитва</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation>Домакин за докладчици</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Моб. тел.</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Делнично събрание</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>Доклад</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Събрание в края на седмицата</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>Водещ на изучаването на „Стражева кула“</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation>Четец на изучаването на „Стражева кула“</translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation>Духовни бисери</translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation>Обсъждане с клип</translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Вестители</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation>Списък за избор</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Настройки</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Изход</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Информация</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Версия</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>TheocBase уебсайт</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Отзиви</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>Последно синхронизиране: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Програма</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Покажи час</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Покажи продължителност</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Потребителски интерфейс</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Език</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Влез</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Имейл</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Име</translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation>Показвай заглавията на песните</translation>
    </message>
    <message>
        <source>Printing</source>
        <translation>Отпечатване</translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation>Индивидуални шаблони</translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Синхронизиране...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>Председателстващ</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Песен</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation>Председателстващ на събранието в края на седмицата</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>Песен</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation>Песен за „Стражева кула“</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Молитва</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation>Брой на „Стражева кула“</translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>Статия</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Тема</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>Водещ</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>Четец</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>Линия на времето</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>Брой седмици преди избраната дата</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>Брой седмици след избраната дата</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>седмици</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation>Брой седмици след задачата, които да бъдат отбелязани със сиво</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>Същите промени се намират и локално, и в облака (%1 реда). Искаш ли да запазиш локалните промени?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>Данните в облака са изтрити. Твоите локални данни ще бъдат заменени. Да продължим ли?</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Не назначавай следващия урок</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>Не е посочено</translation>
    </message>
</context></TS>