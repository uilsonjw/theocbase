#include "accesscontrol.h"

Role::Role()
    : m_Id(Permission::RoleId::Publisher), m_parent(nullptr), m_Name("")
{
    m_Permissions = QList<Permission::Rule>();
}

Permission::RoleId Role::id() const
{
    return m_Id;
}

void Role::setId(Permission::RoleId id)
{
    m_Id = id;
}

int Role::parentId() const
{
    return m_ParentId;
}

void Role::setParentId(int parentId)
{
    m_ParentId = parentId;
}

Role *Role::parent() const
{
    return m_parent;
}

void Role::setParent(Role *parent)
{
    m_parent = parent;
}

QList<Permission::Rule> Role::permissions() const
{
    return m_Permissions;
}

void Role::setPermissions(const QList<Permission::Rule> &permissions)
{
    m_Permissions = permissions;
}

bool Role::hasPermission(Permission::Rule rule) const
{
    if (m_parent != nullptr && m_parent->hasPermission(rule))
        return true;

    foreach (Permission::Rule permission, m_Permissions) {
        if (permission == rule)
            return true;
    }
    return false;
}

void Role::write(QJsonObject &json) const
{
    json["id"] = static_cast<int>(m_Id);
    json["name"] = m_Name;
    QJsonArray permissionArray;
    foreach (const Permission::Rule rule, m_Permissions) {
        permissionArray.append(QJsonValue(static_cast<int>(rule)));
    }
    json["permissions"] = permissionArray;
}

User::User(QObject *parent)
    : QObject(parent)
{
    m_Roles = QList<const Role *>();
}

int User::id() const
{
    return m_Id;
}

void User::setId(int Id)
{
    m_Id = Id;
}

QString User::name() const
{
    return m_Name;
}

void User::setName(const QString &name)
{
    m_Name = name;
}

QString User::email() const
{
    return m_Email;
}

void User::setEmail(const QString &email)
{
    m_Email = email;
}

const QList<const Role *> &User::roles() const
{
    return m_Roles;
}

void User::setRoles(const QList<const Role *> &roles)
{
    m_Roles = roles;
}

void User::addRole(const Role *role)
{
    if (!m_Roles.contains(role))
        m_Roles.append(role);
}

void User::removeRole(const Role *role)
{
    m_Roles.removeOne(role);
}

bool User::hasRole(Role &role) const
{
    foreach (const Role *r, m_Roles) {
        if (r->id() == role.id())
            return true;
    }
    return false;
}

bool User::hasPermission(PermissionRule permissionRule)
{
    foreach (const Role *role, roles())
        if (role->hasPermission(permissionRule))
            return true;
    return false;
}

void User::write(QJsonObject &json) const
{
    json["id"] = m_Id;
    json["name"] = m_Name;
    json["email"] = m_Email;
    QJsonArray roleArray;
    foreach (const Role *role, m_Roles) {
        roleArray.append(static_cast<int>(role->id()));
    }
    json["roles"] = roleArray;
}

AccessControl::AccessControl(QObject *parent)
    : QObject(parent)
{
    m_Roles = QList<Role>();
    m_Users = QList<User *>();

    setDefaultRolePermissions();

    qRegisterMetaType<PermissionRule>("PermissionRule");
    qmlRegisterUncreatableType<Permission>("net.theocbase", 1, 0, "PermissionRule", "Not creatable as it is an enum type");
}

void AccessControl::setDefaultRolePermissions()
{
    if (m_Roles.count() > 0)
        m_Roles.clear();

    // Publisher
    Role publisherRole;
    publisherRole.setId(Permission::RoleId::Publisher);
    QList<Permission::Rule> publisherPermissions;
    publisherPermissions.append(Permission::Rule::CanPrintMidweekMeetingSchedule);
    publisherPermissions.append(Permission::Rule::CanPrintWeekendMeetingSchedule);
    publisherPermissions.append(Permission::Rule::CanViewMidweekMeetingSchedule);
    publisherPermissions.append(Permission::Rule::CanViewWeekendMeetingSchedule);
    publisherPermissions.append(Permission::Rule::CanViewTerritories);
    publisherPermissions.append(Permission::Rule::CanViewSpecialEvents);
    publisherPermissions.append(Permission::Rule::CanViewMeetingNotes);
    publisherRole.setPermissions(publisherPermissions);
    m_Roles.append(publisherRole);

    // Elder
    Role elderRole;
    elderRole.setId(Permission::RoleId::Elder);
    elderRole.setParent(findRole(publisherRole.id()));
    QList<Permission::Rule> elderPermissions;
    elderPermissions.append(Permission::Rule::CanViewPublicTalkList);
    elderPermissions.append(Permission::Rule::CanViewPublishers);
    elderPermissions.append(Permission::Rule::CanViewPrivileges);
    elderPermissions.append(Permission::Rule::CanViewCongregationSettings);
    elderPermissions.append(Permission::Rule::CanViewSongList);
    // TODO: Move permission to edit meeting notes to Coordinator of the Body of Elders
    // (and Life and Ministry Meeting Overseer or even Talk coordinator)
    // as soon as other schedules etc. are available
    elderPermissions.append(Permission::Rule::CanEditMeetingNotes);
    elderRole.setPermissions(elderPermissions);
    m_Roles.append(elderRole);

    // Life and Ministry Meeting Chairman
    Role lmmChairmanRole;
    lmmChairmanRole.setId(Permission::RoleId::LMMChairman);
    lmmChairmanRole.setParent(findRole(elderRole.id()));
    QList<Permission::Rule> lmmChairmanPermissions;
    lmmChairmanPermissions.append(Permission::Rule::CanEditMidweekMeetingSchedule);
    lmmChairmanPermissions.append(Permission::Rule::CanPrintMidweekMeetingWorksheets);
    lmmChairmanPermissions.append(Permission::Rule::CanViewMidweekMeetingSettings);
    lmmChairmanPermissions.append(Permission::Rule::CanSendMidweekMeetingReminders);
    lmmChairmanPermissions.append(Permission::Rule::CanViewStudentData);
    lmmChairmanPermissions.append(Permission::Rule::CanViewMidweekMeetingTalkHistory);
    lmmChairmanPermissions.append(Permission::Rule::CanViewAvailabilities);
    lmmChairmanPermissions.append(Permission::Rule::CanEditCongregationSettings);
    lmmChairmanRole.setPermissions(lmmChairmanPermissions);
    m_Roles.append(lmmChairmanRole);

    // Life and Ministry Meeting Overseer
    Role lmmOverseerRole;
    lmmOverseerRole.setId(Permission::RoleId::LMMOverseer);
    lmmOverseerRole.setParent(findRole(lmmChairmanRole.id()));
    QList<Permission::Rule> lmmOverseerPermissions;
    lmmOverseerPermissions.append(Permission::Rule::CanPrintMidweekMeetingAssignmentSlips);
    lmmOverseerPermissions.append(Permission::Rule::CanEditMidweekMeetingSettings);
    lmmOverseerPermissions.append(Permission::Rule::CanEditPublishers);
    lmmOverseerPermissions.append(Permission::Rule::CanEditStudentData);
    lmmOverseerPermissions.append(Permission::Rule::CanEditAvailabilities);
    lmmOverseerPermissions.append(Permission::Rule::CanEditCongregationSettings);
    lmmOverseerPermissions.append(Permission::Rule::CanEditSpecialEvents);
    lmmOverseerPermissions.append(Permission::Rule::CanEditSongList);
    lmmOverseerRole.setPermissions(lmmOverseerPermissions);
    m_Roles.append(lmmOverseerRole);

    // Talk Coordinator
    Role talkCoordinatorRole;
    talkCoordinatorRole.setId(Permission::RoleId::TalkCoordinator);
    talkCoordinatorRole.setParent(findRole(publisherRole.id()));
    QList<Permission::Rule> talkCoordinatorPermissions;
    talkCoordinatorPermissions.append(Permission::Rule::CanEditWeekendMeetingSchedule);
    talkCoordinatorPermissions.append(Permission::Rule::CanPrintWeekendMeetingWorksheets);
    talkCoordinatorPermissions.append(Permission::Rule::CanPrintSpeakersSchedule);
    talkCoordinatorPermissions.append(Permission::Rule::CanPrintSpeakersAssignments);
    talkCoordinatorPermissions.append(Permission::Rule::CanViewWeekendMeetingSettings);
    talkCoordinatorPermissions.append(Permission::Rule::CanEditWeekendMeetingSettings);
    talkCoordinatorPermissions.append(Permission::Rule::CanViewPublicTalkList);
    talkCoordinatorPermissions.append(Permission::Rule::CanPrintPublicTalkList);
    talkCoordinatorPermissions.append(Permission::Rule::CanEditPublicTalkList);
    talkCoordinatorPermissions.append(Permission::Rule::CanScheduleHospitality);
    talkCoordinatorPermissions.append(Permission::Rule::CanPrintHospitality);
    talkCoordinatorPermissions.append(Permission::Rule::CanViewPublicSpeakers);
    talkCoordinatorPermissions.append(Permission::Rule::CanEditPublicSpeakers);
    talkCoordinatorPermissions.append(Permission::Rule::CanViewCongregationSettings);
    talkCoordinatorPermissions.append(Permission::Rule::CanEditCongregationSettings);
    talkCoordinatorPermissions.append(Permission::Rule::CanEditSpecialEvents);
    talkCoordinatorPermissions.append(Permission::Rule::CanEditSongList);
    talkCoordinatorRole.setPermissions(talkCoordinatorPermissions);
    m_Roles.append(talkCoordinatorRole);

    // Territory Servant
    Role territoryServantRole;
    territoryServantRole.setId(Permission::RoleId::TerritoryServant);
    territoryServantRole.setParent(findRole(publisherRole.id()));
    QList<Permission::Rule> territoryServantPermissions;
    territoryServantPermissions.append(Permission::Rule::CanEditTerritories);
    territoryServantPermissions.append(Permission::Rule::CanPrintTerritoryRecord);
    territoryServantPermissions.append(Permission::Rule::CanPrintTerritoryMapCard);
    territoryServantPermissions.append(Permission::Rule::CanPrintTerritoryMapAndAddressSheets);
    territoryServantPermissions.append(Permission::Rule::CanViewTerritoryAssignments);
    territoryServantPermissions.append(Permission::Rule::CanViewTerritoryAddresses);
    territoryServantPermissions.append(Permission::Rule::CanViewTerritorySettings);
    territoryServantPermissions.append(Permission::Rule::CanEditTerritorySettings);
    territoryServantPermissions.append(Permission::Rule::CanViewPublishers);
    territoryServantPermissions.append(Permission::Rule::CanEditPublishers);
    territoryServantRole.setPermissions(territoryServantPermissions);
    m_Roles.append(territoryServantRole);

    // Secretary
    Role secretaryRole;
    secretaryRole.setId(Permission::RoleId::Secretary);
    secretaryRole.setParent(findRole(elderRole.id()));
    QList<Permission::Rule> secretaryPermissions;
    secretaryPermissions.append(Permission::Rule::CanEditPublishers);
    secretaryRole.setPermissions(secretaryPermissions);
    m_Roles.append(secretaryRole);

    // Service Overseer
    Role serviceOverseerRole;
    serviceOverseerRole.setId(Permission::RoleId::ServiceOverseer);
    serviceOverseerRole.setParent(findRole(elderRole.id()));
    QList<Permission::Rule> serviceOverseerPermissions;
    serviceOverseerPermissions.append(Permission::Rule::CanPrintTerritoryRecord);
    serviceOverseerPermissions.append(Permission::Rule::CanPrintTerritoryMapCard);
    serviceOverseerPermissions.append(Permission::Rule::CanPrintTerritoryMapAndAddressSheets);
    serviceOverseerPermissions.append(Permission::Rule::CanViewTerritoryAssignments);
    serviceOverseerPermissions.append(Permission::Rule::CanViewTerritoryAddresses);
    serviceOverseerPermissions.append(Permission::Rule::CanViewTerritorySettings);
    serviceOverseerRole.setPermissions(serviceOverseerPermissions);
    m_Roles.append(serviceOverseerRole);

    // Coordinator of the Body of Elders
    Role coordinatorBOERole;
    coordinatorBOERole.setId(Permission::RoleId::CoordinatorOfBOE);
    coordinatorBOERole.setParent(findRole(lmmChairmanRole.id()));
    QList<Permission::Rule> coordinatorBOEPermissions;
    coordinatorBOEPermissions.append(Permission::Rule::CanEditMidweekMeetingSettings);
    coordinatorBOEPermissions.append(Permission::Rule::CanPrintWeekendMeetingWorksheets);
    coordinatorBOEPermissions.append(Permission::Rule::CanPrintSpeakersSchedule);
    coordinatorBOEPermissions.append(Permission::Rule::CanPrintSpeakersAssignments);
    coordinatorBOEPermissions.append(Permission::Rule::CanViewWeekendMeetingSettings);
    coordinatorBOEPermissions.append(Permission::Rule::CanPrintPublicTalkList);
    coordinatorBOEPermissions.append(Permission::Rule::CanPrintHospitality);
    coordinatorBOEPermissions.append(Permission::Rule::CanEditPublishers);
    coordinatorBOEPermissions.append(Permission::Rule::CanViewPublicSpeakers);
    coordinatorBOEPermissions.append(Permission::Rule::CanEditPrivileges);
    coordinatorBOEPermissions.append(Permission::Rule::CanEditAvailabilities);
    coordinatorBOEPermissions.append(Permission::Rule::CanViewPermissions);
    coordinatorBOEPermissions.append(Permission::Rule::CanEditCongregationSettings);
    coordinatorBOEPermissions.append(Permission::Rule::CanEditSpecialEvents);
    coordinatorBOEPermissions.append(Permission::Rule::CanEditSongList);
    coordinatorBOERole.setPermissions(coordinatorBOEPermissions);
    m_Roles.append(coordinatorBOERole);

    // Administrator
    Role adminRole;
    adminRole.setId(Permission::RoleId::Administrator);
    adminRole.setParent(findRole(publisherRole.id()));
    QList<Permission::Rule> adminPermissions;
    adminPermissions.append(Permission::Rule::CanViewPermissions);
    adminPermissions.append(Permission::Rule::CanEditPermissions);
    adminPermissions.append(Permission::Rule::CanEditCongregationSettings);
    adminPermissions.append(Permission::Rule::CanDeleteCloudData);
    adminRole.setPermissions(adminPermissions);
    m_Roles.append(adminRole);
}

const QList<Role> &AccessControl::roles() const
{
    return m_Roles;
}

QList<User *> &AccessControl::users()
{
    return m_Users;
}

void AccessControl::setUsers(const QList<User *> &users)
{
    m_Users = users;
}

User *AccessControl::findUser(int id)
{
    for (int i = 0; i < m_Users.size(); ++i) {
        if (m_Users.at(i)->id() == id)
            return m_Users[i];
    }
    return nullptr;
}

User *AccessControl::findUser(QString email)
{
    for (int i = 0; i < m_Users.size(); ++i) {
        if (!m_Users.at(i)->email().compare(email, Qt::CaseInsensitive))
            return m_Users[i];
    }
    return nullptr;
}

bool AccessControl::addUser(QString name, QString email)
{
    try {
        User *newUser = new User();
        int id = 0;
        for (int i = 0; i < m_Users.size(); ++i) {
            id = qMax(id, m_Users.at(i)->id());
            if (!m_Users.at(i)->email().compare(email, Qt::CaseInsensitive))
                return false;
        }
        id++;
        newUser->setId(id);
        newUser->setName(name);
        newUser->setEmail(email);

        Role *publisherRole = findRole(Permission::RoleId::Publisher);
        newUser->addRole(publisherRole);

        m_Users.append(newUser);
    } catch (...) {
        return false;
    }
    return true;
}

void AccessControl::removeUser(int id)
{
    for (int i = 0; i < m_Users.size(); ++i) {
        if (m_Users.at(i)->id() == id)
            m_Users.removeAt(i);
    }
}

void AccessControl::load(const QJsonObject &json, bool &valid)
{
    // save current user
    User *currentUser = user();
    QString currentUserEmail;
    if (currentUser)
        currentUserEmail = currentUser->email();

    // check the file
    if (json.contains("checksum")) {
        QByteArray checksum = json["checksum"].toString().toLatin1();
        QJsonObject tempJson(json);
        tempJson.remove("checksum");
        valid = (QCryptographicHash::hash(QJsonDocument(tempJson).toJson(),
                                          QCryptographicHash::Sha3_512)
                         .toHex()
                         .constData()
                 == checksum);
    } else {
        valid = false;
    }
    qDebug() << QString("Json file is %1").arg(valid ? "valid" : "not valid");
    if (!valid)
        return;

    QList<User *> users;
    QJsonArray usersArray = json["users"].toArray();
    for (QJsonValue v : usersArray) {
        User *user = new User();
        QJsonObject metadata = v.toObject();
        int id = metadata.value("id").toInt();
        QString name = metadata.value("name").toString();
        QString email = metadata.value("email").toString();
        user->setId(id);
        user->setName(name);
        user->setEmail(email);

        QJsonArray rolesArray = metadata.value("roles").toArray();
        for (QJsonValue r : rolesArray) {
            Role *ur = findRole(static_cast<Permission::RoleId>(r.toInt()));
            if (ur != nullptr)
                user->addRole(ur);
        }
        Role *publisherRole = findRole(Permission::RoleId::Publisher);
        user->addRole(publisherRole);
        users.append(user);
    }
    setUsers(users);

    // restore current user
    if (!currentUserEmail.isEmpty()) {
        currentUser = findUser(currentUserEmail);
        if (currentUser)
            setUser(currentUser);
    }
}

void AccessControl::write(QJsonObject &json)
{
    QJsonArray userArray;
    foreach (User *user, users()) {
        QJsonObject userObject;
        user->write(userObject);
        userArray.append(userObject);
    }
    json["users"] = userArray;
    // checksum
    json["checksum"] = QCryptographicHash::hash(QJsonDocument(json).toJson(),
                                                QCryptographicHash::Sha3_512)
                               .toHex()
                               .constData();
}

Role *AccessControl::findRole(Permission::RoleId id)
{
    for (int i = 0; i < m_Roles.size(); ++i) {
        if (m_Roles.at(i).id() == id)
            return &m_Roles[i];
    }
    return nullptr;
}

User *AccessControl::user() const
{
    return m_User;
}

void AccessControl::setUser(User *user)
{
    m_User = user;
    emit userChanged();
}
