/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQuickView>
#include <QQmlContext>
#include <QQuickItem>
#include <QObject>
#include <QString>
#include <QDebug>
#include <QObject>
#include <QStandardPaths>
#include <QQmlPropertyMap>
#include <QScreen>
#include "../../sql_class.h"
#include "publishers_model.h"
#include "../../cloud/cloud_controller.h"
#include "messagebox.h"
#include "qmltranslator.h"
#include <QScreen>
#include <QtMath>
#include "../../lmm_meeting.h"
#include "../../lmm_schedule.h"
#include "../../lmm_assignment.h"
#include "../../lmm_assignmentcontoller.h"
#include "../../publicmeeting_controller.h"
#include "../../csync.h"
#include "../../importlmmworkbook.h"
#include "../../wtimport.h"
#include "../../outgoingspeakersmodel.h"
#include "../../weekinfo.h"
#include "printcontroller.h"

#if defined(Q_OS_IOS)
#include "iosutil.h"
#endif

#if defined(Q_OS_ANDROID)
#include <QtAndroid>
#endif

#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
#include "shareutils.h"
#endif

bool transactionStarted;

void initDatabase()
{
    // database from resource file
    QString databasepath = ":/database/theocbase.sqlite";
    QString localdatabasedir = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    QString localdatabasepath = localdatabasedir + "/theocbase.sqlite";

    QDir dir(localdatabasedir);
    if(!dir.exists()){
        if (!dir.mkpath(localdatabasedir)) {
            qDebug() << "directory not created!";
            QMessageBox::warning(nullptr,"TheocBase", "Directory not found:\n" + localdatabasedir);
            return;
        }
        QFile oldfile(QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation) + "/theocbase.sqlite");
        if (oldfile.exists())
            oldfile.copy(localdatabasepath);
    }

    if(!QFile::exists(localdatabasepath)){
        // copy database to user's local
        QFile::copy(databasepath,localdatabasepath);
        QFile::setPermissions(localdatabasepath,QFile::ReadOwner | QFile::WriteOwner |
                              QFile::ReadUser | QFile::WriteUser | QFile::ReadGroup |
                              QFile::WriteOwner | QFile::ReadOther | QFile::WriteOther);
        qDebug() << "New database copied to " + localdatabasedir;
        //QMessageBox::information(0,"",QObject::tr("Database copied to ") + localdatabasedir);
    }else{
        qDebug() << "Use existing database!";
    }

    sql_class *sql = &Singleton<sql_class>::Instance();
    sql->databasepath=localdatabasepath;
    sql->createConnection();
    sql->updateDatabase(APP_VERSION);
}

int main(int argc, char *argv[])
{
    qputenv("QT_QUICK_CONTROLS_STYLE", "material");
    qputenv("QT_QUICK_CONTROLS_CONF", ":/style/qtquickcontrols2.conf");
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    app.setApplicationName("TheocBase");
    app.setOrganizationDomain("mobile.theocbase.net");

    app.setAttribute(Qt::AA_UseHighDpiPixmaps);


    qDebug() << app.applicationDirPath();

    initDatabase();

    qmlRegisterType<publishers_modelview>("net.theocbase.mobile", 1, 0, "Publishers");
    qmlRegisterType<person>("net.theocbase.mobile",1,0,"Publisher");
    qRegisterMetaType<person::UseFor>("UseFor");
    qmlRegisterSingletonType<cpersons>("net.theocbase.mobile", 1, 0, "CPersons", [](QQmlEngine *engine, QJSEngine *scriptEngine) -> QObject * {
        Q_UNUSED(engine);
        Q_UNUSED(scriptEngine);
        return new cpersons();
    });
    qmlRegisterType<family>("net.theocbase.mobile",1,0,"Family");
    qmlRegisterType<MessageBox>("net.theocbase.mobile",1,0,"MsgBox");
    qmlRegisterType<cloud_controller>("net.theocbase.mobile",1,0,"Cloud");
    qmlRegisterType<dropbox>("net.theocbase.mobile",1,0,"Dropbox");
    qmlRegisterType<LMM_Schedule>("net.theocbase.mobile",1,0,"LMM_Schedule");
    qmlRegisterType<LMM_Meeting>("net.theocbase.mobile",1,0,"LMM_Meeting");
    qmlRegisterType<LMM_Assignment>("net.theocbase.mobile",1,0,"LMM_Assignment");
    qmlRegisterType<LMM_AssignmentContoller>("net.theocbase.mobile",1,0, "AssignmentController");
    qmlRegisterType<SortFilterProxyModel>("net.theocbase.mobile",1,0,"SortFilterProxyModel");
    qmlRegisterType<ccongregation>("net.theocbase.mobile",1,0,"CongregationCtrl");
    qmlRegisterType<publicmeeting_controller>("net.theocbase.mobile", 1,0,"PublicMeetingController");
    qmlRegisterType<cptmeeting>("net.theocbase.mobile",1,0,"CPTMeeting");
    qmlRegisterType<csync>("net.theocbase.mobile",1,0,"CSync");
    qmlRegisterType<OutgoingSpeakersModel>("net.theocbase.mobile",1,0,"OutgoingSpeakersModel");
    qmlRegisterType<importlmmworkbook>("net.theocbase.mobile",1,0,"MWBImport");
    qmlRegisterType<wtimport>("net.theocbase.mobile",1,0,"WTImport");
    qmlRegisterType<AccessControl>("net.theocbase.mobile", 1, 0, "AccessControl");
    qmlRegisterType<WeekInfo>("net.theocbase.mobile", 1, 0, "WeekInfo");
    qmlRegisterType<PrintController>("net.theocbase.mobile",1, 0, "PrintController");
    qmlRegisterUncreatableType<TemplateData>("net.theocbase.mobile", 1, 0, "TemplateData", "Not creatable as it is an enum type.");

#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
    qmlRegisterType<ShareUtils>("net.theocbase.mobile",1,0,"ShareUtils");
#endif
    // qmlRegisterSingletonType() works without instance...

    ShareUtils sUtils;

    QQmlApplicationEngine engine;
    QQmlContext *ctxt = engine.rootContext();
    ctxt->setContextProperty("shareUtils", &sUtils);
    QmlTranslator qmlTranslator;
    qmlTranslator.initTranslator();
    ctxt->setContextProperty("qmlTranslator", &qmlTranslator);    

    // Access control
    AccessControl *ac = &Singleton<AccessControl>::Instance();
    ctxt->setContextProperty("accessControl", ac);

    QQmlPropertyMap appInfo;
    ctxt->setContextProperty("app_info",&appInfo);
    appInfo.insert("version",QString(APP_VERSION));
    // font sizes
    QFont defaultfont;
    appInfo.insert("fontpixels",defaultfont.pixelSize());
    appInfo.insert("fontsizepoint", defaultfont.pointSize());
    appInfo.insert("fontsize",defaultfont.pointSize() <= 12 ? 16 : defaultfont.pointSize());
    appInfo.insert("fontsizeSmall", qRound(appInfo.value("fontsize").toInt() / 1.2));
    appInfo.insert("devicepixelratio",qApp->primaryScreen()->devicePixelRatio());
    appInfo.insert("linewidth", qApp->primaryScreen()->devicePixelRatio() >= 2 ? 0.5 : 1);
    appInfo.insert("uilanguages",QLocale::system().uiLanguages().join(","));

    // calculte screen size
    qreal w = qApp->primaryScreen()->physicalSize().width();
    qreal h = qApp->primaryScreen()->physicalSize().height();
    qreal inches = qSqrt(w * w + h * h) / 25.4;
    appInfo.insert("screensize",QString::number(inches, 'f', 2).toDouble());
    qDebug() << "Screen size: " << appInfo.value("screensize");

#if defined(Q_OS_IOS)    
    iosutil ios_util;
    ctxt->setContextProperty("IOSUtil",&ios_util);
    // ios fix
    iosutil::initiOS();
    appInfo.insert("devicename", iosutil::getDeviceName());
#endif

    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));        
#ifdef Q_OS_ANDROID
    QtAndroid::hideSplashScreen();    
#endif

    return app.exec();
}
