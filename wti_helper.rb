def before_push()
  puts "Prepare before wti push; #{RUBY_PLATFORM}"
  case RUBY_PLATFORM
  when "x86_64-linux-gnu"
    system("/opt/Qt/5.15.2/gcc_64/bin/lupdate -no-obsolete src/theocbase.pro")
    system("/opt/Qt/5.15.2/gcc_64/bin/lupdate -no-obsolete src/mobile/theocbase_mobile.pro")
    system("xsltproc -o docs/theocbase_en.xml docs/theocbase_qhp2xml.xsl docs/theocbase_en.qhp")
    system("xml2po -o docs/theocbase_en.po docs/theocbase_en.xml")
  when "mac"
    # adjust and add mac commands
  when "windows"
    # adjust and add windows commands
  end
end

def after_push()
  puts "Remove obsolete files after wti push; #{RUBY_PLATFORM}"
  case RUBY_PLATFORM
  when "x86_64-linux-gnu"
    system("rm docs/*.po")
    system("rm docs/*.xml")
  when "mac"
    # adjust and add mac commands
  when "windows"
    # adjust and add windows commands
  end
end

STATUS_FORMAT = /(\w{2,3}|\w{2,3}-\w{2,5}+): (\d{1,3})% translated, (\d{1,3})% completed./
def after_pull()
  puts "Compile help file after wti pull; #{RUBY_PLATFORM}"
  output = `wti status`
  output.each_line do |line|
    m = line.match(STATUS_FORMAT)
    if m != nil
      language = m[1]
      translated = m[2].to_i
      completed = m[3].to_i
      if translated >= 90 && language != 'en'
        puts "#{language} >= #{translated}% translated. Translate documentation."
        case RUBY_PLATFORM
        when "x86_64-linux-gnu"
          system("xsltproc -o docs/theocbase_en.xml docs/theocbase_qhp2xml.xsl docs/theocbase_en.qhp")
          system("xml2po -p docs/theocbase_#{language}.po -o docs/theocbase_#{language}.xml docs/theocbase_en.xml")
          system("xsltproc -o docs/theocbase_#{language}.qhp --stringparam lang #{language} docs/theocbase_xml2qhp.xsl docs/theocbase_#{language}.xml")
          File.write(o = "docs/theocbase_#{language}.qhcp", File.read(i = "docs/theocbase_en.qhcp").gsub(/theocbase_en/,"theocbase_#{language}"))
          system("/opt/Qt/5.15.2/gcc_64/bin/qhelpgenerator docs/theocbase_#{language}.qhcp -o docs/theocbase_#{language}.qhc")
          system("rm -fr docs/#{language}")
          system("rm -fr docs/theocbase_#{language}.qhcp")
          system("rm -fr docs/theocbase_#{language}.qhp")
        when "mac"
          # adjust and add mac commands
        when "windows"
          # adjust and add windows commands
        end
      elsif language != 'en'
        puts "#{language} #{translated}% translated. Cannot translate documentation (< 90%)."
        case RUBY_PLATFORM
        when "x86_64-linux-gnu"
          system("rm -fr docs/#{language}")
        when "mac"
          # adjust and add mac commands
        when "windows"
          # adjust and add windows commands
        end
      end
    end
  end
  case RUBY_PLATFORM
  when "x86_64-linux-gnu"
    system("rm docs/*.po")
    system("rm docs/*.xml")
    system("/opt/Qt/5.15.2/gcc_64/bin/lrelease src/theocbase.pro")
    system("/opt/Qt/5.15.2/gcc_64/bin/lrelease src/mobile/theocbase_mobile.pro")
  when "mac"
    # adjust and add mac commands
  when "windows"
    # adjust and add windows commands
  end
end


case ARGV[0]
when "1"
  before_push()
when "2"
  after_push()
when "3"
  after_pull()
end
