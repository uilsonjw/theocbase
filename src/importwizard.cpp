/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "importwizard.h"
#include "ui_importwizard.h"

importwizard::importwizard(QWidget *parent) :
    QWizard(parent),
    ui(new Ui::importwizard)
{
    ui->setupUi(this);
#ifdef Q_OS_MAC
    this->setWizardStyle(QWizard::MacStyle);
#endif
    sql = &Singleton<sql_class>::Instance();

    // modal dialog
    this->setModal(true);
    this->setWindowModality(Qt::WindowModal);
    this->setOptions(options() & ~QWizard::NoCancelButton);
    // add button texts
    this->setButtonText(QWizard::FinishButton,tr("Save to database"));
    this->setButtonText(QWizard::BackButton,tr("< Back"));
    this->setButtonText(QWizard::NextButton,tr("Next >"));
    this->setButtonText(QWizard::CancelButton,tr("Cancel"));
}

importwizard::~importwizard()
{
    delete ui;
}

void importwizard::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void importwizard::setType(int type)
{
    m_importtype = type;
    switch (type)
    {
    case 0:
        ui->label->setText(tr("Theocratic school schedule import. Copy full schedule from WTLibrary and paste below (Ctrl + V / cmd + V)"));
        ui->label_2->setText(tr("Check schedule"));
        break;
    case 1:
        ui->label->setText(tr("Studies import. Copy studies from WTLibrary and paste below (Ctrl + V / cmd + V)"));
        ui->label_2->setText(tr("Check studies"));
        break;
    case 2:
        ui->label->setText(tr("Settings import. Copy settings from WTLibrary and paste below (Ctrl + V / cmd + V)"));
        ui->label_2->setText(tr("Check settings"));
        break;
    case 3:
        ui->label->setText(tr("Add public talk's subjects. Copy themes and paste below (Ctrl + V / cmd + V).\nNumber should be in the first column and theme in the second."));
        ui->label_2->setText(tr("Check subjects"));
        break;
    case 4:
        ui->label->setText(tr("Add speakers and congregations. Copy all data to clipboard and paste below (Ctrl + V)"));
        ui->label_2->setText(tr("Check data"));
        break;
    case 5:
        ui->label->setText(tr("Add songs. Copy all data to clipboard and paste below (Ctrl + V / cmd + V).\nNumber should be in the first column and theme in the second."));
        ui->label_2->setText(tr("Check songs"));
        break;
    }
}

bool importwizard::checkValues()
{
    bool ok;

    // check if text area is empty
    if(ui->plainTextEdit->document()->toPlainText() == ""){
        QMessageBox::warning(this,"Theocbase",
                             tr("No schedule to import."));
        return false;
    }
    QStringList items;
    QString item;
    QList<QPair<int, QString> > l = sql->getLanguages();
    int defaultLangId = sql->getLanguageDefaultId();
    int setLangId = 0;

    // default date to school import
    QString defaultdate = "";

    if(m_importtype == 0){
        // Show the date query if school schedule
        QLocale loc;
        QStringList list = ui->plainTextEdit->document()->toPlainText().split('\n');

        qDebug() << list.at(0).toUpper();
        qDebug() << "------";
        QString sdate;        

        // Try to parse default date
        QRegularExpression rx("(\\d+)");
        if (list.at(0).indexOf(rx) > -1) {
            QRegularExpressionMatch match = rx.match(list.at(0));
            sdate = match.capturedTexts().first();
        }

        if(sdate.length() < 2) sdate = "0" + sdate;
        for(int i=1;i<13;i++){
            qDebug() << "locale short name = " +  loc.standaloneMonthName(i,QLocale::ShortFormat).toUpper();
            if(list.at(0).toUpper().contains(loc.standaloneMonthName(i,QLocale::ShortFormat).left(3),Qt::CaseInsensitive)){
                QString monthstring = QString::number(i);
                if(monthstring.length() < 2) monthstring = "0" + monthstring;
                defaultdate = "2015-" + monthstring + "-" + sdate;
                break;
            }
        }
        if(defaultdate == "" && loc.language() == QLocale::Russian) defaultdate = "2013-05-" + sdate; // ?? not sure about this ??
        if(defaultdate == "") defaultdate = "2015-01-05";
        QDate date;
        do{
            m_schoolday = QInputDialog::getText(this, tr("Date"),
                                              tr("Please add start date YYYY-MM-DD (eg. 2011-01-03)"), QLineEdit::Normal,
                                              defaultdate, &ok);
            if(!ok) return false;
            date = QDate::fromString(m_schoolday,Qt::ISODate);
            // The date must be first day of week (Monday) because data will be saved according to first day of week
            if (date.dayOfWeek() != 1){
                QMessageBox::warning(this,"",m_schoolday + "\n" +
                                     tr("The date is not first day of week (Monday)"));
            }
        } while (date.dayOfWeek() != 1);
    }

    switch (m_importtype)
    {
    case 0:
        // theocratic ministry school
        fillSchoolSchedule();
        break;
    case 1:
        // studies
        fillSchoolStudies();
        break;
    case 2:
        // school settings
        fillSchoolSettings();
        break;
    case 3:
        // public talk themes
        // language is asked

        // get default language
        for(int i=0; i<l.count();i++){
            items << l.at(i).second;
            if (l.at(i).first == defaultLangId) setLangId = i;
        }

        item = QInputDialog::getItem(this, tr("Import subjects"),
                                     tr("Choose language"), items, setLangId, false, &ok);
        if (ok && !item.isEmpty()){
            qDebug() << "selected language = " << l.at( items.indexOf(item) ).first << "index = " << item.indexOf(item);
            m_selectedLanguage = l.at( items.indexOf(item) ).first;
            fillPublicTalkThemes();
        }else{
            return false;
        }
        break;
        // speakers and congregations
    case 4:
        fillPublicTalkSpeakers();
        break;
    case 5:
        // songs
        // language is asked

        // get default language
        for(int i=0; i<l.count();i++){
            items << l.at(i).second;
            if (l.at(i).first == defaultLangId) setLangId = i;
        }

        item = QInputDialog::getItem(this, tr("Import songs"),
                                     tr("Choose language"), items, setLangId, false, &ok);
        if (ok && !item.isEmpty()){
            qDebug() << "selected language = " << l.at( items.indexOf(item) ).first << "index = " << item.indexOf(item);
            m_selectedLanguage = l.at( items.indexOf(item) ).first;
            fillSongs();
        }else{
            return false;
        }
        break;
    }

    return true;
}

void importwizard::fillSchoolSchedule()
{   
    // Fill theocratic ministry school program
    // Schedule is copied from Watchtower Library or WOL (Online library)
    // Every theme should be in a one row.
    QProgressDialog progress( this );
    progress.setCancelButton(0);
    progress.setWindowModality(Qt::WindowModal);
    progress.show();

    QString alltext = ui->plainTextEdit->document()->toPlainText();
    QStringList list = alltext.split('\n');

    ui->tableWidget->setRowCount(0);
    ui->tableWidget->setColumnCount(5);
    QTableWidgetItem *header1 = new QTableWidgetItem;
    header1->setText(tr("date"));
    ui->tableWidget->setHorizontalHeaderItem(0,header1);
    QTableWidgetItem *header2 = new QTableWidgetItem;
    header2->setText(tr("number"));
    ui->tableWidget->setHorizontalHeaderItem(1,header2);
    QTableWidgetItem *header3 = new QTableWidgetItem;
    header3->setText(tr("subject"));
    ui->tableWidget->setHorizontalHeaderItem(2,header3);
    QTableWidgetItem *header4 = new QTableWidgetItem;
    header4->setText(tr("material"));
    ui->tableWidget->setHorizontalHeaderItem(3,header4);
    ui->tableWidget->setHorizontalHeaderItem(4,new QTableWidgetItem(tr("Only brothers")));

    qDebug() << QString::number(list.count()) + " rows";
    int rownumber = 0;

    QDate date = QDate::fromString(m_schoolday, Qt::ISODate);
    qDebug() << "startdate::" << date;

    // remove empty rows
    for (int i = list.size(); i-- > 0; ){
        if(list.at(i).trimmed() == "") list.removeAt(i);
    }

    progress.setValue(0);
    progress.setMaximum(list.size());

    for (int i = 0; i < list.size(); ++i)
    {
        progress.setValue(i);
        QString line = list.at(i);

        // remove empty parts
        line = line.trimmed();
        // remove boxes case when import from PDF
        line = line.remove(QByteArray::fromHex("f48fb080"));

        qDebug() << "row read: " + line;

        if (i > 0 && !line.contains(":")){
            // TMS review
            rownumber = 10;
        }

        if(rownumber < 4){
            line = line.mid(line.indexOf(":") + 2);
            line = line.simplified();
        }

        if(line.length() > 1){

            // Number should be:
            // 0 = Bible highlights
            // 1 = Number 1
            // 2 = Number 2
            // 3 = Number 3
            // 10 = TMS Review
            if (rownumber > 3 && rownumber != 10){
                // rownumber is not valid -> continue to next row
                continue;
            }

            QString source;
            if(line.indexOf("(")>0){
                // brakets found - this row contains a source material
                // remove brackets from source text
                source = line.mid(line.lastIndexOf("("),line.lastIndexOf(")"));
                line = line.left(line.lastIndexOf("(")-1);
            }

            int rowcount = ui->tableWidget->rowCount();
            rowcount += 1;
            ui->tableWidget->setRowCount(rowcount);
            qDebug() << QString::number(ui->tableWidget->rowCount());

            QTableWidgetItem *dateItem = new QTableWidgetItem;
            dateItem->setText(date.toString(Qt::ISODate));
            ui->tableWidget->setItem(rowcount-1,0,dateItem);
            QTableWidgetItem *number = new QTableWidgetItem;
            number->setText(QString::number(rownumber));
            ui->tableWidget->setItem(rowcount-1,1,number);
            QTableWidgetItem *subject = new QTableWidgetItem;
            subject->setText(line);
            ui->tableWidget->setItem(rowcount-1,2,subject);
            QTableWidgetItem *sourceitem = new QTableWidgetItem;
            sourceitem->setText(source);
            ui->tableWidget->setItem(rowcount-1,3,sourceitem);
            if (rownumber == 3){
                QTableWidgetItem* chkBoxItem = new QTableWidgetItem;
                // assignment is for brother when theme starts with asterisk and year < 2015
                chkBoxItem->setCheckState( (line.startsWith("*") && date.year() < 2015 ) ? Qt::Checked : Qt::Unchecked );
                ui->tableWidget->setItem(rowcount-1,4,chkBoxItem);
            }else{
                ui->tableWidget->setItem(rowcount-1,4,new QTableWidgetItem(""));
            }
            // the next row will be bible highlights when current row is #3 or TMS review
            if (rownumber == 3 || rownumber == 10){
                rownumber = 0;
                date = date.addDays(7);
            }else{
                rownumber++;
            }
        }
    }

    progress.close();
 }

void importwizard::fillSchoolSettings()
{
    // school settings
    QString alltext = ui->plainTextEdit->document()->toPlainText();
    QStringList list = alltext.split('\n');

    ui->tableWidget->setRowCount(0);
    ui->tableWidget->setColumnCount(2);
    QTableWidgetItem *header1 = new QTableWidgetItem;
    header1->setText("id");
    ui->tableWidget->setHorizontalHeaderItem(0,header1);
    QTableWidgetItem *header2 = new QTableWidgetItem;
    header2->setText(tr("setting"));
    ui->tableWidget->setHorizontalHeaderItem(1,header2);

    QString line = "";
    int settingnumber = 0;
    for (int i = 0; i < list.size(); ++i)
    {
        QString readline = list.at(i);

        // Get setting number
        QRegularExpression rx("(\\d+)");
        if (readline.indexOf(rx) > -1){
            QRegularExpressionMatch match = rx.match(readline);
            settingnumber = match.capturedTexts().first().toInt();
        }

        int test = readline.indexOf(".");
        readline = readline.mid(test +2);

        // Check if the name continues on the next row
        QString nextline;
        if(i < (list.count()-1)){
            nextline = list.at(i+1);
        }

        readline = readline.trimmed();
        if(line != "") line += " ";

        if(nextline.indexOf(".") > 0  || i == list.count()-1){
            line += readline;

            if(line != ""){
                int rowcount = ui->tableWidget->rowCount();
                rowcount += 1;
                ui->tableWidget->setRowCount(rowcount);
                QTableWidgetItem *id = new QTableWidgetItem;
                id->setText(QString::number(settingnumber));
                ui->tableWidget->setItem(rowcount-1,0,id);
                QTableWidgetItem *name = new QTableWidgetItem;
                name->setText(line);
                ui->tableWidget->setItem(rowcount-1,1,name);
                line = "";
            }
        }else{
            line += readline;
        }

    }
}

void importwizard::fillSchoolStudies()
{
    // studies
    QString alltext = ui->plainTextEdit->document()->toPlainText();
    QStringList list = alltext.split('\n');

    ui->tableWidget->setRowCount(0);
    ui->tableWidget->setColumnCount(2);
    QTableWidgetItem *header1 = new QTableWidgetItem;
    header1->setText("id");
    ui->tableWidget->setHorizontalHeaderItem(0,header1);
    QTableWidgetItem *header2 = new QTableWidgetItem;
    header2->setText(tr("study"));
    ui->tableWidget->setHorizontalHeaderItem(1,header2);

    QString line = "";
    qDebug() << QString::number(list.size());
    for (int i = 0; i < list.size(); ++i)
    {
        QString readline = list.at(i);
        int test = readline.indexOf("  ");
        readline = readline.mid(0,test);

        // remove empty tokens
        readline = readline.trimmed();

        // checking if to continue in the next line
        QString nextline;
        if(i < (list.count()-1)){
            nextline = list.at(i+1);
            nextline = nextline.trimmed();

            QString teststr = nextline.mid(0,nextline.indexOf(" "));
            QChar firstchar = teststr.left(1).data()->toLatin1();
            if(!firstchar.isNumber()){
                qDebug() << teststr + "is not numeric " + QString::number(i);
                line += readline;
                continue;
            }
        }

        if(line != "") line += " ";
        line += readline;

        if(line != ""){
            qDebug() << line + "-1";
            int rowcount = ui->tableWidget->rowCount();
            rowcount += 1;
            ui->tableWidget->setRowCount(rowcount);
            QTableWidgetItem *id = new QTableWidgetItem;
            id->setText(QString::number(rowcount));
            ui->tableWidget->setItem(rowcount-1,0,id);
            QTableWidgetItem *name = new QTableWidgetItem;
            name->setText(line);
            ui->tableWidget->setItem(rowcount-1,1,name);
            line = "";
        }

    }
}
//esitelmat
void importwizard::fillPublicTalkThemes()
{
    QString alltext = ui->plainTextEdit->document()->toPlainText();
    QStringList list = alltext.split('\n');

    ui->tableWidget->setRowCount(0);
    ui->tableWidget->setColumnCount(2);
    QTableWidgetItem *header1 = new QTableWidgetItem;
    header1->setText("id");
    ui->tableWidget->setHorizontalHeaderItem(0,header1);
    QTableWidgetItem *header2 = new QTableWidgetItem;
    header2->setText(tr("Subject"));
    ui->tableWidget->setHorizontalHeaderItem(1,header2);

    QRegularExpression rxTalk("(\\d+)[.]*\\s*(.+)");
    QString line = "";
    for (int i = 0; i < list.size(); ++i)
    {
        QString readline = list.at(i);

        QRegularExpressionMatch m = rxTalk.match(readline);
        if (!m.hasMatch())
            continue;

        QString number = m.captured(1);
        number = number.trimmed();
        QChar cn = number.data()->toLatin1();
        if(!cn.isNumber()) continue;

        QString teema = m.captured(2);
        teema = teema.trimmed();

        int rowcount = ui->tableWidget->rowCount();
        rowcount += 1;
        ui->tableWidget->setRowCount(rowcount);
        QTableWidgetItem *id = new QTableWidgetItem;
        id->setText(number);
        ui->tableWidget->setItem(rowcount-1,0,id);
        QTableWidgetItem *name = new QTableWidgetItem;
        name->setText(teema);
        ui->tableWidget->setItem(rowcount-1,1,name);
        line = "";
    }
}

void importwizard::fillPublicTalkSpeakers(){

    bool firstnamefirst = true;
    QString alltext = ui->plainTextEdit->document()->toPlainText();
    QStringList list = alltext.split('\n');

    ui->tableWidget->clear();
    ui->tableWidget->setColumnCount(6);
    QTableWidgetItem *header1 = new QTableWidgetItem;
    header1->setText(tr("id"));
    ui->tableWidget->setHorizontalHeaderItem(0,header1);
    QTableWidgetItem *header2 = new QTableWidgetItem;
    header2->setText(tr("Congregation"));
    ui->tableWidget->setHorizontalHeaderItem(1,header2);
    QTableWidgetItem *header3 = new QTableWidgetItem;
    header3->setText(tr("Speaker"));
    ui->tableWidget->setHorizontalHeaderItem(2,header3);
    QTableWidgetItem *header4 = new QTableWidgetItem;
    header4->setText(tr("Phone"));
    ui->tableWidget->setHorizontalHeaderItem(3,header4);
    QTableWidgetItem *header5 = new QTableWidgetItem;
    header5->setText(tr("Public talks"));
    ui->tableWidget->setHorizontalHeaderItem(4,header5);
    QTableWidgetItem *header6 = new QTableWidgetItem;
    header6->setText(tr("Language"));
    ui->tableWidget->setHorizontalHeaderItem(5,header6);
    QString line = "";
    for (int i = 0; i < list.size(); ++i)
    {
        QString readline = list.at(i);
        qDebug() << readline;
        QStringList row = readline.split("\t");
        if (row.size() < 4) {
            continue;
        }

        if (i == 0){
            // ask name format for first row
            QMessageBox msgBox;
            msgBox.setText(row[1]);
            msgBox.setIcon(QMessageBox::Question);
            QPushButton *firstButton = msgBox.addButton(tr("First name") + " " + tr("Last name"), QMessageBox::ActionRole);
            QPushButton *lastButton = msgBox.addButton(tr("Last name") + " " + tr("First name"), QMessageBox::NoRole);

            msgBox.exec();
            if (msgBox.clickedButton() == firstButton) {
                firstnamefirst = true;
            } else if (msgBox.clickedButton() == lastButton) {
                firstnamefirst = false;
            }
        }

        int rowcount = ui->tableWidget->rowCount();
        rowcount += 1;
        ui->tableWidget->setRowCount(rowcount);
        ui->tableWidget->setItem(rowcount-1,0,new QTableWidgetItem(""));
        ui->tableWidget->setItem(rowcount-1,1,new QTableWidgetItem(row[0]));
        QString name = row[1];
        if (!firstnamefirst){
            if (row[1].split(" ").size() > 0)
                name = row[1].split(" ").at(1) + " " + row[1].split(" ").at(0);
        }
        ui->tableWidget->setItem(rowcount-1,2,new QTableWidgetItem(name));
        ui->tableWidget->setItem(rowcount-1,3,new QTableWidgetItem(row[2]));
        ui->tableWidget->setItem(rowcount-1,4,new QTableWidgetItem(row[3]));
        QComboBox* combo = new QComboBox();
        QList<QPair<int, QString> > l = sql->getLanguages();
        int defaultLangId = sql->getLanguageDefaultId();
        int setLangId = 0;
        for(int i=0; i<l.count();i++){
            if(combo->findText(l.at(i).second,Qt::MatchExactly) < 0)
                combo->addItem(l.at(i).second);
            if (l.at(i).first == defaultLangId) setLangId = i;
        }
        combo->setCurrentIndex(setLangId);

        ui->tableWidget->setCellWidget(rowcount-1,5,combo);
        line = "";
    }
}

void importwizard::fillSongs()
{
    QString alltext = ui->plainTextEdit->document()->toPlainText();
    QStringList list = alltext.split('\n');

    ui->tableWidget->setRowCount(0);
    ui->tableWidget->setColumnCount(2);
    QTableWidgetItem *header1 = new QTableWidgetItem;
    header1->setText("Number");
    ui->tableWidget->setHorizontalHeaderItem(0, header1);
    QTableWidgetItem *header2 = new QTableWidgetItem;
    header2->setText(tr("Title"));
    ui->tableWidget->setHorizontalHeaderItem(1, header2);

    QRegularExpression rxSong("(\\d+)[.]*\\s*(.+)");
    QString line = "";
    for (int i = 0; i < list.size(); ++i)
    {
        QString readline = list.at(i);

        QRegularExpressionMatch m = rxSong.match(readline);
        if (!m.hasMatch())
            continue;

        QString songNumber = m.captured(1);
        songNumber = songNumber.trimmed();
        QChar cn = songNumber.data()->toLatin1();
        if(!cn.isNumber()) continue;

        QString songTitle = m.captured(2);
        songTitle = songTitle.trimmed();

        int rowCount = ui->tableWidget->rowCount();
        rowCount += 1;
        ui->tableWidget->setRowCount(rowCount);
        QTableWidgetItem *numberWidgetItem = new QTableWidgetItem;
        numberWidgetItem->setText(songNumber);
        ui->tableWidget->setItem(rowCount - 1, 0, numberWidgetItem);
        QTableWidgetItem *titleWidgetItem = new QTableWidgetItem;
        titleWidgetItem->setText(songTitle);
        ui->tableWidget->setItem(rowCount - 1, 1, titleWidgetItem);
        line = "";
    }
}

void importwizard::saveToDatabase()
{
    int added = 0;
    school s;
    QProgressDialog progress( this );
    progress.setCancelButton(0);
    progress.setWindowModality(Qt::WindowModal);
    progress.show();
    switch (m_importtype)
    {
    case 0:
        // school program
        // checking if exist        
        progress.setMaximum(ui->tableWidget->rowCount());
        for (int i = 0; i < ui->tableWidget->rowCount(); ++i)
        {
            progress.setValue(i);
            QDate d = QDate::fromString(ui->tableWidget->item(i,0)->text(),Qt::ISODate);
            int assignmentNo = ui->tableWidget->item(i,1)->text().toInt();
            if (s.getTheme(d,assignmentNo) == -1){

                QString theme = ui->tableWidget->item(i,2)->text();
                QString source = ui->tableWidget->item(i,3)->text();

                QTableWidgetItem *chkBox = ui->tableWidget->item(i,4);
                bool onlyForBrothers = (assignmentNo == 3 && chkBox->checkState() == Qt::Checked);

                if (s.addTheme(QDate::fromString(ui->tableWidget->item(i,0)->text(),Qt::ISODate),
                           ui->tableWidget->item(i,1)->text().toInt(),
                           // now that sql_class::insertSql uses parameters, the apostrophes are not an issue
                           theme,
                           source,
                           onlyForBrothers) > 0){
                    added ++;
                }
            }
        }
        break;
    case 1:
        // school studies
        for (int i = 0; i < ui->tableWidget->rowCount(); ++i)
        {
            progress.setValue(i);
            sql_item sp;
            sp.insert("study_name",ui->tableWidget->item(i,1)->text());
            sp.insert("study_number",ui->tableWidget->item(i,0)->text());

            sql_items e = sql->selectSql("studies","study_number",ui->tableWidget->item(i,0)->text(),"");
            if(e.empty()){
                // create new
                if(sql->insertSql("studies",&sp,"id") != -1){
                    added ++;
                }
            }else{
                sp.insert("active",1);
                if(sql->updateSql("studies","id",e[0].value("id").toString(),&sp)){
                    added ++;
                }
            }
        }
        sql->execSql("update studies set "
                    "reading = case when study_number > 17 then 0 else 1 end, "
                    "demonstration = case when study_number in (7, 52, 53) then 0 else 1 end, "
                    "discource = case when study_number in (7, 18, 30) then 0 else 1 end");
        break;
    case 2:
        // school settings
        for (int i = 0; i < ui->tableWidget->rowCount(); ++i)
        {
            progress.setValue(i);
            sql_items e = sql->selectSql("school_settings","number",ui->tableWidget->item(i,0)->text(),"");
            if(e.empty()){
                // create new
                sql_item sp;
                sp.insert("number",ui->tableWidget->item(i,0)->text());
                sp.insert("name",ui->tableWidget->item(i,1)->text());
                sp.insert("brothers",ui->tableWidget->item(i,0)->text().toInt() > 30);
                if(sql->insertSql("school_settings",&sp,"id") != -1){
                    added ++;
                }
            }
        }
        break;
    case 3:
    {
        // public talk themes
        QDate date = QDate::currentDate();
        for (int i = 0; i < ui->tableWidget->rowCount(); ++i)
        {
            progress.setValue(i);
            sql_items e = sql->selectSql("SELECT * FROM publictalks WHERE theme_number = '" +
                                         ui->tableWidget->item(i,0)->text() +
                                         "' AND lang_id = " + QString::number(m_selectedLanguage) + " AND active "
                                         "AND (discontinue_date IS NULL OR discontinue_date='' OR discontinue_date > '" + date.toString(Qt::ISODate) + "') "
                                         "AND (release_date IS NULL OR release_date='' OR release_date <= '" + date.toString(Qt::ISODate) + "') ");


            bool cancelImport = false;

            if(!e.empty()){
                QString existingTheme = e[0].value("theme_name").toString();
                QString newTheme = ui->tableWidget->item(i,1)->text();

                // similarity check
                if (existingTheme.compare(newTheme, Qt::CaseInsensitive) == 0)
                {
                    // public talk is already saved
                    cancelImport = true;
                }
                else
                {
                    if (QMessageBox::question(this,"TheocBase",
                                              tr("A public talk with the same number is already saved!\n"
                                                 "Do you want to discontinue the previous talk?\n\n"
                                                 "Scheduled talks will be moved to the To Do List.") + "\n\n" +
                                              tr("Previous talk: ") + ui->tableWidget->item(i,0)->text() + " " + existingTheme + "\n" +
                                              tr("New talk: ") + ui->tableWidget->item(i,0)->text() + " " + ui->tableWidget->item(i,1)->text(),
                                              QMessageBox::Ok,QMessageBox::Cancel) == QMessageBox::Cancel)
                        cancelImport = true;
                    else
                    {
                        // discontinue previous talk
                        sql_item s;
                        s.insert("discontinue_date", QDate::currentDate().addDays(-1).toString(Qt::ISODate));
                        sql->updateSql("publictalks", "id", e[0].value("id").toString(), &s);

                        emit discontinueTalk(e[0].value("id").toInt());
                    }
                }
            }

            if(!cancelImport){
                // create new
                sql_item sp;
                sp.insert("theme_number",ui->tableWidget->item(i,0)->text());
                // now that sql_class::insertSql uses parameters, the apostrophes are not an issue
                // (fix if theme contains apostrophe (fix: ' --> ''))
                sp.insert("theme_name",ui->tableWidget->item(i,1)->text());
                sp.insert("lang_id",m_selectedLanguage);

                // check for discontinued talks
                e = sql->selectSql("SELECT * FROM publictalks WHERE theme_number = '" +
                                   ui->tableWidget->item(i,0)->text() +
                                   "' AND lang_id = " + QString::number(m_selectedLanguage) + " AND active");
                if(!e.empty()){
                    // discontined talk exists; add current date as release date,
                    // to make sure no concurrent talks exist at the same time
                    sp.insert("release_date", date.toString(Qt::ISODate));
                }

                if(sql->insertSql("publictalks",&sp,"id") != -1){
                    added ++;
                }
            }
        }
        break;
    }
    case 4:
    {
        // public talk speakers
        QDate date = QDate::currentDate();
        sql_items aiheet = sql->selectSql("SELECT * FROM publictalks WHERE active");
        if(!aiheet.empty()){
            for (int i = 0; i < ui->tableWidget->rowCount(); ++i)
            {
                progress.setValue(i);

                // onko seurakunta olemassa
                ccongregation cc;
                int srkid = cc.getCongregationId(ui->tableWidget->item(i,1)->text());
                if(srkid < 0){
                    // luodaan uusi seurakunta
                    srkid = cc.addCongregation(ui->tableWidget->item(i,1)->text()).id;
                }

                // check speaker
                cpersons cp;
                qDebug() << ui->tableWidget->item(i,2)->text();
                person *speaker = cp.getPerson(ui->tableWidget->item(i,2)->text(),"FirstName LastName");
                //qDebug() << QString::number(puhuja->id);
                if (!speaker){
                    // add new speaker
                    speaker = new person();
                    if (ui->tableWidget->item(i,2)->text().split(" ").size() > 0){
                        speaker->setFirstname( ui->tableWidget->item(i,2)->text().split(" ").at(0) );
                        speaker->setLastname( ui->tableWidget->item(i,2)->text().split(" ").at(1) );
                    }else{
                        speaker->setLastname( ui->tableWidget->item(i,2)->text() );
                    }
                    speaker->setPhone(ui->tableWidget->item(i,3)->text());
                    speaker->setCongregationid( srkid );
                    speaker->setServant(true);
                    speaker->setGender(person::Male);
                    speaker->setUsefor( person::PublicTalk );
                    speaker->setId( cp.addPerson(speaker) );
                }
                // language

                QComboBox *c = static_cast<QComboBox *>(ui->tableWidget->cellWidget(i,5));
                QList<QPair<int, QString> > l = sql->getLanguages();
                QString kieli_id = QVariant(l.at( c->currentIndex() ).first).toString();
                // speaker's themes to database
                QStringList row = ui->tableWidget->item(i,4)->text().split(",");
                qDebug() << row.count();

                foreach(QString value, row){
                    value = value.trimmed();
                    value.replace(" ","");
                    qDebug() << "-" << value << "-";
                    QChar checkNumber = value.data()->toLatin1();
                    if (!checkNumber.isNumber()) continue;

                    // check talk
                    sql_items e = sql->selectSql("SELECT * FROM publictalks WHERE theme_number = '" +
                                                 value +
                                                 "' AND lang_id = " + kieli_id + " AND active "
                                                 "AND (discontinue_date IS NULL OR discontinue_date='' OR discontinue_date > '" + date.toString(Qt::ISODate) + "') "
                                                 "AND (release_date IS NULL OR release_date='' OR release_date <= '" + date.toString(Qt::ISODate) + "') ");

                    if(!e.empty()){
                        QString theme_id = e[0].value("id").toString();
                        sql_items pe = sql->selectSql("SELECT * FROM speaker_publictalks WHERE speaker_id ='"+
                                                      QString::number(speaker->id())+"' AND theme_id = '" + theme_id +
                                                      "' AND lang_id = '" + kieli_id + "' AND active");
                        if(pe.empty()){
                            sql_item s;
                            s.insert("speaker_id",speaker->id());
                            s.insert("theme_id", theme_id);
                            s.insert("lang_id", kieli_id);
                            sql->insertSql("speaker_publictalks",&s,"id");
                        }
                    }
                }
                added ++;

            }
        }else{
            QMessageBox::information(this,"Theocbase",tr("Public talk themes not found. Add themes and try again!"));
        }
    }
        break;
    case 5:
        // songs
        sql->startTransaction();
        for (int i = 0; i < ui->tableWidget->rowCount(); ++i)
        {
            progress.setValue(i);
            sql_items e = sql->selectSql("SELECT * FROM song WHERE song_number = '" +
                                         ui->tableWidget->item(i,0)->text() +
                                         "' AND lang_id = " + QString::number(m_selectedLanguage) + " AND active");
            if(e.empty()){
                // create new
                sql_item song;
                song.insert("song_number",ui->tableWidget->item(i,0)->text());
                song.insert("title",ui->tableWidget->item(i,1)->text());
                song.insert("lang_id",m_selectedLanguage);
                if(sql->insertSql("song", &song, "id") != -1){
                    added ++;
                }
            }
        }
        sql->commitTransaction();
        break;
    }
    QMessageBox::information(this,"Theocbase",QString::number(added) + tr(" rows added"));
    progress.close();
}

bool importwizard::validateCurrentPage()
{
    qDebug() << this->currentId();

    switch(this->currentId())
    {
    case 0:
        return checkValues();
        break;
    case 1:
        saveToDatabase();
        break;
    }

    return true;
}




































