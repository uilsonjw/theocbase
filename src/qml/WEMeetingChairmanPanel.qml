/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import net.theocbase 1.0

ScrollView {
    id: wtStudyEdit
    property string title: "Weekend Meeting Chairman"
    width: parent.width
    height: parent.height
    clip: true

    property CPTMeeting meeting
    onMeetingChanged: {
        if (meeting)
            controller.date = meeting.date
    }

    ColumnLayout {
        id: layout
        x: 10
        y: 10
        width: wtStudyEdit.width - 20

        // Chairman
        RowLayout {
            ToolButton {
                icon.source: "qrc:/icons/servant.svg"
                background: null
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                ToolTip.text: qsTr("Chairman")
                ToolTip.visible: hovered
            }
            ComboBoxTable {
                Layout.fillWidth: true
                Layout.bottomMargin: 10
                currentText: meeting.chairman ? meeting.chairman.fullname : ""
                column4.visible: false
                onBeforeMenuShown: {
                    model = controller.brotherList(Publisher.Chairman)
                    column2.resizeToContents()
                }
                onRowSelected: {
                    console.log(id)
                    var chairman = CPersons.getPerson(id)
                    meeting.chairman = chairman
                    meeting.save()
                }
                PublicMeetingController { id: controller }
            }
        }

        // WT Song
        RowLayout {
            ToolButton {
                icon.source: "qrc:/icons/song.svg"
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                background: null
                ToolTip.text: qsTr("Song")
                ToolTip.visible: hovered
                Layout.fillWidth: true
            }
            NumberSelector {
                Layout.fillWidth: true
                Layout.preferredHeight: height
                maxValue: 151
                selectedValue: meeting.songTalk
                onSelectedValueChanged: {
                    if (selectedValue !== meeting.songTalk) {
                        meeting.songTalk = selectedValue
                        meeting.save()
                    }
                }
            }
        }
    }
}
