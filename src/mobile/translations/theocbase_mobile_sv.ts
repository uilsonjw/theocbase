<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation>Namn</translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation>Datum</translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Tilldela inte nästa övning</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Stå kvar på samma övning</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Ogiltig data</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Lägga till tidtagningen?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Källa</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Elev</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Medhjälpare</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Resultat</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Avklarad</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Inhoppare</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Välj en inhoppare</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Samma övning</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Övningar avklarade</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Nästa övning</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Välj nästa övning</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Tidtagning</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Anteckningar</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Skolschema</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Starta tidtagningen</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Stoppa tidtagningen</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Samtalssituation</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Välj samtalssituation</translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>No exception</source>
        <translation>Inget undantag</translation>
    </message>
    <message>
        <source>Circuit overseer&#x27;s visit</source>
        <translation>Kretstillsyningsmannens besök</translation>
    </message>
    <message>
        <source>Circuit assembly</source>
        <translation>Kretssammankomst</translation>
    </message>
    <message>
        <source>Regional convention</source>
        <translation>Regional sammankomst</translation>
    </message>
    <message>
        <source>Memorial</source>
        <translation>Åminnelsen</translation>
    </message>
    <message>
        <source>Other exception</source>
        <translation>Annat undantag</translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation>Veckan som börjar %1</translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation>Mötesdagar</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Veckomöte</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Helgmöte</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Källa</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Ledare</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Talare</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Läsare</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Noteringar</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Detaljer</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <comment>Page title</comment>
        <translation>Noteringar</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>HÖJDPUNKTER FRÅN BIBELN</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>ÖVNING FÖR TJÄNSTEN</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>LIVET SOM KRISTEN</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Ordförande</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Rådgivare</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Sång %1 och bön</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Sång</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Ledare</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Läsare</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Inledande ord</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Bön</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation>Importera schema…</translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation>MH</translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation>A1</translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation>A2</translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation>Avslutande ord</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Veckomötet</translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Stå kvar på samma övning</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Starta tidtagningen</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Stoppa tidtagningen</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Lägga till tidtagningen?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Källa</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Elev</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Resultat</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Avklarad</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Tidtagning</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Samma övning</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Övningar avklarade</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Nästa övning</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Välj nästa övning</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Noteringar</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Medhjälpare</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Inhoppare</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>Tidtagning</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>Medhjälparen skall inte vara någon av motsatt kön</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>Lektion</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Detaljer</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>Dra neråt för att uppdatera…</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Släpp för att uppdatera...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation>Användarnamn</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Lösenord</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Logga in</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Användarnamn och e-postadress</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-post</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Skapa konto</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>Återställ lösenord</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>Hittade inte någon E-postadress</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Glömt lösenordet</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>Inloggningssida</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation>Veckan startar %1</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation>Noteringar</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation>Utlånade talare</translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>% 1 talare utlånad det här veckoslutet</numerusform>
            <numerusform>% 1 talare utlånade det här veckoslutet</numerusform>
        </translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation>Inga talare utlånade det här veckoslutet</translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation>Utskriftsalternativ</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Schema</translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation>Arbetshäften</translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation>Schema för utlånade talare</translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation>Utlånade talrares uppgifter</translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation>Samtalslista och värdschema</translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation>Tal av talare</translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Skriv ut</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Veckomöte</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Helgmöte</translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation>Kombinering</translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation>Uppgiftslappar</translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation>Mall</translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation>Uppgiftslappar för medjälpare</translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation>Skriv ut endast tilldelade</translation>
    </message>
    <message>
        <source>Text size</source>
        <translation>Textstorlek</translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation>Anpassade mallar</translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Sång och bön</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Sång %1 och bön</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>OFFENTLIGT TAL</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>VT-STUDIUM</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Sång %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Ledare</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Läsare</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation>Importera VT ...</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Helgmötet</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Veckomötet</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>Församling</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Talare</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Mobil</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-post</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Värd</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>Offentligt föredrag</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>Förnamn</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Efternamn</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Broder</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Syster</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Förordnad</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Familj</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>Familjemedlem till</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Kontaktinformation</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-post</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>Samtliga skolor</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Endast skolan i huvudsalen</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Endast skolor i andra salar</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Familjeöverhuvud</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Medhjälpare</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>Aktiv</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Ordförande</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>Höjdpunkter från Bibeln</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Bibelläsning</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Förstabesök</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Återbesök</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Bibelkurs</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>Tal i Livet som kristen</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Församlingens bibelstudium</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Läsare till bibelstudiet</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>Ny förkunnare</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Bön</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation>Värd för offentlig talare</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Mobil</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Veckomöte</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>Tal</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Helgmöte</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>Vakttornsstudieledare</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation>Vakttornsstudieläsare</translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation>Andliga guldkorn</translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation>Resonemang med video</translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Förkunnare</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation>Vallista</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Inställningar</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Logga ut</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Information</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>TheocBase-hemsidan</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Respons</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>Senast synkroniserad: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Schema</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Visa tid</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Visa längd</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Användargränssnitt</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Språk</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Logga in</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Mail</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Namn</translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation>Visa sångtitlar</translation>
    </message>
    <message>
        <source>Printing</source>
        <translation>Skriver ut</translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation>Anpassade mallar</translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Synkroniserar...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>Ordförande</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Sång</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation>Ordförande för helgmötet</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>Sång</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation>Sång för vakttornsstudiet</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Bön</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation>Utgåva av Vakttornet</translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>Artikel</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>Studieledare</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>Läsare</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>Översikt över uppgifter</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>Antal veckor före valt datum</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>Antal veckor efter valt datum</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>veckor</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation>Antal grå veckor efter en uppgift</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>Samma ändringar finns både lokalt och i molnet (%1 rows). Vill du spara de lokala ändringarna?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>Molndata har återställts. Dina lokala data kommer att ersättas. Fortsätta?</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Tilldela inte nästa övning</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>Inte satt</translation>
    </message>
</context></TS>