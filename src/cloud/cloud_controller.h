#ifndef CLOUD_CONTROLLER_H
#define CLOUD_CONTROLLER_H

#include <QObject>
#include <QDebug>
#include <QString>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QNetworkConfigurationManager>
#include <QCryptographicHash>
#include <QEventLoop>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QSettings>
#include <QTimer>
#include <QQmlEngine>
#include "../sync_cloud.h"
#include "../sql_class.h"
#include "../constants.h"
#include "dropbox.h"
#include "../accesscontrol.h"

class cloud_controller : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QDateTime lastSyncTime READ lastSyncTime WRITE setLastSyncTime NOTIFY lastSyncTimeChanged)
    Q_PROPERTY(QString backgroundDebug READ debugBackground WRITE setDebugBackground)
    Q_PROPERTY(SyncState syncState READ syncState NOTIFY stateChanged)
    Q_PROPERTY(dropbox *authentication READ authentication CONSTANT)
    Q_PROPERTY(bool logged READ logged NOTIFY loggedChanged)

public:
    enum SyncState {
        Synchronized,
        Upload,
        Download,
        Both
    };
    Q_ENUM(SyncState)

    cloud_controller(QObject *parent = nullptr);

    Q_INVOKABLE void login();
    bool logged();
    Q_INVOKABLE void logout(int clearDB = 1);
    Q_INVOKABLE bool checkCloudUpdates();

    Q_INVOKABLE void synchronize(bool ignoreUser = false);
    Q_INVOKABLE void continueSynchronize(bool keepLocalChanges);
    Q_INVOKABLE void uploadAccessControl();
    Q_INVOKABLE void initAccessControl();
    Q_INVOKABLE void loadAccessControl();
    Q_INVOKABLE void runTest();
    Q_INVOKABLE bool canResetCloudData();
    Q_INVOKABLE void resetCloudData();
    Q_INVOKABLE void clearDatabase();
    dropbox *authentication();
    QDateTime lastSyncTime() const;
    void setLastSyncTime(QDateTime value);
    SyncState syncState();

    QString debugBackground();
    void setDebugBackground(QString value);

private:
    const QString apiurl = "https://api.theocbase.net/api.php";
    const QString defaultDropboxDir = "/Apps/TheocBase/";
    sync_cloud s;
    QDateTime mLastSyncTime;
    SyncState mSyncState;

    dropbox *mDropbox;
    DBAccount *mDBAccount;

    void syncTimeCheck();
    void initDropbox();
    const QString cachedAccessControlFile();
    const QString cloudAccessControlFile();

public slots:
    void databaseChanged(const QString tablename);
private slots:

signals:
    void loginRequired(const QUrl &url);
    void loggedChanged(bool ok);
    void syncStarted();
    void syncConflict(int values);
    void syncProgressed(int value, int max);
    void syncFinished();
    void error(QString message);
    void lastSyncTimeChanged(QDateTime datetime);
    void differentLastDbUser();
    void stateChanged(cloud_controller::SyncState state);
    void cloudResetStarted();
    void cloudResetFound();
    void cloudResetFinished();
};

#endif // CLOUD_CONTROLLER_H
