/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SHAREUTILS_H
#define SHAREUTILS_H

#include <QObject>
#include <QStandardPaths>
#include <QUrl>
#include <QUrlQuery>
#include <QPoint>

class ShareUtils : public QObject
{
    Q_PROPERTY(QString receivedUrl READ receivedUrl WRITE setReceivedUrl NOTIFY receivedUrlChanged)
    Q_OBJECT

public:
    ShareUtils(QObject *parent = nullptr);
    Q_INVOKABLE void sendMail();
    Q_INVOKABLE void saveBackup();
    Q_INVOKABLE void shareText(const QString text);
    Q_INVOKABLE void loginDropbox(const QUrl &url);

    Q_INVOKABLE QString openFile(QString format);

    Q_INVOKABLE void setPoint(QPoint p) { mPoint = p; }

    Q_INVOKABLE bool copyFile(QString sourcePath, QString destPath);
    Q_INVOKABLE bool deleteFile(QString file);

    QString receivedUrl();
    void setReceivedUrl(QString path);

    static ShareUtils* getInstance();

public slots:
    void handleUrlReceived(const QUrl &url);

signals:
    void receivedUrlChanged(QString filename);
    void error(QString message);
    void dbcallback(QString token);    
    
private:
    static ShareUtils* mInstance;
    QString mViewFilePath = "";
    QPoint mPoint;
};
#endif // SHAREUTILS_H
