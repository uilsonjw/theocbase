/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2016, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "territorymanagement.h"
#include "ui_territorymanagement.h"

TerritoryTreeItem::TerritoryTreeItem(const QList<QVariant> &data, TerritoryTreeItem *parent)
    : parentItem(parent), itemData(data)
{
}

TerritoryTreeItem::~TerritoryTreeItem()
{
    qDeleteAll(childItems);
}

TerritoryTreeItem *TerritoryTreeItem::child(int row)
{
    return childItems.value(row);
}

int TerritoryTreeItem::childCount() const
{
    return childItems.count();
}

int TerritoryTreeItem::columnCount() const
{
    return itemData.count();
}

QVariant TerritoryTreeItem::data(int column) const
{
    return itemData.value(column);
}

void TerritoryTreeItem::appendChild(TerritoryTreeItem *item)
{
    childItems.append(item);
}

bool TerritoryTreeItem::insertChildren(int position, int count)
{
    if (position < 0 || position > childItems.size())
        return false;

    for (int row = 0; row < count; ++row) {
        QList<QVariant> columnData;
        columnData << 0;
        columnData << 0;
        columnData << "";
        columnData << 0;
        TerritoryTreeItem *item = new TerritoryTreeItem(columnData, this);
        childItems.insert(position, item);
    }

    return true;
}

bool TerritoryTreeItem::removeChildren(int position, int count)
{
    if (position < 0 || position + count > childItems.size())
        return false;

    for (int row = 0; row < count; ++row)
        delete childItems.takeAt(position);

    return true;
}

TerritoryTreeItem *TerritoryTreeItem::parent()
{
    return parentItem;
}

int TerritoryTreeItem::row() const
{
    if (parentItem)
        return parentItem->childItems.indexOf(const_cast<TerritoryTreeItem *>(this));

    return 0;
}

bool TerritoryTreeItem::setData(int column, const QVariant &value)
{
    if (column < 0 || column >= itemData.size())
        return false;

    itemData[column] = value;
    return true;
}

TerritoryTreeModel::TerritoryTreeModel(QObject *parent)
    : QAbstractItemModel(parent), m_groupByIndex(0), m_selectedTerritory(nullptr)
{
    QList<QVariant> rootData;
    rootData << "ID"
             << "Territory No"
             << "Locality"
             << "City-ID";
    rootItem = new TerritoryTreeItem(rootData);
    workedThroughNames[1] = tr("< 6 months", "territory worked");
    workedThroughNames[2] = tr("6 to 12 months", "territory worked");
    workedThroughNames[3] = tr("> 12 months ago", "territory worked");
    setupModelData(rootItem);
}

TerritoryTreeModel::~TerritoryTreeModel()
{
    delete rootItem;
}

int TerritoryTreeModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return static_cast<TerritoryTreeItem *>(parent.internalPointer())->columnCount();
    else
        return rootItem->columnCount();
}

QVariant TerritoryTreeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    TerritoryTreeItem *item = static_cast<TerritoryTreeItem *>(index.internalPointer());

    switch (role) {
    //    case TerritoryRole:
    //        return QVariant::fromValue( item );
    case TerritoryIdRole:
        return item->data(0);
    case TerritoryNumberRole:
        return item->data(1);
    case LocalityRole:
        return item->data(2);
    case CityIdRole:
        return item->data(3);
    }

    //    if (role != Qt::DisplayRole)
    //        return QVariant();

    return item->data(index.column());
}

Qt::ItemFlags TerritoryTreeModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

bool TerritoryTreeModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    TerritoryTreeItem *item = getItem(index);

    bool result = false;
    switch (role) {
    case Qt::EditRole:
        result = item->setData(index.column(), value);
        break;
    case TerritoryNumberRole:
        result = item->setData(1, value);
        break;
    case LocalityRole:
        result = item->setData(2, value);
        break;
    }

    if (result)
        emit dataChanged(index, index);

    return result;
}

QVariant TerritoryTreeModel::headerData(int section, Qt::Orientation orientation,
                                        int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return rootItem->data(section);

    return QVariant();
}

QModelIndex TerritoryTreeModel::index(int row, int column, const QModelIndex &parent)
        const
{
    //    if (!hasIndex(row, column, parent))
    //        return QModelIndex();

    if (!rootItem || row < 0 || column < 0)
        return QModelIndex();

    TerritoryTreeItem *parentItem;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TerritoryTreeItem *>(parent.internalPointer());

    TerritoryTreeItem *childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

QModelIndex TerritoryTreeModel::index(int territoryId) const
{
    for (int parentRow = 0; parentRow < this->rowCount(); ++parentRow) {
        QModelIndex parent = this->index(parentRow, 0);

        for (int childRow = 0; childRow < this->rowCount(parent); ++childRow) {
            QModelIndex child = this->index(childRow, 0, parent);
            if (child.data(TerritoryIdRole) == territoryId)
                return child;
        }
    }
    return QModelIndex();
}

QModelIndex TerritoryTreeModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    TerritoryTreeItem *childItem = static_cast<TerritoryTreeItem *>(index.internalPointer());
    TerritoryTreeItem *parentItem = childItem->parent();

    if (parentItem == rootItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}

int TerritoryTreeModel::rowCount(const QModelIndex &parent) const
{
    TerritoryTreeItem *parentItem;
    if (parent.column() > 0)
        return 0;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TerritoryTreeItem *>(parent.internalPointer());

    return parentItem->childCount();
}

void TerritoryTreeModel::setupModelData(TerritoryTreeItem *parent)
{
    TerritoryTreeItem *rootItem = parent;
    TerritoryTreeItem *currentParent = nullptr;

    sql_class *sql = &Singleton<sql_class>::Instance();
    QString sqlCommand = "SELECT * FROM territories";
    if (m_groupByIndex == GroupByPublisher || m_groupByIndex == GroupByWorkedThrough) {
        // check for permissions to see only assigned or all territories grouped by publisher or worked date
        AccessControl *ac = &Singleton<AccessControl>::Instance();
        if (ac->isActive() && !ac->user()->hasPermission(PermissionRule::CanViewTerritoryAssignments)) {
            QString email = ac->user()->email();
            int congregationId = sql->getSetting("congregation_id").toInt();
            cpersons cp;
            person *p = cp.getPersonByEmail(email, congregationId);
            int personId = -1;
            if (p)
                personId = p->id();
            sqlCommand.append(" WHERE person_id = " + QVariant(personId).toString());
        }
    }

    QByteArray groupByFieldName = groupByNames().value(m_groupByIndex);
    sqlCommand.append(" ORDER BY " + groupByFieldName + ", locality;");
    sql_items territories = sql->selectSql(sqlCommand);

    if (!territories.empty()) {
        QString currentGroupByFieldValue = nullptr;

        for (unsigned int i = 0; i < territories.size(); i++) {
            const sql_item territory = territories[i];

            QString newGroupByFieldValue = territory.value(groupByFieldName).toString();
            if (newGroupByFieldValue == nullptr) {
                if (m_groupByIndex == 3)
                    newGroupByFieldValue = tr("Not worked", "Group text for territories that are not worked yet.");
                else
                    newGroupByFieldValue = tr("Not assigned", "Value of the field, the territories are grouped by, is empty.");
            } else if (m_groupByIndex == GroupByWorkedThrough) {
                newGroupByFieldValue = workedThroughNames.value(newGroupByFieldValue.toInt());
            }

            QList<QVariant> columnData;

            if (currentGroupByFieldValue != newGroupByFieldValue) {
                if (currentParent != nullptr) {
                    int childCount = currentParent->childCount();
                    // update current group header before the next will be added
                    currentParent->setData(2, QVariant(childCount).toString() + " " + (childCount == 1 ? tr("territory") : tr("territories")));
                }

                // add new group header
                columnData << 0;
                columnData << newGroupByFieldValue;
                columnData << 0;
                columnData << 0;
                currentParent = new TerritoryTreeItem(columnData, rootItem);
                rootItem->appendChild(currentParent);

                currentGroupByFieldValue = newGroupByFieldValue;
            }

            columnData.clear();
            columnData << territory.value("id").toInt();
            columnData << territory.value("territory_number").toInt();
            columnData << territory.value("locality").toString();
            columnData << territory.value("city_id").toInt();

            // Append a new item to the current parent's list of children.
            if (currentParent != nullptr)
                currentParent->appendChild(new TerritoryTreeItem(columnData, currentParent));
        }
        if (currentParent != nullptr) {
            // update last group header
            int childCount = currentParent->childCount();
            currentParent->setData(2, QVariant(childCount).toString() + " " + (childCount == 1 ? tr("territory") : tr("territories")));
        }
    }
}

bool TerritoryTreeModel::insertRows(int position, int rows, const QModelIndex &parent)
{
    TerritoryTreeItem *parentItem = getItem(parent);
    bool success;

    beginInsertRows(parent, position, position + rows - 1);
    success = parentItem->insertChildren(position, rows);
    endInsertRows();

    return success;
}

bool TerritoryTreeModel::removeRows(int position, int rows,
                                    const QModelIndex &parent)
{
    TerritoryTreeItem *parentItem = getItem(parent);
    bool success;

    if (position < 0 || rows < 1 || position + rows > parentItem->childCount())
        return false;

    beginRemoveRows(parent, position, position + rows - 1);
    success = parentItem->removeChildren(position, rows);
    endRemoveRows();

    return success;
}

QHash<int, QByteArray> TerritoryTreeModel::roleNames() const
{
    QHash<int, QByteArray> items;
    items[TerritoryIdRole] = "id";
    items[TerritoryNumberRole] = "territory_number";
    items[LocalityRole] = "locality";
    items[CityIdRole] = "city_id";
    return items;
}

QHash<int, QByteArray> TerritoryTreeModel::groupByNames() const
{
    QHash<int, QByteArray> items;
    items[GroupByCity] = "city";
    items[GroupByPublisher] = "publisher";
    items[GroupByTerritoryType] = "type_name";
    items[GroupByWorkedThrough] = "workedthrough";
    return items;
}

void TerritoryTreeModel::setGroupByIndex(int value)
{
    m_groupByIndex = value;
    updateModel();
}

void TerritoryTreeModel::updateModel()
{
    if (m_selectedTerritory != nullptr)
        if (m_selectedTerritory->isDirty())
            if (!m_selectedTerritory->save())
                return;

    beginResetModel();

    if (rootItem->childCount() > 0) {
        beginRemoveRows(QModelIndex(), 0, rootItem->childCount() - 1);
        rootItem->removeChildren(0, rootItem->childCount());
        endRemoveRows();
    }

    setupModelData(rootItem);

    endResetModel();
}

TerritoryTreeItem *TerritoryTreeModel::getItem(const QModelIndex &index) const
{
    if (index.isValid()) {
        TerritoryTreeItem *item = static_cast<TerritoryTreeItem *>(index.internalPointer());
        if (item)
            return item;
    }
    return rootItem;
}

void TerritoryTreeModel::addNewTerritory()
{
    if (m_selectedTerritory != nullptr) {
        if (m_selectedTerritory->isDirty()) {
            // save current territory
            if (m_selectedTerritory->save()) {
                delete m_selectedTerritory;
            } else {
                return;
            }
        }
    }

    //  create new territory datarow
    territory *newTerritory = new territory();
    newTerritory->setTerritoryNumber(-1);
    newTerritory->save();
    int newId = newTerritory->territoryId();

    if (newId > 0) {
        QModelIndex parent;

        // find "Not assigned" group
        for (int row = 0; row < this->rowCount(); ++row) {
            QModelIndex node = this->index(row, 0);
            if (node.data(TerritoryNumberRole) == tr("Not assigned", "Value of the field, the territories are grouped by, is empty.")) {
                parent = node;
                break;
            }
        }

        QModelIndex rootIndex = createIndex(rootItem->row(), 0, rootItem);

        if (!parent.isValid()) {
            if (this->insertRow(0)) {
                QModelIndex parent_col1 = this->index(0, 1, rootIndex);
                this->setData(parent_col1, tr("Not assigned", "Value of the field, the territories are grouped by, is empty."));

                parent = this->index(0, 0);
            }
        }

        if (parent.isValid()) {
            int pos = this->rowCount(parent);
            if (insertRows(pos, 1, parent)) {
                QModelIndex child = this->index(pos, 0, parent); //this->index(pos, 0, parent);
                this->setData(child, newTerritory->territoryId());
                child = this->index(pos, 1, parent);
                this->setData(child, newTerritory->territoryNumber());

                int childCount = this->rowCount(parent);

                QModelIndex parent_col2 = this->index(parent.row(), 2, rootIndex);
                if (this->setData(parent_col2, QVariant(childCount).toString() + " " + (childCount == 1 ? tr("territory") : tr("territories")))) {
                    emit dataChanged(parent, parent);
                }

                setSelectedTerritory(this->index(pos, 0, parent));
            }
        }
    }
}

void TerritoryTreeModel::removeTerritory(int territoryId)
{
    cterritories *ct = new cterritories;
    if (ct->removeTerritory(territoryId)) {
        // update tree
        QModelIndex territoryIndex = index(territoryId);
        QModelIndex parent = territoryIndex.parent();
        if (territoryIndex.isValid())
            if (removeRows(territoryIndex.row(), 1, territoryIndex.parent())) {
                if (this->rowCount(parent) == 0)
                    removeRows(parent.row(), 1, parent.parent());
            }
        if (selectedTerritory() && selectedTerritory()->territoryId() == territoryId)
            setSelectedTerritory(QModelIndex());
    }
    emit territoryRemoved();
}

QModelIndex TerritoryTreeModel::getGroupNodeIndex(QVariant value) const
{
    int grpRows = this->rowCount();
    for (int i = 0; i < grpRows; i++) {
        QModelIndex idx = index(i, 0);

        if (this->data(idx, TerritoryIdRole) == value)
            return idx;
    }
    return QModelIndex();
}

void TerritoryTreeModel::setSelectedTerritory(const QModelIndex &selectedIndex)
{
    if (selectedIndex.isValid()) {
        if (m_selectedTerritory != nullptr) {
            if (m_selectedTerritory->isDirty()) {
                // save current territory
                if (m_selectedTerritory->save()) {
                    // territory saved
                } else {
                    // territory not saved
                }
            }
        } else {
            m_selectedTerritory = new territory();
            connect(m_selectedTerritory, SIGNAL(territoryNumberChanged(int, int)), this, SLOT(changeTerritoryNumber(int, int)));
            connect(m_selectedTerritory, SIGNAL(localityChanged(int, QString)), this, SLOT(changeLocality(int, QString)));
            connect(m_selectedTerritory, SIGNAL(cityChanged(int, int)), this, SLOT(changeCity(int, int)));
            connect(m_selectedTerritory, SIGNAL(typeChanged(int, int)), this, SLOT(changeType(int, int)));
        }

        // load territory data
        int territoryId = this->data(selectedIndex, TerritoryIdRole).toInt();
        m_selectedTerritory->loadTerritory(territoryId);
    } else {
        m_selectedTerritory = nullptr;
    }
    emit selectedChanged(selectedIndex);
}

void TerritoryTreeModel::changeTerritoryNumber(int territoryId, int territoryNumber)
{
    if (territoryId > 0) {
        QModelIndex territoryIndex = index(territoryId);
        setData(territoryIndex, territoryNumber, TerritoryNumberRole);
    }
}

void TerritoryTreeModel::changeLocality(int territoryId, QString locality)
{
    if (territoryId > 0) {
        QModelIndex territoryIndex = index(territoryId);
        setData(territoryIndex, locality, LocalityRole);
    }
}

void TerritoryTreeModel::changeCity(int territoryId, int cityId)
{
    // reload; TODO: move treeitem
    Q_UNUSED(territoryId)
    Q_UNUSED(cityId)
    if (m_groupByIndex == GroupByCity)
        setGroupByIndex(GroupByCity);
}

void TerritoryTreeModel::changeType(int territoryId, int typeId)
{
    // reload; TODO: move treeitem
    Q_UNUSED(territoryId)
    Q_UNUSED(typeId)
    if (m_groupByIndex == GroupByTerritoryType)
        setGroupByIndex(GroupByTerritoryType);
}

TerritoryTreeSFProxyModel::TerritoryTreeSFProxyModel(QObject *parent)
    : QSortFilterProxyModel(parent)
{
}

QObject *TerritoryTreeSFProxyModel::source() const
{
    return sourceModel();
}

void TerritoryTreeSFProxyModel::setSource(QObject *source)
{
    setSourceModel(qobject_cast<QAbstractItemModel *>(source));
}

QByteArray TerritoryTreeSFProxyModel::sortRole() const
{
    return roleNames().value(QSortFilterProxyModel::sortRole());
}

void TerritoryTreeSFProxyModel::setSortRole(const QByteArray &role)
{
    QSortFilterProxyModel::setSortRole(roleKey(role));
    emit sortChanged();
}

void TerritoryTreeSFProxyModel::setSortOrder(Qt::SortOrder order)
{
    QSortFilterProxyModel::sort(0, order);
    emit sortChanged();
}

QVariantMap TerritoryTreeSFProxyModel::get(int row)
{
    QHash<int, QByteArray> names = roleNames();
    QHashIterator<int, QByteArray> i(names);
    QVariantMap res;
    QModelIndex idx = index(row, 0);
    while (i.hasNext()) {
        i.next();
        QVariant data = idx.data(i.key());
        res[i.value()] = data;
    }
    return res;
}

int TerritoryTreeSFProxyModel::roleKey(const QByteArray &role) const
{
    QHash<int, QByteArray> roles = roleNames();
    QHashIterator<int, QByteArray> it(roles);
    while (it.hasNext()) {
        it.next();
        if (it.value() == role)
            return it.key();
    }
    return -1;
}

QHash<int, QByteArray> TerritoryTreeSFProxyModel::roleNames() const
{
    if (QAbstractItemModel *source = sourceModel())
        return source->roleNames();
    return QHash<int, QByteArray>();
}

territorymanagement::territorymanagement(QWidget *parent)
    : QDialog(parent), ui(new Ui::territorymanagement)
{
    sql = &Singleton<sql_class>::Instance();
    ac = &Singleton<AccessControl>::Instance();

    ui->setupUi(this);

    // territory-treeview
    territoryTreeModel = new TerritoryTreeModel();

    // list of territory-types
    sql_items typeItems = sql->selectSql("SELECT * FROM territory_type WHERE active = 1 ORDER BY id;");
    typeListModel = new DataObjectListModel();
    typeListModel->addDataObject(DataObject(-1, " "));
    for (unsigned int i = 0; i < typeItems.size(); i++) {
        typeListModel->addDataObject(DataObject(typeItems[i].value("id").toInt(),
                                                typeItems[i].value("type_name").toString()));
    }

    // list of cities
    sql_items cityItems = sql->selectSql("SELECT * FROM territory_city WHERE active = 1 ORDER BY city;");
    cityListModel = new DataObjectListModel();
    cityListModel->addDataObject(DataObject(-1, " "));
    for (unsigned int i = 0; i < cityItems.size(); i++) {
        cityListModel->addDataObject(DataObject(cityItems[i].value("id").toInt(),
                                                cityItems[i].value("city").toString()));
    }

    // assignments
    assignmentModel = new TerritoryAssignmentModel();

    // streets
    streetModel = new TerritoryStreetModel();
    TerritoryStreetSortFilterProxyModel *streetProxyModel = new TerritoryStreetSortFilterProxyModel(this);
    streetProxyModel->setSourceModel(streetModel);

    // addresses
    addressModel = new TerritoryAddressModel();
    TerritoryAddressSortFilterProxyModel *addressProxyModel = new TerritoryAddressSortFilterProxyModel(this);
    addressProxyModel->setSourceModel(addressModel);

    QVariantMap geoServiceParameters;
    QSettings settings;
    int defaultGeoServiceProvider = settings.value("geo_service_provider/default", 0).toInt();
    QString defaultGeoServiceProviderName;
    switch (defaultGeoServiceProvider) {
    case 2: {
        defaultGeoServiceProviderName = "here";
        QString hereAppId = settings.value("geo_service_provider/here_app_id", "").toString();
        if (!hereAppId.isEmpty())
            geoServiceParameters["here.app_id"] = hereAppId;
        QString hereAppCode = settings.value("geo_service_provider/here_app_code", "").toString();
        if (!hereAppCode.isEmpty())
            geoServiceParameters["here.token"] = hereAppCode;
        break;
    }
    default:
        defaultGeoServiceProviderName = "osm";
        break;
    }

    QQmlContext *ctxt = ui->quickWidget->rootContext();
    ctxt->setContextProperty("accessControl", ac);
    ctxt->setContextProperty("territoryTreeModel", territoryTreeModel);
    ctxt->setContextProperty("typeListModel", typeListModel);
    ctxt->setContextProperty("cityListModel", cityListModel);
    ctxt->setContextProperty("assignmentModel", assignmentModel);
    ctxt->setContextProperty("streetModel", streetModel);
    ctxt->setContextProperty("streetProxyModel", streetProxyModel);
    ctxt->setContextProperty("addressModel", addressModel);
    ctxt->setContextProperty("addressProxyModel", addressProxyModel);
    ctxt->setContextProperty("mainWindow", parent);
    ctxt->setContextProperty("defaultGeoServiceProvider", defaultGeoServiceProviderName);
    ctxt->setContextProperty("geoServiceParameters", QVariant::fromValue(geoServiceParameters));
#if defined(Q_OS_LINUX) || defined(Q_OS_MAC) || defined(Q_OS_WIN)
    ctxt->setContextProperty("isGdalAvailable", true);
#endif

    ui->quickWidget->setAttribute(Qt::WA_AlwaysStackOnTop);
    ui->quickWidget->setClearColor(Qt::transparent);
    ui->quickWidget->setSource(QUrl("qrc:/qml/TerritoryManagement.qml"));

    ui->buttonBox->setVisible(false);
}

territorymanagement::~territorymanagement()
{
    delete ui;
}

void territorymanagement::closeEvent(QCloseEvent *event)
{
    emit(territoryTreeModel->saveSelected());
    event->accept();
}

void territorymanagement::on_buttonBox_clicked(QAbstractButton *button)
{
    Q_UNUSED(button)
    emit(territoryTreeModel->saveSelected());
}
