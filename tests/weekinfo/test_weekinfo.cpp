#include <QtTest>
#include <QTest>
#include <QDebug>
#include "../common/common.h"
#include "sql_class.h"
#include "weekinfo.h"
#include "ccongregation.h"
#include "lmm_schedule.h"

class test_weekinfo : public QObject
{
    Q_OBJECT

public:
    test_weekinfo();
    ~test_weekinfo();

private slots:
    void initTestCase();
    void cleanupTestCase();
    void test1_noexception();
    void test2_covisit();
    void test3_clearException();
    void test4_assemblyexception();
    void test5_conventionexception();
    void test6_memorialexception();
    void test7_otherexception();
};

test_weekinfo::test_weekinfo()
{
}

test_weekinfo::~test_weekinfo()
{
}

void test_weekinfo::initTestCase()
{
    common::initDataBase();
}

void test_weekinfo::cleanupTestCase()
{
    common::clearDatabase();
}

void test_weekinfo::test1_noexception()
{
    WeekInfo wi;
    QDate d = QDate::currentDate();
    d = d.addDays(1 - d.dayOfWeek());
    qDebug() << d;
    wi.setDate(d);
    wi.load();
    QVERIFY(wi.exception() == ccongregation::exceptions::None);
    QVERIFY(wi.exceptionDisplayText().isEmpty());
}

void test_weekinfo::test2_covisit()
{
    WeekInfo wi;
    QDate d = QDate::currentDate();
    d = d.addDays(1 - d.dayOfWeek());
    qDebug() << d;
    wi.setDate(d);
    wi.setException(ccongregation::exceptions::CircuitOverseersVisit);
    wi.setExceptionStart(d.addDays(1));
    wi.setExceptionEnd(d.addDays(6));
    wi.saveChanges();

    QVERIFY(wi.exception() == ccongregation::exceptions::CircuitOverseersVisit);
    QVERIFY(!wi.exceptionDisplayText().isEmpty());
}

void test_weekinfo::test3_clearException()
{
    WeekInfo wi;
    QDate d = QDate::currentDate();
    d = d.addDays(1 - d.dayOfWeek());
    wi.setDate(d);
    wi.load();

    QVERIFY(wi.exception() != ccongregation::exceptions::None);
    wi.setException(ccongregation::exceptions::None);
    wi.saveChanges();

    QVERIFY(wi.exception() == ccongregation::exceptions::None);
    QVERIFY(wi.exceptionText().isEmpty());
}

void test_weekinfo::test4_assemblyexception()
{
    WeekInfo wi;
    QDate d = QDate::currentDate();
    d = d.addDays(1 - d.dayOfWeek() + 7);
    wi.setDate(d);
    wi.load();

    wi.setException(ccongregation::exceptions::CircuitAssembly);
    wi.setExceptionStart(d.addDays(6));
    wi.setExceptionEnd(d.addDays(6));
    wi.saveChanges();

    QVERIFY(wi.exception() == ccongregation::exceptions::CircuitAssembly);
    QVERIFY(!wi.exceptionDisplayText().isEmpty());

    // no meetings
    QVERIFY(wi.midweekDay() == 0);
    QVERIFY(wi.weekendDay() == 0);
    // start and end day should be same
    QVERIFY(wi.exceptionStart() == wi.exceptionEnd());
}

void test_weekinfo::test5_conventionexception()
{
    WeekInfo wi;
    QDate d = QDate::currentDate();
    d = d.addDays(1 - d.dayOfWeek() + 7);
    wi.setDate(d);
    wi.load();

    wi.setException(ccongregation::exceptions::RegionalConvention);
    wi.setExceptionStart(d.addDays(4));
    wi.setExceptionEnd(d.addDays(6));
    wi.saveChanges();

    QVERIFY(wi.exception() == ccongregation::exceptions::RegionalConvention);
    QVERIFY(!wi.exceptionDisplayText().isEmpty());

    // no meetings
    QVERIFY(wi.midweekDay() == 0);
    QVERIFY(wi.weekendDay() == 0);
    // start and end day should be same
    QVERIFY(wi.exceptionStart() != wi.exceptionEnd());
}

void test_weekinfo::test6_memorialexception()
{
    WeekInfo wi;
    // Memorial on Tuesday
    QDate memorialDate(2020, 4, 7);
    QDate d = memorialDate.addDays(1 - memorialDate.dayOfWeek());
    wi.setDate(d);
    wi.load();

    wi.setException(ccongregation::exceptions::Memorial);
    wi.setExceptionStart(memorialDate);
    wi.setExceptionEnd(memorialDate);
    wi.saveChanges();

    QVERIFY(wi.exception() == ccongregation::exceptions::Memorial);
    QVERIFY(!wi.exceptionDisplayText().isEmpty());

    // no midweek meeting
    QVERIFY(wi.midweekDay() == 0);
    QVERIFY(wi.weekendDay()  != 0);

    // Memorian on Saturday
    memorialDate.setDate(2021, 3, 27);
    d = memorialDate.addDays(1 - memorialDate.dayOfWeek());
    wi.setDate(d);
    wi.load();

    wi.setException(ccongregation::exceptions::Memorial);
    wi.setExceptionStart(memorialDate);
    wi.setExceptionEnd(memorialDate);
    wi.saveChanges();

    QVERIFY(wi.exception() == ccongregation::exceptions::Memorial);
    QVERIFY(!wi.exceptionDisplayText().isEmpty());

    // no weekend meeting
    QVERIFY(wi.midweekDay() != 0);
    QVERIFY(wi.weekendDay()  == 0);
}

void test_weekinfo::test7_otherexception()
{
    WeekInfo wi;
    QDate d = QDate::currentDate();
    d = d.addDays(1 - d.dayOfWeek() + 7);
    wi.setDate(d);
    wi.load();

    wi.setException(ccongregation::exceptions::Other);
    QString exceptionText = "My custom exception";
    wi.setExceptionText(exceptionText);
    wi.setExceptionStart(d.addDays(0));
    wi.setExceptionEnd(d.addDays(6));
    // midweek meeting on Wednesday
    wi.setMidweekDay(3);
    // weekend meeting on Saturday
    wi.setWeekendDay(6);
    wi.saveChanges();

    QVERIFY(wi.exception() == ccongregation::exceptions::Other);
    QVERIFY(wi.exceptionDisplayText() == exceptionText);

    // meeting days
    QVERIFY(wi.midweekDay() == 3);
    QVERIFY(wi.weekendDay() == 6);
}

QTEST_MAIN(test_weekinfo)

#include "test_weekinfo.moc"
