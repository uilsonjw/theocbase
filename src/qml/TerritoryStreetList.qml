import QtQuick 2.9
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Window 2.0
import net.theocbase 1.0

TerritoryStreetListForm {
    id: territoryStreetListForm

    property bool canEditTerritories: accessControl.user && accessControl.user.hasPermission(PermissionRule.CanEditTerritories)
    property DataObjectListModel streetTypeListModel: territories.getStreetTypes()

    signal updateStreets()
    signal streetDoubleClicked()

    function addStreet()
    {
        if (territoryTreeModel.selectedTerritory)
        {
            addStreetDialog.x = mainWindow.x + mainWindow.width / 2 - width / 2;
            addStreetDialog.y = mainWindow.y + mainWindow.height / 2 - height / 2;

            addStreetDialog.show();
        }
    }

    function removeStreet()
    {
        streetTableView.selection.forEach(
                    function(rowIndex) {
                        var streetProxyIndex = streetProxyModel.index(rowIndex, 0);
                        var streetIndex = streetProxyModel.mapToSource(streetProxyIndex);
                        var streetId = streetModel.get(streetIndex.row).id;

                        if (streetId > 0)
                            streetModel.removeStreet(streetId);
                        else
                            streetModel.removeRows(streetIndex, 1); // no id because it isn't saved yet
                    })
        updateStreets();
    }

    function selectStreet()
    {
        streetTableView.selection.clear();
        if (streetModel.selectedStreet !== null)
        {
            var streetIndex = streetModel.getStreetIndex(streetModel.selectedStreet.id);
            var streetProxyIndex = streetProxyModel.mapFromSource(streetIndex);
            if (streetTableView.rowCount > streetProxyIndex.row)
            {
                streetTableView.selection.select(streetProxyIndex.row);
                streetTableView.currentRow = streetProxyIndex.row;
            }
        }
    }

    function streetTabFocusChanged(activeFocus)
    {
        if (activeFocus)
            selectStreet();
    }

    addStreetButton.onClicked: addStreet()
    removeStreetButton.onClicked: removeStreet()

    streetTableView.model: streetProxyModel
    streetTableView.onSortIndicatorColumnChanged: streetProxyModel.sort(
                                                       streetTableView.sortIndicatorColumn,
                                                       streetTableView.sortIndicatorOrder)
    streetTableView.onSortIndicatorOrderChanged: streetProxyModel.sort(
                                                      streetTableView.sortIndicatorColumn,
                                                      streetTableView.sortIndicatorOrder)
    streetTableView.onCurrentRowChanged: {
        var currProxyIndex = streetProxyModel.index(streetTableView.currentRow, 0);
        var currIndex = streetProxyModel.mapToSource(currProxyIndex);
        streetModel.setSelectedStreet(currIndex);
    }
    streetTableView.onDoubleClicked: streetDoubleClicked()

    addStreetButton.enabled: territoryStreetListForm.enabled && isGdalAvailable
    removeStreetButton.enabled: streetTableView.rowCount > 0 && streetTableView.selection.count > 0

    Territories {
        id: territories
    }

    NumberAnimation {
        id: invalidDataAnimation
        property: "opacity";
        duration: 300;
        loops: 4;
        easing.type: Easing.InOutElastic;
    }

    Component {
        id: streetNameItemDelegate
        ValidationTextInput {
            id: streetNameTextInput
            text: typeof styleData.value === "undefined" ? "" : styleData.value
            placeholderText: qsTr("Street name")
            color: myPalette.text
            isReadOnly: !canEditTerritories

            Component.onCompleted: {
                var currProxyIndex = streetProxyModel.index(styleData.row, styleData.column);
                var currIndex = streetProxyModel.mapToSource(currProxyIndex);
                isTextValid = streetModel.data(currIndex, styleData.role) === "undefined" ? false : (streetModel.data(currIndex, styleData.role) === "" ? false : true);
                isUntouched = streetModel.get(currIndex.row).id < 1;
            }

            onTextChanged: {
                var currProxyIndex = streetProxyModel.index(styleData.row, styleData.column);
                if (currProxyIndex.row < 0 || currProxyIndex.column < 0)
                    return;

                var currIndex = streetProxyModel.mapToSource(currProxyIndex)
                if (text === "")
                {
                    // delete input
                    streetModel.setData(currIndex, "");
                    isTextValid = false;
                }
                else
                {
                    streetModel.setData(currIndex, text);
                    isTextValid = streetModel.data(currIndex, styleData.role) === "undefined" ? false : true;
                }
            }

            onSelectedRowChanged: {
                streetTableView.selection.clear();
                if (streetTableView.rowCount > row)
                {
                    streetTableView.selection.select(row);
                    streetTableView.currentRow = row;
                }
            }
        }
    }

    Component{
        id: fromToNumberDelegate
        TextInput {
            anchors.fill: parent
            anchors.leftMargin: 5
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft

            text: typeof styleData.value === "undefined" ? "" : styleData.value
            renderType: Text.NativeRendering

            onTextChanged: {
                var currProxyIndex = streetProxyModel.index(styleData.row, styleData.column);
                if (currProxyIndex.row < 0 || currProxyIndex.column < 0)
                    return;

                var currIndex = streetProxyModel.mapToSource(currProxyIndex);
                streetModel.setData(currIndex, text);
            }
        }
    }

    Component{
        id: quantityDelegate
        SpinBox {
            anchors.fill: parent
            anchors.leftMargin: 5
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
            horizontalAlignment: Text.AlignLeft

            value: typeof styleData.value === "undefined" ? "" : styleData.value

            onValueChanged: {
                var currProxyIndex = streetProxyModel.index(styleData.row, styleData.column);
                if (currProxyIndex.row < 0 || currProxyIndex.column < 0)
                    return;

                var currIndex = streetProxyModel.mapToSource(currProxyIndex);
                streetModel.setData(currIndex, value);
            }
        }
    }

    Component{
        id: streetTypeItemDelegate

        ValidationComboBox {
            id: streetTypeComboBox
            textRole: "name"
            model: streetTypeListModel
            showComboAlways: false
            property int streetRow: styleData.row
            enabled: canEditTerritories

            onStreetRowChanged: {
                if (streetRow > -1) {
                    var streetProxyIndex = streetProxyModel.index(styleData.row, styleData.column);
                    var streetIndex = streetProxyModel.mapToSource(streetProxyIndex);
                    var streetTypeId = streetModel.get(streetIndex.row).streetTypeId;
                    var streetTypeIndex = model.getIndex(streetTypeId);
                    if (streetTypeIndex === -1) {
                        model.addDataObject(streetTypeId,
                                            qsTr("Undefined [%1]", "Undefined street type").arg(streetTypeId),
                                            "#000000",
                                            false);
                        streetTypeIndex = model.getIndex(streetTypeId);
                    }
                    if (streetTypeIndex !== -1 && currentIndex !== streetTypeIndex)
                        currentIndex = streetTypeIndex;
                }
                else
                    currentIndex = -1;
                isDataValid = model.get(currentIndex).id > 0 && model.get(currentIndex).isValid;
            }

            onActivated: {
                isUntouched = false;
                var streetProxyIndex = streetProxyModel.index(styleData.row, styleData.column);
                var streetIndex = streetProxyModel.mapToSource(streetProxyIndex);
                streetModel.setData(streetIndex, model.get(currentIndex).id);
                updateStreets();
                isDataValid = model.get(currentIndex).id > 0 && model.get(currentIndex).isValid;
            }

            onSelectedRowChanged: {
                streetTableView.selection.clear();
                if (streetTableView.rowCount > row)
                {
                    streetTableView.selection.select(row);
                    streetTableView.currentRow = row;
                }
            }

            Component.onCompleted: {
                if (styleData.row > -1)
                {
                    var streetProxyIndex = streetProxyModel.index(styleData.row, styleData.column);
                    var streetIndex = streetProxyModel.mapToSource(streetProxyIndex);
                    var streetTypeIndex = model.getIndex(streetModel.get(streetIndex.row).streetTypeId);
                    if (streetTypeIndex !== undefined && currentIndex !== streetTypeIndex)
                        currentIndex = streetTypeIndex;
                }
                else
                    currentIndex = -1;
                isDataValid = model.get(currentIndex).id > 0 && model.get(currentIndex).isValid;
            }
        }
    }

    Window {
        id: addStreetDialog
        title: qsTr("Add streets")
        width: 800
        height: 600
        flags: Qt.Tool | Qt.CustomizeWindowHint | Qt.WindowTitleHint | Qt.WindowStaysOnTopHint
        color: myPalette.window

        property alias territoryId: addStreetForm.territoryId

        function streetListReceived(streetResults) {
            territories.onStreetListReceived.disconnect(streetListReceived);
            territories.onStreetRequestFailed.disconnect(streetRequestFailed);
            streetResultModel.removeRows(0, streetResultModel.rowCount());
            addStreetForm.isBusy = false
            if (streetResults.length > 0) {
                for (var i=0; i<streetResults.length; i++)
                    streetResultModel.addStreetResult(streetResults[i]);
            }
            else
            {
                message.title = qsTr("Add streets", "Add streets to a territory");
                message.text = qsTr("No streets found.", "Add streets to a territory");
                message.visible = true;
                return;
            }
        }

        function streetRequestFailed(errorMessage) {
            territories.onStreetListReceived.disconnect(streetListReceived);
            territories.onStreetRequestFailed.disconnect(streetRequestFailed);
            addStreetForm.isBusy = false;
            message.title = qsTr("Add streets", "Add streets to a territory");
            message.text = errorMessage;
            message.visible = true;
            return;
        }

        onVisibleChanged: {
            x = mainWindow.x + mainWindow.width / 2 - width / 2;
            y = mainWindow.y + mainWindow.height / 2 - height / 2;

            streetResultModel.removeRows(0, streetResultModel.rowCount());
            addStreetForm.streetResultTableView.selection.clear();
            addStreetForm.searchStreetTextField.text = "";
            addStreetForm.isBusy = true;

            if (visible && territoryTreeModel.selectedTerritory)
            {
                territoryId = territoryTreeModel.selectedTerritory.territoryId;
                territories.onStreetListReceived.connect(streetListReceived);
                territories.onStreetRequestFailed.connect(streetRequestFailed);
                territories.requestStreetList(territoryId);
            }
        }

        MessageDialog {
            id: message
            standardButtons: StandardButton.Ok
            icon: StandardIcon.Warning
            modality: Qt.ApplicationModal
        }

        TerritoryAddStreetForm {
            id: addStreetForm
            anchors.leftMargin: 10
            anchors.rightMargin: 10
            anchors.topMargin: 10
            anchors.bottomMargin: 10
            isBusy: true

            StreetResultModel {
                id: streetResultModel
            }

            StreetResultSFProxyModel {
                id: streetResultSFProxyModel
                source: streetResultModel
                searchText: addStreetForm.searchStreetTextField.text
                hideAddedStreets: addStreetForm.hideAddedStreets.checked
            }

            ShareUtils { id: shareUtils }
            copyShortCut.onActivated: {
                var streetNames = [];
                streetResultTableView.selection.forEach(
                            function(rowIndex) {
                                var currProxyIndex = streetResultSFProxyModel.index(rowIndex, 0);
                                var currIndex = streetResultSFProxyModel.mapToSource(currProxyIndex);
                                var streetName = streetResultModel.get(currIndex.row).streetName;
                                streetNames.push(streetName);
                            });
                shareUtils.copyToClipboard(streetNames.join('\n'));
            }

            Component {
                id: streetSelectionDelegate
                Item {
                    anchors.fill: parent
                    CheckBox {
                        id: streetSelectionCheckBox
                        anchors.centerIn: parent
                        checked: styleData.value;
                        onClicked: {
                            var currProxyIndex = streetResultSFProxyModel.index(styleData.row, styleData.column);
                            if (currProxyIndex.row < 0 || currProxyIndex.column < 0)
                                return;
                            var currIndex = streetResultSFProxyModel.mapToSource(currProxyIndex);
                            streetResultModel.setData(currIndex, checked);
                            // restore binding, since the checked property was set to a
                            // static value (true or false) when the CheckBox was clicked
                            checked = Qt.binding(function() { return styleData.value; });
                        }
                    }
                }
            }

            streetResultTableView.onSortIndicatorColumnChanged: {
                if (streetResultTableView.sortIndicatorColumn === 0)
                {
                    for (var i = 0; i < streetResultSFProxyModel.rowCount(); i++)
                    {
                        var currProxyIndex = streetResultSFProxyModel.index(i, 0);
                        var currIndex = streetResultSFProxyModel.mapToSource(currProxyIndex);
                        streetResultModel.setData(currIndex, streetResultTableView.sortIndicatorOrder !== Qt.AscendingOrder);
                    }
                }
            }

            streetResultTableView.onSortIndicatorOrderChanged: {
                if (streetResultTableView.sortIndicatorColumn === 0)
                {
                    for (var i = 0; i < streetResultSFProxyModel.rowCount(); i++)
                    {
                        var currProxyIndex = streetResultSFProxyModel.index(i, 0);
                        var currIndex = streetResultSFProxyModel.mapToSource(currProxyIndex);
                        streetResultModel.setData(currIndex, streetResultTableView.sortIndicatorOrder !== Qt.AscendingOrder);
                    }
                }
            }

            okButton.onClicked: {
                if (territoryId > 0)
                {
                    var newStreetId = 0;
                    var defaultStreetTypeId = streetModel.getDefaultStreetTypeId();
                    // add selected streets
                    for (var i = 0; i < streetResultModel.rowCount(); ++i)
                    {
                        if (streetResultModel.get(i).isChecked)
                        {
                            var streetName = streetResultModel.get(i).streetName;
                            var wktGeometry = streetResultModel.get(i).wktGeometry;

                            newStreetId = streetModel.addStreet(territoryId,
                                                                streetName, wktGeometry, defaultStreetTypeId);
                        }
                    }

                    // select last street, that was added
                    if (newStreetId > 0)
                    {
                        var streetIndex = streetModel.getStreetIndex(newStreetId);
                        var streetProxyIndex = streetProxyModel.mapFromSource(streetIndex);
                        if (streetProxyIndex)
                        {
                            streetTableView.currentRow = streetProxyIndex.row;
                            streetTableView.selection.clear();
                            streetTableView.selection.select(streetProxyIndex.row);
                        }
                    }

                    addStreetDialog.close();
                    updateStreets();
                }
            }

            cancelButton.onClicked: addStreetDialog.close();
        }
    }
}
