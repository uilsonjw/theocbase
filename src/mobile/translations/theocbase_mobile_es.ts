<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>No se asigna el próximo aspecto de la oratoria</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Sigue trabajando en el aspecto de la oratoria actual</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Datos no válidos</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>¿Añadir tiempo?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Fuente</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Estudiante</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Ayudante</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Resultado</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Completado</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Voluntario</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Seleccione a un voluntario</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Aspecto de la oratoria actual</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Ejercicio completado</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Siguiente aspecto de la oratoria</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Seleccione el próximo aspecto de la oratoria</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Tiempo</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Apuntes</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Detalles de la Escuela</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Iniciar cronómetro</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Detener cronómetro</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Marco de circunstancias</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Seleccione un marco de circunstancias</translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>No exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit overseer&#x27;s visit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit assembly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Regional convention</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Memorial</source>
        <translation>Conmemoración</translation>
    </message>
    <message>
        <source>Other exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Fuente</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Conductor</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Discursante</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lector</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Apuntes</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Detalles</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <comment>Page title</comment>
        <translation>Apuntes</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>TESOROS DE LA BIBLIA</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>SEAMOS MEJORES MAESTROS</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>NUESTRA VIDA CRISTIANA</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Presidente</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Consejero</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Canción %1 y oración</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Canción</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Conductor</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lector</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Palabras de introducción</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Oración</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation>Importar programa...</translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation>SP</translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation>A1</translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation>A2</translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation>Palabras de conclusión</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Reunión de entre semana</translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Sigue trabajando en el aspecto de la oratoria actual</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Iniciar cronómetro</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Detener cronómetro</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>¿Añadir tiempo?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Fuente</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Estudiante</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Resultado</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Completado</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Tiempo</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Aspecto de la oratoria actual</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Ejercicio completado</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Siguiente aspecto de la oratoria</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Seleccione el próximo aspecto de la oratoria</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Apuntes</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Ayudante</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Voluntario</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>Cronómetro</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>El ayudante no debe ser una persona del sexo opuesto.</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>Lección</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Detalles</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>Tire para refrescar...</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Suelte para refrescar...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation>Nombre de usuario</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Contraseña</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Iniciar sesión</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Usuario o correo electrónico</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Correo</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Crear cuenta</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>Reiniciar contraseña</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>¡Correo electrónico no disponible!</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Contraseña olvidada</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>Página de acceso</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation>Semana comenzando %1</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation>Apuntes</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation>Discursantes afuera</translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>%1 discursante afuera este fin de semana</numerusform>
            <numerusform>%1 discursantes afuera este fin de semana</numerusform>
        </translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation>Ningún discursante afuera en este fin de semana</translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Programa</translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation>Hoja de trabajo</translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation>Programa para discursantes de nuestra congregación</translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Imprimir</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Text size</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Canción y oración</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Canción %1 y oración</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>DISCURSO PÚBLICO</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>ESTUDIO DE LA ATALAYA</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Canción %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Conductor</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lector</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation>Importar Atalaya...</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Reunión del fin de semana</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Reunión de entre semana</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>Congregación</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Discursante</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Celular</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Teléfono</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Correo electrónico</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Anfitrión</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>Discurso público</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Apellido</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Hermano</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Hermana</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Siervo</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Familia</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>Miembro de la familia de</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Datos de contacto</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Teléfono</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Correo electrónico</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>Todas las salas</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Únicamente sala principal</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Únicamente sala auxiliar</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Cabeza de familia</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Ayudante</translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Active</source>
        <translation>activo</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Presidente</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>TESOROS DE LA BIBLIA</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Lectura de la Biblia</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Primera conversación</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Revisita</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Curso bíblico</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>Discursos de &quot;Nuestra Vida Cristiana&quot;</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Estudio Bíblico de Congregación</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Lector estudio bíblico de cong.</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>Nuevo publicador</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Oración</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation>Hospitalidad</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Celular</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Reunión de entre semana</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>Discurso</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Reunión del fin de semana</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>Conductor del estudio de La Atalaya</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation>Lector del estudio de La Atalaya</translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation>Busquemos perlas escondidas</translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation>Análisis con el auditorio (video)</translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Publicadores</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation>Lista de selección</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Ajustes</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Terminar sesión</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Informaciones</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Versión</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>Página web de TheocBase</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Sugerencias</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>Última sincronización: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Programa</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Mostrar la hora</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Mostrar el tiempo</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Interfaz de usuario</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Idioma</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Acceso</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Correo</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation>Mostrar los títulos de las canciones</translation>
    </message>
    <message>
        <source>Printing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Sincronizando...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>Presidente</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Canción</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation>Presidente de la reunión del fin de semana </translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>Canción</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation>Canción para La Atalaya</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Oración</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation>Edición de La Atalaya</translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>Artículo</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>Conductor</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>Lector</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>Historia</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>Cantidad de semanas antes de la fecha seleccionada</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>Cantidad de semanas después de la fecha seleccionada</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>semanas</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation>Cantidad de semanas en gris después de una asignación</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>Los mismos cambios se encuentran a nivel local como en la nube (%1 filas). ¿Desea guardar los cambios locales?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>Los datos en el servidor se reiniciaron. Sus datos locales se reemplazarán. ¿Desea continuar?</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>No se asigna el próximo aspecto de la oratoria</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>No determinado</translation>
    </message>
</context></TS>