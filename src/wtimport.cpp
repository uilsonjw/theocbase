#include "wtimport.h"

wtimport::wtimport(QObject *parent)
    : QObject(parent), rxSong("", QRegularExpression::UseUnicodePropertiesOption | QRegularExpression::CaseInsensitiveOption)
{
    sql = &Singleton<sql_class>::Instance();
    connect(&epb, &epub::ProcessTOCEntry, this, &wtimport::ProcessTOCEntry);
}

wtimport::wtimport(QString fileName)
    : rxSong("", QRegularExpression::UseUnicodePropertiesOption | QRegularExpression::CaseInsensitiveOption)
{
    sql = &Singleton<sql_class>::Instance();
    connect(&epb, &epub::ProcessTOCEntry, this, &wtimport::ProcessTOCEntry);
    init(fileName);
}

QString wtimport::Import()
{
    validDates = 0;
    lastError.clear();
    firstDate.clear();
    lastDate.clear();
    rxSongState = rxSongStates::noLoadAttempt;
    if (prepared) {
        sql->startTransaction();
        epb.ImportTOC();
        if (lastError.isEmpty()) {
            sql->commitTransaction();
            QString x = QObject::tr("Imported %1 weeks from %2 thru %3").arg(QVariant(validDates).toString(), firstDate, lastDate);
            return QObject::tr("Imported %1 weeks from %2 thru %3").arg(QVariant(validDates).toString(), firstDate, lastDate);
        } else {
            return lastError;
        }
    } else {
        lastError = epb.lastErr;
        return lastError;
    }
}

QString wtimport::importFile(const QString fileName)
{
    if (fileName.endsWith("jwpub", Qt::CaseInsensitive)) {
        jwpub jwpub;
        jwpub.Prepare(fileName);
        return "";
    } else {
        init(fileName);
        return Import();
    }
}

void wtimport::init(QString fileName)
{
    prepared = !epb.Prepare(fileName);
    if (prepared) {
        int yr(0);
        int mo(0);
        QRegularExpression extractYearAndMonth("/w_.+_(\\d\\d\\d\\d\\d\\d)");
        QRegularExpressionMatch match = extractYearAndMonth.match(fileName);
        if (match.hasMatch()) {
            QString txt = match.captured(1);
            yr = txt.leftRef(4).toInt();
            mo = txt.midRef(4, 2).toInt();
            if (yr > 2015 && yr < 3000 && mo > 0 && mo < 13) {
                year = yr;
                month = mo;
            } else {
                yr = 0;
            }
        }
        if (yr == 0) {
            year = -1;
            month = -1;
        }
    } else {
        lastError = epb.lastErr;
    }
}

void wtimport::ProcessTOCEntry(QString href, QString chapter)
{
    Q_UNUSED(chapter);

    if (epb.curlocal != nullptr && rxSongState == rxSongStates::noLoadAttempt) {
        rxSongState = rxSongStates::attempted;
        sql_items rx = sql->selectSql("lmm_workbookregex", "lang", epb.language_ex);
        if (rx.size() == 0)
            rx = sql->selectSql("lmm_workbookregex", "lang", epb.language);
        for (unsigned int i = 0; i < rx.size(); i++) {
            sql_item kv = rx[i];
            if (kv.value("key").toString() == "song") {
                rxSong.setPattern(kv.value("value").toString());
                rxSongState = rxSongStates::loaded;
            }
        }
        if (rxSongState == rxSongStates::attempted) {
            lastError = "no song regex found for " + epb.language;
        }
    }

    queueTheme.clear();
    queueOpenSong = 0;
    queueCloseSong = 0;
    queueArticle_TOC = 0;
    queueArticle_Detail = 0;

    xml_reader r(epb.oebpsPath + "/" + href);

    // will work if in Table of Contents page
    r.register_elementsearch("h3", xmlPartsContexts::studyArticleInfo, -1, true);

    // will work if in wt article
    r.register_attributesearch("p", "class", "contextTtl", xmlPartsContexts::articlePara);
    r.register_elementsearch("h1", xmlPartsContexts::h1);
    r.register_elementsearch("a", xmlPartsContexts::link);
    r.register_elementsearch("strong", xmlPartsContexts::article, xmlPartsContexts::articlePara, true);
    r.register_elementsearch("strong", xmlPartsContexts::title, xmlPartsContexts::h1, true);
    r.register_elementsearch("strong", xmlPartsContexts::song, xmlPartsContexts::link, true);
    XLM_READER_CONNECT(r);
    r.read();

    if (queueOpenSong > 0 && queueCloseSong > 0 && !queueTheme.isEmpty()) {

        if (articleDates.size() == 0) {
            downloadArticleDates();
        }

        QDate dt = articleDates.value(queueArticle_Detail);
        if (dt.isValid()) {
            lastDate = dt.toString(Qt::DefaultLocaleShortDate);
            if (validDates++ == 0)
                firstDate = lastDate;

            sql_item parts;
            parts.insert("wt_theme", queueTheme);
            parts.insert("song_wt_start", queueOpenSong);
            parts.insert("song_wt_end", queueCloseSong);
            parts.insert("wt_source", QVariant(month).toString() + "/" + QString::number(queueArticle_Detail));
            sql->updateSql("publicmeeting", "date", dt.toString(Qt::ISODate), &parts);

            if (sql->lastNumRowsAffected == 0) {
                parts.insert("date", dt.toString(Qt::ISODate));
                sql->insertSql("publicmeeting", &parts, "id");
            }
        }
    }
}

void wtimport::xmlPartFound(QXmlStreamReader *xml, QXmlStreamReader::TokenType tokenType, int context, int relativeDepth)
{
    Q_UNUSED(relativeDepth);

    switch (context) {
    case xmlPartsContexts::studyArticleInfo:
        if (tokenType == QXmlStreamReader::TokenType::Characters) {
            QStringRef txt = xml->text();
            QRegularExpression rxArticle("Study\\s*Article\\s*(\\d+):\\s*(\\D+)\\s+(\\d+).*?,\\s*(\\d+)", QRegularExpression::UseUnicodePropertiesOption); //"Study Article (\\d+).*\\s*(\\D+)\\s*(\\d+).*,\\s*(\\d+)"
            QRegularExpressionMatch match = rxArticle.match(txt);
            if (match.hasMatch()) {
                QString temp = match.captured(2) + " " + match.captured(3) + " " + match.captured(4);
                QLocale loc(QLocale::English);
                QDate dt = loc.toDate(temp, "MMMM d yyyy");
                if (dt.isValid()) {
                    queueArticle_TOC = match.captured(1).toInt();
                    addArticleDate(queueArticle_TOC, dt);
                }
            }
        }
        break;

    case xmlPartsContexts::article:
        if (tokenType == QXmlStreamReader::TokenType::Characters) {
            QStringRef txt = xml->text();
            QRegularExpression numberMatcher("(\\d+)", QRegularExpression::UseUnicodePropertiesOption);
            QRegularExpressionMatch match = numberMatcher.match(txt);
            if (match.hasMatch()) {
                queueArticle_Detail = match.captured(1).toInt();
            }
        }
        break;

    case xmlPartsContexts::title:
        if (tokenType == QXmlStreamReader::TokenType::Characters) {
            queueTheme = xml->text().toString();
        }
        break;

    case xmlPartsContexts::song:
        if (tokenType == QXmlStreamReader::TokenType::Characters && rxSongState == rxSongStates::loaded) {
            QRegularExpressionMatch match = rxSong.match(xml->text());
            if (match.hasMatch()) {
                if (queueOpenSong > 0)
                    queueCloseSong = match.captured(1).toInt();
                else
                    queueOpenSong = match.captured(1).toInt();
            }
        }
        break;
    }
}

bool wtimport::downloadArticleDates()
{
    QNetworkAccessManager manager;
    QEventLoop q;
    QTimer timer;
    bool imported(false);

    timer.setSingleShot(true);
    QObject::connect(&timer, SIGNAL(timeout()), &q, SLOT(quit()));
    QObject::connect(&manager, SIGNAL(finished(QNetworkReply *)),
                     &q, SLOT(quit()));

    QNetworkRequest request;
    QString sUrl =
            "https://www.theocbase.net/workbook/wtassist" + QString("%1").arg(year, 4, 10, QLatin1Char('0')) + ".txt";
    request.setUrl(QUrl(sUrl));
    QNetworkReply *reply = manager.get(request);

    timer.start(10000); // 10s timeout
    q.exec();

    if (timer.isActive()) {
        // download complete
        timer.stop();
        if (reply->error() != QNetworkReply::NoError) {
            qDebug() << "downloadArticleDates error" << reply->errorString();
        } else {
            char ln[20];
            QDate dt;
            while (reply->readLine(ln, 20) != -1) {
                QString ln2(ln);
                if (ln2.endsWith("\n"))
                    ln2 = ln2.left(ln2.length() - 1);
                QStringList parts(ln2.split('\t'));
                queueArticle_TOC = QVariant::fromValue(parts[1]).toInt();
                dt = QDate::fromString(parts[2], "yyyy-MM-dd");
                if (queueArticle_TOC > 0 && dt.isValid()) {
                    addArticleDate(queueArticle_TOC, dt);
                }
            }
            imported = true;
        }
    }

    if (reply != nullptr)
        delete reply;
    return !imported;
}

void wtimport::addArticleDate(int article, QDate dt)
{
    articleDates.insert(queueArticle_TOC, dt);

    sql_item values;
    values.insert(":yr", year);
    values.insert(":article", article);
    values.insert(":dt", dt.toString("yyyy-MM-dd"));
    sql->execSql("replace into wt_article_dates (yr, article, dt) values (:yr, :article, :dt)", &values);
}

void wtimport::exportAssistFiles()
{
    QFile *file(nullptr);
    QTextStream *stream(nullptr);
    sql_item param;
    param.insert(":yr", year - 1);
    sql_items data = sql->selectSql("select * from wt_article_dates where yr >= :yr order by yr, article", &param);
    int lastYr(0);
    for (sql_item row : data) {
        int yr = row.value("yr").toInt();
        QDate dt(row.value("dt").toDate());
        if (lastYr != yr) {
            if (file) {
                delete stream;
                file->close();
                delete file;
            }
            QString assist(sql->databasepath.mid(0, sql->databasepath.lastIndexOf('/')) + "/wtassist" + QString("%1").arg(yr, 4, 10, QLatin1Char('0')) + ".txt");
            file = new QFile(assist);
            if (!file->open(QIODevice::ReadWrite)) {
                QMessageBox::information(0, "TheocBase", "Could not create " + assist);
                delete file;
                return;
            }
            stream = new QTextStream(file);
        }
        (*stream) << yr << '\t' << row.value("article").toString() << '\t' << dt.toString("yyyy-MM-dd") << "\n";
        lastYr = yr;
    }

    if (file) {
        delete stream;
        file->close();
        delete file;
        QMessageBox::information(0, "TheocBase", "lmmassist files created created");
    }
}
