<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="th-TH">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation type="unfinished">ชื่อ</translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>ไม่มอบหมายบทเรียนต่อไป</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>ทำบทเรียนเดิม</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>ข้อมูลไม่ถูกต้อง</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>ต่อเวลาไหม?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>หัวเรื่อง</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>แหล่งข้อมูล</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>นักเรียน</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>ผู้ช่วย</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>ผลลัพธ์</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>เสร็จสมบูรณ์</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>ผู้ทำส่วนแทน</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>เลือกผู้ทำส่วนแทน</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>บทเรียนที่เอาใจใส่</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>ทำแบบฝึกหัดแล้ว</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>บทเรียนต่อไป</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>เลือกบทเรียนต่อไป</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>เวลา</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>ข้อสังเกต</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>ข้อมูลโรงเรียน</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>เริ่มนับเวลา</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>หยุดการนับเวลา</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>ฉาก</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>เลือกฉาก</translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>No exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit overseer&apos;s visit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit assembly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Regional convention</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Memorial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Other exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation type="unfinished">สัปดาห์ซึ่งเริ่มวันที่ %1</translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished">การประชุมกลางสัปดาห์</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished">การประชุมสุดสัปดาห์</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>หัวเรื่อง</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>แหล่งข้อมูล</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>ผู้นำส่วน</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>ผู้ทำส่วน</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>ผู้อ่าน</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>ข้อสังเกต</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>รายละเอียด</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <comment>Page title</comment>
        <translation>ข้อสังเกต</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation type="unfinished">ข้อสังเกต</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&apos;S WORD</source>
        <translation>ความรู้ที่มีค่าจากพระคัมภีร์</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>สิ่ง​ที่​คุณ​จะ​นำ​ไป​ใช้​ใน​งาน​ประกาศ</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>ชีวิต​คริสเตียน</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>ประธาน</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>ผู้ให้คำแนะนำ</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>เพลง %1 และคำอธิษฐาน</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>เพลง</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>ผู้นำส่วน</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>ผู้อ่าน</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>คำนำเปิดการประชุม</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>อธิษฐาน</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation>เพิ่มกำหนดการ...</translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished">การประชุมกลางสัปดาห์</translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>ให้ทำบทเรียนเดิม</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>เริ่มนับเวลา</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>หยุดนับเวลา</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>เพิ่มเวลาไหม?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>หัวเรื่อง</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>แหล่งข้อมูล</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>นักเรียน</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>คุณภาพส่วน</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>เสร็จแล้ว</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>เวลา</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>บทเรียนเดิม</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>ทำแบบฝึกหัดแล้ว</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>บทเรียนต่อไป</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>เลือกบทเรียนต่อไป</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>ข้อสังเกต</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>ผู้ช่วย</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>อาสาสมัคร</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>หยุดนับเวลา</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>ไม่ควรมอบหมายผู้ช่วยที่เป็นเพศตรงข้าม</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>บทเรียน</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>รายละเอียด</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>ลากลงเพื่อโหลด</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>ปล่อยเพื่อโหลด</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation>ชื่อผู้ใช้</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>รหัสผ่าน</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>เข้าสู่ระบบ</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>อีเมลหรือชื่อผู้ใช้</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>อีเมล</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>สร้างบัญชี</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>ตั้งค่ารหัสผ่านใหม่</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>ไม่พบที่อยู่อีเมลนี้!</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>ลืมรหัสผ่าน</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>เข้าสู่ระบบ</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation>สัปดาห์ซึ่งเริ่มวันที่ %1</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation type="unfinished">ข้อสังเกต</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation>พี่น้องที่ไปบรรยายต่างประชาคม</translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>ผู้บรรยายไปที่อื่นในสุดสัปดาห์นี้ % 1 </numerusform>
        </translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation>ไม่มีผู้บรรยายไปที่อื่นในสัปดาห์นี่</translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation type="unfinished">กำหนดการ</translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished">การประชุมกลางสัปดาห์</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished">การประชุมสุดสัปดาห์</translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Text size</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>เพลงและคำอธิษฐาน</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>เพลง %1 และคำอธิษฐาน</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>คำบรรยายสาธารณะ</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>การศึกษาหอสังเกตการณ์</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>เพลง %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>ผู้นำส่วน</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>ผู้อ่าน</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation>เพิ่ม หอฯ...</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished">การประชุมสุดสัปดาห์</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished">การประชุมกลางสัปดาห์</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>หัวเรื่อง</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>ประชาคม</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>ผู้บรรยาย</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>มือถือ</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>โทรศัพท์</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>อีเมล</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>ข้อมูล</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>ผู้ต้อนรับ</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>คำบรรยายสาธารณะ</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>ชื่อ</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>นามสกุล</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>ชาย</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>หญิง</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>ถูกแต่งตั้ง</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>ครอบครัว</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>สมาชิกครอบครัวของ</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>ข้อมูลติดต่อ</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>โทรศัพท์</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>อีเมล์</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>ทุกห้องประชุม</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>ห้องใหญ่</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>ห้องเล็ก</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>หัวหน้าครอบครัว</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>ผู้ช่วย</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>ข้อมูล</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>สม่ำเสมอ</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>ประธาน</translation>
    </message>
    <message>
        <source>Treasures from God&apos;s Word</source>
        <translation>ความรู้ที่มีค่าจากพระคัมภีร์</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>การอ่านพระคัมภีร์</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>ประกาศ</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>กลับเยี่ยม</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>นำการศึกษา</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>ชีวิต​คริสเตียน</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>การศึกษาพระคัมภีร์ประจำประชาคม</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>ผู้อ่านการศึกษาพระคัมภีร์ประจำประชาคม</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>ผู้ประกาศใหม่</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>อธิษฐาน</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation>ผู้ต้อนรับผู้บรรยาย</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>มือถือ</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>การประชุมกลางสัปดาห์</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>คำบรรยาย</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>การประชุมสุดสัปดาห์</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>ผู้นำการศึกษาหอสังเกตการณ์</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation>ผู้อ่าน</translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>ผู้ประกาศ</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation>รายการให้เลือก</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>การตั้งค่า</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>ออกจากระบบ</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>ข้อมูล</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>เวอร์ชั่น</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>หน้าหลัก TheocBase</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>การตอบรับ</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>ปรับข้อมูลครั้งล่าสุด: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>กำหนดการ</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>แสดงเวลา</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>แสดงช่วงเวลา</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>หน้าจอผู้ใช้</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>ภาษา</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>เข้าระบบ</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>อีเมล</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>ชื่อ</translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Printing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>กำลังปรับข้อมูล...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>ประธาน</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>เพลง</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation>ประธานการประชุมสุดสัปดาห์</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>เพลง</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation>เพลงตามหอสังเกตการณ์</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation type="unfinished">อธิษฐาน</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation>หอสังเกตการณ์ฉบับที่</translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>บทความ</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>เรื่อง</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>ผู้นำหอฯ</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>ผู้อ่าน</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>ดูตามลำดับ</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>จำนวนสัปดาห์ก่อนวันที่เลือก</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>จำนวนสัปดาห์หลังวันที่เลือก</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>สัปดาห์</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation>สีเทาเป็นจำนวนสัปดาห์หลังจากที่ได้รับมอบหมาย</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>การเปลี่ยนแปลงนี้สามารถทำได้ในอุปกรณ์ของคุณและในฐานข้อมูล (% 1 แถว) คุณต้องการให้มีการเปลี่ยนแปลงในอุปกรณ์ของคุณหรือไม่?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>มีการตั้งค่าในฐานข้อมูลซึ่งจะแทนที่ในฐานข้อมูลเดิมของคุณ ดำเนินการต่อไหม?</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>ไม่มอบหมายบทเรียนต่อไป</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>ไม่มี</translation>
    </message>
</context>
</TS>
