/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "printoutgoingschedule.h"

PrintOutgoingSchedule::PrintOutgoingSchedule()
{
    currentTemplateData = QSharedPointer<TemplateData>(
            new TemplateData(TemplateData::OutgoingScheduleTemplate, "outgoingSpeakersTemplate", "WE-OutgoingSchedule_1.htm", "we-outgoingschedule"));
    qrCodeSupport = true;
}

QVariant PrintOutgoingSchedule::fillTemplate()
{
    QString str = "";
    if (slips) {
        str = getTemplate();
    } else {
        str = getTemplate();
    }
    str = initLayout(str);
    str = ifthen.tokenize(str);

    fillWeekList(publicmeetingday - 1);
    QDate firstdate(weekList[0]->week);
    qDebug() << firstdate.toString(Qt::ISODate);

    QString basepage = "";
    QString pagesec = "";
    QString ptsec = "";
    if (slips) {
        QPair<QString, QString> sec = templateGetSection(str, "!PTOUT_START!", "!PTOUT_END!", "ptout_class");
        basepage = fillTemplateBlockPTOUTTitle(sec.first);
        sec = templateGetSection(sec.second, "!REPEAT_START!", "!REPEAT_END!", "repeat_class");
        pagesec = sec.first;
        ptsec = sec.second;
    } else {
        QPair<QString, QString> sec = templateGetSection(str, "!PTOUT_START!", "!PTOUT_END!", "ptout");
        basepage = fillTemplateBlockPTOUTTitle(sec.first);
        ptsec = sec.second;
    }

    // titles for all meetings
    basepage.replace("!CONGREGATION_TITLE!", tr("%1 Congregation", "Congregation_Title Tag. %1 is Congregation Name").arg(myCongregation.name));
    basepage.replace("!CONGREGATION!", tr("Congregation"));
    basepage.replace("!CONGREGATION_NAME!", myCongregation.name);
    basepage.replace("!PT_TITLE!", tr("Public Talk"));
    basepage.replace("!WT_TITLE!", tr("Watchtower Study"));

    QString weekContext = "";

    if (slips) {
        // print slips for each speaker
        cpersons cp_class;
        // get speakers in own congregation
        QList<person *> speakers = cp_class.getAllPersons(2);
        bool firstpage = true;
        for (person *speaker : speakers) { // loop each speaker
            QString filledsection = "";
            QDate lastdate = weekList[weekList.length() - 1]->week;
            QString filledpage = fillTemplateBlockPTOUTTitle(QString(pagesec), speaker->fullname());
            filledsection = fillTemplateBlockPTOUTSlips(speaker, firstdate, lastdate, QString(ptsec));

            if (filledsection != "") {
                if (!firstpage)
                    filledpage = addCssPageBreak() + filledpage;
                filledpage.replace("<div class=\"repeat_class\"></div>",
                                   "<div>" + filledsection + "</div>");
                weekContext.append(filledpage);
                firstpage = false;
            }
        }
        qDeleteAll(speakers);
        basepage.replace("<div class=\"ptout_class\"></div>", "<div>" + weekContext + "</div>");
    } else {
        // print overview list for bulleting board
        int lastMonth(-1);
        ptCounter = 0;
        for (weekInfo *w : weekList) {
            currentWeek = w;
            //QDate tempdate(w->week);
            QString oneweek(ptsec);

            if (w->mtgDate.month() != lastMonth) {
                lastMonth = w->mtgDate.month();
                ptCounter = 1;
            }

            oneweek = fillTemplateBlockPTOUT(oneweek); // fill a one week
            if (oneweek != "") {
                weekContext.append(oneweek);
            }
        }
        basepage.replace("<div class=\"ptout\"></div>", "<div>" + weekContext + "</div>");
    }
    return ifthen.compute(basepage);
}

QString PrintOutgoingSchedule::fillTemplateBlockPTOUT(QString context)
{
    QString newcontext = "";
    cpublictalks cpt;
    QList<cpoutgoing *> out = cpt.getOutgoingSpeakers(currentWeek->week);

    bool didOne(false);
    foreach (cpoutgoing *o, out) {
        person *speaker = o->getSpeaker();
        if (!speaker)
            continue;
        didOne = true;

        QString currentSection(context);
        auto starttime = QTime::fromString(o->getCongregation().getPublicmeeting(currentWeek->week).getMeetingtime(), "hh:mm");
        currentSection = fillCommonItems(currentSection, false, c.isException(currentWeek->week));
        currentSection = replaceIntTag(currentSection, "PTCOUNTER", ptCounter++);
        currentSection = replaceDateTag(currentSection, "DATE", currentWeek->week.addDays(o->getCongregation().getPublicmeeting(currentWeek->week).getMeetingday() - 1));
        currentSection = replaceTimeTag(currentSection, "PM_STARTTIME", starttime);
        currentSection.replace("!PT_SPEAKER!", o->getSpeaker()->fullname());
        currentSection.replace("!PT_NO!", QVariant(o->getTheme().number).toString());
        currentSection.replace("!PT_THEME!", o->getTheme().themeInLanguage(sql->getLanguageDefaultId(), currentWeek->week));
        currentSection.replace("!PT_CONGREGATION!", o->getCongregation().name);
        currentSection.replace("!CONGREGATION_ADDRESS!", o->getCongregation().address);

        currentSection = fillTemplateBlockPTOUTTitle(currentSection);

        newcontext.append(currentSection);
    }
    if (!didOne) {
        QString currentSection(context);
        currentSection = fillCommonItems(currentSection, false, c.isException(currentWeek->week));
        currentSection = replaceDateTag(currentSection, "DATE", currentWeek->week.addDays(myCongregation.getPublicmeeting(currentWeek->week).getMeetingday() - 1));
        currentSection.replace("!PT_SPEAKER!", "");
        currentSection.replace("!PT_NO!", "");
        currentSection.replace("!PT_THEME!", "");
        currentSection.replace("!CONGREGATION!", "");
        currentSection.replace("!CONGREGATION_ADDRESS!", "");
        currentSection.replace("!PM_STARTTIME!", "");

        currentSection = fillTemplateBlockPTOUTTitle(currentSection);

        newcontext.append(currentSection);
    }
    return newcontext;
}

QString PrintOutgoingSchedule::fillTemplateBlockPTOUTTitle(QString context, QString speakername)
{
    context.replace("!TALK!", tr("Talk"));
    context.replace("!TITLE!", tr("Outgoing Speakers"));
    context.replace("!DATE_TITLE!", tr("Date"));
    context.replace("!SPEAKER!", tr("Speaker"));
    context.replace("!PT_THEME_TITLE!", tr("Theme"));
    context.replace("!PT_NO_TITLE!", tr("Theme Number"));
    context.replace("!CIRCUIT!", tr("Circuit"));
    context.replace("!CONGREGATION!", tr("Congregation"));
    context.replace("!TIME!", tr("Start Time"));

    context.replace("!DISCLAIMER!", tr("NOTE: Dear brother, in spite of careful database-maintenance, "
                                       "sometimes times or addresses might be out of date. "
                                       "So, please verify by looking those up via JW.ORG. Thank you!"));

    if (speakername != "")
        context.replace("!PT_SPEAKER!", speakername);
    return context;
}

QString PrintOutgoingSchedule::fillTemplateBlockPTOUTSlips(person *speaker, QDate fromdate, QDate todate, QString context)
{
    QString newcontext = "";

    cpublictalks cp;
    QList<cpoutgoing *> out = cp.getOutgoingBySpeaker(speaker->id(), fromdate, todate);

    foreach (cpoutgoing *o, out) {
        QString currentSection(context);
        currentSection = fillCommonItems(currentSection, false, ccongregation::None);

        auto starttime = QTime::fromString(o->time(), "hh:mm");
        currentSection = replaceDateTag(currentSection, "DATE", o->date());
        currentSection = replaceTimeTag(currentSection, "PM_STARTTIME", starttime);
        currentSection.replace("!PT_SPEAKER!", o->getSpeaker()->fullname());
        currentSection = replaceIntTag(currentSection, "PT_NO", o->getTheme().number);
        currentSection.replace("!PT_THEME!", o->getTheme().themeInLanguage(sql->getLanguageDefaultId(), o->date()));
        currentSection.replace("!PT_CONGREGATION!", o->getCongregation().name);
        currentSection.replace("!CONGREGATION_ADDRESS!", o->getCongregation().address);

        currentSection = fillTemplateBlockPTOUTTitle(currentSection);

        newcontext.append(currentSection);
    }
    return newcontext;
}
