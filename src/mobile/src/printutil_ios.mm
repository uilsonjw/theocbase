/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "printutil.h"
#include <UIKit/UIKit.h>
#include <WebKit/WebKit.h>
#include <QFile>
#include <QApplication>

@interface ViewController : UIViewController <WKNavigationDelegate>

@property (nonatomic, strong) NSURL *URL;
@property (nonatomic, strong) NSString *HTML;
@property (nonatomic, strong) NSData *PDFdata;
@property (nonatomic, strong) WKWebView *webview;
@property (nonatomic, assign) CGSize pageSize;
@property (nonatomic, assign) UIEdgeInsets pageMargins;
@property (nonatomic, assign) QPageLayout pageLayout;
@property (nonatomic, assign) int fontSize;

@end

@interface UIPrintPageRenderer (PDF)

- (NSData*) printToPDF;

@end

@implementation ViewController
@synthesize webview;

-(id)initWithHTML:(NSString*)HTML layout:(QPageLayout)layout fontSizeFactory:(int)fontSizeFactory {
    if (self = [super init]) {
        //self.successBlock = successBlock;
        self.HTML = HTML;
        self.pageLayout = layout;
        self.fontSize = fontSizeFactory;

        self.webview = [[WKWebView alloc] initWithFrame:[[self view] bounds]];
        self.webview.navigationDelegate = self;


        if (self.HTML == nil) {
            //sxNSLog(@"nil");
            [webview loadRequest:[NSURLRequest requestWithURL:self.URL]];
        }else{
            //NSLog(@"%@", self.HTML);
            [webview loadHTMLString:self.HTML baseURL:self.URL];
        }
        [self.view addSubview:webview];
    }
    return self;
}

- (void)viewDidLoad
{    
    [super viewDidLoad];        
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation {
    Q_UNUSED(webView);
    Q_UNUSED(navigation);
    NSLog(@"Loading failed!");
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    Q_UNUSED(navigation);

    if (webView.isLoading) return;

    NSLog(@"didFinishedNavigation");

    if (self.fontSize == 0) {
        [self printPage:webView];
    } else {
        NSString *script = [NSString stringWithFormat:
                @"document.documentElement.style.fontSize = (parseInt(window.getComputedStyle"
                "(document.documentElement).getPropertyValue('font-size') )+%d).toString()+'px'", self.fontSize];
        [webView evaluateJavaScript:script completionHandler:^(NSString *result, NSError *error) {
           NSLog(@"result: %@", result);
           NSLog(@"error: %@", error);
           [self printPage:webView];
        }];
    }
}

- (void) printPage:(WKWebView *)webView {
    UIPrintPageRenderer *render = [[UIPrintPageRenderer alloc] init];

    [render addPrintFormatter:webView.viewPrintFormatter startingAtPageAtIndex:0];
    //#define kPaperSizeA4 CGSizeMake(595.2,841.8)

    //self.pageSize = CGSizeMake(595.2, 841.8); //kPaperSizeA4; //
    //QPageLayout layout(QPageSize(QPageSize::A4), QPageLayout::Landscape, QMarginsF(0, 0, 0, 0));
    QSize s = self.pageLayout.pageSize().sizePoints();
    if (self.pageLayout.orientation() == QPageLayout::Landscape)
        s.transpose();
    qDebug() << s;
    self.pageSize = s.toCGSize();

     self.pageMargins = UIEdgeInsetsMake(0, 0, 0, 0);
    //self.pageMargins = UIEdgeInsetsMake(10, 5, 10, 5);

    CGRect printableRect = CGRectMake(self.pageMargins.left,
                                  self.pageMargins.top,
                                  self.pageSize.width - self.pageMargins.left - self.pageMargins.right,
                                  self.pageSize.height - self.pageMargins.top - self.pageMargins.bottom);

    CGRect paperRect = CGRectMake(0, 0, self.pageSize.width, self.pageSize.height);
    NSLog(@"%@", NSStringFromCGRect(paperRect));

    [render setValue:[NSValue valueWithCGRect:paperRect] forKey:@"paperRect"];
    [render setValue:[NSValue valueWithCGRect:printableRect] forKey:@"printableRect"];

    self.PDFdata = [render printToPDF];
    NSLog(@"didFinishedNavigation pdfdata");
    emit PrintUtil::getInstance()->pageReady();
}

@end

@implementation UIPrintPageRenderer (PDF)

- (NSData*) printToPDF
{
    NSMutableData *pdfData = [NSMutableData data];
    UIGraphicsBeginPDFContextToData( pdfData, self.paperRect, nil );
    [self prepareForDrawingPages: NSMakeRange(0, self.numberOfPages)];
    CGRect bounds = UIGraphicsGetPDFContextBounds();

    for ( int i = 0 ; i < self.numberOfPages ; i++ )
    {
        UIGraphicsBeginPDFPage();
        [self drawPageAtIndex: i inRect: bounds];
    }

    UIGraphicsEndPDFContext();
    return pdfData;
}

@end

ViewController *pdf;

PrintUtil* PrintUtil::mInstance = nullptr;

PrintUtil::PrintUtil(QObject *parent) : QObject(parent)
{
    mInstance = this;
    connect(this,&PrintUtil::pageReady,[=](){
        if (pdf != nil) {
            qDebug() << "PDF Print ready!";
            showPrint(QByteArray::fromNSData(pdf.PDFdata));
            [pdf dismissViewControllerAnimated:NO completion:nil];
        }
    });
}

void PrintUtil::printHTML(QString content, QPageLayout layout, int fontSizeFactory)
{
    NSString *htmlstring = content.toNSString();
    pdf = nil;
    pdf = [[ViewController alloc] initWithHTML:htmlstring layout:layout fontSizeFactory:fontSizeFactory];
}

void PrintUtil::printPDF(QByteArray b, QPageLayout layout)
{
    Q_UNUSED(layout);
    showPrint(b);
}

PrintUtil *PrintUtil::getInstance()
{
    if (!mInstance)
        mInstance = new PrintUtil();
    return mInstance;
}

void PrintUtil::showPrint(QByteArray data)
{

    UIPrintInteractionController *pc = [UIPrintInteractionController sharedPrintController];
    UIPrintInfo *printInfo = [UIPrintInfo printInfo];
    printInfo.outputType = UIPrintInfoOutputGeneral;
    printInfo.jobName = @"TheocBase Print";

    pc.printInfo = printInfo;
    pc.printingItem = data.toNSData();

    [pc presentAnimated:YES completionHandler:^(UIPrintInteractionController *printInteractionController, BOOL completed, NSError *error) {
        Q_UNUSED(completed);
        Q_UNUSED(error);
        [printInteractionController dismissAnimated:NO];
        [pc dismissAnimated:NO];
        printInteractionController = nil;
    }];
}
