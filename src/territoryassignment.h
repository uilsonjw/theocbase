/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2016, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TERRITORYASSIGNMENT_H
#define TERRITORYASSIGNMENT_H

#include <QObject>
#include <QAbstractTableModel>
#include <QDate>
#include "cpersons.h"
#include "sortfilterproxymodel.h"
#include "territory.h"

class TerritoryAssignment
{
public:
    TerritoryAssignment(int territoryId);
    TerritoryAssignment(const int id, const int territoryId, const int personId, const QString publisher, const QDate checkedOutDate, const QDate checkedBackInDate);

    int id() const;
    int territoryId() const;
    int personId() const;
    void setPersonId(int value);
    QString publisher() const;
    // void setPublisher(const QString &value);
    QDate checkedOutDate() const;
    void setCheckedOutDate(QDate value);
    QDate checkedBackInDate() const;
    void setCheckedBackInDate(QDate value);
    bool isDirty() { return m_isDirty; }
    void setIsDirty(bool value);
    bool save();

private:
    int m_id;
    int m_territoryId;
    int m_personId;
    QString m_publisher;
    QDate m_checkedOutDate;
    QDate m_checkedBackInDate;
    bool m_isDirty;
};

class TerritoryAssignmentModel : public QAbstractTableModel
{
    Q_OBJECT

    class MySortFilterProxyModel : public SortFilterProxyModel
    {
    public:
        enum MyRoles {
            id = Qt::UserRole + 1,
            name,
            date,
            icon,
            // hidden fields
            h_firstname,
            h_lastname,
            h_offset,
        };
        explicit MySortFilterProxyModel(QObject *parent = nullptr, QStandardItemModel *itemmodel = nullptr, int defaultSortRole = 0, QString settingName = "")
            : SortFilterProxyModel(parent, itemmodel, defaultSortRole, settingName) {}
        virtual bool lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const;
    };

public:
    enum Roles {
        AssignmentIdRole = Qt::UserRole,
        PersonIdRole,
        PublisherIndexRole,
        CheckedOutRole,
        CheckedBackInRole,
        PublisherRole
    };

    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

    TerritoryAssignmentModel();
    TerritoryAssignmentModel(QObject *parent);
    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    Q_INVOKABLE QVariantMap get(int row);
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    Q_INVOKABLE bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) Q_DECL_OVERRIDE;
    Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    Q_INVOKABLE QModelIndex getAssignmentIndex(int assignmentId) const;
    bool addAssignment(const TerritoryAssignment &territoryAssignment);
    Q_INVOKABLE bool addAssignment(int territoryId);
    Q_INVOKABLE void removeAssignment(int row);
    Q_INVOKABLE bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) Q_DECL_OVERRIDE;
    Q_INVOKABLE void loadAssignments(int territoryId);
    Q_INVOKABLE void saveAssignments();

    Q_INVOKABLE DataObjectListModel *getPublisherList() const;

private:
    QList<TerritoryAssignment> territoryAssignments;
    DataObjectListModel *m_publisherList;

signals:
    void notification();
    void editCompleted();
};

#endif // TERRITORYASSIGNMENT_H
