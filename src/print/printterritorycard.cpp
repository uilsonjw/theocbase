/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "printterritorycard.h"

PrintTerritoryCard::PrintTerritoryCard()
{
    currentTemplateData = QSharedPointer<TemplateData>(
            new TemplateData(TemplateData::TerritoryCardTemplate, "territoryCardTemplate", "TC-Map_OSM.htm", "tc-"));
}

QVariant PrintTerritoryCard::fillTemplate()
{
    QString str = getTemplate();
    str = initLayout(str);
    str = ifthen.tokenize(str);

    // titles
    str = fillTemplateBlockTerritoryOverall(str);
    str.replace("!TITLE!", tr("Territory Map Card", "Title tag for a S-12 or similar card"));
    str.replace("!TITLE_MAP!", tr("Territory Map", "Title tag for a sheet with a territory map"));
    str.replace("!TITLE_ADDRESSES!", tr("Address List", "Title tag for a sheet with a territory's address list"));
    str.replace("!TITLE_MAP_ADDRESSES!", tr("Territory Map with Address List", "Title tag for a sheet with a territory's map and address list"));
    str.replace("!TITLE_STREETS!", tr("Street List", "Title tag for a sheet with a territory's street list"));
    str.replace("!TITLE_MAP_STREETS!", tr("Territory Map with Street List", "Title tag for a sheet with a territory's map and street list"));
    str.replace("!TITLE_DNC!", tr("Do-Not-Call List", "Title tag for a sheet with a territory's Do-Not-Call list"));

    QString basepage = "";
    QString pagesec = "";
    QString tr_sec = "";
    QString ad_sec = "";
    QString st_sec = "";
    QPair<QString, QString> sec = templateGetSection(str, "!TERRITORY_START!", "!TERRITORY_END!", "territory_class");
    basepage = sec.first;
    tr_sec = sec.second;

    sql_items territories = sql->selectSql("SELECT t.*,tc.city FROM territories t"
                                           " LEFT JOIN territory_city tc ON t.city_id = tc.id"
                                           " WHERE territory_number in ("
                                           + territoryNumberList + ")");

    cpersons cp;
    QString pages = "";

    for (unsigned int i = 0; i < territories.size(); i++) {
        QString filledpage(tr_sec);

        filledpage.replace("!TR_TERRITORY_NUMBER!", territories[i].value("territory_number").toString());
        filledpage.replace("!TR_LOCALITY!", territories[i].value("locality").toString());
        filledpage.replace("!TR_TERRITORY_TYPE!", territories[i].value("type_name").toString());
        filledpage.replace("!TR_CITY!", territories[i].value("city").toString());
        filledpage.replace("!TR_DATE_LAST_WORKED!", territories[i].value("lastworked_date").toString());
        filledpage.replace("!TR_TERRITORY_REMARK!", territories[i].value("remark").toString());
        filledpage.replace("!TR_TERRITORY_GEOMETRY!", territories[i].value("wkt_geometry").toString());
        person *personAssignedTo = cp.getPerson(territories[i].value("person_id").toInt());
        QString assignedTo("");
        if (personAssignedTo)
            assignedTo = personAssignedTo->fullname();
        filledpage.replace("!TR_ASSIGNED_TO!", assignedTo);

        sec = templateGetSection(filledpage, "!REPEAT_START!", "!REPEAT_END!", "repeat_class");
        filledpage = sec.first;
        ad_sec = sec.second;

        sql_items addresses = sql->selectSql("SELECT * FROM territoryaddresses WHERE territory_id = " + territories[i].value("id").toString() + " ORDER BY city, street, housenumber, name");
        QString filledsection = "";
        for (unsigned int j = 0; j < addresses.size(); j++) {
            QString currentSection(ad_sec);
            currentSection = fillCommonItems(currentSection, false, ccongregation::None);

            currentSection.replace("!TA_COUNTRY!", addresses[j].value("country").toString());
            currentSection.replace("!TA_STATE!", addresses[j].value("state").toString());
            currentSection.replace("!TA_COUNTY!", addresses[j].value("county").toString());
            currentSection.replace("!TA_CITY!", addresses[j].value("city").toString());
            currentSection.replace("!TA_DISTRICT!", addresses[j].value("district").toString());
            currentSection.replace("!TA_STREET!", addresses[j].value("street").toString());
            currentSection.replace("!TA_HOUSENUMBER!", addresses[j].value("housenumber").toString());
            currentSection.replace("!TA_POSTALCODE!", addresses[j].value("postalcode").toString());
            currentSection.replace("!TA_POINT_GEOMETRY!", addresses[j].value("wkt_geometry").toString());
            currentSection.replace("!TA_NAME!", addresses[j].value("name").toString());
            currentSection.replace("!TA_ADDRESSTYPE_NUMBER!", addresses[j].value("addresstype_number").toString());
            currentSection.replace("!TA_ADDRESSTYPE_NAME!", addresses[j].value("addresstype_name").toString());
            currentSection.replace("!TA_ADDRESSTYPE_COLOR!", addresses[j].value("color").toString());

            filledsection.append(currentSection);
        }
        if (filledsection != "")
            filledpage.replace("<div class=\"repeat_class\"></div>", "<div class=\"repeat_class\">" + filledsection + "</div>");

        sec = templateGetSection(filledpage, "!STREET_START!", "!STREET_END!", "street_class");
        filledpage = sec.first;
        st_sec = sec.second;

        sql_items streets = sql->selectSql("SELECT * FROM territorystreets WHERE territory_id = " + territories[i].value("id").toString() + " ORDER BY street_name, from_number");
        filledsection = "";
        for (unsigned int j = 0; j < streets.size(); j++) {
            QString currentSection(st_sec);
            currentSection = fillCommonItems(currentSection, false, ccongregation::None);

            currentSection.replace("!TS_STREET!", streets[j].value("street_name").toString());
            currentSection.replace("!TS_FROM_NUMBER!", streets[j].value("from_number").toString());
            currentSection.replace("!TS_TO_NUMBER!", streets[j].value("to_number").toString());
            currentSection.replace("!TS_QUANTITY!", streets[j].value("quantity").toString());
            currentSection.replace("!TS_LINE_GEOMETRY!", streets[j].value("wkt_geometry").toString());
            currentSection.replace("!TS_STREETTYPE_NAME!", streets[j].value("streettype_name").toString());
            currentSection.replace("!TS_STREETTYPE_COLOR!", streets[j].value("color").toString());

            filledsection.append(currentSection);
        }
        if (filledsection != "")
            filledpage.replace("<div class=\"street_class\"></div>", "<div class=\"street_class\">" + filledsection + "</div>");

        pages.append(filledpage);
    }

    basepage.replace("<div class=\"territory_class\"></div>", "<div class=\"territory_class\">" + pages + "</div>");
    return ifthen.compute(basepage);
}

void PrintTerritoryCard::setTerritoryNumberList(QString numbers)
{
    territoryNumberList = numbers;
}
