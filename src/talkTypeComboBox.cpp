#include "talkTypeComboBox.h"

talkTypeComboBox::talkTypeComboBox(QObject *parent)
    : QItemDelegate(parent)
{

}

QWidget *talkTypeComboBox::createEditor(QWidget *parent,
                                          const QStyleOptionViewItem &option,
                                          const QModelIndex &index) const
{
    Q_UNUSED(option);
    Q_UNUSED(index);
    QComboBox *editor = new QComboBox(parent);

    editor->addItem(LMM_Schedule::getFullStringTalkType(LMM_Schedule::TalkType_Treasures));
    editor->addItem(LMM_Schedule::getFullStringTalkType(LMM_Schedule::TalkType_Digging));
    editor->addItem(LMM_Schedule::getFullStringTalkType(LMM_Schedule::TalkType_BibleReading));
    editor->addItem(LMM_Schedule::getFullStringTalkType(LMM_Schedule::TalkType_SampleConversationVideo));
    editor->addItem(LMM_Schedule::getFullStringTalkType(LMM_Schedule::TalkType_ApplyYourself));
    editor->addItem(LMM_Schedule::getFullStringTalkType(LMM_Schedule::TalkType_InitialCall));
    editor->addItem(LMM_Schedule::getFullStringTalkType(LMM_Schedule::TalkType_ReturnVisit1));
    editor->addItem(LMM_Schedule::getFullStringTalkType(LMM_Schedule::TalkType_ReturnVisit2));
    editor->addItem(LMM_Schedule::getFullStringTalkType(LMM_Schedule::TalkType_ReturnVisit3));
    editor->addItem(LMM_Schedule::getFullStringTalkType(LMM_Schedule::TalkType_BibleStudy));
    editor->addItem(LMM_Schedule::getFullStringTalkType(LMM_Schedule::TalkType_StudentTalk));
    editor->addItem(LMM_Schedule::getFullStringTalkType(LMM_Schedule::TalkType_LivingTalk1));
    editor->addItem(LMM_Schedule::getFullStringTalkType(LMM_Schedule::TalkType_LivingTalk2));
    editor->addItem(LMM_Schedule::getFullStringTalkType(LMM_Schedule::TalkType_LivingTalk3));
    editor->addItem(LMM_Schedule::getFullStringTalkType(LMM_Schedule::TalkType_CBS));
    editor->addItem(LMM_Schedule::getFullStringTalkType(LMM_Schedule::TalkType_COTalk));
    editor->addItem(LMM_Schedule::getFullStringTalkType(LMM_Schedule::TalkType_MemorialInvitation));
    editor->addItem(LMM_Schedule::getFullStringTalkType(LMM_Schedule::TalkType_OtherFMVideoPart));
    QTimer::singleShot(0, editor, &QComboBox::showPopup);
    return editor;
}

void talkTypeComboBox::setEditorData(QWidget *editor,
                                       const QModelIndex &index) const
{
    QComboBox *cb = static_cast<QComboBox*>(editor);
    cb->setCurrentText(index.model()->data(index, Qt::EditRole).toString());
    //cb->showPopup();
}

void talkTypeComboBox::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QComboBox *cb = static_cast<QComboBox*>(editor);
    model->setData(index, cb->currentText(), Qt::EditRole);
}

void talkTypeComboBox::updateEditorGeometry(QWidget *editor,
                                              const QStyleOptionViewItem &option,
                                              const QModelIndex &/* index */) const
{
    editor->setGeometry(option.rect);
}


talkTypeTextEdit::talkTypeTextEdit(QObject *parent)
    : QItemDelegate(parent)
{

}

QWidget *talkTypeTextEdit::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(option);
    Q_UNUSED(index);
    return new QTextEdit(parent);
}

void talkTypeTextEdit::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QTextEdit *txt = static_cast<QTextEdit*>(editor);
    txt->setHtml("<HTML><BODY>"+ index.model()->data(index, Qt::EditRole).toString().replace("\r\n", "<br>").replace("\n", "<br>") +"</BODY></HTML>");
}

void talkTypeTextEdit::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QTextEdit *txt = static_cast<QTextEdit*>(editor);
    model->setData(index, txt->toPlainText(), Qt::EditRole);
}

void talkTypeTextEdit::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &) const
{
    editor->setGeometry(option.rect);
}
