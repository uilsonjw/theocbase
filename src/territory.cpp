/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2016, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "territory.h"

territory::territory(QObject *parent)
    : QObject(parent), m_territoryId(-1), m_territoryNumber(-1), m_locality(""), m_cityId(-1), m_typeId(-1), m_priority(-1), m_remark(""), m_wktGeometry(""), m_isDirty(false), m_uuid("")
{
}

territory::territory(int territoryId, QString uuid, QObject *parent)
    : QObject(parent), m_territoryId(territoryId), m_territoryNumber(-1), m_locality(""), m_cityId(-1), m_typeId(-1), m_priority(-1), m_remark(""), m_wktGeometry(""), m_isDirty(false), m_uuid(uuid)
{
}

territory::~territory()
{
}

void territory::setTerritoryNumber(int value)
{
    int newNumber = value;

    if (newNumber < 0) {
        newNumber = 1;
        sql_class *sql = &Singleton<sql_class>::Instance();
        sql_items items = sql->selectSql("SELECT MAX(territory_number) AS maxNum FROM territory");
        if (items.size() > 0) {
            sql_item s = items[0];
            int maxvalue = s.value("maxNum").toInt();
            newNumber = maxvalue + 1;
        }
    } else if (m_territoryNumber == newNumber)
        return;

    m_territoryNumber = newNumber;
    setIsDirty(true);
    emit territoryNumberChanged(m_territoryId, m_territoryNumber);
}

void territory::setLocality(QString value)
{
    if (m_locality == value)
        return;
    m_locality = value;
    setIsDirty(true);
    emit localityChanged(m_territoryId, m_locality);
}

void territory::setCityId(int value)
{
    if (m_cityId == value)
        return;
    m_cityId = value;
    setIsDirty(true);
    emit cityChanged(m_territoryId, m_cityId);
}

void territory::setTypeId(int value)
{
    if (m_typeId == value)
        return;
    m_typeId = value;
    setIsDirty(true);
    emit typeChanged(m_territoryId, m_typeId);
}

void territory::setPriority(int value)
{
    if (m_priority == value)
        return;
    m_priority = value;
    setIsDirty(true);
}

void territory::setRemark(QString value)
{
    if (m_remark == value)
        return;
    m_remark = value;
    setIsDirty(true);
}

void territory::setWktGeometry(QString value)
{
    if (m_wktGeometry == value)
        return;
    m_wktGeometry = value;
    setIsDirty(true);
}

OGRGeometry *territory::getOGRGeometry()
{
    //    OGRSpatialReference osr;
    //    osr.SetWellKnownGeogCS("EPSG:4326");
    OGRGeometry *territoryGeometry = nullptr;

    // Parse WKT
    QByteArray bytes = wktGeometry().toUtf8();
    const char *data = bytes.constData();
    OGRErr err = OGRGeometryFactory::createFromWkt(data, nullptr, &territoryGeometry);
    if (err != OGRERR_NONE) {
        // process error, like emit signal
        return nullptr;
    }
    if (!territoryGeometry->IsValid()) {
        qDebug() << "Saved WKT geometry of territory" << territoryNumber() << "is invalid!";
    }
    return territoryGeometry;
}

bool territory::setOGRGeometry(OGRGeometry *geometry)
{
    char *txt;
    if (geometry != nullptr) {
        if (!geometry->IsEmpty() && !geometry->IsValid()) {
            qDebug() << "Geometry of territory" << territoryNumber() << "cannot be set because it is not valid!";
            return false;
        }
        if (geometry->exportToWkt(&txt) == OGRERR_NONE) {
            // test geometry before saving
            OGRSpatialReference osr;
            OGRGeometry *testGeometry = nullptr;

            // Parse WKT
            OGRErr err = OGRGeometryFactory::createFromWkt(txt, &osr, &testGeometry);
            if (err != OGRERR_NONE) {
                // process error, like emit signal
                qDebug() << "Geometry check for territory" << territoryNumber() << "failed!";
                return false;
            }
            if (!testGeometry->IsValid())
                testGeometry = testGeometry->Buffer(-0.001);
            if (!testGeometry->IsValid()) {
                qDebug() << "Geometry check for territory" << territoryNumber() << "failed because it is invalid!";
                return false;
            }

            qDebug() << "Geometry of territory" << territoryNumber() << "is valid. Set WKT geometry.";
            setWktGeometry(QString(txt));
            CPLFree(txt);
            return true;
        }
    }
    return false;
}

bool territory::crosses(QVariantList path)
{
    OGRGeometry *territoryGeometry = getOGRGeometry();
    if (territoryGeometry != nullptr && territoryGeometry->IsEmpty())
        return true;

    if (territoryGeometry == nullptr || territoryGeometry->IsEmpty() || !territoryGeometry->IsValid())
        return false;

    OGRPolygon polygon;
    OGRLinearRing poRing;
    for (int i = 0; i < path.length(); i++) {
        QGeoCoordinate c = path[i].value<QGeoCoordinate>();

        // check if point is within territory geometry, because check via Crosses doesn't work yet
        // nevertheless point check is not enough, since a line segment can also cross the territory
        OGRPoint pt;
        pt.setX(c.longitude());
        pt.setY(c.latitude());
        if (pt.Within(territoryGeometry))
            return true;

        poRing.addPoint(c.longitude(), c.latitude());
    }
    poRing.closeRings();
    polygon.addRing(&poRing);
    OGRGeometry *testGeometry = polygon.getLinearGeometry();
    if (territoryGeometry->Intersects(testGeometry)) {
        OGRGeometry *intersection = territoryGeometry->Intersection(testGeometry);
        if (intersection->getGeometryType() == wkbPolygon || intersection->getGeometryType() == wkbMultiPolygon)
            return true;
        if (intersection->getGeometryType() == wkbGeometryCollection) {
            OGRGeometryCollection *poColl = intersection->toGeometryCollection();
            for (auto &&poSubGeom : *poColl) {
                switch (poSubGeom->getGeometryType()) {
                case wkbPolygon:
                    return true;
                case wkbMultiPolygon:
                    return true;
                default:
                    break;
                }
            }
        }
    }
    return false;
}

void territory::setIsDirty(bool value)
{
    m_isDirty = value;
}

int territory::getNewNumber()
{
    int newNumber = 1;
    sql_class *sql = &Singleton<sql_class>::Instance();
    sql_items items = sql->selectSql("SELECT MAX(territory_number) FROM territory");
    if (items.size() > 0) {
        sql_item s = items[0];
        int maxvalue = s.value(nullptr).toInt();
        newNumber = maxvalue + 1;
    }

    return newNumber;
}

bool territory::loadTerritory(int territoryId)
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    sql_item queryitem;
    queryitem.insert(":id", territoryId);
    sql_items items = sql->selectSql("SELECT * FROM territory WHERE id = :id and active",
                                     &queryitem);
    if (items.size() > 0) {
        sql_item s = items[0];
        m_territoryId = s.value("id").toInt();
        m_territoryNumber = s.value("territory_number").toInt();
        m_locality = s.value("locality").toString();

        m_cityId = s.value("city_id").toInt();
        // check for missing city
        sql_item queryitem_city;
        queryitem_city.insert(":id", m_cityId);
        sql_items cityItems = sql->selectSql("SELECT * FROM territory_city WHERE id = :id and active",
                                             &queryitem_city);
        if (m_cityId != -1 && cityItems.size() == 0) {
            m_cityId = -1;
            m_isDirty = true;
        }

        m_typeId = s.value("type_id").toInt();
        // check for missing territory type
        sql_item queryitem_type;
        queryitem_type.insert(":id", m_typeId);
        sql_items typeItems = sql->selectSql("SELECT * FROM territory_type WHERE id = :id and active",
                                             &queryitem_type);
        if (m_typeId != -1 && typeItems.size() == 0) {
            m_typeId = -1;
            m_isDirty = true;
        }

        m_priority = s.value("priority").toInt();
        m_remark = s.value("remark").toString();
        m_wktGeometry = s.value("wkt_geometry").toString();

        if (m_isDirty)
            save();

        m_isDirty = false;
        return true;
    }
    return false;
}

bool territory::remove()
{

    sql_class *sql = &Singleton<sql_class>::Instance();

    sql_item s;
    s.insert(":id", m_territoryId);
    s.insert(":active", 0);
    return sql->execSql("UPDATE territory SET active = :active WHERE id = :id", &s, true);
}

bool territory::save()
{
    if (territoryNumber() < 1)
        return false;

    // save changes to database
    if (!isDirty())
        return true;
    sql_class *sql = &Singleton<sql_class>::Instance();
    int lang_id = sql->getLanguageDefaultId();

    sql_item queryitems;
    queryitems.insert(":id", m_territoryId);
    int id = m_territoryId > 0 ? sql->selectScalar("SELECT id FROM territory WHERE id = :id", &queryitems, -1).toInt() : -1;

    sql_item insertItems;
    insertItems.insert("territory_number", territoryNumber());
    insertItems.insert("locality", locality());
    insertItems.insert("city_id", cityId());
    insertItems.insert("type_id", typeId());
    insertItems.insert("priority", priority());
    insertItems.insert("remark", remark());
    insertItems.insert("wkt_geometry", wktGeometry());
    insertItems.insert("lang_id", lang_id);

    bool ret = false;
    if (id > 0) {
        // update
        ret = sql->updateSql("territory", "id", QString::number(id), &insertItems);
    } else {
        // insert new row
        int newId = sql->insertSql("territory", &insertItems, "id");
        ret = newId > 0;
        if (newId > 0)
            m_territoryId = newId;
    }
    setIsDirty(!ret);

    return ret;
}

territory_type::territory_type(int id, QString typeName, QObject *parent)
    : QObject(parent), m_territoryTypeId(id), m_territoryTypeName(typeName)
{
}

territory_type::~territory_type()
{
}

DataObject::DataObject(const int &id, const QString &name)
    : m_id(id), m_name(name), m_color(""), m_isValid(true)
{
}

DataObject::DataObject(const int &id, const QString &name, const QString &color)
    : m_id(id), m_name(name), m_color(color), m_isValid(true)
{
}

DataObject::DataObject(const int &id, const QString &name, const QString &color, const bool isValid)
    : m_id(id), m_name(name), m_color(color), m_isValid(isValid)
{
}

int DataObject::id() const
{
    return m_id;
}

QString DataObject::name() const
{
    return m_name;
}

QString DataObject::color() const
{
    return m_color;
}

bool DataObject::isValid() const
{
    return m_isValid;
}

DataObjectListModel::DataObjectListModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

QHash<int, QByteArray> DataObjectListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[IdRole] = "id";
    roles[NameRole] = "name";
    roles[ColorRole] = "color";
    roles[IsValid] = "isValid";
    return roles;
}

void DataObjectListModel::addDataObject(const DataObject &dataObject)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_dataObjects << dataObject;
    endInsertRows();
}

void DataObjectListModel::addDataObject(const int &id, const QString &name, const QString &color, const bool isValid)
{
    DataObject newDataObject(id, name, color, isValid);
    addDataObject(newDataObject);
}

int DataObjectListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_dataObjects.count();
}

QVariant DataObjectListModel::data(const QModelIndex &index, int role) const
{
    int row = index.row();

    if (row < 0 || row > m_dataObjects.count())
        return QVariant();

    const DataObject dataObject = m_dataObjects[index.row()];

    switch (role) {
    case IdRole:
        return dataObject.id();
    case NameRole:
        return dataObject.name();
    case ColorRole:
        return dataObject.color();
    case IsValid:
        return dataObject.isValid();
    }

    return QVariant();
}

int DataObjectListModel::getId(int index) const
{
    if (index < 0 || index > m_dataObjects.count())
        return -1;
    const DataObject dataObject = m_dataObjects[index];
    return dataObject.id();
}

QVariantMap DataObjectListModel::get(int row)
{
    QHash<int, QByteArray> names = roleNames();
    QHashIterator<int, QByteArray> i(names);
    QVariantMap res;
    QModelIndex idx = index(row, 0);
    while (i.hasNext()) {
        i.next();
        QVariant data = idx.data(i.key());
        res[i.value()] = data;
    }
    return res;
}

int DataObjectListModel::getIndex(int id) const
{
    int rows = rowCount();

    for (int i = 0; i < rows; i++) {
        QModelIndex idx = index(i, 0);
        if (id == idx.data(IdRole).toInt()) {
            return i;
        }
    }
    return -1;
}

int DataObjectListModel::getIndex(QString name) const
{
    int rows = rowCount();

    for (int i = 0; i < rows; i++) {
        QModelIndex idx = index(i, 0);
        if (name == idx.data(NameRole).toString()) {
            return i;
        }
    }
    return -1;
}
