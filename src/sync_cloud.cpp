#include "sync_cloud.h"

sync_cloud::sync_cloud(QObject *parent)
    : QObject(parent)
{
    sql = &Singleton<sql_class>::Instance();

    m_changeid = sql->getIntSetting("last_dbsync_id", 0);
}

void sync_cloud::setAuthentication(dropbox *db)
{
    mDropbox = db;
}

void sync_cloud::sync(bool ignoredbuser)
{
    // reset values
    //sql->saveSetting("last_dbsync_id","0");
    //sql->saveSetting("last_dbsync_time","-1");
    m_changeid = sql->getIntSetting("last_dbsync_id", 0);
    mError = false;
    mResetStarted = false;
    mResetTime = 0;

    mSyncFileModified = sql->getIntSetting("last_dbsyncfile_time", 0);

    if (!mDropbox->logged()) {
        qDebug() << "not logged in";
        mDropbox->authenticate();
        emit ready();
        return;
    }

    mDropbox->authenticate();
    m_useremail = mDropbox->getAccountInfo()->getEmail();
    mCloudPath = mDropbox->getAccountInfo()->getSyncFile();
    if (mCloudPath.isEmpty()) {
        emit error("Dropbox syncfile has not been set!");
        return;
    }

    QString last_db_user = sql->getSetting("last_db_user", "");
    if (m_useremail != last_db_user && last_db_user.length() > 0) {
        qDebug() << m_useremail << last_db_user;
        if (!ignoredbuser) {
            emit differentLastDbUser();
            return;
        }
        // reset sync id
        sql->saveSetting("last_dbsync_id", "0");
        sql->saveSetting("last_dbsync_time", "-1");
    }

    if (!QSslSocket::supportsSsl()) {
        emit error("Your installation doesn't support SSL.\n"
                   "Install the OpenSSL run-time libraries.");
        return;
    }

    qDebug() << "start";
    emit progressed(20);

    // Get local changeset
    getLocalChangeset(mLocalChangeSet, mLocalUnpermittedChangeSet);
    qDebug() << "localchangeset" << mLocalChangeSet.count() << mLocalUnpermittedChangeSet.count();
    emit progressed(40);
    // Get remote changeset(s)
    getChangesFromCloud();
}

void sync_cloud::continueSync(bool keepLocalChanges)
{
    emit progressed(20);
    // reset unpermitted changes
    if (mLocalUnpermittedOriginalSet.count() > 0)
        saveChangesToDatabase(mLocalUnpermittedOriginalSet);
    if (mLocalUnpermittedDeleteSet.count() > 0)
        deleteChangesFromDatabase(mLocalUnpermittedDeleteSet);

    // resolve conflicts
    for (auto value : mDuplicateValues) {
        if (keepLocalChanges) {
            // remove items from remote change set
            if (mRemoteChangeSet1.contains(value)) {
                mRemoteChangeSet1.remove(value);
            } else if (mRemoteChangeSet2.contains(value)) {
                mRemoteChangeSet2.remove(value);
            } else {
                mRemoteChangeSet3.remove(value);
            }
        } else {
            // remove items from local change set
            mLocalChangeSet.remove(value);
        }
    }
    emit progressed(40);
    // Push changes
    if (mLocalChangeSet.count() > 0) {
        QHash<QString, QJsonArray> tablelist;
        QJsonArray tablearray;

        m_changeid++;

        for (auto item : mLocalChangeSet) {
            item.insert("changeid", QVariant(m_changeid).toString());
            mAllRows.insert(item.value("uuid").toString(), item);
        }

        for (auto item : mAllRows) {
            QString tablename = "";
            QJsonObject fobjs;
            QHashIterator<QString, QVariant> i(item);
            while (i.hasNext()) {
                i.next();
                if (i.key() == "id" || i.key() == "time_stamp")
                    continue;
                if (i.key() == "tablename") {
                    tablename = i.value().toString();
                    continue;
                }
                fobjs[i.key()] = i.value().toString();
            }

            if (tablelist.contains(tablename)) {
                tablearray = tablelist.value(tablename);
            } else {
                tablearray = QJsonArray();
            }

            tablearray.append(fobjs);
            tablelist.insert(tablename, tablearray);
        }

        qDebug() << "tablelist.count" << tablelist.count();

        QJsonArray rootArray;
        QHashIterator<QString, QJsonArray> i2(tablelist);
        while (i2.hasNext()) {
            i2.next();
            QJsonObject tableObject;
            tableObject["table"] = i2.key();
            tableObject["rows"] = i2.value();
            rootArray.append(tableObject);
        }
        QJsonObject rootObject;
        rootObject["tables"] = rootArray;
        rootObject["sync_id"] = QVariant(m_changeid).toString();
        rootObject["version_id"] = QVariant(versionnumber).toString();
        rootObject["reset_time"] = QVariant(mResetTime).toString();

        QJsonDocument doc(rootObject);

        qDebug() << "Localchangeset is ready to send"; // << doc;

        pushChangesToCloud(doc);
    } else {
        saveChanges();
    }
}

bool sync_cloud::cloudUpdateAvailable()
{
    mSyncFileModified = sql->getIntSetting("last_dbsyncfile_time", 0);
    mCloudPath = mDropbox->getAccountInfo()->getSyncFile();
    QDateTime dt = mDropbox->getModifiedDate(mCloudPath);
    qDebug() << dt.toSecsSinceEpoch() << mSyncFileModified;

    return mSyncFileModified < dt.toSecsSinceEpoch();
}

void sync_cloud::resetCloudData()
{
    mResetStarted = true;
    if (mCloudPath.isEmpty())
        mCloudPath = mDropbox->getAccountInfo()->getSyncFile();
    getChangesFromCloud();
    m_changeid++;

    QJsonObject rootObject;
    rootObject["tables"] = "[]";
    rootObject["sync_id"] = QVariant(m_changeid).toString();
    rootObject["version_id"] = QVariant(versionnumber).toString();
    rootObject["reset_time"] = QVariant(QDateTime::currentDateTime().toSecsSinceEpoch()).toString();

    QJsonDocument doc(rootObject);

    QDateTime dt = mDropbox->upload(doc.toJson(QJsonDocument::Compact), mCloudPath);
    if (dt.isValid()) {
        mSyncFileModified = QVariant(dt.toSecsSinceEpoch()).toInt();
        sql->saveSetting("last_dbsync_id", "0");
        sql->saveSetting("last_dbsync_time", "-1");
        sql->deleteNonActive();
        mResetStarted = false;
        emit cloudResetReady();
    }
}

void sync_cloud::runTest()
{
    getChangesFromCloud();
}

void sync_cloud::getChangesFromCloud()
{

    QJsonDocument d;
    QByteArray b;
    QDateTime dt;

    mDropbox->download(mCloudPath, b, dt);

    if (b.size() > 0) {
        QJsonParseError err;
        d = QJsonDocument::fromJson(b, &err);
        if (err.error != QJsonParseError::NoError) {
            mError = true;
            emit error("Json parse error: " + err.errorString());
            return;
        }
    }
    if (dt.isValid())
        mSyncFileModified = QVariant(dt.toSecsSinceEpoch()).toInt();
    getChangesFromCloudFinished(d);
}

void sync_cloud::getChangesFromCloudFinished(QJsonDocument doc)
{
    mRemoteChangeSet1.clear();
    mRemoteChangeSet2.clear();
    mRemoteChangeSet3.clear();
    mAllRows.clear();

    QJsonDocument d = doc;

    if (!d.isEmpty()) {
        int version_id = QVariant(d.object().value("version_id").toString()).toInt();
        if (version_id > versionnumber) {
            mError = true;
            emit error(tr("Version conflict: The cloud changes have been made with a newer version!"));
            return;
        }

        if (version_id > 0 && version_id < versionnumber && mLocalUnpermittedChangeSet.count() > 0) {
            mError = true;
            emit error(tr("Version conflict: The cloud data needs to be updated with the same version by an authorized user."));
            return;
        }

        int changeid = QVariant(d.object().value("sync_id").toString()).toInt();
        uint lastDbSyncTime = QVariant(sql->getSetting("last_dbsync_time")).toUInt();
        mResetTime = QVariant(d.object().value("reset_time").toString()).toUInt();

        if (mResetTime > 0 && m_changeid > 0 && mResetTime > lastDbSyncTime) {
            // the cloud data reset after the last sync
            emit cloudResetFound();
            return;
        }

        if (d.object().value("tables").toArray().isEmpty()) {
            qDebug() << "No changes from cloud";
        } else {
            QJsonArray a = d.object().value("tables").toArray();

            for (QJsonValue dataValue : a) {
                QJsonObject dataObject = dataValue.toObject();

                QJsonArray rowsArray = dataObject.value("rows").toArray();
                QString table = dataObject.value("table").toString();

                for (QJsonValue v : rowsArray) {
                    QJsonObject o = v.toObject();

                    QString uuId = o.value("uuid").toString();

                    // do not add if already in remotechangeset
                    if (mRemoteChangeSet1.contains(uuId) || mRemoteChangeSet2.contains(uuId) || mRemoteChangeSet3.contains(uuId))
                        continue;

                    sql_item item;
                    item.insert("tablename", table);

                    for (QString key : o.keys()) {
                        item.insert(key, o.value(key).toString());
                    }

                    mAllRows.insert(uuId, item);

                    // add only items with the new changeid
                    if (QVariant(o.value("changeid").toString()).toInt() <= m_changeid)
                        continue;

                    item.remove("changeid");
                    // items can be in random order but some tables are needed before anything else
                    if (table == "congregations" || table == "territory_city" || table == "territory_type") {
                        mRemoteChangeSet1.insert(uuId, item);
                    } else if (table == "persons" || table == "lmm_schedule" || table == "publictalks" || table == "territory") {
                        mRemoteChangeSet2.insert(uuId, item);
                    } else if (table != "languages") {
                        mRemoteChangeSet3.insert(uuId, item);
                    }
                }
            }
        }
        if (changeid > m_changeid)
            m_changeid = changeid;
    } else {
        qDebug() << "Invalid JSON message!";
    }
    if (!mResetStarted)
        compareChangeSets();
}

void sync_cloud::pushChangesToCloud(QJsonDocument doc)
{
    QDateTime dt = mDropbox->upload(doc.toJson(QJsonDocument::Compact), mCloudPath);
    if (dt.isValid()) {
        mSyncFileModified = QVariant(dt.toSecsSinceEpoch()).toInt();
        pushChangesToCloudFinished();
    } else {
        mError = true;
        emit error("Dropbox API error:\n" + mDropbox->errorString());
    }
}

void sync_cloud::pushChangesToCloudFinished()
{
    saveChanges();
}

void sync_cloud::networkError(QNetworkReply::NetworkError errorcode)
{
    mError = true;
    QNetworkReply *reply = qobject_cast<QNetworkReply *>(sender());
    emit error(QString("Network error!\nCode: %1, description: %2").arg(QVariant(errorcode).toString(), reply ? reply->errorString() : ""));
    reply->deleteLater();
}

void sync_cloud::getLocalChangeset(QHash<QString, sql_item> &localChangeset, QHash<QString, sql_item> &localUnpermittedChangeset)
{
    QString timestamp = sql->getSetting("last_dbsync_time", "-1");

    QList<QPair<QPair<bool, QString>, QString>> tables;

    AccessControl *ac = &Singleton<AccessControl>::Instance();
    User *user = ac->user();

    // tables and SQL clauses to query changes
    tables.append(qMakePair(qMakePair(user->hasPermission(PermissionRule::CanEditPublishers) || user->hasPermission(PermissionRule::CanEditPublicSpeakers), QString("persons")),
                            QString("SELECT id,firstname,lastname,servant,elder,publisher,gender,usefor,info,email,phone,mobile,active,uuid,"
                                    "ifnull((SELECT uuid FROM congregations WHERE id = congregation_id),-1) AS congregation_id FROM persons")));
    tables.append(qMakePair(qMakePair(user->hasPermission(PermissionRule::CanEditWeekendMeetingSchedule), QString("publictalks_todolist")), QString("SELECT * FROM publictalks_todolist")));
    tables.append(qMakePair(qMakePair(user->hasPermission(PermissionRule::CanEditCongregationSettings) || user->hasPermission(PermissionRule::CanEditPublicSpeakers), QString("congregations")), QString("SELECT * FROM congregations")));
    //tables.append(qMakePair(QString("languages"),QString("SELECT * FROM languages")));

    tables.append(qMakePair(qMakePair(user->hasPermission(PermissionRule::CanEditMidweekMeetingSettings), QString("studies")), QString("SELECT * FROM studies")));

    tables.append(qMakePair(qMakePair(user->hasPermission(PermissionRule::CanEditMidweekMeetingSchedule), QString("school_break")), QString("SELECT * FROM school_break")));
    tables.append(qMakePair(qMakePair(user->hasPermission(PermissionRule::CanEditMidweekMeetingSchedule), QString("school_settings")), QString("SELECT * FROM school_settings")));

    tables.append(qMakePair(qMakePair(user->hasPermission(PermissionRule::CanEditSpecialEvents), QString("exceptions")), QString("SELECT * FROM exceptions")));
    tables.append(qMakePair(qMakePair(user->hasPermission(PermissionRule::CanEditMidweekMeetingSchedule), QString("student_studies")),
                            QString("SELECT id,study_number, start_date, end_date, exercises, active, uuid,"
                                    "ifnull((SELECT uuid FROM persons WHERE id = student_id),-1) AS student_id FROM student_studies")));
    tables.append(qMakePair(qMakePair(user->hasPermission(PermissionRule::CanEditCongregationSettings) || user->hasPermission(PermissionRule::CanEditPublicSpeakers), QString("congregationmeetingtimes")),
                            QString("SELECT id,mtg_year,mtg_day,mtg_time,"
                                    "ifnull((SELECT uuid FROM congregations WHERE id = congregation_id),-1) AS congregation_id FROM congregationmeetingtimes")));
    tables.append(qMakePair(qMakePair(user->hasPermission(PermissionRule::CanEditCongregationSettings) || user->hasPermission(PermissionRule::CanEditPublicSpeakers), QString("speaker_publictalks")),
                            QString("SELECT id,active,uuid,"
                                    "ifnull((SELECT uuid FROM publictalks WHERE id = theme_id),-1) AS theme_id,"
                                    "ifnull((SELECT uuid FROM persons WHERE id = speaker_id),-1) AS speaker_id,"
                                    "ifnull((SELECT uuid FROM languages WHERE id = lang_id),-1) AS lang_id FROM speaker_publictalks")));
    tables.append(qMakePair(qMakePair(user->hasPermission(PermissionRule::CanEditCongregationSettings) || user->hasPermission(PermissionRule::CanEditPublicSpeakers), QString("publictalks")),
                            QString("SELECT id,theme_number, theme_name, active, uuid,revision,release_date,discontinue_date,"
                                    "ifnull((SELECT uuid FROM languages WHERE id = lang_id),-1) AS lang_id FROM publictalks")));

    tables.append(qMakePair(qMakePair(user->hasPermission(PermissionRule::CanEditAvailabilities), QString("unavailables")),
                            QString("SELECT id,start_date, end_date, active, uuid,"
                                    "ifnull((SELECT uuid FROM persons WHERE id = person_id),-1) AS person_id FROM unavailables")));
    tables.append(qMakePair(qMakePair(user->hasPermission(PermissionRule::CanEditCongregationSettings) || user->hasPermission(PermissionRule::CanEditPublicSpeakers), QString("families")),
                            QString("SELECT id, ifnull((SELECT uuid FROM persons WHERE id = person_id),-1) AS person_id,"
                                    "ifnull((SELECT uuid FROM persons WHERE id = family_head),-1) AS family_head FROM families")));
    tables.append(qMakePair(qMakePair(user->hasPermission(PermissionRule::CanEditPublicSpeakers), QString("outgoing")),
                            QString("SELECT id, date,active,uuid,"
                                    "ifnull((SELECT uuid FROM publictalks WHERE id = theme_id),-1) AS theme_id,"
                                    "ifnull((SELECT uuid FROM persons WHERE id = speaker_id),-1) AS speaker_id,"
                                    "ifnull((SELECT uuid FROM congregations WHERE id = congregation_id),-1) AS congregation_id FROM outgoing")));

    tables.append(qMakePair(qMakePair(user->hasPermission(PermissionRule::CanEditCongregationSettings), QString("settings")),
                            QString("SELECT * FROM (SELECT * FROM settings WHERE "
                                    "name = 'school_day' OR "
                                    "name = 'circuitoverseer')")));
    tables.append(qMakePair(qMakePair(user->hasPermission(PermissionRule::CanEditMidweekMeetingSettings), QString("settings")),
                            QString("SELECT * FROM (SELECT * FROM settings WHERE "
                                    "name = 'schools_qty' OR "
                                    "(name LIKE 'Talk Type:%' AND value != -1))")));

    tables.append(qMakePair(qMakePair(user->hasPermission(PermissionRule::CanEditWeekendMeetingSchedule), QString("publicmeeting")),
                            QString("SELECT id,date,"
                                    "ifnull((SELECT uuid FROM publictalks WHERE id = theme_id),-1) AS theme_id,"
                                    "ifnull((SELECT uuid FROM persons WHERE id = speaker_id),-1) AS speaker_id,"
                                    "ifnull((SELECT uuid FROM persons WHERE id = wtreader_id),-1) AS wtreader_id,"
                                    "ifnull((SELECT uuid FROM persons WHERE id = chairman_id),-1) AS chairman_id,"
                                    "ifnull((SELECT uuid FROM persons WHERE id = wt_conductor_id),-1) AS wt_conductor_id,"
                                    "ifnull((SELECT uuid FROM persons WHERE id = hospitality_id),-1) AS hospitality_id,"
                                    "ifnull((SELECT uuid FROM persons WHERE id = final_prayer_id),-1) AS final_prayer_id,"
                                    "wt_source,song_pt,wt_theme,"
                                    "song_wt_start,song_wt_end,final_talk,active,uuid "
                                    "FROM publicmeeting")));
    tables.append(qMakePair(qMakePair(user->hasPermission(PermissionRule::CanEditMidweekMeetingSchedule)
                                              || user->hasPermission(PermissionRule::CanEditMidweekMeetingSettings),
                                      QString("lmm_schedule")),
                            QString("SELECT id,date,talk_id,theme,source,time,study_number,roworder,active,uuid "
                                    "FROM lmm_schedule")));
    tables.append(qMakePair(qMakePair(user->hasPermission(PermissionRule::CanEditMidweekMeetingSchedule)
                                              || user->hasPermission(PermissionRule::CanEditMidweekMeetingSettings),
                                      QString("lmm_meeting")),
                            QString("SELECT id,date,"
                                    "ifnull((SELECT uuid FROM persons WHERE id = chairman),-1) as chairman,"
                                    "ifnull((SELECT uuid FROM persons WHERE id = counselor2),-1) as counselor2,"
                                    "ifnull((SELECT uuid FROM persons WHERE id = counselor3),-1) as counselor3,"
                                    "ifnull((SELECT uuid FROM persons WHERE id = prayer_beginning),-1) as prayer_beginning,"
                                    "song_beginning,song_middle,song_end,"
                                    "ifnull((SELECT uuid FROM persons WHERE id = prayer_end),-1) as prayer_end,"
                                    "bible_reading,opening_comments,closing_comments,active,uuid "
                                    "FROM lmm_meeting")));

    tables.append(qMakePair(qMakePair(user->hasPermission(PermissionRule::CanEditMidweekMeetingSchedule), QString("lmm_assignment")),
                            QString("SELECT id,"
                                    "ifnull((SELECT uuid FROM lmm_schedule WHERE id = lmm_schedule_id),-1) AS lmm_schedule_id,"
                                    "classnumber,"
                                    "ifnull((SELECT uuid FROM persons WHERE id = assignee_id),-1) as assignee_id,"
                                    "ifnull((SELECT uuid FROM persons WHERE id = volunteer_id),-1) as volunteer_id,"
                                    "ifnull((SELECT uuid FROM persons WHERE id = assistant_id),-1) as assistant_id,"
                                    "date,completed,note,timing,setting,uuid "
                                    "FROM lmm_assignment")));
    tables.append(qMakePair(qMakePair(user->hasPermission(PermissionRule::CanEditTerritorySettings), QString("territory_city")),
                            QString("SELECT id,city,wkt_geometry,active,uuid,"
                                    "ifnull((SELECT uuid FROM languages WHERE id = lang_id),-1) AS lang_id "
                                    "FROM territory_city")));
    tables.append(qMakePair(qMakePair(user->hasPermission(PermissionRule::CanEditTerritorySettings), QString("territory_type")),
                            QString("SELECT id,type_name,active,uuid,"
                                    "ifnull((SELECT uuid FROM languages WHERE id = lang_id),-1) AS lang_id "
                                    "FROM territory_type")));

    tables.append(qMakePair(qMakePair(user->hasPermission(PermissionRule::CanEditTerritories), QString("territory")),
                            QString("SELECT id,territory_number,locality,priority,remark,wkt_geometry,active,uuid,"
                                    "ifnull((SELECT uuid FROM territory_city WHERE id = city_id),-1) AS city_id,"
                                    "ifnull((SELECT uuid FROM territory_type WHERE id = type_id),-1) AS type_id,"
                                    "ifnull((SELECT uuid FROM languages WHERE id = lang_id),-1) AS lang_id "
                                    "FROM territory")));
    tables.append(qMakePair(qMakePair(user->hasPermission(PermissionRule::CanEditTerritories), QString("territory_assignment")),
                            QString("SELECT id,checkedout_date,checkedbackin_date,active,uuid,"
                                    "ifnull((SELECT uuid FROM territory WHERE id = territory_id),NULL) AS territory_id,"
                                    "ifnull((SELECT uuid FROM persons WHERE id = person_id),-1) AS person_id,"
                                    "ifnull((SELECT uuid FROM languages WHERE id = lang_id),NULL) AS lang_id "
                                    "FROM territory_assignment")));
    tables.append(qMakePair(qMakePair(user->hasPermission(PermissionRule::CanEditTerritorySettings), QString("territory_streettype")),
                            QString("SELECT id,streettype_name,color,active,uuid "
                                    "FROM territory_streettype")));
    tables.append(qMakePair(qMakePair(user->hasPermission(PermissionRule::CanEditTerritories), QString("territory_street")),
                            QString("SELECT id,street_name,from_number,to_number,quantity,streettype_id,wkt_geometry,active,uuid,"
                                    "ifnull((SELECT uuid FROM territory WHERE id = territory_id),NULL) AS territory_id "
                                    "FROM territory_street")));
    tables.append(qMakePair(qMakePair(user->hasPermission(PermissionRule::CanEditTerritorySettings), QString("territory_addresstype")),
                            QString("SELECT id,addresstype_number,addresstype_name,color,active,uuid,"
                                    "ifnull((SELECT uuid FROM languages WHERE id = lang_id),-1) AS lang_id "
                                    "FROM territory_addresstype")));
    tables.append(qMakePair(qMakePair(user->hasPermission(PermissionRule::CanEditTerritories), QString("territory_address")),
                            QString("SELECT id,country,state,county,district,city,street,housenumber,postalcode,wkt_geometry,name,addresstype_number,active,uuid,"
                                    "ifnull((SELECT uuid FROM territory WHERE id = territory_id),NULL) AS territory_id,"
                                    "ifnull((SELECT uuid FROM languages WHERE id = lang_id),NULL) AS lang_id "
                                    "FROM territory_address")));

    tables.append(qMakePair(qMakePair(user->hasPermission(PermissionRule::CanEditSongList), QString("song")),
                            QString("SELECT id,song_number,title,active,uuid,"
                                    "ifnull((SELECT uuid FROM languages WHERE id = lang_id),NULL) AS lang_id "
                                    "FROM song")));
    tables.append(qMakePair(qMakePair(user->hasPermission(PermissionRule::CanEditMidweekMeetingSettings), QString("lmm_studies")),
                            QString("SELECT id,study_number,study_name,lang,active,uuid FROM lmm_studies")));
    tables.append(qMakePair(qMakePair(user->hasPermission(PermissionRule::CanEditMeetingNotes), QString("notes")),
                            QString("SELECT id, type_id, date, notes, active, uuid FROM notes")));

    for (QPair<QPair<bool, QString>, QString> tableinfo : tables) {
        sql_items allp = sql->selectSql(tableinfo.second + " WHERE time_stamp > " + timestamp);
        if (!allp.empty()) {
            bool isChangePermitted = tableinfo.first.first;
            QString tableName = tableinfo.first.second;
            qDebug() << tableinfo.first << allp.size();
            sql->blockSignals(true);
            for (unsigned int i = 0; i < allp.size(); i++) {
                sql_item item = allp[i];

                // check uuid
                QString uuid = item.value("uuid").toString();

                QString basedonUuid = "";

                // uuid is based on combination of fields in some tables
                if (tableName == "studies") {
                    basedonUuid = item.value("study_number").toString();
                } else if (tableName == "families") {
                    basedonUuid = item.value("person_id").toString();
                } else if (tableName == "publicmeeting") {
                    basedonUuid = item.value("date").toString();
                } else if (tableName == "speaker_publictalks") {
                    basedonUuid = item.value("speaker_id").toString() + "/" + item.value("theme_id").toString() + "/" + item.value("lang_id").toString();
                } else if (tableName == "settings") {
                    basedonUuid = item.value("name").toString();
                } else if (tableName == "lmm_schedule") {
                    basedonUuid = item.value("date").toString() + "/" + item.value("talk_id").toString();
                } else if (tableName == "lmm_meeting") {
                    basedonUuid = item.value("date").toString();
                } else if (tableName == "lmm_assignment") {
                    basedonUuid = item.value("lmm_schedule_id").toString() + "/" + item.value("classnumber").toString();
                } else if (tableName == "congregationmeetingtimes") {
                    basedonUuid = item.value("congregation_id").toString() + "/" + item.value("mtg_year").toString();
                } else if (tableName == "song") {
                    basedonUuid = item.value("song_number").toString() + "/" + item.value("lang_id").toString();
                } else if (tableName == "territory_addresstype") {
                    basedonUuid = item.value("addresstype_number").toString() + "/" + item.value("lang_id").toString();
                } else if (tableName == "lmm_studies") {
                    basedonUuid = item.value("study_number").toString();
                }

                if (basedonUuid != "") {
                    uuid = QUuid::createUuidV5(QUuid(), tableName + basedonUuid).toString().remove("{").remove("}");
                } else if (item.value("uuid").toString() == "") {
                    uuid = QUuid::createUuid().toString().remove("{").remove("}");
                }

                if (uuid != item.value("uuid").toString()) {
                    // insert or update uuid
                    sql_item s;
                    s.insert("uuid", uuid);
                    sql->updateSql(tableName, "id", item.value("id").toString(), &s);
                    item.insert("uuid", uuid);
                }

                item.insert("tablename", tableName);

                // do not save time_stamp or id
                item.remove("time_stamp");
                item.remove("id");
                if (isChangePermitted)
                    localChangeset.insert(item.value("uuid").toString(), item);
                else
                    localUnpermittedChangeset.insert(item.value("uuid").toString(), item);
            }
            sql->blockSignals(false);
        }
    }
}

void sync_cloud::saveChanges()
{
    // Save changes to local database
    if (mRemoteChangeSet1.count() > 0 || mRemoteChangeSet2.count() > 0 || mRemoteChangeSet3.count() > 0) {
        // Use QTime to debug saving time
        QElapsedTimer myTimer;
        myTimer.start();

        sql->startTransaction();
        // congregations must be saved before anything else (mRemoteChangeSet1)
        saveChangesToDatabase(mRemoteChangeSet1);
        // save second change set
        saveChangesToDatabase(mRemoteChangeSet2);
        emit progressed(80);

        // cached values
        mCachedValues.clear();

        QStringList tables;
        tables << "persons"
               << "languages"
               << "congregations"
               << "publictalks";
        for (auto table : tables) {
            sql_items s = sql->selectSql("SELECT uuid,id FROM " + table);
            for (unsigned int i = 0; i < s.size(); i++) {
                sql_item item = s[i];
                mCachedValues.insert(item.value("uuid").toString(), item.value("id").toString());
            }
        }

        // save second change set
        saveChangesToDatabase(mRemoteChangeSet3);
        sql->commitTransaction();
        int nMSecs = myTimer.elapsed();
        qDebug() << "!!!!! Saving time:" << nMSecs << "milliseconds !!!!!";
    } else {
        qDebug() << "No changes from cloud";
    }

    // Save sync id
    sql->saveSetting("last_dbsync_id", QVariant(m_changeid).toString());
    // Save sync time
    if (mSyncFileModified > sql->getIntSetting("last_dbsyncfile_time", 0)) {
        sql->saveSetting("last_dbsync_time", QVariant(QDateTime::currentDateTime().toSecsSinceEpoch()).toString());
        sql->saveSetting("last_dbsyncfile_time", QVariant(mSyncFileModified).toString());
    }
    // Save last cloud user
    sql->saveSetting("last_db_user", m_useremail);

    emit ready();
    emit progressed(100);
}

void sync_cloud::saveChangesToDatabase(QHash<QString, sql_item> changeset)
{
    sql->blockSignals(true);
    int counter = 0;
    QHashIterator<QString, sql_item> i(changeset);
    while (i.hasNext()) {
        i.next();
        sql_item item = i.value();
        QString uuId = i.key();
        QString tableName = item.value("tablename").toString();
        item.remove("tablename");
        item.remove("changeid");
        QString keepFields = "";
        QString keepValues = "";

        // id mapping from uuid to internal id value
        if (tableName == "school" || tableName == "school_schedule" || tableName == "school_break" || tableName == "servicemeeting" || tableName == "serviceprogram" || tableName == "biblestudymeeting") {
            continue;
        } else if (tableName == "student_studies") {
            item.insert("student_id", idMapping("persons", item.value("student_id").toString()));
        } else if (tableName == "congregationmeetingtimes") {
            item.insert("congregation_id", idMapping("congregations", item.value("congregation_id").toString()));
        } else if (tableName == "speaker_publictalks") {
            item.insert("theme_id", idMapping("publictalks", item.value("theme_id").toString()));
            item.insert("speaker_id", idMapping("persons", item.value("speaker_id").toString()));
            item.insert("lang_id", idMapping("languages", item.value("lang_id").toString()));
            // theme_number is replaced by theme_id
            if (item.contains("theme_number"))
                item.remove("theme_number");
            if (item.value("theme_id").toInt() == -1 || item.value("speaker_id").toInt() == -1) {
                continue;
            }
        } else if (tableName == "publictalks") {
            item.insert("lang_id", idMapping("languages", item.value("lang_id").toString()));
            if (item.value("lang_id").toInt() == -1) {
                continue;
            }
        } else if (tableName == "unavailables") {
            item.insert("person_id", idMapping("persons", item.value("person_id").toString()));
        } else if (tableName == "families") {
            item.insert("person_id", idMapping("persons", item.value("person_id").toString()));
            item.insert("family_head", idMapping("persons", item.value("family_head").toString()));
        } else if (tableName == "publicmeeting") {
            item.insert("theme_id", idMapping("publictalks", item.value("theme_id").toString()));
            item.insert("speaker_id", idMapping("persons", item.value("speaker_id").toString()));
            item.insert("wtreader_id", idMapping("persons", item.value("wtreader_id").toString()));
            item.insert("chairman_id", idMapping("persons", item.value("chairman_id").toString()));
            item.insert("wt_conductor_id", idMapping("persons", item.value("wt_conductor_id").toString()));
            item.insert("hospitality_id", idMapping("persons", item.value("hospitality_id").toString()));
            item.insert("final_prayer_id", idMapping("persons", item.value("final_prayer_id").toString()));
            // theme_number is replaced by theme_id
            if (item.contains("theme_number"))
                item.remove("theme_number");
            else if (item.contains("notes"))
                item.remove("notes");
        } else if (tableName == "lmm_meeting") {
            item.insert("chairman", idMapping("persons", item.value("chairman").toString()));
            item.insert("counselor2", idMapping("persons", item.value("counselor2").toString()));
            item.insert("counselor3", idMapping("persons", item.value("counselor3").toString()));
            item.insert("prayer_beginning", idMapping("persons", item.value("prayer_beginning").toString()));
            item.insert("prayer_end", idMapping("persons", item.value("prayer_end").toString()));
            if (item.contains("notes"))
                item.remove("notes");
        } else if (tableName == "lmm_schedule") {
            if (!item.contains("study_number"))
                continue;
        } else if (tableName == "lmm_assignment") {
            item.insert("lmm_schedule_id", idMapping("lmm_schedule", item.value("lmm_schedule_id").toString()));
            item.insert("assignee_id", idMapping("persons", item.value("assignee_id").toString()));
            item.insert("volunteer_id", idMapping("persons", item.value("volunteer_id").toString()));
            item.insert("assistant_id", idMapping("persons", item.value("assistant_id").toString()));
        } else if (tableName == "persons") {
            if (item.value("congregation_id").toString().length() > 2)
                item.insert("congregation_id", idMapping("congregations", item.value("congregation_id").toString()));
        } else if (tableName == "outgoing") {
            item.insert("speaker_id", idMapping("persons", item.value("speaker_id").toString()));
            item.insert("theme_id", idMapping("publictalks", item.value("theme_id").toString()));
            item.insert("congregation_id", idMapping("congregations", item.value("congregation_id").toString()));
            if (item.value("theme_id").toInt() == -1 || item.value("speaker_id").toInt() == -1) {
                continue;
            }
        } else if (tableName == "settings") {
            // use saveSetting when settings table
            sql->saveSetting(item.value("name").toString(), item.value("value").toString());
            continue;
        } else if (tableName == "territory_city" || tableName == "territory_type" || tableName == "territory_addresstype") {
            item.insert("lang_id", idMapping("languages", item.value("lang_id").toString()));
        } else if (tableName == "territory") {
            item.insert("city_id", idMapping("territory_city", item.value("city_id").toString()));
            item.insert("type_id", idMapping("territory_type", item.value("type_id").toString()));
            item.insert("lang_id", idMapping("languages", item.value("lang_id").toString()));
        } else if (tableName == "territory_assignment") {
            item.insert("territory_id", idMapping("territory", item.value("territory_id").toString()));
            item.insert("person_id", idMapping("persons", item.value("person_id").toString()));
            item.insert("lang_id", idMapping("languages", item.value("lang_id").toString()));
        } else if (tableName == "territory_street") {
            item.insert("territory_id", idMapping("territory", item.value("territory_id").toString()));
        } else if (tableName == "territory_address") {
            item.insert("territory_id", idMapping("territory", item.value("territory_id").toString()));
            item.insert("lang_id", idMapping("languages", item.value("lang_id").toString()));
        } else if (tableName == "song") {
            item.insert("lang_id", idMapping("languages", item.value("lang_id").toString()));
        }

        QString sqlstr = "INSERT OR REPLACE INTO %1 (id,uuid,time_stamp%3%5) values ("
                         "( SELECT IFNULL((SELECT id FROM %1 WHERE uuid = '%2'), "
                         "( SELECT IFNULL(( SELECT MAX(id) FROM %1 ),0)+1 )"
                         ") ),"
                         "'%2',:time_stamp%4%6)";

        QString fieldnames = "";
        QString values = "";
        for (sql_item::const_iterator ii = item.constBegin(); ii != item.constEnd(); ++ii) {
            if (ii.key() == "uuid")
                continue;
            fieldnames += "," + ii.key();
            values += ",:" + ii.key();
        }
        sqlstr = QString(sqlstr).arg(tableName, uuId, fieldnames, values, keepFields, keepValues);
        qDebug() << sqlstr;

        item.insert(":time_stamp", "");
        item.remove("time_stamp");
        item.remove("id");
        item.remove("uuid");

        sql->execSql(sqlstr, &item, true);
        counter += 1;
    }
    sql->blockSignals(false);
}

void sync_cloud::deleteChangesFromDatabase(QHash<QString, sql_item> changeset)
{
    sql->blockSignals(true);
    int counter = 0;
    QHashIterator<QString, sql_item> i(changeset);
    while (i.hasNext()) {
        i.next();
        sql_item item = i.value();
        QString uuId = i.key();
        QString tableName = item.value("tablename").toString();

        QString sqlstr = "DELETE FROM %1 WHERE uuid = '%2'";
        sqlstr = QString(sqlstr).arg(tableName, uuId);
        qDebug() << sqlstr;
        sql->execSql(sqlstr); //(sqlstr, &item, true);
        counter += 1;
    }
    sql->blockSignals(false);
}

void sync_cloud::compareChangeSets()
{
    qDebug() << "compareChangeSets";

    // Compare local and remote change sets
    mDuplicateValues.clear();
    QHashIterator<QString, sql_item> i(mLocalChangeSet);
    while (i.hasNext()) {
        i.next();
        if ((mRemoteChangeSet1.contains(i.key()) && !compareChangeSetValues(i.value(), mRemoteChangeSet1.value(i.key()))) || (mRemoteChangeSet2.contains(i.key()) && !compareChangeSetValues(i.value(), mRemoteChangeSet2.value(i.key()))) || (mRemoteChangeSet3.contains(i.key()) && !compareChangeSetValues(i.value(), mRemoteChangeSet3.value(i.key()))))
            mDuplicateValues.append(i.key());
        //mDuplicateValues.append(i.key());
    }

    // Compare unpermitted local changes with remote change sets
    mLocalUnpermittedOriginalSet.clear();
    mLocalUnpermittedDeleteSet.clear();
    QHashIterator<QString, sql_item> j(mLocalUnpermittedChangeSet);
    while (j.hasNext()) {
        j.next();
        if (mAllRows.contains(j.key())) {
            sql_item item = mAllRows.value(j.key());
            item.remove("changeid");
            if (!compareChangeSetValues(j.value(), item))
                mLocalUnpermittedOriginalSet.insert(j.key(), item);
        } else
            mLocalUnpermittedDeleteSet.insert(j.key(), j.value());
    }

    if (mLocalChangeSet.count() == 0 && mLocalUnpermittedOriginalSet.count() == 0 && mLocalUnpermittedDeleteSet.count() == 0
        && mRemoteChangeSet1.count() == 0 && mRemoteChangeSet2.count() == 0 && mRemoteChangeSet3.count() == 0) {
        qDebug() << "Nothing to sync";
        emit ready();
        return;
    }
    emit progressed(60);
    // Resolve conflicts
    if (mDuplicateValues.count() > 0) {
        qDebug() << "sync conflict...";
        for (QString val : mDuplicateValues)
            qDebug() << "conflict" << val;
        emit syncConflict(mDuplicateValues.count());
        return;
    }
    emit progressed(80);
    continueSync(true);
}

bool sync_cloud::compareChangeSetValues(const sql_item first, const sql_item second)
{
    if (first.count() != second.count()) {
        return false;
    } else {
        for (QString n : second.keys()) {
            if (n == "changeid")
                continue;
            if (!first.contains(n) || second.value(n) != first.value(n)) {
                return false;
            }
        }
    }
    return true;
}

QByteArray sync_cloud::cleanHttpResponse(QByteArray value)
{
    if (value.endsWith("200"))
        value.remove(value.length() - 3, 3);
    return value;
}

QString sync_cloud::idMapping(QString fromTable, QString uuid)
{
    if (uuid == "" || uuid == "-1")
        return "-1";

    if (mCachedValues.contains(uuid))
        return mCachedValues.value(uuid);

    QString value = sql->selectSql(
                               QString("SELECT IFNULL((SELECT id FROM %1 WHERE uuid = '%2'),-1) as id").arg(fromTable, uuid))[0]
                            .value("id")
                            .toString();

    return value;
}
