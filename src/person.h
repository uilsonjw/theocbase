/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PERSON_H
#define PERSON_H

#include <QString>
#include <QStringList>
#include <QPair>
#include <QDate>
#include <QList>
#include <vector>
#include "sql_class.h"

/**
 * @brief The person class - This class represent a one person
 *                           Save changes using update() function.
 */
class person : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(QString firstname READ firstname WRITE setFirstname NOTIFY firstnameChanged)
    Q_PROPERTY(QString lastname READ lastname WRITE setLastname NOTIFY lastnameChanged)
    Q_PROPERTY(QString info READ info WRITE setInfo)
    Q_PROPERTY(Gender gender READ gender WRITE setGender NOTIFY genderChanged)
    Q_PROPERTY(QString phone READ phone WRITE setPhone NOTIFY phoneChanged)
    Q_PROPERTY(QString mobile READ mobile WRITE setMobile NOTIFY mobileChanged)
    Q_PROPERTY(QString email READ email WRITE setEmail NOTIFY emailChanged)
    Q_PROPERTY(bool servant READ servant WRITE setServant NOTIFY servantChanged)
    Q_PROPERTY(int usefor READ usefor WRITE setUsefor NOTIFY useforChanged)
    Q_PROPERTY(int congregationid READ congregationid WRITE setCongregationid)
    Q_PROPERTY(QString fullname READ fullname CONSTANT)
    Q_PROPERTY(QString congregationName READ congregationName CONSTANT)

public:
    person(QObject *parent = nullptr);
    person(int id, QString uuid = "", QObject *parent = nullptr);
    virtual ~person() = default;

    /**
     * @brief The Gender enum - Male or Female
     */
    enum Gender { Male,
                  Female };
    Q_ENUM(Gender)

    /**
     * @brief The UseFor enum
     */
    enum UseFor {
        // use for bit values
        Highlights = 1 << 0, // school - bible highlights             1
        No1 = 1 << 1, // school - number 1                            2
        No2 = 1 << 2, // school - number 2                            4
        No3 = 1 << 3, // school - number 3                            8
        IsBreak = 1 << 4, // is break                                 16
        FieldMinistry = 1 << 5, // field ministry meeting conductor   32
        Chairman = 1 << 6, // public meeting chairman                 64
        WtReader = 1 << 7, // watchtower reader                       128
        Assistant = 1 << 8, // assistant                              256
        PublicTalk = 1 << 9, // public talks                          512
        CBSConductor = 1 << 10, // congregation bible study conductor 1024
        Prayer = 1 << 11, // prayer                                   2048
        SM = 1 << 12, // service meeting programs                     4096
        CBSReader = 1 << 13, // congregation bible study reader       8192
        WtCondoctor = 1 << 14, // watchtower study conductor          16384
        SchoolMain = 1 << 15, // school - only for Main Class         32768
        SchoolAux = 1 << 16, // school - only for Auxiliary Classes   65536
        LMM_Chairman = 1 << 17, // midweek meeting - chairman         131072
        LMM_Treasures = 1 << 18, // Treasures from God's Word         262144
        LMM_Digging = 1 << 19, // Digging for Gems                    524288
        LMM_BibleReading = 1 << 20, // Bible Reading                  1048576
        LMM_PreparePresentations = 1 << 21, // Prepare Presentations  2097152
        LMM_InitialCall = 1 << 22, // Inital Call                     4194304
        LMM_ReturnVisit = 1 << 23, // Return Visit                    8388608
        LMM_BibleStudy = 1 << 24, // Bible Study                      16777216
        LMM_LivingTalks = 1 << 25, // The Living as Christian Talks   33554432
        Hospitality = 1 << 26, // Hospitality                         67108864
        LMM_ApplyTalks = 1 << 27, // Talk in "Apply" section          134217728
        LMM_OtherVideoPart = 1 << 28 // Other video part              268435456
    };
    Q_ENUM(UseFor)

    /**
     * @brief id - Get object id in database
     * @return - Id
     */
    int id();
    bool isNew();
    /**
     * @brief firstname - Get firstname
     * @return - firstname
     */
    QString firstname() const;

    /**
     * @brief lastname - Get lastname
     * @return - lastname
     */
    QString lastname() const;

    /**
     * @brief fullname - Get fullname
     * @param format - Name format (optional: if parameter is omitted, default value 'FirstName LastName' will be used.)
     *                 Options are:
     *                 + FirstName LastName
     *                 + LastName FirstName
     *                 + LastName, FirstName
     * @return - Fullname
     */
    QString fullname(QString format = "FirstName LastName");

    /**
     * @brief info - Get info text
     * @return - Info
     */
    QString info() const;

    /**
     * @brief gender - Get gender (Male or Female)
     * @return - gender
     */
    Gender gender() const;

    /**
     * @brief phone - Get phone number
     * @return - Phone number
     */
    QString phone() const;

    /**
     * @brief mobile - Get mobile number
     * @return - Mobile number
     */
    QString mobile() const;

    /**
     * @brief email - Get email address
     * @return - email
     */
    QString email() const;

    /**
     * @brief servant - Is a brother servant?
     * @return - true or false
     */
    bool servant() const;

    /**
     * @brief usefor - Get use for value. This value tells what assignments is allowed for this person.
     * @return - Binary value of assignments. Use bitwise operations to check allowed assignments.
     *           Example: Is allowed to use Wt reader? bool useWtReader = (person::usefor() & person::wt_reader);
     */
    int usefor() const;

    /**
     * @brief congregationid - Get congregation id number
     * @return - id number
     */
    int congregationid() const;

    /**
     * @brief congregationName - Get congregation name
     * @return - Name of congregation
     */
    QString congregationName();

    /**
     * @brief congregationInfo - Get congregation record
     * @return - the db record for the congregation
     */
    sql_items congregationInfo();

    bool dirtyFlag();

    QString uuid() const;

    QList<QPair<QDate, QDate>> getUnavailabilities();
    void setUnavailability(QDate start, QDate end);
    bool removeUnavailability(QDate start, QDate end);
    /**
     * @brief update - This function save changes to database
     * @return - success or failure
     */
    Q_INVOKABLE virtual bool update();

public slots:

    /**
     * @brief setFirstname - Set firstname
     * @param arg - Firstname
     */
    void setFirstname(QString arg);

    /**
     * @brief setLastname - Set lastname
     * @param arg - Lastname
     */
    void setLastname(QString arg);

    /**
     * @brief setInfo - Set info value
     * @param value - Info
     */
    void setInfo(QString value);

    /**
     * @brief setGender - Set gender
     * @param gender - Gender (Male or Female) e.g. setGender(person::Male)
     */
    void setGender(Gender gender);

    /**
     * @brief setPhone - Set phone number
     * @param value - phone
     */
    void setPhone(QString value);

    /**
     * @brief setMobile - Set mobile number
     * @param value - mobile
     */
    void setMobile(QString value);

    /**
     * @brief setEmail - Set email address
     * @param value - email
     */
    void setEmail(QString value);

    /**
     * @brief setServant - Set servant value for brother
     * @param value - true if servant or false
     */
    void setServant(bool value);

    /**
     * @brief setUsefor - Set use for value. This value tells what assignments is allowed for this person.
     * @param value - Value is in a binary format and saved in a one column in the database.
     *                Example: Use for bible highlights in the school AND Wt reader in the Watchtower study - setUsefor(person::no1 + person::wt_reader)
     */
    void setUsefor(int value);

    /**
     * @brief setId - Set database id for person object
     * @param id - id number
     */
    void setId(int id);

    /**
     * @brief setCongregationid - Set congregation id number
     * @param id - id number
     */
    void setCongregationid(int id);
    void setDirtyFlag(bool dirty);

signals:
    void idChanged(int id);
    void firstnameChanged(QString fname);
    void lastnameChanged(QString lname);
    void emailChanged(QString email);
    void phoneChanged(QString phone);
    void mobileChanged(QString mobile);
    void genderChanged(Gender g);
    void useforChanged(int value);
    void servantChanged(bool servant);

private:
    int m_id;
    QString m_info;
    QString m_firstname;
    QString m_lastname;
    Gender m_gender;
    QString m_phone;
    QString m_mobile;
    QString m_email;
    bool m_servant;
    int m_usefor;
    int m_congregationid;
    QString m_congregationname;
    bool m_dirty;
    QString m_uuid;
    sql_class *sql;
};

#endif // PERSON_H
