/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "printcombination.h"

PrintCombination::PrintCombination()
{
    currentTemplateData = QSharedPointer<TemplateData>(
            new TemplateData(TemplateData::CombinationTemplate, "combinationTemplate", "COMBO_1.htm", "combo"));
    qrCodeSupport = true;
}

QVariant PrintCombination::fillTemplate()
{
    if (!_midweek && !_publicMeeting && !_publicMeetingOut) {
        return "<HTML><BODY><h1>" + tr("Select at least one option") + "</h1></BODY></HTML>";
    }

    QString str = getTemplate();
    str = initLayout(str);
    str = ifthen.tokenize(str);

    // titles for all meetings
    str.replace("!CONGREGATION!", tr("Congregation"));
    str.replace("!CONGREGATION_NAME!", myCongregation.name);
    str.replace("!CONGREGATION_TITLE!", tr("%1 Congregation", "Congregation_Title Tag. %1 is Congregation Name").arg(myCongregation.name));
    str.replace("!LM!", tr("Christian Life and Ministry Meeting"));
    str.replace("!MIDWEEK!", tr("Midweek Meeting"));
    str.replace("!PM!", tr("Public Meeting"));
    str.replace("!PT!", tr("Public Talk"));
    str.replace("!WT!", tr("Watchtower Study"));
    str.replace("!CHAIRMAN!", tr("Chairman"));
    str.replace("!COUNSELOR!", tr("Counselor"));
    str.replace("!CONDUCTOR!", tr("Conductor"));
    str.replace("!READER!", tr("Reader"));
    str.replace("!SPEAKER!", tr("Speaker", "Public talk speaker"));
    str.replace("!LOCALE_NAME!", QLocale().bcp47Name());
    str.replace("!MINUTES!", tr("min.", "Abbreviation of minutes"));

    fillWeekList(0);
    qDebug() << weekList[0]->week.toString(Qt::ISODate);

    QPair<QString, QString> p1 = templateGetSection(str, "!REPEAT_START!", "!REPEAT_END!", "repeat_class");
    QString all_without_repeat = p1.first;
    p1 = templateGetSection(p1.second, "!MIDWEEK_START!", "!MIDWEEK_END!", "mwk_class");
    QString mwk_section = fillTemplateBlockLMMOverall(p1.second);
    p1 = templateGetSection(p1.first, "!WEEKEND_START!", "!WEEKEND_END!", "pm_class");
    QString pm_section = p1.second;
    p1 = templateGetSection(p1.first, "!PTOUT_START!", "!PTOUT_END!", "ptout_class");
    QString ptout_section = p1.second;
    QString repeat_section = p1.first;

    all_without_repeat.replace("!TITLE!", tr("Combined Schedule"));
    repeat_section.replace("!TITLE!", tr("Combined Schedule"));
    pm_section.replace("!TITLE!", sql->getSetting("print_we_title", tr("Weekend Meeting")));
    ptout_section.replace("!TITLE!", tr("Outgoing Speakers"));

    QString weekContext = "";
    int lastMonth(-1);
    ptCounter = 0;

    for (weekInfo *w : weekList) {
        currentWeek = w;
        //QDate tempdate(w->week);
        ptCounter = 1;

        if (w->mtgDate.month() != lastMonth) {
            lastMonth = w->mtgDate.month();
            ptCounter = 1;
        }

        QString oneweek(repeat_section);
        oneweek = replaceDateTag(oneweek, "WEEKSTARTING", w->week, tr("Week Starting %1", "%1 is Monday of the week"));
        oneweek = replaceDateTag(oneweek, "WEEKSTART_DATE", w->week);
        oneweek = replaceDateRangeTag(oneweek, "WEEK_DATERANGE", w->week, w->week.addDays(6));
        oneweek = fillCommonItems(oneweek, c.noMeeting(currentWeek->week), c.isException(currentWeek->week));

        // Midweek
        if (_midweek) {
            QString sectionCopy = QString(mwk_section);
            oneweek.replace("<div class=\"mwk_class\"></div>",
                            "<div>" + fillTemplateBlockLMM(sectionCopy, false) + "</div>");
        }

        // public meeting
        if (_publicMeeting) {
            QString sectionCopy = QString(pm_section);
            oneweek.replace("<div class=\"pm_class\"></div>",
                            "<div>" + fillTemplateBlockPM(sectionCopy) + "</div>");
        }

        oneweek.remove("!NO_MEETING!");

        if (_publicMeetingOut) {
            oneweek.replace("<div class=\"ptout_class\"></div>",
                            "<div>" + fillTemplateBlockPTOUT(QString(ptout_section)) + "</div>");
        }
        weekContext.append(oneweek);
    }

    all_without_repeat.replace("<div class=\"repeat_class\"></div>", "<div class=\"repeat_class\">" + weekContext + "</div>");
    // Add QR Code
    // TODO ???
    // all_without_repeat = addQrCode(all_without_repeat);

    return ifthen.compute(all_without_repeat);
}

void PrintCombination::setMidweek(bool printMidweek)
{
    _midweek = printMidweek;
}

void PrintCombination::setPublicMeeting(bool printPublicMeeting)
{
    _publicMeeting = printPublicMeeting;
}

void PrintCombination::setPublicMeetingOut(bool printOutgoing)
{
    _publicMeetingOut = printOutgoing;
}
