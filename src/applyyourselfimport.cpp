#include "applyyourselfimport.h"

applyyourselfimport::applyyourselfimport(QString fileName)
{
    sql = &Singleton<sql_class>::Instance();
    prepared = !epb.Prepare(fileName);
}

bool applyyourselfimport::Import()
{
    lastError.clear();
    if (prepared) {
        connect(&epb, &epub::ProcessTOCEntry, this, &applyyourselfimport::ProcessTOCEntry);
        epb.ImportTOC();
        return true;
    } else {
        lastError = epb.lastErr;
        return false;
    }
}

void applyyourselfimport::ProcessTOCEntry(QString href, QString chapter)
{
    Q_UNUSED(chapter);

    sql->startTransaction();

    //sql->execSql("Delete from lmm_studies");
    studiesFound = 0;

    xml_reader r(epb.oebpsPath + "/" + href);
    r.register_elementsearch("a", xmlPartsContexts::link, -1, true);
    r.register_elementsearch("strong", xmlPartsContexts::strong, xmlPartsContexts::link, true);
    r.register_elementsearch(QXmlStreamReader::TokenType::EndElement, "a", xmlPartsContexts::link);
    XLM_READER_CONNECT(r);
    r.read();

    if (studiesFound == 20) {
        sql->execSql("delete from lmm_studies where time_stamp < " + QVariant(QDateTime::currentDateTime().toTime_t() - 5).toString());
        sql->commitTransaction();
    } else {
        sql->rollbackTransaction();
    }
}

void applyyourselfimport::xmlPartFound(QXmlStreamReader *xml, QXmlStreamReader::TokenType tokenType, int context, int relativeDepth)
{
    Q_UNUSED(relativeDepth);

    switch (context) {
    case xmlPartsContexts::strong:
        if (tokenType == QXmlStreamReader::TokenType::Characters) {
            queueStudyNumber = epb.stringToInt(xml->text().toString());
        }
        break;

    case xmlPartsContexts::link:
        switch (tokenType) {
        case QXmlStreamReader::TokenType::StartElement:
            queueStudyNumber = 0;
            queueStudy = xml->text().toString();
            break;

        case QXmlStreamReader::TokenType::Characters:
            queueStudy += xml->text().toString();
            break;

        case QXmlStreamReader::TokenType::EndElement:
            if (queueStudyNumber > 0 && !queueStudy.isEmpty()) {
                studiesFound++;
                sql_item parts;
                parts.insert("study_number", queueStudyNumber);
                parts.insert("lang", epb.language);
                parts.insert("study_name", queueStudy);
                sql->insertSql("lmm_studies", &parts, "id");
            }
            break;
        default:
            break;
        }
        break;
    }
}
