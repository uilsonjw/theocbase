/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cpersoncombobox.h"
#include "availability/weekendmeetingavailabilitychecker.h"
#include "availability/midweekmeetingavailabilitychecker.h"


using namespace tbAvailability;

// Combobox used for selecting bros and sisters for assignments. Currently it is only used in the
// Public meeting page for assigning chairman, wt conductor and wt reader. Elsewhere on the Public
// meeting page (e.g. Speaker) and the midweek meeting a different mechanism is used.
//
// TODO: See above. It would seem a good idea to have a single standard mechanism for selecting
// people for assignments across all meetings.

cPersonComboBox::cPersonComboBox(QWidget *parent)
    : QComboBox(parent)
    , tableWidget_(nullptr)
{
    setPersonType(-1, ccongregation::cbs);
}

// Initialise the TableWidget model and set the checkbox Model and View
void cPersonComboBox::InitTableWidget()
{
    if(!tableWidget_)
    {
        tableWidget_ = new QTableWidget(this->parentWidget());
        tableWidget_->horizontalHeader()->show();
        tableWidget_->verticalHeader()->setVisible(false);
        tableWidget_->setColumnCount(4);
        tableWidget_->setColumnHidden(0,true);
        tableWidget_->setColumnWidth(1,150);
        tableWidget_->setColumnWidth(2,100);
        tableWidget_->setColumnWidth(3,30);
        tableWidget_->setMinimumWidth(275);

        tableWidget_->setHorizontalHeaderItem(1,new QTableWidgetItem(QObject::tr("Name")));
        tableWidget_->setHorizontalHeaderItem(2,new QTableWidgetItem(QObject::tr("Last")));
        tableWidget_->setHorizontalHeaderItem(3,new QTableWidgetItem());

        tableWidget_->setSelectionMode(QAbstractItemView::SingleSelection);
        tableWidget_->setSelectionBehavior(QAbstractItemView::SelectRows);

        setModel(tableWidget_->model());
        setView(tableWidget_);
    }
}

void cPersonComboBox::hidePopup()
{    
    emit popupClosed(this);
    QComboBox::hidePopup();    
}

void cPersonComboBox::showPopup()
{
    QString ct = currentText();
    qDebug() << "combo text before show popup:" << ct;

    if(personType_ != -1)
    {                
        InitTableWidget();                                
        tableWidget_->setSortingEnabled(false);
        setProperty("currenttext", ct);
        AddPersons(personType_, ct);
        tableWidget_->setSortingEnabled(true);
    }

    QString name = objectName();
    emit beforeShowPopup(name,this);
    QComboBox::showPopup();
}

void cPersonComboBox::setCurrentText(QString value)
{
    qDebug() << "setting combo text:" << value;

    auto index = findText(value, Qt::MatchExactly);
    if(index == -1)
    {        
        // could not find the value in the combo
        qDebug() << "not found in combo:" << value;

        if(value.isEmpty())
        {
            setCurrentIndex(-1);
        }
        else
        {
            addItem(value);
        }
    }
    else
    {
        qDebug() << "found at index:" << index;
        setCurrentIndex(index);
    }
}

// collect the person data and add to the combobox list
void cPersonComboBox::AddPersons(int typeValue, QString currentText)
{
    qDebug() << "typevalue:" << typeValue;

    if(typeValue >= 0)
    {
        QScopedPointer<AvailabilityChecker> checker;

        QDate meetingDate = GetMeetingDate(weekCommencingDate_, meetingtype_);

        switch(meetingtype_)
        {
            case ccongregation::cbs:
            case ccongregation::tms:
            case ccongregation::sm:
                checker.reset(new MidWeekMeetingAvailabilityChecker(meetingDate, weekCommencingDate_));
                break;

            case ccongregation::pm:
            case ccongregation::wt:
                checker.reset(new WeekendMeetingAvailabilityChecker(meetingDate, weekCommencingDate_));
                break;
        }

        PopulateModel(*checker, currentText, (person::UseFor)typeValue);
        setModelColumn(1);
        setCurrentText(currentText);
    }
}

void cPersonComboBox::PopulateModel(AvailabilityChecker &checker, QString currentText, person::UseFor roleNeeded)
{
    Availability availability = checker.Get(roleNeeded);

    tableWidget_->clearContents();
    tableWidget_->setRowCount(availability.Count() + 1);

    DateDelegate *delegate = new DateDelegate(this);
    tableWidget_->setItemDelegateForColumn(2, delegate);

    // blank row first...
    tableWidget_->setItem(0, 0, new QTableWidgetItem());    

    for(int n=0; n<availability.Count(); ++n)
    {
        PopulateModelRow(availability.GetItem(n), n+1, currentText, roleNeeded);
    }
}

QDate cPersonComboBox::GetMeetingDate(const QDate &weekCommencingDate, ccongregation::meetings meetingType)
{
    Q_ASSERT(weekCommencingDate.dayOfWeek() == Qt::Monday);
    return weekCommencingDate.addDays(cong_.getMeetingDay(weekCommencingDate, meetingType) -1);
}

// populate a single row of the model using data in "item"
void cPersonComboBox::PopulateModelRow(AvailabilityItem *item, int row, QString currentText, person::UseFor roleNeeded)
{
    int personId = item->Id;
    tableWidget_->setItem(row, 0, new QTableWidgetItem(QString::number(personId)));

    QTableWidgetItem *nameItem = new QTableWidgetItem(item->DisplayName);
    tableWidget_->setItem(row, 1, nameItem);
    QString nameText = nameItem->text();
    if(nameText == currentText)
    {
        QFont boldfont(nameItem->font().family(),
                       nameItem->font().pointSize(),
                       QFont::Bold);
        nameItem->setFont(boldfont);
    }

    QDate lastAssignedWeekCommencing = item->GetDateLastAssigned(roleNeeded);
    QDate displayDate;
    if(!lastAssignedWeekCommencing.isNull())
    {
        displayDate = GetMeetingDate(lastAssignedWeekCommencing, meetingtype_);
    }

    QTableWidgetItem *dateWidgetItem = new QTableWidgetItem();
    dateWidgetItem->setData(Qt::DisplayRole, displayDate);
    tableWidget_->setItem(row, 2, dateWidgetItem);

    if (!weekCommencingDate_.isNull())
    {
        // unavailabilities...
        ShowPossibleConflicts(item, row, roleNeeded);
    }
}

QTableWidgetItem *cPersonComboBox::CreateStatusImageWidget(QString imageResPath, QString tooltipText)
{
    QTableWidgetItem *status = new QTableWidgetItem;
    QImage *img = new QImage(imageResPath);
    status->setData(Qt::DecorationRole, QPixmap::fromImage(*img));
    status->setToolTip(tooltipText);
    status->setFlags(Qt::NoItemFlags);

    return status;
}

// display appropriate icons to denote possible conflicts in assignments of bros
void cPersonComboBox::ShowPossibleConflicts(AvailabilityItem *item, int row, person::UseFor roleNeeded)
{
    bool preventSelection = false;

    if(item->OutsideSpeaker)
    {        
        tableWidget_->setItem(row, 3, CreateStatusImageWidget(":/icons/warning_busy.svg", tr("Outgoing speaker")));
        preventSelection = true;
    }
    else if(item->OnHoliday)
    {
        tableWidget_->setItem(row, 3, CreateStatusImageWidget(":/icons/warning_busy.svg", tr("Person unavailable")));
        preventSelection = true;
    }
    else if(item->HasAssignmentsOtherThan(roleNeeded, weekCommencingDate_))
    {        
        tableWidget_->setItem(row, 3, CreateStatusImageWidget(":/icons/warning.svg", tr("Brother has other meeting parts")));
        preventSelection = false; // don't necessarily prevent
    }

    if(preventSelection)
    {
        // can't select this bro...
        tableWidget_->item(row, 1)->setFlags(Qt::NoItemFlags);
        tableWidget_->item(row, 2)->setFlags(Qt::NoItemFlags);
    }
}

void cPersonComboBox::focusOutEvent(QFocusEvent *e)
{
    emit focusOut(this);
    QComboBox::focusOutEvent(e);
}

int cPersonComboBox::personType()
{
    return personType_;
}

void cPersonComboBox::setPersonType(int typeNumber, ccongregation::meetings meetingtype, const QDate &date)
{
    Q_ASSERT(!tableWidget_); // ensure we haven't initialised the model yet

    meetingtype_ = meetingtype;
    weekCommencingDate_ = date;
    personType_ = typeNumber;
}

void cPersonComboBox::setDate(const QDate &date)
{
    weekCommencingDate_ = date;
}




