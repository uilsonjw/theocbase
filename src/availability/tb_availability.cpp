#include "tb_availability.h"

using namespace tbAvailability;

// AvailabilityItem
//=================

AvailabilityItem::AvailabilityItem()
    : Id(0)
    , OnHoliday(false)
    , OutsideSpeaker(false)
{

}

bool AvailabilityItem::operator<(const AvailabilityItem &rhs) const
{
    return QString::localeAwareCompare(DisplayName, rhs.DisplayName) < 0;
}

bool AvailabilityItem::HasRole(person::UseFor role)
{
    return Roles.find(role) != Roles.end();
}

void AvailabilityItem::AddHomeAssignment(QDate weekStartDate, person::UseFor assignment)
{
    AssignmentsByDateI iter = AssignmentsAtHome.find(weekStartDate);
    if(iter == AssignmentsAtHome.end())
    {
        SetOfRoles roles;
        roles.insert(assignment);
        AssignmentsAtHome.insert(std::pair<QDate, SetOfRoles>(weekStartDate, roles));
    }
    else
    {
        iter->second.insert(assignment);
    }
}

bool AvailabilityItem::HasAssignmentsOtherThan(person::UseFor assignment, QDate weekCommencingDate)
{    
    bool result = false;

    AssignmentsByDateI iter = AssignmentsAtHome.find(weekCommencingDate);
    if(iter != AssignmentsAtHome.end())
    {
        SetOfRoles &assignments = iter->second;
        int numAssignments = static_cast<int>(assignments.size());
        result = numAssignments > 1 || (numAssignments == 1 && assignments.find(assignment) == assignments.end());
    }

    return result;
}

// NB - returns week commencing date
QDate AvailabilityItem::GetDateLastAssigned(person::UseFor assignment)
{
    QDate result;
    for(AssignmentsByDateI iter = AssignmentsAtHome.begin();
        iter != AssignmentsAtHome.end(); ++iter)
    {
        if(iter->second.find(assignment) != iter->second.end())
        {
            if(result.isNull() || iter->first > result)
            {
                result = iter->first;
            }
        }
    }
    return result;
}


// Availability
//=============

void Availability::Add(AvailabilityItem &item)
{
    items_.push_back(item);
}

void Availability::Sort()
{
    std::sort(items_.begin(), items_.end());
}

void Availability::Filter(person::UseFor role)
{
    items_.erase(
                std::remove_if(
                     items_.begin(), items_.end(),
                        [&role](AvailabilityItem &item){ return !item.HasRole(role); }), items_.end());
}

void Availability::FilterOnPersonIds(std::vector<int> personIds)
{
    items_.erase(std::remove_if(
        items_.begin(), items_.end(),
            [&personIds](AvailabilityItem &item) {
                    return std::find(personIds.begin(), personIds.end(), item.Id) == personIds.end();
                }), items_.end());
}

int Availability::Count()
{
    return static_cast<int>(items_.size());
}

AvailabilityItem *Availability::GetItem(int index)
{    
    return &items_[static_cast<size_t>(index)];
}

