/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2021, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Window 2.12
import net.theocbase.mobile 1.0

Page {
    property string pageTitle: ""
    property var meeting
    property bool editable: false

    header: BaseToolbar {
        title: pageTitle
        componentLeft: ToolButton {
            icon.source: "qrc:///back.svg"
            icon.color: "white"
            onClicked: {
                stackView.pop()
            }
        }
    }

    ColumnLayout {
        id: layout
        anchors.fill: parent
        anchors.rightMargin: parent.rightPadding
        spacing: 0

        RowTitle { text: qsTr("Notes", "Meeting Notes") }

        TextArea {
            Layout.fillWidth: true
            id: textAreaNotes
            text: meeting ? meeting.notes : ""
            wrapMode: Text.WordWrap
            Layout.fillHeight: true
            font.pointSize: app_info.fontsize
            leftPadding: 10*dpi
            rightPadding: 10*dpi
            readOnly: !editable
            background: null
            onEditingFinished: {
                if (meeting.notes !== text) {
                    meeting.notes = text
                    meeting.saveNotes()
                }
            }
        }
    }
}
