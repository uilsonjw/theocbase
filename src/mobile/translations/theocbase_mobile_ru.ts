<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation>Имя</translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation>Дата</translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Не назначать следующий урок</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Оставить текущий урок</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Неверные данные</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Добавить хронометраж?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Тема</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Источник</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Учащийся</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Помощник</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Результат</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Выполнено</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Доброволец</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Выбрать добровольца</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Текущий урок</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Упражнения выполнены</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Следующий урок</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Выбрать следующий урок</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Хронометраж</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Заметки</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Детали школы</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Запустить секундомер</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Остановить секундомер</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Ситуация</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Выбери ситуацию</translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>No exception</source>
        <translation>нет исключения</translation>
    </message>
    <message>
        <source>Circuit overseer&#x27;s visit</source>
        <translation>Посещение районного надзирателя</translation>
    </message>
    <message>
        <source>Circuit assembly</source>
        <translation>Районый конгресс</translation>
    </message>
    <message>
        <source>Regional convention</source>
        <translation>Региональный конгресс</translation>
    </message>
    <message>
        <source>Memorial</source>
        <translation>Вечеря</translation>
    </message>
    <message>
        <source>Other exception</source>
        <translation>Другие исключения</translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation>Неделя от %1</translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation>Дни встречи</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Встреча в будние дни</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Встреча в выходные дни</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Тема</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Источник</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Ведущий</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Докладчик</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Чтец</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Заметки</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Детали</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <comment>Page title</comment>
        <translation>Заметки</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>СОКРОВИЩА ИЗ СЛОВА БОГА</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>ОТТАЧИВАЕМ НАВЫКИ СЛУЖЕНИЯ</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>ХРИСТИАНСКАЯ ЖИЗНЬ</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Председатель</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Дающий совет</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Песня %1 и Молитва</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Песня</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Ведущий</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Чтец</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Вступительные слова</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Молитва</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation>Импортировать расписание ...</translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation>ГЗ</translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation>Д1</translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation>Д2</translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation>Заключительные слова</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Оставить текущий урок</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Запустить секундомер</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Остановить секундомер</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Добавить хронометраж?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Тема</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Источник</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Учащийся</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Результат</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Завершено</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Хронометраж</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Текущий урок</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Упражнения выполнены</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Следующий урок</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Выбрать следующий урок</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Заметки</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Помощник</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Доброволец</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>Секундомер</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>Напарник не должен быть противоположного пола.</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>Урок</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Детали</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>Потяните вниз для обновления...</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Отпустите для обновления...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation>Имя пользователя</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Логин</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Имя пользователя или E-mail</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Регистрация</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>Сброс пароля</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>Адрес e-mail не найден!</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Забыл пароль</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>Страница входа</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation>Неделя от %1</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation>Уходящие докладчики</translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>%1 уходящий докладчик в эти выходные</numerusform>
            <numerusform>%1 уходящие докладчики в эти выходные</numerusform>
            <numerusform>%1 уходящих докладчиков в эти выходные</numerusform>
            <numerusform>%1 уходящих докладчиков в эти выходные</numerusform>
        </translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation>Уходящих докладчиков в эти выходные нет</translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation>Параметры печати</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Расписание</translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation>Рабочие листы</translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation>Расписание уходящих докладчиков</translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation>Назначение уходящих докладчиков</translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation>список обзвона и график приема</translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation>Речи докладчиков</translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Печать</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Встреча в будние дни</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Встреча в выходные</translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation>Комбинация</translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation>Бланки заданий</translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation>Шаблон</translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation>Бланки заданий для помощников</translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation>Печатать только назначенное</translation>
    </message>
    <message>
        <source>Text size</source>
        <translation>Размер текста</translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation>Пользовательские шаблоны</translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Песня и молитва</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Песня %1 и молитва</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>ПУБЛИЧНАЯ РЕЧЬ</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>ИЗУЧЕНИЕ СБ</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Песня %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Ведущий</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Чтец</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation>Импортировать СБ ...</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>Тема</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>Собрание</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Докладчик</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Мобильный</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Телефон</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Эл. почта</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>инфо</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Гостеприимство</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>Публичная речь</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Фамилия</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Брат</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Сестра</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Служитель</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Семья</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>Член семьи связан с</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Контактная информация</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Телефон</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>Все классы</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Только главный класс</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Только вспомогательные классы</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Глава семьи</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Помощник</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Инфо</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>Активен</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Председатель</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>СОКРОВИЩА ИЗ СЛОВА БОГА</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Чтение Библии</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Первое посещение</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Повторное посещение</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Изучение Библии</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>Христианская жизнь Речи</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Изучение Библии в собрании</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Чтец на изучении Библии</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>Новый возвещатель</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Молитва</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation>Гостеприимство докладчика</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Мобильный</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Встреча в будние дни</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>Речь</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Встреча в выходные</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>Ведущий изучение СБ</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation>Чтец СБ</translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation>Духовные жемчужины</translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Возвещатели</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation>Cписок выбора</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Настройки</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Выйти</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Инфо</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Версия</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>Домашняя страница TheocBase</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Обратная связь</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>Последняя синхронизация: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Расписание</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Показывать время</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Показывать длительность</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Пользовательский интерфейс</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Язык</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Вход</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation>Показать заголовки песен</translation>
    </message>
    <message>
        <source>Printing</source>
        <translation>Печать</translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation>Пользовательские шаблоны</translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Синхронизация</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>Председатель</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Песня</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation>Председатель встречи на выходных</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>Песня</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation>Песня СБ</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Молитва</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation>Выпуск СБ</translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>Артикль №</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Тема</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>Ведущий</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>Чтец</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>График</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>Число недель перед выбранной датой</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>Число недель после выбранной даты</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>недели</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation>Число недель выделенных после назначения</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>Те же изменения могут быть найдены и локально и в облаке (%1 строки). Хотите сохранить локальные изменения?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>Облачные данные удалены. Ваши локальные данные будут заменены. Продолжить?</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Не назначать следующий урок</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>Не установлен</translation>
    </message>
</context></TS>