/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "windowsshareutils.h"
#include <Windows.ApplicationModel.datatransfer.h>
#include <ShObjIdl.h>
#include <wrl/client.h>
#include <wrl/event.h>
#include <wrl/wrappers/corewrappers.h>
#include <tchar.h>
#include <roapi.h>
#include <sstream>
//#include <windows.foundation.collections.h>

#pragma comment(lib, "runtimeobject.lib")

namespace WRL = Microsoft::WRL;
namespace awf = ABI::Windows::Foundation;
namespace dt = ABI::Windows::ApplicationModel::DataTransfer;
namespace st = ABI::Windows::Storage;
using namespace ABI::Windows::Storage;
//using namespace ABI::Windows::Foundation::Collections;

using Microsoft::WRL::Wrappers::HStringReference;

Microsoft::WRL::ComPtr<IDataTransferManagerInterop> g_dtmInterop;
Microsoft::WRL::ComPtr<dt::IDataTransferManager> g_dtm;
EventRegistrationToken g_dataRequestedToken;

WindowsShareUtils::WindowsShareUtils(QObject *parent)
    : PlatformShareUtils(parent)
{
}

void WindowsShareUtils::share(const QString shareText, const QString email, QPoint pos)
{
    Q_UNUSED(pos);
    // https://github.com/microsoft/Windows-universal-samples/tree/master/Samples/ShareSource
    QWindowList windows = qApp->topLevelWindows();
    QWindow *mainWindow = nullptr;
    for (auto w : windows) {
        if (!w->title().isEmpty())
            mainWindow = w;
    }
    if (!mainWindow)
        return;

    if (initShareSheet(mainWindow->winId(), shareText)) {
        HWND handle = (HWND)mainWindow->winId();
        g_dtmInterop->ShowShareUIForWindow(handle);
    }
}

bool WindowsShareUtils::initShareSheet(quintptr id, const QString shareText)
{
    HWND handle = (HWND)id;

    RoGetActivationFactory(HStringReference(
                                   RuntimeClass_Windows_ApplicationModel_DataTransfer_DataTransferManager)
                                   .Get(),
                           IID_PPV_ARGS(&g_dtmInterop));
    if (!g_dtmInterop)
        return false;

    g_dtmInterop->GetForWindow(handle, IID_PPV_ARGS(&g_dtm));
    if (!g_dtm)
        return false;

    auto callback = WRL::Callback<awf::ITypedEventHandler<
            dt::DataTransferManager *, dt::DataRequestedEventArgs *>>(
            [shareText](auto &&, dt::IDataRequestedEventArgs *e) {
                WRL::ComPtr<dt::IDataRequest> request;
                e->get_Request(&request);

                WRL::ComPtr<dt::IDataPackage> data;
                request->get_Data(&data);

                WRL::ComPtr<dt::IDataPackagePropertySet> properties;
                data->get_Properties(&properties);

                // Title is mandatory
                QString title = shareText.split("\n").first();
                properties->put_Title(HStringReference(reinterpret_cast<LPCWSTR>(title.utf16()), title.length()).Get());

                // Description is optional
                properties->put_Description(HStringReference(L"Descriptions").Get());

                // Text
                data->SetText(HStringReference(reinterpret_cast<LPCWSTR>(shareText.utf16()), shareText.length()).Get());
                return S_OK;
            });
    g_dtm->add_DataRequested(callback.Get(), &g_dataRequestedToken);

    return true;
}
