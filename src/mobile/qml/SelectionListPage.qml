/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2019, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.3
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import net.theocbase.mobile 1.0

Page {
    id: root

    property string pageTitle: qsTr("Selection List", "Page title")
    property alias model: list.model
    property alias selectedRow: list.currentIndex
    property bool showName: false
    signal activeRowChanged(int index)

    header: BaseToolbar {
        title: pageTitle
        componentLeft: ToolButton {
            icon.source: "qrc:///back.svg"
            icon.color: "white"
            onClicked: { stackView.pop() }
        }
    }

    ScrollView {
        width: parent.width
        height: parent.height
        contentWidth: width
        ListView {
            id: list
            width: root.width
            highlightFollowsCurrentItem: true

            onCurrentItemChanged: {
                root.activeRowChanged(currentIndex)
                console.log("Current item:" + currentIndex)
            }
            ButtonGroup {
                id: buttonGroup
            }
            delegate: CheckDelegate {
                text: showName ?
                          (typeof modelData === "undefined" ? (typeof name === "undefined" ? "-" : name) : model.modelData.name) :
                          model.modelData
                checked: list.currentIndex == index
                onClicked:{
                    console.log("clicked " + index)
                    if (checked)
                        list.currentIndex = index
                }
                width: root.width
                ButtonGroup.group: buttonGroup
            }
        }
    }

    Component {
        id: delegateItem
        Item {
            width: parent.width
            height: 50*dpi
            RowLayout {
                anchors.fill: parent
                anchors.margins: 5*dpi
                spacing: 5*dpi
                width: parent.width - 10*dpi
                height: parent.height -10*dpi
                CheckBox {
                    id: chk
                    checked: list.currentIndex == index
                }
                Label {
                    id: changeText
                    text: showName ?
                              (typeof modelData === "undefined" ? (typeof name === "undefined" ? "-" : name) : model.modelData.name) :
                              model.modelData
                    //elide: Text.ElideRight
                    //width: parent.width - chk.width
                    Layout.fillWidth: true
                    verticalAlignment: Text.AlignVCenter
                    renderType: Text.QtRendering
                    Component.onCompleted: console.log("QMLDEBUG: " + name + " " + showName)
                }
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    console.log("clicked" + index)
                    list.currentIndex = index
                }
            }
            Rectangle {
                width: parent.width
                anchors.bottom: parent.top
                anchors.topMargin: 0
                height: app_info.linewidth
                color: "lightgrey"
                visible: index == 0
            }
            Rectangle {
                width: parent.width
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
                height: app_info.linewidth
                color: "lightgrey"
            }
        }
    }

}

