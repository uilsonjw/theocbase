#include "sqlcombo.h"
#include "ccongregation.h"
#include "lmm_assignment.h"
#include "person.h"

SqlComboHelper::SqlComboHelper()
{
    bro_only = true;
    school_assign = false;
    assisting_id = 0; // not assisting anyone.
    specific_talk = true;
    specific_together = false;
    specific_assigned = false;
    classnum = 1;
}

void SqlComboHelper::set_sort_filter(SQLCOMBO::sf_school::sort_filter_1,
                                     SQLCOMBO::sf_school::sort_filter_2) {}

void SqlComboHelper::set_talk_conductor(LMM_Schedule::TalkTypes x)
{
    talk_num = x;
    switch (talk_num) {
    case LMM_Schedule::TalkType_Chairman:
        cap = person::Chairman;
        break;
    case LMM_Schedule::TalkType_Treasures:
        cap = person::LMM_Treasures;
        break;
    case LMM_Schedule::TalkType_Digging:
        cap = person::LMM_Digging;
        break;
    case LMM_Schedule::TalkType_BibleReading:
        cap = person::LMM_BibleReading;
        school_assign = true;
        break;
    case LMM_Schedule::TalkType_SampleConversationVideo:
    case LMM_Schedule::TalkType_ApplyYourself:
        cap = person::LMM_PreparePresentations;
        break;
    case LMM_Schedule::TalkType_OtherFMVideoPart:
        cap = person::LMM_OtherVideoPart;
        bro_only = true;
        break;
    case LMM_Schedule::TalkType_InitialCall:
    case LMM_Schedule::TalkType_MemorialInvitation:
        cap = person::LMM_InitialCall;
        bro_only = false;
        school_assign = true;
        break;
    case LMM_Schedule::TalkType_ReturnVisit1:
    case LMM_Schedule::TalkType_ReturnVisit2:
    case LMM_Schedule::TalkType_ReturnVisit3:
        cap = person::LMM_ReturnVisit;
        bro_only = false;
        school_assign = true;
        break;
    case LMM_Schedule::TalkType_BibleStudy:
        cap = person::LMM_BibleStudy;
        bro_only = false;
        school_assign = true;
        break;
    case LMM_Schedule::TalkType_StudentTalk:
        cap = person::LMM_ApplyTalks;
        bro_only = true;
        school_assign = true;
        break;
    case LMM_Schedule::TalkType_LivingTalk1:
    case LMM_Schedule::TalkType_LivingTalk2:
    case LMM_Schedule::TalkType_LivingTalk3:
        cap = person::LMM_LivingTalks;
        break;
    case LMM_Schedule::TalkType_CBS:
        cap = person::CBSConductor;
        break;
    }
}

QString SqlComboHelper::get_sql(SQLCOMBO::aggregate aggr) const
{
    return get_midweek_sql(aggr);
}

QString SqlComboHelper::get_midweek_sql(SQLCOMBO::aggregate aggr) const
{
    using namespace SQLCOMBO;
    QString w, any_where, specific_where;

    any_where += "and (not (p.usefor & (%1 | %2) ) or (p.usefor & %";
    if (this->classnum > 1)
        any_where += "2)) ";
    else
        any_where += "1)) ";

    any_where.replace("%1", QString::number(person::SchoolMain),
                      Qt::CaseSensitive);
    any_where.replace("%2", QString::number(person::SchoolAux),
                      Qt::CaseSensitive);

    if (!assisting_id) {
        w = "and (usefor & ";
        w += QString::number(cap);
        w += ")";
    }

    QString s1, s2;

    switch (aggr) {
    case aggr_closest_prefer_first:
        s1 = "0";
        s2 = "0.5";
        break;
    case aggr_closest_prefer_last:
        s1 = "0.5";
        s2 = "0";
        break;
    case aggr_closest:
        s1 = "0";
        s2 = "0";
        break;
    }

    QString s =
            "select q1.id id,q1.lastname,q1.firstname,q1.talk_id talk_id_any, "
            "q1.classnumber classnumber_any,"
            "q1.assignee_id,q1.date date_any,q2.talk_id "
            "talk_specific,q1.date_offset1 date_offset_any,"
            "q2.date_offset2 date_offset_specific,q2.date "
            "date_specific,q2.classnumber classnumber_specific "
            "from (select p.id,p.usefor,p.lastname lastname, p.firstname "
            "firstname,p.id id,p.date, p.talk_id,"
            "p.classnumber,p.assignee_id,%4 date_offset1 from (%7) p left join "
            "unavailables u on p.id ="
            "u.person_id and u.active and '%5' between u.start_date and u.end_date "
            "where u.person_id is null and not (p.usefor & 16) "
            "%3 %8 group by p.id having min(date_offset1) ) q1 left join ( select "
            "p.id,p.date, p.classnumber,"
            "%4 date_offset2, p.talk_id from (%P) p,persons pp where p.id = pp.id "
            "%3 %9 group by p.id having min(date_offset2) ) q2 on (q1.id=q2.id)";
    QString date_offset_seqno_sql_frag =
            " case when p.date is null then 1000000 "
            "else "
            "(abs(julianday(p.date) - julianday('%1'))"
            "+ (case when julianday(p.date) >= julianday('%1') then %2 else %3 "
            "end) +1 ) "
            "end ";
    QString ph_mt =
            "select p.id,sh1.date,sh1.talk_id,sh1.classnumber,sh1.assistant_id, case "
            "when sh1.assignee_id is null "
            "then null when p.id = sh1.assignee_id then 0 else sh1.assignee_id end "
            "assignee_id from persons p "
            "left join ( SELECT lmm_assignment.assignee_id, "
            "lmm_assignment.assistant_id,lmm_assignment.volunteer_id,"
            "lmm_assignment.date, lmm_schedule.talk_id, lmm_assignment.classnumber "
            "FROM lmm_assignment "
            "INNER JOIN lmm_schedule ON lmm_assignment.lmm_schedule_id = "
            "lmm_schedule.id "
            "WHERE lmm_assignment.date IN (SELECT sss.date FROM lmm_schedule sss "
            "WHERE sss.date "
            "NOT IN (SELECT ss.date FROM lmm_schedule AS ss, exceptions AS e WHERE "
            "e.schoolday = 0 AND "
            "0 <= julianday(e.date) - julianday(ss.date) AND julianday(e.date) - "
            "julianday(ss.date) < 7 and e.active))) sh1 "
            "ON (p.id = sh1.assignee_id and sh1.volunteer_id = -1 or "
            "sh1.assistant_id = p.id) where p.active";

    if (bro_only)
        ph_mt += " and gender = 'B'";

    if (specific_talk) {
        QString q = "and p.talk_id between %1 and %2 ";
        q.replace("%1", QString::number(talk_num * 10), Qt::CaseSensitive);
        q.replace("%2", QString::number(talk_num * 10 + 9), Qt::CaseSensitive);
        specific_where += q;
    }
    if (specific_together) {
        QString q =
                "and (pp.id = %1 or p.assignee_id = %1 or p.assistant_id = %1) ";
        q.replace("%1", QString::number(assisting_id), Qt::CaseSensitive);
        specific_where += q;
    }
    if (!assisting_id)
        specific_where += "and p.assignee_id = 0 ";

    date_offset_seqno_sql_frag.replace("%1", dt_str, Qt::CaseSensitive);
    date_offset_seqno_sql_frag.replace("%2", s1, Qt::CaseSensitive);
    date_offset_seqno_sql_frag.replace("%3", s2, Qt::CaseSensitive);

    if (assisting_id && school_assign) {
        QString q = "select p.*,pp.usefor,pp.gender,pp.lastname, pp.firstname from "
                    "(%P) p,persons pp,persons p1 "
                    "where p1.id=%1 and pp.id <> p1.id and not (pp.usefor & 16) "
                    "and (pp.usefor & %2) and "
                    "p.id = pp.id and ((p1.gender = pp.gender) %3)";
        q.replace("%1", QString::number(assisting_id), Qt::CaseSensitive);
        q.replace("%2", QString::number(person::Assistant), Qt::CaseSensitive);
        // Initial call: The assistant should be of the same sex or should be a
        // family member. Return visit or bible study: The assistant should not be
        // someone of the opposite sex.
        q.replace("%3", cap == person::LMM_InitialCall ? "or pp.id in (select f2.person_id "
                                                         "from families f1,families f2 where f1.family_head = "
                                                         "f2.family_head and "
                                                         "p1.id <> f2.person_id and p1.id = f1.person_id)"
                                                       : "");
        s.replace("%7", q, Qt::CaseSensitive);
    } else {
        QString q = "select p.*,pp.usefor,pp.lastname, pp.firstname from (%P) p,"
                    "persons pp where p.id = pp.id ";
        int assist_cap = person::Assistant;
        switch (cap) {
        case person::CBSConductor:
            assist_cap = person::CBSReader;
            break;
        }

        if (assisting_id) {
            q += "and not (pp.usefor & 16) and (pp.usefor & %2)";
        }

        q.replace("%2", QString::number(assist_cap), Qt::CaseSensitive);
        s.replace("%7", q, Qt::CaseSensitive);
    }
    s.replace("%1", dt_str, Qt::CaseSensitive);
    ccongregation c;
    int md = c.getMeetingDay(dt, ccongregation::tms);
    s.replace("%5", dt.addDays(md - 1).toString(Qt::ISODate));
    s.replace("%3", w, Qt::CaseSensitive);
    s.replace("%4", date_offset_seqno_sql_frag, Qt::CaseSensitive);
    s.replace("%8", school_assign ? any_where : "", Qt::CaseSensitive);
    s.replace("%9", specific_where, Qt::CaseSensitive);
    s.replace("%P", ph_mt, Qt::CaseSensitive);
    if (school_assign)
        // prevent student assignment simultaneously for main hall and aux.
        // classroom
        s += QString(" where q1.classnumber = %1 or (q1.talk_id < %2 or q1.talk_id "
                     "> %3) or q1.classnumber is null or q1.date <> '%4'")
                     .arg(QString::number(classnum),
                          QString::number(LMM_Schedule::TalkType_BibleReading * 10),
                          QString::number(LMM_Schedule::TalkType_StudentTalk * 10 + 9),
                          dt.toString(Qt::ISODate));

    return s;
}
