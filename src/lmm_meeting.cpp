#include "lmm_meeting.h"

QString LMM_historyTableAbbreviation(int talk_id, bool assistant)
{
    switch (talk_id) {
    case LMM_Schedule::TalkType_Chairman:
        return QObject::tr("Ch", "history table: abbreviation for the 'chairman' of the Christian Life and Ministry Meeting");
    case LMM_Schedule::TalkType_Treasures:
        return QObject::tr("H", "history table: abbreviation for the 'highlights'");
    case LMM_Schedule::TalkType_Digging:
        return QObject::tr("D", "history table: abbreviation for 'Diging spiritual gem'");
    case LMM_Schedule::TalkType_BibleReading:
        return QObject::tr("#R", "history table: abbreviation for the 'reader'");
    case LMM_Schedule::TalkType_SampleConversationVideo:
        return QObject::tr("#V", "history table: abbreviation for 'prepare video presentation'");
    case LMM_Schedule::TalkType_ApplyYourself:
        return QObject::tr("#A", "history table: abbreviation for 'Apply yourself to Reading and Teaching'");
    case LMM_Schedule::TalkType_MemorialInvitation:
        return QObject::tr("Mem", "history table: abbreviation for assistant/householder of 'Memorial Inviation'");
    case LMM_Schedule::TalkType_OtherFMVideoPart:
        return QObject::tr("Vid", "history table: abbreviation for 'Extra Video Part'");
    case LMM_Schedule::TalkType_InitialCall:
        if (assistant)
            return QObject::tr("♢1", "history table: abbreviation for assignment 1, assistant/householder of 'Initial Visit'");
        return QObject::tr("#1", "history table: abbreviation for assignment 1, 'Initial Visit'");
    case LMM_Schedule::TalkType_ReturnVisit1:
    case LMM_Schedule::TalkType_ReturnVisit2:
    case LMM_Schedule::TalkType_ReturnVisit3:
        if (assistant)
            return QObject::tr("♢2", "history table: abbreviation for assignment 2, assistant/householder of 'Return Visit'");
        return QObject::tr("#2", "history table: abbreviation for assignment 2, 'Return Visit'");
    case LMM_Schedule::TalkType_BibleStudy:
    case LMM_Schedule::TalkType_StudentTalk:
        if (assistant)
            return QObject::tr("♢3", "history table: abbreviation for assignment 3, assistant/householder of 'Bible Study'");
        return QObject::tr("#3", "history table: abbreviation for assignment 3, 'Bible Study'");
    case LMM_Schedule::TalkType_LivingTalk1: // christian life: 1
        return QObject::tr("CL1", "history table: abbreviation for cristian life, talk 1");
    case LMM_Schedule::TalkType_LivingTalk2: // christian life: 2
        return QObject::tr("CL2", "history table: abbreviation for cristian life, talk 2");
    case LMM_Schedule::TalkType_LivingTalk3: // christian life: 3
        return QObject::tr("CL3", "history table: abbreviation for cristian life, talk 3");
    case LMM_Schedule::TalkType_CBS: // chrisitan life: bible study
        if (assistant)
            return QObject::tr("BS-R", "history table: abbreviation for cristian life, Bible Study Reader");
        return QObject::tr("BS", "history table: abbreviation for cristian life, Bible Study");
    default:
        return QString("?%1").arg(talk_id);
    }
}

QColor LMM_foregroundColor(int talk_id, QColor const &defCol)
{
    if (talk_id <= LMM_Schedule::TalkType_Chairman || talk_id > LMM_Schedule::TalkType_CBS)
        return defCol;

    if (talk_id <= LMM_Schedule::TalkType_BibleReading) // treasures, digging and reading
        return LMM_Color::spiritual_gem_text;
    if (talk_id <= LMM_Schedule::TalkType_StudentTalk || talk_id == LMM_Schedule::TalkType_ApplyYourself) // presentation, initial call, return visit 1-3, bible study or student talk
        return LMM_Color::ministry_text;
    if (talk_id <= LMM_Schedule::TalkType_CBS) // living talks 1, 2, 3, CBS, COTalk
        return LMM_Color::christian_life_text;
    return QColor();
}

QColor LMM_backgroundColor(int talk_id, QColor const &defCol)
{
    if (talk_id <= LMM_Schedule::TalkType_Chairman || talk_id > LMM_Schedule::TalkType_CBS)
        return defCol;

    if (talk_id <= LMM_Schedule::TalkType_BibleReading)
        return LMM_Color::spiritual_gem_bg;
    if (talk_id <= LMM_Schedule::TalkType_StudentTalk || talk_id == LMM_Schedule::TalkType_ApplyYourself)
        return LMM_Color::ministry_bg;
    if (talk_id <= LMM_Schedule::TalkType_CBS)
        return LMM_Color::christian_life_bg;
    return QColor();
}

//-----------------------------------------------------------------------------

LMM_Meeting::LMM_Meeting(QObject *parent, bool delayLoadingDetails)
    : QObject(parent)
{
    m_meeting_id = -1;
    m_songBeginning = 0;
    m_songMiddle = 0;
    m_songEnd = 0;
    m_chairman = nullptr;
    m_counselor2 = nullptr;
    m_counselor3 = nullptr;
    m_prayer_beginning = nullptr;
    m_prayer_end = nullptr;
    m_classes = -1;
    m_delayLoadingDetails = delayLoadingDetails;
}

LMM_Meeting::~LMM_Meeting()
{
    qDeleteAll(m_talks);
    m_talks.clear();
}

QDate LMM_Meeting::date() const
{
    return m_date;
}

bool LMM_Meeting::loadMeeting(QDate date, bool includeAllOptions)
{
    m_classes = -1; // provides reloading
    emit classesChanged(classes());
    m_date = date;

    qDeleteAll(m_talks);
    m_talks.clear();

    if (date.isValid()) {
        sql_class *sql = &Singleton<sql_class>::Instance();
        sql_item queryitem;
        queryitem.insert(":date", m_date);

        setNotes(sql->selectScalar("SELECT notes FROM notes WHERE date = :date and type_id = 1 and active", &queryitem, "").toString());
        sql_items items = sql->selectSql("SELECT * FROM lmm_meeting WHERE date = :date and active",
                                         &queryitem);

        if (items.size() > 0) {
            cpersons cp;
            sql_item s = items[0];
            m_meeting_id = s.value("id").toInt();
            setSongBeginning(s.value("song_beginning").toInt());
            setSongMiddle(s.value("song_middle").toInt());
            setSongEnd(s.value("song_end").toInt());
            setChairman(cp.getPerson(s.value("chairman").toInt()));
            setCounselor2(cp.getPerson(s.value("counselor2").toInt()));
            setCounselor3(cp.getPerson(s.value("counselor3").toInt()));
            setPrayerBeginning(cp.getPerson(s.value("prayer_beginning").toInt()));
            setPrayerEnd(cp.getPerson(s.value("prayer_end").toInt()));
            setBibleReading(s.value("bible_reading").toString());
            setOpeningComments(s.value("opening_comments").toString());
            setClosingComments(s.value("closing_comments").toString());

            if (!m_delayLoadingDetails)
                loadAssignments(includeAllOptions);
            emit dateChanged(date);
            return true;
        }
    } else {
        setNotes("");
    }

    setSongBeginning(0);
    setSongMiddle(0);
    setSongEnd(0);
    setChairman(nullptr);
    setCounselor2(nullptr);
    setCounselor3(nullptr);
    setPrayerBeginning(nullptr);
    setPrayerEnd(nullptr);
    setBibleReading("");
    setOpeningComments("");
    setClosingComments("");

    emit dateChanged(date);
    return false;
}

person *LMM_Meeting::chairman() const
{
    return m_chairman;
}

void LMM_Meeting::setChairman(person *chairman)
{
    m_chairman = chairman;
    if (m_chairman)
        m_chairman->setParent(this);
    emit chairmanChanged(chairman);
}

person *LMM_Meeting::counselor2() const
{
    return m_counselor2;
}

void LMM_Meeting::setCounselor2(person *counselor)
{
    m_counselor2 = counselor;
    if (m_counselor2)
        m_counselor2->setParent(this);
    emit counselor2Changed(counselor);
}

person *LMM_Meeting::counselor3() const
{
    return m_counselor3;
}

void LMM_Meeting::setCounselor3(person *counselor)
{
    m_counselor3 = counselor;
    if (m_counselor3)
        m_counselor3->setParent(this);
    emit counselor3Changed(counselor);
}

person *LMM_Meeting::prayerBeginning() const
{
    return m_prayer_beginning;
}

void LMM_Meeting::setPrayerBeginning(person *prayer)
{
    m_prayer_beginning = prayer;
    if (m_prayer_beginning)
        m_prayer_beginning->setParent(this);
    emit prayerBeginningChanged(prayer);
}

person *LMM_Meeting::prayerEnd() const
{
    return m_prayer_end;
}

void LMM_Meeting::setPrayerEnd(person *prayer)
{
    m_prayer_end = prayer;
    if (m_prayer_end)
        m_prayer_end->setParent(this);
    emit prayerEndChanged(prayer);
}

int LMM_Meeting::songBeginning() const
{
    return m_songBeginning;
}
QString LMM_Meeting::songBeginningTitle() const
{
    return getSongTitle(m_songBeginning);
}
void LMM_Meeting::setSongBeginning(int songBeginning)
{
    m_songBeginning = songBeginning;
    emit songBeginningChanged(songBeginning);
}

int LMM_Meeting::songMiddle() const
{
    return m_songMiddle;
}
QString LMM_Meeting::songMiddleTitle() const
{
    return getSongTitle(m_songMiddle);
}
void LMM_Meeting::setSongMiddle(int songMiddle)
{
    m_songMiddle = songMiddle;
    emit songMiddleChanged(songMiddle);
}

int LMM_Meeting::songEnd() const
{
    return m_songEnd;
}
QString LMM_Meeting::songEndTitle() const
{
    return getSongTitle(m_songEnd);
}
void LMM_Meeting::setSongEnd(int songEnd)
{
    m_songEnd = songEnd;
    emit songEndChanged(songEnd);
}

QString LMM_Meeting::bibleReading() const
{
    return m_biblereading;
}

void LMM_Meeting::setBibleReading(QString reading)
{
    m_biblereading = reading;
    emit bibleReadingChanged(reading);
}

QString LMM_Meeting::openingComments() const
{
    return m_openingcomments;
}

void LMM_Meeting::setOpeningComments(QString comments)
{
    m_openingcomments = comments;
    emit openingCommentsChanged(comments);
}

QString LMM_Meeting::closingComments() const
{
    return m_closingcomments;
}

void LMM_Meeting::setClosingComments(QString comments)
{
    m_closingcomments = comments;
    emit closingCommentsChanged(comments);
}

int LMM_Meeting::classes()
{
    // Get quantity of classes. Value is stored to settings table in the database
    if (m_classes == -1) {
        sql_class *sql = &Singleton<sql_class>::Instance();
        QString value = sql->getSetting("schools_qty");
        m_classes = ((value == "") ? 1 : QVariant(value).toInt());
    }
    return m_classes;
}

void LMM_Meeting::setClasses(int classes)
{
    if (m_classes != classes) {
        sql_class *sql = &Singleton<sql_class>::Instance();
        // Save quantity of classes.
        m_classes = classes;
        // Save value to settings table in the database
        sql->saveSetting("schools_qty", QVariant(m_classes).toString());
    }
    emit classesChanged(classes);
}

QDateTime LMM_Meeting::startTime()
{
    ccongregation c;
    QDateTime t = date().startOfDay();
    t.setTime(QTime::fromString(c.getMyCongregation().time_meeting1, "hh:mm"));
    return t;
}

QString LMM_Meeting::notes() const
{
    return m_notes;
}

void LMM_Meeting::setNotes(QString notes)
{
    if (notes != m_notes) {
        m_notes = notes;
        emit notesChanged(m_notes);
    }
}

bool LMM_Meeting::save()
{
    // save changes to database
    sql_class *sql = &Singleton<sql_class>::Instance();

    if (!date().isValid())
        return false;

    sql_item queryitems;
    queryitems.insert(":id", m_meeting_id);
    int id = m_meeting_id > 0 ? sql->selectScalar("SELECT id FROM lmm_meeting WHERE id = :id", &queryitems, -1).toInt() : -1;

    sql_item insertitems;
    insertitems.insert("date", date());
    insertitems.insert("chairman", chairman() ? chairman()->id() : -1);
    insertitems.insert("counselor2", counselor2() ? counselor2()->id() : -1);
    insertitems.insert("counselor3", counselor3() ? counselor3()->id() : -1);
    insertitems.insert("prayer_beginning", prayerBeginning() ? prayerBeginning()->id() : -1);
    insertitems.insert("prayer_end", prayerEnd() ? prayerEnd()->id() : -1);
    insertitems.insert("song_beginning", songBeginning());
    insertitems.insert("song_middle", songMiddle());
    insertitems.insert("song_end", songEnd());
    insertitems.insert("bible_reading", bibleReading());
    insertitems.insert("opening_comments", openingComments());
    insertitems.insert("closing_comments", closingComments());

    if (id > 0) {
        // update
        return sql->updateSql("lmm_meeting", "id", QString::number(id), &insertitems);
    } else {
        // insert new row
        return (sql->insertSql("lmm_meeting", &insertitems, "id") > 0);
    }
}

bool LMM_Meeting::saveNotes()
{
    if (!date().isValid())
        return false;
    sql_class *sql = &Singleton<sql_class>::Instance();
    sql_item items;
    items.insert(":date", date());
    int id = sql->selectScalar("SELECT id FROM notes WHERE date = :date AND type_id = 1", &items, -1).toInt();
    sql_item insertitems;
    insertitems.insert("date", date());
    insertitems.insert("notes", notes());
    insertitems.insert("type_id", 1);
    if (id > 0)
        return sql->updateSql("notes", "id", QString::number(id), &insertitems);
    else
        return sql->insertSql("notes", &insertitems, "id") > 0;
}

LMM_Assignment *LMM_Meeting::getAssignment(int talkId, int sequence, int classnumber)
{
    for (LMM_Assignment *a : m_talks) {
        if (a->talkId() == talkId && a->sequence() == sequence && a->classnumber() == classnumber)
            return a;
    }
    return nullptr;
}

QList<LMM_Assignment *> LMM_Meeting::getAssignments()
{
    return m_talks;
}

QVariant LMM_Meeting::getAssignmentsVariant()
{
    QList<QObject *> list;
    for (LMM_Assignment *a : getAssignments()) {
        bool show = true;
        if (a->classnumber() == 2)
            show = this->counselor2() && !this->counselor2()->fullname().isEmpty();
        else if (a->classnumber() == 3)
            show = this->counselor3() && !this->counselor3()->fullname().isEmpty();

        if (show)
            list.append(a);
    }
    return QVariant::fromValue(list);
}

SortFilterProxyModel *LMM_Meeting::getChairmanList()
{
    return getBrotherList(person::LMM_Chairman);
}

SortFilterProxyModel *LMM_Meeting::getPrayerList()
{
    return getBrotherList(person::Prayer);
}

void LMM_Meeting::loadAssignments(bool includeAllOptions)
{
    qDeleteAll(m_talks);
    m_talks.clear();

    ccongregation c;
    ccongregation::exceptions exception = c.isException(m_date);

    // sql
    sql_class *sql = &Singleton<sql_class>::Instance();
    QString sqlquery = includeAllOptions ? "select "
                                           "s.id as schedule_id,"
                                           "s.date,"
                                           "tlk.talk_id,"
                                           "s.theme,"
                                           "s.source,"
                                           "s.time,"
                                           "s.study_number,"
                                           "a.id as assignment_id,"
                                           "a.assignee_id,"
                                           "a.assistant_id,"
                                           "a.volunteer_id,"
                                           "cls.classnumber,"
                                           "a.completed,"
                                           "a.note,"
                                           "a.timing, "
                                           "s.roworder "
                                           "from " // TODO: (select talk_id * 10 talk_id from talkinfo), select(1 thru 3) subTalkId
                                           "(select 20 talk_id union all "
                                           "select 30 talk_id union all "
                                           "select 40 talk_id union all "
                                           "select 50 talk_id union all "
                                           "select 60 talk_id union all "
                                           "select 61 talk_id union all "
                                           "select 62 talk_id union all "
                                           "select 70 talk_id union all "
                                           "select 71 talk_id union all "
                                           "select 72 talk_id union all "
                                           "select 80 talk_id union all "
                                           "select 81 talk_id union all "
                                           "select 90 talk_id union all "
                                           "select 91 talk_id union all "
                                           "select 100 talk_id union all "
                                           "select 101 talk_id union all "
                                           "select 102 talk_id union all "
                                           "select 110 talk_id union all "
                                           "select 120 talk_id union all "
                                           "select 130 talk_id union all "
                                           "select 140 talk_id union all "
                                           "select 150 talk_id union all "
                                           "select 160 talk_id union all "
                                           "select 170 talk_id union all "
                                           "select 270 talk_id union all "
                                           "select 271 talk_id union all "
                                           "select 272 talk_id union all "
                                           "select 280) tlk "
                                           "INNER JOIN "
                                           "(select 1 classnumber union all "
                                           "select 2 classnumber union all "
                                           "select 3 classnumber) cls "
                                           "on tlk.talk_id between 40 and 110 or tlk.talk_id = 170 or tlk.talk_id = 270 or cls.classnumber = 1 "
                                           "LEFT JOIN lmm_schedule s ON tlk.talk_id = s.talk_id and s.date = :date and s.active "
                                           "LEFT JOIN lmm_assignment a ON s.id = a.lmm_schedule_id and cls.classnumber = a.classnumber "
                                           "ORDER BY roworder, tlk.talk_id, cls.classnumber"
                                         : "SELECT "
                                           "s.id as schedule_id,"
                                           "s.date,"
                                           "s.talk_id,"
                                           "s.theme,"
                                           "s.source,"
                                           "s.time,"
                                           "s.study_number,"
                                           "a.id as assignment_id,"
                                           "a.assignee_id,"
                                           "a.assistant_id,"
                                           "a.volunteer_id,"
                                           "a.classnumber,"
                                           "a.completed,"
                                           "a.note,"
                                           "a.timing,"
                                           "CASE WHEN s.roworder IS NULL THEN CASE "
                                           "WHEN talk_id = 170 THEN 45 ELSE talk_id END ELSE s.roworder END AS roworder "
                                           "FROM lmm_schedule s "
                                           "LEFT JOIN lmm_assignment a ON s.id = a.lmm_schedule_id "
                                           "WHERE s.date = :date and s.active "
                                           "ORDER BY roworder, a.classnumber";
    sql_item qitem;
    qitem.insert(":date", m_date);
    qitem.insert(":pp", LMM_Schedule::TalkType_SampleConversationVideo);
    sql_items items;
    items = sql->selectSql(sqlquery, &qitem);
    cpersons cp;
    for (unsigned int i = 0; i < items.size(); i++) {
        sql_item s = items[i];
        LMM_Assignment *a = nullptr;
        int talkID(0);
        int sequence(0);
        LMM_Schedule::splitDbTalkId(s.value("talk_id").toInt(), talkID, sequence);
        switch (talkID) {
        case LMM_Schedule::TalkType_BibleReading:
        case LMM_Schedule::TalkType_InitialCall:
        case LMM_Schedule::TalkType_ReturnVisit1:
        case LMM_Schedule::TalkType_ReturnVisit2:
        case LMM_Schedule::TalkType_ReturnVisit3:
        case LMM_Schedule::TalkType_BibleStudy:
        case LMM_Schedule::TalkType_StudentTalk:
        case LMM_Schedule::TalkType_MemorialInvitation:
        case LMM_Schedule::TalkType_OtherFMVideoPart: {
            if (exception == ccongregation::CircuitOverseersVisit && s.value("classnumber").toInt() > 1 && !includeAllOptions)
                continue;
            LMM_Assignment_ex *a_ex = new LMM_Assignment_ex(talkID, sequence, s.value("schedule_id").toInt(),
                                                            s.value("assignment_id").toInt(), this);
            a = a_ex;
            a_ex->setAssistant(cp.getPerson(s.value("assistant_id").toInt()));
            a_ex->setVolunteer(cp.getPerson(s.value("volunteer_id").toInt()));
            a_ex->setTiming(s.value("timing").toString());
            a_ex->setSetting(s.value("setting").toString());
            a_ex->setCompleted(s.value("completed").toBool());
            a_ex->setClassnumber(s.value("classnumber").toInt());
        } break;
        case LMM_Schedule::TalkType_CBS: {
            if (exception == ccongregation::CircuitOverseersVisit) {
                // circuit overseer visit -> do not show in the list
                continue;
            }
            LMM_Assignment_ex *a_cbs = new LMM_Assignment_ex(LMM_Schedule::TalkType_CBS, 0, s.value("schedule_id").toInt(),
                                                             s.value("assignment_id").toInt(), this);
            a = a_cbs;
            a_cbs->setAssistant(cp.getPerson(s.value("assistant_id").toInt()));
        } break;
        default:
            if (talkID == LMM_Schedule::TalkType_COTalk && exception != ccongregation::CircuitOverseersVisit)
                continue;
            a = new LMM_Assignment(talkID, sequence, s.value("schedule_id").toInt(),
                                   s.value("assignment_id").toInt(), this);
            a->setClassnumber(s.value("classnumber").toInt()); // if including all options, we need the video parts in all classes
            break;
        }
        a->setDate(m_date);
        a->setSpeaker(cp.getPerson(s.value("assignee_id").toInt()));
        a->setTheme(s.value("theme").toString());
        a->setTime(s.value("time").toInt());
        a->setNote(s.value("note").toString());
        a->setSource(s.value("source").toString());
        a->setStudy_number(s.value("study_number").toInt());

        m_talks.append(a);

        // other classes - add empty assignments even not exist in database
        // NOTE: before 2018, other classes should not be when prepare presentations video talk
        if (a->canMultiSchool() && classes() > 1 && (date().year() > 2017 || !s.value("firstweek").toBool()) && exception != ccongregation::CircuitOverseersVisit) {
            for (int ci = 1; ci < classes() + 1; ci++) {
                if (a->classnumber() == ci || i + 1 >= items.size())
                    continue;
                LMM_Assignment_ex *a_ex = new LMM_Assignment_ex(talkID, sequence, s.value("schedule_id").toInt(), -1, this);
                a_ex->setDate(m_date);
                a_ex->setClassnumber(ci);
                a_ex->setTheme(a->theme());
                a_ex->setTime(a->time());
                a_ex->setSource(a->source());
                a_ex->setStudy_number(a->study_number());
                a_ex->setSpeaker(nullptr);
                a_ex->setAssistant(nullptr);
                a_ex->setVolunteer(nullptr);

                int a1_dbTalkID(items[i + 1].value("talk_id").toInt());
                int a1_talkID(0);
                int a1_sequence(0);
                LMM_Schedule::splitDbTalkId(a1_dbTalkID, a1_talkID, a1_sequence);

                if (a->classnumber() > ci && (m_talks[m_talks.count() - a->classnumber()]->talkId() != a->talkId() || m_talks[m_talks.count() - a->classnumber()]->sequence() != a->sequence())) {
                    m_talks.insert(m_talks.count() - 1, a_ex);
                } else if (ci > a->classnumber() && (i + 1 < items.size()) && (a1_talkID != a->talkId() || (a1_talkID == a->talkId() && a1_sequence != a->sequence()))) {
                    m_talks.append(a_ex);
                } else {
                    delete a_ex;
                    a_ex = nullptr;
                }
            }
        }
    }
    // add CO talk case when schedule has been imported after the addition of exception
    if (exception == ccongregation::CircuitOverseersVisit && m_talks.count() > 0 && m_talks[m_talks.count() - 1]->talkId() != LMM_Schedule::TalkType_COTalk) {
        LMM_Schedule cotalk(LMM_Schedule::TalkType_COTalk, 0, m_date, "", "", 30, -1);
        cotalk.setTheme(cotalk.talkName());
        cotalk.save();
    }
}

void LMM_Meeting::createAssignments()
{
    QList<int> talks = LMM_Schedule::getExpectedTalks(m_date);
    int tid(0);
    int seq(0);
    for (int t : talks) {
        LMM_Schedule::splitDbTalkId(t, tid, seq); //!talkid temporary measure until we go back to previous constructor
        LMM_Schedule sch(tid, seq, m_date, LMM_Schedule::getFullStringTalkType(t), tr("Enter source material here"));
        sch.save();
    }
}

QString LMM_Meeting::getSongTitle(int song) const
{
    if (song < 1)
        return "";
    sql_class *sql = &Singleton<sql_class>::Instance();
    return sql->selectScalar("select title from song where song_number = ? and active", QVariant(song), "").toString();
}

QList<LMM_Schedule *> LMM_Meeting::findSchedules(QDate firstDayOfWeek)
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    sql_item queryitem;
    QString sqlquery = "SELECT * FROM lmm_schedule";
    if (firstDayOfWeek.isValid()) {
        queryitem.insert(":date", firstDayOfWeek);
        sqlquery.append(" WHERE date = :date and active");
    }
    sqlquery.append(" ORDER BY talk_id, date");
    sql_items items = sql->selectSql(sqlquery, &queryitem);
    ccongregation c;
    ccongregation::exceptions exception = c.isException(firstDayOfWeek);
    QList<LMM_Schedule *> list;
    for (unsigned int i = 0; i < items.size(); i++) {
        if (exception == ccongregation::CircuitOverseersVisit && items[i].value("talk_id") == LMM_Schedule::TalkType_CBS)
            continue;
        int tid(0);
        int seq(0);
        LMM_Schedule::splitDbTalkId(items[i].value("talk_id").toInt(), tid, seq); //!talkid temporary measure until we go back to previous constructor
        list.append(new LMM_Schedule(tid, seq,
                                     items[i].value("date").toDate(),
                                     items[i].value("theme").toString(),
                                     items[i].value("source").toString(),
                                     items[i].value("study_number").toInt(),
                                     items[i].value("time").toInt(),
                                     items[i].value("id").toInt(),
                                     this));
    }
    return list;
}

QColor LMM_Meeting::monthlyColor()
{
    if (m_date.year() < 2021) {
        switch (m_date.month()) {
        case 1:
        case 5:
        case 9:
            return QColor("#31446E");
        case 2:
        case 6:
        case 10:
            return QColor("#2F4F4C");
        case 3:
        case 7:
        case 11:
            return QColor("#3D2442");
        case 4:
        case 8:
        case 12:
            return QColor("#00617C");
        default:
            return QColor("black");
        }
    } else {
        switch (m_date.month()) {
        case 1:
        case 2:
            return QColor("#08456F");
        case 3:
        case 4:
            return QColor("#234D48");
        case 5:
        case 6:
            return QColor("#520F47");
        case 7:
        case 8:
            return QColor("#002C4F");
        case 9:
        case 10:
            return QColor("#00667d");
        case 11:
        case 12:
            return QColor("#00667d");
        default:
            return QColor("black");
        }
    }
}

QString LMM_Meeting::monthlyRGBColor()
{
    QColor c(monthlyColor());
    return QVariant(c.red()).toString() + ", " + QVariant(c.green()).toString() + ", " + QVariant(c.blue()).toString();
}

QColor LMM_Meeting::monthlyLightColor()
{
    switch (m_date.month()) {
    case 1:
    case 2:
        return QColor("#DAE7F6");
    case 3:
    case 4:
        return QColor("#DAE7E1");
    case 5:
    case 6:
        return QColor("#E8E0E5");
    case 7:
    case 8:
        return QColor("#DAE7F6");
    case 9:
    case 10:
        return QColor("#00667d");
    case 11:
    case 12:
        return QColor("#00667d");
    default:
        return QColor("black");
    }
}

SortFilterProxyModel *LMM_Meeting::getBrotherList(int usefor)
{

    QHash<int, QByteArray> roles;
    roles[MySortFilterProxyModel::MyRoles::id] = "id";
    roles[MySortFilterProxyModel::MyRoles::name] = "name";
    roles[MySortFilterProxyModel::MyRoles::date] = "date";
    roles[MySortFilterProxyModel::MyRoles::date_val] = "date_val";
    roles[MySortFilterProxyModel::MyRoles::icon] = "icon";

    QStandardItemModel *itemmodel = new QStandardItemModel(0, 3, this);
    itemmodel->setItemRoleNames(roles);
    itemmodel->setHorizontalHeaderItem(0, new QStandardItem("Id"));
    itemmodel->setHorizontalHeaderItem(1, new QStandardItem("Name"));
    itemmodel->setHorizontalHeaderItem(2, new QStandardItem("Date"));
    itemmodel->setHorizontalHeaderItem(3, new QStandardItem("Icon"));

    QString w;
    switch (usefor) {
    case person::LMM_Chairman:
        w = "SELECT p1.firstname,"
            "       p1.lastname,"
            "       p2.*"
            "  FROM persons p1,"
            "       ("
            "           SELECT m.chairman id,"
            "                  CASE WHEN m.date IS NULL THEN 1000000 ELSE (abs(julianday(m.date) - julianday('%1') ) + (CASE WHEN julianday(m.date) >= julianday('%1') THEN 0 ELSE 0.5 END) + 1) END date_offset,"
            "                  m.date"
            "             FROM lmm_meeting m"
            "           UNION ALL"
            "           SELECT id,"
            "                  1000000 date_offset,"
            "                  NULL date"
            "             FROM persons"
            "       )"
            "       p2"
            "    LEFT JOIN unavailables u ON p1.id = u.person_id and u.active and '%3' BETWEEN u.start_date AND u.end_date"
            " WHERE p1.id = p2.id AND"
            "       p1.active AND"
            "       p1.usefor & %2 AND"
            "       u.person_id IS NULL"
            "       group by p2.id"
            "       having min(date_offset);";
        break;
    case person::Prayer:
        w = "SELECT p1.firstname,"
            "       p1.lastname,"
            "       p2.*"
            "  FROM persons p1,"
            "       ("
            "           SELECT m.prayer_beginning id,"
            "                  CASE WHEN m.date IS NULL THEN 1000000 ELSE (abs(julianday(m.date) - julianday('%1') ) + (CASE WHEN julianday(m.date) >= julianday('%1') THEN 0 ELSE 0.5 END) +1 ) END date_offset,"
            "                  m.date"
            "             FROM lmm_meeting m"
            "           UNION ALL"
            "           SELECT m.prayer_end id,"
            "                  CASE WHEN m.date IS NULL THEN 1000000 ELSE (abs(julianday(m.date) - julianday('%1') ) + (CASE WHEN julianday(m.date) >= julianday('%1') THEN 0 ELSE 0.5 END) +1 ) END date_offset,"
            "                  m.date"
            "             FROM lmm_meeting m"
            "           UNION ALL"
            "           SELECT id,"
            "                  1000000 date_offset,"
            "                  NULL date"
            "             FROM persons"
            "       )"
            "       p2"
            "    LEFT JOIN unavailables u ON p1.id = u.person_id and u.active and '%3' BETWEEN u.start_date AND u.end_date"
            " WHERE p1.id = p2.id AND"
            "       p1.active AND"
            "       p1.usefor & %2 AND"
            "       u.person_id IS NULL"
            "       group by p2.id"
            "       having min(date_offset);";
    }
    w.replace("%1", m_date.toString(Qt::ISODate), Qt::CaseSensitive);
    w.replace("%2", QString::number(usefor));
    ccongregation c;
    w.replace("%3", m_date.addDays(c.getMeetingDay(m_date, ccongregation::tms) - 1).toString(Qt::ISODate));
    sql_class *sql = &Singleton<sql_class>::Instance();
    sql_items items = sql->selectSql(w);

    QString nameFormat = sql->getSetting("nameFormat", "%2, %1");

    // empty row
    itemmodel->setRowCount(1);

    for (unsigned int i = 0; i < items.size(); i++) {
        QStandardItem *item = new QStandardItem();
        item->setData(items[i].value("id").toInt(), MySortFilterProxyModel::MyRoles::id);
        item->setData(nameFormat.arg(items[i].value("firstname").toString(),
                                     items[i].value("lastname").toString()),
                      MySortFilterProxyModel::MyRoles::name);
        QDate d = items[i].value("date").toDate();
        d = d.addDays(c.getMeetingDay(d, ccongregation::tms) - 1);
        item->setData(d.toString("yyyy-MM-dd"), MySortFilterProxyModel::MyRoles::date);
        item->setData(d, MySortFilterProxyModel::MyRoles::date_val);
        item->setData(items[i].value("firstname"), MySortFilterProxyModel::MyRoles::h_firstname);
        item->setData(items[i].value("lastname"), MySortFilterProxyModel::MyRoles::h_lastname);
        item->setData(items[i].value("date_offset"), MySortFilterProxyModel::MyRoles::h_offset_any);

        itemmodel->setRowCount(itemmodel->rowCount() + 1);
        itemmodel->setItem(static_cast<int>(i) + 1, item);
    }
    SortFilterProxyModel *sortmodel = new MySortFilterProxyModel(this, itemmodel, MySortFilterProxyModel::MyRoles::name, "brother");
    return sortmodel;
}
