#ifndef PRINTINGCONDITIONALS_H
#define PRINTINGCONDITIONALS_H

#include <QString>
#include <QList>
#include <QRegularExpression>
//#include <QFile>
//#include <QTextStream>

#define DeleteAndClear(list) qDeleteAll(list);list.clear();

class printingconditionals
{
public:
    enum tokens {
        tUnknown = 0,
        tIf = 1,
        tElse = 2,
        tElseIf = 3,
        tEndIf = 4,
        tEnd = 5,
        tEq = 6,
        tNeq = 7,
        tLs = 8,
        tGr = 14,
        tLsEq = 15,
        tGrEq = 16,
        tHas = 17,
        tNhas = 18,
        tEmpty = 19,
        tNempty = 20,
        tNemptyDefault = 21,
        tAnd = 22,
        tOr = 23,
        tToBeRemoved = 24,
        tToken = 25 // makes searching for tokens more efficient
    };

    enum compstates {
        sNotset,
        sMatch,
        sNomatch
    };

    class condition {
    public:
        condition();
        int conditionend();
        tokens token;
        int conditionpos;
        int conditionlen;
        compstates compstate;
    };

    printingconditionals();
    printingconditionals(QString ifthentag);
    ~printingconditionals();
    QString tokenize(QString html);
    QString compute(QString html);
    void findoption(QStringRef text, QList<QString> options, int* selPos, int* selIdx);
    void pastetoken(QString* html, tokens t, int pos, int len, bool includeid, bool cleanspacebefore, bool cleanspaceafter, int* offset);
    void _compute();
    void _expandtoelement();
    void _render();

private:
    void init();
    QString ifthentag;
    QString iftag;
    QString elsetag;
    QString elseiftag;
    QString endiftag;

    QString html;
    QString results;
    QList<condition*> conditions;
};

#endif // PRINTINGCONDITIONALS_H
