/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "helpviewer.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <ciso646>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

#ifdef Q_OS_MACOS
    setUnifiedTitleAndToolBarOnMac(true);
    MacHelper::colorizeTitleBar(this->windowHandle());
    // Use QML_USE_GLYPHCACHE_WORKAROUND variable on macOS.
    // QML texts become distorted and unreadable on some Mac devices but not all.
    // This bug occurs with Qt 5.15. Probably bug in OpenGL driver...
    qputenv("QML_USE_GLYPHCACHE_WORKAROUND", "1");
#endif
    ui->quickWidget->setAttribute(Qt::WA_AcceptTouchEvents);

    sql = &Singleton<sql_class>::Instance();
    zipr = &Singleton<zipper>::Instance();
    this->setAcceptDrops(true);

    connect(sql, &sql_class::dbChanged, this, &MainWindow::databaseChanged);

    // create main toolbar
    QToolBar *mainToolBar = new QToolBar(this);
    mainToolBar->setOrientation(Qt::Vertical);
    mainToolBar->setMovable(false);
    mainToolBar->setStyleSheet("QToolBar{ background-color: #424242; border: none; margin: 0; padding: 0; spacing: 0; qproperty-iconSize: 24px 24px; }"
                               "QToolButton{ padding: 12px; border: none; }"
                               "QToolButton:hover{ background-color: #5b7bbf; }"
                               "QToolButton:checked{ background-color: #002860; }");
    mainToolBar->addAction(ui->actionHome);
    mainToolBar->addAction(ui->actionPublishers);
    mainToolBar->addAction(ui->actionSpeakers);
    mainToolBar->addAction(ui->actionTerritories);
    mainToolBar->addAction(ui->actionPrint);
    mainToolBar->addAction(ui->actionReminders);
    mainToolBar->addAction(ui->actionDataExchange);

    QWidget *spacer = new QWidget(this);
    spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    mainToolBar->addWidget(spacer);
    mainToolBar->addAction(ui->actionSettings);

    for (QAction *a : mainToolBar->actions()) {
        if (a->objectName() == "")
            continue;
        a->setCheckable(true);
        a->setIcon(general::changeIconColor(a->icon(), QColor("#eeeeee")));
    }

    QActionGroup *aGroup = new QActionGroup(mainToolBar);
    aGroup->addAction(ui->actionHome);
    aGroup->addAction(ui->actionPublishers);
    aGroup->addAction(ui->actionSpeakers);
    aGroup->addAction(ui->actionTerritories);
    aGroup->addAction(ui->actionPrint);
    aGroup->addAction(ui->actionReminders);
    aGroup->addAction(ui->actionDataExchange);
    aGroup->addAction(ui->actionSettings);
    aGroup->setExclusive(true);
    ui->actionHome->setChecked(true);
    // disable context menu in main toolbar
    mainToolBar->setContextMenuPolicy(Qt::PreventContextMenu);
    ui->leftToolbarArea->layout()->addWidget(mainToolBar);
    activeButton = ui->actionHome;

    ui->dockTimeLine->setContextMenuPolicy(Qt::PreventContextMenu);
    // button to show app updates
    ui->toolButtonAppUpdates->setIcon(general::changeIconColor(ui->toolButtonAppUpdates->icon(), QColor("white")));
    ui->toolButtonAppUpdates->setVisible(false);
    connect(ui->toolButtonAppUpdates, &QToolButton::clicked, this, &MainWindow::on_actionCheckUpdates_triggered);

    // create 'more'-menu
    ui->toolButtonMore->setIcon(general::changeIconColor(ui->toolButtonMore->icon(), QColor("white")));
    QMenu *menuMore = new QMenu();
    menuMore->addAction(ui->actionHelp);
    menuMore->addAction(ui->actionReport_bug);
    menuMore->addAction(ui->actionFeedback);
    menuMore->addAction(ui->actionTheocBase_net);
    menuMore->addAction(ui->actionStartup_Screen);
    menuMore->addAction(ui->actionCheckUpdates);
    menuMore->addAction(ui->actionAbout);
    ui->toolButtonMore->setMenu(menuMore);
    ui->toolButtonMore->setArrowType(Qt::NoArrow);
    ui->toolButtonMore->setPopupMode(QToolButton::InstantPopup);

    helpViewer = new HelpViewer;

    createCalendarPopup();

    hw = new historytable(this);
    connect(hw, &historytable::visibleChanged, this, &MainWindow::historyWindowVisibleChanged);
    ui->dockTimeLine->setVisible(false);

    // register C++ classes in the QML
    qmlRegisterType<person>("net.theocbase", 1, 0, "Publisher");
    qRegisterMetaType<person::UseFor>("UseFor");
    qmlRegisterSingletonType<cpersons>("net.theocbase", 1, 0, "CPersons", [](QQmlEngine *engine, QJSEngine *scriptEngine) -> QObject * {
        Q_UNUSED(engine);
        Q_UNUSED(scriptEngine);
        return new cpersons();
    });
    qmlRegisterType<LMM_Schedule>("net.theocbase", 1, 0, "LMM_Schedule");
    qmlRegisterType<LMM_Meeting>("net.theocbase", 1, 0, "LMM_Meeting");
    qmlRegisterType<LMM_Assignment>("net.theocbase", 1, 0, "LMM_Assignment");
    qmlRegisterType<LMM_AssignmentContoller>("net.theocbase", 1, 0, "AssignmentController");
    qmlRegisterType<SortFilterProxyModel>("net.theocbase", 1, 0, "SortFilterProxyModel");
    qmlRegisterType<publicmeeting_controller>("net.theocbase", 1, 0, "PublicMeetingController");
    qmlRegisterType<cptmeeting>("net.theocbase", 1, 0, "CPTMeeting");
    qmlRegisterType<schoolStudentStudy>("net.theocbase", 1, 0, "StudentStudy");
    qmlRegisterType<ccongregation>("net.theocbase", 1, 0, "CongregationCtrl");
    qmlRegisterType<DataObjectListModel>("net.theocbase", 1, 0, "DataObjectListModel");
    qmlRegisterType<territory>("net.theocbase", 1, 0, "Territory");
    qmlRegisterType<TerritoryTreeSFProxyModel>("net.theocbase", 1, 0, "TerritoryTreeSFProxyModel");
    qmlRegisterType<cterritories>("net.theocbase", 1, 0, "Territories");
    qmlRegisterType<GeocodeResult>("net.theocbase", 1, 0, "GeocodeResult");
    qmlRegisterType<StreetResult>("net.theocbase", 1, 0, "StreetResult");
    qmlRegisterType<StreetResultModel>("net.theocbase", 1, 0, "StreetResultModel");
    qmlRegisterType<StreetResultSortFilterProxyModel>("net.theocbase", 1, 0, "StreetResultSFProxyModel");
    qmlRegisterType<CSVSchema>("net.theocbase", 1, 0, "CSVSchema");
    qmlRegisterType<OutgoingSpeakersModel>("net.theocbase", 1, 0, "OutgoingSpeakersModel");
    qmlRegisterType<TodoModel>("net.theocbase", 1, 0, "TodoModel");
    qmlRegisterType<wtimport>("net.theocbase", 1, 0, "WTImport");
    qmlRegisterType<MapSettings>("net.theocbase", 1, 0, "MapSettings");
    qmlRegisterType<ShareUtils>("net.theocbase", 1, 0, "ShareUtils");

    ui->quickWidget->setClearColor(Qt::transparent);
    ui->quickWidget->setSource(QUrl("qrc:/qml/main.qml"));
    QFont defaultfont;
    int defaultfontsize = defaultfont.pointSize() + (qApp->platformName() == "windows" ? 1 : 0);
    ui->quickWidget->rootObject()->setProperty("defaultfontsize", defaultfontsize);
    connect(ui->quickWidget->rootObject(), SIGNAL(importClicked()),
            this, SLOT(onScheduleImportClicked()));
    connect(ui->quickWidget->rootObject(), SIGNAL(fileDropped(QUrl)),
            this, SLOT(onFileDropped(QUrl)));
    connect(ui->quickWidget->rootObject(), SIGNAL(publicMeetingEdit()),
            SLOT(on_button_EditPublicMeeting_clicked()));
    connect(ui->quickWidget->rootObject(), SIGNAL(gotoNextWeek()), this, SLOT(on_toolButtonNextWeek_clicked()));
    ui->toolButtonNextWeek->setIcon(general::changeIconColor(ui->toolButtonNextWeek->icon(), QColor("white")));
    connect(ui->quickWidget->rootObject(), SIGNAL(gotoPreviousWeek()), this, SLOT(on_toolButtonPreviousWeek_clicked()));
    ui->toolButtonPreviousWeek->setIcon(general::changeIconColor(ui->toolButtonPreviousWeek->icon(), QColor("white")));
    connect(ui->quickWidget->rootObject(), SIGNAL(sidebarWidthChanged(int)), this, SLOT(sidebarWidthChanged(int)));
    connect(ui->quickWidget->rootObject(), SIGNAL(showTimeline()), hw, SLOT(showHistory()));

    //read colors from QML
    QColor materialBackground = ui->quickWidget->rootObject()->property("materialBackground").value<QColor>();
    QColor materialForeground = ui->quickWidget->rootObject()->property("materialForeground").value<QColor>();
    QColor materialPrimary = ui->quickWidget->rootObject()->property("materialPrimary").value<QColor>();
    QColor materialAccent = ui->quickWidget->rootObject()->property("materialAccent").value<QColor>();
    QPalette p = palette();
    p.setColor(QPalette::Window, materialBackground);
    p.setColor(QPalette::WindowText, materialForeground);
    p.setColor(QPalette::Text, materialForeground);
    p.setColor(QPalette::ButtonText, materialForeground);
    p.setColor(QPalette::Mid, materialPrimary);
    p.setColor(QPalette::BrightText, materialAccent);
    qApp->setPalette(p);

    // button menu for import
    QMenu *m = new QMenu(this);
    QAction *ata1ks = new QAction("Ta1ks", this);
    connect(ata1ks, SIGNAL(triggered()), SLOT(buttonImportXmlTa1ks_clicked()));
    m->addAction(ata1ks);
    ui->buttonImportXml->setMenu(m);

    initCloud();
    applyAuthorizationRules();
    infoinit();
    //oDate->setDate(QDate::fromString("2016-01-04",Qt::ISODate));
    if (firstdayofweek.year() >= 2016)
        ui->stackedWidget->setCurrentIndex(3);
    else
        ui->stackedWidget->setCurrentIndex(0);
    updateinfo();

    // check available cloud updates
    if (loggedCloud && !QVariant(sql->getSetting("local_changes", "false")).toBool()) {
        QTimer *cloudtimer = new QTimer(this);
        cloudtimer->setSingleShot(true);
        connect(cloudtimer, &QTimer::timeout, [=]() {
            cloud->checkCloudUpdates();
            cloudtimer->deleteLater();
        });
        cloudtimer->start(5000);
    }

    // check software update
    checkupdates *chkUpd = new checkupdates(qApp->applicationVersion(), 15);
    chkUpd->sb = ui->statusBar;
    connect(chkUpd, SIGNAL(updatesFound()), this, SLOT(updatesFound()));
    chkUpd->start();

    ui->menuBar->setVisible(false);
    ui->statusBar->setVisible(false);
}

void MainWindow::changeActivePage(QWidget *w)
{
    QAction *action = qobject_cast<QAction *>(sender());
    if (action && action == ui->actionHistory)
        return;

    if (!saveCloseActivePage())
        return;

    if (transactionStarted) {
        // commit or rollback??
        if (QMessageBox::question(this, "", QObject::tr("Save changes?"),
                                  QMessageBox::No, QMessageBox::Yes)
            == QMessageBox::Yes) {
            sql->commitTransaction();
        } else {
            sql->rollbackTransaction();
        }
    }

    if (w == ui->quickWidget) {
        // schedule
        ui->stackedWidget->setCurrentIndex(3);
        ui->labelTopBar->setText(w->property("title").toString());
        ui->labelBibleReading->setVisible(true);
        ui->labelBibleReading->setText(w->property("bibleReading").toString());
        ui->toolButtonCalendar->setVisible(true);
        ui->toolButtonPreviousWeek->setVisible(true);
        ui->toolButtonNextWeek->setVisible(true);
    } else {
        ui->stackedWidget->setCurrentIndex(ui->stackedWidget->addWidget(w));
        ui->labelTopBar->setText(w->windowTitle().toUpper());
        ui->labelBibleReading->setVisible(false);
        ui->toolButtonCalendar->setVisible(false);
        ui->toolButtonPreviousWeek->setVisible(false);
        ui->toolButtonNextWeek->setVisible(false);
        w->installEventFilter(this);
    }
    hw->setVisible(w == ui->quickWidget || w == ui->page_PublicMeeting);
    activeButton = action;
}

bool MainWindow::saveCloseActivePage()
{
    if (ui->stackedWidget->count() > 5) {
        QWidget *old = ui->stackedWidget->widget(5);
        if (!old->close()) {
            activeButton->setChecked(true);
            return false;
        }
        delete old;
    }
    return true;
}

void MainWindow::infoinit(bool resetDate)
{
    if (resetDate) {
        firstdayofweek.setDate(QDate::currentDate().year(), QDate::currentDate().month(), QDate::currentDate().day());
        firstdayofweek = firstdayofweek.addDays((QDate::currentDate().dayOfWeek() - 1) * -1);
    }

    loggedCloud = cloud->logged();
    setCloudIcon(loggedCloud, QVariant(sql->getSetting("local_changes", "false")).toBool(), false);
    QDateTime dt = cloud->lastSyncTime();
    ui->toolButtonCloud->setToolTip(loggedCloud ? tr("Last synchronized") + ": " + dt.toString(Qt::DefaultLocaleShortDate) : "");

    ui->toolButtonPreviousWeek->setIcon(
            general::changeIconColor(
                    QIcon(isLeftToRight() ? ":/icons/chevron_left.svg" : ":/icons/chevron_right.svg"),
                    QColor("white")));
    ui->toolButtonNextWeek->setIcon(
            general::changeIconColor(
                    QIcon(isLeftToRight() ? ":/icons/chevron_right.svg" : ":/icons/chevron_left.svg"),
                    QColor("white")));
}

MainWindow::~MainWindow()
{
    delete helpViewer;
    delete ui;
}

void MainWindow::on_actionPublishers_triggered()
{
    if (!saveCloseActivePage())
        return;
    personsui *pd = new personsui(this);
    connect(pd, SIGNAL(updateScheduledTalks(int, int, int, bool)),
            ui->wPublicTalkEdit, SLOT(updateScheduledTalks(int, int, int, bool)));
    changeActivePage(pd);
}

void MainWindow::on_actionSettings_triggered()
{
    if (!saveCloseActivePage())
        return;
    Settings *s = new Settings(this);
    connect(s, SIGNAL(discontinueTalk(int)),
            ui->wPublicTalkEdit, SLOT(updateScheduledTalks(int)));
    connect(s, &Settings::destroyed, [=]() {
        ui->retranslateUi(this);
        infoinit(false);
        updateinfo();
    });
    s->setCloud(cloud);
    changeActivePage(s);
}

void MainWindow::on_actionExit_triggered()
{
    this->close();
}
void MainWindow::on_actionPrint_triggered()
{
    if (!saveCloseActivePage())
        return;
    printui *pr = new printui(firstdayofweek, this);
    pr->setCloud(cloud);
    changeActivePage(pr);
}

void MainWindow::on_toolButtonNextWeek_clicked()
{
    oDate->setDate(oDate->date().addDays(7));
}

void MainWindow::on_toolButtonPreviousWeek_clicked()
{
    oDate->setDate(oDate->date().addDays(-7));
}

void MainWindow::on_actionHome_triggered()
{
    if (activeButton == ui->actionHome)
        oDate->setDate(QDate::currentDate());
    changeActivePage(ui->quickWidget);
}

void MainWindow::on_actionReport_bug_triggered()
{
    QUrl url("http://www.theocbase.net/support-forum.html");
    url.setScheme("http");
    QDesktopServices::openUrl(url);
    //ui->stackedWidget->setCurrentIndex(5);
}

void MainWindow::on_button_EditSchool_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);
    updateinfo();
}

void MainWindow::on_button_EditPublicMeeting_clicked()
{
    ui->stackedWidget->setCurrentIndex(2);
    updateinfo();
    if (!hw->windowMinimized()) {
        hw->refreshHistory(true);
    }
}

void MainWindow::on_button_ReturnSchool_clicked()
{
    // return from school page
    ui->stackedWidget->setCurrentIndex(0);
    updateinfo();
}

void MainWindow::on_button_ReturnPublicTalk_clicked()
{
    // return from public talk page
    ui->stackedWidget->setCurrentIndex(firstdayofweek.year() >= 2016 ? 3 : 0);
    updateinfo();
    if (!hw->windowMinimized()) {
        hw->refreshHistory(false);
    }
}

void MainWindow::updateinfo()
{
    QObject *obj = qobject_cast<QObject *>(ui->quickWidget->rootObject());
    QVariant retVal;
    QMetaObject::invokeMethod(obj, "setDate",
                              Q_RETURN_ARG(QVariant, retVal),
                              Q_ARG(QVariant, firstdayofweek.startOfDay()));
    ui->labelTopBar->setText("");
    ui->toolButtonCalendar->setText(tr("WEEK STARTING %1").arg(firstdayofweek.toString(Qt::DefaultLocaleShortDate)));
    ui->labelBibleReading->setText(QQmlProperty::read(obj, "bibleReading").toString());

    ui->quickWidget->setProperty("title", ui->labelTopBar->text());
    ui->quickWidget->setProperty("bibleReading", ui->labelBibleReading->text());

    hw->firstdayofweek = firstdayofweek;

    ccongregation c;
    c.clearExceptionCache();
    myCongregation = c.getMyCongregation();

    // Check if exceptions on current week
    ccongregation::exceptions e = c.isException(firstdayofweek);
    int day2 = c.getMeetingDay(firstdayofweek, ccongregation::pm);

    switch (e) {
    case ccongregation::None:
        ui->label_8->setText(firstdayofweek.addDays(day2 - 1).toString(Qt::DefaultLocaleLongDate));
        break;
    case ccongregation::CircuitOverseersVisit:
        ui->label_8->setText(firstdayofweek.addDays(day2 - 1).toString(Qt::DefaultLocaleLongDate));
        break;
    case ccongregation::CircuitAssembly:
    case ccongregation::RegionalConvention:
        ui->label_8->setText(tr("Convention week (no meeting) "));
        break;
    default:
        if (day2 == 0) {
            ui->label_8->setText(tr("No meeting"));
        } else {
            ui->label_8->setText(firstdayofweek.addDays(day2 - 1).toString(Qt::DefaultLocaleLongDate));
        }
    }

    if (ui->stackedWidget->currentIndex() == 2) {
        // Edit page for public meeting
        ui->wPublicTalkEdit->setDate(firstdayofweek);
    }

    if (ui->stackedWidget->currentIndex() == 5) {
        on_spPreviousWeeks_valueChanged(ui->spPreviousWeeks->value());
    }
}

void MainWindow::on_actionFeedback_triggered()
{
    QUrl url("http://www.theocbase.net/support-forum.html");
    url.setScheme("http");
    QDesktopServices::openUrl(url);
    //ui->stackedWidget->setCurrentIndex(6);
}

void MainWindow::calendarClicked(QDate date)
{
    qDebug() << date.toString(Qt::ISODate);
    ui->actionDate->setText(oDate->date().toString(Qt::DefaultLocaleShortDate));
    firstdayofweek = oDate->date().addDays((oDate->date().dayOfWeek() - 1) * -1);
    calPopup->close();
    updateinfo();
}

void MainWindow::on_actionTheocBase_net_triggered()
{
    QUrl url("http://theocbase.net");
    url.setScheme("http");
    QDesktopServices::openUrl(url);
}

void MainWindow::on_actionAbout_triggered()
{
    QString abouttext;
    abouttext.append(QString("<p>TheocBase %1</p>").arg(qApp->applicationVersion()));
    abouttext.append("</br>");
    abouttext.append(QString("<p>%1 (c) 2010-2018, %2</p>").arg(tr("Copyright"), tr("TheocBase Team")));
    abouttext.append(QString("<p>%1</p>"
                             "<p><a href='%2'>%2</a></p>")
                             .arg(tr("Licensed under GPLv3."),
                                  "http://www.gnu.org/licenses/gpl.html"));
    abouttext.append("</br>");
    abouttext.append(QString("<p>%1</p>"
                             "<p>%2 %3</p>")
                             .arg(tr("Qt libraries licensed under the GPL."),
                                  tr("Versions of Qt libraries "),
                                  QVariant(qVersion()).toString()));
    abouttext.append("</br>");

    QString localfilepath = QDir::tempPath() + "/theocbase_credits.txt";
    if (QFile::exists(localfilepath))
        QFile::remove(localfilepath);
    QFile::copy(":/credits.txt", localfilepath);
    abouttext.append(QString("<p><a href='%1'>Third party credits...</a></p>").arg(QUrl::fromLocalFile(localfilepath).toString()));

    QMessageBox::about(this, "TheocBase", abouttext);
}

//tietojen tuonti ja vienti lehdelle
void MainWindow::on_actionDataExchange_triggered()
{
    if (!saveCloseActivePage())
        return;
    hw->setVisible(false);

    QDate fromDate = firstdayofweek.addDays(1 - firstdayofweek.day());
    fromDate = fromDate.addDays(1 - fromDate.dayOfWeek());
    if (fromDate.month() != firstdayofweek.month())
        fromDate = fromDate.addDays(7);
    QDate thruDate = firstdayofweek.addMonths(1);
    thruDate = thruDate.addDays(-thruDate.day());
    thruDate = thruDate.addDays(1 - thruDate.dayOfWeek());

    ui->thruDate->setDate(thruDate);
    ui->fromDate->setDate(fromDate);

    ui->tableWidgetSyncReport->setRowCount(0);
    ui->tableWidgetSyncReport->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    ui->button_ImportOK->setEnabled(false);
    ui->button_ImportCancel->setEnabled(false);
    ui->stackedWidget->setCurrentIndex(4);
    ui->labelTopBar->setText(tr("Data exchange").toUpper());
    ui->toolButtonCalendar->setVisible(false);
    ui->labelBibleReading->setVisible(false);
    ui->progressBar->setVisible(false);

    // iCal
    ui->ck_iCalGroupByDate->setChecked(QVariant(sql->getSetting("iCalGroupByDate", "0")).toBool());
    ui->ck_iCalAllDay->setChecked(QVariant(sql->getSetting("iCalAllDay", "1")).toBool());
}

void MainWindow::on_actionSpeakers_triggered()
{
    if (!saveCloseActivePage())
        return;
    speakersui *sp = new speakersui(this);
    connect(sp, SIGNAL(updateScheduledTalks(int, int, int, bool)),
            ui->wPublicTalkEdit, SLOT(updateScheduledTalks(int, int, int, bool)));
    connect(sp, &speakersui::destroyed, [=]() {
        updateinfo();
    });
    changeActivePage(sp);
}

void MainWindow::on_actionCheckUpdates_triggered()
{
    checkupdates *chkUpdates = new checkupdates(qApp->applicationVersion());
    if (chkUpdates->checkUpdatesFromServer()) {
        if (QMessageBox::question(this, "", tr("New update available. Do you want to install?"), QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes) {
            chkUpdates->startUpdateInstall();
        }
    } else {
        QMessageBox::information(this, "TheocBase", tr("No new update available"));
    }
    //    chkUpdates->start();
}

// Export xml and send or save file
void MainWindow::on_pushButton_lahetaTiedot_clicked()
{
    // iCal
    sql->saveSetting("iCalGroupByDate", QVariant(ui->ck_iCalGroupByDate->isChecked()).toString());
    sql->saveSetting("iCalAllDay", QVariant(ui->ck_iCalAllDay->isChecked()).toString());

    bool asXML = ui->rbXML->isChecked();

    if (asXML && ui->chk_ExportOutgoing->isChecked()) {
        QMessageBox::information(this, "TheocBase", tr("Exporting outgoing speakers not ready yet, sorry."));
        return;
    }
    if (!asXML && ui->chk_ExportStudyHistory->isChecked()) {
        QMessageBox::information(this, "TheocBase", tr("Exporting study history to iCal is not supported"));
        return;
    }

    QSettings settings;
    QString filePath;
    if (asXML) {
        filePath = QFileDialog::getSaveFileName(this, tr("Save file"),
                                                settings.value("data_exchange/path", QDir::homePath()).toString(),
                                                "TheocBase-file (*.thb)");
    } else {
        filePath = QFileDialog::getExistingDirectory(this, tr("Save folder"),
                                                     settings.value("data_exchange/path", QDir::homePath()).toString());
    }

    if (filePath == "")
        return;

    if (asXML) {
        exportXML(filePath);
    } else {
        exportICal(filePath);
    }

    QMessageBox::information(this, "", tr("Saved successfully"));
}

void MainWindow::exportXML(QString filename)
{
    QDate tempdate = ui->fromDate->date();
    int weeks = ui->spPreviousWeeks->value();

    if (!filename.endsWith(".thb"))
        filename += ".thb";
    if (QFile::exists(filename))
        QFile::remove(filename);

    csync syn;
    syn.CreateXMLFile(filename, tempdate, weeks,
                      ui->chk_ExportPublishers->isChecked(),
                      ui->chk_ExportSpeakers->isChecked(),
                      ui->chk_MidweekMeeting->isChecked(),
                      ui->chk_ExportStudyHistory->isChecked(),
                      ui->chk_ExportPublicTalks->isChecked(),
                      ui->chk_ExportOutgoing->isChecked());

    QSettings settings;
    QFileInfo info(filename);
    settings.setValue("data_exchange/path", info.path());
}

void MainWindow::exportICal(QString filePath)
{
    qDebug() << "-------------- export iCal ----------------";
    QTime midnight;
    midnight.setHMS(0, 0, 0);
    QHash<QString, iCal::VCALENDAR *> calendars;
    bool groupByDate(QVariant(sql->getSetting("iCalGroupByDate", "0")).toBool());
    qint64 startTime(0);
    if (!QVariant(sql->getSetting("iCalAllDay", "0")).toBool())
        startTime = QTime::fromString(myCongregation.time_meeting1, "hh:mm").msecsSinceStartOfDay() / 1000;

    ccongregation cong;
    QDate date(ui->fromDate->date());
    QDate thruDate(ui->thruDate->date().addDays(-6));
    QString dateRange = date.toString(Qt::ISODate) + " - " + thruDate.toString(Qt::ISODate);

    // TODO: using a radio button to choose midweek/weekend/both for iCal export would be a better choice...
    bool exportMidweek = ui->chk_MidweekMeeting->isChecked();
    bool exportWeekend = ui->chk_ExportPublicTalks->isChecked();
    if (not exportMidweek and not exportWeekend)
        exportMidweek = true; // at least export something...

    for (; date <= thruDate; date = date.addDays(7)) {
        qDebug() << "date: " << date;
        if (exportMidweek) {
            QSharedPointer<LMM_Meeting> mtg(new LMM_Meeting());
            mtg->loadMeeting(date, true);
            int mtgDay = cong.getMeetingDay(date, ccongregation::tms) - 1;
            QDateTime startDate(date.startOfDay().addDays(mtgDay).addMSecs(startTime));
            QDateTime endDate(startDate.addSecs(60 * 60 * 1.75));

            if (groupByDate) {
                addToCalendarsByDate(&calendars, mtg->chairman(), nullptr, startDate, endDate, tr("Chairman"));
                addToCalendarsByDate(&calendars, mtg->counselor2(), nullptr, startDate, endDate, tr("Counselor-Class II"));
                addToCalendarsByDate(&calendars, mtg->counselor3(), nullptr, startDate, endDate, tr("Counselor-Class III"));
                addToCalendarsByDate(&calendars, mtg->prayerBeginning(), nullptr, startDate, endDate, tr("Prayer"));
            } else {
                addToCalendarsByPerson(&calendars, nullptr, startDate, endDate, tr("Chairman"), tr("Chairman"));
                addToCalendarsByPerson(&calendars, nullptr, startDate, endDate, tr("Counselor-Class II"), tr("Counselor-Class II"));
                addToCalendarsByPerson(&calendars, nullptr, startDate, endDate, tr("Counselor-Class III"), tr("Counselor-Class III"));
                addToCalendarsByPerson(&calendars, nullptr, startDate, endDate, tr("Prayer"), "");
            }

            QList<LMM_Assignment *> prog = mtg->getAssignments();
            for (LMM_Assignment *a : prog) {
                QStringList parts;
                parts << tr("Source", "short for Source material") + ": " + a->source() << tr("Timing") + ": " + QString::number(a->time());

                LMM_Assignment_ex *exPart = nullptr;

                if (a->canHaveAssistant() && a->speaker()) {
                    exPart = (LMM_Assignment_ex *)a;
                    if (!groupByDate)
                        addToCalendarsByPerson(&calendars, exPart->assistant(), startDate, endDate,
                                               tr("Assistant to %1", "%1 is student's name").arg(a->speaker()->fullname()) + ", " + a->theme(),
                                               parts.join(", "));
                }
                if (a->talkId() == LMM_Schedule::TalkType_CBS) {
                    exPart = (LMM_Assignment_ex *)a;
                    if (!groupByDate)
                        addToCalendarsByPerson(&calendars, exPart->assistant(), startDate, endDate,
                                               tr("Reader for Congregation Bible Study"),
                                               tr("Source", "short for Source material") + ": " + a->source());
                }

                if (groupByDate) {
                    addToCalendarsByDate(
                            &calendars,
                            a->speaker(),
                            exPart ? exPart->assistant() : nullptr,
                            startDate,
                            endDate,
                            a->theme());
                } else {
                    addToCalendarsByPerson(
                            &calendars,
                            a->speaker(),
                            startDate,
                            endDate,
                            a->theme(),
                            parts.join(", "));
                }
            }

            if (groupByDate) {
                addToCalendarsByDate(&calendars, mtg->prayerEnd(), nullptr, startDate, endDate, tr("Prayer"));
            } else {
                addToCalendarsByPerson(&calendars, mtg->prayerEnd(), startDate, endDate, tr("Prayer"), "");
            }
        }

        if (exportWeekend) {
            // retreive exact start date time of the public meeting
            QString talk_time = cong.getMyCongregation().getPublicmeeting(date).getMeetingtime();
            qint64 talk_startTime = QVariant(sql->getSetting("iCalAllDay", "0")).toBool()
                    ? 0
                    : QTime::fromString(talk_time, "hh:mm").msecsSinceStartOfDay() / 1000;

            int talk_day = cong.getMeetingDay(date, ccongregation::pm) - 1;
            if (talk_day < 0) {
                // No meeting: do we want to export this ? It may depend on the exception !!!
            } else {
                QDateTime startDate(date.startOfDay().addDays(talk_day).addSecs(talk_startTime));
                QDateTime endDate(startDate.addSecs(60 * 60 * 1.75));

                cpublictalks cpt;
                QSharedPointer<cptmeeting> meeting(cpt.getMeeting(date));

                if (groupByDate) {
                    qDebug() << "   ==> iCal/weekend: groupByDate";
                    QString key = QObject::tr("All weekend meetings for", "Filename prefix for weekend meetings iCal export");
                    if (!calendars.contains(key))
                        calendars.insert(key, new iCal::VCALENDAR("theocbase"));
                    iCal::VCALENDAR *c = calendars[key];

                    iCal::VEVENT *ev;
                    if (c->LastEvent() == NULL || c->LastEvent()->start() != startDate) {
                        ev = new iCal::VEVENT();
                        c->AddEvent(ev);
                        ev->setStart(startDate);
                        ev->setEnd(endDate);
                        ev->setLocation(myCongregation.address.length() == 0 ? tr("Kingdom Hall") : myCongregation.address);
                        ev->setSummary(QObject::tr("Weekend Meeting"));
                    } else
                        ev = c->LastEvent();

                    QString description =
                            "◼ " + tr("Chairman") + "\r\n◻ " + (meeting->chairman() ? meeting->chairman()->fullname() : QString()) + "\r\n\r\n" + "◼ " + tr("Public Talk") + "\r\n◻ " + (meeting->theme.id > 0 ? meeting->theme.theme : QString()) + "\r\n◻ " + (meeting->speaker() ? meeting->speaker()->fullname() : QString()) + " (" + (meeting->speaker() ? meeting->speaker()->congregationName() : QString()) + ")" + "\r\n\r\n" + "◼ " + tr("Watchtower Study") + "\r\n◻ " + meeting->wttheme + "\r\n◻ " + (meeting->wtConductor() ? meeting->wtConductor()->fullname() : QString()) + " / " + (meeting->wtReader() ? meeting->wtReader()->fullname() : QString());
                    if (ev->description().length() == 0)
                        ev->setDescription(description);
                    else
                        ev->setDescription(ev->description() + "\r\n\r\n" + description);
                } else // groupByPerson
                {
                    qDebug() << "   ==> iCal/weekend: groupByPerson";

                    endDate = startDate.addSecs(60 * 5);
                    if (meeting->chairman())
                        addToCalendarsByPerson(&calendars, meeting->chairman(), startDate, endDate, tr("Chairman"), tr("Chairman"));

                    startDate = endDate;
                    endDate = startDate.addSecs(60 * 30);
                    //if (ui->chk_ExportPublicTalks->isChecked())
                    if (meeting->speaker() && meeting->speaker()->congregationid() == cong.getMyCongregation().id)
                        addToCalendarsByPerson(&calendars, meeting->speaker(), startDate, endDate, tr("Speaker"), tr("Speaker"));

                    startDate = endDate;
                    endDate = startDate.addSecs(60 * (5 + 60 + 5));
                    if (meeting->wtConductor())
                        addToCalendarsByPerson(&calendars, meeting->wtConductor(), startDate, endDate, tr("Watchtower Study Conductor"), tr("Watchtower Study Conductor"));
                    if (meeting->wtReader())
                        addToCalendarsByPerson(&calendars, meeting->wtReader(), startDate, endDate, tr("Watchtower reader"), tr("Watchtower reader"));
                }
            } // endif no meeting else
        } // endif weekend

        if (ui->chk_ExportOutgoing->isChecked()) {
            struct Outgoing {
                QString congregation;
                QString address;
                QDateTime date;
                person *speaker;
                QString theme;
                int theme_number;
            };
            std::vector<Outgoing> outgoing;

            qDebug() << "   collecting outgoing...";
            sql_item args;
            QDate firstDayOfWeek = date.addDays(1 - date.dayOfWeek());
            QDate lastDayOfWeek = firstDayOfWeek.addDays(6);
            args.insert(":firstDayOfWeek", firstDayOfWeek);
            args.insert(":lastDayOfWeek", lastDayOfWeek);
            sql_items values = sql->selectSql("SELECT * FROM outgoing "
                                              "WHERE date >= :firstDayOfWeek AND date <= :lastDayOfWeek AND active",
                                              &args);

            for (auto &value : values) {
                Outgoing out;
                // where he goes
                auto congregation = cong.getCongregationById(value.value("congregation_id").toInt());
                out.congregation = congregation.name;
                out.address = congregation.address;
                // when
                //qDebug() << "    date1 = " << value.value("date").toString();
                //qDebug() << "    date1 = " << value.value("date").toDate().toString("yyyyMMdd---HHmmss");
                //qDebug() << "    date2 = " << QDateTime::fromString(value.value("date").toString(), "yyyy-MM-dd").toString("yyyyMMddTHHmmss");
                //qDebug() << "    time  = " << congregation.time_meeting1;
                auto day_time = congregation.getPublicmeeting(QDate::fromString(value.value("date").toString(), "yyyy-MM-dd"));
                if (day_time.getMeetingtime().isEmpty())
                    out.date = QDateTime::fromString(value.value("date").toString(), "yyyy-MM-dd");
                else
                    out.date = QDateTime::fromString(value.value("date").toString() + " " + day_time.getMeetingtime(), "yyyy-MM-dd hh:mm");
                out.date = out.date.addDays(cong.getMeetingDay(out.date.date(), ccongregation::pm) - 1);
                // who does the talk
                out.speaker = cpersons::getPerson(value.value("speaker_id").toInt());
                // what theme
                cpublictalks ptalkclass;
                auto ctheme = ptalkclass.getThemeById(value.value("theme_id").toInt());
                out.theme = ctheme.theme;
                out.theme_number = ctheme.number;

                outgoing.push_back(out);
                qDebug() << "    out.congreg = " << out.congregation;
                qDebug() << "    out.address = " << out.address;
                qDebug() << "    out.date    = " << out.date.toString("yyyyMMddTHHmmss");
                qDebug() << "    out.speaker = " << (out.speaker ? out.speaker->fullname() : QString("?"));
                qDebug() << "    out.theme   = " << out.theme;
                qDebug() << "    -------------";
            }
            qDebug() << "       ==> outgoing.size()" << outgoing.size();

            // retreive exact start date time of the public meeting
            QString talk_time = cong.getMyCongregation().getPublicmeeting(date).getMeetingtime();
            qint64 talk_startTime = QVariant(sql->getSetting("iCalAllDay", "0")).toBool()
                    ? 0
                    : QTime::fromString(talk_time, "hh:mm").msecsSinceStartOfDay() / 1000;

            int talk_day = cong.getMeetingDay(date, ccongregation::pm) - 1;
            if (talk_day < 0) {
                // No meeting: do we want to export this ? It may depend on the exception !!!
            } else {
                QDateTime startDate(date.startOfDay().addDays(talk_day).addSecs(talk_startTime));
                QDateTime endDate(startDate.addSecs(60 * 60 * 1.75));

                cpublictalks cpt;
                QSharedPointer<cptmeeting> meeting(cpt.getMeeting(date));

                if (groupByDate) {
                    qDebug() << "   ==> iCal/outgoing: groupByDate";
                    QString key = QObject::tr("All outgoing talks for", "File name prefix for outgoing talks iCal export");
                    if (!calendars.contains(key))
                        calendars.insert(key, new iCal::VCALENDAR("theocbase"));
                    iCal::VCALENDAR *c = calendars[key];

                    if (not outgoing.empty()) {
                        // sort talks by date
                        std::sort(outgoing.begin(), outgoing.end(), [](Outgoing const &o1, Outgoing const &o2) {
                            return o1.date < o2.date;
                        });

                        // first, create a new event, because we don't want to reuse the one of "our" congregation meeting
                        {
                            auto ev = new iCal::VEVENT();
                            c->AddEvent(ev);
                            ev->setStart(outgoing.front().date.date().startOfDay());
                            ev->setSummary(QObject::tr("Outgoing Talks"));
                        }

                        for (auto &out : outgoing) {
                            QDateTime fullDayDate = out.date.date().startOfDay();
                            iCal::VEVENT *ev;
                            if (c->LastEvent() == nullptr || c->LastEvent()->start().date() != out.date.date()) {
                                ev = new iCal::VEVENT();
                                c->AddEvent(ev);
                                ev->setStart(fullDayDate);
                                //ev->setEnd(fullDayDate);
                                ev->setSummary(QObject::tr("Outgoing Talks"));
                            } else
                                ev = c->LastEvent();

                            QString description =
                                    "◼ " + (out.speaker ? out.speaker->fullname() : QString()) + "\r\n◻ " + out.theme + " (" + QString::number(out.theme_number) + ")" + "\r\n◻ " + out.congregation;
                            if (out.date.time() != midnight)
                                description += " (" + out.date.toString("HH:mm") + ")";

                            qDebug() << description;

                            // append desc
                            if (ev->description().length() == 0)
                                ev->setDescription(description);
                            else
                                ev->setDescription(ev->description() + "\r\n\r\n" + description);
                        }
                    }
                } else // groupByPerson
                {
                    qDebug() << "   ==> iCal/outgoing: groupByPerson";
                    for (auto &out : outgoing) {
                        qDebug() << "            outgoing_talk(name=" << (out.speaker ? out.speaker->fullname() : QString("?")) << ")";

                        QString key = out.speaker->fullname();
                        if (!calendars.contains(key))
                            calendars.insert(key, new iCal::VCALENDAR("theocbase"));
                        iCal::VCALENDAR *c = calendars[key];

                        iCal::VEVENT *ev = new iCal::VEVENT();
                        c->AddEvent(ev);

                        ev->setStart(out.date);
                        ev->setEnd(out.date.addSecs(60 * 35));
                        ev->setLocation(out.address);
                        ev->setSummary(tr("Public Talk"));
                        ev->setDescription("◼ " + QObject::tr("Congregation") + "\r\n◻ " + out.congregation + "\r\n\r\n◼ " + QObject::tr("Theme") + "\r\n◻ " + out.theme + " (" + QString::number(out.theme_number) + ")");
                    }
                }
            } // endif no meeting else
        } // endif weekend and/or outgoing

    } // endloop dates

    if (!filePath.endsWith("/") && !filePath.endsWith("\\"))
        filePath.append("/");
    for (QString key : calendars.keys()) {
        QFile file(filePath + key + " " + dateRange + ".ics");
        if (QFile::exists(file.fileName())) {
            QFile::remove(file.fileName());
        }
        file.open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream out(&file);
        out.setCodec("UTF-8");
        iCal::VCALENDAR *c = calendars[key];
        out << c->toString();
        out.flush();
        file.close();
    }
}

void MainWindow::addToCalendarsByPerson(QHash<QString, iCal::VCALENDAR *> *calendars,
                                        person *assigned,
                                        QDateTime startDate,
                                        QDateTime endDate,
                                        QString summary,
                                        QString description)
{
    if (assigned) {
        QString key = assigned->fullname();
        if (!calendars->contains(key))
            calendars->insert(key, new iCal::VCALENDAR("theocbase"));
        iCal::VCALENDAR *c = (*calendars)[key];

        iCal::VEVENT *ev = new iCal::VEVENT();
        c->AddEvent(ev);

        ev->setStart(startDate);
        ev->setEnd(endDate);
        ev->setLocation(myCongregation.address.length() == 0 ? tr("Kingdom Hall") : myCongregation.address);
        ev->setSummary(summary);
        ev->setDescription(description);
    }
}

void MainWindow::addToCalendarsByDate(QHash<QString, iCal::VCALENDAR *> *calendars,
                                      person *assigned,
                                      person *assistant,
                                      QDateTime startDate,
                                      QDateTime endDate,
                                      QString summary)
{
    if (assigned) {
        QString key = "All for";
        if (!calendars->contains(key))
            calendars->insert(key, new iCal::VCALENDAR("theocbase"));
        iCal::VCALENDAR *c = (*calendars)[key];

        iCal::VEVENT *ev;
        if (c->LastEvent() == NULL || c->LastEvent()->start() != startDate) {
            ev = new iCal::VEVENT();
            c->AddEvent(ev);
            ev->setStart(startDate);
            ev->setEnd(endDate);
            ev->setLocation(myCongregation.address.length() == 0 ? tr("Kingdom Hall") : myCongregation.address);
            ev->setSummary(QObject::tr("Midweek Meeting"));
        } else
            ev = c->LastEvent();

        QString assignment = assigned->fullname();
        if (assistant)
            assignment += " / " + assistant->fullname();
        QString description = "◼ " + summary + "\r\n◻ " + assignment;
        if (ev->description().length() == 0)
            ev->setDescription(description);
        else
            ev->setDescription(ev->description() + "\r\n\r\n" + description);
    }
}

void MainWindow::initCloud()
{
    cloud = new cloud_controller(this);

    connect(cloud, &cloud_controller::loginRequired, QDesktopServices::openUrl);
    connect(cloud, &cloud_controller::loginRequired, ui->toolButtonCloud, &DropboxSyncButton::closePanel);
    connect(cloud, &cloud_controller::loggedChanged, [=](bool logged) {
        // logged changed
        loggedCloud = logged;
        cloudStateChanged(cloud->syncState());
        infoinit(false);
        updateinfo();
        reloadActivePage();
        applyAuthorizationRules();
    });
    connect(cloud, &cloud_controller::stateChanged, this, &MainWindow::cloudStateChanged);

    connect(cloud, &cloud_controller::syncStarted, ui->toolButtonCloud, &DropboxSyncButton::startIconAnimation);
    connect(cloud, &cloud_controller::syncConflict, [=](int v) {
        bool local = (QMessageBox::question(this,
                                            "TheocBase Cloud", tr("The same changes can be found both locally and in the cloud (%1 rows). "
                                                                  "Do you want to keep the local changes?")
                                                                       .arg(QVariant(v).toString()))
                      == QMessageBox::Yes);

        cloud->continueSynchronize(local);
    });
    connect(cloud, &cloud_controller::syncFinished, [=] {
        // synchronization ready
        // refresh page
        ui->toolButtonCloud->stopIconAnimation();
        infoinit(false);
        updateinfo();
        activeButton = nullptr;
        reloadActivePage();
        applyAuthorizationRules();
    });
    AccessControl *ac = &Singleton<AccessControl>::Instance();
    connect(ac, &AccessControl::userChanged, [=] {
        if (ui->toolButtonCloud->panel()->rootObject())
            ui->toolButtonCloud->panel()->rootObject()->setProperty("canDeleteCloudData", ac->user() && ac->user()->hasPermission(PermissionRule::CanDeleteCloudData));
    });
    connect(cloud, &cloud_controller::error, [=](QString message) {
        QMessageBox::information(this, "TheocBase Cloud", message);
        ui->toolButtonCloud->stopIconAnimation();
    });
    connect(cloud, &cloud_controller::differentLastDbUser, [=] {
        // DB synchronized last time by other cloud user!
        if (QMessageBox::question(this, "TheocBase Cloud", "The database has been synchronized last time by other cloud user."
                                                           "Do you still want to continue?")
            == QMessageBox::Yes) {
            cloud->synchronize(true);
        } else {
            ui->toolButtonCloud->stopIconAnimation();
        }
    });
    connect(cloud, &cloud_controller::cloudResetStarted, ui->toolButtonCloud, &DropboxSyncButton::startIconAnimation);
    connect(cloud, &cloud_controller::cloudResetFinished, [=]() {
        ui->toolButtonCloud->stopIconAnimation();
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Question);
        msgBox.setText(tr("The cloud data has now been deleted."));
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);

        QAbstractButton *yesButton = msgBox.button(QMessageBox::Yes);
        yesButton->setText(tr("Synchronize"));
        QAbstractButton *noButton = msgBox.button(QMessageBox::No);
        noButton->setText(tr("Sign Out"));
        msgBox.exec();
        if (msgBox.clickedButton() == yesButton) {
            cloud->synchronize();
        } else {
            cloud->logout(0);
        }
    });
    connect(cloud, &cloud_controller::cloudResetFound, [=] {
        if (QMessageBox::question(this, "TheocBase Cloud",
                                  tr("The cloud data has been deleted. Your local data will be replaced. Continue?"),
                                  QMessageBox::Yes | QMessageBox::Cancel)
            == QMessageBox::Yes) {
            // clear database
            cloud->clearDatabase();
            // start new synchronization
            on_toolButtonCloud_clicked();
        } else {
            ui->toolButtonCloud->stopIconAnimation();
        }
    });

    ui->toolButtonCloud->panel()->rootContext()->setContextProperty("cloud", cloud);

    cloud->initAccessControl();
}

void MainWindow::applyAuthorizationRules()
{
    AccessControl *ac = &Singleton<AccessControl>::Instance();
    User *user = ac->user();

    // uncomment the following lines to remove admin role for testing
    //    foreach (const Role *role, user->roles()) {
    //        if (role->id() == Permission::RoleId::Administrator)
    //            user->removeRole(role);
    //    }
    //    qDebug() << "CanEditPermissions" << user->hasPermission(Permission::Rule::CanEditPermissions);

    ui->actionPublishers->setVisible(user && (user->hasPermission(Permission::Rule::CanViewPublishers)));
    ui->actionSpeakers->setVisible(user && (user->hasPermission(Permission::Rule::CanViewPublicSpeakers)));
    ui->actionTerritories->setVisible(user && (user->hasPermission(Permission::Rule::CanViewTerritories)));
    ui->actionReminders->setVisible(user && (user->hasPermission(Permission::Rule::CanSendMidweekMeetingReminders)));
    ui->actionDataExchange->setVisible(user && (user->hasPermission(Permission::Rule::CanEditPublishers) || user->hasPermission(Permission::Rule::CanEditPublicSpeakers) || user->hasPermission(Permission::Rule::CanEditMidweekMeetingSchedule) || user->hasPermission(Permission::Rule::CanEditWeekendMeetingSchedule)));
    if ((ui->actionPublishers->isChecked() && !ui->actionPublishers->isVisible())
        || (ui->actionSpeakers->isChecked() && !ui->actionSpeakers->isVisible())
        || (ui->actionTerritories->isChecked() && !ui->actionTerritories->isVisible())
        || (ui->actionReminders->isChecked() && !ui->actionReminders->isVisible())
        || (ui->actionDataExchange->isChecked() && !ui->actionDataExchange->isVisible()))
        on_actionHome_triggered();

    ui->quickWidget->rootObject()->setProperty("canViewMidweekMeetingSchedule", user->hasPermission(PermissionRule::CanViewMidweekMeetingSchedule));
    ui->quickWidget->rootObject()->setProperty("canEditMidweekMeetingSchedule", user->hasPermission(PermissionRule::CanEditMidweekMeetingSchedule));
    ui->quickWidget->rootObject()->setProperty("canViewWeekendMeetingSchedule", user->hasPermission(PermissionRule::CanViewWeekendMeetingSchedule));
    ui->quickWidget->rootObject()->setProperty("canEditWeekendMeetingSchedule", user->hasPermission(PermissionRule::CanEditWeekendMeetingSchedule));
    ui->quickWidget->rootObject()->setProperty("canSendMidweekMeetingReminders", user->hasPermission(PermissionRule::CanSendMidweekMeetingReminders));
    ui->quickWidget->rootObject()->setProperty("canViewMeetingNotes", user->hasPermission(PermissionRule::CanViewMeetingNotes));
    ui->quickWidget->rootObject()->setProperty("canEditMeetingNotes", user->hasPermission(PermissionRule::CanEditMeetingNotes));
}

void MainWindow::setCloudIcon(bool logged, bool localChanges, bool cloudChanges)
{
    QString iconname = "";
    if (!logged) {
        iconname = "cloud_offline.svg";
    } else if (localChanges) {
        iconname = "cloud_upload.svg";
    } else if (cloudChanges) {
        iconname = "cloud_download.svg";
    } else {
        iconname = "cloud_done.svg";
    }
    ui->toolButtonCloud->setIcon(general::changeIconColor(QIcon(":/icons/" + iconname), QColor("white")));
}

void MainWindow::cloudStateChanged(cloud_controller::SyncState state)
{
    switch (state) {
    case cloud_controller::Synchronized:
        setCloudIcon(loggedCloud, false, false);
        break;
    case cloud_controller::Upload:
        setCloudIcon(loggedCloud, true, false);
        break;
    case cloud_controller::Download:
        setCloudIcon(loggedCloud, false, true);
        break;
    case cloud_controller::Both:
        setCloudIcon(loggedCloud, true, true);
        break;
    }
}

void MainWindow::mailState(QString stateStr)
{
    QString msg;
    if (stateStr.contains("Error")) {
        msg = tr("Error sending e-mail") + "\n";
    } else {
        msg = tr("E-mail sent successfully") + "\n";
    }
    QMessageBox::information(this, "", msg + stateStr);
}

// Import xml file
void MainWindow::on_buttonImportXml_clicked()
{
    QSettings settings;
    QString filepath = QFileDialog::getOpenFileName(this, tr("Open file"),
                                                    settings.value("data_exchange/path", QDir::homePath()).toString(),
                                                    "TheocBase-file (*.thb);;XML-file (*.xml);;"
                                                    "TMS Explorer Students.csv file (*Students.csv);;"
                                                    "TMS Explorer Talk History.csv file (*Talk*History.csv);;"
                                                    "TMS Explorer Study History.csv file (*Study*History.csv);;"
                                                    "WINTM Student file (current.sd6)");

    if (filepath != "") {
        QString filepath_lower = filepath.toLower();
        if (filepath_lower.endsWith("sd6")) {
            this->setCursor(Qt::WaitCursor);
            importwintm importer(filepath);
            importer.Go();
            this->setCursor(Qt::ArrowCursor);
        } else {
            QFileInfo info(filepath);
            importXml(filepath);
            settings.setValue("data_exchange/path", info.path());
        }
    }
}

// Import Ta1ks data
void MainWindow::buttonImportXmlTa1ks_clicked()
{
    QSettings settings;
    QString ta1ksDirectory = QFileDialog::getExistingDirectory(this, tr("Open directory"),
                                                               settings.value("data_exchange/path", QDir::homePath()).toString(),
                                                               QFileDialog::ShowDirsOnly);

    if (ta1ksDirectory != "") {
        QStringList nameFilter("*.dat");
        QDir directory(ta1ksDirectory);
        QStringList ta1ksDatFiles = directory.entryList(nameFilter, QDir::Files);

        bool congDatExists = false;
        bool speakersDatExists = false;
        bool xtendDatExists = false;
        QString fileMissingMsg = "";

        if (ta1ksDatFiles.length() > 0) {
            congDatExists = ta1ksDatFiles.contains("cong.dat", Qt::CaseInsensitive);
            speakersDatExists = ta1ksDatFiles.contains("speakers.dat", Qt::CaseInsensitive);
            xtendDatExists = ta1ksDatFiles.contains("xtend.dat", Qt::CaseInsensitive);
        }

        if (!(congDatExists & speakersDatExists & xtendDatExists)) {
            if (!congDatExists) {
                fileMissingMsg += "\n    cong.dat";
            }
            if (!speakersDatExists) {
                fileMissingMsg += "\n    speakers.dat";
            }
            if (!xtendDatExists) {
                fileMissingMsg += "\n    xtend.dat";
            }

            QMessageBox::information(this, tr("Import Error"), tr("Could not import from Ta1ks. Files are missing:") + fileMissingMsg);
            return;
        }

        QApplication::setOverrideCursor(Qt::WaitCursor);
        importTa1ks importer(ta1ksDirectory);
        importer.Go();
        QApplication::restoreOverrideCursor();
    }
}

void MainWindow::importXml(QString filePath)
{
    // import xml file
    if (transactionStarted) {
        if (QMessageBox::question(this, "", tr("Save unsaved data?"),
                                  QMessageBox::No, QMessageBox::Yes)
            == QMessageBox::No) {
            sql->rollbackTransaction();
        } else {
            sql->commitTransaction();
        }
    }

    if (QMessageBox::question(this, "", tr("Import file?"),
                              QMessageBox::No, QMessageBox::Yes)
        == QMessageBox::No)
        return;
    QProgressDialog progress(tr("TheocBase data exchange"), "", 0, 100, this);
    progress.setCancelButton(0);
    progress.setWindowModality(Qt::WindowModal);
    progress.show();

    sql->startTransaction();
    csync s;
    connect(&s, &csync::newReportRow, this, &MainWindow::addSyncReportRow);
    connect(&s, &csync::progressBarChanged, &progress, &QProgressDialog::setValue);

    QFileInfo info(filePath);
    if (info.suffix().toLower() == "csv") {
        s.readFromTmsWare(filePath);
    } else {
        s.readXmlFile(filePath);
    }

    progress.close();
    ui->button_ImportOK->setEnabled(true);
    ui->button_ImportCancel->setEnabled(true);
}

void MainWindow::on_spPreviousWeeks_valueChanged(int arg1)
{
    // spinbox in export page. show how many week to export
    ui->fromDate->setDate(ui->thruDate->date().addDays(-7 * arg1 - 6));
}

void MainWindow::on_fromDate_dateChanged(const QDate &date)
{
    QDate fromDate = date.addDays(1 - date.dayOfWeek());
    if (fromDate.month() != date.month())
        fromDate = fromDate.addDays(7);
    if (fromDate != date)
        ui->fromDate->setDate(fromDate);
    if (fromDate > ui->thruDate->date()) {
        QDate thruDate = fromDate.addMonths(1);
        thruDate = thruDate.addDays(-thruDate.day());
        thruDate = thruDate.addDays(1 - thruDate.dayOfWeek()).addDays(6);
        ui->thruDate->setDate(thruDate);
    }
    ui->spPreviousWeeks->setValue((ui->thruDate->date().toJulianDay() - ui->fromDate->date().toJulianDay() - 6) / 7);
}

void MainWindow::on_thruDate_dateChanged(const QDate &date)
{
    QDate thruDate = date.addDays(1 - date.dayOfWeek());
    if (ui->fromDate->date() > thruDate)
        thruDate = ui->fromDate->date();
    thruDate = thruDate.addDays(6);
    if (thruDate != date)
        ui->thruDate->setDate(thruDate);
}

void MainWindow::on_button_ImportOK_clicked()
{
    // import xml - accept changes
    sql->commitTransaction();
    ui->button_ImportOK->setEnabled(false);
    ui->button_ImportCancel->setEnabled(false);
}

void MainWindow::on_button_ImportCancel_clicked()
{
    // import xml - discard changed
    sql->rollbackTransaction();
    ui->button_ImportOK->setEnabled(false);
    ui->button_ImportCancel->setEnabled(false);
}

void MainWindow::addSyncReportRow(QString text, csync::SyncType typ)
{
    Q_UNUSED(typ);
    ui->tableWidgetSyncReport->setRowCount(ui->tableWidgetSyncReport->rowCount() + 1);
    ui->tableWidgetSyncReport->setItem(ui->tableWidgetSyncReport->rowCount() - 1, 0, new QTableWidgetItem(text));
}

void MainWindow::on_chk_ExportPublishers_clicked(bool checked)
{
    if (!checked) {
        ui->chk_MidweekMeeting->setChecked(false);
        ui->chk_ExportStudyHistory->setChecked(false);
    }
}

void MainWindow::on_chk_ExportSpeakers_clicked(bool checked)
{
    if (!checked) {
        ui->chk_ExportPublicTalks->setChecked(false);
        ui->chk_ExportOutgoing->setChecked(false);
    }
}

void MainWindow::on_chk_ExportStudyHistory_clicked(bool checked)
{
    if (checked)
        ui->chk_ExportPublishers->setChecked(true);
}

void MainWindow::on_chk_MidweekMeeting_clicked(bool checked)
{
    if (checked)
        ui->chk_ExportPublishers->setChecked(true);
}

void MainWindow::databaseChanged(QString tablename)
{
    if (!loggedCloud)
        return;

    if (tablename == "e_reminder" || tablename == "lmm_workbookregex")
        return;

    sql->saveSetting("local_changes", "true");
    setCloudIcon(true, true, false);
}

void MainWindow::on_chk_ExportPublicTalks_clicked(bool checked)
{
    if (checked)
        ui->chk_ExportSpeakers->setChecked(true);
}

void MainWindow::on_chk_ExportOutgoing_clicked(bool checked)
{
    if (checked)
        ui->chk_ExportSpeakers->setChecked(true);
}

// open help
void MainWindow::on_actionHelp_triggered()
{
    helpViewer->showHelp("index.html");
}

// drop file to theocbase
void MainWindow::dropEvent(QDropEvent *event)
{
    qDebug() << "dropEvent";
    const QMimeData *mimeData = event->mimeData();
    // check for our needed mime type, here a file or a list of files
    if (mimeData->hasUrls()) {
        QStringList pathList;
        QList<QUrl> urlList = mimeData->urls();

        // extract the local paths of the files
        for (int i = 0; i < urlList.size() && i < 32; ++i) {
            pathList.append(urlList.at(i).toLocalFile());
            //QMessageBox::information(this,"TheocBase",urlList.at(i).toLocalFile());
            QFileInfo info(urlList.at(i).toLocalFile());
            if (info.suffix() != "xml" && info.suffix() != "thb")
                return;

            this->on_actionDataExchange_triggered();
            importXml(urlList.at(i).toLocalFile());
            ui->tabWidgetTransfer->setCurrentIndex(1);
            return;
        }

        // call a function to open the files
    }
}

void MainWindow::dragEnterEvent(QDragEnterEvent *event)
{
    qDebug() << "dragenterevent";
    if (event->mimeData()->hasUrls())
        event->acceptProposedAction();
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    int historyheight = hw->windowMinimized() ? hw->height() : this->height() / 3;
    if (hw->windowMinimized())
        hw->setGeometry(ui->stackedWidget->pos().x() + ui->stackedWidget->width() - hw->minimizedWidth(),
                        this->height() - historyheight,
                        hw->minimizedWidth(),
                        historyheight);
    else {

        hw->setGeometry(ui->stackedWidget->pos().x(),
                        this->height() - historyheight,
                        ui->stackedWidget->width() - sidebarWidth,
                        historyheight);
    }
    event->accept();
}

bool MainWindow::eventFilter(QObject *watched, QEvent *event)
{
    if (watched == ui->stackedWidget->currentWidget() && event->type() == QEvent::KeyPress) {
        QKeyEvent *ke = static_cast<QKeyEvent *>(event);
        if (ke->key() == Qt::Key_Escape)
            return true;
    }
    return false;
}

void MainWindow::showPersonsList(QString defaultname, bool speaker)
{
    Q_UNUSED(speaker);
    personsui *p = new personsui(this);
    connect(p, SIGNAL(updateScheduledTalks(int, int, int, bool)),
            ui->wPublicTalkEdit, SLOT(updateScheduledTalks(int, int, int, bool)));
    p->setModal(true);

    if (defaultname != "")
        p->setDefaultPerson(defaultname);

    p->show();
    p->exec();
    delete p;
}

void MainWindow::showEvent(QShowEvent *event)
{
    QWidget::showEvent(event);

    if (event->spontaneous())
        return;

    if (this->openfile != "")
        receiveMessage(this->openfile);

    // Restore saved geometry and state of the main screen
    QSettings settings;
    restoreGeometry(settings.value("mainwindow/windowGeometry").toByteArray());
    restoreState(settings.value("mainwindow/windowState").toByteArray());

    // history table is visible - load content
    ui->dockTimeLine->close();

    // Show Startup screen
    QTimer *timer = new QTimer(this);
    timer->setSingleShot(true);
    connect(timer, &QTimer::timeout, this, &MainWindow::on_actionStartup_Screen_triggered);
    timer->start(1000);
    timer->deleteLater();
    sql->saveSetting("screen", "false");

    event->accept();
}

void MainWindow::on_actionHistory_triggered()
{
}

void MainWindow::changeFullScreen()
{
    if (this->isMaximized()) {
        this->showNormal();
    } else {
        this->showMaximized();
    }
    ui->actionFullScreen->setChecked(this->isMaximized());
}

void MainWindow::receiveMessage(QString msg)
{
    //QMessageBox::information(this,"",msg);
    QFileInfo info(msg);
    if (info.suffix() != "xml" && info.suffix() != "thb")
        return;

    this->on_actionDataExchange_triggered();
    importXml(msg);
    ui->tabWidgetTransfer->setCurrentIndex(1);
}

void MainWindow::on_actionStartup_Screen_triggered()
{

    QObject *obj = qobject_cast<QObject *>(ui->quickWidget->rootObject());

    QAction *action = qobject_cast<QAction *>(sender());
    bool forceLoad = action && action == ui->actionStartup_Screen;

    QMetaObject::invokeMethod(obj, "openStartPage", Q_ARG(QVariant, forceLoad));
}

// MainWindow close event
void MainWindow::closeEvent(QCloseEvent *event)
{
    saveCloseActivePage();

    // Send School reminders if this settings has selected
    if (QVariant(sql->getSetting("reminder_send_onclosing")).toBool()) {
        reminders *rdialog = new reminders(this);
        if (rdialog->remindersCount() > 0) {
            if (QMessageBox::question(this, "", tr("Send e-mail reminders?"), QMessageBox::No, QMessageBox::Yes) == QMessageBox::Yes) {

                rdialog->setWindowFlags(Qt::Sheet);
                rdialog->setWindowModality(Qt::WindowModal);
                rdialog->exec();
            }
        }
        delete rdialog;
    }

    // Save state and geometry of window
    QSettings settings;
    settings.setValue("mainwindow/windowState", this->saveState());
    settings.setValue("mainwindow/windowGeometry", this->saveGeometry());

    event->accept();
}

void MainWindow::on_actionReminders_triggered()
{
    if (!saveCloseActivePage())
        return;
    reminders *rdialog = new reminders(this);
    connect(rdialog, &reminders::sent, [=]() {
        // activate schedule page after sending
        ui->actionHome->setChecked(true);
        on_actionHome_triggered();
    });
    changeActivePage(rdialog);
}

void MainWindow::updatesFound()
{
    ui->toolButtonAppUpdates->setVisible(true);
    ui->toolButtonAppUpdates->setToolTip(tr("Updates available..."));
}

void MainWindow::on_toolButtonCloud_clicked()
{
    if (!loggedCloud) {
        QMessageBox::information(this, "", "Not logged to cloud!");
        return;
    }

    if (ui->actionPublishers->isChecked() && ui->stackedWidget->count() > 5) {
        // publishers page is active -> save changes before the sync
        personsui *pui = qobject_cast<personsui *>(ui->stackedWidget->widget(5));
        if (pui && !pui->saveChanges())
            return;
    }

    cloud->synchronize();
}

void MainWindow::createCalendarPopup()
{
    calPopup = new QFrame(this, Qt::Popup);
    calPopup->setFrameShape(QFrame::Box);
    QHBoxLayout *layout = new QHBoxLayout;
    oDate = new QDateEdit();
    oDate->setCalendarPopup(true);
    oDate->setDate(QDate::currentDate());
    QCalendarWidget *oCal = oDate->calendarWidget();
    oCal->setFirstDayOfWeek(Qt::Monday);
    layout->addWidget(oCal);
    calPopup->setLayout(layout);

    connect(oDate, &QDateEdit::dateChanged, this, &MainWindow::calendarClicked);
}

void MainWindow::onScheduleImportClicked(QString filepath)
{
    // back door to enter regex editor
    if (QGuiApplication::keyboardModifiers() & Qt::ControlModifier) {
        lmmWorksheetRegEx *dialog = new lmmWorksheetRegEx(this);
        dialog->show();
        dialog->exec();
        return;
    }

    if (filepath.isEmpty())
        filepath = QFileDialog::getOpenFileName(this, tr("Select ePub file"),
                                                QStandardPaths::writableLocation(QStandardPaths::DesktopLocation),
                                                "ePub -file (*.epub)");

    if (filepath != "") {
        QString results;
        QSharedPointer<importlmmworkbook> mwb(new importlmmworkbook(filepath));
        results = mwb->Import();
        while (results.startsWith("#ERR#")) {
            lmmtalktypeedit talktypeedit;
            talktypeedit.Init();
            if (talktypeedit.exec() == QDialog::Rejected)
                break;
            mwb.reset(new importlmmworkbook(filepath));
            results = mwb->Import();
        }
        results = results.replace("#ERR#", "");
        QMessageBox::information(this, "", results);

        // refresh page
        updateinfo();
    }
}

void MainWindow::onFileDropped(QUrl url)
{
    qDebug() << "LOCAL FILE" << url.toLocalFile();
    QFileInfo info(url.toLocalFile());
    QString fileExt = info.suffix();
    if (fileExt.compare("epub", Qt::CaseInsensitive) == 0) {
        onScheduleImportClicked(url.toLocalFile());
    } else if (fileExt.compare("thb", Qt::CaseInsensitive) == 0) {
        this->on_actionDataExchange_triggered();
        importXml(url.toLocalFile());
        ui->tabWidgetTransfer->setCurrentIndex(1);
    }
}

void MainWindow::on_rbXML_clicked(bool checked)
{
    ui->ck_iCalAllDay->setVisible(!checked);
    ui->ck_iCalGroupByDate->setVisible(!checked);
}

void MainWindow::on_rbiCal_clicked(bool checked)
{
    ui->ck_iCalAllDay->setVisible(checked);
    ui->ck_iCalGroupByDate->setVisible(checked);
}

void MainWindow::on_actionTerritories_triggered()
{
    if (!saveCloseActivePage())
        return;
    territorymanagement *te = new territorymanagement(this);
    changeActivePage(te);
}

void MainWindow::on_toolButtonCalendar_clicked()
{
    int y = ui->toolButtonCalendar->pos().y() + ui->toolButtonCalendar->height();
    int x = ui->toolButtonCalendar->pos().x();
    calPopup->move(mapToGlobal(QPoint(x, y)));
    calPopup->show();
}

void MainWindow::on_buttonPreviousWeek_clicked()
{
    oDate->setDate(oDate->date().addDays(-7));
}

void MainWindow::on_buttonNextWeek_clicked()
{
    oDate->setDate(oDate->date().addDays(7));
}

void MainWindow::historyWindowVisibleChanged(bool visible)
{
    resizeEvent(new QResizeEvent(size(), size()));
    if (visible)
        hw->refreshHistory(ui->stackedWidget->currentWidget() == ui->page_PublicMeeting);
}

void MainWindow::reloadActivePage()
{
    if (ui->actionHome->isChecked()) {
        on_actionHome_triggered();
    } else if (ui->actionPublishers->isChecked()) {
        on_actionPublishers_triggered();
    } else if (ui->actionSpeakers->isChecked()) {
        on_actionSpeakers_triggered();
    } else if (ui->actionTerritories->isChecked()) {
        on_actionTerritories_triggered();
    } else if (ui->actionPrint->isChecked()) {
        on_actionPrint_triggered();
    } else if (ui->actionReminders->isChecked()) {
        on_actionReminders_triggered();
    } else if (ui->actionDataExchange->isChecked()) {
        on_actionDataExchange_triggered();
    }
}

void MainWindow::sidebarWidthChanged(const int width)
{
    sidebarWidth = width;
    if (!hw->isMinimized())
        resizeEvent(new QResizeEvent(size(), size()));
}
