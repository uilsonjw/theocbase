import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.3
import net.theocbase.mobile 1.0

Rectangle {
    LMM_Meeting{
        id: myMeeting
        function updateOnCounselorChange() {
            lmmListView.model = myMeeting.getAssignmentsVariant()
        }
        onCounselor2Changed: updateOnCounselorChange()
        onCounselor3Changed: updateOnCounselorChange()
    }
    AssignmentController { id: myController }    
    property string bibleReadering
    property int length: lmmListView.model ? lmmListView.model.length : 0
    property var t
    property bool showTime: false
    property int looptest: 0
    property bool noMultiSchool: myMeeting.date.getFullYear() < 2018 && myMeeting.date.getDate() < 8
    property bool editpossible: true

    Layout.fillWidth: true

    function loadSchedule(currentdate){
        showTime = settings_ui.showTime
        // load meeting data
        myMeeting.loadMeeting(currentdate)                

        // song beginning
        t = myMeeting.startTime
        songBeginningRow.timeText.text = showTime ? Qt.formatTime(t, Qt.DefaultLocaleShortDate) : ""
        t.setMinutes(t.getMinutes()+5)
        // opening comments
        var ocTime = myMeeting.date.getFullYear() < 2020 ? 3 : 1
        openingCommentsRow.timeText.text = showTime ? Qt.formatTime(t, Qt.DefaultLocaleShortDate) : ocTime.toString()
        t.setMinutes(t.getMinutes()+ocTime)
        // assignments
        lmmListView.model = myMeeting.getAssignmentsVariant()

        coServiceTalkRow.visible = false
        // calculate listView height
        var listViewHeight = lmmListView.model.length > 0 ? 150*dpi : 0
        for(var i=0;i<lmmListView.model.length;i++){
            if (lmmListView.model[i].talkId === LMM_Schedule.TalkType_COTalk){
                coServiceTalkRow.visible = true
                coServiceTalkRow.themeText.text = lmmListView.model[i].theme
                coServiceTalkRow.nameText1.text = lmmListView.model[i].speakerFullName
            }else if (!lmmListView.model[i].canMultiSchool || lmmListView.model[i].classnumber === tabView.currentIndex + 1){
                listViewHeight += 40*dpi
            }
        }
        var t2 = new Date(myMeeting.startTime)
        t2.setMinutes(t2.getMinutes()+(coServiceTalkRow.visible ? 67 : 97))
        // concluding comments
        reviewCommentsRow.timeText.text = showTime ? Qt.formatTime(t2, Qt.DefaultLocaleShortDate) : "3"
        t2.setMinutes(t2.getMinutes()+3)
        // co service talk (it is after review comments)
        if (coServiceTalkRow.visible){
            coServiceTalkRow.timeText.text = showTime ? Qt.formatTime(t2, Qt.DefaultLocaleShortDate) : 30
            t2.setMinutes(t2.getMinutes()+30)
        }

        // song end
        songEndRow.timeText.text = showTime ? Qt.formatTime(t2, Qt.DefaultLocaleShortDate) : ""

        lmmListView.height = listViewHeight
        lmmListView.Layout.preferredHeight = listViewHeight

        chairmanRow.visible = (listViewHeight > 0)
        songBeginningRow.visible = (listViewHeight > 0)
        songEndRow.visible = (listViewHeight > 0)
        height = listViewHeight
        height += tabView.height
        height += coServiceTalkRow.height
        height += 6*40*dpi
        Layout.preferredHeight = height
        // bible reading
        bibleReadering = myMeeting.bibleReading
    }

    function editButtonClicked(index, replacepage){
        var assignment = lmmListView.model[index]
        if (replacepage === "undefined")
            replacepage = false

        var dialogType = ""
        var talkid = assignment.talkId
        switch(talkid){
        case LMM_Schedule.TalkType_BibleReading:
        case LMM_Schedule.TalkType_InitialCall:
        case LMM_Schedule.TalkType_ReturnVisit1:
        case LMM_Schedule.TalkType_ReturnVisit2:
        case LMM_Schedule.TalkType_ReturnVisit3:
        case LMM_Schedule.TalkType_BibleStudy:
        case LMM_Schedule.TalkType_StudentTalk:
        case LMM_Schedule.TalkType_MemorialInvitation:
            dialogType = "LMMStudentAssignmentDialog.qml";
            break;
        default:
            dialogType =  "LMMAssignmentDialog.qml";
            break;
        }
        var detailsPage
        if (replacepage) {
            detailsPage = stackView.replace(replacepage, Qt.resolvedUrl(dialogType),
                                            {currentAssignment: assignment, currentindex: index, modelLength: length},
                                            StackView.Immediate)
        } else {
            stackView.pop(null)
            detailsPage = stackView.clearAndPush(Qt.resolvedUrl(dialogType),
                                                 {currentAssignment: assignment, currentindex: index, modelLength: length})
        }
        detailsPage.onGotoNext.connect(function(){editButtonClicked(index+1,true)})
        detailsPage.onGotoPrevious.connect(function(){editButtonClicked(index-1,true)})
    }

    function getAssignmentByIndex(index){
        return lmmListView.model[index]
    }

    function getIconSource(index){
        switch(index){
        case LMM_Schedule.TalkType_Treasures: return "qrc:///LMM_bible.png"
        case LMM_Schedule.TalkType_SampleConversationVideo:
        case LMM_Schedule.TalkType_InitialCall:
        case LMM_Schedule.TalkType_ReturnVisit1:
        case LMM_Schedule.TalkType_ReturnVisit2:
        case LMM_Schedule.TalkType_ReturnVisit3:
        case LMM_Schedule.TalkType_BibleStudy:
        case LMM_Schedule.TalkType_ApplyYourself:
        case LMM_Schedule.TalkType_StudentTalk:
        case LMM_Schedule.TalkType_MemorialInvitation:
        case LMM_Schedule.TalkType_OtherFMVideoPart: return "qrc:///LMM_spike.png"
        case LMM_Schedule.TalkType_LivingTalk1: return "qrc:///LMM_sheep.png"
        default: return ""
        }
    }

    function getTextColor(index){
        switch(index){
        case LMM_Schedule.TalkType_Treasures:
        case LMM_Schedule.TalkType_Digging:
        case LMM_Schedule.TalkType_BibleReading: return Qt.rgba(101/255, 97/255, 100/255, 1)
        case LMM_Schedule.TalkType_SampleConversationVideo:
        case LMM_Schedule.TalkType_InitialCall:
        case LMM_Schedule.TalkType_ReturnVisit1:
        case LMM_Schedule.TalkType_ReturnVisit2:
        case LMM_Schedule.TalkType_ReturnVisit3:
        case LMM_Schedule.TalkType_BibleStudy:
        case LMM_Schedule.TalkType_ApplyYourself:
        case LMM_Schedule.TalkType_StudentTalk:
        case LMM_Schedule.TalkType_MemorialInvitation:
        case LMM_Schedule.TalkType_OtherFMVideoPart: return Qt.rgba(199/255, 137/255, 9/255, 1)
        case LMM_Schedule.TalkType_LivingTalk1:
        case LMM_Schedule.TalkType_LivingTalk2:
        case LMM_Schedule.TalkType_LivingTalk3:
        case LMM_Schedule.TalkType_CBS: return Qt.rgba(153/255, 19/255, 30/255, 1)
        case LMM_Schedule.TalkType_COTalk: return "red"
        default: return "white"
        }
    }

    function getBackgroundColor(index){
        switch(index){
        case LMM_Schedule.TalkType_Treasures:
        case LMM_Schedule.TalkType_Digging:
        case LMM_Schedule.TalkType_BibleReading: return Qt.rgba(101/255, 97/255, 100/255, 0.1)
        case LMM_Schedule.TalkType_SampleConversationVideo:
        case LMM_Schedule.TalkType_InitialCall:
        case LMM_Schedule.TalkType_ReturnVisit1:
        case LMM_Schedule.TalkType_ReturnVisit2:
        case LMM_Schedule.TalkType_ReturnVisit3:
        case LMM_Schedule.TalkType_BibleStudy:
        case LMM_Schedule.TalkType_ApplyYourself:
        case LMM_Schedule.TalkType_StudentTalk:
        case LMM_Schedule.TalkType_MemorialInvitation:
        case LMM_Schedule.TalkType_OtherFMVideoPart: return Qt.rgba(199/255, 137/255, 9/255, 0.1)
        case LMM_Schedule.TalkType_LivingTalk1:
        case LMM_Schedule.TalkType_LivingTalk2:
        case LMM_Schedule.TalkType_LivingTalk3:
        case LMM_Schedule.TalkType_CBS: return Qt.rgba(153/255, 19/255, 30/255, 0.1)
        case LMM_Schedule.TalkType_COTalk: return "white"
        default:return "white"
        }
    }


    function getHeaderText(index){
        switch(index){
        case LMM_Schedule.TalkType_Treasures:
        case LMM_Schedule.TalkType_Digging:
        case LMM_Schedule.TalkType_BibleReading: return qsTr("TREASURES FROM GOD'S WORD")
        case LMM_Schedule.TalkType_SampleConversationVideo:
        case LMM_Schedule.TalkType_InitialCall:
        case LMM_Schedule.TalkType_ReturnVisit1:
        case LMM_Schedule.TalkType_ReturnVisit2:
        case LMM_Schedule.TalkType_ReturnVisit3:
        case LMM_Schedule.TalkType_BibleStudy:
        case LMM_Schedule.TalkType_ApplyYourself:
        case LMM_Schedule.TalkType_StudentTalk:
        case LMM_Schedule.TalkType_MemorialInvitation:
        case LMM_Schedule.TalkType_OtherFMVideoPart: return qsTr("APPLY YOURSELF TO THE FIELD MINISTRY")
        case LMM_Schedule.TalkType_LivingTalk1:
        case LMM_Schedule.TalkType_LivingTalk2:
        case LMM_Schedule.TalkType_LivingTalk3:
        case LMM_Schedule.TalkType_CBS:
        case LMM_Schedule.TalkType_COTalk: return qsTr("LIVING AS CHRISTIANS")
        default: return ""
        }
    }

    ColumnLayout {
        id: layout
        anchors.fill: parent
        spacing: 0

        RowLayout {
            Layout.preferredHeight: 40 * dpi
            Label {
                text: "●●○ " + Qt.locale().dayName(mwDate.getDay(), Locale.LongFormat) +
                      " | " + qsTr("Midweek Meeting").toUpperCase()
                font.pointSize: app_info.fontsizeSmall
                color: Qt.rgba(47/255, 72/255, 112/255, 1)
                Layout.leftMargin: 5*dpi
                Layout.preferredHeight: 40*dpi
                Layout.alignment: Qt.AlignVCenter
                verticalAlignment: Text.AlignVCenter
                Layout.fillWidth: true
                elide: Text.ElideRight
            }
            Item {
                Layout.preferredHeight: 40 * dpi
                width: height
                ToolButton {
                    icon.source: myMeeting.notes ? "qrc:/notes-text.svg" : "qrc:/notes.svg"
                    icon.color: Qt.rgba(47/255, 72/255, 112/255, 1)
                    visible: canViewMeetingNotes && editpossible
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.right
                    onClicked: {
                        stackView.clearAndPush(Qt.resolvedUrl("MeetingNotes.qml"),
                                               { "pageTitle": qsTr("Midweek Meeting"), "meeting": myMeeting, "editable": canEditMeetingNotes })
                    }
                }
            }
        }

        ScheduleRowItem {
            id: importText
            timeText.text: ""
            Layout.fillWidth: true
            visible: length == 0 && editpossible && canEditMidweekMeetingSchedule
            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter                
                text: qsTr("<html><a href='#'>" + qsTr("Import Schedule...") + "</a></html>")
                onLinkActivated: {
                    console.log("Import schedule link clicked!")
                    shareUtils.openFile("epub")
                }                
            }
        }


        TabBar {
            id: tabView
            property int classqty: myMeeting.classes
            Layout.fillWidth: true
            Layout.preferredHeight: 40*dpi
            visible: classqty > 1
            Material.accent: "transparent"

            Repeater {
                model: tabView.classqty
                TabButton {
                    Material.accent: Qt.rgba(47/255, 72/255, 112/255, 1)
                    id: control
                    height: 40*dpi
                    text: modelData == 0 ? qsTr("MH","abbreviation for main hall") :
                                           modelData == 1 ? qsTr("A1", "abbreviation for auxiliary classroom 1") :
                                                            qsTr("A2", "abbreviation for auxiliary classroom 2")
                }
            }
        }
        Item {
            id: chairmanRow
            Layout.fillWidth: true
            Layout.leftMargin: 5*dpi
            Layout.preferredHeight: 40*dpi
            RowLayout {
                anchors.fill: parent
                Text {
                    id: chairmanLabel
                    Layout.fillWidth: true
                    font.pointSize: app_info.fontsize
                    text: tabView.currentIndex === 0 ? qsTr("Chairman") : qsTr("Counselor")
                    color: "grey"
                }

                Text {
                    font.pointSize: app_info.fontsize
                    Layout.fillWidth: true
                    horizontalAlignment: Text.AlignRight
                    text: tabView.currentIndex === 0 ?
                              (myMeeting.chairman ? myMeeting.chairman.fullname : "") :
                              tabView.currentIndex === 1 ? (myMeeting.counselor2 ? myMeeting.counselor2.fullname : "") :
                                                          (myMeeting.counselor3 ? myMeeting.counselor3.fullname : "")
                }
                Image {
                    Layout.rowSpan: 2
                    Layout.preferredHeight: 20*dpi
                    Layout.preferredWidth: 20*dpi
                    source: listRowMouseArea.enabled ? "qrc:///chevron_right.svg" : ""
                }
            }
            Rectangle {
                anchors.fill: parent
                color: "grey"
                opacity: listRowMouseArea.pressed ? 0.1 : 0
                MouseArea {
                    anchors.fill: parent
                    id: listRowMouseArea
                    enabled: canEditMidweekMeetingSchedule && editpossible
                    onClicked: {
                        var mylist = myMeeting.getChairmanList()
                        var rowindex;
                        switch (tabView.currentIndex)
                        {
                        case 0: rowindex = myMeeting.chairman ? mylist.find(myMeeting.chairman.id,1) : -1; break
                        case 1: rowindex = myMeeting.counselor2 ? mylist.find(myMeeting.counselor2.id,1) : -1; break
                        case 2: rowindex = myMeeting.counselor3 ? mylist.find(myMeeting.counselor3.id,1) : -1; break
                        }
                        var chairmanPage = stackView.clearAndPush(Qt.resolvedUrl("SelectionListPage.qml"),
                                                                  {showName: true, model: mylist, selectedRow: rowindex, pageTitle: chairmanLabel})
                        chairmanPage.onActiveRowChanged.connect(function(index){
                            var p = (typeof mylist.get(index).id === "undefined") ? null : myController.getPublisherById(mylist.get(index).id)
                            switch (tabView.currentIndex){
                            case 0: myMeeting.chairman = p; break
                            case 1: myMeeting.counselor2 = p; break
                            case 2: myMeeting.counselor3 = p; break
                            }
                            if (myMeeting.date.getFullYear() >= 2018) {
                                // sample conversation video
                                var video = myMeeting.getAssignment(LMM_Schedule.TalkType_SampleConversationVideo, 0, tabView.currentIndex+1)
                                if (video !== null) {
                                    video.speaker = p
                                    video.save()
                                }
                                // apply yourself to reading and teaching
                                var apply = myMeeting.getAssignment(LMM_Schedule.TalkType_ApplyYourself, 0, tabView.currentIndex+1)
                                if (apply !== null) {
                                    apply.speaker = p
                                    apply.save()
                                }
                            }
                            myMeeting.save()
                        })
                    }
                }
            }
        }

        ScheduleRowItem {
            id: songBeginningRow
            themeText.text: qsTr("Song %1 and Prayer").arg(myMeeting.songBeginning.toString()) +
                            (settings_ui.showSongTitles ? "\n" + myMeeting.songBeginningTitle : "")
            themeText.color: "grey"
            timeboxColor: showTime ? "grey" : "transparent"
            nameText1.text: myMeeting.prayerBeginning ? myMeeting.prayerBeginning.fullname : ""            
            imageSource: clickable ? "qrc:///chevron_right.svg" : ""
            clickable: canEditMidweekMeetingSchedule && editpossible
            showTopLine: true
            onClicked: {
                var mylist = myMeeting.getPrayerList()
                var rowindex = myMeeting.prayerBeginning ? mylist.find(myMeeting.prayerBeginning.id,1) : -1
                var speakersPage = stackView.clearAndPush(Qt.resolvedUrl("SelectionListPage.qml"),
                                                          {showName: true, model: mylist, selectedRow: rowindex, pageTitle: qsTr("Prayer")})
                speakersPage.onActiveRowChanged.connect(function(index){
                    myMeeting.prayerBeginning = (typeof mylist.get(index).id === "undefined") ? null : myController.getPublisherById(mylist.get(index).id)
                    myMeeting.save()
                })
            }
        }

        ScheduleRowItem {
            id: openingCommentsRow
            themeText.text: qsTr("Opening Comments")
            themeText.color: "grey"
            timeboxColor: "grey"
            visible: lmmListView.model.length > 0
            imageSource: clickable ? "qrc:///chevron_right.svg" : ""
            clickable: canEditMidweekMeetingSchedule && editpossible
            onClicked: {
                var notePage = stackView.clearAndPush(Qt.resolvedUrl("LMMNotesDialog.qml"),
                                                      {pageTitle: themeText.text, notes : myMeeting.openingComments})
                notePage.onSaveChanges.connect(function(notes){
                    if (myMeeting.openingComments !== notes) {
                        myMeeting.openingComments = notes
                        myMeeting.save()
                    }
                })
            }
        }

        ListView {
            Layout.fillWidth: true
            id: lmmListView
            height: 100
            delegate: listRow
            interactive: false            
        }
        ScheduleRowItem {
            id: reviewCommentsRow
            themeText.text: qsTr("Concluding Comments")
            themeText.color: "grey"
            timeboxColor: "grey"
            visible: lmmListView.model.length > 0
            imageSource: clickable ? "qrc:///chevron_right.svg" : ""
            clickable: canEditMidweekMeetingSchedule && editpossible
            onClicked: {
                var notePage = stackView.clearAndPush(Qt.resolvedUrl("LMMNotesDialog.qml"),
                                                      {pageTitle: themeText.text, notes : myMeeting.closingComments})
                notePage.onSaveChanges.connect(function(notes){
                    if (myMeeting.closingComments !== notes) {
                        myMeeting.closingComments = notes
                        myMeeting.save()
                    }
                })
            }
        }
        ScheduleRowItem {
            id: coServiceTalkRow
            visible: false
            timeboxColor: "black"
        }

        ScheduleRowItem {
            id: songEndRow
            themeText.text: qsTr("Song %1 and Prayer").arg(myMeeting.songEnd.toString()) +
                            (settings_ui.showSongTitles ? "\n" + myMeeting.songEndTitle : "")
            themeText.color: "grey"
            timeboxColor: showTime ? "grey" : "transparent"
            nameText1.text: myMeeting.prayerEnd ? myMeeting.prayerEnd.fullname : ""
            imageSource: clickable ? "qrc:///chevron_right.svg" : ""
            clickable: canEditMidweekMeetingSchedule && editpossible
            onClicked: {
                var mylist = myMeeting.getPrayerList()
                var rowindex = myMeeting.prayerEnd ? mylist.find(myMeeting.prayerEnd.id,1) : -1
                var speakersPage = stackView.clearAndPush(Qt.resolvedUrl("SelectionListPage.qml"),
                                                          {showName: true, model: mylist, selectedRow: rowindex, pageTitle: qsTr("Prayer")})
                speakersPage.onActiveRowChanged.connect(function(index){
                    myMeeting.prayerEnd = (typeof mylist.get(index).id === "undefined") ? null : myController.getPublisherById(mylist.get(index).id)
                    myMeeting.save()
                })
            }
        }
        Item { Layout.fillHeight: true }
    }
    Component {
        id: listRow
        Rectangle {
            id: headerBackground
            height: visible ?
                        (gridLayout.height + (hdr && model.index > 0 ? 10*dpi : 0)) : 0
            width: layout.width
            visible: (!model.modelData.canMultiSchool || tabView.currentIndex + 1 === model.modelData.classnumber) &&
                     model.modelData.talkId !== LMM_Schedule.TalkType_COTalk            
            Component.onCompleted: {
                headerBackground.color = getBackgroundColor(model.modelData.talkId)
                hdr_text_color = getTextColor(model.modelData.talkId)
                headerText.text = getHeaderText(model.modelData.talkId)
                // time
                if (model.modelData.talkId === LMM_Schedule.TalkType_BibleReading &&
                        model.modelData.classnumber === 1){
                    t.setMinutes(t.getMinutes()+1)
                }else if (model.modelData.talkId === LMM_Schedule.TalkType_LivingTalk1){
                    t = new Date(myMeeting.startTime)
                    t.setMinutes(t.getMinutes()+47)
                    songRow.timeText.text = showTime ? Qt.formatTime(t, Qt.DefaultLocaleShortDate) : ""
                    t.setMinutes(t.getMinutes()+5)
                }
                themeRow.timeText.text = showTime ? Qt.formatTime(t, Qt.DefaultLocaleShortDate) : model.modelData.time

                var classesThisWeek = 1
                if (myMeeting.counselor3 && myMeeting.classes == 3)
                    classesThisWeek = 3
                else if (myMeeting.counselor2 && myMeeting.classes > 1)
                    classesThisWeek = 2
                if (model.modelData.talkId !== LMM_Schedule.TalkType_COTalk &&
                        (noMultiSchool || !model.modelData.canMultiSchool || model.modelData.classnumber === classesThisWeek))
                    t.setMinutes(t.getMinutes() + model.modelData.time + (model.modelData.canCounsel ? 1 : 0))
            }

            property bool hdr: typeof model.modelData.talkId==="undefined" ?
                                   false :
                                   (model.modelData.talkId === LMM_Schedule.TalkType_Treasures ||
                                    (index > 0 && model.modelData.talkId !== lmmListView.model[index-model.modelData.classnumber].talkId &&
                                     lmmListView.model[index-model.modelData.classnumber].talkId === LMM_Schedule.TalkType_BibleReading) ||
                                    model.modelData.talkId === LMM_Schedule.TalkType_LivingTalk1)
            property string hdr_text_color: "white"

            Rectangle {
                id: whiteRect
                color: "white"
                anchors.fill: parent
                anchors.bottomMargin: parent.height - 10*dpi
                visible: hdr && model.index > 0
                Rectangle { color: "lightgrey"; anchors.bottom: parent.bottom; width: parent.width; height: app_info.linewidth }

            }

            ColumnLayout {
                id: gridLayout
                height: hdr ? (model.modelData.talkId === LMM_Schedule.TalkType_LivingTalk1 ? 110*dpi : 70*dpi) : 40*dpi
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                spacing: 0

                // header for meeting part
                Text {
                    id: headerText
                    Layout.fillWidth: true
                    Layout.columnSpan: 4
                    Layout.preferredHeight: 30*dpi
                    Layout.leftMargin: 5*dpi
                    visible: hdr
                    Layout.fillHeight: true
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    font.bold: true
                    font.pointSize: app_info.fontsize
                    color: hdr_text_color
                    elide: Text.ElideRight
                }
                // song row
                ScheduleRowItem {
                    id: songRow
                    timeboxColor: showTime ? "grey" : "transparent"
                    themeText.text: qsTr("Song") + " " + myMeeting.songMiddle.toString() +
                                    (settings_ui.showSongTitles ? "\n" + myMeeting.songMiddleTitle : "")
                    themeText.color: "grey"
                    showTopLine: true
                    visible: hdr && model.modelData.talkId === LMM_Schedule.TalkType_LivingTalk1
                }

                ScheduleRowItem {
                    id: themeRow
                    timeboxColor: hdr_text_color
                    themeText.text: model.modelData.theme
                    themeText.color: tabView.currentIndex > 0 && model.modelData.classnumber === 1 ? "grey" : "black"
                    nameText1.text: model.modelData.volunteer ? ">" + model.modelData.volunteer.fullname : model.modelData.speakerFullName +
                                                                (model.modelData.talkId === LMM_Schedule.TalkType_CBS ? " (" + qsTr("Conductor") + ")" : "")
                    nameText2.text: !model.modelData.assistant ? "" : model.modelData.assistant.fullname +
                                                                 (model.modelData.talkId === LMM_Schedule.TalkType_CBS ? " (" + qsTr("Reader") + ")" : "")
                    nameText2.color: "grey"
                    imageSource: clickable ? "qrc:///chevron_right.svg" : ""
                    clickable: canEditMidweekMeetingSchedule && editpossible
                    showTopLine: hdr && !songRow.visible
                    onClicked: {
                        console.log("Row clicked!")
                        editButtonClicked(index)
                    }
                }
            }
        }
    }
}

