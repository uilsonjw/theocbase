/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <UIKit/UIKit.h>
#include <QGuiApplication>
#include <QtQuick>
#include "messagebox.h"
#include <QDebug>

MessageBox::MessageBox()

{
}

void MessageBox::show(QString title, QString message){
    //myViewController *mvc = [[myViewController alloc ] initWithMessageBox:this];
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:title.toNSString()
            message:message.toNSString()
            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okButton = [UIAlertAction actionWithTitle:@"OK"
            style:UIAlertActionStyleDefault
            handler:^(UIAlertAction * action __unused) {
                qDebug() << "OK Clicked";
                emit buttonClicked(true,-1);
            }];
    [alert addAction:okButton];
    UIViewController *qtController = [[UIApplication sharedApplication].keyWindow rootViewController];
    [qtController presentViewController:alert animated:YES completion:nil];
}

void MessageBox::showYesNo(QString title, QString message, int id){
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:title.toNSString()
        message:message.toNSString()
        preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Yes"
            style:UIAlertActionStyleDefault
            handler:^(UIAlertAction * action __unused) {
                qDebug() << "YES Clicked";
                emit buttonClicked(true,id);
            }];
    UIAlertAction* noButton = [UIAlertAction actionWithTitle:@"No"
            style:UIAlertActionStyleDefault
            handler:^(UIAlertAction * action __unused) {
                qDebug() << "NO Clicked";
                emit buttonClicked(false,id);
            }];
    [alert addAction:yesButton];
    [alert addAction:noButton];
    UIViewController *qtController = [[UIApplication sharedApplication].keyWindow rootViewController];
    [qtController presentViewController:alert animated:YES completion:nil];
}
