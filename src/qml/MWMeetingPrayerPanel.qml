/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import net.theocbase 1.0

ScrollView {
    id: mwPrayerPanel
    property string title: "Midweek Meeting Prayer"
    property bool beginningPrayer: true
    width: 500
    height: 400
    clip: true

    property LMM_Meeting meeting
    anchors.fill: parent

    ColumnLayout {
        id: layout
        x: 10
        y: 10
        width: mwPrayerPanel.width - 20

        // Prayer
        RowLayout {
            ToolButton {
                icon.source: "qrc:/icons/servant.svg"
                background: null
                ToolTip.text: qsTr("Prayer")
                ToolTip.visible: hovered
            }
            ComboBoxTable {
                id: comboPrayer
                Layout.fillWidth: true
                column4.visible: false
                onBeforeMenuShown: {
                    model = meeting.getPrayerList()
                    column2.resizeToContents()
                }
                currentText: beginningPrayer ?
                                 meeting.prayerBeginning ?
                                               meeting.prayerBeginning.fullname : "" : meeting.prayerEnd ?
                                         meeting.prayerEnd.fullname : ""
                onRowSelected: {
                    var prayer = id < 1 ? null : CPersons.getPerson(id)
                    if (beginningPrayer)
                        meeting.prayerBeginning = prayer
                    else
                        meeting.prayerEnd = prayer
                    meeting.save()
                }
            }
        }
    }
}
