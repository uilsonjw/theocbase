#ifndef ACCESSCONTROL_H
#define ACCESSCONTROL_H

#include <QObject>
#include <QList>
#include <QString>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QDebug>
#include <QCryptographicHash>
#include <QQmlEngine>

class Permission
{
    Q_GADGET

public:
    enum class Rule : quint16 {
        // Midweek Meeting
        CanViewMidweekMeetingSchedule = 1000,
        CanEditMidweekMeetingSchedule = 1001,
        CanPrintMidweekMeetingSchedule = 1010,
        CanPrintMidweekMeetingAssignmentSlips = 1011,
        CanPrintMidweekMeetingWorksheets = 1012,
        CanViewMidweekMeetingSettings = 1100,
        CanEditMidweekMeetingSettings = 1101,
        CanSendMidweekMeetingReminders = 1200,
        // Weekend Meeting
        CanViewWeekendMeetingSchedule = 2000,
        CanEditWeekendMeetingSchedule = 2001,
        CanPrintWeekendMeetingSchedule = 2010,
        CanPrintWeekendMeetingWorksheets = 2011,
        CanPrintSpeakersSchedule = 2012,
        CanPrintSpeakersAssignments = 2013,
        CanViewWeekendMeetingSettings = 2100,
        CanEditWeekendMeetingSettings = 2101,
        CanViewPublicTalkList = 2110,
        CanEditPublicTalkList = 2111,
        CanPrintPublicTalkList = 2120,
        CanScheduleHospitality = 2200,
        CanPrintHospitality = 2210,
        // Territories
        CanViewTerritories = 3000,
        CanEditTerritories = 3001,
        CanPrintTerritoryRecord = 3010,
        CanPrintTerritoryMapCard = 3011,
        CanPrintTerritoryMapAndAddressSheets = 3012,
        CanViewTerritoryAssignments = 3020,
        CanViewTerritoryAddresses = 3030,
        CanViewTerritorySettings = 3100,
        CanEditTerritorySettings = 3101,
        // Persons
        CanViewPublishers = 4000,
        CanEditPublishers = 4001,
        CanViewStudentData = 4100,
        CanEditStudentData = 4101,
        CanViewPublicSpeakers = 4200,
        CanEditPublicSpeakers = 4201,
        CanViewPrivileges = 4300,
        CanEditPrivileges = 4301,
        CanViewMidweekMeetingTalkHistory = 4302,
        CanViewAvailabilities = 4310,
        CanEditAvailabilities = 4311,
        CanViewPermissions = 4400,
        CanEditPermissions = 4401,
        // General
        CanViewCongregationSettings = 5000,
        CanEditCongregationSettings = 5001,
        CanViewSpecialEvents = 5010,
        CanEditSpecialEvents = 5011,
        CanViewMeetingNotes = 5020,
        CanEditMeetingNotes = 5021,
        CanViewSongList = 5030,
        CanEditSongList = 5031,
        CanDeleteCloudData = 5100
    };
    Q_ENUM(Rule)

    enum class RoleId : quint8 {
        Publisher = 1,
        Elder = 8,
        LMMChairman = 2,
        LMMOverseer = 3,
        TalkCoordinator = 4,
        TerritoryServant = 5,
        Secretary = 10,
        ServiceOverseer = 9,
        CoordinatorOfBOE = 6,
        Administrator = 7
    };
    Q_ENUM(RoleId)

    static QString toString(Rule rule)
    {
        switch (rule) {
        case Rule::CanViewMidweekMeetingSchedule:
            return QObject::tr("View midweek meeting schedule", "Access Control");
        case Rule::CanEditMidweekMeetingSchedule:
            return QObject::tr("Edit midweek meeting schedule", "Access Control");
        case Rule::CanViewMidweekMeetingSettings:
            return QObject::tr("View midweek meeting settings", "Access Control");
        case Rule::CanEditMidweekMeetingSettings:
            return QObject::tr("Edit midweek meeting settings", "Access Control");
        case Rule::CanSendMidweekMeetingReminders:
            return QObject::tr("Send midweek meeting reminders", "Access Control");
        case Rule::CanPrintMidweekMeetingSchedule:
            return QObject::tr("Print midweek meeting schedule", "Access Control");
        case Rule::CanPrintMidweekMeetingAssignmentSlips:
            return QObject::tr("Print midweek meeting assignment slips", "Access Control");
        case Rule::CanPrintMidweekMeetingWorksheets:
            return QObject::tr("Print midweek meeting worksheets", "Access Control");
        case Rule::CanViewWeekendMeetingSchedule:
            return QObject::tr("View weekend meeting schedule", "Access Control");
        case Rule::CanEditWeekendMeetingSchedule:
            return QObject::tr("Edit weekend meeting schedule", "Access Control");
        case Rule::CanViewWeekendMeetingSettings:
            return QObject::tr("View weekend meeting settings", "Access Control");
        case Rule::CanEditWeekendMeetingSettings:
            return QObject::tr("Edit weekend meeting settings", "Access Control");
        case Rule::CanViewPublicTalkList:
            return QObject::tr("View public talk list", "Access Control");
        case Rule::CanEditPublicTalkList:
            return QObject::tr("Edit public talk list", "Access Control");
        case Rule::CanScheduleHospitality:
            return QObject::tr("Schedule hospitality", "Access Control");
        case Rule::CanPrintWeekendMeetingSchedule:
            return QObject::tr("Print weekend meeting schedule", "Access Control");
        case Rule::CanPrintWeekendMeetingWorksheets:
            return QObject::tr("Print weekend meeting worksheets", "Access Control");
        case Rule::CanPrintSpeakersSchedule:
            return QObject::tr("Print speakers schedule", "Access Control");
        case Rule::CanPrintSpeakersAssignments:
            return QObject::tr("Print speakers assignments", "Access Control");
        case Rule::CanPrintHospitality:
            return QObject::tr("Print hospitality", "Access Control");
        case Rule::CanPrintPublicTalkList:
            return QObject::tr("Print public talk list", "Access Control");
        case Rule::CanViewPublishers:
            return QObject::tr("View publishers", "Access Control");
        case Rule::CanEditPublishers:
            return QObject::tr("Edit publishers", "Access Control");
        case Rule::CanViewStudentData:
            return QObject::tr("View student data", "Access Control");
        case Rule::CanEditStudentData:
            return QObject::tr("Edit student data", "Access Control");
        case Rule::CanViewPublicSpeakers:
            return QObject::tr("View public speakers", "Access Control");
        case Rule::CanEditPublicSpeakers:
            return QObject::tr("Edit public speakers", "Access Control");
        case Rule::CanViewPrivileges:
            return QObject::tr("View privileges", "Access Control");
        case Rule::CanEditPrivileges:
            return QObject::tr("Edit privileges", "Access Control");
        case Rule::CanViewMidweekMeetingTalkHistory:
            return QObject::tr("View midweek meeting talk history", "Access Control");
        case Rule::CanViewAvailabilities:
            return QObject::tr("View availabilities", "Access Control");
        case Rule::CanEditAvailabilities:
            return QObject::tr("Edit availabilities", "Access Control");
        case Rule::CanViewPermissions:
            return QObject::tr("View permissions", "Access Control");
        case Rule::CanEditPermissions:
            return QObject::tr("Edit permissions", "Access Control");
        case Rule::CanViewTerritories:
            return QObject::tr("View territories", "Access Control");
        case Rule::CanEditTerritories:
            return QObject::tr("Edit territories", "Access Control");
        case Rule::CanPrintTerritoryRecord:
            return QObject::tr("Print territory record", "Access Control");
        case Rule::CanPrintTerritoryMapCard:
            return QObject::tr("Print territory map card", "Access Control");
        case Rule::CanPrintTerritoryMapAndAddressSheets:
            return QObject::tr("Print territory map and address sheets", "Access Control");
        case Rule::CanViewTerritorySettings:
            return QObject::tr("View territory settings", "Access Control");
        case Rule::CanEditTerritorySettings:
            return QObject::tr("Edit territory settings", "Access Control");
        case Rule::CanViewTerritoryAssignments:
            return QObject::tr("View territory assignments", "Access Control");
        case Rule::CanViewTerritoryAddresses:
            return QObject::tr("View territory addresses", "Access Control");
        case Rule::CanViewCongregationSettings:
            return QObject::tr("View congregation settings", "Access Control");
        case Rule::CanEditCongregationSettings:
            return QObject::tr("Edit congregation settings", "Access Control");
        case Rule::CanViewSpecialEvents:
            return QObject::tr("View special events", "Access Control");
        case Rule::CanEditSpecialEvents:
            return QObject::tr("Edit special events", "Access Control");
        case Rule::CanViewMeetingNotes:
            return QObject::tr("View special events", "Access Control");
        case Rule::CanEditMeetingNotes:
            return QObject::tr("Edit special events", "Access Control");
        case Rule::CanViewSongList:
            return QObject::tr("View song list", "Access Control");
        case Rule::CanEditSongList:
            return QObject::tr("Edit song list", "Access Control");
        case Rule::CanDeleteCloudData:
            return QObject::tr("Delete cloud data", "Access Control");
        }
        return "";
    }

    static QString toString(RoleId roleId)
    {
        switch (roleId) {
        case RoleId::Publisher:
            return QObject::tr("Publisher", "Roles and access control");
        case RoleId::Elder:
            return QObject::tr("Elder", "Roles and access control");
        case RoleId::LMMChairman:
            return QObject::tr("LMM Chairman", "Roles and access control");
        case RoleId::LMMOverseer:
            return QObject::tr("LMM Overseer", "Roles and access control");
        case RoleId::TalkCoordinator:
            return QObject::tr("Public Talk Coordinator", "Roles and access control");
        case RoleId::TerritoryServant:
            return QObject::tr("Territory Servant", "Roles and access control");
        case RoleId::Secretary:
            return QObject::tr("Secretary", "Roles and access control");
        case RoleId::ServiceOverseer:
            return QObject::tr("Service Overseer", "Roles and access control");
        case RoleId::CoordinatorOfBOE:
            return QObject::tr("Coordinator of BOE", "Roles and access control");
        case RoleId::Administrator:
            return QObject::tr("Administrator", "Roles and access control");
        }
        return "";
    }
};
typedef Permission::Rule PermissionRule;

class Role
{
public:
    Role();

    Permission::RoleId id() const;
    void setId(Permission::RoleId id);

    int parentId() const;
    void setParentId(int parentId);

    Role *parent() const;
    void setParent(Role *parent);

    QList<Permission::Rule> permissions() const;
    void setPermissions(const QList<Permission::Rule> &permissions);
    bool hasPermission(Permission::Rule rule) const;
    void write(QJsonObject &json) const;

private:
    Permission::RoleId m_Id;
    int m_ParentId;
    Role *m_parent;
    QString m_Name;
    QList<Permission::Rule> m_Permissions;
};

class User : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name)
    Q_DISABLE_COPY(User)
public:
    User(QObject *parent = Q_NULLPTR);

    int id() const;
    void setId(int Id);

    QString name() const;
    void setName(const QString &name);

    QString email() const;
    void setEmail(const QString &email);

    const QList<const Role *> &roles() const;
    void setRoles(const QList<const Role *> &roles);
    void addRole(const Role *role);
    void removeRole(const Role *role);

    bool hasRole(Role &role) const;
    Q_INVOKABLE bool hasPermission(PermissionRule permissionRule);

    void write(QJsonObject &json) const;

private:
    int m_Id;
    QString m_Name;
    QString m_Email;
    QList<const Role *> m_Roles;
};

class AccessControl : public QObject
{
    Q_OBJECT
    Q_PROPERTY(User *user READ user NOTIFY userChanged)
    Q_DISABLE_COPY(AccessControl)
public:
    AccessControl(QObject *parent = Q_NULLPTR);
    void setDefaultRolePermissions();

    const QList<Role> &roles() const;

    QList<User *> &users();
    User *findUser(int id);
    User *findUser(QString email);
    bool addUser(QString name, QString email);
    void removeUser(int id);

    User *user() const;
    void setUser(User *user);

    bool isActive() { return user() && user()->email() != "admin"; }

    void load(const QJsonObject &json, bool &valid);
    void write(QJsonObject &json);

signals:
    void userChanged();

private:
    QList<Role> m_Roles;
    User *m_User;
    QList<User *> m_Users;

    void setRoles(const QList<Role> &roles);
    Role *findRole(Permission::RoleId id);
    void setUsers(const QList<User *> &users);
};

#endif // ACCESSCONTROL_H
