/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QStringBuilder>
#include "availabilitychecker.h"

using namespace tbAvailability;

// AvailabilityChecker
//====================

AvailabilityChecker::AvailabilityChecker(const QDate &meetingDate,
                                         const QDate &weekCommencingDate,
                                         const std::vector<person::UseFor> &relevantQualifications,
                                         const std::vector<QString> &dataSourceIds)
    : sql_(Singleton<sql_class>::Instance())
    , relevantQualifications_(relevantQualifications)
    , dataSourceIds_(dataSourceIds)
    , weekCommencingDate_(weekCommencingDate)
    , meetingDate_(meetingDate)
{
    Q_ASSERT(weekCommencingDate.dayOfWeek() == Qt::Monday);
    localCongregationId_ = sql_.getSetting("congregation_id").toInt();
    nameFormat = sql_.getSetting("nameFormat", "%2, %1");
}

AvailabilityChecker::~AvailabilityChecker()
{
}

// get comprehensive list of persons available and unavailable...
Availability AvailabilityChecker::Get(int congregationId /* = 0*/)
{
    Availability result;

    sql_items qualifiedPersons = sql_.selectSql(GenerateQualifiedPersonsSql(congregationId));
    sql_items holidayPersons = sql_.selectSql(GenerateHolidayPersonsSql());
    std::map<QString, sql_items> assignedPersons = GetAssignedPersonsFromAllDataSources();

    foreach (sql_item qualifiedPerson, qualifiedPersons) {
        AvailabilityItem item = CreateAvailabilityItem(qualifiedPerson, holidayPersons, assignedPersons);
        result.Add(item);
    }

    result.Sort();
    return result;
}

Availability AvailabilityChecker::GetLocal()
{
    return Get(localCongregationId_);
}

std::map<QString, sql_items> AvailabilityChecker::GetAssignedPersonsFromAllDataSources()
{
    std::map<QString, sql_items> result;

    foreach (QString dataSourceId, dataSourceIds_) {
        sql_items items = sql_.selectSql(GenerateAssignedPersonsSql(dataSourceId));
        result.insert(std::pair<QString, sql_items>(dataSourceId, items));
    }

    return result;
}

AvailabilityItem AvailabilityChecker::CreateAvailabilityItem(const sql_item &qualifiedPerson,
                                                             const sql_items &holidayPersons,
                                                             const std::map<QString, sql_items> &assignedPersons)
{
    AvailabilityItem result = CreateStandardAvailabilityItem(qualifiedPerson, holidayPersons);

    if (!assignedPersons.empty() && result.Id > 0) {
        for (std::map<QString, sql_items>::const_iterator i = assignedPersons.begin();
             i != assignedPersons.end();
             ++i) {
            PopulateAssignments(i->first, result, i->second);
        }
    }

    return result;
}

bool AvailabilityChecker::IsLocalCongRoleOnly(person::UseFor role)
{
    return role != person::PublicTalk;
}

// get list of persons available and unavailable for a particular role
Availability AvailabilityChecker::Get(person::UseFor role, int congregationId /*= 0*/)
{
    if (congregationId == 0 && IsLocalCongRoleOnly(role)) {
        congregationId = localCongregationId_;
    }

    Availability result = Get(congregationId);
    result.Filter(role);
    return result;
}

Availability AvailabilityChecker::GetLocal(person::UseFor role)
{
    return Get(role, localCongregationId_);
}

QString AvailabilityChecker::GenerateQualificationsWhereClause()
{
    QString result;

    foreach (auto qual, relevantQualifications_) {
        if (!result.isEmpty()) {
            result += " OR ";
        }

        result += "(usefor & ";
        result += QString::number((int)qual);
        result += ")";
    }

    return result.isEmpty() ? "(1=1) " : "(" + result + ") ";
}

QString AvailabilityChecker::GenerateQualifiedPersonsSql(int congregationId /* = 0*/)
{
    QString s =
            "SELECT p.id, p.firstname, p.lastname, p.usefor, c.name as congregation, c.circuit "
            "FROM persons p ";
    s += "LEFT JOIN congregations c ON p.congregation_id = c.id ";

    s += "WHERE ";
    s += GenerateQualificationsWhereClause();
    s += "AND p.active";

    if (congregationId > 0) {
        s += " AND p.congregation_id = %1";
        s = s.arg(congregationId);
    }
    return s;
}

AvailabilityItem AvailabilityChecker::CreateStandardAvailabilityItem(const sql_item &qualifiedPerson,
                                                                     const sql_items &holidayPersons)
{
    AvailabilityItem result;
    result.Id = qualifiedPerson.value("id").toInt();
    result.FirstName = qualifiedPerson.value("firstname").toString();
    result.LastName = qualifiedPerson.value("lastname").toString();
    result.DisplayName = GenerateDisplayName(result.FirstName, result.LastName);
    result.CongregationName = qualifiedPerson.value("congregation").toString();
    result.Circuit = qualifiedPerson.value("circuit").toString();

    AddRoles(result, qualifiedPerson);
    AddHolidays(result, holidayPersons);

    return result;
}

QString AvailabilityChecker::GenerateDisplayName(const QString &firstName, const QString &lastName)
{
    // override in derivatives if required
    return nameFormat.arg(firstName, lastName);
}

void AvailabilityChecker::AddRoles(AvailabilityItem &item, const sql_item &qualifiedPerson)
{
    Q_ASSERT(item.Roles.empty());

    int useFor = qualifiedPerson.value("usefor").toInt();
    foreach (auto qual, relevantQualifications_) {
        if (useFor & qual) {
            item.Roles.insert(qual);
        }
    }
}

void AvailabilityChecker::AddHolidays(AvailabilityItem &item, const sql_items &holidayPersons)
{
    int personId = item.Id;

    auto iterator = std::find_if(holidayPersons.begin(), holidayPersons.end(),
                                 [&personId](const sql_item &holItem) { return holItem.value("person_id").toInt() == personId; });

    item.OnHoliday = iterator != holidayPersons.end();
}

QString AvailabilityChecker::GenerateHolidayPersonsSql()
{
    // ignore cong filter as not worth it.

    // NOTE: db may benefit from an index on the "unavailables.end_date" column if table is large

    QString meetingDateStr = "'" + meetingDate_.toString(Qt::ISODate) + "'";

    QString s =
            "SELECT person_id FROM unavailables WHERE "
            "active AND ";

    s += "start_date <= ";
    s += meetingDateStr;
    s += " AND end_date >= ";
    s += meetingDateStr;

    return s;
}

std::vector<int> AvailabilityChecker::GetSpeakersForTheme(int themeId)
{
    std::vector<int> result;

    QString s =
            "SELECT speaker_id "
            "FROM speaker_publictalks "
            "WHERE theme_id = %1";

    s = s.arg(themeId);

    sql_items items = sql_.selectSql(s);
    foreach (sql_item item, items) {
        result.push_back(item.value("speaker_id").toInt());
    }

    return result;
}
