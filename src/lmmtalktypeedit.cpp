#include "lmmtalktypeedit.h"
#include "ui_lmmtalktypeedit.h"

lmmtalktypeedit::lmmtalktypeedit(QWidget *parent, Qt::WindowFlags f) :
    QDialog(parent, f),
    ui(new Ui::lmmtalktypeedit)
{
    ui->setupUi(this);
    connect(ui->buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(ui->buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);

    ui->gridTalkTypes->verticalHeader()->hide();
    ui->gridTalkTypes->setColumnCount(2);
    ui->gridTalkTypes->setHorizontalHeaderItem(0, new QTableWidgetItem(tr("Talk Name in the Workbook")));
    ui->gridTalkTypes->horizontalHeader()->setSectionResizeMode(0,QHeaderView::Stretch);
    ui->gridTalkTypes->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("Meeting Item")));
    ui->gridTalkTypes->horizontalHeader()->setSectionResizeMode(1,QHeaderView::Stretch);

    ui->gridTalkTypes->setItemDelegateForColumn(1,new talkTypeComboBox(this));
    ui->gridTalkTypes->setEditTriggers(QAbstractItemView::AllEditTriggers);
}

lmmtalktypeedit::~lmmtalktypeedit()
{
    delete ui;
}

void lmmtalktypeedit::Init(bool showButtons)
{
    ui->gridTalkTypes->blockSignals(true);        
    ui->gridTalkTypes->setRowCount(0);

    sql = &Singleton<sql_class>::Instance();
    sql_items items = sql->selectSql("SELECT * FROM settings WHERE name LIKE 'Talk Type:%'");

    for (sql_item item : items) {
        int row = ui->gridTalkTypes->rowCount();
        ui->gridTalkTypes->setRowCount(row+1);
        ui->gridTalkTypes->setItem(row, 0, new QTableWidgetItem(item.value("name").toString()));
        ui->gridTalkTypes->item(row,0)->setFlags(ui->gridTalkTypes->item(row,0)->flags() ^ Qt::ItemIsEditable);
        int talk_id = item.value("value").toInt();
        ui->gridTalkTypes->setItem(row, 1, new QTableWidgetItem(talk_id == -1 ? tr("Unknown", "Unknown talk name") : LMM_Schedule::getFullStringTalkType(talk_id)));
    }
    ui->gridTalkTypes->blockSignals(false);
    ui->buttonBox->setVisible(showButtons);
    if (!showButtons) {
        this->layout()->setContentsMargins(0,0,0,0);
    }
}

void lmmtalktypeedit::on_gridTalkTypes_itemChanged(QTableWidgetItem *item)
{
    if (!item) return;

    int talk_id = LMM_Schedule::getTalkTypeFromFullString(item->text());
    qDebug() << item->text() << talk_id;

    QString talk = ui->gridTalkTypes->item(item->row(),0)->text();
    sql->saveSetting(talk, QString::number(talk_id));

    // update existing schedules to match changes
    sql->execSql(
                "update lmm_schedule "
                "set "
                "    time_stamp = strftime('%s','now'), "
                "    talk_id = (select value from settings where name like 'talk type:%' and lmm_schedule.theme = substr(name,12) and talk_id / 10 != value) * 10 + talk_id % 10 "
                "where exists (select value from settings where name like 'talk type:%' and lmm_schedule.theme = substr(name,12) and talk_id / 10 != value)");
}
