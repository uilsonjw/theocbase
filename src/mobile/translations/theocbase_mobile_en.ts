<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Do not assign the next study</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Leave on current study</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Invalid data</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Add the timing?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Theme</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Source</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Student</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Assistant</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Result</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Completed</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Volunteer</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Select a volunteer</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Current Study</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Exercises Completed</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Next Study</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Select next study</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Timing</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notes</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>School Details</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Start stopwatch</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Stop stopwatch</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Setting</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Select setting</translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>No exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit overseer&apos;s visit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit assembly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Regional convention</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Memorial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Other exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation>Week starting %1</translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Midweek Meeting</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Weekend Meeting</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Theme</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Source</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Conductor</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Speaker</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Reader</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notes</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Details</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <translation>Notes</translation>
    </message>
    <message>
        <source>Notes</source>
        <comment>Page title</comment>
        <translation type="unfinished">Notes</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&apos;S WORD</source>
        <translation>TREASURES FROM GOD&apos;S WORD</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>APPLY YOURSELF TO THE FIELD MINISTRY</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>LIVING AS CHRISTIANS</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Chairman</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Counselor</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Song %1 and Prayer</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Song</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Conductor</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Reader</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Opening Comments</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Prayer</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Midweek Meeting</translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Leave on current study</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Start stopwatch</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Stop stopwatch</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Add the timing?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Theme</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Source</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Student</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Result</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Completed</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Timing</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Current Study</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Exercises Completed</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Next Study</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Select next study</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notes</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Assistant</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Volunteer</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>Stopwatch</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>The assistant should not be someone of the opposite sex.</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Details</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>Pull to refresh...</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Release to refresh...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation>Username</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Login</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Username or Email</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Create Account</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>Reset Password</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>Email address not found!</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Forgot Password</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>Login Page</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation>Week starting %1</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation>Notes</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Schedule</translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Midweek Meeting</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Weekend Meeting</translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Text size</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Song and Prayer</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Song %1 and Prayer</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>PUBLIC TALK</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>WATCHTOWER STUDY</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Song %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Conductor</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Reader</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Weekend Meeting</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Midweek Meeting</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>Theme</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Speaker</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Phone</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>Public Talk</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>First Name</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Last Name</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Brother</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Sister</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Servant</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Family</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>Family member linked to</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Contact Information</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Phone</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>All Classes</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Only Main Class</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Only Auxiliary Classes</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Family Head</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Assistant</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>Active</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Chairman</translation>
    </message>
    <message>
        <source>Treasures from God&apos;s Word</source>
        <translation>Treasures from God&apos;s Word</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Bible Reading</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Initial Call</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Return Visit</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Bible Study</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>Living as Christians Talks</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Congregation Bible Study</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Cong. Bible Study Reader</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>New publisher</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Prayer</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Midweek Meeting</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Weekend Meeting</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Publishers</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Settings</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Logout</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>TheocBase Homepage</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Feedback</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>Last synchronized: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Schedule</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Show Time</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Show Duration</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>User Interface</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Language</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Login</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Printing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Synchronizing...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>Chairman</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Song</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>Song</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Prayer</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Theme</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>Conductor</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>Reader</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>weeks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Do not assign the next study</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>Not set</translation>
    </message>
</context>
</TS>
