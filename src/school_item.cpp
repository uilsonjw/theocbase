/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "school_item.h"

school_item::school_item() :
    m_id(-1), // int
    m_schedule_id(-1), // int
    m_assisgnment_number(-1), // int
    m_setting(0), // school_setting
    m_student(0), // person
    m_assistant(0), // person
    m_volunteer(0), // person
    m_classnumber(0), // int
    m_done(0), // bool
    m_onlybrothers(0) // bool
{}

school_item::~school_item()
{
    delete m_setting;

    // APC, July 20017
    // TODO: examine ownership of these objects (passed in via setters)
    // to determine whether they should be deleted here:
    // m_student
    // m_assistant
    // m_volunteer
}

int school_item::getId()
{
    return m_id;
}

void school_item::setId(int id)
{
    m_id = id;
}

int school_item::getScheduleId()
{
    return m_schedule_id;
}

void school_item::setScheduleId(int id)
{
    m_schedule_id = id;
}

int school_item::getAssignmentNumber() const
{
    return m_assisgnment_number;
}

void school_item::setAssignmentNumber(int number)
{
    m_assisgnment_number = number;
}

person *school_item::getStudent() const
{
    return m_student;
}

void school_item::setStudent(person *value)
{
    m_student = value;
}

person *school_item::getAssistant() const
{
    return m_assistant;
}

void school_item::setAssistant(person *value)
{
    m_assistant = value;
}

person *school_item::getVolunteer() const
{
    return m_volunteer;
}

void school_item::setVolunteer(person *value)
{
    m_volunteer = value;
}

QDate school_item::getDate() const
{
    return m_date;
}

void school_item::setDate(const QDate value)
{
    m_date = value;
}

QString school_item::getSource() const
{
    return m_source;
}

void school_item::setSource(const QString &value)
{
    m_source = value;
}

QString school_item::getTheme() const
{
    return m_theme;
}

void school_item::setTheme(const QString &value)
{
    m_theme = value;
}

QString school_item::getNote() const
{
    return m_note;
}

void school_item::setNote(const QString &value)
{
    m_note = value;
}


bool school_item::getDone() const
{
    return m_done;
}

void school_item::setDone(bool value)
{
    m_done = value;
}

QString school_item::getTiming() const
{
    return m_timing;
}

void school_item::setTiming(QString time)
{
    m_timing = time;
}

bool school_item::getOnlyBrothers() const
{
    return m_onlybrothers;
}

void school_item::setOnlyBrothers(bool arg)
{
    m_onlybrothers = arg;
}

school_item::AssignmentType school_item::getType() const
{
    switch (getAssignmentNumber())
    {
    case 0:
        return school_item::Highlights;
        break;
    case 1:
        return school_item::Reading;
        break;
    case 2:
        return school_item::Demonstration;
        break;
    case 3:
        return ( (getStudent() && getStudent()->gender() == person::Female) ||
                 (getDate().year() >= 2015 && (getSource().contains("nwt") || getTheme().startsWith("*"))) ?
                     school_item::Demonstration : school_item::Discource);
        break;
    case 10:
        return school_item::Review;
        break;
    default:
        return school_item::Reading;
        break;
    }
}

// Save changes to the database
bool school_item::save()
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    sql_item s;

    // schedule id must be exists
    if (getScheduleId() == -1) return false;

    s.insert("school_schedule_id", getScheduleId() );
    s.insert("date", getDate());
    s.insert("classnumber", getClassNumber());
    s.insert("completed", getDone());
    s.insert("setting_id", getSetting() ? getSetting()->getNumber() : 0 );
    s.insert("student_id", getStudent() ? getStudent()->id() : -1 );
    s.insert("assistant_id", getAssistant() ? getAssistant()->id() : -1 );
    s.insert("volunteer_id", getVolunteer() ? getVolunteer()->id() : -1 );
    qDebug() << "save note:" << getNote();
    s.insert("note", getNote());
    s.insert("timing", getTiming());

    qDebug() << "save id:" << getId();
    if(this->getId() == -1){
        // insert a new row to database
        this->setId( sql->insertSql("school",&s,"id") );
    }else{
        // update existing row in the database
        sql->updateSql("school","id", QVariant(this->getId()).toString(), &s);
    }

    return true;
}

school_setting *school_item::getSetting(){
    return m_setting;
}

void school_item::setSetting(int number){
    delete m_setting;
    m_setting = new school_setting();
    m_setting->setNumber(number);
}

int school_item::getClassNumber() const
{
    return m_classnumber;
}

void school_item::setClassNumber(int value)
{
    m_classnumber = value;
}


QString school_item::getNumberName() const
{
    QString numberitem;
    switch(getAssignmentNumber())
    {
    case 0:
        numberitem = QObject::tr("Bible highlights:");
        break;
    case 1:
        numberitem = QObject::tr("No. 1:");
        break;
    case 2:
        numberitem = QObject::tr("No. 2:");
        break;
    case 3:
        numberitem = QObject::tr("No. 3:");
        break;
    case 10:
        numberitem = QObject::tr("Reader:");
        break;
    }
    return numberitem;
}
