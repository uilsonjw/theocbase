/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ccongregation.h"
#include <ciso646>

ccongregation::ccongregation(QObject *parent) : QObject(parent)
{
    sql = &Singleton<sql_class>::Instance();
    clearExceptionCache();
}

ccongregation::congregation ccongregation::getMyCongregation() {
    return getCongregationById(sql->getSetting("congregation_id").toInt());
}

QList<ccongregation::congregation> ccongregation::getAllCongregations(bool groupedByCircuit)
{
    // return all congregations <id,name>

    QList<ccongregation::congregation> congregations;

    sql_items cgr;
    QString query = "SELECT * FROM congregations WHERE active ORDER BY ";
    if (groupedByCircuit){
        query.append("circuit, name");
    }else{
        query.append("name");
    }
    cgr = sql->selectSql(query);
    if(!cgr.empty()){
        for(unsigned int i = 0; i<cgr.size(); i++){
            sql_item s = cgr[i];
            congregation c;
            c.id = s.value("id").toInt();
            c.name = s.value("name").toString();
            c.time_meeting1 = s.value("meeting1_time").toString();
            c.address = s.value("address").toString();
            c.circuit = s.value("circuit").toString();
            c.info = s.value("info").toString();
            congregations.append(c);
        }
    }else{
        // create default
        congregation c = addCongregation("-");
        sql->saveSetting("congregation_id",QString::number(c.id));
        congregations.append(c);
    }
    return congregations;
}

int ccongregation::getCongregationId(QString name)
{
    // get congregation id by name

    sql_item values;
    values.insert(":name", name);
    return sql->selectScalar("select id from congregations where name = :name and active", &values, -1).toInt();
}

ccongregation::congregation ccongregation::getOrAddCongregation(QString name)
{
    int id = getCongregationId(name);
    if (id < 1)
        return addCongregation(name);
    else
        return getCongregationById(id);
}

ccongregation::congregation ccongregation::addCongregation(QString name)
{
    // add new congregation
    sql_item newsrk;
    newsrk.insert("name",name);
    int srkid = sql->insertSql("congregations",&newsrk,"id");
    sql->updateThisYearMeetingTimes();
    sql->updateNextYearMeetingTimes();
    // calling getCongregationId will fill meeting time objects properly
    return getCongregationById(srkid);
}

bool ccongregation::removeCongregation(int id)
{
    sql_item s;
    s.insert("id",id);
    s.insert("time_stamp",0);
    return sql->execSql("update congregations set active = 0, time_stamp = :time_stamp where id = :id",&s,true);
    //return sql->removeSql("congregations","id = " + QVariant(id).toString());
}

QString ccongregation::getCongregationName(int id)
{
    sql_items s = sql->selectSql("congregations","id",QVariant(id).toString(),"");
    return s[0].value("name").toString();
}

ccongregation::congregation ccongregation::getCongregationById(int id)
{
    sql_items s = sql->selectSql("congregations","id",QVariant(id).toString(),"");
    congregation c;
    if (s.empty()) {
        c.name = tr("(Missing Record)", "database is now missing this entry");
    } else {
        sql_item citem = s[0];
        c.name = citem.value("name").toString();
        c.id = citem.value("id").toInt();
        c.address = citem.value("address").toString();
        c.time_meeting1 = citem.value("meeting1_time").toString();
        c.circuit = citem.value("circuit").toString();
        c.info = citem.value("info").toString();

        int thisyear = QDate::currentDate().year();
        sql_item criteria;
        criteria.insert(":congregation_id", id);
        criteria.insert(":thisyear", thisyear);
        s = sql->selectSql("select id, mtg_year, mtg_day, mtg_time from congregationmeetingtimes where congregation_id = :congregation_id and (mtg_year = :thisyear or mtg_year = :thisyear + 1)", &criteria);
        for(sql_items::size_type i = 0; i != s.size(); i++) {
            sql_item mt = s[i];
            int yr = mt["mtg_year"].toInt();
            if ((yr == thisyear) || (yr == thisyear + 1))
            {
                auto& mtgtime = (yr == thisyear)
                   ? c.getPublicmeeting_now()
                   : c.getPublicmeeting_next();

                mtgtime.setId(mt["id"].toInt());
                mtgtime.setOfyear(mt["mtg_year"].toInt());
                mtgtime.setMeetingday(mt["mtg_day"].toInt());
                mtgtime.setMeetingtime(mt["mtg_time"].toString());
                mtgtime.setIsdirty(false);
            }
        }
    }
    return c;
}

int ccongregation::getCongregationIDByPartialName(QString name)
{
    QVariant id = sql->selectScalar("select id from congregations where name like '%" + name + "%' and active", nullptr, 0);
    return id.toInt();
}

ccongregation::meeting_dayandtime::meeting_dayandtime()
    : id(-1),
      isdirty(false),
      ofyear(0),
      meetingday(7),
      meetingtime("")
{
}

int ccongregation:: meeting_dayandtime::getId() const
{
    return id;
}

void ccongregation::meeting_dayandtime::setId(int value)
{
    id = value;
}

QString ccongregation::meeting_dayandtime::getMeetingtime() const
{
    return meetingtime;
}

void ccongregation::meeting_dayandtime::setMeetingtime(const QString &value)
{
    if (meetingtime.compare(value)) {
        meetingtime = value;
        isdirty = true;
    }
}

int ccongregation::meeting_dayandtime::getMeetingday() const
{
    return meetingday;
}

void ccongregation::meeting_dayandtime::setMeetingday(int value)
{
    if (meetingday != value) {
        meetingday = value;
        isdirty = true;
    }
}

/**
 * @brief ccongregation::meeting_dayandtime::getMeetingDate - translates weekof to the actual date of the talk
 * @param weekOf
 * @return date of the talk
 */
QDate ccongregation::meeting_dayandtime::getMeetingDate(QDate weekOf) const
{
    return weekOf.addDays(meetingday - 1);
}

int ccongregation::meeting_dayandtime::getOfyear() const
{
    return ofyear;
}

void ccongregation::meeting_dayandtime::setOfyear(int value)
{
    ofyear = value;
}

bool ccongregation::meeting_dayandtime::getIsvalid() const
{
    return ofyear > 2000 && ofyear < 9999 && meetingday > 0 && meetingday < 8 && meetingtime.contains(":");
}

bool ccongregation::meeting_dayandtime::getIsdirty() const
{
    return isdirty;
}

void ccongregation::meeting_dayandtime::setIsdirty(bool value)
{
    isdirty = value;
}

// init
ccongregation::congregation::congregation()
    :
      id(-1),
      name(""),
      time_meeting1(""),
      address(""),
      circuit(""),
      info(""),
      blank(),
      publicmeeting_now(),
      publicmeeting_next()
{
    int thisyear = QDate::currentDate().year();
    publicmeeting_now.setOfyear(thisyear++);
    publicmeeting_next.setOfyear(thisyear);
}

bool ccongregation::congregation::isValid() const
{
    // this test comes from orignal ccongregation::congregation::save() code
    // QUESTION: why not id > 0 ??? is id==0 a valid one ?
    return id >= 0;
}

ccongregation::exceptions ccongregation::isException(QDate date)
{
    updateExCache(date);

    if (cachedExceptions.contains(date)) {
        sql_item ex(cachedExceptions[date]);
        int exceptiontype = ex.value("type").toInt();
        if (exceptiontype == 0) {
            return ccongregation::CircuitOverseersVisit;
        } else if (exceptiontype == 1) {
            QDate d1 = ex.value("date").toDate();
            QDate d2 = ex.value("date2").toDate();                        
            if (d2.isValid() && d2.toJulianDay() - d1.toJulianDay() > 0)
                return ccongregation::RegionalConvention;
            else
                return ccongregation::CircuitAssembly;
        } else if (exceptiontype == 2) {
            return ccongregation::Memorial;
        } else {
            return ccongregation::Other;
        }
    }else{
        return ccongregation::None;
    }
}

bool ccongregation::noMeeting(QDate date)
{
    updateExCache(date);

    if (cachedExceptions.contains(date)) {
        sql_item ex(cachedExceptions[date]);
        int exceptiontype = ex.value("type").toInt();
        if (exceptiontype == 0){
            // CO visit
            return false;
        }else if (exceptiontype == 1){
            return true;
        }else{
            int midweekday = ex.value("schoolday").toInt();
            int weekendday = ex.value("publicmeetingday").toInt();
            return (midweekday == 0 || weekendday == 0);
        }
    }else{
        return false;
    }
}

QString ccongregation::getExceptionText(QDate date)
{
    updateExCache(date);
    sql_item v = cachedExceptions.value(date);

    if(!v.empty()){
        int exceptiontype(v.value("type").toInt());
        bool nomeeting(v.value("schoolday").toInt() == 0);
        if(exceptiontype == 0){
            return QObject::tr("Circuit Overseer's visit");
        }else if(exceptiontype == 1){            
            return QObject::tr("%1 (No meeting)", "no meeting exception type").arg(QObject::tr("Convention week"));
        }else if(exceptiontype == 2){
            return QObject::tr("Memorial");
        }else{
            if (nomeeting)
                return QObject::tr("%1 (No meeting)", "no meeting exception type").arg(v.value("desc").toString());
            else
                return v.value("desc").toString();
        }
    }else{
        return "";
    }
}

QString ccongregation::getStandardExceptionText(QDate date, bool isReview)
{
    updateExCache(date);
    sql_item v = cachedExceptions.value(date);

    if(!v.empty()) {
        int exceptiontype = v.value("type").toInt();
        switch (exceptiontype)
        {
        case 0:
            return QObject::tr("Circuit Overseer's visit");
        case 1:
            return QObject::tr("Convention week");
        case 2:
            return QObject::tr("Memorial");
        default:
            return v.value("desc").toString();
        }
    } else if (isReview) {
        return QObject::tr("Review", "Theocratic ministry school review");
    } else {
        return "";
    }
}

bool ccongregation::getExceptionDates(const QDate weekDate, QDate &date1, QDate &date2)
{
    sql_item v = cachedExceptions.value(weekDate);
    if (v.empty()) {
        return  false;
    } else {
        date1 = v.value("date").toDate();
        date2 = v.value("date2").toDate();
        return true;
    }
}

int ccongregation::getMeetingDay(QDate date, meetings meetingtype)
{
    // return meetingday
    int type = 0;
    switch(meetingtype){
    case cbs:
    case tms:
    case sm:
        type = 0;
        break;
    case pm:
    case wt:
        type = 1;
        break;
    }

    // type 0 = school
    // type 1 = publictalk

    updateExCache(date);
    sql_item v = cachedExceptions.value(date);

    int ret;
    if(!v.empty()){
        int exceptiontype = v.value("type").toInt();
        int schoolday = v.value("schoolday").toInt();
        int publicmeetingday = v.value("publicmeetingday").toInt();
        //int cbsday = v.value("cbsday").toInt();
        if(exceptiontype == 0){
            // circuit overseers visit
            if (type == 0){
                if (meetingtype == cbs){
                    // cong. bible study (no meeting)
                    ret = 0;
                }else{
                    // school and service meeting
                    ret = 2;
                }
            }else{
                // use default value for public meeting
                ret = getMyCongregation().getPublicmeeting(date).getMeetingday();
            }
        }else if(exceptiontype == 1){
            // convention -> no meetings
            ret = 0;
        }else if(exceptiontype == 2){
            // memorial
            if (date.year() <= 2018) {
                ret = type == 0 ? schoolday : publicmeetingday;
            } else {
                // when the memorial falls on a weekday, no midweek meetin will be scheduled
                // when the memorial falls on a weekend, no weekend meeting will be scheduled
                if (v.value("date").toDate().dayOfWeek() <= 5)
                    ret = type == 0 ? 0 : publicmeetingday;
                else
                    ret = type == 0 ? schoolday : 0;
            }
        }else{
            // other exception
            if (type == 0){
                ret = schoolday;
            }else{
                ret = publicmeetingday;
            }
        }
    }else{
        // no exceptions -> return default values
        if(type == 0){
            ret = sql->getIntSetting("school_day",1);
        }else{
            ret = getMyCongregation().getPublicmeeting(date).getMeetingday();
        }
    }
    return ret;
}

void ccongregation::clearExceptionCache()
{
    cachedExceptions.clear();
    minCachedException = QDate::fromString("12-31-2035", "MM-dd-yyyy");
}

void ccongregation::updateExCache(QDate date)
{
    if (date < minCachedException) {
        sql_items s;
        s = sql->selectSql(QString("SELECT * FROM exceptions where date <= '%1' AND date2 >= '%2' and active").arg(
                               minCachedException.toString(Qt::ISODate),date.toString(Qt::ISODate)));
        for (sql_item v: s) {            
            QDate date1 = v.value("date").toDate();
            date1 = date1.addDays(-date1.dayOfWeek()+1);
            QDate date2 = v.value("date2").toDate();
            date2 = date2.addDays(7-date2.dayOfWeek());
            for (QDate d = date1; d <= date2; d = d.addDays(1)) {
                cachedExceptions.insert(d, v);
            }
        }
        minCachedException = date;
    }
}

bool ccongregation::congregation::save()
{
    if (not this->isValid())
        return false;

    auto sql = &Singleton<sql_class>::Instance();

    sql_item s;
    s.insert("address",this->address);
    s.insert("meeting1_time",time_meeting1);
    s.insert("name",this->name);
    s.insert("circuit",this->circuit);
    s.insert("info", this->info);
    bool savedOK = sql->updateSql("congregations","id",QVariant(this->id).toString(),&s);
    if (!savedOK)
        return savedOK;

    ccongregation::meeting_dayandtime const& mtgtime_now = this->getPublicmeeting_now();
    if (mtgtime_now.getIsdirty() && mtgtime_now.getIsvalid()) {
        s.clear();
        s.insert("mtg_day", mtgtime_now.getMeetingday());
        s.insert("mtg_time", mtgtime_now.getMeetingtime());
        savedOK = sql->updateSql("congregationmeetingtimes", "id", QVariant(mtgtime_now.getId()).toString(), &s);
    }
    ccongregation::meeting_dayandtime& mtgtime = this->getPublicmeeting_next();
    if (!mtgtime.getIsvalid()) {
        mtgtime.setMeetingday(this->getPublicmeeting_now().getMeetingday());
        mtgtime.setMeetingtime(this->getPublicmeeting_now().getMeetingtime());
    }
    if (mtgtime.getIsdirty() && mtgtime.getIsvalid()) {
        s.clear();
        s.insert("mtg_day", mtgtime.getMeetingday());
        s.insert("mtg_time", mtgtime.getMeetingtime());
        savedOK = sql->updateSql("congregationmeetingtimes", "id", QVariant(mtgtime.getId()).toString(), &s);
    }
    return savedOK;
}

ccongregation::meeting_dayandtime const& ccongregation::congregation::getPublicmeeting(QDate onDate) const
{
    ccongregation::meeting_dayandtime const& dayandtime = this->getPublicmeeting(onDate.year());
    QDate ptDate(onDate.addDays(dayandtime.getMeetingday() - 1));
    // call it again to make sure we have the time for the actual meeting day, not week of
    return this->getPublicmeeting(ptDate.year());
}

ccongregation::meeting_dayandtime const& ccongregation::congregation::getPublicmeeting(int onYear) const
{
    if (onYear > publicmeeting_now.getOfyear())
        return publicmeeting_next;
    return publicmeeting_now;
}

ccongregation::meeting_dayandtime const& ccongregation::congregation::getPublicmeeting_next() const
{
    return publicmeeting_next;
}

ccongregation::meeting_dayandtime const& ccongregation::congregation::getPublicmeeting_now() const
{
    return publicmeeting_now;
}

ccongregation::meeting_dayandtime& ccongregation::congregation::getPublicmeeting(QDate onDate)
{
    ccongregation::meeting_dayandtime const& dayandtime = this->getPublicmeeting(onDate.year());
    QDate ptDate(onDate.addDays(dayandtime.getMeetingday() - 1));
    // call it again to make sure we have the time for the actual meeting day, not week of
    return this->getPublicmeeting(ptDate.year());
}

ccongregation::meeting_dayandtime& ccongregation::congregation::getPublicmeeting(int onYear)
{
    if (onYear > publicmeeting_now.getOfyear())
        return publicmeeting_next;
    return publicmeeting_now;
}

ccongregation::meeting_dayandtime& ccongregation::congregation::getPublicmeeting_next()
{
    return publicmeeting_next;
}

ccongregation::meeting_dayandtime& ccongregation::congregation::getPublicmeeting_now()
{
    return publicmeeting_now;
}
