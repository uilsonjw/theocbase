/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2019, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import net.theocbase.mobile 1.0
import net.theocbase 1.0

Page {
    id: pagePublisherDetail
    property bool canEditPublishers: accessControl.user && accessControl.user.hasPermission(PermissionRule.CanEditPublishers)
    property bool canViewStudentData: accessControl.user && accessControl.user.hasPermission(PermissionRule.CanViewStudentData)
    property bool canEditStudentData: accessControl.user && accessControl.user.hasPermission(PermissionRule.CanEditStudentData)
    property bool canViewPrivileges: accessControl.user && accessControl.user.hasPermission(PermissionRule.CanViewPrivileges)
    property bool canEditPrivileges: accessControl.user && accessControl.user.hasPermission(PermissionRule.CanEditPrivileges)
    readonly property color treasuresColor: "#eff0f1"
    readonly property color applyMinistryColor: "#f9f3ea"
    readonly property color livingColor: "#f5e8ea"

    signal pageClosed()

    function returnBackPage(){
        // hide keyboard
        if (Qt.inputMethod.visible) Qt.inputMethod.hide()
        // return to previous page
        pageClosed()
        stackView.pop()
    }    
    function saveChanges(accept){
        if (p.id < 0 && !accept){
            if (textFirstname.text === "" && textLastname.text === ""){
                _msg.show("Save","Type first and last name!",1);
            }else{
                _msg.showYesNo("Save","Add a new publisher?",1);
            }
            return;
        }

        // Save use for
        var v = p.usefor;
        v = prepareUsefor(switchAuxClasses.checked,v,Publisher.SchoolAux)
        v = prepareUsefor(switchMainClass.checked,v,Publisher.SchoolMain)
        v = prepareUsefor(switchChairman.checked,v,Publisher.LMM_Chairman)
        v = prepareUsefor(switchTreasures.checked,v,Publisher.LMM_Treasures)
        v = prepareUsefor(switchDigging.checked,v,Publisher.LMM_Digging)
        v = prepareUsefor(switchReading.checked,v,Publisher.LMM_BibleReading)
        v = prepareUsefor(switchInitialCall.checked,v,Publisher.LMM_InitialCall)
        v = prepareUsefor(switchReturnVisit.checked,v,Publisher.LMM_ReturnVisit)
        v = prepareUsefor(switchBibleStudy.checked,v,Publisher.LMM_BibleStudy)
        v = prepareUsefor(switchApplyTalks.checked,v,Publisher.LMM_ApplyTalks)
        v = prepareUsefor(switchLivingTalks.checked,v,Publisher.LMM_LivingTalks)
        v = prepareUsefor(switchOtherVideoPart.checked,v,Publisher.LMM_OtherVideoPart)
        v = prepareUsefor(switchCBS.checked,v,Publisher.CBSConductor)
        v = prepareUsefor(switchCBSReader.checked,v,Publisher.CBSReader)
        v = prepareUsefor(switchPrayer.checked, v, Publisher.Prayer)

        v = prepareUsefor(switchAssistant.checked,v,Publisher.Assistant)
        v = prepareUsefor(!switchActive.checked,v,Publisher.IsBreak)

        v = prepareUsefor(checkHospitality.checked,v,Publisher.Hospitality)

        v = prepareUsefor(checkWeekendChairman.checked,v,Publisher.Chairman)
        v = prepareUsefor(checkWtConductor.checked,v,Publisher.WtCondoctor)
        v = prepareUsefor(checkWtReader.checked,v,Publisher.WtReader)

        var gender = checkBrother.checked ? Publisher.Male : Publisher.Female
        if (textFirstname.text != p.firstname ||
                textLastname.text != p.lastname ||
                checkServant.checked != p.servant ||
                gender !== p.gender ||
                v !== p.usefor ||
                textMobile.text !== p.mobile ||
                textPhone.text !== p.phone ||
                textEmail.text !== p.email){
            console.log("need to save changes");
            p.firstname = textFirstname.text
            p.lastname = textLastname.text
            p.servant = checkServant.checked
            p.gender = gender
            p.usefor = v;
            p.mobile = textMobile.text
            p.phone = textPhone.text
            p.email = textEmail.text
            _publishers.save(p)
        }

        // Save publisher's family
        // Get previous saved family
        // var prev_family = _family.getPersonFamily(p.id)
        if (checkFamilyHead.checked){
            if(!f || f.headId !== p.id){
                // add a new family
                _family.getOrAddFamily(p.id)
            }
        }else{
            if (f && f.headId === p.id){
                // remove family
                f.removeFamily()
            }
            // Family member
            if (familiesListPage.selectedRow > 0){
                // add member
                if (!f || (f.headId !== familiesListPage.model[familiesListPage.selectedRow].headId)){
                    familiesListPage.model[familiesListPage.selectedRow].addMember(p.id)
                }
            }else{
                // remove member
                if (f) f.removeMember(p.id)
            }
        }

        returnBackPage();
    }

    function prepareUsefor(checked,allvalue, value){
        //console.log("checked:" + checked.toString() + " value:" + value.toString())
        if (checked){
            //console.log("not exist " + (!(allvalue & value)).toString())
            if (!(allvalue & value)) allvalue += value;
        }else{
            //console.log("should be removed " + (allvalue & value).toString())
            if (allvalue & value) allvalue -= value;
        }
        return allvalue;
    }

    property bool isNew: p.id < 0
    property Publisher p
    property Family f
    MsgBox {id: _msg}
    Connections {
        target: _msg
        function onButtonClicked(ok, id) {
            if (ok){
                switch (id){
                case 1:
                    // add a new publisher
                    console.log("add a new publisher")
                    saveChanges(true)
                    break;
                case 2:
                    // call phone number
                    Qt.openUrlExternally("tel:" + textPhone.text); break;
                default: break;
                }
            }
        }
    }

    // Family
    Family { id:_family }
    SelectionListPage {
        id: familiesListPage
        pageTitle: "Families"
        visible: false
        showName: true
        model: _family.getFamiliesVariantList()
        onActiveRowChanged: {
            console.log("families row changed - index:" + index)
            //f = familiesListPage.model[index]
            textLinkedFamily.text = familiesListPage.model[index].name
        }
        Component.onCompleted: {
            f = _family.getPersonFamily(p.id)
            if (f){
                for(var i=0;i<familiesListPage.model.length;i++){
                    if (familiesListPage.model[i].headId === f.headId){
                        familiesListPage.selectedRow = i
                        break;
                    }
                }
            }else{
                familiesListPage.selectedRow = 0
            }
        }
    }

    header: BaseToolbar {
        title: p.id < 0 ? qsTr("New publisher") : p.fullname;
        componentLeft: ToolButton {
            icon.source: "qrc:///cancel.svg"
            icon.color: "white"
            onClicked: { returnBackPage() }
        }
        componentRight: ToolButton {
            icon.source: "qrc:///done.svg"
            icon.color: enabled ? "white" : "grey"
            onClicked: { saveChanges() }
            enabled: textFirstname.text.length > 0 && textLastname.text.length > 0 &&
                     canEditPublishers
        }
    }

    ScrollView {
        id: scrollView
        width: parent.width
        height: parent.height
        contentWidth: width

        ColumnLayout {
            id: layout
            spacing: 0
            width: pagePublisherDetail.availableWidth
            enabled: canEditPublishers

            RowTitle { text: qsTr("Info") }
            RowText {
                id: textFirstname
                title: qsTr("First Name")
                text: p.firstname
                editable: true
                firstrow: true
            }
            RowText {
                id: textLastname
                title: qsTr("Last Name")
                text: p.lastname
                editable: true
            }

            Item { height: 10*dpi; width: parent.width }
            ButtonGroup { id: genderGrp }
            RowSwitch {
                id: checkBrother
                title: qsTr("Brother")
                checked: false
                checkbox: true
                exclusiveGroup: genderGrp
                firstrow: true
                Component.onCompleted: {
                    checkBrother.checked = p.gender === Publisher.Male
                    checkSister.checked = !checkBrother.checked
                }
            }
            RowSwitch {
                id: checkSister
                title: qsTr("Sister")
                checked: false
                checkbox: true
                exclusiveGroup: genderGrp
            }
            RowSwitch {
                id: switchActive
                title: qsTr("Active")
                checked: (!(p.usefor & Publisher.IsBreak))
            }
            RowSwitch {
                id: checkHospitality
                title:  qsTr("Host for Public Speakers")
                checked: p.usefor & Publisher.Hospitality
            }
            RowSwitch {
                id: checkServant
                title: qsTr("Servant")
                checked: p.servant
                visible: checkBrother.checked
            }
            RowSwitch {
                id: switchPrayer
                title: qsTr("Prayer")
                checked: p.usefor & Publisher.Prayer
                enabled: p.gender === Publisher.Male
            }

            RowTitle { text: qsTr("Family") }
            RowSwitch {
                id: checkFamilyHead
                title: qsTr("Family Head")
                checked: (f && p.id == f.headId)
                firstrow: true
            }
            RowText {
                id: textLinkedFamily
                title: qsTr("Family member linked to")
                text: f ? f.name : ""
                visible: !checkFamilyHead.checked
                arrow: true
                onClicked: {
                    if (Qt.inputMethod.visible) Qt.inputMethod.hide()
                    stackView.push(familiesListPage)
                }
            }

            RowTitle { text: qsTr("Contact Information") }
            RowText {
                id: textMobile
                title: qsTr("Mobile")
                text: p.mobile
                editable: true
                firstrow: true
                onPressAndHold: {
                    if (textMobile.text.length > 6 && app_info.screensize < 7) {
                        var globalCoordinares = mapToItem(scrollView.contentItem, mouse.x, mouse.y)
                        popupMobile.show(globalCoordinares.x,globalCoordinares.y)
                    }
                }
            }
            RowText {
                id: textPhone
                title: qsTr("Phone")
                text: p.phone
                editable: true
                firstrow: true
                onPressAndHold: {
                    if (textPhone.text.length > 6 && app_info.screensize < 7) {
                        var globalCoordinares = mapToItem(scrollView.contentItem, mouse.x, mouse.y)
                        popupPhone.show(globalCoordinares.x,globalCoordinares.y)
                    }
                }
            }
            RowText {
                id: textEmail
                title: qsTr("Email")
                text: p.email
                editable: true
            }


            RowTitle {
                text: qsTr("Midweek Meeting")
                visible: canViewPrivileges
            }
            RowSwitch {
                id: switchChairman
                title: qsTr("Chairman")
                checked: p.usefor & Publisher.LMM_Chairman
                enabled: checkServant.checked &&
                         canEditPrivileges
                textColor: "black"
                backgroundColor: treasuresColor
                visible: canViewPrivileges
            }
            RowSwitch {
                id: switchTreasures
                title: qsTr("Treasures from God's Word")
                checked: p.usefor & Publisher.LMM_Treasures
                enabled: checkServant.checked &&
                         canEditPrivileges
                textColor: "black"
                backgroundColor: treasuresColor
                visible: canViewPrivileges
            }
            RowSwitch {
                id: switchDigging
                title: qsTr("Spiritual Gems")
                checked: p.usefor & Publisher.LMM_Digging
                enabled: checkServant.checked &&
                         canEditPrivileges
                textColor: "black"
                backgroundColor: treasuresColor
                visible: canViewPrivileges
            }
            RowSwitch {
                id: switchReading
                title: qsTr("Bible Reading")
                checked: p.usefor & Publisher.LMM_BibleReading
                textColor: "black"
                backgroundColor: treasuresColor
                visible: canViewStudentData
                enabled: canEditStudentData
            }

            RowSwitch {
                id: switchInitialCall
                title: qsTr("Initial Call")
                checked: p.usefor & Publisher.LMM_InitialCall
                textColor: "black"
                backgroundColor: applyMinistryColor
                visible: canViewStudentData
                enabled: canEditStudentData
            }
            RowSwitch {
                id: switchReturnVisit
                title: qsTr("Return Visit")
                checked: p.usefor & Publisher.LMM_ReturnVisit
                textColor: "black"
                backgroundColor: applyMinistryColor
                visible: canViewStudentData
                enabled: canEditStudentData
            }
            RowSwitch {
                id: switchBibleStudy
                title: qsTr("Bible Study")
                checked: p.usefor & Publisher.LMM_BibleStudy
                textColor: "black"
                backgroundColor: applyMinistryColor
                visible: canViewStudentData
                enabled: canEditStudentData
            }
            RowSwitch {
                id: switchApplyTalks
                title: qsTr("Talk")
                checked: p.usefor & Publisher.LMM_ApplyTalks
                textColor: "black"
                backgroundColor: applyMinistryColor
                visible: canViewStudentData
                enabled: canEditStudentData
            }

            RowSwitch {
                id: switchAssistant
                title: qsTr("Assistant")
                checked: p.usefor & Publisher.Assistant
                textColor: "black"
                backgroundColor: applyMinistryColor
                visible: canViewStudentData
                enabled: canEditStudentData
            }

            RowSwitch {
                id: switchOtherVideoPart
                title: qsTr("Discussion with Video")
                checked: p.usefor & Publisher.LMM_OtherVideoPart
                textColor: "black"
                backgroundColor: applyMinistryColor
                visible: canViewPrivileges
                enabled: checkServant.checked &&
                         canEditPrivileges
            }

            RowSwitch {
                id: switchLivingTalks
                title: qsTr("Living as Christians Talks")
                checked: p.usefor & Publisher.LMM_LivingTalks
                enabled: checkServant.checked &&
                         canEditPrivileges
                textColor: "black"
                backgroundColor: livingColor
                visible: canViewPrivileges
            }
            RowSwitch {
                id: switchCBS
                title: qsTr("Congregation Bible Study")
                checked: p.usefor & Publisher.CBSConductor
                textColor: "black"
                backgroundColor: livingColor
                enabled: checkServant.checked &&
                         canEditPrivileges
                visible: canViewPrivileges
            }
            RowSwitch {
                id: switchCBSReader
                title: qsTr("Cong. Bible Study Reader")
                checked: p.usefor & Publisher.CBSReader
                textColor: "black"
                backgroundColor: livingColor
                enabled: p.gender === Publisher.Male &&
                         canEditPrivileges
                visible: canViewPrivileges
            }

            //RowTitle { text: qsTr("Use for School") }
            Item { height: 10*dpi; width: parent.width }
            ButtonGroup { id: classesGrp }
            RowSwitch {
                id: switchAllClasses
                checkbox: true
                title: qsTr("All Classes")
                checked: !(p.usefor & Publisher.SchoolMain || p.usefor & Publisher.SchoolAux)
                exclusiveGroup: classesGrp
                firstrow: true
                visible: canViewStudentData
                enabled: canEditStudentData
            }
            RowSwitch {
                id: switchMainClass
                checkbox: true
                title: qsTr("Only Main Class")
                checked: p.usefor & Publisher.SchoolMain
                exclusiveGroup: classesGrp
                visible: canViewStudentData
                enabled: canEditStudentData
            }
            RowSwitch {
                id: switchAuxClasses
                checkbox: true
                title: qsTr("Only Auxiliary Classes")
                checked: p.usefor & Publisher.SchoolAux
                exclusiveGroup: classesGrp
                visible: canViewStudentData
                enabled: canEditStudentData
            }

            RowTitle {
                text: qsTr("Weekend Meeting")
                visible: canViewPrivileges
            }
            RowSwitch {
                id: checkWeekendChairman
                title: qsTr("Chairman")
                checked: p.usefor & Publisher.Chairman
                enabled: checkServant.checked &&
                         canEditPrivileges
                visible: canViewPrivileges
            }
            RowSwitch {
                id: checkWtConductor
                title: qsTr("Watchtower Study Conductor")
                checked: p.usefor & Publisher.WtCondoctor
                enabled: checkServant.checked &&
                         canEditPrivileges
                visible: canViewPrivileges
            }
            RowSwitch {
                id: checkWtReader
                title: qsTr("Watchtower Study Reader")
                checked: p.usefor & Publisher.WtReader
                enabled: checkBrother.checked &&
                         canEditPrivileges
                visible: canViewPrivileges
            }

        } // layout
    } // scrollView

    PopupMenu {
        id: popupPhone
        menuContent: RowLayout {
            anchors.fill: parent
            ToolButton {
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.preferredWidth: height
                icon.source: "qrc:///phone.svg"
                icon.color: "white"
                onClicked: {
                    // call phone number
                    Qt.openUrlExternally("tel:" + textPhone.text)
                    popupPhone.visible = false
                }
            }
            ToolButton {
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.preferredWidth: height
                icon.source: "qrc:///sms.svg"
                icon.color: "white"
                onClicked: {
                    // send sms
                    Qt.openUrlExternally("sms:" + textPhone.text)
                    popupPhone.visible = false
                }
            }
        }
    }

    PopupMenu {
        id: popupMobile
        menuContent: RowLayout {
            anchors.fill: parent
            ToolButton {
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.preferredWidth: height
                icon.source: "qrc:///phone.svg"
                icon.color: "white"
                onClicked: {
                    // call mobile number
                    Qt.openUrlExternally("tel:" + textMobile.text)
                    popupMobile.visible = false
                }
            }
            ToolButton {
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.preferredWidth: height
                icon.source: "qrc:///sms.svg"
                icon.color: "white"
                onClicked: {
                    // send sms
                    Qt.openUrlExternally("sms:" + textMobile.text)
                    popupMobile.visible = false
                }
            }
        }
    }
}

