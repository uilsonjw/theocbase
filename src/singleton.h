#ifndef SINGLETON_H
#define SINGLETON_H

#include <QObject>
#include "sharedlib_global.h"

template <class T>
class TB_EXPORT Singleton
{
public:
    static T& Instance()
    {
        static T _instance; // create static instance of our class
        return _instance;   // return it
    }

private:
    Singleton();	// hide constructor
    ~Singleton();	// hide destructor
    Singleton(const Singleton &); // hide copy constructor
    Singleton& operator=(const Singleton &); // hide assign op
};

#endif // SINGLETON_H
