/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2019, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.5
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.3
import QtQuick.Layouts 1.3
import QtQuick.Window 2.1
import net.theocbase.mobile 1.0
import net.theocbase 1.0

Page {
    id: assignmentDialog
    property bool canSendMidweekMeetingReminders: accessControl.user && accessControl.user.hasPermission(PermissionRule.CanSendMidweekMeetingReminders)
    property string returnValue: ""
    property LMM_Assignment currentAssignment
    property int oldSpeakerId: -1
    property int oldAssistantId: -1
    property int oldVolunteerId: -1
    property int currentStudyId: 0
    property int nextStudyId: 0
    property string _current_timer_title: _timer_text
    property color _timer_color: "black"
    property string _timer_text: qsTr("Stopwatch")
    property string _leave_study: qsTr("Leave on current study")
    property string _start_timer_text: qsTr("Start stopwatch")
    property string _stop_timer_text: qsTr("Stop stopwatch")
    property date _startTime
    property int currentindex: -1
    property int modelLength: 0
    signal gotoNext()
    signal gotoPrevious()

    AssignmentController { id: myController }
    onCurrentAssignmentChanged: {
        if (!currentAssignment) return
        myController.assignment = currentAssignment
        oldSpeakerId = currentAssignment.speaker ? currentAssignment.speaker.id : -1
        oldAssistantId = currentAssignment.assistant ? currentAssignment.assistant.id : -1
        oldVolunteerId = currentAssignment.volunteer ? currentAssignment.volunteer.id : -1
    }
    function saveChanges(){
        if (Qt.inputMethod.visible)
            Qt.inputMethod.hide()
        if (!currentAssignment)
            return
        if (_timer.running){
            _msg.showYesNo(_stop_timer_text,"",1)
            return false
        }

        var s_id = currentAssignment.speaker ? currentAssignment.speaker.id : -1
        var a_id = currentAssignment.assistant ? currentAssignment.assistant.id : -1
        var v_id = currentAssignment.volunteer ? currentAssignment.volunteer.id : -1

        if (currentAssignment.note !== textAreaNotes.text ||
                currentAssignment.completed !== checkCompleted.checked ||
                currentAssignment.setting !== textSetting.text ||
                currentAssignment.timing !== textTiming.text ||
                s_id !== oldSpeakerId || a_id !== oldAssistantId || v_id !== oldVolunteerId){
            currentAssignment.note = textAreaNotes.text
            currentAssignment.completed = checkCompleted.checked
            currentAssignment.setting = textSetting.text
            currentAssignment.timing = textTiming.text
            currentAssignment.save()
            console.log("assignment saved")
            oldSpeakerId = s_id
            oldAssistantId = a_id
        }

        if (currentAssignment.speaker && currentAssignment.date.getFullYear() < 2019){
            if (currentAssignment.volunteer)
                myController.saveStudy(checkExerice.checked,
                                       false, currentStudyId)
            else
                myController.saveStudy(checkExerice.checked,
                                       checkCompleted.checked,
                                       nextStudyId)
        }
        return true
    }
    function timerTriggered(){
        if (_timer.running){
            _timer.stop()
            _timer_color = _timer_color
            //timertext.text = _start_timer_text
            _msg.showYesNo(qsTr("Add the timing?"), title.text,2)
        }else{
            _startTime = new Date()
            _timer.running = true
            _timer_color = "black"
            //timertext.text = _stop_timer_text
        }
    }
    MsgBox { id: _msg }
    Connections {
        target: _msg
        function  onButtonClicked(ok, id) {
            switch (id){
            case 1:
                if (ok) timerTriggered()
                break
            case 2:
                if (ok) textTiming.text = _current_timer_title
                _current_timer_title = _timer_text
                saveChanges()
                break
            default: break
            }
        }
    }
    Timer {
        id: _timer
        interval: 500
        running: false
        repeat: true
        triggeredOnStart: true
        onTriggered: {
            var elapsedTime = Math.round((new Date().getTime() - _startTime.getTime()) / 1000)
            var minutes = Math.floor(elapsedTime / 60)
            var seconds = elapsedTime-minutes * 60
            _current_timer_title = (minutes < 10 ? "0" : "") + minutes.toString() +
                    ":" + (seconds < 10 ? "0" : "") + seconds.toString()
            _timer_color
        }
    }

    header: ToolBar {
        height: 45 * dpi + toolbarTopMargin        
        RowLayout {
            anchors.fill: parent
            anchors.topMargin: toolbarTopMargin
            ToolButton {
                icon.source: "qrc:/back.svg"
                icon.color: "white"
                onClicked: {
                    if (!saveChanges()) return
                    stackView.pop()
                }
            }
            Label {
                Layout.fillWidth: true
                text: qsTr("Details", "Page title")
                horizontalAlignment: Text.AlignHCenter
                font.bold: true
                font.pointSize: app_info.fontsize
                color: "white"
            }
            ToolButton {
                icon.source: "qrc:/share.svg"
                icon.color: "white"
                visible: canSendMidweekMeetingReminders
                onClicked: {
                    shareUtils.setPoint(mapToGlobal(width/2, height))
                    shareUtils.shareText(currentAssignment.getReminderText())
                }
            }
            ToolButton {
                icon.source: "qrc:/assignment_previous.svg"
                icon.color: enabled ? "white" : "grey"
                enabled: currentindex > 0
                onClicked: {
                    if (!saveChanges()) return;
                    gotoPrevious()
                }
            }
            ToolButton {
                icon.source: "qrc:/assignment_next.svg"
                icon.color: enabled ? "white" : "grey"
                enabled: currentindex < (modelLength -1)
                onClicked: {
                    if (!saveChanges()) return;
                    gotoNext()
                }
            }
        }
    }

    ScrollView {
        id: flickable
        width: parent.width
        height: parent.height
        contentWidth: availableWidth
        ColumnLayout {
            id: layout
            width: assignmentDialog.availableWidth
            spacing: 0

            RowLayout {
                Layout.fillWidth: true
                visible: !checkCompleted.checked && currentAssignment && currentAssignment.speaker
                ToolButton {
                    icon.source: "qrc:///timer.svg"
                    Layout.fillHeight: true
                    Layout.preferredWidth: height
                    onClicked: timerTriggered()
                }
                Text {
                    id: timertext
                    text: _current_timer_title
                    color: _timer_color
                    font.pointSize: app_info.fontsize
                    horizontalAlignment: Text.AlignRight
                    verticalAlignment: Text.AlignVCenter
                    Layout.preferredHeight: 40*dpi
                    Layout.fillWidth: true
                    Layout.rightMargin: 10*dpi
                }
            }
            RowTitle {
                text: qsTr("Theme")
            }
            Label {
                Layout.fillWidth: true
                text: currentAssignment ? currentAssignment.theme : ""
                wrapMode: Text.WordWrap
                padding: 10*dpi
                font.pointSize: app_info.fontsize
            }
            RowTitle {
                text: qsTr("Source")
            }
            Label {
                Layout.fillWidth: true
                text: currentAssignment ? currentAssignment.source : ""
                wrapMode: Text.WordWrap
                padding: 10*dpi
                font.pointSize: app_info.fontsize
            }

            RowTitle {
                id: studentTitle
                text: qsTr("Student")
            }

            ComboBoxTable {
                Layout.fillWidth: true
                currentText: currentAssignment && currentAssignment.speaker ? currentAssignment.speaker.fullname : ""
                enabled: !checkCompleted.checked
                col1Name: studentTitle.text
                onBeforeMenuShown: {
                    model = currentAssignment.getSpeakerList()
                }
                onRowSelected: {
                    var student = CPersons.getPerson(id)
                    currentAssignment.speaker = student
                    saveChanges()
                }
                onCurrentTextChanged: {
                    if (text.length > 2){
                        var study = myController.currentStudy
                        currentStudyId = study.number
                        textCurrentStudy.text = study.number + " " + study.name
                        checkExerice.checked = study.exercise
                        nextStudyId = myController.nextStudy
                        var nextnumber = currentAssignment.completed ? nextStudyId : study.number
                        textNextStudy.text = nextnumber === study.number ? _leave_study : myController.getStudies()[nextnumber]
                    }else{
                        textCurrentStudy.text = ""
                    }
                }
                onTooltipRequest: {
                    if (model.get(row).id !== "undefined") {
                        var userid = model.get(row).id
                        rowTooltip = myController.getHistoryTooltip(userid, app_info.fontsizeSmall)
                    }
                }
            }

            RowTitle {
                id: assistantTitle
                text: qsTr("Assistant","Assistant to student")
                visible: currentAssignment && currentAssignment.canHaveAssistant
            }

            ComboBoxTable {
                Layout.fillWidth: true
                currentText: currentAssignment && currentAssignment.assistant ? currentAssignment.assistant.fullname : ""
                visible: currentAssignment && currentAssignment.canHaveAssistant                
                enabled: currentAssignment && currentAssignment.speaker  && !checkCompleted.checked
                col1Name: assistantTitle.text
                onBeforeMenuShown: {
                    model = currentAssignment.getAssistantList()
                }
                onRowSelected: {
                    var assistant = CPersons.getPerson(id)
                    currentAssignment.assistant = assistant
                    if (currentAssignment.assistant.gender !== currentAssignment.speaker.gender &&
                            (currentAssignment.talkId === LMM_Schedule.TalkType_ReturnVisit1 ||
                             currentAssignment.talkId === LMM_Schedule.TalkType_ReturnVisit2 ||
                             currentAssignment.talkId === LMM_Schedule.TalkType_ReturnVisit3 ||
                             currentAssignment.talkId === LMM_Schedule.TalkType_BibleStudy)){
                        _message.show("TheocBase", qsTr('The assistant should not be someone of the opposite sex.'));
                    }
                    saveChanges()
                }
                MsgBox { id: _message }
                onTooltipRequest: {
                    if (model.get(row).id !== "undefined") {
                        var userid = model.get(row).id
                        rowTooltip = myController.getHistoryTooltip(userid, app_info.fontsizeSmall)
                    }
                }
            }

            RowTitle {
                text: qsTr("Study point")
                visible: currentAssignment.date.getFullYear() >= 2019
            }
            RowText {
                id: textSetting
                text: currentAssignment.studyNumber + " " + currentAssignment.studyName
                visible: currentAssignment.date.getFullYear() >= 2019
                editable: false
            }

            RowTitle { text: qsTr("Result") }

            RowSwitch {
                id: checkCompleted
                title: qsTr("Completed")
                checked: currentAssignment && currentAssignment.completed
                enabled: currentAssignment && currentAssignment.speaker
                onClicked: saveChanges()
            }

            RowText {
                id: textVolunteer
                title: qsTr("Volunteer")
                text: currentAssignment && currentAssignment.volunteer ? currentAssignment.volunteer.fullname : ""
                arrow: true
                visible: checkCompleted.checked
                onClicked: {
                    var mylist = currentAssignment.getSpeakerList()
                    var rowindex = currentAssignment.volunteer ?
                                mylist.find(currentAssignment.volunteer.id,1) : -1
                    var volunteersPage = stackView.push(Qt.resolvedUrl("SelectionListPage.qml"),
                                                        {showName: true, model: mylist, selectedRow: rowindex, pageTitle: title})
                    volunteersPage.onActiveRowChanged.connect(function(index){
                        var id = mylist.get(index).id
                        if (typeof id === "undefined"){
                            text = ""
                            currentAssignment.volunteer = null
                        }else{
                            text = mylist.get(index).name
                            currentAssignment.volunteer = myController.getPublisherById(id)
                        }
                        saveChanges()
                    })
                }
            }

            RowText {
                id: textTiming
                title: qsTr("Timing")
                text: currentAssignment ? currentAssignment.timing : ""
                editable: true
                onEditingFinished: saveChanges()
            }

            RowText {
                id: textCurrentStudy
                title: qsTr("Current Study")
                visible: currentAssignment.date.getFullYear() < 2019
            }

            RowSwitch {
                id: checkExerice
                title: qsTr("Exercises Completed")
                enabled: checkCompleted.checked
                visible: currentAssignment.date.getFullYear() < 2019
            }
            RowText {
                id: textNextStudy
                title: qsTr("Next Study")
                arrow: true
                visible: checkCompleted.checked && currentAssignment.date.getFullYear() < 2019
                enabled: checkExerice.checked
                onClicked: {
                    // hide keyboard
                    if (Qt.inputMethod.visible) Qt.inputMethod.hide()
                    var stydyPage = stackView.push(Qt.resolvedUrl("SelectionListPage.qml"),
                                                   {model: myController.getStudies(), pageTitle: qsTr("Select next study"),
                                                       selectedRow: nextStudyId})
                    stydyPage.onActiveRowChanged.connect(function(index){
                        console.log("selected study " + index)
                        nextStudyId = index
                        textNextStudy.text = index === currentStudyId ? _leave_study : myController.getStudies()[index]
                    })
                }
            }

            RowTitle { text: qsTr("Notes") }

            TextArea {
                id: textAreaNotes
                Layout.fillWidth: true                
                Layout.minimumHeight: 100*dpi
                text: currentAssignment ? currentAssignment.note : ""
                font.pointSize: app_info.fontsize
                wrapMode: Text.WordWrap
                leftPadding: 10*dpi
                rightPadding: 10*dpi
                background: Rectangle {
                    color: "transparent"
                }
                onEditingFinished: saveChanges()
            }
        }
    }
}
