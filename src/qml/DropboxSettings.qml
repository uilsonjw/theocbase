import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.3
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2
import net.theocbase 1.0

Rectangle {
    id: dialog
    width: 300
    height: 500

    LayoutMirroring.enabled: Qt.application.layoutDirection === Qt.RightToLeft
    LayoutMirroring.childrenInherit: true

    property bool canDeleteCloudData: cloud.canResetCloudData()
    property DBAccount account: cloud.authentication.getAccountInfo()
    property string syncfile: account.syncFile
    property bool logged: false

    property int preferredHeight: 0

    color: Material.background

    ListModel { id: defaultList }

    function loadList(showAll) {
        if (showAll) {
            listView.model = cloud.authentication.searchFile("syncfile.json")
        } else {
            listView.model = defaultList
            listView.model.clear()
            listView.model.append({"path": syncfile, "checked":true})
        }
        dialog.enabled = true
    }

    function calculateHeight() {
        if (logged) {
            preferredHeight = (buttonEditSyncFile.listLoaded ? 500 : 380)
        } else {
            preferredHeight = 110
        }
        heightChanged(preferredHeight)
        height = preferredHeight
    }

    Component.onCompleted: {
        logged = cloud.logged
        if (logged)
            loadList(false)
        calculateHeight()
    }

    Connections {
        target: cloud
        function onLoggedChanged(ok) {
            logged = ok
            calculateHeight()
            if (ok) {
                labelName.text = account.name
                labelEmail.text = account.email
                loadList()
            } else {
                labelName.text = ""
                labelEmail.text = ""
                listView.model = null
            }
        }
        function onSyncStarted() { dialog.enabled = false }
        function onSyncFinished() { dialog.enabled = true }
        function onCloudResetStarted() { dialog.enabled = false }
        function onCloudResetFinished() { dialog.enabled = true }
        function onError() { dialog.enabled = true }
    }

    MessageDialog {
        id: msgbox
        text: qsTr("Are you sure you want to permanently delete your cloud data?")
        standardButtons: MessageDialog.Yes | MessageDialog.No
        icon: StandardIcon.Question
        modality: Qt.ApplicationModal
        onYes: cloud.resetCloudData()
    }

    ColumnLayout {
        id: columnLayout1
        anchors.fill: parent
        anchors.margins: 10

        GroupBox {
            id: groupBox1
            width: 200
            Layout.fillWidth: true

            ColumnLayout {
                id: columnLayout
                anchors.fill: parent

                RowLayout {
                    id: rowLayout
                    width: 100
                    height: 100

                    Image {
                        id: image
                        width: 100
                        height: 100
                        Layout.fillWidth: true
                        fillMode: Image.PreserveAspectFit
                        source: "qrc:/icons/Dropbox.svg"
                    }

                    ToolButton {
                        id: buttonLogin
                        text: ""
                        onClicked: logged ? cloud.logout(2) : cloud.login()
                        width: 40
                        height: 40
                        icon.source: "qrc:/icons/logout.svg"
                    }
                }

                Label {
                    id: labelName
                    text: account.name
                }

                Label {
                    id: labelEmail
                    text: account.email
                }
            }
        }

        GroupBox {
            id: groupBox
            width: 200
            height: 200
            Layout.fillHeight: true
            Layout.fillWidth: true
            enabled: logged

            RowLayout {
                id: rowLayout1
                anchors.fill: parent
                ListView {
                    id: listView
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    clip: true
                    model: ListModel {}
                    delegate: RowLayout {
                        height: 40
                        width: listView.width
                        CheckBox {
                            id: listCheckBox
                            enabled: buttonEditSyncFile.listLoaded
                            checked: account.syncFile === model.path
                            onCheckedChanged: {
                                if (checked)
                                    account.syncFile = model.path
                            }                            
                        }
                        Label {
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                            width: listView.width-30
                            elide: Text.ElideMiddle
                            text: model.path+
                                  (typeof (model.sharedby) == "undefined" || model.sharedby === "" ?
                                       "" : "\n("+ model.sharedby + ")")
                            font.pointSize: 12
                            verticalAlignment: Text.AlignVCenter

                            MouseArea {
                                anchors.fill: parent
                                hoverEnabled: true
                                id: mousearea
                            }
                            ToolTip {
                                text: model.path
                                visible: mousearea.containsMouse
                            }
                        }
                    }
                }
                ToolButton {
                    id: buttonEditSyncFile
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    icon.source: listLoaded ? "qrc:/icons/chevron_up.svg" : "qrc:/icons/chevron_down.svg"
                    property bool listLoaded: false
                    onClicked: {
                        dialog.enabled = false
                        loadList(!listLoaded)
                        listLoaded = !listLoaded
                        calculateHeight()
                    }
                }
            }
        }

        GroupBox {
            id: groupBox2
            width: 200
            height: 200
            Layout.fillWidth: true
            enabled: logged

            GridLayout {
                id: columnLayout2
                anchors.fill: parent
                columns: 2

                Label {
                    id: label
                    text: qsTr("Last synchronized: %1").arg(cloud.lastSyncTime.toLocaleString(Qt.locale(), Locale.ShortFormat))
                    Layout.columnSpan: 2
                }
                Label {
                    text: qsTr("Synchronize")
                    Layout.fillWidth: true
                }
                ToolButton {
                    id: buttonSync
                    icon.source: "qrc:/icons/cloud_sync.svg"
                    onClicked: {
                        cloud.synchronize(false)
                        console.log("sync ready")
                    }
                }
                Label {
                    text: qsTr("Delete Cloud Data")
                    Layout.fillWidth: true
                    visible: canDeleteCloudData
                }
                ToolButton {
                    id: buttonReset
                    icon.source: "qrc:/icons/cloud_delete.svg"
                    visible: canDeleteCloudData
                    onClicked: msgbox.open()
                }
            }
        }

    }
    states: [
        State {
            name: "logout"
            when: !logged

            PropertyChanges {
                target: buttonLogin
                icon.source: "qrc:/icons/login.svg"
            }

            PropertyChanges {
                target: labelEmail
                visible: false
            }

            PropertyChanges {
                target: labelName
                visible: false
            }

            PropertyChanges {
                target: groupBox
                visible: false
            }

            PropertyChanges {
                target: groupBox2
                visible: false
            }                       
        }
    ]
}
