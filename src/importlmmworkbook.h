#ifndef importlmmworkbook_H
#define importlmmworkbook_H

#include <QEventLoop>
#include <QTimer>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QString>
#include <QFile>
#include <QMap>
#include <QRegularExpression>
#include <QXmlStreamReader>
#include <QDate>
#include <QTextStream>
#include <iostream>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QPair>
#include <QDebug>
#include "epub.h"
#include "lmm_meeting.h"
#include "lmm_schedule.h"
#include "xml_reader.h"
#include "sql_class.h"
#include "sharedlib_global.h"

class TB_EXPORT importlmmworkbook : public QObject
{
    Q_OBJECT

public:
    explicit importlmmworkbook(QObject *parent = nullptr);
    importlmmworkbook(QString fileName);
    importlmmworkbook(QString fileName, QString language, bool debugMode);
    importlmmworkbook(QString fileName, bool usePDFLangCode); // debug mode (see lmmworksheetregex)
    ~importlmmworkbook();
    QString Import();
    Q_INVOKABLE QString importFile(const QString fileName);
    void ImportDate(QDate dt, QString href, QString chapter);
    bool DownloadAssistDoc(QDate sampleDate);

    enum xmlPartsContexts {
        tocEntry,
        languageCode,
        body,
        bibleReading,
        bibleReadingDetail,
        section1,
        songDetail,
        section2,
        section3,
        section4,
        talk
    };

    enum foundParts {
        biblereading = 0,
        opensong,
        treasures,
        digging,
        br,
        fm1,
        fm2,
        fm3,
        fm4,
        midsong,
        talk1,
        talk2,
        talk3,
        talk4,
        talk5,
        endsong,
        lastItem = endsong
    };

    QHash<QString, QString> regexes;
    QStringList tocEntries;
    QList<QDate *> tocEntriesDate;
    QStringList tocEntriesHTML;
    QStringList meetingResults;
    QHash<QString, QString> newSettingValues;
    epub epb;
    bool prepared;

public slots:
    void xmlPartFound(QXmlStreamReader *xml, QXmlStreamReader::TokenType tokenType, int context, int relativeDepth);
    void ProcessTOCEntry(QString href, QString chapter);

private:
    void Init(QString fileName);
    void InsertSchedule(int talkId, int sequence, int rownumber, QDate date, QString theme, QString source1, QString source2, int time, int studyNumber);
    QString cleanText(QString text);
    void getTalkIdIndex(QDate date, int (&index)[15]);
    void saveTalkIndex(QDate dt, int (&talkid_index)[15]);

    sql_class *sql;
    bool debugMode;
    QString forcedLanguage;
    int year;
    int month;
    QString err;

    QString firstDate;
    QString lastDate;
    int validDates;
    QDate nullDate;
    QDate dtBeingImported;

    int epubliIdx;
    int epubSectionOffset;
    QStringList epubResults;
    int ignoreTextContext;
    int ignoreTextDepth;
};

#endif // importlmmworkbook_H
