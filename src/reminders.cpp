/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2016, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "reminders.h"
#include "ui_reminders.h"

reminders::reminders(QWidget *parent)
    : QDialog(parent), ui(new Ui::reminders)
{
    ui->setupUi(this);

    general::changeButtonColors(QList<QToolButton *>({ ui->buttonSend }), this->palette().window().color());
    updateList_ = false;

    // header checkbox to select or deselect all rows
    QHash<int, bool> checkedcols;
    checkedcols[0] = true;
    checkedcols[1] = false;
    CheckBoxHeader *myHeader = new CheckBoxHeader(Qt::Horizontal, checkedcols, ui->tableWidget);
    connect(myHeader, &CheckBoxHeader::checkBoxClicked, this, &reminders::headerCheckBoxClicked);
    ui->tableWidget->setHorizontalHeader(myHeader);
    ui->tableWidget->verticalHeader()->setVisible(true);
    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tableWidget->resizeColumnToContents(0);
    ui->tableWidget->resizeColumnToContents(1);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeToContents);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(3, QHeaderView::ResizeToContents);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(4, QHeaderView::ResizeToContents);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(5, QHeaderView::ResizeToContents);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(6, QHeaderView::Stretch);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(7, QHeaderView::ResizeToContents);

    // from and thru dates
    QDate fromDate = QDate::currentDate();
    QDate thruDate = fromDate.addMonths(1);
    thruDate = thruDate.addDays(7 - thruDate.dayOfWeek());

    ui->dateEditFrom->setDate(fromDate);
    ui->dateEditThru->setDate(thruDate);
    ui->dateEditFrom->setMinimumDate(QDate::currentDate());
    ui->dateEditThru->setMinimumDate(QDate::currentDate());

    sql = &Singleton<sql_class>::Instance();

    updateList_ = true;
    updateList();

    smtp = QSharedPointer<SmtpClient>(new SmtpClient());
    smtp->setHost("");
}

reminders::~reminders()
{
    qDeleteAll(r);
    r.clear();
    delete ui;
}

int reminders::remindersCount()
{
    return r.size();
}

void reminders::on_buttonSend_clicked()
{
    if (ui->tableWidget->rowCount() == 0)
        return;
    QSettings settings;

    QString fromName = sql->getSetting("email_from_name");
    QString fromAddress = sql->getSetting("email_from_address");
    SimpleCrypt crypt;
    crypt.setKey(constants::smtppasswd());
    QString userName = crypt.decryptToString(settings.value("email/smtpusername", "").toString());
    QString passWord = crypt.decryptToString(settings.value("email/smtppassword", "").toString());

    QProgressDialog *prgDialog = new QProgressDialog(tr("Email sending..."), "", 0, ui->tableWidget->rowCount(), this);
    prgDialog->setCancelButtonText(tr("Cancel"));
    prgDialog->setWindowModality(Qt::ApplicationModal);
    prgDialog->setValue(0);
    prgDialog->show();

    smtp->setHost(settings.value("email/smtphost").toString());
    smtp->setPort(settings.value("email/smtpport").toInt());
    int iConnType = settings.value("email/smtpconntype", 1).toInt();
    switch (iConnType) {
    case 0:
        smtp->setConnectionType(SmtpClient::TcpConnection);
        break;
    case 1:
        smtp->setConnectionType(SmtpClient::SslConnection);
        break;
    case 2:
        smtp->setConnectionType(SmtpClient::TlsConnection);
        break;
    }

    bool ok = smtp->connectToHost();
    if (ok)
        ok = smtp->login(userName, passWord, SmtpClient::AuthLogin);
    if (!ok) {
        smtp->quit();
        prgDialog->hide();
        QMessageBox::warning(this, "", tr("Error sending e-mail"));
        return;
    }

    for (int i = 0; i < ui->tableWidget->rowCount(); i++) {
        QTableWidgetItem *item = ui->tableWidget->item(i, 0);
        QTableWidgetItem *resendItem = ui->tableWidget->item(i, 1);
        // only selected rows to send
        if (item->checkState() == Qt::Checked || resendItem->checkState() == Qt::Checked) {
            MimeMessage message;
            MimeMultiPart mmp;
            // some reason attachments are visible only if 'multipart/mixed' is used, and 'multipart/alternative' when send to gmail address
            mmp.setMimeType(ui->tableWidget->item(i, 4)->text().endsWith("@gmail.com") ? MimeMultiPart::Alternative : MimeMultiPart::Mixed);
            message.setContent(&mmp);

            message.setHeaderEncoding(MimePart::Base64);
            EmailAddress sender(fromAddress, fromName);
            message.setSender(&sender);

            EmailAddress to(ui->tableWidget->item(i, 4)->text(), ui->tableWidget->item(i, 3)->text());
            message.addRecipient(&to);
            message.setSubject(ui->tableWidget->item(i, 5)->text());

            // Message
            MimeText text;
            text.setEncoding(MimePart::Base64);
            text.setText(ui->tableWidget->item(i, 6)->text());

            message.addPart(&text);

            // Create ics-file
            QFile file(QDir::tempPath() + "/" + r[i]->date.toString("yyyyMMdd") + ".ics");
            if (file.exists())
                file.remove();
            file.open(QIODevice::WriteOnly | QIODevice::Text);
            QTextStream out(&file);
            out.setCodec("UTF-8");
            out << r[i]->get_iCal();
            out.flush();
            file.close();

            // Add attachment to message
            MimeAttachment att(new QFile(file.fileName(), this));
            att.setContentType("text/calendar");
            message.addPart(&att);

            bool msgSent = smtp->sendMail(message);

            if (msgSent) {
                r[i]->save();
            } else {
                qDebug() << "message not sent";
            }

            // Remove temporary ics file
            if (file.exists())
                file.remove();
        }

        prgDialog->setValue(prgDialog->value() + 1);
    }
    smtp->quit();
    prgDialog->hide();
    //this->close();
    emit sent();
}

void reminders::headerCheckBoxClicked(bool val, int column)
{
    for (int i = 0; i < ui->tableWidget->rowCount(); i++) {
        QTableWidgetItem *chkBoxItem = ui->tableWidget->item(i, column);
        if (chkBoxItem->type() == 1)
            chkBoxItem->setCheckState(val ? Qt::Checked : Qt::Unchecked);
    }
}

void reminders::on_dateEditFrom_dateChanged(const QDate &date)
{
    ui->dateEditThru->setMinimumDate(date);
    if (updateList_)
        updateList();
}

void reminders::on_dateEditThru_dateChanged(const QDate &date)
{
    Q_UNUSED(date);
    if (updateList_)
        updateList();
}

void reminders::updateList()
{
    schoolreminder *sch = new schoolreminder(this);
    r = sch->getRemindersList(ui->dateEditFrom->date(), ui->dateEditThru->date());

    ui->tableWidget->setRowCount(r.size());
    ui->tableWidget->setSelectionMode(QAbstractItemView::NoSelection);

    if (r.size() > 0) {
        for (int i = 0; i < r.size(); i++) {
            schoolreminder *rmind = r[i];
            QTableWidgetItem *chkBoxItem = new QTableWidgetItem(1);
            chkBoxItem->setCheckState(rmind->counter == 0 ? Qt::Checked : Qt::Unchecked);
            ui->tableWidget->setItem(i, rmind->counter == 0 ? 0 : 1, chkBoxItem);
            QTableWidgetItem *textBox = new QTableWidgetItem(0);
            ui->tableWidget->setItem(i, rmind->counter == 0 ? 1 : 0, textBox);
            // date
            ui->tableWidget->setItem(i, 2, new QTableWidgetItem(rmind->date.toString(Qt::DefaultLocaleShortDate)));
            // name
            ui->tableWidget->setItem(i, 3, new QTableWidgetItem(rmind->toName));
            // email address
            ui->tableWidget->setItem(i, 4, new QTableWidgetItem(rmind->toEmail));
            // subject
            ui->tableWidget->setItem(i, 5, new QTableWidgetItem(rmind->reminderSubject));
            // message
            ui->tableWidget->setItem(i, 6, new QTableWidgetItem(rmind->reminderText));
        }
    }
}

void reminders::on_tableWidget_doubleClicked(const QModelIndex &index)
{
    if (index.column() == 5 || index.column() == 6) {
        bool ok;
        QString text = QInputDialog::getMultiLineText(this, "", ui->tableWidget->horizontalHeaderItem(index.column())->text(),
                                                      ui->tableWidget->item(index.row(), index.column())->text(), &ok);
        if (ok)
            ui->tableWidget->item(index.row(), index.column())->setText(text);
    }
}

void reminders::on_tableWidget_cellChanged(int row, int column)
{
    if (row < 0 || column > 1)
        return;
    QTableWidgetItem *chkBoxItem = ui->tableWidget->item(row, column);
    if (chkBoxItem->type() != 1)
        return;

    QTableWidgetItem *item(ui->tableWidget->item(row, 7));
    if (!item) {
        item = new QTableWidgetItem();
        ui->tableWidget->setItem(row, 7, item);
    }
    if (ui->tableWidget->item(row, column)->checkState() == Qt::Checked)
        item->setIcon(column == 0 ? QIcon(":/icons/email.svg") : QIcon(":/icons/email_resend.svg"));
    else
        item->setIcon(QIcon(""));
}
