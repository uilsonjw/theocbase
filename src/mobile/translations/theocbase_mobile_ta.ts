<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ta">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation>பெயர்</translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation>தேதி</translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>அடுத்த படிப்பை கொடுக்க வேண்டாம்</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>தலைப்பு</translation>
    </message>
    <message>
        <source>Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Student</source>
        <translation>மாணவர்</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>உதவியாளர்</translation>
    </message>
    <message>
        <source>Result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Timing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>School Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Setting தேர்ந்தெடுக்கவும்</translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>No exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit overseer&#x27;s visit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit assembly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Regional convention</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Memorial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Other exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>வாரமத்திபக் கூட்டம்</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>வார இறுதி கூட்டம்</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>வாசிப்பவர்</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>விவரங்கள்</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <comment>Page title</comment>
        <translation>குறிப்புகள்</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>பைபிளில் இருக்கும் புதையல்கள்</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>ஊழியத்தை நன்றாக செய்யுங்கள்</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>கிறிஸ்தவர்களாக வாழுங்கள்</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>சேர்மன்</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>ஆலோசகர்</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>பாட்டு %1 &amp; ஜெபம்</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>பாட்டு</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>நடத்துனர்</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>வாசிப்பவர்</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>ஆரம்ப குறிப்புகள்</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>ஜெபம்</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation>முடிவான குறிப்புகள்</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Student</source>
        <translation>மாணவர்</translation>
    </message>
    <message>
        <source>Result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Timing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>உதவியாளர்</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Study point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>விவரங்கள்</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>அட்டவணை</translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation>பயிற்சிப் புத்தகம்</translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print</source>
        <translation>அச்சிடு</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>வாரமத்திபக் கூட்டம்</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>வார இறுதி கூட்டம்</translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation>நியமிக்கப்பட்டதை மட்டும் அச்சிடுங்கள்</translation>
    </message>
    <message>
        <source>Text size</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>பாட்டு &amp; ஜெபம்</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>பாட்டு %1 &amp; ஜெபம்</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>பொது பேச்சு</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>காவற்கோபுர படிப்பு</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>பாட்டு %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>நடத்துனர்</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>வாசிப்பவர்</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Phone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>பொது பேச்சு</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>முதல் பெயர்</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>குடும்ப பெயர்</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sister</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Servant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Family</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Phone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>உதவியாளர்</translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Active</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>சேர்மன்</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>பைபிளில் இருக்கும் புதையல்கள்</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>பைபிள் வாசிப்பு</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>பைபிள் படிப்பு</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>கிறிஸ்தவர்களாக வாழுங்கள் பேச்சுக்கள்</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>சபை பைபிள் படிப்பு</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>சபை பைபிள் படிப்பு வாசிப்பவர்</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>ஜெபம்</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>வாரமத்திபக் கூட்டம்</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>வார இறுதி கூட்டம்</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation>காவற்கோபுர வாசிப்பவர்</translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation>புதையல்களைத் தோண்டி எடுங்கள்</translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation>தேர்வு பட்டியல்</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Logout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name</source>
        <translation>பெயர்</translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation>பாடல் தலைப்புகளைக் காட்டு</translation>
    </message>
    <message>
        <source>Printing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>சேர்மன்</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>பாட்டு</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation>வார இறுதி கூட்டத்தின் சேர்மன்</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>பாட்டு</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation>காவற்கோபுர பாட்டு</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>ஜெபம்</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>கட்டுரை</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>தலைப்பு</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>நடத்துனர்</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>வாசிப்பவர்</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>weeks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Not set</source>
        <translation type="unfinished"></translation>
    </message>
</context></TS>