/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "schoolstudy.h"

schoolStudy::schoolStudy(QObject *parent)
    : QObject(parent)
    , m_studyid(-1)
    , m_number(0)    
    , m_reading(false)    
    , m_discourse(false)
    , m_demonstration(false)
    , sql(&Singleton<sql_class>::Instance())
{       

}

QString schoolStudy::getName() const
{
    // return only name without number
    return QString(m_name).remove(QRegularExpression("(^[0-9]* )"));
}

void schoolStudy::setName(const QString &value)
{
    m_name = value;
}

bool schoolStudy::getDemonstration() const
{
    return m_demonstration;
}

void schoolStudy::setDemonstration(bool value)
{
    m_demonstration = value;
}

bool schoolStudy::getDiscourse() const
{
    return m_discourse;
}

void schoolStudy::setDiscourse(bool value)
{
    m_discourse = value;
}

int schoolStudy::getNumber() const
{
    return m_number;
}

void schoolStudy::setNumber(int value)
{
    m_number = value;
}

int schoolStudy::getId() const
{
    return m_studyid;
}

void schoolStudy::setId(int value)
{
    m_studyid = value;
}

bool schoolStudy::getReading() const
{
    return m_reading;
}

void schoolStudy::setReading(bool value)
{
    m_reading = value;
}


bool schoolStudy::update()
{
    sql_item s;
    s.insert("study_name",this->getName());
    s.insert("study_number",this->getNumber());
    s.insert("reading",this->getReading());
    s.insert("demonstration",this->getDemonstration());
    s.insert("discource", this->getDiscourse());

    return sql->updateSql("studies","id",QVariant(this->getId()).toString(),&s);
}

bool schoolStudy::remove()
{
    qDebug() << "remove" << this->getId();
    if (this->getId() == -1) return false;

//    return sql->removeSql("studies", "id='" +
//                         QVariant(this->getId()).toString() + "'");
    // deactive study in studies-table
    sql_item s;
    s.insert("active",0);
    return sql->updateSql("studies","id",QString::number(getId()),&s);


}

// schoolStudentStudy class functions

schoolStudentStudy::schoolStudentStudy(QObject *parent)
    : schoolStudy(parent)
    , m_exercise(false)
    , m_studentid(0)
{
    this->setId(-1);    
}

QDate schoolStudentStudy::getStartdate() const
{
    return m_startdate;
}

void schoolStudentStudy::setStartdate(const QDate &value)
{
    m_startdate = value;
}
QDate schoolStudentStudy::getEndDate() const
{
    return m_enddate;
}

void schoolStudentStudy::setEndDate(const QDate &value)
{
    m_enddate = value;
}
bool schoolStudentStudy::getExercise() const
{
    return m_exercise;
}

void schoolStudentStudy::setExercise(bool value)
{
    m_exercise = value;
}

bool schoolStudentStudy::update()
{
    sql_item s;
    s.insert("start_date",getStartdate());
    s.insert("study_number",this->getNumber());
    s.insert("end_date",this->getEndDate());
    s.insert("exercises",this->getExercise());
    s.insert("student_id",this->getStudentId());

    // start date can't be newer than end date
    QDate d1 = getStartdate();
    QDate d2 = getEndDate();
    if (getEndDate().isValid() && d1 > d2){
        setStartdate(getEndDate());
        s.insert("start_date",this->getStartdate());
    }

    if (getId() < 1){
        // create a new row to database
        int newid = sql->insertSql("student_studies",&s,"id");
        if (newid < 1) return false;
        setId(newid);
        return true;
    }else{
        // update existing row
        return sql->updateSql("student_studies","id",QVariant(this->getId()).toString(),&s);
    }
}

bool schoolStudentStudy::remove()
{
    qDebug() << "remove" << this->getId();
    if (this->getId() == -1) return false;

//    return sql->removeSql("student_studies", "id='" +
//                         QVariant(this->getId()).toString() + "'");
    // deactive study in student_studies-table
    sql_item s;
    s.insert("active",0);
    return sql->updateSql("student_studies","id",QString::number(getId()),&s);
}
int schoolStudentStudy::getStudentId() const
{
    return m_studentid;
}

void schoolStudentStudy::setStudentId(int id)
{
    m_studentid = id;
}



