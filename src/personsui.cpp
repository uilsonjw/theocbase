/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "personsui.h"
#include "ui_personsui.h"
#include "lmm_meeting.h"
#include "historytable.h"
#include <ciso646>

// persons class creator
personsui::personsui(QWidget *parent)
    : QDialog(parent), m_ui(new Ui::personsui)
{
    sql = &Singleton<sql_class>::Instance();
    ac = &Singleton<AccessControl>::Instance();
    editmode = false;
    m_ui->setupUi(this);

    general::changeButtonColors(QList<QToolButton *>({ m_ui->buttonAddPerson, m_ui->buttonRemovePerson }), this->palette().window().color());
    m_ui->tabPersonalInfo_2->setCurrentIndex(0);
    updatePersonList();
    updateFamiliesList();

    m_ui->framePersonalInfo->setEnabled(false);

    m_ui->dateUnavailableStart->calendarWidget()->setFirstDayOfWeek(Qt::Monday);
    m_ui->dateUnavailableStop->calendarWidget()->setFirstDayOfWeek(Qt::Monday);
    m_ui->dateUnavailableStart->setDate(QDate::currentDate());
    m_ui->radBrother_2->setChecked(true);

    // Get default Watchtower Study Conductor
    defaultWtConductor = sql->getSetting("wtconductor", "-1").toInt();
    m_ui->buttonWtConductorDefault->setVisible(false);

    // Get own congregation
    myCongregationId = c.getMyCongregation().id;

    m_ui->tabGeneral_2->setEnabled(false);
    m_ui->chkLeadsMinistry_2->setVisible(false);

    m_studiesGridScrollPosition = 0;

    applyAuthorizationRules();
}

personsui::~personsui()
{
    qDeleteAll(m_families);
    delete m_ui;
}

std::unique_ptr<person> personsui::currentPublisher() const
{
    auto item = m_ui->lstPublishers->currentItem();
    if (not item)
        return nullptr;

    return std::unique_ptr<person> { cpersons::getPerson(item->data(Qt::UserRole).toInt()) };
}

void personsui::updatePersonList()
{
    // Get all publishers in own congregation
    cpersons *cp = new cpersons();
    auto users = cp->getAllPersons(0);

    // Add publishers to list
    for (person *p : users) {
        auto item = new QListWidgetItem(m_ui->lstPublishers);
        item->setText(p->fullname("LastName, FirstName"));
        item->setData(Qt::UserRole, p->id());
    }

    // Show quantity of publishers
    m_ui->lblCount->setText(QVariant(users.size()).toString());

    qDeleteAll(users);
}

void personsui::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        m_ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void personsui::applyAuthorizationRules()
{
    // basic publisher infos and functions
    if (!ac->user()->hasPermission(Permission::Rule::CanEditPublishers)) {
        m_ui->buttonAddPerson->setEnabled(false);
        m_ui->buttonRemovePerson->setEnabled(false);
        m_ui->information1Widget->setEnabled(false);
        m_ui->information2Widget->setEnabled(false);
        m_ui->chkActive->setEnabled(false);
    }

    // privileges and student assignments
    if (!ac->user()->hasPermission(Permission::Rule::CanViewPrivileges) && !ac->user()->hasPermission(Permission::Rule::CanViewStudentData)) {
        m_ui->labelDetails->setHidden(true);
        m_ui->tabPersonalInfo_2->setHidden(true);
    }

    // MW student assignments
    if (!ac->user()->hasPermission(Permission::Rule::CanViewStudentData)) {
        m_ui->chkBibleReading->setHidden(true);
        m_ui->frameMWSecondSectionStudenTalks->setHidden(true);
    } else if (!ac->user()->hasPermission(Permission::Rule::CanEditStudentData)) {
        m_ui->chkBibleReading->setEnabled(false);
        m_ui->frameMWSecondSectionStudenTalks->setEnabled(false);
    }

    // MW and WE privileges
    if (!ac->user()->hasPermission(Permission::Rule::CanViewPrivileges)) {
        m_ui->chkNominated_2->setHidden(true);
        m_ui->chkLMMChairman->setHidden(true);
        m_ui->chkTreasures->setHidden(true);
        m_ui->chkDigging->setHidden(true);
        m_ui->chkOtherVideoPart->setHidden(true);
        m_ui->frameMWThirdSection->setHidden(true);
        m_ui->frameWeekendMeeting->setHidden(true);
        m_ui->chkLeadsMinistry_2->setHidden(true);
        m_ui->chkPrayer->setHidden(true);
    } else if (!ac->user()->hasPermission(Permission::Rule::CanEditPrivileges)) {
        m_ui->chkNominated_2->setEnabled(false);
        m_ui->chkLMMChairman->setEnabled(false);
        m_ui->chkTreasures->setEnabled(false);
        m_ui->chkOtherVideoPart->setEnabled(false);
        m_ui->chkDigging->setEnabled(false);
        m_ui->frameMWThirdSection->setEnabled(false);
        m_ui->frameWeekendMeeting->setEnabled(false);
        m_ui->chkLeadsMinistry_2->setEnabled(false);
        m_ui->chkPrayer->setEnabled(false);
    }

    // hospitality
    if (!ac->user()->hasPermission(Permission::Rule::CanScheduleHospitality)) {
        m_ui->chkHospitality->setHidden(true);
    }

    // availabilities
    if (!ac->user()->hasPermission(Permission::Rule::CanViewAvailabilities)) {
        m_ui->tabPersonalInfo_2->removeTab(3);
    } else if (!ac->user()->hasPermission(Permission::Rule::CanEditAvailabilities)) {
        m_ui->widgetEditUnavailable->setHidden(true);
    }

    // talk history
    if (!ac->user()->hasPermission(Permission::Rule::CanViewMidweekMeetingTalkHistory)) {
        m_ui->tabPersonalInfo_2->removeTab(1);
    }
}

// this is always called when a publisher item is clicked.
// Note that if the click causes the current item to change
// (invoking "on_lstPublishers_currentItemChanged"), this handler
// is always called _after_ "on_lstPublishers_currentItemChanged"
void personsui::on_lstPublishers_itemClicked(QListWidgetItem *item)
{
    //qDebug() << "on_lstPublishers_clicked:";
    //qDebug() << "   current:" << (item ? item->data(Qt::UserRole).toInt() : -1) << "," <<
    //            (item ? item->text() : "<empty>");

    editmode = false;
    saveChanges(item);
    editmode = true;
}

void personsui::on_lstPublishers_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
    //qDebug() << "lstPublisherItemChanged:";
    //qDebug() << "   current:" << (current ? current->data(Qt::UserRole).toInt() : -1) << "," <<
    //            (current ? current->text() : "<empty>");
    //qDebug() << "   previous:" << (previous ? previous->data(Qt::UserRole).toInt() : -1) << "," <<
    //            (previous ? previous->text() : "<empty>");

    editmode = false;

    // Try to save the changes before loading a selected publisher
    if (previous && !saveChanges(previous)) {
        qDebug("personsui: current publisher changed CANCELED");
        // Cannot save changes
        m_ui->lstPublishers->setCurrentItem(previous);
        editmode = true;
        return;
    }

    updateFamiliesList();

    if (current)
        current->setSelected(true);
    updateDetailsPaneWith(current);
}

void personsui::ClearDetailsPane()
{
    m_ui->framePersonalInfo->setEnabled(false);

    // Personal info...
    m_ui->tabGeneral_2->setEnabled(true);
    m_ui->txtFirstName->setText(QString());
    m_ui->txtLastName->setText(QString());
    m_ui->txtPhone->setText("");
    m_ui->txtMobile->setText("");
    m_ui->txtEmail->setText("");
    m_ui->radBrother_2->setChecked(true);
    m_ui->chkNominated_2->setChecked(false); // "servant"
    m_ui->chkPrayer->setChecked(false);
    m_ui->chkFamilyHead->setChecked(false);
    m_ui->chkActive->setChecked(true);

    // Details...
    m_ui->chkLMMChairman->setChecked(false);

    m_ui->chkTreasures->setChecked(false);
    m_ui->chkDigging->setChecked(false);
    m_ui->chkBibleReading->setChecked(false);

    m_ui->chkInitialCall->setChecked(false);
    m_ui->chkReturnVisit->setChecked(false);
    m_ui->chkBibleStudy->setChecked(false);
    m_ui->chkAssistant->setChecked(false);
    m_ui->chkTalk->setChecked(false);
    m_ui->chkOtherVideoPart->setChecked(false);
    m_ui->radioAllClasses->setChecked(true);

    m_ui->chkLivingTalks->setChecked(false);
    m_ui->chkCBS->setChecked(false);
    m_ui->chkBibleStudyReader->setChecked(false);

    // Weekend meeting...
    m_ui->chkPuheenjohtaja_2->setChecked(false); // weekend chairman
    m_ui->checkBoxPublicTalk->setChecked(false);

    m_ui->chkWTConductor->setChecked(false);
    m_ui->buttonWtConductorDefault->setChecked(false);
    m_ui->chkWTReader_2->setChecked(false);

    m_ui->chkLeadsMinistry_2->setChecked(false);

    m_ui->tableWidgetSchoolTask->setRowCount(0);
    m_ui->tableWidget->setRowCount(0);
}

//
// update details pane
//
void personsui::updateDetailsPaneWith(QListWidgetItem const *item)
{
    std::unique_ptr<person> p { cpersons::getPerson(item ? item->data(Qt::UserRole).toInt() : 0) };
    if (!p) {
        ClearDetailsPane();
        editmode = true;
        return;
    }

    m_ui->framePersonalInfo->setEnabled(true);

    //qDebug() << "name" + p.name;
    m_ui->txtLastName->setText(p->lastname());
    m_ui->txtFirstName->setText(p->firstname());
    //qDebug() << "phone: " + p.phone;
    m_ui->txtPhone->setText(p->phone());
    m_ui->txtMobile->setText(p->mobile());
    m_ui->txtEmail->setText(p->email());

    m_ui->radBrother_2->setChecked(p->gender() == person::Male);
    m_ui->radSister_2->setChecked(p->gender() == person::Female);

    // is servant ?
    m_ui->chkNominated_2->setChecked(p->servant());
    on_chkNominated_2_toggled(m_ui->chkNominated_2->isChecked());

    m_ui->chkActive->setChecked(!(person::IsBreak & p->usefor()));
    if (p->usefor() & person::SchoolMain) {
        m_ui->radioMainClass->setChecked(true);
    } else if (p->usefor() & person::SchoolAux) {
        m_ui->radioAuxClasses->setChecked(true);
    } else {
        m_ui->radioAllClasses->setChecked(true);
    }

    m_ui->chkLeadsMinistry_2->setChecked(person::FieldMinistry & p->usefor());

    m_ui->chkPuheenjohtaja_2->setChecked(person::Chairman & p->usefor());
    m_ui->chkWTConductor->setChecked(person::WtCondoctor & p->usefor());

    m_ui->buttonWtConductorDefault->setChecked(p->id() == defaultWtConductor);
    m_ui->chkWTReader_2->setChecked(person::WtReader & p->usefor());
    m_ui->chkAssistant->setChecked(person::Assistant & p->usefor());
    m_ui->checkBoxPublicTalk->setChecked(person::PublicTalk & p->usefor());
    m_ui->chkCBS->setChecked(person::CBSConductor & p->usefor());
    m_ui->chkBibleStudyReader->setChecked(person::CBSReader & p->usefor());
    m_ui->chkPrayer->setChecked(person::Prayer & p->usefor());

    // hospitality
    m_ui->chkHospitality->setChecked(person::Hospitality & p->usefor());

    // LMM
    m_ui->chkLMMChairman->setChecked(person::LMM_Chairman & p->usefor());
    m_ui->chkTreasures->setChecked(person::LMM_Treasures & p->usefor());
    m_ui->chkDigging->setChecked(person::LMM_Digging & p->usefor());
    m_ui->chkBibleReading->setChecked(person::LMM_BibleReading & p->usefor());
    m_ui->chkTalk->setChecked(person::LMM_ApplyTalks & p->usefor());
    m_ui->chkInitialCall->setChecked(person::LMM_InitialCall & p->usefor());
    m_ui->chkReturnVisit->setChecked(person::LMM_ReturnVisit & p->usefor());
    m_ui->chkBibleStudy->setChecked(person::LMM_BibleStudy & p->usefor());
    m_ui->chkLivingTalks->setChecked(person::LMM_LivingTalks & p->usefor());
    m_ui->chkOtherVideoPart->setChecked(person::LMM_OtherVideoPart & p->usefor());

    m_ui->tabGeneral_2->setEnabled(true);

    // studies tab
    m_ui->tabPersonalInfo_2->setTabEnabled(2, p->usefor() & (person::No1 | person::No2 | person::No3 | person::Highlights | person::LMM_BibleReading | person::LMM_InitialCall | person::LMM_ReturnVisit | person::LMM_BibleStudy));

    // family
    QSharedPointer<family> personsFamily(family::getPersonFamily(p->id()));
    // is family head ?
    m_ui->chkFamilyHead->setChecked(personsFamily ? personsFamily->getHeadId() == p->id() : false);
    // member linked to
    m_ui->comboFamilies->setCurrentText(personsFamily ? (personsFamily->getHeadId() < 1 ? "" : personsFamily->getName()) : "");

    // show study points
    m_studiesGridScrollPosition = 0;
    getStudies();

    // get all school assignments
    getSchoolHistory();

    // add unavailabilities
    m_ui->tableUnavailabilities->setRowCount(0);
    QList<QPair<QDate, QDate>> ulist = p->getUnavailabilities();
    if (!ulist.empty()) {
        m_ui->tableUnavailabilities->setRowCount(ulist.size());
        for (int i = 0; i < (ulist.size()); i++) {
            QPair<QDate, QDate> datepair = ulist[i];
            m_ui->tableUnavailabilities->setItem(i, 0, new QTableWidgetItem(datepair.first.toString(Qt::ISODate)));
            m_ui->tableUnavailabilities->setItem(i, 1, new QTableWidgetItem(datepair.second.toString(Qt::ISODate)));
        }
    }

    editmode = true;
}

bool personsui::saveChanges(QListWidgetItem *item)
{
    if (not item) {
        if (m_ui->txtFirstName->text() == "" && m_ui->txtLastName->text() == "") {
            //qDebug() << "saveChanges(nullptr) without name -> true";
            return true;
        }
        //qDebug() << "saveChanges(nullptr) with name -> false";
        return false;
    }

    auto id = item->data(Qt::UserRole).toInt();
    std::unique_ptr<person> p { cpersons::getPerson(id) };
    if (not p) {
        //qDebug() << "saveChanges(item with ID:" << id << ") but CANNOT retreive in DB -> false";
        return false;
    }

    // name check
    // check duplicates
    person *checkperson = cpersons::getPerson(m_ui->txtFirstName->text() + " " + m_ui->txtLastName->text());

    if (checkperson && checkperson->id() != p->id()) {
        if (QMessageBox::question(this, "",
                                  tr("A person with the same name already exists: '%1'. Do you want to change the name?").arg(m_ui->txtFirstName->text() + " " + m_ui->txtLastName->text()),
                                  QMessageBox::No, QMessageBox::Yes)
            == QMessageBox::Yes) {
            return false;
        }
    }

    // public talk check
    // check for scheduled talks
    bool removeSpeaker = false;
    int speakerId = p->id();
    if (!m_ui->checkBoxPublicTalk->isChecked() && (person::PublicTalk & p->usefor())) {
        // check for scheduled talks
        QDate startDate = QDateTime::currentDateTime().date().addDays(-7);

        sql_items incomingTalks;
        incomingTalks = sql->selectSql("SELECT * from publicmeeting WHERE date > '" + startDate.toString(Qt::ISODate) + "' AND speaker_id = " + QVariant(speakerId).toString() + " AND active");

        sql_items outgoingTalks;
        outgoingTalks = sql->selectSql("SELECT * from outgoing WHERE date > '" + startDate.toString(Qt::ISODate) + "' AND speaker_id = " + QVariant(speakerId).toString() + " AND active");

        if (!incomingTalks.empty() || !outgoingTalks.empty()) {
            if (QMessageBox::question(this, "",
                                      tr("%1 is scheduled for public talks! These talks will\n"
                                         "be moved to the To Do List if you remove him as speaker.\nRemove him as speaker?")
                                              .arg(p->fullname()),
                                      QMessageBox::No, QMessageBox::Yes)
                == QMessageBox::Yes) {
                removeSpeaker = true;
            } else
                return false;
        }
    }

    //p->setDirtyFlag(false);

    p->setLastname(m_ui->txtLastName->text());
    p->setFirstname(m_ui->txtFirstName->text());
    p->setPhone(m_ui->txtPhone->text());
    p->setMobile(m_ui->txtMobile->text());
    p->setEmail(m_ui->txtEmail->text());
    p->setServant(m_ui->chkNominated_2->isChecked());
    p->setGender(m_ui->radBrother_2->isChecked() ? person::Male : person::Female);

    int usefor = 0;

    if (!m_ui->chkActive->isChecked())
        usefor += person::IsBreak;
    if (m_ui->chkLeadsMinistry_2->isChecked())
        usefor += person::FieldMinistry;
    if (m_ui->chkPuheenjohtaja_2->isChecked())
        usefor += person::Chairman;
    if (m_ui->chkWTReader_2->isChecked())
        usefor += person::WtReader;
    if (m_ui->chkWTConductor->isChecked())
        usefor += person::WtCondoctor;
    if (m_ui->buttonWtConductorDefault->isChecked()) {
        sql->saveSetting("wtconductor", QVariant(p->id()).toString());
        defaultWtConductor = p->id();
    } else {
        if (defaultWtConductor == p->id())
            sql->saveSetting("wtconductor", "-1");
    }

    if (m_ui->chkAssistant->isChecked())
        usefor += person::Assistant;
    if (m_ui->checkBoxPublicTalk->isChecked())
        usefor += person::PublicTalk;
    if (m_ui->chkCBS->isChecked())
        usefor += person::CBSConductor;
    if (m_ui->chkBibleStudyReader->isChecked())
        usefor += person::CBSReader;
    if (m_ui->chkPrayer->isChecked())
        usefor += person::Prayer;

    // hospitality
    if (m_ui->chkHospitality->isChecked())
        usefor += person::Hospitality;

    // LMM
    if (m_ui->chkLMMChairman->isChecked())
        usefor += person::LMM_Chairman;
    if (m_ui->chkTreasures->isChecked())
        usefor += person::LMM_Treasures;
    if (m_ui->chkDigging->isChecked())
        usefor += person::LMM_Digging;
    if (m_ui->chkBibleReading->isChecked())
        usefor += person::LMM_BibleReading;
    if (m_ui->chkTalk->isChecked())
        usefor += person::LMM_ApplyTalks;
    if (m_ui->chkInitialCall->isChecked())
        usefor += person::LMM_InitialCall;
    if (m_ui->chkReturnVisit->isChecked())
        usefor += person::LMM_ReturnVisit;
    if (m_ui->chkBibleStudy->isChecked())
        usefor += person::LMM_BibleStudy;
    if (m_ui->chkLivingTalks->isChecked())
        usefor += person::LMM_LivingTalks;
    if (m_ui->chkOtherVideoPart->isChecked())
        usefor += person::LMM_OtherVideoPart;

    // use in these classes. all is default
    if (m_ui->radioMainClass->isChecked())
        usefor += person::SchoolMain;
    if (m_ui->radioAuxClasses->isChecked())
        usefor += person::SchoolAux;

    qDebug() << "save usefor:" << usefor;
    p->setUsefor(usefor);

    // congregation
    p->setCongregationid(myCongregationId);

    // family
    SaveFamilySetting(p->id());

    bool updated = p->update();

    if (updated) {
        item->setText(p->fullname("LastName, FirstName"));
        if (removeSpeaker) {
            // move scheduled talks to To Do List
            emit updateScheduledTalks(speakerId, p->congregationid(), p->congregationid(), true);
        }
    }

    return updated;
}

void personsui::SaveFamilySetting(int personId)
{
    QSharedPointer<family> personsFamily(family::getPersonFamily(personId));

    if (m_ui->chkFamilyHead->isChecked()) {
        if (!personsFamily || personsFamily->getHeadId() != personId) {
            family::getOrAddFamily(personId);
        }
    } else {
        if (personsFamily && personsFamily->getHeadId() == personId) {
            // remove family
            personsFamily->removeFamily();
        }

        // family member
        if (m_ui->comboFamilies->currentIndex() > 0) {
            // add member
            if (!personsFamily || (personsFamily->getHeadId() != m_families.at(m_ui->comboFamilies->currentIndex() - 1)->getHeadId())) {
                m_families.at(m_ui->comboFamilies->currentIndex() - 1)->addMember(personId);
            }
        } else {
            // remove member
            if (personsFamily) {
                personsFamily->removeMember(personId);
            }
        }
    }
}

//+ button
void personsui::on_buttonAddPerson_clicked()
{
    editmode = false;
    if (!saveChanges(m_ui->lstPublishers->currentItem())) {
        editmode = true;
        return;
    }

    std::unique_ptr<person> newperson { new person() };
    newperson->setGender(person::Male);
    newperson->setServant(false);
    newperson->setLastname(tr("Last name"));
    newperson->setFirstname(tr("First name"));
    newperson->setCongregationid(myCongregationId);

    auto cp = std::make_shared<cpersons>();
    newperson->setId(cp->addPerson(newperson.get()));

    auto item = new QListWidgetItem(m_ui->lstPublishers);
    item->setText(newperson->fullname("LastName, FirstName"));
    item->setData(Qt::UserRole, newperson->id());
    m_ui->lstPublishers->setCurrentItem(item);

    // Show quantity of publishers
    m_ui->lblCount->setText(QVariant(m_ui->lstPublishers->count()).toString());

    //qDebug() << "created person id:" << newperson->id();
}

//- button
void personsui::on_buttonRemovePerson_clicked()
{
    auto item = m_ui->lstPublishers->currentItem();
    if (not item)
        return;

    std::unique_ptr<person> p { cpersons::getPerson(item->data(Qt::UserRole).toInt()) };
    qDebug() << "Poistetaan";

    QString msg = tr("Remove student") + " " + p->fullname() + "?";
    bool talksScheduled = false;
    int speakerId = p->id();
    if (person::PublicTalk & p->usefor()) {
        // check for scheduled talks
        QDate startDate = QDateTime::currentDateTime().date().addDays(-7);

        sql_items incomingTalks;
        incomingTalks = sql->selectSql("SELECT * from publicmeeting WHERE date > '" + startDate.toString(Qt::ISODate) + "' AND speaker_id = " + QVariant(speakerId).toString() + " AND active");

        sql_items outgoingTalks;
        outgoingTalks = sql->selectSql("SELECT * from outgoing WHERE date > '" + startDate.toString(Qt::ISODate) + "' AND speaker_id = " + QVariant(speakerId).toString() + " AND active");

        if (!incomingTalks.empty() || !outgoingTalks.empty()) {
            msg = tr("%1 is scheduled for public talks! These talks will\n"
                     "be moved to the To Do List if you remove the student.")
                            .arg(p->fullname())
                    + "\n" + msg;
            talksScheduled = true;
        }
    }

    //QMessageBox:: ( Icon icon, const QString & title, const QString & text, StandardButtons buttons = NoButton, QWidget * parent = 0, Qt::WindowFlags f = Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint )
    if (QMessageBox::question(this, tr("Remove student?"),
                              msg,
                              QMessageBox::No, QMessageBox::Yes)
        == QMessageBox::Yes) {
        m_ui->lstPublishers->setCurrentItem(nullptr);

        cpersons::removePerson(p->id());

        delete item;

        // Show quantity of publishers
        m_ui->lblCount->setText(QVariant(m_ui->lstPublishers->count()).toString());

        // move scheduled talks to To Do List
        if (talksScheduled)
            emit updateScheduledTalks(speakerId, p->congregationid(), p->congregationid(), true);
    }
}

void personsui::on_radBrother_2_toggled(bool checked)
{
    //  m_ui->chkAssistant->setEnabled(!checked);
    m_ui->chkLeadsMinistry_2->setEnabled(checked);
    m_ui->chkPuheenjohtaja_2->setEnabled(checked);
    m_ui->chkWTReader_2->setEnabled(checked);
    m_ui->chkWTConductor->setEnabled(checked);
    m_ui->chkNominated_2->setEnabled(checked);
    m_ui->checkBoxPublicTalk->setEnabled(checked);
    m_ui->chkCBS->setEnabled(checked);
    m_ui->chkBibleStudyReader->setEnabled(checked);
    m_ui->chkPrayer->setEnabled(checked);
    m_ui->chkLMMChairman->setEnabled(checked);
    m_ui->chkOtherVideoPart->setEnabled(checked);
    m_ui->chkTreasures->setEnabled(checked);
    m_ui->chkDigging->setEnabled(checked);
    m_ui->chkBibleReading->setEnabled(checked);
    m_ui->chkTalk->setEnabled(checked);
    m_ui->chkLivingTalks->setEnabled(checked);
    applyAuthorizationRules();
}

void personsui::on_chkNominated_2_toggled(bool checked)
{
    m_ui->checkBoxPublicTalk->setEnabled(checked);
    m_ui->chkWTConductor->setEnabled(checked);
    m_ui->chkCBS->setEnabled(checked);
    //m_ui->chkPrayer->setEnabled(checked);
    m_ui->chkLMMChairman->setEnabled(checked);
    m_ui->chkTreasures->setEnabled(checked);
    m_ui->chkDigging->setEnabled(checked);
    m_ui->chkOtherVideoPart->setEnabled(checked);
    m_ui->chkLivingTalks->setEnabled(checked);
    applyAuthorizationRules();
}

void personsui::on_buttonAddUnavailable_clicked()
{
    // add new row
    auto item = m_ui->lstPublishers->currentItem();
    if (not item)
        return;

    std::unique_ptr<person> p { cpersons::getPerson(item->data(Qt::UserRole).toInt()) };
    if (not p)
        return;

    p->setUnavailability(m_ui->dateUnavailableStart->date(), m_ui->dateUnavailableStop->date());
    m_ui->tableUnavailabilities->setRowCount(m_ui->tableUnavailabilities->rowCount() + 1);
    m_ui->tableUnavailabilities->setItem(m_ui->tableUnavailabilities->rowCount() - 1, 0, new QTableWidgetItem(m_ui->dateUnavailableStart->date().toString(Qt::ISODate)));
    m_ui->tableUnavailabilities->setItem(m_ui->tableUnavailabilities->rowCount() - 1, 1, new QTableWidgetItem(m_ui->dateUnavailableStop->date().toString(Qt::ISODate)));
}

void personsui::on_dateUnavailableStart_dateChanged(QDate date)
{
    m_ui->dateUnavailableStop->setDate(date);
}

void personsui::on_buttonRemoveUnavailable_clicked()
{
    if (not m_ui->lstPublishers->currentItem())
        return;

    if (m_ui->tableUnavailabilities->selectedItems().count() < 1)
        return;

    QTableWidgetItem *selecteditem = m_ui->tableUnavailabilities->selectedItems().first();
    qDebug() << QString::number(selecteditem->row());

    if (currentPublisher()->removeUnavailability(QDate::fromString(m_ui->tableUnavailabilities->item(selecteditem->row(), 0)->text(), Qt::ISODate),
                                                 QDate::fromString(m_ui->tableUnavailabilities->item(selecteditem->row(), 1)->text(), Qt::ISODate)))
        m_ui->tableUnavailabilities->removeRow(selecteditem->row());
}

// column name reminder: (0) date (1) num (2) source (3) note (4) time (5) assistant
void personsui::getSchoolHistory()
{
    auto cp = std::make_shared<cpersons>();
    m_ui->tableWidgetSchoolTask->setRowCount(0);
    m_ui->tableWidgetSchoolTask->setColumnWidth(1, 30);
    m_ui->tableWidgetSchoolTask->setColumnWidth(4, 60);

    auto assigneeID = m_ui->lstPublishers->currentItem()->data(Qt::UserRole).toInt();
    auto assigneeIDstr = QVariant(assigneeID).toString();

    auto history = ::getSchoolHistory(assigneeID,
                                      QTableWidgetItem().foreground().color(),
                                      QTableWidgetItem().background().color());

    m_ui->tableWidgetSchoolTask->setRowCount(history.rowCount());
    for (int iLine = 0; iLine < history.rowCount(); ++iLine) //auto const& assignment : history)
    {
        for (int iCol = 0; iCol <= 5; ++iCol) {
            auto const &formatedText = history.item(iLine, iCol);
            auto item = new QTableWidgetItem(formatedText.text());
            m_ui->tableWidgetSchoolTask->setItem(iLine, iCol, item);

            // coloring
            if (formatedText.customForeground()) {
                QBrush fgBrush = item->foreground();
                QColor fg = formatedText.foreground();
                fgBrush.setColor(fg);
                item->setForeground(fgBrush);
            }

            if (formatedText.customBackground()) {
                item->setBackground(formatedText.background());
            }

            if (formatedText.italic() || formatedText.bold() || formatedText.strikeOut()) {
                QFont f(item->font());
                f.setItalic(formatedText.italic());
                f.setBold(formatedText.bold());
                f.setStrikeOut(formatedText.strikeOut());

                item->setFont(f);
            }
            // icon
            if (!formatedText.iconName().isEmpty())
                item->setIcon(QIcon(formatedText.iconName()));
        }
    }

    m_ui->tableWidgetSchoolTask->sortItems(0, Qt::DescendingOrder);
    m_ui->tableWidgetSchoolTask->resizeColumnToContents(0); // date
    m_ui->tableWidgetSchoolTask->resizeColumnToContents(1); // what
    m_ui->tableWidgetSchoolTask->resizeColumnToContents(4); // time
    m_ui->tableWidgetSchoolTask->resizeColumnToContents(5); // assistant
}

void personsui::getStudies()
{
    bool isReadOnly = !ac->user()->hasPermission(Permission::Rule::CanEditStudentData);

    m_ui->tableWidget->setColumnHidden(0, true);
    m_ui->tableWidget->setColumnHidden(1, true);
    m_ui->tableWidget->setColumnWidth(2, 20);
    m_ui->tableWidget->setColumnWidth(3, 20);
    m_ui->tableWidget->setColumnWidth(4, 20);
    m_ui->tableWidget->horizontalHeader()->setSectionResizeMode(5, QHeaderView::Stretch);
    m_ui->tableWidget->setColumnWidth(7, 30);
    m_ui->tableWidget->setColumnWidth(9, 30);
    m_ui->tableWidget->setColumnHidden(9, isReadOnly);
    m_ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
    school s;
    auto p = currentPublisher();
    if (p->gender() == person::Female) {
        m_ui->lblActiveStudy1->setText("-");
    } else {
        m_ui->lblActiveStudy1->setText(QVariant(s.getActiveStudyPoint(p->id(), school_item::Reading, false)->getNumber()).toString());
    }
    m_ui->lblActiveStudy2->setText(QVariant(s.getActiveStudyPoint(p->id(), school_item::Demonstration, false)->getNumber()).toString());
    m_ui->lblActiveStudy3->setText(QVariant(s.getActiveStudyPoint(p->id(), school_item::Discource, false)->getNumber()).toString());

    QList<schoolStudentStudy *> studies = s.getStudentStudies(p->id());

    QLocale local;
    m_ui->tableWidget->setRowCount(0);
    if (!studies.empty()) {
        m_ui->tableWidget->setRowCount(studies.size());
        int row = 0;
        int studystartrow = 0;
        for (int i = 0; i < (studies.size()); i++) {
            schoolStudentStudy *st = studies[i];

            m_ui->tableWidget->setItem(row, 0, new QTableWidgetItem(QVariant(st->getId()).toString()));
            m_ui->tableWidget->setItem(row, 1, new QTableWidgetItem(QVariant(st->getNumber()).toString()));

            if (i > 0 && (m_ui->tableWidget->item(row, 1)->text() != m_ui->tableWidget->item(row - 1, 1)->text())) {
                studystartrow = row;
            }

            m_ui->tableWidget->setItem(row, 2, new QTableWidgetItem());
            if (st->getReading())
                m_ui->tableWidget->item(row, 2)->setBackground(QColor(255, 200, 0, 255));

            m_ui->tableWidget->setItem(row, 3, new QTableWidgetItem());
            if (st->getDemonstration())
                m_ui->tableWidget->item(row, 3)->setBackground(QColor(255, 240, 160, 255));

            m_ui->tableWidget->setItem(row, 4, new QTableWidgetItem());
            if (st->getDiscourse())
                m_ui->tableWidget->item(row, 4)->setBackground(QColor(255, 106, 0, 255));

            auto item5 = new QTableWidgetItem(QString::number(st->getNumber()) + " " + st->getName());
            m_ui->tableWidget->setItem(row, 5, item5);
            item5->setToolTip(item5->text());
            item5->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

            QTableWidgetItem *chkBoxItem = new QTableWidgetItem;
            chkBoxItem->setCheckState(Qt::Unchecked);
            if (p->gender() == person::Female) {
                if (!st->getDemonstration())
                    chkBoxItem->setFlags(Qt::NoItemFlags);
            }
            if (st->getExercise())
                chkBoxItem->setCheckState(Qt::Checked);
            if (isReadOnly)
                chkBoxItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
            m_ui->tableWidget->setItem(row, 7, chkBoxItem);

            if (st->getStartdate().isNull()) {
                m_ui->tableWidget->setItem(row, 6, new QTableWidgetItem());
            } else {
                QTableWidgetItem *startitem = new QTableWidgetItem();
                startitem->setText(st->getStartdate().toString(local.dateFormat(QLocale::ShortFormat)));
                // save QDate object -> no need do date conversion from string
                startitem->setData(Qt::UserRole, st->getStartdate());
                m_ui->tableWidget->setItem(row, 6, startitem);

                addStudyRemoveButton(m_ui->tableWidget, row, 9);
            }

            if (st->getEndDate().isNull()) {
                QTableWidgetItem *enditem = new QTableWidgetItem();
                m_ui->tableWidget->setItem(row, 8, enditem);
            } else {
                QTableWidgetItem *enditem = new QTableWidgetItem();
                enditem->setText(st->getEndDate().toString(local.dateFormat(QLocale::ShortFormat)));
                // save QDate object -> no need do date conversion from string
                enditem->setData(Qt::UserRole, st->getEndDate());
                m_ui->tableWidget->setItem(row, 8, enditem);
            }

            if (!isReadOnly && i < studies.size()) {
                schoolStudy *nst = studies[0];
                if (i < studies.size() - 1)
                    nst = studies[i + 1];

                if (nst->getNumber() != st->getNumber() && !st->getStartdate().isNull() && st->getExercise()) {
                    // add empty row
                    m_ui->tableWidget->insertRow(row + 1);
                    row += 1;
                    m_ui->tableWidget->setItem(row, 0, new QTableWidgetItem(""));
                    m_ui->tableWidget->setItem(row, 1, new QTableWidgetItem(QVariant(st->getNumber()).toString()));
                    m_ui->tableWidget->setItem(row, 6, new QTableWidgetItem(""));
                    QTableWidgetItem *chkBoxItem2 = new QTableWidgetItem();
                    chkBoxItem2->setCheckState(Qt::Unchecked);
                    chkBoxItem2->setFlags(chkBoxItem->flags());
                    m_ui->tableWidget->setItem(row, 7, chkBoxItem2);
                    m_ui->tableWidget->setItem(row, 8, new QTableWidgetItem(""));
                }
            }

            if ((row - studystartrow) > 0) {
                // set span of rows when showing multiple rows for same study
                m_ui->tableWidget->setSpan(studystartrow, 2, row - studystartrow + 1, 1);
                m_ui->tableWidget->setSpan(studystartrow, 3, row - studystartrow + 1, 1);
                m_ui->tableWidget->setSpan(studystartrow, 4, row - studystartrow + 1, 1);
                m_ui->tableWidget->setSpan(studystartrow, 5, row - studystartrow + 1, 1);
            }

            row += 1;
        }
    }
    // Restore scrollbar position
    m_ui->tableWidget->verticalScrollBar()->setValue(m_studiesGridScrollPosition);
}

/*void personsui::selectPerson(int personid)
{
    // choose a person from list
    foreach (person *p,m_users){
        if(personid == p->id()){
            m_ui->lstPublishers->setCurrentIndex(
                    m_ui->lstPublishers->model()->index(m_users.indexOf(p),0));
            iPersonIndex = m_users.indexOf(p);
            break;
        }
    }
}*/

void personsui::updateFamiliesList()
{
    m_ui->comboFamilies->clear();
    m_ui->comboFamilies->addItem("");
    qDeleteAll(m_families);
    m_families.clear();
    m_families = family::getFamilies();
    foreach (family *f, m_families) {
        m_ui->comboFamilies->addItem(f->getName());
    }
}

void personsui::setDefaultPerson(QString name)
{
    m_defaultPersonName = name;
}

bool personsui::saveChanges()
{
    bool ok = saveChanges(m_ui->lstPublishers->currentItem());
    if (ok) {
        m_ui->lstPublishers->setCurrentItem(nullptr);
        ClearDetailsPane();
        m_ui->lstPublishers->clearFocus();
    }
    return ok;
}

void personsui::showEvent(QShowEvent *)
{
    if (m_defaultPersonName != "") {

        std::unique_ptr<person> defaultperson { cpersons::getPerson(m_defaultPersonName) };
        if (!defaultperson)
            return;
        // select person from list
        for (auto i = 0, n = m_ui->lstPublishers->count(); i < n; ++i) {
            auto item = m_ui->lstPublishers->item(i);
            if (defaultperson->id() == item->data(Qt::UserRole).toInt()) {
                on_lstPublishers_currentItemChanged(item, m_ui->lstPublishers->currentItem());
                break;
            }
        }
    }
}

void personsui::closeEvent(QCloseEvent *e)
{
    // save changes in close event
    if (!saveChanges(m_ui->lstPublishers->currentItem())) {
        e->ignore();
    } else {
        e->accept();
    }
}

// add, change or remove study
void personsui::on_tableWidget_itemChanged(QTableWidgetItem *item)
{
    if (!editmode)
        return;
    QLocale local;
    qDebug() << "changed" << item->row() << item->column();
    editmode = false;
    school s_class;

    schoolStudentStudy *std = new schoolStudentStudy();
    std->setStudentId(m_ui->lstPublishers->currentItem()->data(Qt::UserRole).toInt());
    std->setId(m_ui->tableWidget->item(item->row(), 0)->text().toInt());
    std->setNumber(m_ui->tableWidget->item(item->row(), 1)->text().toInt());
    bool checked = (m_ui->tableWidget->item(item->row(), 7)->checkState() == Qt::Checked);
    std->setExercise(checked);

    if (item->column() == 7) {
        if (checked) {
            // add dates
            if (m_ui->tableWidget->item(item->row(), 6)->text() == "") {
                QTableWidgetItem *startitem = new QTableWidgetItem(QDate::currentDate().toString(local.dateFormat(QLocale::ShortFormat)));
                startitem->setData(Qt::UserRole, QDate::currentDate());
                m_ui->tableWidget->setItem(item->row(), 6, startitem);
            }
            QTableWidgetItem *enditem = new QTableWidgetItem(QDate::currentDate().toString(local.dateFormat(QLocale::ShortFormat)));
            enditem->setData(Qt::UserRole, QDate::currentDate());
            m_ui->tableWidget->setItem(item->row(), 8, enditem);
        } else {
            m_ui->tableWidget->setItem(item->row(), 8, new QTableWidgetItem());

            // undone
            qDebug() << "undone";
            std->setEndDate(QDate());
            std->setExercise(false);
        }

    } else if (item->column() == 6 || item->column() == 8) {
        if (item->text() == "") {
            qDebug() << "empty";
            return;
        }
    }

    // days
    // read start date from user data -> get QDate object directly
    QDate d1 = m_ui->tableWidget->item(item->row(), 6)->data(Qt::UserRole).toDate();
    std->setStartdate(d1);
    // read end date from user data -> get QDate object directly
    QDate d2 = m_ui->tableWidget->item(item->row(), 8)->data(Qt::UserRole).toDate();
    std->setEndDate(d2);

    if (m_ui->tableWidget->item(item->row(), 0)->text().toInt() < 1) {
        // insert a new study
        int newid = s_class.addStudentStudy(m_ui->lstPublishers->currentItem()->data(Qt::UserRole).toInt(), std)->getId();
        m_ui->tableWidget->item(item->row(), 0)->setText(QVariant(newid).toString());
    } else {
        // update existing study
        qDebug() << "id=" << m_ui->tableWidget->item(item->row(), 0)->text();
        qDebug() << "lopetus" << d2.toString(Qt::ISODate) << "///" << m_ui->tableWidget->item(item->row(), 8)->text();
        std->update();
    }

    // save scrollbar position
    m_studiesGridScrollPosition = m_ui->tableWidget->verticalScrollBar()->value();
    // reload studies grid list
    getStudies();
    editmode = true;
}

void personsui::calendarChanged(int row)
{
    qDebug() << "date changed" << row << "-" << studycolumn;
    m_ui->tableWidget->removeCellWidget(row, studycolumn);
    QSignalMapper *s = qobject_cast<QSignalMapper *>(sender());
    QDateEdit *d = qobject_cast<QDateEdit *>(s->mapping(row));
    QLocale local;
    QTableWidgetItem *item = new QTableWidgetItem(d->date().toString(local.dateFormat(QLocale::ShortFormat)));
    // save QDate object in user data
    item->setData(Qt::UserRole, d->date());
    m_ui->tableWidget->setItem(row, studycolumn, item);
}

void personsui::on_tableWidget_itemClicked(QTableWidgetItem *item)
{
    if (!item)
        return;

    if (!ac->user()->hasPermission(Permission::Rule::CanEditStudentData))
        return;

    studycolumn = item->column();

    if (m_ui->tableWidget->item(item->row(), 7)->flags() == Qt::NoItemFlags)
        return;

    qDebug() << "itemclicked" << studycolumn;
    if ((item->text() == "" && item->column() == 8) || (item->column() != 6 && item->column() != 8))
        return;

    QDateEdit *sdate = new QDateEdit();
    if (item->text() == "") {
        sdate->setDate(QDate::currentDate());
    } else {
        sdate->setDate(item->data(Qt::UserRole).toDate());
    }

    if (item->column() == 6) {
        if ((m_ui->tableWidget->item(item->row(), 8))->text() != "")
            sdate->setMaximumDate(m_ui->tableWidget->item(item->row(), 8)->data(Qt::UserRole).toDate());
    } else {
        sdate->setMinimumDate(m_ui->tableWidget->item(item->row(), 6)->data(Qt::UserRole).toDate());
    }

    QSignalMapper *s = new QSignalMapper(this);
    s->setMapping(sdate, item->row());
    connect(sdate, SIGNAL(editingFinished()), s, SLOT(map()));
    connect(s, SIGNAL(mapped(int)), this, SLOT(calendarChanged(int)));
    m_ui->tableWidget->setCellWidget(item->row(), studycolumn, sdate);
    sdate->setFocus(Qt::MouseFocusReason);
}

void personsui::addStudyRemoveButton(QTableWidget *table, int row, int col)
{
    QIcon doneicon = QIcon(":/icons/remove.svg");
    QToolButton *button = new QToolButton();
    button->setIcon(doneicon);
    button->setToolTip(tr("Remove study"));
    button->setProperty("id", table->item(row, 0)->text().toInt());
    connect(button, SIGNAL(clicked()), this, SLOT(on_buttonStudyRemove_clicked()));
    table->setCellWidget(row, col, button);
}

void personsui::on_buttonStudyRemove_clicked()
{
    int id = static_cast<QToolButton *>(sender())->property("id").toInt();
    schoolStudentStudy *std = new schoolStudentStudy();
    std->setId(id);
    if (std->remove()) {
        editmode = false;
        // save scrollbar position
        m_studiesGridScrollPosition = m_ui->tableWidget->verticalScrollBar()->value();
        // reload studies grid list
        getStudies();
        editmode = true;
    }
}

void personsui::on_buttonWtConductorDefault_toggled(bool checked)
{
    m_ui->buttonWtConductorDefault->setIcon(QIcon(checked ? ":/icons/default.svg" : ":/icons/default_off.svg"));
}
