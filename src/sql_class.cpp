/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sql_class.h"

sql_class::sql_class()
{
    //createConnection();;
    db = new QSqlDatabase();
}
sql_class::~sql_class()
{
}
bool sql_class::createConnection()
{
    if (db->open()) {
        return true;
    }

    if (!QFile::exists(databasepath)) {
        QMessageBox::critical(0, "", QObject::tr("Database file not found! Searching path =") + databasepath);
    }
    if (db->contains("theocbase"))
        return true;

#ifndef NOSQLDUMP
    qDebug() << "-- new database connection --" << databasepath;
#endif

    db = new QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE", "theocbase"));
    db->setHostName("localhost");
    db->setConnectOptions();
    db->setDatabaseName(databasepath);
    db->setUserName("user");
    db->setPassword("1234");
#ifndef NOSQLDUMP
    qDebug() << "!! transaction support " << db->driver()->hasFeature(QSqlDriver::Transactions); //true
#endif
    if (!db->open()) {
        QMessageBox::critical(0, QObject::tr("Database Error"),
                              db->lastError().text());
        return false;
    }
    return true;
}

// sql search
sql_items sql_class::selectSql(QString tname, QString searchfield, QString value, QString orderbytable)
{
#ifndef NOSQLDUMP
    qDebug() << "tablename=" + tname + " searchfield=" + searchfield + " value=" + value;
#endif
    QSqlQuery query(*db);
    QString str;
    sql_item *values = NULL;

    if (searchfield != "") {
        values = new sql_item();
        values->insert(":value", value);
        str = ("SELECT * FROM '" + tname + "' WHERE " + searchfield + "=:value");
    } else {
        str = ("SELECT * FROM " + tname);
    }
    if (orderbytable != "") {
        str += " ORDER BY " + orderbytable;
    }

#ifndef NOSQLDUMP
    qDebug() << str;
#endif

    query.prepare(str);
    bindQueryValues(&query, values);
    query.exec();
    sql_items temp;
    temp.clear();

    while (query.next()) {
        sql_item s;

        for (int i = 0; i < query.record().count(); i++) {
            s.insert(query.record().fieldName(i), query.value(i).toString());
            //qDebug() << "sqlSelect: added " + query.record().fieldName(i) + " value=" + query.value(i).toString();
        }
        temp.push_back(s);
    }

    if (values)
        delete values;
    return temp;
}

sql_items sql_class::selectSql(QString fullsqlquery)
{
    return selectSql(fullsqlquery, NULL);
}

sql_items sql_class::selectSql(QString fullsqlquery, sql_item *values)
{
#ifndef NOSQLDUMP
    qDebug() << "sql query=" + fullsqlquery;
    /* to show long queries:
        qDebug() << "sql query=";
        int len(1000);
        for(int p(0); p < fullsqlquery.length(); p+=1000) {
            if (p + 1000 > fullsqlquery.length())
                len = fullsqlquery.length() - p;
            qDebug() << fullsqlquery.mid(p, len);
        }
        */
#endif

    sql_items temp;
    temp.clear();
    bool ret;
    QSqlQuery query(*db);

    if (values) {
        query.prepare(fullsqlquery);
        bindQueryValues(&query, values);
        ret = query.exec();
    } else {
        ret = query.exec(fullsqlquery);
    }

    if (ret) {
        while (query.next()) {
            sql_item s;

            for (int i = 0; i < query.record().count(); i++) {
                s.insert(query.record().fieldName(i), query.value(i));
                //qDebug() << "sqlSelect: added " + query.record().fieldName(i) + " value=" + query.value(i).toString();
            }
            temp.push_back(s);
        }
    } else {
        QString errMsg = "Error in SQL query\n" + fullsqlquery + "\n" + query.lastError().text();
        QMessageBox::critical(NULL, "TheocBase", errMsg);
        /* next version? ...
            if (QMessageBox::critical(NULL,"TheocBase", errMsg + "\n\nCopy Message to Clipboard?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes) {
                QClipboard *clipboard = QApplication::clipboard();
                clipboard->setText(errMsg);
            }
            or even better maybe create a separate class for error messages
            also use exceptions instead of showing messages from sql_class
            */
    }
    return temp;
}

// returns <data, columnNames> since a plain hash doesn't keep the order of the columns
QPair<sql_items, QList<QString>> sql_class::selectSql2(QString fullsqlquery, sql_item *values)
{
#ifndef NOSQLDUMP
    qDebug() << "sql query=" + fullsqlquery;
    /* to show long queries:
        qDebug() << "sql query=";
        int len(1000);
        for(int p(0); p < fullsqlquery.length(); p+=1000) {
            if (p + 1000 > fullsqlquery.length())
                len = fullsqlquery.length() - p;
            qDebug() << fullsqlquery.mid(p, len);
        }
        */
#endif
    QSqlQuery query(*db);
    query.prepare(fullsqlquery);
    bindQueryValues(&query, values);
    query.exec();
    sql_items temp;
    QList<QString> cols;
    temp.clear();
    bool isFirst(true);

    if (query.lastError().type() == QSqlError::NoError) {
        while (query.next()) {
            sql_item s;

            if (isFirst) {
                for (int i = 0; i < query.record().count(); i++) {
                    cols.append(query.record().fieldName(i));
                }
                isFirst = false;
            }
            for (int i = 0; i < query.record().count(); i++) {
                s.insert(query.record().fieldName(i), query.value(i));
                //qDebug() << "sqlSelect: added " + query.record().fieldName(i) + " value=" + query.value(i).toString();
            }
            temp.push_back(s);
        }
    } else {
        QString errMsg = "Error in SQL query\n" + fullsqlquery + "\n" + query.lastError().text();
        QMessageBox::critical(NULL, "TheocBase", errMsg);
        /* next version? ...
            if (QMessageBox::critical(NULL,"TheocBase", errMsg + "\n\nCopy Message to Clipboard?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes) {
                QClipboard *clipboard = QApplication::clipboard();
                clipboard->setText(errMsg);
            }
            or even better maybe create a separate class for error messages
            also use exceptions instead of showing messages from sql_class
            */
    }
    return qMakePair(temp, cols);
}

QVariant sql_class::selectScalar(QString fullsqlquery, sql_item *values)
{
    QVariant defaultvalue;
    return selectScalar(fullsqlquery, values, defaultvalue);
}

QVariant sql_class::selectScalar(QString fullsqlquery, sql_item *values, QVariant defaultvalue)
{
#ifndef NOSQLDUMP
    qDebug() << "sql query=" + fullsqlquery;
    /* to show long queries:
        qDebug() << "sql query=";
        int len(1000);
        for(int p(0); p < fullsqlquery.length(); p+=1000) {
            if (p + 1000 > fullsqlquery.length())
                len = fullsqlquery.length() - p;
            qDebug() << fullsqlquery.mid(p, len);
        }
        */
#endif
    QSqlQuery query(*db);
    query.prepare(fullsqlquery);
    bindQueryValues(&query, values);
    query.exec();
    QVariant temp;

    if (query.lastError().type() == QSqlError::NoError) {
        if (query.next())
            temp = query.record().value(0);
        else
            temp = defaultvalue;
    } else {
        QMessageBox::critical(NULL, "TheocBase", "Error in SQL query\n" + fullsqlquery + "\n" + query.lastError().text());
    }
    return temp;
}

QVariant sql_class::selectScalar(QString fullsqlquery, QVariant value, QVariant defaultvalue)
{
#ifndef NOSQLDUMP
    qDebug() << "sql query=" + fullsqlquery;
    /* to show long queries:
        qDebug() << "sql query=";
        int len(1000);
        for(int p(0); p < fullsqlquery.length(); p+=1000) {
            if (p + 1000 > fullsqlquery.length())
                len = fullsqlquery.length() - p;
            qDebug() << fullsqlquery.mid(p, len);
        }
        */
#endif
    QSqlQuery query(*db);
    query.prepare(fullsqlquery);
    query.bindValue(0, value);
    query.exec();
    QVariant temp;

    if (query.lastError().type() == QSqlError::NoError) {
        if (query.next())
            temp = query.record().value(0);
        else
            temp = defaultvalue;
    } else {
        QMessageBox::critical(NULL, "TheocBase", "Error in SQL query\n" + fullsqlquery + "\n" + query.lastError().text());
    }
    return temp;
}

int sql_class::insertSql(QString tname, sql_item *values, QString idfield)
{
    // insert a new row to the database

    QSqlQuery query(*db);
    QString str = "INSERT INTO " + tname + "(" + idfield;
    int nextid = getNextId(tname, idfield);
    QString valuesstr = " VALUES (" + QString::number(nextid);
    for (sql_item::const_iterator i = values->constBegin(); i != values->constEnd(); ++i) {
        str.append(",");
        str.append(i.key()); // values.getFieldName(i);
        // our "value" in the query is a parameter name. bindQueryValues will add the actual values.
        valuesstr.append(",:");
        valuesstr.append(i.key());
    }
    str.append(",time_stamp");
    valuesstr.append("," + QVariant(getCurrentTimeStamp()).toString());
    if (!values->contains("uuid")) {
        str.append(",uuid");
        valuesstr.append(",'" + getUuid() + "'");
    }
    str.append(")");
    valuesstr.append(")");

    str.append(valuesstr);

#ifndef NOSQLDUMP
    qDebug() << "SQLINSERT= " + str;
#endif

    query.prepare(str);
    bindQueryValues(&query, values);
    query.exec();

    if (query.lastError().type() != QSqlError::NoError) {
        QMessageBox::critical(NULL, QString("insertSql failed!"),
                              query.lastError().text());
        QMessageBox::critical(NULL, QString("sql"), str);
        return -1;
    }
    if (query.numRowsAffected() > 0)
        emit dbChanged(tname);
    return nextid;
}

bool sql_class::updateSql(QString tname, QString idname, QString idvalue, sql_item *values)
{
    // update existing row in the database

    QSqlQuery query(*db);
    QString str = "UPDATE " + tname;
    QString valuesstr = " SET ";
    bool isFirst = true;
    for (sql_item::const_iterator i = values->constBegin(); i != values->constEnd(); ++i) {
        if (isFirst)
            isFirst = false;
        else
            valuesstr.append(",");
        valuesstr.append(i.key() + " = :" + i.key());
    }
    valuesstr.append(",time_stamp = " + QVariant(getCurrentTimeStamp()).toString());
    str.append(valuesstr + " WHERE " + idname + "='" + idvalue + "'");

#ifndef NOSQLDUMP
    qDebug() << "SQLUPDATE= " + str;
#endif

    query.prepare(str);
    bindQueryValues(&query, values);
    query.exec();
    lastNumRowsAffected = query.numRowsAffected();

    if (query.lastError().type() != QSqlError::NoError) {
        QMessageBox::critical(NULL, QString("updateSql failed!"),
                              query.lastError().text());
        QMessageBox::critical(NULL, QString("sql"), str);
        return false;
    }
    if (lastNumRowsAffected > 0)
        emit dbChanged(tname);
    return true;
}

bool sql_class::removeSql(QString tname, QString where)
{
    QString str = "DELETE FROM " + tname + " WHERE " + where;
    QSqlQuery query(*db);

#ifndef NOSQLDUMP
    qDebug() << "SQLDELETE = " + str;
#endif

    query.prepare(str);
    query.exec();

    if (query.lastError().type() != QSqlError::NoError) {
        QMessageBox::critical(NULL, QString("removeSql failed!"),
                              query.lastError().text());
        QMessageBox::critical(NULL, QString("sql"), str);
        return false;
    }
    if (query.numRowsAffected() > 0)
        emit dbChanged(tname);
    return true;
}

bool sql_class::removeSql(QString tname, QString where, sql_item *values)
{
    QString str = "DELETE FROM " + tname + " WHERE " + where;
    QSqlQuery query(*db);

#ifndef NOSQLDUMP
    qDebug() << "SQLDELETE = " + str;
#endif

    query.prepare(str);
    bindQueryValues(&query, values);
    query.exec();

    if (query.lastError().type() != QSqlError::NoError) {
        QMessageBox::critical(NULL, QString("removeSql failed!"),
                              query.lastError().text());
        QMessageBox::critical(NULL, QString("sql"), str);
        return false;
    }
    if (query.numRowsAffected() > 0)
        emit dbChanged(tname);
    return true;
}

bool sql_class::execSql(QString query, QString okErr)
{
#ifndef NOSQLDUMP
    qDebug() << "SQLEXEC = " + query;
#endif
    QSqlQuery q(*db);
    q.prepare(query);
    q.exec();

    if (q.lastError().type() == QSqlError::NoError) {
        if (q.numRowsAffected() > 0)
            emit dbChanged();
        return true;
    } else {
        QString err = q.lastError().text();
        if (okErr.isNull() || (okErr != "Any" && !err.contains(okErr))) {
            QMessageBox::critical(NULL, "TheocBase", "Error in SQL query:\n" + query + "\n" + err + "(#" + QVariant(q.lastError().nativeErrorCode()).toString() + ")");
            q.clear();
            return false;
        }
        return true;
    }
}

bool sql_class::execSql(QString query, sql_item *values, bool valueAssistant)
{
#ifndef NOSQLDUMP
    qDebug() << "SQLEXEC = " + query;
#endif
    QSqlQuery q(*db);
    q.prepare(query);
    bindQueryValues(&q, values, valueAssistant);
    q.exec();

    if (q.lastError().type() == QSqlError::NoError) {
        if (q.numRowsAffected() > 0)
            emit dbChanged();
        return true;
    } else {
        QMessageBox::critical(NULL, "TheocBase", "Error in SQL query:\n" + query + "\n" + q.lastError().text());
        q.clear();
        return false;
    }
}

int sql_class::getNextId(QString tablename, QString fieldname)
{
    QString squery =
            "SELECT MAX(%1) FROM %2 "
            "UNION ALL "
            "SELECT seq FROM theocbase_sequence WHERE name = '%2'";
    squery = QString(squery).arg(fieldname, tablename);
#ifndef NOSQLDUMP
    qDebug() << squery;
#endif
    QSqlQuery query(squery, *db);
    query.first();

    if (query.lastError().type() != QSqlError::NoError) {
        // table not exists
        query.prepare(QString("SELECT MAX(%1) FROM %2").arg(fieldname, tablename));
        query.exec();
        query.first();
        return query.value(0).toInt() + 1;
    }

    QString savestring = "";
    int newvalue = 0;

    int maxvalue = query.value(0).toInt();
    if (query.next()) {
        int seqvalue = query.value(0).toInt();
        if (maxvalue > seqvalue)
            seqvalue = maxvalue;
        newvalue = seqvalue + 1;
        savestring = "UPDATE theocbase_sequence SET seq = %1 WHERE name = '%2'";
    } else {
        newvalue = maxvalue + 1;
        savestring = "INSERT INTO theocbase_sequence (name, seq) VALUES ('%2', %1)";
    }

    savestring = QString(savestring).arg(QVariant(newvalue).toString(), tablename);
#ifndef NOSQLDUMP
    qDebug() << "newidvalue=" + QString::number(newvalue) + " " + savestring;
#endif

    query.prepare(savestring);
    query.exec();
    return newvalue;
}

// get unique id value
QString sql_class::getUuid(QString basedOn)
{
    QUuid u;
    if (basedOn != "") {
        u = QUuid::createUuidV5(QUuid(), QString(basedOn));
    } else {
        u = QUuid::createUuid();
    }
    return u.toString().remove("{").remove("}");
}

// Get current timestamp UNIT TIME formatted
int sql_class::getCurrentTimeStamp()
{
    QDateTime date = QDateTime::currentDateTime();
    return date.toTime_t();
}

bool sql_class::deleteNonActive()
{
    QStringList list;
    list << "delete from biblestudyMeeting where active = 0"
         << "delete from congregations where active = 0"
         << "delete from e_reminder where active = 0"
         << "delete from exceptions where active = 0"
         << "delete from languages where active = 0"
         << "delete from lmm_meeting where active = 0"
         << "delete from lmm_schedule where active = 0"
         << "delete from outgoing where active = 0"
         << "delete from persons where active = 0 and id not in (select distinct chairman from lmm_meeting) and id not in (select distinct prayer_beginning from lmm_meeting) and id not in (select distinct prayer_end from lmm_meeting) and id not in (select distinct counselor2 from lmm_meeting) and id not in (select distinct counselor3 from lmm_meeting) and id not in (select distinct assignee_id from lmm_assignment) and id not in (select distinct volunteer_id from lmm_assignment) and id not in (select distinct assistant_id from lmm_assignment) and id not in (select distinct assignee_id from lmm_assignment) and id not in (select distinct assignee_id from lmm_assignment) and id not in (select distinct speaker_id from publicmeeting) and id not in (select distinct wtreader_id from publicmeeting) and id not in (select distinct chairman_id from publicmeeting) and id not in (select distinct speaker_id from speaker_publictalks) and id not in (select distinct speaker_id from outgoing) and id not in (select distinct person_id from territory_assignment)"
         << "delete from publicmeeting where active = 0"
         << "delete from publictalks where active = 0"
         << "delete from school where active = 0"
         << "delete from school_break where active = 0"
         << "delete from school_schedule where active = 0"
         << "delete from school_settings where active = 0"
         << "delete from serviceMeeting where active = 0"
         << "delete from serviceProgram where active = 0"
         << "delete from settings where active = 0"
         << "delete from song where active = 0"
         << "delete from speaker_publictalks where active = 0"
         << "delete from student_studies where active = 0"
         << "delete from studies where active = 0"
         << "delete from territory where active = 0"
         << "delete from territory_street where active = 0"
         << "delete from territory_streettype where active = 0"
         << "delete from territory_address where active = 0"
         << "delete from territory_addresstype where active = 0"
         << "delete from territory_assignment where active = 0"
         << "delete from territory_city where active = 0"
         << "delete from territory_type where active = 0"
         << "delete from unavailables where active = 0";
    for (QString x : list) {
        if (!execSql(x))
            return false;
    }
    return true;
}

bool sql_class::saveSetting(QString name, QString value)
{
    QSqlQuery query(*db);
    query.prepare("INSERT OR REPLACE INTO settings (id, name, value, time_stamp) "
                  "VALUES "
                  "((SELECT ifnull((SELECT id FROM settings WHERE name=:name),(SELECT ifnull((SELECT max(id) FROM settings),0)+1))),"
                  ":name,:value,:time_stamp)");
    query.bindValue(":name", name);
    query.bindValue(":value", value);
    query.bindValue(":time_stamp", getCurrentTimeStamp());
    query.exec();

    if (query.lastError().type() != QSqlError::NoError) {
        QMessageBox::information(NULL, "Save failed!", query.lastQuery() + "\n\n" + query.lastError().text());
        return false;
    } else {
#ifndef NOSQLDUMP
        qDebug() << "saveSetting: " << name << "-" << value;
#endif
    }

    sql_class::getSettings()->insert(name, value);
    return true;
}

// get cached setting
QString sql_class::getSetting(QString name, QString defaultvalue)
{
    if (sql_class::getSettings()->contains(name))
        return sql_class::getSettings()->value(name);
    return defaultvalue;
}

// get cached setting
int sql_class::getIntSetting(QString name, int defaultvalue)
{
    if (sql_class::getSettings()->contains(name)) {
        bool ok(false);
        int iVal(sql_class::getSettings()->value(name).toInt(&ok));
        if (ok)
            return iVal;
    }
    return defaultvalue;
}

// cache settings table for quick
QHash<QString, QString> *sql_class::getSettings()
{
    if (cachedSettings.count() == 0) {
        sql_class *sql = &Singleton<sql_class>::Instance();

        QSqlQuery query("SELECT name, value FROM settings", *sql->db);
        query.exec();
        if (query.lastError().type() == QSqlError::NoError) {
            while (query.next()) {
                QSqlRecord rec = query.record();
                cachedSettings.insert(rec.value(0).toString(), rec.value(1).toString());
            }
        }
    }
    return &cachedSettings;
}

sql_item sql_class::getPublicMeetingInfo(int congregation_id, QDate weekof)
{
    QString qry("select mtg_day, mtg_time from congregationmeetingtimes where congregation_id = :congregation_id order by abs(mtg_year - :year) limit 1");
    sql_item values;
    values.insert(":congregation_id", congregation_id);
    values.insert(":year", weekof.year());
    sql_items mInfoSet(selectSql(qry, &values));
    sql_item mInfo;
    if (!mInfoSet.empty()) {
        mInfo = mInfoSet[0];
        // if the year of the meeting is different than the given week of, run the query again with that date
        QDate mdate(weekof.addDays(mInfo.value("mtg_day").toInt() - 1));
        if (weekof.year() != mdate.year()) {
            values[":year"] = mdate.year();
            mInfoSet = selectSql(qry, &values);
            if (!mInfoSet.empty()) {
                mInfo = mInfoSet[0];
            }
        }
    }
    return mInfo;
}

void sql_class::updateThisYearMeetingTimes()
{
    execSql("insert into congregationmeetingtimes (congregation_id, mtg_year, mtg_day, mtg_time) "
            "select c.id, strftime('%Y', 'now'), ifnull(current.mtg_day, 7), ifnull(current.mtg_time, '10:00') "
            "from congregations c "
            "left join congregationmeetingtimes current "
            "on c.id = current.congregation_id and current.mtg_year = strftime('%Y', 'now') "
            "where current.congregation_id is null");
}

void sql_class::updateNextYearMeetingTimes()
{
    execSql("insert into congregationmeetingtimes (congregation_id, mtg_year, mtg_day, mtg_time) "
            "select c.id, strftime('%Y', 'now') + 1, ifnull(current.mtg_day, 7), ifnull(current.mtg_time, '10:00') "
            "from congregations c "
            "left join congregationmeetingtimes current "
            "on c.id = current.congregation_id and current.mtg_year = strftime('%Y', 'now') "
            "left join congregationmeetingtimes next "
            "on c.id = next.congregation_id and next.mtg_year = strftime('%Y', 'now') + 1 "
            "where next.congregation_id is null");
}

void sql_class::updateDatabase(QString version)
{
    if (!createConnection()) {
        return;
    }

    // SQLITE 3.25.0 and newer (Qt 5.12 and newer) has a new behaviour:
    // alter table updates references to the renamed table in views
    // -> option to behave as it did earlier
    // https://www.sqlite.org/pragma.html#pragma_legacy_alter_table
    execSql("PRAGMA legacy_alter_table=ON");

    QString dbVersion = getSetting("appver");

    if (dbVersion == version) {
#ifndef NOSQLDUMP
        qDebug() << "no need update database";
#endif
#ifndef QT_DEBUG // when in non-production, always check for database upgrades
        return;
#endif
    } else if (dbVersion > version) {
#if !defined(Q_OS_IOS) && !defined(Q_OS_ANDROID) && !defined(Q_OS_WINRT)
        QMessageBox::information(0, "TheocBase", QObject::tr("This version of the application (%1) is older than the database (%2). There is a strong probability that error messages will popup and changes may not be saved correctly. Please download and install the latest version for best results.").arg(version).arg(dbVersion));
#endif
        return;
    }

    // save platform
    saveSetting("platform", QSysInfo::productType());
    saveSetting("appver_old", getSetting("appver"));

    QString resourcepath = ":/database";
    QDir myDir(resourcepath);
    QStringList nameFilter;
    nameFilter << "database_upgrade*.txt";
    myDir.setNameFilters(nameFilter);
    myDir.setSorting(QDir::Name);
    QStringList dbUpgrades = myDir.entryList();
    QRegularExpression rx("database_upgrade(.+)[.]txt");
    for (QString f : dbUpgrades) {
        QRegularExpressionMatch match(rx.match(f));
        if (match.hasMatch()) {
            QString upgradeTo = match.captured(1).replace("_", ".");
            if (dbVersion < upgradeTo) {
                if (upgradeTo == "2014.01.0")
                    if (updateDatabase_2014010())
                        return;

                if (runDatabaseBatch(resourcepath + "/" + f))
                    return;

                // successfully upgraded at least this far
                saveSetting("appver", upgradeTo);
            }
        }
    }

#ifndef QT_DEBUG // only needed in production. Debug version will have set this above if all went well
    saveSetting("appver", version);
#endif
#ifndef NOSQLDUMP
    qDebug() << "updateDatabase done";
#endif
    // show startup screen after database update
    saveSetting("screen", "true");
#if !defined(Q_OS_IOS) && !defined(Q_OS_ANDROID) && !defined(Q_OS_WINRT)
    QMessageBox::information(0, "TheocBase", QObject::tr("Database updated"));
#endif
    // force to reload the settings table
    cachedSettings.clear();
}

int sql_class::updateDatabase_2014010()
{

    // copy old database to backup
    QString backupfile = databasepath + "." + QDateTime::currentDateTime().toString("yyyyMMddhhmmss");

    QFile::copy(databasepath, backupfile);

#ifndef NOSQLDUMP
    qDebug() << "database updating...";
#endif

    // sequences table
    // ---------------
    addColumn("theocbase_sequence", "name", "");
    addColumn("theocbase_sequence", "seq", "");

    // settings table
    // --------------
    addColumn("settings", "name", "TEXT");
    addColumn("settings", "value", "TEXT");
    addColumn("settings", "time_stamp", "INT", "0");
    addColumn("settings", "active", "BOOLEAN", "1");
    addColumn("settings", "uuid", "TEXT");
    // copy data from asetukset table when upgrade
    if (isTableExists("asetukset")) {
        execSql("INSERT INTO settings "
                "SELECT id,nimi,arvo,0,1,NULL "
                "FROM asetukset");
        deleteTable("asetukset");
        execSql("UPDATE theocbase_sequence SET name = 'settings' WHERE name = 'asetukset'");
    }

    // person table scheme
    // -------------------
    addColumn("persons", "firstname", "VARCHAR(255)");
    addColumn("persons", "lastname", "VARCHAR(255)");
    addColumn("persons", "servant", "BOOLEAN", "false");
    addColumn("persons", "gender", "CHAR(1)", "B");
    addColumn("persons", "usefor", "INT");
    addColumn("persons", "congregation_id", "INT");
    addColumn("persons", "phone", "VARCHAR(255)");
    addColumn("persons", "email", "VARCHAR(255)");
    addColumn("persons", "info", "VARCHAR(255)");
    addColumn("persons", "time_stamp", "INT", "0");
    addColumn("persons", "active", "BOOLEAN", "1");
    addColumn("persons", "uuid", "TEXT");
    // copy content from henkilot table when upgrade
    if (isTableExists("henkilot")) {
        addColumn("henkilot", "etunimi", "VARCHAR(255)");
        addColumn("henkilot", "srk", "INT");
        addColumn("henkilot", "email", "VARCHAR(255)");
        addColumn("henkilot", "info", "VARCHAR(255)");
        execSql("INSERT INTO persons "
                "SELECT id,etunimi,nimi,nimitetty,sukupuoli,ohjelmatyypit,srk,puh,email,info,0,1,NULL "
                "FROM henkilot");
        // change gender 'V' to 'B'
        execSql("UPDATE persons SET gender = 'B' WHERE gender = 'V'");
        // remove henkilot table
        deleteTable("henkilot");
        // change name in sequence table
        execSql("UPDATE theocbase_sequence SET name = 'persons' WHERE name = 'henkilot'");
    }

    // school table scheme
    // -------------------
    addColumn("school", "school_schedule_id", "INT");
    addColumn("school", "classnumber", "INT", "1");
    addColumn("school", "student_id", "INT");
    addColumn("school", "volunteer_id", "INT", "-1");
    addColumn("school", "assistant_id", "INT", "-1");
    addColumn("school", "date", "DATE");
    addColumn("school", "completed", "BOOLEAN", "0");
    addColumn("school", "note", "VARCHAR(255)");
    addColumn("school", "timing", "VARCHAR(255)");
    addColumn("school", "setting_id", "INT");
    addColumn("school", "time_stamp", "INT", "0");
    addColumn("school", "active", "BOOLEAN", "1");
    addColumn("school", "uuid", "TEXT");
    // copy content from koulu table when upgrade
    if (isTableExists("koulu")) {
        addColumn("koulu", "ajoitus", "VARCHAR(255)");
        addColumn("koulu", "idavustaja", "INT");
        addColumn("koulu", "koulunro", "INT", "1");
        execSql("INSERT INTO school "
                "SELECT idkoulu,k_ohjelma_idk_ohjelma,koulunro,idhenkilo,idsijainen,idavustaja,pvm,suoritettu,huomautus,ajoitus,idasetelma,0,1,NULL "
                "FROM koulu");
        deleteTable("koulu");
        execSql("UPDATE theocbase_sequence SET name = 'school' WHERE name = 'koulu'");
    }

    // school_schedule table
    // ---------------------
    addColumn("school_schedule", "date", "DATE");
    addColumn("school_schedule", "number", "INT");
    addColumn("school_schedule", "theme", "TEXT");
    addColumn("school_schedule", "source", "TEXT");
    addColumn("school_schedule", "onlybrothers", "BOOLEAN", "0");
    addColumn("school_schedule", "time_stamp", "INT", "0");
    addColumn("school_schedule", "active", "BOOLEAN", "1");
    addColumn("school_schedule", "uuid", "TEXT");
    // copy content from k_ohjelma table when upgrade
    if (isTableExists("k_ohjelma")) {
        addColumn("k_ohjelma", "aineisto", "VARCHAR(255)");
        addColumn("k_ohjelma", "veli", "BOOLEAN", "0");
        execSql("INSERT INTO school_schedule "
                "SELECT idk_ohjelma,pvm,p_nro,teema,aineisto,veli,0,1,NULL "
                "FROM k_ohjelma");
        deleteTable("k_ohjelma");
        execSql("UPDATE theocbase_sequence SET name = 'school_schedule' WHERE name = 'k_ohelma'");
    }

    // school_break table
    // ------------------
    addColumn("school_break", "date", "DATE");
    addColumn("school_break", "classnumber", "INT");
    addColumn("school_break", "time_stamp", "INT", "0");
    addColumn("school_break", "active", "BOOLEAN", "1");
    addColumn("school_break", "uuid", "TEXT");
    // copy content from k_tauko table when upgrade
    if (isTableExists("k_tauko")) {
        addColumn("k_tauko", "id", "INT");
        addColumn("k_tauko", "pvm", "DATE");
        addColumn("k_tauko", "nro", "INT");
        execSql("INSERT INTO school_break "
                "SELECT id,pvm,nro,0,1,NULL "
                "FROM k_tauko");
        deleteTable("k_tauko");
        execSql("UPDATE theocbase_sequence SET name = 'school_break' WHERE name = 'k_tauko'");
    }

    // student_studies table
    // ---------------------
    addColumn("student_studies", "student_id", "INT");
    addColumn("student_studies", "study_number", "INT");
    addColumn("student_studies", "start_date", "DATE");
    addColumn("student_studies", "end_date", "DATE");
    addColumn("student_studies", "exercises", "BOOLEAN", "0");
    addColumn("student_studies", "time_stamp", "INT", "0");
    addColumn("student_studies", "active", "BOOLEAN", "1");
    addColumn("student_studies", "uuid", "TEXT");
    // copy content from tehtavat table when upgrade
    if (isTableExists("tehtavat")) {
        execSql("INSERT INTO student_studies "
                "SELECT idtehtavat,id_henkilo,tutkielma_nro,aloitus,lopetus,harjoitus,0,1,NULL "
                "FROM tehtavat");
        deleteTable("tehtavat");
        execSql("UPDATE theocbase_sequence SET name = 'student_studies' WHERE name = 'tehtavat'");
    }
    // remove errors from school studies
    sql_items studies = selectSql("student_studies", "study_number", "0", "");
    if (!studies.empty())
        removeSql("student_studies", "study_number = 0");

    // studies table
    // -------------
    addColumn("studies", "study_number", "INT");
    addColumn("studies", "study_name", "TEXT");
    addColumn("studies", "reading", "INT");
    addColumn("studies", "demonstration", "INT");
    addColumn("studies", "discource", "INT");
    addColumn("studies", "time_stamp", "INT", "0");
    addColumn("studies", "active", "BOOLEAN", "1");
    addColumn("studies", "uuid", "TEXT");
    // copy content from tutkielmat table when upgrade
    if (isTableExists("tutkielmat")) {
        addColumn("tutkielmat", "luku", "VARCHAR(255)");
        addColumn("tutkielmat", "nayte", "VARCHAR(255)");
        addColumn("tutkielmat", "puhe", "VARCHAR(255)");
        execSql("INSERT INTO studies "
                "SELECT idtutkielmat,idtutkielmat,tutkielma,luku,nayte,puhe,0,1,NULL "
                "FROM tutkielmat");
        deleteTable("tutkielmat");
        execSql("UPDATE theocbase_sequence SET name = 'studies' WHERE name = 'tutkielmat'");
    }

    // school_settings table
    // ---------------------
    addColumn("school_settings", "number", "INT");
    addColumn("school_settings", "name", "INT");
    addColumn("school_settings", "time_stamp", "INT", "0");
    addColumn("school_settings", "active", "BOOLEAN", "1");
    addColumn("school_settings", "uuid", "TEXT");
    // copy content from Asetelmat table when upgrade
    if (isTableExists("Asetelmat")) {
        execSql("INSERT INTO school_settings "
                "SELECT idAsetelmat,idAsetelmat,asetelma,0,1,NULL "
                "FROM Asetelmat");
        deleteTable("Asetelmat");
        execSql("UPDATE theocbase_sequence SET name = 'school_settings' WHERE name = 'Asetelmat'");
    }

    // exceptions table
    // ----------------
    addColumn("exceptions", "type", "INT");
    addColumn("exceptions", "date", "DATE");
    addColumn("exceptions", "date2", "DATE");
    addColumn("exceptions", "schoolday", "INT");
    addColumn("exceptions", "publicmeetingday", "INT");
    addColumn("exceptions", "desc", "TEXT");
    addColumn("exceptions", "time_stamp", "INT", "0");
    addColumn("exceptions", "active", "BOOLEAN", "1");
    addColumn("exceptions", "uuid", "TEXT");
    // change Poikkeukset to exceptions
    if (isTableExists("Poikkeukset")) {
        addColumn("Poikkeukset", "pvm2", "NUMERIC");
        addColumn("Poikkeukset", "schoolday", "NUMERIC");
        addColumn("Poikkeukset", "publicmeetingday", "NUMERIC");
        addColumn("Poikkeukset", "selitys", "VARCHAR(255)");
        execSql("INSERT INTO exceptions "
                "SELECT poikkeukset_id,poikkeus,pvm,pvm2,schoolday,publicmeetingday,selitys,0,1,NULL "
                "FROM Poikkeukset");
        deleteTable("Poikkeukset");
        execSql("UPDATE theocbase_sequence SET name = 'exceptions' WHERE name = 'Poikkeukset'");
    }

    // publictalks table
    // -----------------
    addColumn("publictalks", "theme_number", "INT");
    addColumn("publictalks", "theme_name", "TEXT");
    addColumn("publictalks", "lang_id", "INT");
    addColumn("publictalks", "time_stamp", "INT", "0");
    addColumn("publictalks", "active", "BOOLEAN", "1");
    addColumn("publictalks", "uuid", "TEXT");
    // copy content from Esitelmat table when upgrade
    if (isTableExists("Esitelmat")) {
        addColumn("Esitelmat", "id", "INT");
        addColumn("Esitelmat", "aihe_id", "NUMERIC");
        addColumn("Esitelmat", "aihe", "VARCHAR(255)");
        addColumn("Esitelmat", "kieli_id", "NUMERIC");
        execSql("INSERT INTO publictalks "
                "SELECT id, aihe_id, aihe, kieli_id,0,1,NULL "
                "FROM Esitelmat");
        deleteTable("Esitelmat");
        execSql("UPDATE theocbase_sequence SET name = 'publictalks' WHERE name = 'Esitelmat'");
    }

    // speaker_publictalks table
    // -------------------------
    addColumn("speaker_publictalks", "speaker_id", "INT");
    addColumn("speaker_publictalks", "theme_number", "INT");
    addColumn("speaker_publictalks", "lang_id", "INT");
    addColumn("speaker_publictalks", "time_stamp", "INT", "0");
    addColumn("speaker_publictalks", "active", "BOOLEAN", "1");
    addColumn("speaker_publictalks", "uuid", "TEXT");
    // copy data from P_esitelmat table when upgrade
    if (isTableExists("P_esitelmat")) {
        addColumn("P_esitelmat", "puhuja_id", "NUMERIC");
        addColumn("P_esitelmat", "aihe_id", "NUMERIC");
        addColumn("P_esitelmat", "kieli_id", "NUMERIC");
        execSql("INSERT INTO speaker_publictalks "
                "SELECT id, puhuja_id, aihe_id, kieli_id,0,1,NULL "
                "FROM P_Esitelmat");
        deleteTable("P_Esitelmat");
        execSql("UPDATE theocbase_sequence SET name = 'speaker_publictalks' WHERE name = 'P_Esitelmat'");
    }

    // publicmeeting table
    // -------------------
    addColumn("publicmeeting", "date", "DATE");
    addColumn("publicmeeting", "theme_number", "INT");
    addColumn("publicmeeting", "speaker_id", "INT");
    addColumn("publicmeeting", "wtreader_id", "INT");
    addColumn("publicmeeting", "chairman_id", "INT");
    addColumn("publicmeeting", "cancelled_id", "INT");
    addColumn("publicmeeting", "wt_conductor_id", "INT");
    addColumn("publicmeeting", "wt_source", "TEXT");
    addColumn("publicmeeting", "song_pt", "INT");
    addColumn("publicmeeting", "song_wt_start", "INT");
    addColumn("publicmeeting", "song_wt_end", "INT");
    addColumn("publicmeeting", "time_stamp", "INT", "0");
    addColumn("publicmeeting", "active", "BOOLEAN", "1");
    addColumn("publicmeeting", "uuid", "TEXT");
    // copy data from yleisokokous table
    if (isTableExists("yleisokokous")) {
        addColumn("yleisokokous", "pvm", "NUMERIC");
        addColumn("yleisokokous", "aihe_id", "NUMERIC");
        addColumn("yleisokokous", "puhuja_id", "NUMERIC");
        addColumn("yleisokokous", "vt_lukija_id", "NUMERIC");
        addColumn("yleisokokous", "puheenjohtaja_id", "NUMERIC");
        addColumn("yleisokokous", "peruutettu_id", "NUMERIC");
        addColumn("yleisokokous", "wt_conductor_id", "NUMERIC");
        addColumn("yleisokokous", "wt_source", "VARCHAR(255)");
        addColumn("yleisokokous", "song_pt", "NUMERIC");
        addColumn("yleisokokous", "song_wt_start", "NUMERIC");
        addColumn("yleisokokous", "song_wt_end", "NUMERIC");
        execSql("INSERT INTO publicmeeting "
                "SELECT id,pvm,aihe_id,puhuja_id,vt_lukija_id,puheenjohtaja_id,peruutettu_id,wt_conductor_id,wt_source,song_pt,song_wt_start,song_wt_end,0,1,NULL "
                "FROM yleisokokous");
        deleteTable("yleisokokous");
        execSql("UPDATE theocbase_sequence SET name = 'publicmeeting' WHERE name = 'yleisokokous'");
    }

    // outgoing speakers table
    // -----------------------
    addColumn("outgoing", "id", "INT");
    addColumn("outgoing", "date", "NUMERIC");
    addColumn("outgoing", "speaker_id", "NUMERIC");
    addColumn("outgoing", "congregation_id", "NUMERIC");
    addColumn("outgoing", "theme_id", "NUMERIC");
    addColumn("outgoing", "time_stamp", "INT", "0");
    addColumn("outgoing", "active", "BOOLEAN", "1");
    addColumn("outgoing", "uuid", "TEXT");

    // congregations table
    // -------------------
    addColumn("congregations", "name", "VARCHAR(255)");
    addColumn("congregations", "address", "VARCHAR(255)");
    addColumn("congregations", "meeting1_time", "VARCHAR(255)");
    addColumn("congregations", "meeting2_time", "VARCHAR(255)");
    addColumn("congregations", "circuit", "VARCHAR(255)");
    addColumn("congregations", "info", "VARCHAR(255)");
    addColumn("congregations", "time_stamp", "INT", "0");
    addColumn("congregations", "active", "BOOLEAN", "1");
    addColumn("congregations", "uuid", "TEXT");
    // copy contents from seurakunnat table when upgrade
    if (isTableExists("seurakunnat")) {
        addColumn("seurakunnat", "s_nimi", "VARCHAR(255)");
        addColumn("seurakunnat", "address", "VARCHAR(255)");
        addColumn("seurakunnat", "meeting1_time", "VARCHAR(255)");
        addColumn("seurakunnat", "meeting2_time", "VARCHAR(255)");
        addColumn("seurakunnat", "circuit", "VARCHAR(255)");
        addColumn("seurakunnat", "info", "VARCHAR(255)");
        execSql("INSERT INTO congregations "
                "SELECT id,s_nimi,address,meeting1_time,meeting2_time,circuit,info,0,1,NULL "
                "FROM seurakunnat");
        deleteTable("seurakunnat");
        execSql("UPDATE theocbase_sequence SET name = 'congregations' WHERE name = 'Poikkeukset'");
    }

    // languages table
    //----------------
    addColumn("languages", "language", "VARCHAR(255)");
    addColumn("languages", "code", "VARCHAR(255)");
    addColumn("languages", "time_stamp", "INT", "0");
    addColumn("languages", "active", "BOOLEAN", "1");
    addColumn("languages", "uuid", "TEXT");
    // language codes
    QList<QPair<QString, QString>> list;
    list.append(qMakePair(QString("Finnish"), QString("fi")));
    list.append(qMakePair(QString("English"), QString("en")));
    list.append(qMakePair(QString("Russian"), QString("ru")));
    list.append(qMakePair(QString("Hungarian"), QString("hu")));
    list.append(qMakePair(QString("Portuguese"), QString("pt")));
    list.append(qMakePair(QString("Greek"), QString("el")));
    list.append(qMakePair(QString("German"), QString("de")));
    list.append(qMakePair(QString("French"), QString("fr")));
    list.append(qMakePair(QString("Dutch"), QString("nl")));
    list.append(qMakePair(QString("Italian"), QString("it")));
    list.append(qMakePair(QString("Spanish"), QString("es")));
    list.append(qMakePair(QString("Estonian"), QString("et")));
    list.append(qMakePair(QString("Romanian"), QString("ro")));
    list.append(qMakePair(QString("Afrikaans"), QString("af")));
    list.append(qMakePair(QString("Swedish"), QString("sv")));
    list.append(qMakePair(QString("Croatian"), QString("hr")));
    list.append(qMakePair(QString("Polish"), QString("pl")));
    list.append(qMakePair(QString("Danish"), QString("da")));
    list.append(qMakePair(QString("Slovak"), QString("sk")));
    for (int i = 0; i < list.size(); ++i) {
        updateTable("languages", "language", list[i].first);
        sql_item s;
        s.insert("code", list[i].second);
        updateSql("languages", "language", list[i].first, &s);
    }
    // remove old 'kieli' table
    if (isTableExists("kieli"))
        deleteTable("kieli");

    // biblestudyMeeting table
    // -----------------------
    addColumn("biblestudyMeeting", "id", "NUMERIC");
    addColumn("biblestudyMeeting", "song", "NUMERIC");
    addColumn("biblestudyMeeting", "material", "VARCHAR(255)");
    addColumn("biblestudyMeeting", "date", "DATE");
    addColumn("biblestudyMeeting", "person_id", "NUMERIC");
    addColumn("biblestudyMeeting", "reader_id", "NUMERIC");
    addColumn("biblestudyMeeting", "prayer_id", "NUMERIC");
    addColumn("biblestudyMeeting", "time_stamp", "INT", "0");
    addColumn("biblestudyMeeting", "active", "BOOLEAN", "1");
    addColumn("biblestudyMeeting", "uuid", "TEXT");

    // serviceMeeting table
    // --------------------
    addColumn("serviceMeeting", "id", "NUMERIC");
    addColumn("serviceMeeting", "date", "DATE");
    addColumn("serviceMeeting", "song_middle", "NUMERIC");
    addColumn("serviceMeeting", "song_end", "NUMERIC");
    addColumn("serviceMeeting", "prayer_id", "NUMERIC");
    addColumn("serviceMeeting", "time_stamp", "INT", "0");
    addColumn("serviceMeeting", "active", "BOOLEAN", "1");
    addColumn("serviceMeeting", "uuid", "TEXT");

    // serviceProgram table
    // --------------------
    addColumn("serviceProgram", "id", "NUMERIC");
    addColumn("serviceProgram", "meeting_id", "NUMERIC");
    addColumn("serviceProgram", "program_id", "NUMERIC");
    addColumn("serviceProgram", "person_id", "NUMERIC");
    addColumn("serviceProgram", "theme", "VARCHAR(255)");
    addColumn("serviceProgram", "time", "NUMERIC");
    addColumn("serviceProgram", "time_stamp", "INT", "0");
    addColumn("serviceProgram", "active", "BOOLEAN", "1");
    addColumn("serviceProgram", "uuid", "TEXT");

    // unavailables table
    // ------------------
    addColumn("unavailables", "person_id", "INT");
    addColumn("unavailables", "start_date", "DATE");
    addColumn("unavailables", "end_date", "DATE");
    addColumn("unavailables", "time_stamp", "INT", "0");
    addColumn("unavailables", "active", "BOOLEAN", "1");
    addColumn("unavailables", "uuid", "TEXT");
    // copy content from poissaolot table when upgrade
    if (isTableExists("poissaolot")) {
        addColumn("poissaolot", "henkilo_id", "NUMERIC");
        addColumn("poissaolot", "aloitus", "DATE");
        addColumn("poissaolot", "lopetus", "DATE");
        execSql("INSERT INTO unavailables "
                "SELECT id,henkilo_id,aloitus,lopetus,0,1,NULL "
                "FROM poissaolot");
        deleteTable("poissaolot");
        execSql("UPDATE theocbase_sequence SET name = 'unavailables' WHERE name = 'poissaolot'");
    }

    // email reminders table
    // ---------------------
    addColumn("e_reminder", "id", "NUMERIC");
    addColumn("e_reminder", "type", "NUMERIC");
    addColumn("e_reminder", "object_id", "NUMERIC");
    addColumn("e_reminder", "user_id", "NUMERIC");
    addColumn("e_reminder", "counter", "NUMERIC");
    addColumn("e_reminder", "time_stamp", "INT", "0");
    addColumn("e_reminder", "active", "BOOLEAN", "1");
    addColumn("e_reminder", "uuid", "TEXT");

    // history views
    createView("schoolhistory", "SELECT school.date as date, school.student_id as person_id, school.volunteer_id as volunteer_id, school_schedule.number as type, school.classnumber as class "
                                "FROM school, school_schedule WHERE school.school_schedule_id=school_schedule.id");
    createView("servicemeetinghistory", "SELECT serviceMeeting.date, serviceProgram.person_id, serviceProgram.time as type "
                                        "FROM serviceMeeting LEFT JOIN serviceProgram ON serviceProgram.meeting_id = serviceMeeting.id");
    createView("publicmeetinghistory", "SELECT publicmeeting.date as date, publicmeeting.theme_number as theme_id,publictalks.theme_name as theme, "
                                       "publicmeeting.speaker_id as speaker_id, publicmeeting.chairman_id as chairman_id, "
                                       "publicmeeting.wtreader_id as wtreader_id "
                                       "FROM publicmeeting LEFT JOIN publictalks ON publicmeeting.theme_number = publictalks.theme_number AND publictalks.lang_id = "
                                       "(SELECT id FROM languages WHERE code = (SELECT value FROM settings WHERE name = 'theocbase_language') )");

    // delete old tables
    deleteTable("k_asetelma");
    deleteTable("alueet");
    deleteTable("H_oikeudet");
    deleteTable("peruutettu");
    deleteTable("ryhman_esitelmat");
    deleteTable("t_oik");
    deleteTable("sunnuntai");
    deleteTable("peruutettu");
    deleteTable("ryhman_esitelmat");
    deleteTable("soveltuvuus");
    deleteTable("puhujat");
    deleteTable("ministryServices");

    // my congregation
    if (this->getSetting("congregation_id") == "") {
        // ask congreagation

        QStringList items;
        sql_items s = selectSql("congregations", "", "", "");
        for (unsigned int i = 0; i < s.size(); ++i) {
            items << s[i].value("name").toString();
        }

        bool ok;
        QString item = QInputDialog::getItem(0, QObject::tr("Congregation"),
                                             QObject::tr("Congregation"), items, 0, true, &ok);
        if (item.isEmpty()) {
            item = "-";
        }
        int srkid = 1;
        if (items.indexOf(item) < 0) {
            // create a new congregation
            sql_item sitem;
            sitem.insert("name", item);
            srkid = insertSql("congregations", &sitem, "id");
        } else {
            for (unsigned int i = 0; i < s.size(); ++i) {
                if (s[i].value("name").toString() == item) {
                    srkid = s[i].value("id").toInt();
                    break;
                }
            }
        }
        saveSetting("congregation_id", QString::number(srkid));
    }

    return false;
}

int sql_class::runDatabaseBatch(QString resourcepath)
{
    // database batch file
    QFile f(resourcepath);
    QFileInfo fi(f);

    if (fi.exists()) {
        // found
#ifndef NOSQLDUMP
        qDebug() << "batch found! (" + f.fileName() + ")";
#endif
        bool hasError(false);
        bool skipNext(false);
        bool versionIgnoreActive(false);
        if (f.open(QIODevice::ReadOnly | QIODevice::Text)) {
            startTransaction();
            QTextStream in(&f);
            QString okNextError = QString();
            while (!in.atEnd() && !hasError) {
                QString line = in.readLine();
#ifndef NOSQLDUMP
                qDebug() << "line=" + line;
#endif
                if (line.startsWith("#ifversion", Qt::CaseInsensitive)) {
                    QRegularExpression parse("\\s*([<>=]+)\\s*(.+)");
                    QRegularExpressionMatch match = parse.match(line.midRef(10));
                    if (match.hasMatch()) {
                        QString v = getSetting("appver_old");
                        QString compOp("< > = <=>=");
                        QString comp(match.captured(2).trimmed());
                        switch (compOp.indexOf(match.captured(1)) / 2) {
                        case 0:
                            versionIgnoreActive = !(v < comp);
                            break;
                        case 1:
                            versionIgnoreActive = !(v > comp);
                            break;
                        case 2:
                            versionIgnoreActive = !(v == comp);
                            break;
                        case 3:
                            versionIgnoreActive = !(v <= comp);
                            break;
                        case 4:
                            versionIgnoreActive = !(v >= comp);
                            break;
                        default:
                            break;
                        }
                    }
                } else if (line.startsWith("#endversion", Qt::CaseInsensitive)) {
                    versionIgnoreActive = false;
                } else if (!versionIgnoreActive) {
                    if (skipNext) {
                        skipNext = false;
                    } else if (line.startsWith("#okNextError")) {
                        okNextError = line.mid(12).trimmed(); // this is used when running commands that we are ok with getting an error
                    } else if (line == "#updateMeetingTimes") {
                        updateThisYearMeetingTimes();
                        updateNextYearMeetingTimes();
                    } else if (line.startsWith("#setuuid", Qt::CaseInsensitive)) {
                        if (fillAllUUID(line.mid(8).trimmed(), "id"))
                            return true;
                    } else if (line.startsWith("#setting", Qt::CaseInsensitive)) {
                        int eq(line.indexOf("="));
                        if (eq > 0) {
                            saveSetting(line.mid(8, eq - 8).trimmed(), line.mid(eq + 1).trimmed());
                        }
                    } else if (line.startsWith("#ifexists", Qt::CaseInsensitive)) {
                        QVariant x(selectScalar(line.mid(9), nullptr));
                        skipNext = !x.isValid();
                    } else if (line.startsWith("#ifnotexists", Qt::CaseInsensitive)) {
                        QVariant x(selectScalar(line.mid(12), nullptr));
                        skipNext = x.isValid();
                    } else if (line.startsWith("#renamecolumn", Qt::CaseInsensitive)) {
                        // format:  #renamecolumn tablename.columnName = newColumnName
                        QRegularExpression rcParse("\\s*([^.]+)[.]([^\\s=]+)\\s*=\\s*(.+)");
                        QRegularExpressionMatch rcMatch = rcParse.match(line.midRef(14));
                        if (rcMatch.hasMatch()) {
                            hasError = renameColumn(rcMatch.captured(1), rcMatch.captured(2), rcMatch.captured(3));
                        } else {
                            hasError = true;
                        }
                    } else if (line.length() > 0) {
                        if (line.contains("{timestamp}"))
                            line = line.replace("{timestamp}", ("time_stamp = " + QVariant(getCurrentTimeStamp()).toString()));
                        if (!execSql(line, okNextError))
                            hasError = true; // rollback transaction also?
                        if (!okNextError.isNull())
                            okNextError = QString();
                    }
                }
            }
            commitTransaction();
        }
        return hasError;
    } else {
        QMessageBox::warning(nullptr, "Database Update Error", "Error! File not found: " + fi.filePath());
        return true;
    }
}

bool sql_class::renameColumn(QString tableName, QString columnName, QString newColumnName)
{
    // grab original CREATE query
    sql_item queryvalue;
    queryvalue.insert(":tablename", tableName);
    QString createSQL = selectScalar("SELECT sql FROM sqlite_master WHERE name = :tablename", &queryvalue).toString();

    // rename old table
    QString temptablename(tableName + "_toberenamed");
    if (!execSql("ALTER TABLE " + tableName + " RENAME TO " + temptablename))
        return true;

    // create table again with new column name with a simple replace
    // NOTE: the columnName must be fairly unique (this won't work if the old column name is partially found in another column name)
    createSQL = createSQL.replace(columnName, newColumnName, Qt::CaseInsensitive);
    if (!execSql(createSQL))
        return true;

    // fill new table with old
    if (!execSql("insert into " + tableName + " select * from " + temptablename))
        return true;
    // drop old table
    if (!execSql("drop table " + temptablename))
        return true;

    return false;
}

bool sql_class::fillAllUUID(QString tablename, QString idname)
{
    // touches all records in tablename and sets new UUIDs

    sql_items ids = selectSql("select " + idname + " from " + tablename, NULL);
    QSqlQuery query(*db);
    sql_item *values = new sql_item();
    for (sql_item i : ids) {
        values->insert(":" + idname, i[idname]);
        QString str = "update " + tablename + " set uuid = '" + getUuid() + "' where " + idname + " = :" + idname;
        query.prepare(str);
        bindQueryValues(&query, values);
        query.exec();
        if (query.lastError().type() != QSqlError::NoError) {
            QMessageBox::critical(NULL, QString("insertSql failed!"),
                                  query.lastError().text());
            QMessageBox::critical(NULL, QString("sql"), str);
            return true;
        }
    }
    return false;
}

bool sql_class::addColumn(QString tablename, QString columnname, QString type, QString defaultvalue)
{
    QSqlQuery query(*db);
    query.clear();
    query.prepare("CREATE TABLE IF NOT EXISTS " + tablename + "(id INTEGER PRIMARY KEY)");
    query.exec();
    //QSqlQuery query;
    query.clear();
    query.prepare("SELECT * FROM " + tablename + " LIMIT 1");
    query.first();
    int test = query.record().indexOf(columnname);
    if (test < 0) {
        QString sql = "ALTER TABLE " + tablename + " ADD COLUMN " + columnname + " " + type;
        if (defaultvalue != "")
            sql += " DEFAULT " + defaultvalue;
        query.clear();
        query.prepare(sql);
        query.exec();
    }

    return true;
}

bool sql_class::deleteTable(QString tablename, QString deleteifcolumnexists)
{

    if (deleteifcolumnexists != "") {
        // poistetaan vain jos sarake olemassa
        QSqlQuery checkq(*db);
        checkq.clear();
        checkq.prepare("SELECT * FROM '" + tablename + "' LIMIT 1");
        checkq.exec();
        if (checkq.record().count() > 0) {
            int test = checkq.record().indexOf(deleteifcolumnexists);
            if (test < 0)
                return false;
        }
    }

    QSqlQuery query(*db);
    query.clear();
    query.prepare("DROP TABLE IF EXISTS " + tablename);
    query.exec();

    return true;
}
bool sql_class::updateTable(QString tablename, QString field, QString value)
{
    sql_item kielet;
    kielet.insert(field, value);
    sql_items check = selectSql(tablename, field, value, "");
    if (check.size() < 1) {
        QSqlQuery query(*db);
        query.clear();
        insertSql(tablename, &kielet, "id");
        query.exec();
    }

    return true;
}

void sql_class::closeConnection()
{
    QString connection = db->connectionName();
    db->close();
    delete db;
    QSqlDatabase::removeDatabase(connection);
    //db.close();
    //db.removeDatabase("theocbase");
}

void sql_class::startTransaction()
{
    QSqlQuery query(*db);
    bool ok = query.exec("BEGIN TRANSACTION");
    //bool ok = db->transaction();
#ifndef NOSQLDUMP
    qDebug() << "transaction started " << QVariant(ok).toString();
#endif
    if (ok)
        transactionStarted = true;
}

void sql_class::commitTransaction()
{
    QSqlQuery query(*db);
    bool ok = query.exec("COMMIT");
    //bool ok = db->commit();
#ifndef NOSQLDUMP
    qDebug() << "transaction commit " << QVariant(ok).toString();
#endif
    transactionStarted = false;
}

void sql_class::rollbackTransaction()
{
    QSqlQuery query(*db);
    bool ok = query.exec("ROLLBACK");
    //bool ok = db->rollback();
#ifndef NOSQLDUMP
    qDebug() << "transaction rollbacked " << QVariant(ok).toString();
#endif
    transactionStarted = false;
}

void sql_class::createView(QString viewname, QString select)
{
    QSqlQuery query(*db);
    query.clear();
    query.prepare("DROP VIEW IF EXISTS " + viewname);
    query.exec();
    query.clear();
    QString sqlstring = "CREATE VIEW " + viewname + " AS " + select;
#ifndef NOSQLDUMP
    qDebug() << "create view: " << sqlstring;
#endif
    query.prepare(sqlstring);
    query.exec();
}

bool sql_class::isTableExists(QString tablename)
{
    QSqlQuery query(*db);
    query.clear();
    query.prepare(QString("SELECT name FROM sqlite_master WHERE type='table' AND name='%1'").arg(tablename));
    query.exec();
    if (query.first()) {
        return true;
    } else {
        return false;
    }
}

void sql_class::bindQueryValues(QSqlQuery *query, sql_item *values, bool valueAssistant)
{
    if (values)
        for (sql_item::const_iterator i = values->constBegin(); i != values->constEnd(); ++i) {
            QString key = i.key().startsWith(':') ? i.key() : ":" + i.key();
            QVariant val = i.value();
            if (valueAssistant) {
                if (key == ":time_stamp")
                    val = getCurrentTimeStamp();
                // the lines below aren't useful for inserts that have more than one record.
                //if (key == ":uuid") val = getUuid();
                //if (key == ":next_id") val = getNextId(values->value(":table").toString(), values->value(":tableid").toString());
            }
            query->bindValue(key, val);
        }
}

QList<QPair<int, QString>> sql_class::getLanguages()
{
    // return all languages <id,name>

    QList<QPair<int, QString>> lang;

    sql_items kielet;
    kielet = selectSql("SELECT * FROM languages ORDER BY desc", NULL);
    if (!kielet.empty()) {
        for (unsigned int i = 0; i < kielet.size(); i++) {
            sql_item s = kielet[i];
            lang.append(qMakePair(s.value("id").toInt(), s.value("desc").toString()));
        }
    }
    return lang;
}

int sql_class::getLanguageDefaultId()
{
    sql_items l;
    l = selectSql("SELECT * FROM languages WHERE code = '" + this->getSetting("theocbase_language") + "'", NULL);
    int id = 0;
    if (!l.empty()) {
        id = l[0].value("id").toInt();
    }
    return id;
}

QString sql_class::getLanguageCode(int id)
{
    sql_items l;
    l = selectSql("SELECT * FROM languages WHERE id = '" + QVariant(id).toString() + "'", NULL);
    QString code = "";
    if (!l.empty()) {
        code = l[0].value("code").toString();
    }
    return code;
}

int sql_class::getLanguageId(QString code)
{
    sql_items l;
    l = selectSql("SELECT * FROM languages WHERE code = '" + code + "'", NULL);
    int id = 0;
    if (!l.empty()) {
        id = l[0].value("id").toInt();
    }
    return id;
}

QString sql_class::EscapeQuotes(QString sql)
{
    return sql.replace("'", "''");
}

void sql_class::copyDatabase()
{
    QString exportpath = databasepath + "." + QDateTime::currentDateTime().toString("yyyyMMddhhmmss");
#ifndef NOSQLDUMP
    qDebug() << "database copy path" << exportpath;
#endif
    QFile::copy(databasepath, exportpath);
}

void sql_class::clearDatabase()
{
    QStringList tables;
    tables << "biblestudymeeting"
           << "congregationmeetingtimes"
           << "congregations"
           << "e_reminder"
           << "exceptions"
           << "families"
           << "lmm_assignment"
           << "lmm_meeting"
           << "lmm_schedule"
           << "lmm_schedule_assist"
           << "lmm_studies"
           << "outgoing"
           << "persons"
           << "publicmeeting"
           << "publictalks"
           << "publictalks_todolist"
           << "school"
           << "school_break"
           << "school_schedule"
           << "school_settings"
           << "servicemeeting"
           << "serviceprogram"
           << "song"
           << "speaker_publictalks"
           << "student_studies"
           << "studies"
           << "territory"
           << "territory_street"
           << "territory_streettype"
           << "territory_address"
           << "territory_addresstype"
           << "territory_assignment"
           << "territory_city"
           << "territory_type"
           << "unavailables"
           << "wt_article_dates";
    sql_class *sql = &Singleton<sql_class>::Instance();
    for (QString tablename : tables) {
        sql->execSql("DELETE FROM " + tablename);
    }
    sql->execSql("DELETE FROM settings WHERE name = 'schools_qty' OR name = 'school_day' OR name = 'circuitoverseer'");
    // add default congregation
    sql->execSql("INSERT INTO congregations (id,name,uuid, time_stamp) VALUES (1, '-', '5590acc3-48b2-4f2a-a284-4c03da210d33',-1)");
    sql->saveSetting("congregation_id", "1");
    sql->saveSetting("last_dbsync_id", "0");
    sql->saveSetting("last_dbsync_time", "-1");

    cachedSettings.clear();
}
