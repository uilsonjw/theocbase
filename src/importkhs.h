/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2019, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMPORTKHS_H
#define IMPORTKHS_H
#include <QString>
#include <QTextCodec>
#include "ccongregation.h"
#include "person.h"
#include "cpublictalks.h"

class importKHS
{
public:
    importKHS(QString directory);
    void Go();

private:
    /**
     * @brief DBF header struct
     */
    struct DBFHeader {
        char version;
        char lastUpdate[3];
        uint32_t numRecords;
        uint16_t headerSize;
        uint16_t recordSize;
        char reserved[16];
        char tableFlags;
        char codePageMark;
        char reserved2[2];
    };
    /**
     * @brief DBF field descriptor struct
     */
    struct DBFFieldDescriptor {
        char fieldName[11];
        char fieldType;
        uint32_t fieldOffset;
        uint8_t fieldLength;
        char decimalPlaces;
        char fieldFlags;
        char autoincrementNextValue[4];
        char autoincrementStep;
        char reserved[8];
    };

    /**
     * @brief Simple class to read a DBF file
     */
    class DBFFile
    {
    public:
        DBFFile(QString fileName);
        DBFHeader header;
        QMap<QString, DBFFieldDescriptor> fields;
        QList<QMap<QString, QVariant>> records;

    private:
        QMap<QString, QVariant> getFieldData(const char *recordData) const;
    };

    bool importCongregations();
    bool importNames();
    bool importSpeakers();
    bool importOutlines(int languageId);
    bool importTalklist(int languageId);
    bool importSchedule();
    bool importOutgoing();

    sql_class *sql;

    /**
     * @brief m_directory - directory of KHS data selected by user
     */
    QDir m_directory;

    QMap<QString, int> congregationFields;
    /**
     * @brief congOldIdToNewID - key is KHS congregation ID, value is our congregation ID
     */
    QHash<int, int> congOldIdToNewId;

    /**
     * @brief namesOldIdToNewID - key is KHS names ID, value is our person ID
     */
    QHash<int, int> namesOldIdToNewId;

    /**
     * @brief speakersOldIdToNewID - key is KHS speakers ID, value is our person ID
     */
    QHash<int, int> speakersOldIdToNewId;

    /**
     * @brief outlinesOldIdToNewID - key is KHS outlines ID, value is our publictalks ID
     */
    QHash<int, int> outlinesOldIdToNewId;
};

#endif // IMPORTKHS_H
