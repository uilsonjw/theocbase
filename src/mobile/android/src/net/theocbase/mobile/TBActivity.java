/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.theocbase.mobile;
import android.content.Intent;
import android.util.Log;
import android.net.Uri;
import org.qtproject.qt5.android.bindings.QtActivity;
import org.qtproject.qt5.android.bindings.QtApplication;

import android.app.Activity;
import java.util.Locale;
import java.lang.String;
import android.content.Context;
import android.os.Bundle;
import java.net.URL;
import android.content.ContentResolver;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;

import android.app.AlertDialog;
import android.support.customtabs.CustomTabsClient;
import android.support.customtabs.CustomTabsIntent;
import android.support.customtabs.CustomTabsServiceConnection;
import android.support.customtabs.CustomTabsSession;
import android.webkit.MimeTypeMap;
import android.database.Cursor;
import android.provider.OpenableColumns;

public class TBActivity extends QtActivity {

    public static native void setUrlReceived(String url);

    private static TBActivity m_instance;
    private static Uri pendingUri;

    public TBActivity()
    {
        Log.d("TBActivity::TBActivity", "Constructor");
        m_instance = this;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("onCreate", "onCreate");
        pendingUri = null;
        Intent i = getIntent();
        if (Intent.ACTION_VIEW.equals(i.getAction())) {
            Log.d(i.getData().toString(), "onCreate");

            Uri uri = i.getData();
            //readFile(uri);
            // QML / Qt is not yet ready
            pendingUri = uri;
        }

    }

    @Override
    public void onNewIntent(Intent i) {
        super.onNewIntent(i);
        pendingUri = null;
        if (Intent.ACTION_VIEW.equals(i.getAction())) {
            Log.d(i.getData().toString(),"onNewIntent");
            Uri uri = i.getData();

            pendingUri = uri;
        }
    }

    public void readFile(Uri uri)
    {
        ContentResolver cR = getContentResolver();
        try {
            InputStream iStr = cR.openInputStream(uri);
            if (iStr != null) {                
                Cursor cur = cR.query(uri, null, null, null, null);
                int nameIndex = cur.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                cur.moveToFirst();
                String name = cur.getString(nameIndex);
                cur.close();
                File file = new File(this.getCacheDir(), name);
                //String extension = getFileExtension(uri);
                //File file = File.createTempFile("tmp", "." + extension, this.getCacheDir());
                FileOutputStream output = new FileOutputStream(file);
                byte[] buffer = new byte[1024];
                int len;
                while ((len=iStr.read(buffer)) != -1) {
                    output.write(buffer,0,len);
                }
                output.flush();
                output.close();
                iStr.close();
                setUrlReceived(Uri.fromFile( file ).toString());
            }
        } catch (FileNotFoundException ex_f) {
            Log.d(ex_f.getMessage(), "readFile:FileNotFoundException");
        } catch (IOException ex_i) {
            Log.d(ex_i.getMessage(), "readFile:IOException");
        }
    }

    public static void checkReceivedUrl()
    {
        if (pendingUri != null) {
            if (pendingUri.getScheme().equals("theocbase")) {
                Log.d("custom scheme 'theocbase'", "onNewIntent");
                m_instance.setUrlReceived(pendingUri.toString());
            } else if (pendingUri.getScheme().equals("file") || pendingUri.getScheme().equals("content")) {
                m_instance.readFile(pendingUri);
            }
            pendingUri = null;
        }
    }

    public static String myTest()
    {
        return "This is from java class";
    }

    public static int myTest2(int n)
    {
        return n;
    }

    public static void sendMail(String attachmentFilename)
    {
        Log.d("TBActivity::sendMail", "java-code: sendMail(): 1 file=" + attachmentFilename);
        Log.d("TBActivity::sendMail", "java-code: m_instance=" + m_instance);
        String f = "file://" + attachmentFilename;
        Uri uri = Uri.parse(f);

        try
        {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setType("message/rfc822");
            intent.putExtra(Intent.EXTRA_SUBJECT, "Theocbase Mobile backup");
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"abc@gmail.com"});
            intent.putExtra(Intent.EXTRA_STREAM, uri);
            Log.d("TBActivity::sendMail", "java-code: sendMail(): 2");
            Intent mailer = Intent.createChooser(intent, "Send mail...");
            Log.d("TBActivity::sendMail", "java-code: sendMail(): 3");
            if(mailer == null)
            {
                Log.e("TBActivity::sendMail", "Couldn't get Mail Intent");
                return;
            }

            Log.d("TBActivity", "java-code: mailer    =" + mailer);
            Log.d("TBActivity", "java-code: m_instance=" + m_instance);

            m_instance.startActivity(mailer);
            //m_instance.getApplicationContext().startActivity(mailer);
        }
        catch (android.content.ActivityNotFoundException ex)
        {
            Log.e("TBActivity", "catched android.content.ActivityNotFoundException while starting activity");
            ex.printStackTrace();
        }

        Log.d("TBActivity", "java-code: notify(): END");
    }

    public static void saveFile(String url)
    {
        Uri uri = Uri.parse(url);
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        intent.setType("text/xml");

        m_instance.startActivity(intent);
    }

    public static void shareText(String shareText)
    {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareText);
        m_instance.startActivity(Intent.createChooser(sharingIntent, "Send reminder..."));
    }

    public static void loginDropbox(String url)
    {
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl(m_instance,Uri.parse(url));
    }

    public static void openFileBrowser(String format)
    {
        Log.d("TBActivity", "open file browser");

        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        String mimeType;
        if (format.equals("epub"))
            mimeType = "application/epub+zip";
        else
            mimeType = "*/*";
        i.setType(mimeType);
        m_instance.startActivityForResult(i, 8778);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        Log.d("TBActivity", "onActivityResult");
        if (resultCode == 0) {
            Log.d("activity cancelled", "");
        } else if (requestCode == 8778) {
            Uri uri = data.getData();
            Log.d("file selected", uri.toString());

            readFile(uri);
        }
    }

    private String getFileExtension(Uri uri) {
        ContentResolver cr = getContentResolver();
        String extension = null;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            Log.d("getFileExtension", "scheme content");
            final MimeTypeMap mime = MimeTypeMap.getSingleton();
            extension = mime.getExtensionFromMimeType(cr.getType(uri));
        }
        if (extension == null || extension.isEmpty()) {
            Log.d("getFileExtension", uri.getPath());
            extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(uri.getPath())).toString());
        }
        if (extension == null)
            extension = "";
        Log.d("extension", extension);
        return extension;
    }
}
