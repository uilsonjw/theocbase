#include "internet.h"

internet::internet() { }

QNetworkReply* internet::download(QUrl url, int timeoutSeconds)
{
    QEventLoop q;
    QTimer timer;
    lastErr.clear();

    timer.setSingleShot(true);
    QObject::connect(&timer, SIGNAL(timeout()), &q, SLOT(quit()));
    QObject::connect(&manager, SIGNAL(finished(QNetworkReply*)),
            &q, SLOT(quit()));

    request.setUrl(url);
    QNetworkReply* reply = manager.get(request);

    timer.start(timeoutSeconds * 1000);
    q.exec();

    if(timer.isActive()) {
        // download complete
        timer.stop();
        if (reply->error() != QNetworkReply::NoError){
            lastErr = reply->errorString();
            qDebug() << "internet::download error" << lastErr;
        } else {
            return reply;
        }
    } else {
        lastErr = "timeout waiting for reply";
    }
    return nullptr;
}
