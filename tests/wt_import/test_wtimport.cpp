#include <QtTest>
#include <QTest>
#include <QDebug>
#include "../common/common.h"
#include "sql_class.h"
#include "wtimport.h"

class test_wtimport : public QObject
{
    Q_OBJECT

public:
    test_wtimport();
    ~test_wtimport();

private slots:
    void initTestCase();
    void cleanupTestCase();
    void test_import1_data();
    void test_import1();

};

test_wtimport::test_wtimport()
{
}

test_wtimport::~test_wtimport()
{
}

void test_wtimport::initTestCase()
{
    common::initDataBase();
}

void test_wtimport::cleanupTestCase()
{
    common::clearDatabase();
}

void test_wtimport::test_import1_data()
{
    // Columns
    QTest::addColumn<QString>("filename");
    QTest::addColumn<QDate>("firstdate");
    QTest::addColumn<int>("firstarticle");
    QTest::addColumn<int>("weeks");
    QTest::addColumn<QString>("downloadlink");

    // Rows
    QTest::addRow("July 2019 en") << "w_E_201907.epub" << QDate(2019, 9, 2) << 27 << 4 << "https://download-a.akamaihd.net/files/media_periodical/31/w_E_201907.epub";
    QTest::addRow("July 2019 fi") << "w_FI_201907.epub" << QDate(2019, 9, 2) << 27 << 4 << "https://download-a.akamaihd.net/files/media_periodical/db/w_FI_201907.epub";
    QTest::addRow("November 2019 en") << "w_E_201911.epub" << QDate(2019, 12, 30) << 44 << 5 << "https://download-a.akamaihd.net/files/media_periodical/b4/w_E_201911.epub";
    QTest::addRow("August 2021 en") << "w_E_202108" << QDate(2021, 9, 27) << 30 << 5 << "https://download-a.akamaihd.net/files/media_periodical/53/w_E_202108.epub";
}

void test_wtimport::test_import1()
{
    QFETCH(QString, filename);
    QFETCH(QDate, firstdate);
    QFETCH(int, firstarticle);
    QFETCH(int, weeks);
    QFETCH(QString, downloadlink);

    QString testPath = QT_TESTCASE_BUILDDIR;
    QString srcDir = SRCDIR;
    QVERIFY(common::downloadTestFile(filename, downloadlink, srcDir));
    QDate lastdate = firstdate.addDays(7 * (weeks - 1));
    QString resultText = QString("Imported %1 weeks from %2 thru %3").arg(weeks).arg(
                firstdate.toString(Qt::DefaultLocaleShortDate), lastdate.toString(Qt::DefaultLocaleShortDate));

    wtimport wt;
    QString epubFile = QFINDTESTDATA(filename);
    QString ret = wt.importFile(epubFile);    
    QCOMPARE(ret, resultText);
    sql_class *sql = &Singleton<sql_class>::Instance();
    sql_item params;
    params.insert(":date1", firstdate.toString(Qt::ISODate));
    params.insert(":date2", lastdate.toString(Qt::ISODate));
    sql_items items = sql->selectSql("select * from publicmeeting where date >= :date1 and date <= :date2 order by date", &params);
    QCOMPARE(items.size(), weeks);
    int article = firstarticle;
    QDate dateCheck(firstdate);
    for (auto& item : items) {
        // Check date
        QCOMPARE(item.value("date"), dateCheck);
        dateCheck = dateCheck.addDays(7);
        // Check article number
        QVERIFY(item.value("wt_source").toString().endsWith("/" + QString::number(article)));
        article++;
    }
}

QTEST_MAIN(test_wtimport)

#include "test_wtimport.moc"
