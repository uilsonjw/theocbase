#include "weekinfo.h"

WeekInfo::WeekInfo(QObject *parent) : QObject(parent)
{
    c.clearExceptionCache();
    m_exception = ccongregation::None;
    m_midweekDay = 0;
    m_weekendDay = 0;
}

QDate WeekInfo::date() const
{
    return m_date;
}

QString WeekInfo::exceptionText() const
{
    return m_exceptionText;
}

ccongregation::exceptions WeekInfo::exception() const
{
    return m_exception;
}

QDate WeekInfo::exceptionStart() const
{
    return m_exceptionStart;
}

QDate WeekInfo::exceptionEnd() const
{
    return m_exceptionEnd;
}

int WeekInfo::midweekDay() const
{
    return m_midweekDay;
}

int WeekInfo::weekendDay() const
{
    return m_weekendDay;
}

void WeekInfo::saveChanges()
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    sql_item s;
    ccongregation::exceptions ex = c.isException(m_date);
    QDate d1;
    QDate d2;
    int id = -1;
    QString oldText = "";

    if (ex != ccongregation::None) {
        c.getExceptionDates(m_date, d1, d2);
        sql_item q;
        q.insert("date", d1);
        q.insert("date2", d2);
        sql_items s = sql->selectSql("SELECT id, desc FROM exceptions WHERE date = :date AND date2 = :date2 AND active", &q);
        if (s.size() > 0) {
            id = s[0].value("id").toInt();
            oldText = s[0].value("desc").toString();
        }
    }

    if (ex == exception() && d1 == exceptionStart() && d2 == exceptionEnd() && exceptionText() == oldText) {
        qDebug() << "no changes!";
    } else {

        if (exception() == ccongregation::None) {
            if (id > -1) {
                // remove existing exception
                s.insert("active", 0);
                s.insert("id", id);
                if (sql->execSql("UPDATE exceptions SET active = :active, time_stamp = strftime('%s','now') WHERE id = :id", &s)) {
                    if (ex == ccongregation::CircuitOverseersVisit && m_date.year() >= 2016) {
                        // Remove LMM_Schedule when the previous exception was None and m_date.year() >= 2016
                        s.clear();
                        s.insert("date", m_date);
                        s.insert("talk_id", LMM_Schedule::TalkType_COTalk);
                        s.insert("active", 0);
                        sql->execSql("update lmm_schedule set active = :active, time_stamp = strftime('%s','now') where "
                                     "date=:date and talk_id=:talk_id", &s);
                    }
                }
            }
        } else {            
            if (!exceptionStart().isValid() || !exceptionEnd().isValid())
                return;

            int exceptionType;
            switch (exception()) {
            case ccongregation::CircuitOverseersVisit:
                exceptionType = 0;
                if (m_date.year() >= 2016) {
                    // add co talk for lmm_schedule
                    LMM_Schedule cotalk(LMM_Schedule::TalkType_COTalk, 0, m_date, "", "", 30, -1);
                    cotalk.setTheme(cotalk.talkName());
                    cotalk.save();
                    // remove assignments from aux. classes
                    sql_item a;
                    a.insert("assignee_id", -1);
                    a.insert("assistant_id", -1);
                    a.insert("date", m_date);
                    sql->execSql("UPDATE lmm_assignment SET assignee_id = :assignee_id, assistant_id = :assistant_id,"
                                 "time_stamp = strftime('%s','now') WHERE date = :date AND classnumber > 1", &a);
                }
                break;
            case ccongregation::CircuitAssembly:
                exceptionType = 1;
                setExceptionEnd(exceptionStart());
                break;
            case ccongregation::RegionalConvention:
                exceptionType = 1;
                if (exceptionEnd().toJulianDay() - exceptionStart().toJulianDay() < 2) {
                    if (exceptionStart().dayOfWeek() > 5)
                        setExceptionStart(m_date.addDays(4));
                    setExceptionEnd(exceptionStart().addDays(2));
                }
                break;
            case ccongregation::Memorial:
                exceptionType = 2;
                if (exceptionEnd() > exceptionStart())
                    setExceptionEnd(exceptionStart());
                break;
            default:
                exceptionType = static_cast<int>(exception());
            }
            qDebug() << "type" << exceptionType << exception() << exceptionText();
            s.clear();
            s.insert("type", exceptionType);
            s.insert("active", 1);
            s.insert("date", exceptionStart());
            s.insert("date2", exceptionEnd());
            s.insert("desc", exceptionText());
            s.insert("schoolday", midweekDay());
            s.insert("publicmeetingday", weekendDay());

            if (id < 0) {
                // Create new
                sql->insertSql("exceptions", &s, "id");
            } else {
                // Update existing
                s.insert("id", id);
                sql->updateSql("exceptions", "id", QString::number(id), &s);
            }
        }
        c.clearExceptionCache();
        loadCurrentWeek();
    }
}

void WeekInfo::load()
{
    c.clearExceptionCache();
    loadCurrentWeek();
}

QString WeekInfo::exceptionDisplayText() const
{
    return m_exceptionDisplayText;
}

void WeekInfo::setDate(QDate date)
{
    if (m_date == date)
        return;

    m_date = date;   
    emit dateChanged(m_date);    
}

void WeekInfo::setExceptionText(QString exceptionText)
{
    if (m_exceptionText == exceptionText)
        return;

    m_exceptionText = exceptionText;
    emit exceptionTextChanged(m_exceptionText);
}

void WeekInfo::setException(ccongregation::exceptions exception)
{
    if (m_exception == exception)
        return;

    m_exception = exception;
    emit exceptionChanged(m_exception);
}

void WeekInfo::setExceptionStart(QDate exceptionStart)
{
    if (m_exceptionStart == exceptionStart)
        return;

    m_exceptionStart = exceptionStart;
    emit exceptionStartChanged(m_exceptionStart);
}

void WeekInfo::setExceptionEnd(QDate exceptionEnd)
{
    if (m_exceptionEnd == exceptionEnd)
        return;

    m_exceptionEnd = exceptionEnd;
    emit exceptionEndChanged(m_exceptionEnd);
}

void WeekInfo::setMidweekDay(int midweekDay)
{
    if (m_midweekDay == midweekDay)
        return;

    m_midweekDay = midweekDay;
    emit midweekDayChanged(m_midweekDay);
}

void WeekInfo::setWeekendDay(int weekendDay)
{
    if (m_weekendDay == weekendDay)
        return;

    m_weekendDay = weekendDay;
    emit weekendDayChanged(m_weekendDay);
}

void WeekInfo::loadCurrentWeek()
{
    QDate d1;
    QDate d2;
    ccongregation::exceptions ex = c.isException(m_date);    
    if (ex != ccongregation::None)
        c.getExceptionDates(m_date, d1, d2);

    QString text = c.getExceptionText(m_date);
    if (text != m_exceptionDisplayText) {
        m_exceptionDisplayText = c.getExceptionText(m_date);
        emit exceptionDisplayTextChanged(m_exceptionDisplayText);
    }
    setExceptionText(static_cast<int>(ex) > 3 ? c.getStandardExceptionText(m_date, false) : "");
    setException(ex);
    setExceptionStart(d1);
    setExceptionEnd(d2);
    setMidweekDay(c.getMeetingDay(m_date, ccongregation::meetings::tms));
    setWeekendDay(c.getMeetingDay(m_date, ccongregation::meetings::pm));
}
