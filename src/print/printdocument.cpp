/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "printdocument.h"

PrintDocument::PrintDocument()
    : currentWeek(nullptr)
{
    sql = &Singleton<sql_class>::Instance();

    myCongregation = c.getMyCongregation();

    school s;
    schoolday = s.getSchoolDay();
    publicmeetingday = myCongregation.getPublicmeeting(QDate::currentDate()).getMeetingday();

    // page margins
    QString pm = sql->getSetting("template_pagemargins");
    if (pm == "") {
        pmlist.setLeft(15);
        pmlist.setTop(20);
        pmlist.setRight(15);
        pmlist.setBottom(20);
    } else {
        QStringList temp(pm.split(","));
        pmlist.setLeft(temp[0].toDouble());
        pmlist.setTop(temp[1].toDouble());
        pmlist.setRight(temp[2].toDouble());
        pmlist.setBottom(temp[3].toDouble());
    }
    // page orientation
    int porient = sql->getSetting("template_paperorientation").toInt();
    if (porient == 0) {
        po = QPageLayout::Portrait;
    } else {
        po = QPageLayout::Landscape;
    }

    QSettings settings;
    textSizeFactorySlips = settings.value("printui/textsizeslips", 1).toReal();
}

PrintDocument::~PrintDocument()
{
    qDeleteAll(weekList);
    weekList.clear();
}

QDate PrintDocument::getFromDate()
{
    return fromDate;
}

void PrintDocument::setFromDate(QDate d)
{
    if (fromDate == d)
        return;
    fromDate = d;
    publicmeetingday = myCongregation.getPublicmeeting(fromDate).getMeetingday();
}

QDate PrintDocument::getToDate()
{
    return thruDate;
}

void PrintDocument::setToDate(QDate d)
{
    if (thruDate == d)
        return;
    thruDate = d;
}

bool PrintDocument::qrCodeSupported()
{
    return qrCodeSupport;
}

TemplateData *PrintDocument::getTemplateData()
{
    return currentTemplateData.data();
}

void PrintDocument::setTemplateName(QString templateName)
{
    currentTemplateData->setTemplateName(templateName);
}

QVariant PrintDocument::fillTemplate()
{
    return "";
}

QPageLayout PrintDocument::getLayout()
{
    return layout;
}

QString PrintDocument::getTemplate(bool returnFileName)
{
    QString templateDir;
    QString customTemplateDir = sql->getSetting("customTemplateDirectory");
    QString appDataLocation = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
#if defined(Q_OS_IOS)
    templateDir = QApplication::applicationDirPath() + "/Templates/";
    customTemplateDir = appDataLocation + "/custom_templates";
#elif defined(Q_OS_ANDROID)
    templateDir = "assets:/Templates/";
    customTemplateDir = appDataLocation + "/custom_templates";
#elif defined(Q_OS_MACOS)
    templateDir = QApplication::applicationDirPath() + "/../Resources/templates/";
#else
    templateDir = QApplication::applicationDirPath() + "/templates/";
#endif
    QString templateName = currentTemplateData->getTemplateName();
    QString templateFullPath;

    if (!templateName.isEmpty() && !QFile::exists(templateName)) {
        // try again with path
        templateFullPath = QDir(customTemplateDir).filePath(templateName);
        if (!QFile::exists(templateFullPath)) {
            templateFullPath = QDir(templateDir).filePath(templateName);
        }
    }

    if (returnFileName)
        return templateFullPath;

    if (!QFile::exists(templateFullPath)) {
        return "<HTML><BODY><h3>" + tr("Template not found", "printing template not found") + "</h3><p>Path: " + templateFullPath + "</p></BODY></HTML>"; //<p>!REPEAT_START!</p><p>!REPEAT_END!</p>
    }

    QFile file(templateFullPath);
    if (!file.open(QFile::ReadOnly)) {
        return "<HTML><BODY><h3>" + tr("Can't read file", "cannot read printing template") + "</h3><p>Path: " + templateFullPath + "</p></BODY></HTML>"; //<p>!REPEAT_START!</p><p>!REPEAT_END!</p>
    }
    QByteArray data = file.readAll();
    file.close();
    QTextCodec *codec = QTextCodec::codecForHtml(data);
    QString str = codec->toUnicode(data);
    handleCurrentTemplateMeta(str);
    return str;
}

void PrintDocument::handleCurrentTemplateMeta(QString html)
{
    currentTemplateData->loadMeta(html);
}

void PrintDocument::fillWeekList(int offsetFromMonday)
{
    int selectedWeeks(QVariant((thruDate.toJulianDay() - fromDate.toJulianDay()) / 7 + 1).toInt());
    weekInfo *previous(nullptr);
    qDeleteAll(weekList);
    weekList.clear();
    QDate tempdate = fromDate;
    QDate mtgDate = fromDate.addDays(offsetFromMonday);
    for (int weeks = 0; weeks < selectedWeeks; weeks++) {
        weekInfo *w = new weekInfo(tempdate, mtgDate);
        if (previous) {
            w->newmonth = w->mtgDate.year() != previous->mtgDate.year() || w->mtgDate.month() != previous->mtgDate.month();
            if (w->newmonth) {
                fillWeekList_SetWeeks(previous);
                previous->endmonth = true;
                w->weeknum = 1;
            } else {
                w->weeknum = previous->weeknum + 1;
            }
        } else {
            w->weeknum = 1;
            w->isFirst = true;
            w->newmonth = true;
        }
        w->fullweeknum = weeks + 1;
        weekList.append(w);
        previous = w;
        tempdate = tempdate.addDays(7);
        mtgDate = mtgDate.addDays(7);
    }
    fillWeekList_SetWeeks(previous);
    previous = weekList[weekList.length() - 1];
    previous->isLast = true;
    previous->endmonth = true;
}

void PrintDocument::fillWeekList_SetWeeks(PrintDocument::weekInfo *previous)
{
    if (previous) {
        for (weekInfo *i : weekList) {
            if (i->weeks == 0)
                i->weeks = previous->weeknum;
        }
    }
}

QPair<QString, QString> PrintDocument::templateGetSection(QString context, const QString start, const QString end, const QString classname)
{
    QRegularExpression exp(QString("<p>%1[\\s\\S]*?%2</p>").arg(start, end), QRegularExpression::UseUnicodePropertiesOption | QRegularExpression::CaseInsensitiveOption);
    QRegularExpressionMatch match;
    QString section = "";
    if (context.indexOf(exp, 0, &match) > -1) {
        context = context.replace(exp, QString("<div class=\"%1\"></div>").arg(classname));
        section = match.captured().remove(QRegularExpression(QString("<p>%1</p>|<p>%2</p>").arg(start, end)));
    }
    return qMakePair(context, section);
}

QString PrintDocument::fillCommonItems(QString context, bool nomeeting, ccongregation::exceptions ex)
{
    if (!currentWeek) {
        fillWeekList(0);
        currentWeek = weekList.first();
    }
    context = fillDate(context, ccongregation::pm, "PM_DATE");
    context = fillExceptionDate(context, "EXCEPTION_DATE");
    context.replace("!EXCEPTION!", c.getStandardExceptionText(currentWeek->week, false));
    context.replace("!NO_MEETING_EXCEPTION!", nomeeting ? "Yes" : "");
    context.replace("!CO_VISIT!", ex == ccongregation::CircuitOverseersVisit ? "Yes" : "");
    context.replace("!CO_NAME!", sql->getSetting("circuitoverseer"));
    context.replace("!CO_TITLE!", tr("Service Talk"));
    context.replace("!CONVENTION!", (ex == ccongregation::CircuitAssembly || ex == ccongregation::RegionalConvention) ? "Yes" : "");
    context.replace("!MEMORIAL!", ex == ccongregation::Memorial ? "Yes" : "");
    context.replace("!WEEKS!", QVariant(currentWeek->weeks).toString());
    context = replaceIntTag(context, "FULLWEEKNUMBER", currentWeek->fullweeknum);
    context = replaceIntTag(context, "WEEKNUMBER", currentWeek->weeknum);
    context.replace("!NEWMONTH!", currentWeek->newmonth ? "Yes" : "");
    context.replace("!ENDMONTH!", currentWeek->endmonth ? "Yes" : "");
    context.replace("!ISFIRST!", currentWeek->isFirst ? "Yes" : "");
    context.replace("!ISLAST!", currentWeek->isLast ? "Yes" : "");
    context.replace("!BEGIN!", tr("Begins at", "Used in print template, example 'Begins at 11:00'"));
    return context;
}

QString PrintDocument::fillDate(QString context, ccongregation::meetings meetingtype, QString VarName)
{
    QDate dt;
    int day(c.getMeetingDay(currentWeek->week, meetingtype));
    if (day == 0)
        dt = QDate();
    else
        dt = currentWeek->week.addDays(day - 1);
    context = replaceDateTag(context, VarName, dt);
    return context;
}

QString PrintDocument::fillExceptionDate(QString context, QString tag)
{
    QDate d1, d2;
    QString fulltag("!" + tag);
    if (context.indexOf(fulltag, 0, Qt::CaseInsensitive) > -1) {
        fulltag += "([^!]*)!";
        c.getExceptionDates(currentWeek->week, d1, d2);
        QLocale loc;
        QString defaultFormat = loc.dateFormat(QLocale::ShortFormat);
        int pos = 0;
        QRegularExpression rx(fulltag);
        while ((pos = context.indexOf(rx)) > -1) {
            QRegularExpressionMatch match = rx.match(context);
            if (d1.isNull()) {
                context = context.left(pos) + context.mid(pos + match.capturedLength());
            } else {
                QString format = match.captured(2).isEmpty() ? defaultFormat : match.captured(1).trimmed();
                QString exceptionText = d1.toString(format);
                if (!d2.isNull() && d1 != d2)
                    exceptionText += "-" + d2.toString(format);
                context = context.left(pos) + exceptionText + context.mid(pos + match.capturedLength());
            }
        }
    }
    return context;
}

QString PrintDocument::replaceDateTag(QString context, QString tag, QDate date, QString wrapperText)
{
    QLocale l;
    int pos = 0;
    QString fulltag("!" + tag + " ");
    if (context.indexOf(fulltag, 0, Qt::CaseInsensitive) > -1) {
        fulltag = fulltag + "([^!]+)!";
        QRegularExpression rx(fulltag);
        while ((pos = context.indexOf(rx)) > -1) {
            QRegularExpressionMatch match = rx.match(context);
            if (date.isNull())
                context = context.left(pos) + context.mid(pos + match.capturedLength());
            else
                context = context.left(pos) + wrapperText.arg(l.toString(date, match.captured(1))) + context.mid(pos + match.capturedLength());
        }
    }
    fulltag = "!" + tag + "!";
    if (context.indexOf(fulltag, 0, Qt::CaseInsensitive) > -1) {
        if (date.isNull())
            context.replace(fulltag, "");
        else
            context.replace(fulltag, wrapperText.arg(l.toString(date, QLocale::ShortFormat)));
    }
    return context;
}

QString PrintDocument::replaceDateRangeTag(QString context, QString tag, QDate startDate, QDate endDate)
{
    QLocale l;
    // set en-US and same month format as default
    QString dateRangeFormat = "{0:MMMM dd} - {1:dd, yyyy}";
    QString formatType = "samemonth_rangeformat";
    if (startDate.month() != endDate.month()) {
        if (startDate.year() == endDate.year()) {
            dateRangeFormat = "{0:MMMM dd} - {1:MMMM dd, yyyy}"; // same year format
            formatType = "sameyear_rangeformat";
        } else {
            dateRangeFormat = "{0:MMMM dd, yyyy} - {1:MMMM dd, yyyy}"; // general format}
            formatType = "general_rangeformat";
        }
    }
    sql_class *sql = &Singleton<sql_class>::Instance();
    sql_item parameters;
    parameters.insert(":locale", l.name());
    dateRangeFormat = sql->selectScalar("select " + formatType + " from locales where code = :locale", &parameters, dateRangeFormat).toString();

    QRegularExpression dateRangeFormatRegex("^{\\d:(?<startDate>.+)}(?<separator>.+){\\d:(?<endDate>.+)}");
    QRegularExpressionMatch dateRangeFormatMatch = dateRangeFormatRegex.match(dateRangeFormat);
    QString startDateFormat = dateRangeFormatMatch.captured("startDate");
    QString separator = dateRangeFormatMatch.captured("separator");
    QString endDateFormat = dateRangeFormatMatch.captured("endDate");

    QString fulltag("!" + tag + "!");
    if (context.indexOf(fulltag, 0, Qt::CaseInsensitive) > -1) {
        if (startDate.isNull() || endDate.isNull())
            context.replace(fulltag, "");
        else
            context.replace(fulltag, QString(l.toString(startDate, startDateFormat) + separator + l.toString(endDate, endDateFormat)));
    }
    return context;
}

QString PrintDocument::replaceTimeTag(QString context, QString tag, QTime time)
{
    QLocale l;
    int pos = 0;
    QString fulltag("!" + tag + " ");
    if (context.indexOf(fulltag, 0, Qt::CaseInsensitive) > -1) {
        fulltag = fulltag + "([^!]+)!";
        QRegularExpression rx(fulltag);
        while ((pos = context.indexOf(rx)) > -1) {
            QRegularExpressionMatch match = rx.match(context);
            if (time.isNull())
                context = context.left(pos) + context.mid(pos + match.capturedLength());
            else
                context = context.left(pos) + l.toString(time, match.captured(1)) + context.mid(pos + match.capturedLength());
        }
    }
    fulltag = "!" + tag + "!";
    if (context.indexOf(fulltag, 0, Qt::CaseInsensitive) > -1) {
        if (time.isNull())
            context.replace(fulltag, "");
        else
            context.replace(fulltag, l.toString(time, QLocale::ShortFormat));
    }
    return context;
}

QString PrintDocument::replaceIntTag(QString context, QString tag, int number)
{
    int pos = 0;
    QString fulltag("!" + tag + " ");
    if (context.indexOf(fulltag, 0, Qt::CaseInsensitive) > -1) {
        fulltag = fulltag + "([^!]+)!";
        QRegularExpression rx(fulltag);
        while ((pos = context.indexOf(rx)) > -1) {
            // must be a padding number
            QRegularExpressionMatch match = rx.match(context);
            int padding(match.captured(1).toInt());
            context = context.left(pos) + QString("%1").arg(number, padding, 10, QChar('0')) + context.mid(pos + match.capturedLength());
        }
    }
    fulltag = "!" + tag + "!";
    if (context.indexOf(fulltag, 0, Qt::CaseInsensitive) > -1) {
        context.replace(fulltag, QVariant(number).toString());
    }
    return context;
}

QString PrintDocument::addCssPageBreak()
{
    return ("<div style=\"page-break-after:always\"></div>");
}

QString PrintDocument::initLayout(QString html)
{
    if (currentTemplateData->customPaperSize.width() == 0.0) {
        auto ps = QPageSize(currentTemplateData->standardPaperSize);
        if (!ps.isValid()) {
            auto size = QPageSize::size(currentTemplateData->standardPaperSize, QPageSize::Millimeter);
            ps = QPageSize(size, QPageSize::Millimeter, currentTemplateData->paperSize);
        }
        layout.setPageSize(ps);
    } else {
        layout.setPageSize(QPageSize(currentTemplateData->customPaperSize, currentTemplateData->customPaperUnit));
    }
    layout.setOrientation(getOrientation());
    layout.setMargins(getMargins());

    QSizeF size(layout.pageSize().size(QPageSize::Millimeter));

    html.replace("!PAPERWIDTH!", QString::number(size.width()));
    html.replace("!PAPERHEIGHT!", QString::number(size.height()));
    return html;
}

QMarginsF PrintDocument::getMargins()
{
    if (currentTemplateData->metaValue("tb-margin") == "")
        return pmlist;
    QStringList margins = currentTemplateData->metaValue("tb-margin").split(';');
    QMarginsF ret;
    if (margins.length() > 3) {
        ret.setLeft(margins[0].toDouble());
        ret.setTop(margins[1].toDouble());
        ret.setRight(margins[2].toDouble());
        ret.setBottom(margins[3].toDouble());
    } else if (margins.length() > 1) {
        ret.setLeft(margins[0].toDouble());
        ret.setTop(margins[1].toDouble());
        ret.setRight(margins[0].toDouble());
        ret.setBottom(margins[1].toDouble());
    } else {
        ret.setLeft(margins[0].toDouble());
        ret.setTop(margins[0].toDouble());
        ret.setRight(margins[0].toDouble());
        ret.setBottom(margins[0].toDouble());
    }
    return ret;
}

QPageLayout::Orientation PrintDocument::getOrientation()
{
    if (currentTemplateData->metaValue("tb-orientation") == "")
        return po;
    return currentTemplateData->metaValue("tb-orientation").compare("Landscape", Qt::CaseInsensitive) == 0 ? QPageLayout::Landscape : QPageLayout::Portrait;
}

TemplateData::TemplateData(TemplateTypes templateType, QString settingName, QString currentTemplate, QString fileFilter, QString notFileFilter)
{
    sql = &Singleton<sql_class>::Instance();
    this->templateType = templateType;
    this->settingName = settingName;
    this->templateName = currentTemplate;
    this->fileFilter = fileFilter;
    this->notFileFilter = notFileFilter;
    if (currentTemplate.isEmpty())
        this->templateName = sql->getSetting(settingName);
    setPaperSize();
    metaLoaded = false;
}

TemplateData::TemplateTypes TemplateData::getTemplateType()
{
    return templateType;
}

QString TemplateData::getTemplateName()
{
    return templateName;
}

void TemplateData::setTemplateName(QString name)
{
    templateName = name;
    sql->saveSetting(settingName, templateName);
    metaLoaded = false;
}

QString TemplateData::setPaperSize(QString size)
{
    static QRegularExpression rx("(\\d+[.]*\\d*)\\s*(\"|in|mm)\\s*x\\s*(\\d+[.]*\\d*)\\s*(\"|in|mm)");
    QString setting(settingName + "_papersize");
    bool loading(size.isEmpty());
    if (loading)
        size = sql->getSetting(setting);
    QRegularExpressionMatch m = rx.match(size);
    if (m.hasMatch()) {
        if (m.captured(2) != m.captured(4))
            return QObject::tr("Width unit does not match height unit", "while asking for custom paper size");
        customPaperSize.setWidth(m.captured(1).toDouble());
        customPaperSize.setHeight(m.captured(3).toDouble());
        if (m.captured(2) == "mm")
            customPaperUnit = QPageSize::Millimeter;
        else
            customPaperUnit = QPageSize::Inch;
    } else if (size == "A4") {
        standardPaperSize = QPageSize::PageSizeId::A4;
        customPaperSize.setWidth(0);
    } else if (size == "Letter") {
        standardPaperSize = QPageSize::PageSizeId::Letter;
        customPaperSize.setWidth(0);
    } else if (size == "Legal") {
        standardPaperSize = QPageSize::PageSizeId::Legal;
        customPaperSize.setWidth(0);
    } else if (size == "Tabloid") {
        standardPaperSize = QPageSize::PageSizeId::Tabloid;
        customPaperSize.setWidth(0);
    } else if (size == "A6") {
        standardPaperSize = QPageSize::PageSizeId::A6;
        customPaperSize.setWidth(0);
    } else {
        standardPaperSize = QPageSize::PageSizeId::A4;
        customPaperSize.setWidth(0);
        size = "A4";
        if (!loading) {
            paperSize = size;
            return QObject::tr("Invalid entry, sorry.", "while asking for custom paper size");
        }
    }
    paperSize = size;
    if (!loading)
        sql->saveSetting(setting, size);
    return "";
}

void TemplateData::loadMeta(QString html)
{
    if (!metaLoaded) {
        options.clear();
        meta.clear();
        static QRegularExpression rxMeta("<meta name=\"([^\"]+)\" content=\"([^\"]+)\">");
        QRegularExpressionMatchIterator i = rxMeta.globalMatch(html);
        while (i.hasNext()) {
            QRegularExpressionMatch m = i.next();
            QString key = m.captured(1);
            if (key.startsWith("tb-option-")) {
                key = key.mid(10);
                options.insert(key, m.captured(2));
                QString userValue = sql->getSetting(templateName + ":" + key, "!n/a!");
                if (userValue != "!n/a!")
                    options[key] = userValue;
            } else
                meta.insert(m.captured(1), m.captured(2));
        }
        metaLoaded = true;
    }
}

QString TemplateData::metaValue(QString name, QString defaultValue)
{
    if (meta.contains(name))
        return meta[name];
    return defaultValue;
}

QString TemplateData::optionValue(QString name, QString defaultValue)
{
    if (options.contains(name))
        return options[name];
    return defaultValue;
}

void TemplateData::setOptionValue(QString name, QString value, bool okAdd, bool persist)
{
    if (options.contains(name))
        options[name] = value;
    else {
        if (!okAdd)
            return;
        options.insert(name, value);
    }
    if (persist)
        sql->saveSetting(templateName + ":" + name, value);
}
