<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr-TR">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Sonraki çalışmayı atama</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Mevcut çalışmadan ayrı</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Geçersiz veri</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Zamanlama eklensin mi?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Kaynak</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Öğrenci</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Asistan</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Sonuç</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Tamamlandı</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Gönüllü</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Bir gönüllü seçin</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Mevcut Çalışma</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Alıştırma Tamamlandı</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Sonraki Çalışma</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Sonraki çalışmayı seçin</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Zamanlama</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notlar</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Okul Detayları</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Zamanlayıcı Başlat</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Zamanlayıcı Durdur</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Ayarlar</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Ayarı seçin</translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>No exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit overseer&#x27;s visit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit assembly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Regional convention</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Memorial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Other exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Kaynak</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Konuşmacı</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Okuyucu</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notlar</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Şarkı</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Student</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Timing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Study point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Detaylar</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>Yenilemek için çekin...</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Yenilemek için bırakın...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation>Kullanıcı Adı</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Şifre</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Giriş</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation>%1 Hafta başlıyor</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Baskı</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Text size</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Phone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>İsim</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Soyad</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Erkek kardeş</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Kız kardeş</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Hizmetli</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Aile</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>Bağlantılı aile üyesi</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>İletişim Bilgileri</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Eposta</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>Tüm Sınıflar</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Sadece Ana Sınıf</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Sadece Destek Sınıfları</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Aile Reisi</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Asistan</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Bilgi</translation>
    </message>
    <message>
        <source>Active</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Yayıncılar</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Ayarlar</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Çıkış yap</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Bilgi</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Sürüm</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>TheocBase Anasayfa</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Geri bildirim</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>Son senkronize edilen: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Printing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Senkronize ediliyor...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Song</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>weeks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Sonraki çalışmayı atama</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>Belirlenmedi</translation>
    </message>
</context></TS>