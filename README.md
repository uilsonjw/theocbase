# TheocBase #
TheocBase is a tool for JW to schedule midweek and weekend meetings.

## Links ##
- Webpage: www.theocbase.net
- Twitter: twitter.com/theocbase

## Setup ##
### Prerequisites ###
- macOS: Xcode 10 or newer
- Windows: Visual Studio 2017 (or 2015). Select "Desktop development with C++" workload on VS 2017.
    - You can use Professional or Community version of Visual Studio
- Linux: [http://doc.qt.io/qt-5/linux.html#requirements-for-development-host](http://doc.qt.io/qt-5/linux.html#requirements-for-development-host)

### Installation ###
- TheocBase requires Qt 5.15 or later (Qt 6.* is not yet supported). Go to [www.qt.io/download](www.qt.io/download) and choose open source version.
- Install Qt. Select at least the following compontents:
    - macOS (on macOS)
    - MSVC2017 32-bit or MSVC2017 64-bit (on Windows)
    - Desktop gcc 64-bit (on Linux)
    - Qt WebEngine
    - Qt Network Authorization
    - Qt Creator
- Install GDAL library
    - Linux: Install GDAL and GDAL library header files with your package manager (see also https://trac.osgeo.org/gdal/wiki/DownloadingGdalBinaries) or download it from https://gdal.org/download.html
    - macOS: Download GDAL complete package from http://www.kyngchaos.com/software/frameworks/. Install in the default location /Library/Frameworks/.
    - Windows: 
		- Download GDAL library
			- 32-bit: http://www.gisinternals.com/query.html?content=filelist&file=release-1911-dev.zip
			- 64-bit:http://www.gisinternals.com/query.html?content=filelist&file=release-1911-x64-dev.zip
			- or any more recent version
		- Create lib-folder if it does not exist and extract release-1911 folder of the ZIP package into lib-folder (lib folder at the top level, where bitrock, docs and src folders are)
		- Rename the "release-1911-x??"-folder to "gdal??" where ?? is either 32 or 64
		- Add ..lib/gdal??/bin folder into PATH variable (Projects -> Build Environment)
			- also may need to add "...lib\gdal\include" to the IINCLUDE variable and "...lib\gdal\lib" to the LIB variable
		- **Build -> Run qmake** and **Build -> Rebuild project**
		

### Source codes ###
- Clone repository to your local machine (see [Contributing topic](#contributing))
- Download constant.h file and save it to your `src` folder. You can download it from [Dropbox](https://www.dropbox.com/s/r0kycavb651otey/constants.h?dl=0)

## Usage ##
- Start Qt Creator
- Open `theocbase.pro` file from `src` folder
- Build -> Run qmake
- Build -> Build Project ”theocbase”
- Build -> Run

## Contributing ##
1.	Fork the project and clone locally
2.	Create a new branch to work on (from dev branch)
3.	Do the work, write commit messages
4.	Push to your origin repository
5.	Create a new pull request in Bitbucket (target the project’s dev branch)
6.	Respond to any code review feedback
7.	Once the pull request is approved and merged you can pull the changes form upstream to your local repo and delete your extra branch.

## License ##
GPLv3: [www.gnu.org/licenses/gpl-3.0.html](www.gnu.org/licenses/gpl-3.0.html)
