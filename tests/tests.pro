TEMPLATE = subdirs
SUBDIRS=\
    cloudsync \
    common \
    midweekmeeting \
    weekendmeeting \
    slipscanner \
    wt_import \
    workbook_import \
    weekinfo
wt_import.depends = common
workbook_import.depends = common
weekinfo.depens = common
midweekmeeting.depens = common
