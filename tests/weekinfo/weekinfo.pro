QT += testlib widgets sql network

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

DEFINES += SRCDIR=\\\"$$PWD\\\"

HEADERS += \
     ../../src/weekinfo.h
SOURCES += \
    ../../src/weekinfo.cpp \
    test_weekinfo.cpp

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../common/release/ -lcommon
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../common/debug/ -lcommon
else:unix: LIBS += -L../common -lcommon

INCLUDEPATH += $$PWD/../../src
DEPENDPATH += $$PWD/../../src
