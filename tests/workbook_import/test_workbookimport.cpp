#include <QtTest>
#include <QDebug>
#include "../common/common.h"
#include "../../src/importlmmworkbook.h"

class test_workbookimport : public QObject
{
    Q_OBJECT

public:
    test_workbookimport();
    ~test_workbookimport();

private slots:
    void initTestCase();
    void init();
    void cleanup();
    void cleanupTestCase();
    void test_case1_data();
    void test_case1();
};

test_workbookimport::test_workbookimport()
{

}

test_workbookimport::~test_workbookimport()
{

}

void test_workbookimport::initTestCase()
{
    common::initDataBase();
}

void test_workbookimport::init()
{
    qDebug() << "INIT";
}

void test_workbookimport::cleanup()
{
    qDebug() << "CLEANUP";
}

void test_workbookimport::cleanupTestCase()
{
    common::clearDatabase();
}

void test_workbookimport::test_case1_data()
{
    // Columns
    QTest::addColumn<QString>("filename");
    QTest::addColumn<QDate>("firstdate");
    QTest::addColumn<QString>("downloadlink");

    // Rows
    QTest::addRow("October 2019 en") << "mwb_E_201910.epub" << QDate(2019, 10, 7)
                                     << "https://download-a.akamaihd.net/files/media_periodical/79/mwb_E_201910.epub";
    QTest::addRow("November 2019 en") << "mwb_E_201911.epub" << QDate(2019, 11, 4)
                                     << "https://download-a.akamaihd.net/files/media_periodical/af/mwb_E_201911.epub";
    QTest::addRow("January 2020 en") << "mwb_E_202001.epub" << QDate(2020, 1, 6)
                                     << "https://download-a.akamaihd.net/files/media_periodical/f3/mwb_E_202001.epub";
    QTest::addRow("January 2020 et") << "mwb_ST_202001.epub" << QDate(2020, 1, 6)
                                     << "https://download-a.akamaihd.net/files/media_periodical/70/mwb_ST_202001.epub";
    QTest::addRow("January 2020 fi") << "mwb_FI_202001.epub" << QDate(2020, 1, 6)
                                     << "https://download-a.akamaihd.net/files/media_periodical/59/mwb_FI_202001.epub";
    QTest::addRow("January 2020 pt") << "mwb_TPO_202001.epub" << QDate(2020, 1, 6)
                                     << "https://download-a.akamaihd.net/files/media_periodical/3c/mwb_TPO_202001.epub";
    QTest::addRow("January 2020 pt-BR") << "mwb_T_202001.epub" << QDate(2020, 1, 6)
                                        << "https://download-a.akamaihd.net/files/media_periodical/13/mwb_T_202001.epub";
    QTest::addRow("January 2020 ru") << "mwb_U_202001.epub" << QDate(2020, 1, 6)
                                     << "https://download-a.akamaihd.net/files/media_periodical/15/mwb_U_202001.epub";
    QTest::addRow("January/February 2021 en") << "mwb_E_202101.epub" << QDate(2021, 01, 04)
                                     <<  "https://download-a.akamaihd.net/files/media_periodical/c0/mwb_E_202101.epub";
    QTest::addRow("March/Arpil 2021 en") << "mwb_E_202103.epub" << QDate(2021, 03, 01)
                                     << "https://download-a.akamaihd.net/files/media_periodical/c9/mwb_E_202103.epub";

}

void test_workbookimport::test_case1()
{
    QFETCH(QString, filename);
    QFETCH(QDate, firstdate);
    QFETCH(QString, downloadlink);
    QDate lastDate;
    int weeks;
    if (firstdate.year() < 2021) {
        lastDate.setDate(firstdate.year(), firstdate.month(), firstdate.daysInMonth());
        weeks = (lastDate.day() - firstdate.day()) / 7 + 1;
    } else {
        lastDate.setDate(firstdate.year(), firstdate.addMonths(1).month(), firstdate.addMonths(1).daysInMonth());
        weeks = (lastDate.dayOfYear() - firstdate.dayOfYear()) / 7 + 1;
    }

    int dayOfWeek = lastDate.dayOfWeek();
    lastDate = lastDate.addDays(-(dayOfWeek-1));

    QString testPath = QT_TESTCASE_BUILDDIR;
    QString srcDir = SRCDIR;
    QVERIFY(common::downloadTestFile(filename, downloadlink, srcDir));

    QString epubFile = QFINDTESTDATA(filename);
    importlmmworkbook wb;
    QString result = wb.importFile(epubFile);
    QString resultText = QString("Imported %1 weeks from %2 thru %3").arg(weeks).arg(
                firstdate.toString(Qt::DefaultLocaleShortDate), lastDate.toString(Qt::DefaultLocaleShortDate));    
    QCOMPARE(result, resultText);

    LMM_Meeting mtg(nullptr);
    QDate tmpDate(firstdate);
    for (int i = 0;i < weeks; i++) {
        mtg.loadMeeting(tmpDate);
        QVERIFY(!mtg.bibleReading().isEmpty());
        QVERIFY(mtg.songBeginning() > 0);
        QVERIFY(mtg.songMiddle() > 0);
        QVERIFY(mtg.songEnd() > 0);
        mtg.loadAssignments();
        QVERIFY(mtg.getAssignment(LMM_Schedule::TalkType_Treasures));
        QVERIFY(mtg.getAssignment(LMM_Schedule::TalkType_Digging));
        QVERIFY(mtg.getAssignment(LMM_Schedule::TalkType_Digging));
        QVERIFY(mtg.getAssignment(LMM_Schedule::TalkType_BibleReading));
        QVERIFY(mtg.getAssignment(LMM_Schedule::TalkType_LivingTalk1));
        QVERIFY(mtg.getAssignment(LMM_Schedule::TalkType_CBS));
        tmpDate = tmpDate.addDays(7);
    }
}

QTEST_MAIN(test_workbookimport)

#include "test_workbookimport.moc"
