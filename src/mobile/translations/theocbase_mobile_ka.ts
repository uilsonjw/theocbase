<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ka">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>არ მისცე შემდეგი გაკვეთილი</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>მიმდინარე გაკვეთილის დატოვება</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>არასწორი მონაცემი</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>დავამატო ქრონომეტრაჟი?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>თემა</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>წყარო</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>მოსწავლე</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>დამხმარე</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>შედეგი</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>დასრულებულია</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>მოხალისე</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>მოხალისის მონიშვნა</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>მიმდინარე გაკვეთილი</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>სავარჯიშო შესრულებულია</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>შემდეგი გაკვეთილი</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>მონიშნე შემდეგი გაკვეთილი</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>ქრონომეტრაჟი</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>ჩანაწერები</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>სკოლის დეტალები</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>წამზომის ჩართვა</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>წამზომის გაჩერება</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>სიტუაცია</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>სიტუაციის მონიშვნა</translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>No exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit overseer&#x27;s visit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit assembly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Regional convention</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Memorial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Other exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation>%1-ით დაწყებული კვირა</translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>თემა</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>წყარო</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>ჩამტარებელი</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>მომხსენებელი</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>წამკითხველი</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>ჩანაწერები</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <comment>Page title</comment>
        <translation>ჩანაწერები</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>ღვთის სიტყვის საუნჯე</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>გააუმჯობესე მსახურება</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>იცხოვრე ქრისტიანული პრინციპებით</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>თავმჯდომარე</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>მრჩეველი</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>სიმღერა %1 და ლოცვა</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>სიმღერა</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>ჩამტარებელი</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>წამკითხველი</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>შესავალი სიტყვა</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>ლოცვა</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation>დასკვნითი კომენტარი</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>დაუტოვე იგივე გაკვეთილი</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>წამზომის ჩართვა</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>წამზომის გაჩერება</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>დავამატო ქრონომეტრაჟი?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>თემა</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>წყარო</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>მოსწავლე</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>შედეგი</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>დასრულებულია</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>დრო</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>მიმდინარე გაკვეთილი</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>სავარჯიშო შესრულებულია</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>შემდეგი გაკვეთილი</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>მონიშნე შემდეგი გაკვეთილი</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>ჩანაწერები</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>დამხმარე</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>მოხალისე</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>წამზომი</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>დამხმარე არ უნდა იყოს საპირისპირო სქესის წარმომადგენელი</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>გაკვეთილი</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>დეტალები</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>ჩამოწიე, რომ განაახლო</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>გაუშვი, რომ განაახლო</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation>მომხმარებლის სახელი</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>პაროლი</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>შესვლა</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>მომხმარებლის სახელი ან ფასტა</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>ელ-ფოსტა</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>რეგისტრაცია</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>პაროლის აღდგენა</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>ელ-ფოსტის მისამართი ვერ მოიძებნა!</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>დაგავიწყდა პაროლი?</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>შესვლა</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation>%1-ით დაწყებული კვირა</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation>გასვლითი მომხსენებელი</translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>%1 მომხსენებელი სხვა კრებაში</numerusform>
        </translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation>ამ შაბათ-კვირას ყველა მომხსენებელი ადგილზეა</translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>გეგმა</translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation>სამუშაო ფურცელი</translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation>მიმომსვლელი მომხსენებლების გეგმა</translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print</source>
        <translation>ბეჭდვა</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Text size</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>სიმღერა %1 და ლოცვა</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>სიმღერა %1 და ლოცვა</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>საჯარო მოხსენება</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>საგუშაგო კოშკის შესწავლა</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>სიმღერა %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>ჩამტარებელი</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>წამკითხველი</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>თემა</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>კრება</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>მომხსენებელი</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>მობილური</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>ტელეფონი</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>ელ-ფოსტა</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>ინფო</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>ჰოსტი</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>სახელი</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>გვარი</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>ძმა</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>და</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>მომსახურე</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>ოჯახი</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>ოჯახის წევრი</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>საკონტაქტო ინფორმაცია</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>ტელეფონი</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>ელ-ფოსტა</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>ყველა კლასი</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>მხოლოდ მთავარი დარბაზი</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>მხოლოდ კლასები</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>ოჯახის თავი</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>დამხმარე</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>ინფო</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>აქტიური</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>თავმჯდომარე</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>ღვთის სიტყვის საუნჯე</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>ბიბლის კითხვა</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>პირველი საუბარი</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>განმეორებითი მონახულება</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>ბიბლიის შესწავლა</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>იცხოვრე ქრისტიანული პრინციპებით – მოხსენებები</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>კრების ბიბლიის შესწავლა</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>კრების ბიბლიის შესწავლის წამკითხველი</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>ახალი მაუწყებელი</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>ლოცვა</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation>მომხსენებელს გაუმასპინძლდება</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>მობილური</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>შუა კვირის შეხვედრა</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>მოხსენება</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>შაბათ-კვირის შეხვედრა</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>საგუშაგო კოშკის ჩამტარებელი</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation>ღვთის სიტყვის სიღრმეები</translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>მაუწყებლები</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>პირობითი სიტუაცია</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>სისტემიდან გასვლა</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>ინფო</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>ვერსია</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>TheocBase-ის ვებგვერდი</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>შეფასება</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>ბოლოს დასინქრონირდა: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>გეგმა</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>დროის ჩვენება</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>ხანგრძლივობის ჩვენება</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>მომხმარებლის ინტერფეისი</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>ენა</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>შესვლა</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>ელ-ფოსტა</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>სახელი</translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation>აჩვენე სიმღერის სათაურები</translation>
    </message>
    <message>
        <source>Printing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>მიმდინარეობს სინქრონიზაცია</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>თავმჯდომარე</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>სიმღერა</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>სიმღერა</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation>საგუშაგო კოშკის სიმღერა</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation>საგუშაგო კოშკის გამოცემა</translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>სტატია</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>თემა</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>ჩამტარებელი</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>წამკითხველი</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>დროის დიაგრამა</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>რამდენიმე კვირა არჩეულ თარიღამდე</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>რამდენიმე კვირა არჩეულ თარიღამდე</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>კვირები</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation>დავალების შემდეგ რამდენიმე კვირა ნაცრისფერია</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>ლოკალურად და ქლაუდშიც არის მსგავსი ცვლილებები (%1 მწკრივი). გსურს ლოკალური ცვლილებების შენახვა?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>ღრუბელიში მონაცემთა ბაზა წაშლილია. ადგილობრივი მონაცემთა ბაზა შეიცვლება. გსურს გაგრძელება?</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>არ მისცე შემდეგი გაკვეთილი</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>არაფერი</translation>
    </message>
</context></TS>