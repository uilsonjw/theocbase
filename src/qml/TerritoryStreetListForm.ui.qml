import QtQuick 2.4
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1

Item {
    width: 400
    height: 400
    property alias streetTableView: streetTableView
    property alias removeStreetButton: removeStreetButton
    property alias addStreetButton: addStreetButton

    SystemPalette {
        id: myPalette
        colorGroup: SystemPalette.Active
    }

    ColumnLayout {
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.rightMargin: 12
        anchors.leftMargin: 12
        anchors.bottomMargin: 12
        anchors.topMargin: 12
        visible: true

        TableView {
            id: streetTableView
            sortIndicatorVisible: true

            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

            TableViewColumn {
                role: "id"
                title: qsTr("ID")
                width: 30
                visible: false

                delegate: Item {
                    Text {
                        anchors.fill: parent
                        anchors.leftMargin: 5
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignLeft

                        text: typeof styleData.value === "undefined" ? "" : styleData.value
                        renderType: Text.NativeRendering
                    }
                }
            }

            TableViewColumn {
                role: "territoryId"
                title: qsTr("Territory-ID")
                width: 30
                visible: false

                delegate: Item {
                    Text {
                        anchors.fill: parent
                        anchors.leftMargin: 5
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignLeft

                        text: typeof styleData.value === "undefined" ? "" : styleData.value
                        renderType: Text.NativeRendering
                    }
                }
            }

            TableViewColumn {
                role: "streetName"
                title: qsTr("Street name")
                width: parent.width - 70 - 70 - 70 - 150
                visible: true

                delegate: streetNameItemDelegate
            }

            TableViewColumn {
                role: "fromNumber"
                title: qsTr("From number", "Number range of addresses in the street")
                width: 70
                visible: true

                delegate: fromToNumberDelegate
            }

            TableViewColumn {
                role: "toNumber"
                title: qsTr("To number", "Number range of addresses in the street")
                width: 70
                visible: true

                delegate: fromToNumberDelegate
            }

            TableViewColumn {
                role: "quantity"
                title: qsTr("Quantity", "Quantity of addresses in the street")
                width: 70
                visible: true

                delegate: quantityDelegate
            }

            TableViewColumn {
                id: streetTypeIdColumn
                role: "streetTypeId"
                title: qsTr("Type", "Type of street")
                width: 150
                visible: true

                delegate: streetTypeItemDelegate
            }

            TableViewColumn {
                role: "wktGeometry"
                title: qsTr("Geometry", "Line geometry of the street")
                width: 30
                visible: false

                delegate: Item {
                    Text {
                        anchors.fill: parent
                        anchors.leftMargin: 5
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignLeft

                        text: typeof styleData.value === "undefined" ? "" : styleData.value
                        renderType: Text.NativeRendering
                    }
                }
            }

            rowDelegate: Rectangle {
                id: tableViewRowDelegate
                height: 25
                color: styleData.selected ? myPalette.highlight : (styleData.alternate ? myPalette.alternateBase : myPalette.base)
            }
        }

        RowLayout {
            width: 50
            Layout.fillWidth: false
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            spacing: 10

            ToolButton {
                id: addStreetButton

                Layout.minimumWidth: 24
                Layout.minimumHeight: 24
                tooltip: qsTr("Add new street")
                iconSource: "qrc:///icons/street_add.svg"
                opacity: addStreetButton.enabled ? 1.0 : 0.5
                visible: canEditTerritories
            }

            ToolButton {
                id: removeStreetButton

                Layout.minimumWidth: 24
                Layout.minimumHeight: 24
                tooltip: qsTr("Remove selected street")
                iconSource: "qrc:///icons/street_remove.svg"
                opacity: removeStreetButton.enabled ? 1.0 : 0.5
                visible: canEditTerritories
            }
        }
    }
}
