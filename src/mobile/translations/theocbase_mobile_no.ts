<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="no">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Ikke tildel neste leksjon</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Behold på nåværende leksjon</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Ugyldig data</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Lagre tiden?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Kilde</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Elev</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Partner</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Resultat</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Gjennomført</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Avløser</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Velg en avløser</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Nåværende leksjon</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Øvelser gjennomført</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Neste leksjon</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Velg neste leksjon</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Tid</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notater</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Skoledetaljer</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Start stoppeklokke</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Stopp stoppeklokke</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Situasjon</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Velg situasjon</translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>No exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit overseer&#x27;s visit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit assembly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Regional convention</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Memorial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Other exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Kilde</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Studieleder</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Taler</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Oppleser</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notater</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>SKATTER FRA GUDS ORD</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>OPPLÆRING I FORKYNNELSEN</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>VÅRT LIV SOM KRISTNE</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Ordstyrer</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Veileder</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Sang %1 og Bønn</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Sang</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Studieleder</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Oppleser</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Innledende kommentarer</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Bønn</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Behold på nåværende leksjon</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Start stoppeklokke</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Stopp stoppeklokke</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Legg til tiden?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Kilde</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Elev</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Resultat</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Gjennomført</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Tid</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Nåværende leksjon</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Øvelser gjennomført</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Neste leksjon</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Velg neste leksjon</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notater</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Partner</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Avløser</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>Stoppeklokke</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>Partner skal ikke være en av motsatt kjønn.</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Detaljer</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>Dra for å oppdatere</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Slipp for å oppdatere</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation>Brukernavn</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Passord</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Logg inn</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Brukernavn eller E-post</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-post</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Lag ny konto</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>Resett passord</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>Epost-adresse ble ikke funnet!</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Glemt passord</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>Innloggingssiden</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation>Ukestart %1</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Plan</translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation>Arbeidsark</translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation>Oversikt over foredragsholdere som besøker annen menighet</translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Skriv ut</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Text size</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Sang og Bønn</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Sang %1 og Bønn</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>OFFENTLIG FOREDRAG</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>VAKTTÅRNSTUDIET</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Sang %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Studieleder</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Leser</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Phone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>Fornavn</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Etternavn</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Bror</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Søster</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Tjener</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Familie</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>Familiemedlem med</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Kontaktinformasjon</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Epost</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>Alle skoler</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Kun Hovedsal</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Kun B-Sal</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Familieoverhode</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Partner</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>Aktiv</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Ordstyrer</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>Skatter fra Guds Ord</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Bibellesning</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Førstegangsbesøk</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Gjenbesøk</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Bibelstudium</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>Livet som Kristne Taler</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Menighetsbibelstudiet</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Oppleser på Menighetsbibelstudiet</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>Ny Forkynner</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Bønn</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Forkynnere</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Innstillinger</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Logg ut</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Versjon</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>TheocBase Hjemmeside</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Tilbakemelding</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>Sist synkronisert: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Plan</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Vis klokkeslett</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Vis varighet</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Brukergrensesnitt</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Språk</translation>
    </message>
    <message>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Printing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Synkroniserer...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Song</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>weeks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>De samme endringene finnes både lokalt og i skyen (%1 rows). Vil du beholde de lokale endringene?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Ikke tildel neste leksjon</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>Ikke satt</translation>
    </message>
</context></TS>