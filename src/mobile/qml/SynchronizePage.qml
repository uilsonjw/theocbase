import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.3
//import QtQuick.Controls.Styles 1.1

Rectangle {
    id: busypage
    width: 150 * dpi
    height: 150 * dpi
    color: "transparent"
    //color: "#cacaca"
    //radius: 20 * dpi
    //opacity: 0.9
    visible: false
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.verticalCenter: parent.verticalCenter

    property string text: qsTr("Synchronizing...")

    function runSync()
    {
        busypage.text = busypage.text
        busypage.visible = true
        ccloud.synchronize(false);
    }

    function continueSync(keeplocal)
    {
        busypage.visible = true
        console.log("sync continued");
        ccloud.continueSynchronize(keeplocal);
    }

    function runTest()
    {
        busypage.visible = true;
        ccloud.runTest();
    }

    Connections {
        target: ccloud
        function onSyncProgressed(value, max) {
            console.log("progress bar value changed " + value + "/" + max);
        }
        function onSyncFinished() {
            console.log("sync ready");
            busypage.visible = false;
        }
    }

    BusyIndicator {
        id: busy

        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter

        anchors.centerIn: parent.Center        

        running: busypage.visible
//        style: BusyIndicatorStyle {
//            indicator: Image {
//                visible: control.running
//                source: "qrc:///refresh.svg"
//                sourceSize: Qt.size(75*dpi,75*dpi)
//                RotationAnimator on rotation {
//                    running: control.running
//                    loops: Animation.Infinite
//                    duration: 2000
//                    from: 0 ; to: 360
//                }
//            }
//        }
    }

    Text {
        id: text1
        text: busypage.text
        horizontalAlignment: Text.AlignHCenter
        anchors.top: parent.top
        anchors.topMargin: 5
        anchors.horizontalCenter: parent.horizontalCenter
        font.pointSize: app_info.fontsizeSmall
        renderType: Text.QtRendering
    }
}
