#include "importlmmworkbook.h"
#include <qdebug.h>

importlmmworkbook::importlmmworkbook(QObject *parent)
    : QObject(parent), debugMode(false)
{
    epb.useEpubLangCode = true;
}

importlmmworkbook::importlmmworkbook(QString fileName)
    : debugMode(false)
{
    Init(fileName);
}

importlmmworkbook::importlmmworkbook(QString fileName, QString language, bool debugMode)
    : debugMode(debugMode)
{
    forcedLanguage = language;
    Init(fileName);
}

importlmmworkbook::importlmmworkbook(QString fileName, bool useEpubLangCode)
    : debugMode(true)
{
    epb.useEpubLangCode = useEpubLangCode;
    Init(fileName);
}

void importlmmworkbook::Init(QString fileName)
{
    sql = &Singleton<sql_class>::Instance();

    int yr(0);
    int mo(0);
    QRegularExpression extractYearAndMonth("mwb_.+(\\d\\d\\d\\d\\d\\d)");
    QRegularExpressionMatch match = extractYearAndMonth.match(fileName);
    if (match.hasMatch()) {
        QString txt = match.captured(1);
        yr = txt.leftRef(4).toInt();
        mo = txt.midRef(4, 2).toInt();
        if (yr > 2015 && yr < 3000 && mo > 0 && mo < 13) {
            year = yr;
            month = mo;
        } else {
            yr = 0;
        }
    }
    if (yr == 0) {
        year = -1;
        month = -1;
    }
    firstDate.clear();
    lastDate.clear();
    dtBeingImported = nullDate;
    prepared = !epb.Prepare(fileName);
}

importlmmworkbook::~importlmmworkbook()
{
    qDeleteAll(tocEntriesDate);
}

QString importlmmworkbook::Import()
{
    validDates = 0;

    sql->startTransaction();
    newSettingValues.clear();

    if (prepared) {
        connect(&epb, &epub::ProcessTOCEntry, this, &importlmmworkbook::ProcessTOCEntry);
        epb.ImportTOC();
    }

    if (!err.isEmpty() || firstDate.isEmpty()) {
        sql->rollbackTransaction();
        for (auto s : newSettingValues.keys())
            sql->saveSetting(s, newSettingValues.value(s));
    } else {
        sql->commitTransaction();
    }
    disconnect(&epb, &epub::ProcessTOCEntry, this, &importlmmworkbook::ProcessTOCEntry);

    if (!err.isEmpty()) {
        return "#ERR#" + err;
    } else if (firstDate.isEmpty()) {
        if (epb.language.isEmpty())
            return QObject::tr("Unable to read new Workbook format");
        if (regexes.isEmpty())
            return QObject::tr("Database not set to handle language '%1'").arg(epb.language);
        if (year < 1)
            return QObject::tr("Unable to find year/month");
        return QObject::tr("Nothing imported (no dates recognized)");
    } else if (debugMode) {
        return ""; // no news is good news
    } else {
        return QObject::tr("Imported %1 weeks from %2 thru %3").arg(QVariant(validDates).toString(), firstDate, lastDate);
    }
}

QString importlmmworkbook::importFile(const QString fileName)
{
    epb.useEpubLangCode = true;
    Init(fileName);
    return Import();
}

void importlmmworkbook::ImportDate(QDate dt, QString href, QString chapter)
{

    dtBeingImported = dt;
    ProcessTOCEntry(href, chapter);
    dtBeingImported.fromJulianDay(nullDate.toJulianDay());
}

void importlmmworkbook::ProcessTOCEntry(QString href, QString chapter)
{
    QDate dt(dtBeingImported);
    if (!dt.isValid()) {
        dt.setDate(year, month, 1);
        int dow = dt.dayOfWeek();
        if (dow > 1)
            dt = dt.addDays(8 - dow);
        dt = dt.addDays(validDates * 7);

        // skip over known memorial weeks
        // 2019: 2458589
        // 2020: 2458946
        // 2021: normal midweek meeting
        // 2022: 2459681

        if (dt.toJulianDay() == 2458589 || dt.toJulianDay() == 2458946 || dt.toJulianDay() == 2459681) {
            dt = dt.addDays(7);
            validDates++;
        }
    }

    if (epb.curlocal != nullptr && regexes.size() == 0) {
        sql_items rx = sql->selectSql("lmm_workbookregex", "lang", forcedLanguage);
        if (rx.size() == 0)
            rx = sql->selectSql("lmm_workbookregex", "lang", epb.language_ex);
        if (rx.size() == 0)
            rx = sql->selectSql("lmm_workbookregex", "lang", epb.epubLangCode);
        if (rx.size() == 0)
            rx = sql->selectSql("lmm_workbookregex", "lang", epb.language);
        for (unsigned int i = 0; i < rx.size(); i++) {
            sql_item kv = rx[i];
            regexes.insert(kv.value("key").toString(), kv.value("value").toString());
        }
    }

    // Part 1: parse doc

    epubliIdx = 0;
    epubResults.clear();
    for (int i = 0; i <= foundParts::lastItem; i++) {
        epubResults.append("");
    }

    qDebug() << "";
    qDebug() << dt;
    qDebug() << href;

    xml_reader r(epb.oebpsPath + "/" + href);
    r.register_elementsearch("body", xmlPartsContexts::body);
    r.register_attributesearch("h2", "id", "p2", xmlPartsContexts::bibleReading);
    r.register_elementsearch("strong", xmlPartsContexts::bibleReadingDetail, xmlPartsContexts::bibleReading, true);

    r.register_attributesearch("div", "id", "section1", xmlPartsContexts::section1);
    r.register_elementsearch("a", xmlPartsContexts::songDetail, xmlPartsContexts::section1, true);

    r.register_attributesearch("div", "id", "section2", xmlPartsContexts::section2);
    r.register_elementsearch("li", xmlPartsContexts::talk, xmlPartsContexts::section2, true);

    r.register_attributesearch("div", "id", "section3", xmlPartsContexts::section3);
    r.register_elementsearch("li", xmlPartsContexts::talk, xmlPartsContexts::section3, true);

    r.register_attributesearch("div", "id", "section4", xmlPartsContexts::section4);
    r.register_elementsearch("li", xmlPartsContexts::talk, xmlPartsContexts::section4, true);

    XLM_READER_CONNECT(r);
    ignoreTextDepth = -1;
    ignoreTextContext = -1;
    r.read();

    QRegularExpressionMatch match;

    // cleanup
    int dataCnt = 0;
    QRegularExpression cleanup3("•(\\s+)•", QRegularExpression::UseUnicodePropertiesOption | QRegularExpression::CaseInsensitiveOption);
    for (int rIdx = 0; rIdx <= foundParts::lastItem; rIdx++) {
        epubResults[rIdx] = epubResults[rIdx].trimmed();
        while (epubResults[rIdx].contains("  ")) {
            epubResults[rIdx].replace("  ", " ");
        }
        while (epubResults[rIdx].contains("••")) {
            epubResults[rIdx].replace("••", "");
        }
        while ((match = cleanup3.match(epubResults[rIdx], 0, QRegularExpression::NormalMatch, QRegularExpression::DontCheckSubjectStringMatchOption)).hasMatch()) {
            epubResults[rIdx] =
                    epubResults[rIdx].left(match.capturedStart()) + match.captured(1) + epubResults[rIdx].mid(match.capturedEnd());
        }
        if (epubResults[rIdx].length() > 0)
            dataCnt++;
    }

    bool validDate = dataCnt > 10;

    if (validDate) {
        tocEntries.append("(Date) " + chapter);
    } else {
        tocEntries.append(chapter);
    }
    tocEntriesDate.append(new QDate(dt));
    tocEntriesHTML.append(href);

    if (!validDate) {
        if (debugMode && err.isEmpty())
            err = "Not a schedule";
        return;
    }
    if (firstDate.isEmpty()) {
        firstDate = dt.toString(Qt::DefaultLocaleShortDate);
        DownloadAssistDoc(dt);
        if (dt.year() >= 2021) // two months in the same workbook when 2021 or later
            DownloadAssistDoc(dt.addMonths(1));
    }
    lastDate = dt.toString(Qt::DefaultLocaleShortDate);
    validDates++;

    // load assist data
    int talkid_index[15] { 0 };
    getTalkIdIndex(dt, talkid_index);

    meetingResults.clear();
    meetingResults.append("Date: " + dt.toString(Qt::ISODate));

    // Part 2: add to database

    int partid(-1);
    QRegularExpression rxsong(regexes["song"], QRegularExpression::UseUnicodePropertiesOption | QRegularExpression::CaseInsensitiveOption);
    QRegularExpression rxtiming(regexes["timing"], QRegularExpression::UseUnicodePropertiesOption | QRegularExpression::CaseInsensitiveOption);
    QRegularExpression rxstudy(regexes["study"], QRegularExpression::UseUnicodePropertiesOption | QRegularExpression::CaseInsensitiveOption);
    QList<QRegularExpression *> rxasgn;
    int assignmentoptions(4);
    for (int num = 0; num < assignmentoptions; num++) {
        QString key("assignment" + QString::number(num + 1));
        if (regexes.contains(key)) {
            QRegularExpression *rx = new QRegularExpression(regexes[key], QRegularExpression::UseUnicodePropertiesOption | QRegularExpression::CaseInsensitiveOption);
            rxasgn.append(rx);
            if (!rx->isValid())
                meetingResults.append("invalid " + key + ": " + rx->errorString() + " (offset: " + QString::number(rx->patternErrorOffset()) + ")");
        } else {
            assignmentoptions = num;
            break;
        }
    }
    int bsong(-1);
    int msong(-1);
    int esong(-1);

    match = rxsong.match(epubResults[foundParts::opensong], 0, QRegularExpression::NormalMatch, QRegularExpression::DontCheckSubjectStringMatchOption);
    if (match.hasMatch()) {
        bsong = epb.stringToInt(match.captured(1));
    }
    match = rxsong.match(epubResults[foundParts::midsong], 0, QRegularExpression::NormalMatch, QRegularExpression::DontCheckSubjectStringMatchOption);
    if (match.hasMatch()) {
        msong = epb.stringToInt(match.captured(1));
    }
    int esongpartid(-1);
    for (partid = foundParts::lastItem; partid > foundParts::talk1; partid--) {
        match = rxsong.match(epubResults[partid], 0, QRegularExpression::NormalMatch, QRegularExpression::DontCheckSubjectStringMatchOption);
        if (match.hasMatch()) {
            // closing song
            esong = epb.stringToInt(match.captured(1));
            esongpartid = partid;
            break;
        }
    }
    if (esongpartid > -1) {
        epubResults[esongpartid] = "";
        epubResults[esongpartid - 1] = ""; // remove review also
    }
    LMM_Meeting mtg(nullptr);
    mtg.loadMeeting(dt);
    mtg.setBibleReading(epubResults[foundParts::biblereading].replace("•", ""));
    mtg.setSongBeginning(bsong);
    mtg.setSongMiddle(msong);
    mtg.setSongEnd(esong);
    if (debugMode) {
        meetingResults.append("Beginning Song: " + QString::number(bsong));
        meetingResults.append("Middle Song: " + QString::number(msong));
        meetingResults.append("End Song: " + QString::number(esong));
        meetingResults.append("Bible Reading: " + epubResults[foundParts::biblereading]);
    } else
        mtg.save();

    QString talkPostfixName = "Talk Name Postfix for " + epb.language;
    QString talkPostfix = sql->getSetting(talkPostfixName, ":(");
    if (talkPostfix == ":(") {
        QStringList talks;
        for (partid = 0; partid <= foundParts::lastItem; partid++) {
            QStringRef target(&epubResults[partid]);
            match = rxtiming.match(target, 0, QRegularExpression::NormalMatch, QRegularExpression::DontCheckSubjectStringMatchOption);
            if (match.hasMatch()) {
                QStringRef head(target.left(match.capturedStart()));
                QString tail(target.mid(match.capturedEnd()).toString());
                for (int rxselectedidx = 0; rxselectedidx < assignmentoptions; rxselectedidx++) {
                    match = rxasgn[rxselectedidx]->match(head, 0, QRegularExpression::NormalMatch, QRegularExpression::DontCheckSubjectStringMatchOption);
                    if (match.hasMatch()) {
                        talks.append(match.captured("theme"));
                    }
                }
            }
        }
        if (talks.count() > 0) {
            QString first = talks.first();
            QString talkPostfix_ = "";
            bool ok = true;
            for (int i = 0; ok && i < 10; i++) {
                talkPostfix = talkPostfix_;
                talkPostfix_ = first.right(i);
                QString t;
                foreach (t, talks) {
                    if (!t.endsWith(talkPostfix_)) {
                        ok = false;
                        break;
                    }
                }
            }
            sql->saveSetting(talkPostfixName, talkPostfix);
            newSettingValues.insert(talkPostfixName, talkPostfix);
        }
    }

    int talknum(LMM_Schedule::TalkType_LivingTalk1);
    bool hasNonMatchedTalk(false);
    int sequence(0);
    int roworder(0);
    QMap<int, int> talkIdsFound;
    for (partid = 0; partid <= foundParts::lastItem; partid++) {
        QString target(epubResults[partid]);

        int study(0);
        QRegularExpressionMatch mStudy = rxstudy.match(target, 0, QRegularExpression::NormalMatch, QRegularExpression::DontCheckSubjectStringMatchOption);
        if (mStudy.hasMatch()) {
            study = epb.stringToInt(mStudy.captured(1));
            target = target.left(mStudy.capturedStart()) + target.mid(mStudy.capturedEnd());
        }

        match = rxtiming.match(target, 0, QRegularExpression::NormalMatch, QRegularExpression::DontCheckSubjectStringMatchOption);
        if (match.hasMatch()) {
            int timing(epb.stringToInt(match.captured("timing")));
            QString head(target.left(match.capturedStart()));
            QString tail(target.mid(match.capturedEnd()));
            for (int rxselectedidx = 0; rxselectedidx < assignmentoptions; rxselectedidx++) {
                match = rxasgn[rxselectedidx]->match(head, 0, QRegularExpression::NormalMatch, QRegularExpression::DontCheckSubjectStringMatchOption);
                if (match.hasMatch()) {
                    QString theme = match.captured("theme");
                    if (theme.endsWith(talkPostfix))
                        theme = theme.mid(0, theme.length() - talkPostfix.length());
                    int idx(talkid_index[partid]);
                    int talkID(idx > 0 ? idx : 0);
                    if (talkID < 1 && partid <= foundParts::br)
                        talkID = partid;
                    else if (talkID < 1) {
                        if (partid >= foundParts::fm1 && partid <= foundParts::fm4) {
                            QString talkName = "Talk Type: " + theme;
                            QString talkID_ = sql->getSetting(talkName, "");
                            if (talkID_.isEmpty()) {
                                if (timing > 7) {
                                    // surely this is the video part...we hope
                                    talkID = LMM_Schedule::TalkType_OtherFMVideoPart;
                                } else {
                                    hasNonMatchedTalk = true;
                                    sql->saveSetting(talkName, "-1");
                                    newSettingValues.insert(talkName, "-1");
                                    talkID = -1;
                                }
                            } else {
                                talkID = talkID_.toInt();
                                if (talkID < 0)
                                    hasNonMatchedTalk = true;
                            }
                        } else if (partid >= foundParts::talk1 && partid <= foundParts::talk5) {
                            if (timing == 30)
                                talkID = LMM_Schedule::TalkType_CBS;
                            else
                                talkID = talknum++;
                        }
                    }
                    if (talkID > 0) {
                        if (!talkIdsFound.contains(talkID)) {
                            talkIdsFound.insert(talkID, 0);
                            sequence = 0;
                        } else {
                            talkIdsFound[talkID]++;
                            sequence = talkIdsFound[talkID];
                        }
                        InsertSchedule(talkID, sequence, roworder++, dt, theme, match.captured("source"), tail, timing, study);
                        talkid_index[partid] = talkID;
                        if (idx > 0 && LMM_Schedule::canMultiSchool(talkID) && talkID != LMM_Schedule::TalkType_OtherFMVideoPart)
                            sql->saveSetting("Talk Type: " + theme, QVariant(talkID).toString());
                    }
                    break;
                }
            }
        }
    }

    saveTalkIndex(dt, talkid_index);

    qDeleteAll(rxasgn);

    if (hasNonMatchedTalk && err.isEmpty())
        err = QObject::tr("Please select the Talk Names to match the names we found in the workbook");
}

void importlmmworkbook::xmlPartFound(QXmlStreamReader *xml, QXmlStreamReader::TokenType tokenType, int context, int relativeDepth)
{
    bool grabText = false;

    QStringRef name = xml->name();
    // qDebug() << name << "tokentype " << tokenType << " context " << context;

    if (ignoreTextDepth > -1 && ignoreTextContext != context) {
        ignoreTextDepth = -1;
        ignoreTextContext = -1;
    }

    switch (context) {

    case xmlPartsContexts::bibleReadingDetail:
        epubSectionOffset = 0;
        epubliIdx = foundParts::biblereading;
        grabText = true;
        break;
    case xmlPartsContexts::section1:
        // n/a
        break;
    case xmlPartsContexts::songDetail:
        epubliIdx = foundParts::opensong;
        grabText = true;
        break;
    case xmlPartsContexts::section2:
        epubSectionOffset = foundParts::treasures;
        epubliIdx = -1; // allow for ++ later
        break;
    case xmlPartsContexts::section3:
        epubSectionOffset = foundParts::fm1;
        epubliIdx = -1; // allow for ++ later
        break;
    case xmlPartsContexts::section4:
        epubSectionOffset = foundParts::midsong;
        epubliIdx = -1; // allow for ++ later
        break;
    case xmlPartsContexts::talk:
        if (relativeDepth == 0 && tokenType == QXmlStreamReader::StartElement)
            epubliIdx++;
        grabText = true;
        break;
    }

    if (grabText) {
        QString txt;
        //QStringRef name = xml->name();
        if (name == "strong")
            txt.append("•");
        else if (name == "li" && tokenType == QXmlStreamReader::StartElement && relativeDepth > 0)
            txt.append("~~");

        if (name == "a" && xml->attributes().value("href").contains("footnote")) {
            ignoreTextDepth = relativeDepth;
            ignoreTextContext = context;
        }
        if (ignoreTextDepth > -1 && (ignoreTextDepth < relativeDepth || ignoreTextContext != context)) {
            ignoreTextDepth = -1; // we are done ignoring text
            ignoreTextContext = -1;
        }
        if (ignoreTextDepth < 0)
            txt.append(xml->text().toString().replace("\n", "~~").replace("\r", "~~"));

        epubResults[epubSectionOffset + epubliIdx] += txt;
    }
}

bool importlmmworkbook::DownloadAssistDoc(QDate sampleDate)
{
    QNetworkAccessManager manager;
    QEventLoop q;
    QTimer timer;
    bool imported(false);

    timer.setSingleShot(true);
    QObject::connect(&timer, SIGNAL(timeout()), &q, SLOT(quit()));
    QObject::connect(&manager, SIGNAL(finished(QNetworkReply *)),
                     &q, SLOT(quit()));

    QNetworkRequest request;
    QString sUrl;
    if (sampleDate.isValid())
        sUrl = "https://www.theocbase.net/workbook/lmmassist" + sampleDate.toString("yyMM") + ".txt";
    else
        sUrl = "https://www.theocbase.net/workbook/lmmassist.txt"; // load something so first-time users have good mappings
    request.setUrl(QUrl(sUrl));
    QNetworkReply *reply = manager.get(request);

    timer.start(10000); // 10s timeout
    q.exec();

    if (timer.isActive()) {
        // download complete
        timer.stop();
        if (reply->error() != QNetworkReply::NoError) {
            qDebug() << "DownloadAssistDoc error" << reply->errorString();
        } else {
            char ln[20];
            int talkid_index[15] { 0 };
            bool ctinue(true);
            QDate dt;
            while (ctinue) {
                ctinue = reply->readLine(ln, 20) != -1;
                if (ctinue) {
                    QString ln2(ln);
                    QStringList parts(ln2.split('\t'));
                    QDate dt2 = QDate::fromString(parts[0], "yyyy-MM-dd");
                    if (dt.isValid()) {
                        if (dt != dt2) {
                            saveTalkIndex(dt, talkid_index);
                            for (int i = 0; i < 15; i++)
                                talkid_index[i] = 0;
                        }
                    }
                    talkid_index[QVariant(parts[1]).toInt()] = QVariant(parts[2]).toInt();
                    dt = dt2;
                } else if (dt.isValid()) {
                    saveTalkIndex(dt, talkid_index);
                }
            }
            imported = true;
        }
    }

    if (reply != nullptr)
        delete reply;
    return !imported;
}

// separate method so we can do some additional cleanup on theme or source
void importlmmworkbook::InsertSchedule(int talkId, int sequence, int roworder, QDate date, QString theme, QString source1, QString source2, int time, int studyNumber)
{
    QString source(source1.length() > 3 ? source1.trimmed() : source2.trimmed());
    if (debugMode) {
        meetingResults.append(
                LMM_Schedule::getStringTalkType(talkId) + " (seq " + QString::number(sequence) + "): " + cleanText(theme.trimmed()) + " / min " + QString::number(time) + ", study " + QString::number(studyNumber) + " / " + cleanText(source));
    } else {
        LMM_Schedule sch(talkId, sequence, date, cleanText(theme.trimmed()), cleanText(source), studyNumber, time);
        sch.setRoworder(roworder);
        sch.save();
    }
}

QString importlmmworkbook::cleanText(QString text)
{
    while (text.contains("~~~")) {
        text = text.replace("~~~", "~~");
    }
    while (text.endsWith("~~")) {
        text = text.left(text.length() - 2);
    }
    text = text.replace("~~", "\r\n");
    while (text.startsWith('.') || text.startsWith("•") || text.startsWith(':') || text.startsWith('\r') || text.startsWith('\n')) {
        text = text.mid(1).trimmed();
    }
    while (text.endsWith(':') || text.endsWith("•") || text.endsWith('\r') || text.endsWith('\n')) {
        text = text.left(text.length() - 1).trimmed();
    }
    int space(-1);
    while ((space = text.indexOf("  ")) > -1) {
        text = text.left(space - 1) + text.mid(space + 1);
    }

    return text;
}

void importlmmworkbook::getTalkIdIndex(QDate dt, int (&talkid_index)[15])
{
    sql_item thisdate;
    thisdate.insert(":date", dt);
    sql_items temp = sql->selectSql("select position, talk_id from lmm_schedule_assist where date = :date", &thisdate);
    for (sql_item v : temp) {
        bool ok(false);
        int position(v["position"].toInt(&ok));
        if (ok && position < 15) {
            talkid_index[position] = v["talk_id"].toInt();
        }
    }
}

void importlmmworkbook::saveTalkIndex(QDate dt, int (&talkid_index)[15])
{
    for (int i = 0; i < 15; i++) {
        if (talkid_index[i]) {
            sql_item data;
            data.insert(":date", dt);
            data.insert(":position", i);
            data.insert(":talk_id", talkid_index[i]);
            sql->execSql("insert or replace into lmm_schedule_assist(date, position, talk_id) values(:date, :position, :talk_id)", &data);
        }
    }
}
