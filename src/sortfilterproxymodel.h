#ifndef SORTFILTERPROXYMODEL
#define SORTFILTERPROXYMODEL

#include <QSortFilterProxyModel>
#include <QStandardItemModel>
#include <QString>
#include <QDebug>
#include "sql_class.h"

class SortFilterProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT

    Q_PROPERTY(int sortColumn READ sortColumn NOTIFY sortColumnChanged)
    Q_PROPERTY(Qt::SortOrder sortOrder READ sortOrder NOTIFY sortColumnChanged)

public:
    explicit SortFilterProxyModel(QObject *parent = nullptr, QStandardItemModel *itemmodel = nullptr, int defaultSortRole = 0, QString settingName = "") : QSortFilterProxyModel(parent){
        sql_class *sql = &Singleton<sql_class>::Instance();
        setSourceModel(itemmodel);
        int sortcol(Qt::UserRole+1);
        if (defaultSortRole > 0)
            sortcol = defaultSortRole - Qt::UserRole -1;
        int sortorder(Qt::AscendingOrder);
        if (!settingName.isEmpty()) {
            sortcol = sql->getIntSetting(settingName + "_sort_col", defaultSortRole - Qt::UserRole - 1);
            sortorder = sql->getIntSetting(settingName + "_sortorder", Qt::AscendingOrder);
            m_settingName = settingName;
        }        
        setSortRole(Qt::UserRole+1+sortcol);
        sort(sortcol, static_cast<Qt::SortOrder>(sortorder));
        setDynamicSortFilter(true);
    }

    Q_INVOKABLE virtual void sort(int column, Qt::SortOrder order = Qt::AscendingOrder){
        qDebug() << "Sorting" << column << order;

        if (!m_settingName.isEmpty()) {
            sql_class *sql = &Singleton<sql_class>::Instance();
            sql->saveSetting(m_settingName + "_sort_col", QString::number(column));
            sql->saveSetting(m_settingName + "_sortorder", QString::number(order));
        }
        setSortRole(Qt::UserRole+1+column);
        QSortFilterProxyModel::sort(0,order);        
        emit sortColumnChanged();
    }

    Q_INVOKABLE QVariantMap get(int idx) const {
        QVariantMap map;
        foreach(int k, roleNames().keys()) {
            map[roleNames().value(k)] = data(index(idx, 0), k);
        }        
        return map;
    }

    Q_INVOKABLE int find(QVariant value, int col){
        int proxyLength = rowCount();
        for(int i=0;i<proxyLength;i++){
            QModelIndex idx = index(i,0);
            QVariant data = idx.data(Qt::UserRole+col);
            if (data == value)
                return i;
        }
        return -1;
    }

    int sortColumn() const  {
        return sortRole() - 257;
    }

signals:
    void sortColumnChanged();

private:
    QString m_settingName;
};

#endif // SORTFILTERPROXYMODEL

