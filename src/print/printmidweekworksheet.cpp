/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "printmidweekworksheet.h"

PrintMidweekWorksheet::PrintMidweekWorksheet()
{
    currentTemplateData = QSharedPointer<TemplateData>(
            new TemplateData(TemplateData::MidweekWorksheetTemplate, "schoolTemplateWS", "MW-Worksheet.htm", "mw-worksheet"));
}

QVariant PrintMidweekWorksheet::fillTemplate()
{
    QString str = getTemplate();
    str = initLayout(str);
    str = ifthen.tokenize(str);
    str = fillTemplateBlockLMMOverall(str);
    fillWeekList(0);

    QPair<QString, QString> sec = templateGetSection(str, "!REPEAT_START!", "!REPEAT_END!", "lmm");

    QString basepage = sec.first;
    QString section = sec.second;

    school s;
    int classcnt(s.getClassesQty());
    QString abc("ABC");
    QString iii("III");
    bool isFirst(true);

    QString weekContext = "";

    for (weekInfo *w : weekList) {
        currentWeek = w;
        //QDate tempdate(w->week);
        for (int c = 1; c <= classcnt; c++) {
            QString classLetter(abc.mid(c - 1, 1));
            QString classNumeral(iii.left(c));
            QString oneweek(section);
            oneweek = oneweek.replace(QRegularExpression("!(GW3|FM[1-9])(_SPEAKER!)"), "!\\1_SPEAKER_" + classLetter + "!");
            oneweek = oneweek.replace("_ASSISTANT!", "_ASSISTANT_" + classLetter + "!");
            oneweek = oneweek.replace("_NOTES!", "_NOTES_" + classLetter + "!");
            oneweek = oneweek.replace("_EX_DONE!", "_" + classLetter + "_EX_DONE!");
            oneweek = oneweek.replace("_STUDYNAME!", "_" + classLetter + "_STUDYNAME!");
            oneweek = oneweek.replace("_STUDY!", "_" + classLetter + "_STUDY!");
            oneweek = oneweek.replace("!CLASS!", tr("Class ") + " " + classNumeral);
            oneweek = oneweek.replace("!CLASS_SCHEDULED!", "!CLASS_" + classLetter + "_SCHEDULED!");

            oneweek = fillTemplateBlockLMM(oneweek, true);
            if (oneweek.length() > 0) {
                if (isFirst)
                    isFirst = false;
                else
                    weekContext.append(addCssPageBreak());
                weekContext.append(oneweek);
            }
        }
    }
    basepage.replace("<div class=\"lmm\"></div>", "<div>" + weekContext + "</div>");

    return (ifthen.compute(basepage));
}
