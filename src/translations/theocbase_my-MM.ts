<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="my-MM">
<context>
    <name>AssignmentPanel</name>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="51"/>
        <source>Theme</source>
        <translation>အကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="82"/>
        <source>Source</source>
        <translation>ရင်းမြစ်</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="109"/>
        <source>Chairman</source>
        <translation>သဘာပတိ</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="109"/>
        <source>Speaker</source>
        <translation>ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="120"/>
        <source>Selected</source>
        <comment>Dropdown column title</comment>
        <translation>ရွေးထားတဲ့</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="121"/>
        <source>All</source>
        <comment>Dropdown column title</comment>
        <translation>အားလုံး</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="146"/>
        <source>Note</source>
        <translation>မှတ်ချက်</translation>
    </message>
</context>
<context>
    <name>CBSPanel</name>
    <message>
        <location filename="../qml/CBSPanel.qml" line="43"/>
        <source>Theme</source>
        <translation>အကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="64"/>
        <source>Source</source>
        <translation>_ူဒဖ</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="84"/>
        <source>Conductor</source>
        <translation>သင်တန်းကိုင်</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="90"/>
        <source>CBS conductor</source>
        <comment>Dropdown column title</comment>
        <translation>CBS သင်တန်းမှူး</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="91"/>
        <source>Any CL assignment</source>
        <comment>Dropdown column title</comment>
        <translation>ခရစ်ယာန်အသက်တာ တာဝန်</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="108"/>
        <source>Reader</source>
        <translation>စာဖတ်သူ</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="131"/>
        <source>Note</source>
        <translation>မှတ်ချက်</translation>
    </message>
</context>
<context>
    <name>ComboBoxTable</name>
    <message>
        <location filename="../qml/ComboBoxTable.qml" line="194"/>
        <source>Name</source>
        <translation>နာမည်</translation>
    </message>
    <message>
        <location filename="../qml/ComboBoxTable.qml" line="200"/>
        <source>Date</source>
        <translation>ရက်စွဲ</translation>
    </message>
</context>
<context>
    <name>CongregationMap</name>
    <message>
        <location filename="../qml/CongregationMap.qml" line="68"/>
        <source>Display congregation address</source>
        <comment>Display marker at the location of the congregation on the map</comment>
        <translation>အသင်းတော်လိပ်စာ ပြပါ</translation>
    </message>
</context>
<context>
    <name>DropboxSettings</name>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="79"/>
        <source>Are you sure you want to permanently delete your cloud data?</source>
        <translation>အွန်လိုင်း ဒေတာ အတည်ဖျက်ချင်တာ သေချာလား?</translation>
    </message>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="217"/>
        <source>Last synchronized: %1</source>
        <translation>နောက်ဆုံး တပြေးညီ ချိန်ညှိခဲ့ %1</translation>
    </message>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="221"/>
        <source>Synchronize</source>
        <translation>တပြေးညီ ချိန်ညှိမယ်</translation>
    </message>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="233"/>
        <source>Delete Cloud Data</source>
        <translation>အွန်လိုင်းဒေတာ ဖျက်မယ်</translation>
    </message>
</context>
<context>
    <name>HelpViewer</name>
    <message>
        <location filename="../helpviewer.cpp" line="81"/>
        <source>TheocBase Help</source>
        <translation>TheocBase အကူအညီ</translation>
    </message>
    <message>
        <location filename="../helpviewer.cpp" line="82"/>
        <source>Unable to launch the help viewer (%1)</source>
        <translation>အကူအညီစာမျက်နှာ (%1) ကို ဖွင့်လို့မရ</translation>
    </message>
</context>
<context>
    <name>LMMNotesPanel</name>
    <message>
        <location filename="../qml/LMMNotesPanel.qml" line="45"/>
        <source>Notes</source>
        <translation>မှတ်စု</translation>
    </message>
</context>
<context>
    <name>LMM_Assignment</name>
    <message>
        <location filename="../lmm_assignment.cpp" line="180"/>
        <source>Please find below details of your assignment:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="181"/>
        <source>Date</source>
        <translation>ရက်</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="182"/>
        <source>Name</source>
        <translation>နာမည်</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="183"/>
        <source>Assignment</source>
        <translation>တာဝန်</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="184"/>
        <source>Theme</source>
        <translation>အဓိကအချက်</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="185"/>
        <source>Source material</source>
        <translation>ရင်းမြစ်</translation>
    </message>
</context>
<context>
    <name>LMM_AssignmentContoller</name>
    <message>
        <location filename="../lmm_assignmentcontoller.cpp" line="65"/>
        <source>Do not assign the next study</source>
        <translation>နောက်သွန်သင်ချက်အတွက် တာဝန် မပေးပါနဲ့</translation>
    </message>
</context>
<context>
    <name>LMM_Assignment_ex</name>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="179"/>
        <source>Please find below details of your assignment:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="180"/>
        <source>Date</source>
        <translation>ရက်</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="181"/>
        <source>Name</source>
        <translation>နာမည်</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="184"/>
        <source>Reader</source>
        <translation>စာဖတ်သူ</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="186"/>
        <source>Assistant</source>
        <translation>အကူ</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="188"/>
        <source>Assignment</source>
        <translation>တာဝန်</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="191"/>
        <source>Study</source>
        <translation>သွန်သင်ချက်</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="193"/>
        <source>Theme</source>
        <translation>အဓိကအချက်</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="194"/>
        <source>Source material</source>
        <translation>ရင်းမြစ်</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="199"/>
        <source>Main hall</source>
        <translation>ပင်မ ခန်းမ</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="202"/>
        <source>Auxiliary classroom 1</source>
        <translation>အရန်စာသင်ခန်း ၁</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="205"/>
        <source>Auxiliary classroom 2</source>
        <translation>အရန်စာသင်ခန်း ၂</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="208"/>
        <source>To be given in</source>
        <comment>Refer to main hall or aux. classroom. See S-89</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="253"/>
        <source>See your be book</source>
        <comment>Counsel point is not known yet. See your &apos;Ministry School&apos; book.</comment>
        <translation>ကျောင်းစာအုပ်ကို ကြည့်ပါ</translation>
    </message>
</context>
<context>
    <name>LMM_Meeting</name>
    <message>
        <location filename="../lmm_meeting.cpp" line="621"/>
        <source>Enter source material here</source>
        <translation>ရင်းမြစ် အချက်အလက် ဒီနေရာမှာ ရိုက်ထည့်ပါ</translation>
    </message>
</context>
<context>
    <name>LMM_Schedule</name>
    <message>
        <location filename="../lmm_schedule.cpp" line="138"/>
        <location filename="../lmm_schedule.cpp" line="183"/>
        <source>Chairman</source>
        <comment>talk title</comment>
        <translation>သဘာပတိ</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="140"/>
        <location filename="../lmm_schedule.cpp" line="184"/>
        <source>Treasures From God’s Word</source>
        <comment>talk title</comment>
        <translation>ကျမ်းစာထဲက အဖိုးတန်ဘဏ္ဍာများ</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="185"/>
        <source>Digging for Spiritual Gems</source>
        <comment>talk title</comment>
        <translation>ရှာဖွေလေ့လာ ဘုရားရေးရာရတနာ</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="144"/>
        <location filename="../lmm_schedule.cpp" line="187"/>
        <source>Bible Reading</source>
        <comment>talk title</comment>
        <translation>ကျမ်းစာဖတ်ရှုခြင်း</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="150"/>
        <location filename="../lmm_schedule.cpp" line="190"/>
        <source>Initial Call</source>
        <comment>talk title</comment>
        <translation>ဦးဆုံးတွေ့ဆုံချိန်</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="146"/>
        <location filename="../lmm_schedule.cpp" line="188"/>
        <source>Sample Conversation Video</source>
        <comment>talk title</comment>
        <translation>နမူနာ စကားပြော ဗီဒီယို</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="142"/>
        <location filename="../lmm_schedule.cpp" line="186"/>
        <source>Spiritual Gems</source>
        <comment>talk title</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="152"/>
        <location filename="../lmm_schedule.cpp" line="191"/>
        <source>First Return Visit</source>
        <comment>talk title</comment>
        <translation>ဦးဆုံး ပြန်လည်ပတ်မှု</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="154"/>
        <location filename="../lmm_schedule.cpp" line="192"/>
        <source>Second Return Visit</source>
        <comment>talk title</comment>
        <translation>ဒုတိယအကြိမ် ပြန်လည်ပတ်မှု</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="156"/>
        <location filename="../lmm_schedule.cpp" line="193"/>
        <source>Third Return Visit</source>
        <comment>talk title</comment>
        <translation>တတိယအကြိမ်  ပြန်လည်ပတ်မှု</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="158"/>
        <location filename="../lmm_schedule.cpp" line="194"/>
        <source>Bible Study</source>
        <comment>talk title</comment>
        <translation>ကျမ်းစာသင်အံမှု</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="160"/>
        <location filename="../lmm_schedule.cpp" line="195"/>
        <source>Talk</source>
        <comment>talk title</comment>
        <translation>ဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="162"/>
        <location filename="../lmm_schedule.cpp" line="196"/>
        <source>Living as Christians Talk 1</source>
        <comment>talk title</comment>
        <translation>ခရစ်ယာန်အသက်တာလမ်းစဉ် ဟောပြောချက် ၁</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="164"/>
        <location filename="../lmm_schedule.cpp" line="197"/>
        <source>Living as Christians Talk 2</source>
        <comment>talk title</comment>
        <translation>ခရစ်ယာန်အသက်တာလမ်းစဉ် ဟောပြောချက် ၂</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="166"/>
        <location filename="../lmm_schedule.cpp" line="198"/>
        <source>Living as Christians Talk 3</source>
        <comment>talk title</comment>
        <translation>ခရစ်ယာန်အသက်တာလမ်းစဉ် ဟောပြောချက် ၃</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="168"/>
        <location filename="../lmm_schedule.cpp" line="199"/>
        <source>Congregation Bible Study</source>
        <comment>talk title</comment>
        <translation>အသင်းတော်ကျမ်းစာလေ့လာမှု</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="170"/>
        <location filename="../lmm_schedule.cpp" line="200"/>
        <source>Circuit Overseer&#x27;s Talk</source>
        <comment>talk title</comment>
        <translation>တိုက်နယ် ဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="172"/>
        <location filename="../lmm_schedule.cpp" line="201"/>
        <source>Memorial Invitation</source>
        <comment>talk title</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="174"/>
        <location filename="../lmm_schedule.cpp" line="202"/>
        <source>Other Video Part</source>
        <comment>talk title</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="189"/>
        <source>Apply Yourself to Reading and Teaching</source>
        <comment>talk title</comment>
        <translation>ဖတ်ရှုခြင်းနဲ့ သွန်သင်ခြင်းမှာ ကြိုးစားပါ</translation>
    </message>
</context>
<context>
    <name>Login</name>
    <message>
        <location filename="../qml/Login.qml" line="61"/>
        <source>Username or Email</source>
        <translation>အသုံးပြုသူနာမည်/အီးမေးလ်</translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="69"/>
        <source>Password</source>
        <translation>လျှို့ဝှက်စာသား</translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="78"/>
        <source>Login</source>
        <translation>အကောင့်ဝင်</translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="97"/>
        <source>Forgot Password</source>
        <translation>လျှို့ဝှက်စာသား မေ့သွား</translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="111"/>
        <source>Create Account</source>
        <translation>အကောင့်အသစ်ဖွင့်မယ်</translation>
    </message>
</context>
<context>
    <name>MAC_APPLICATION_MENU</name>
    <message>
        <location filename="../main.cpp" line="62"/>
        <source>Services</source>
        <translation>၀န်ဆောင်မူများ</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="63"/>
        <source>Hide %1</source>
        <translation>%1 ကွယ်ထားမယ်</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="64"/>
        <source>Hide Others</source>
        <translation>တခြားဟာတွေ ကွယ်ထားမယ်</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="65"/>
        <source>Show All</source>
        <translation>အားလုံး ပြပါ</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="66"/>
        <source>Preferences...</source>
        <translation>နှစ်သက်မှု...</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="67"/>
        <source>Quit %1</source>
        <translation>%1 ထွက်မယ်</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="68"/>
        <source>About %1</source>
        <translation>%1 အကြောင်း</translation>
    </message>
</context>
<context>
    <name>MWMeetingChairmanPanel</name>
    <message>
        <location filename="../qml/MWMeetingChairmanPanel.qml" line="81"/>
        <source>Chairman</source>
        <translation>သဘာပတိ</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingChairmanPanel.qml" line="102"/>
        <source>Auxiliary Classroom Counselor II</source>
        <translation>အရန်သွန်သင်သူ ၂</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingChairmanPanel.qml" line="125"/>
        <source>Auxiliary Classroom Counselor III</source>
        <translation>အရန်သွန်သင်သူ ၃</translation>
    </message>
</context>
<context>
    <name>MWMeetingModule</name>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="223"/>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>ကျမ်းစာထဲက အဖိုးတန်ဘဏ္ဍာများ</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="233"/>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>အမှုဆောင်လုပ်ငန်းမှာ ကျင့်သုံးပါ</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="238"/>
        <source>LIVING AS CHRISTIANS</source>
        <translation>ခရစ်ယာန်အသက်တာလမ်းစဥ်</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="253"/>
        <location filename="../qml/MWMeetingModule.qml" line="274"/>
        <source>Midweek Meeting</source>
        <translation>ကြားရက်စည်းဝေး</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="270"/>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="284"/>
        <source>Import Schedule...</source>
        <translation>အစီအစဥ်ဇယား တင်သွင်းမယ်...</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="332"/>
        <location filename="../qml/MWMeetingModule.qml" line="531"/>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation>၁</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="333"/>
        <location filename="../qml/MWMeetingModule.qml" line="532"/>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation>၂</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="334"/>
        <location filename="../qml/MWMeetingModule.qml" line="533"/>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation>အရန်၂</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="343"/>
        <source>Chairman</source>
        <translation>သဘာပတိ</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="343"/>
        <source>Counselor</source>
        <translation>အကြံပေးသူ</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="369"/>
        <location filename="../qml/MWMeetingModule.qml" line="420"/>
        <source>Song %1 and Prayer</source>
        <translation>သီချင်း %1 နဲ့ ဆုတောင်းချက်</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="383"/>
        <source>Opening Comments</source>
        <translation>နိဒါန်း</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="403"/>
        <source>Concluding Comments</source>
        <translation>အပိတ် ဆုတောင်းချက်</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="496"/>
        <source>Song %1</source>
        <translation>သီချင်း %1</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="517"/>
        <source>Conductor</source>
        <translation>သင်တန်းကိုင်</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="519"/>
        <source>Reader</source>
        <translation>စာဖတ်သူ</translation>
    </message>
</context>
<context>
    <name>MWMeetingPrayerPanel</name>
    <message>
        <location filename="../qml/MWMeetingPrayerPanel.qml" line="47"/>
        <source>Prayer</source>
        <translation>ဆုတောင်းချက်</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="437"/>
        <source>No meeting</source>
        <translation>အစည်းအဝေး မရှိပါ</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="617"/>
        <source>Conductor:</source>
        <translation>သင်တန်းကိုင်သူ:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="622"/>
        <source>Reader:</source>
        <translation>စာဖတ်သူ:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="482"/>
        <source>Copyright</source>
        <translation>မူပိုင်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="490"/>
        <source>Qt libraries licensed under the GPL.</source>
        <translation>Qt libraries licensed under the GPL.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="482"/>
        <source>TheocBase Team</source>
        <translation>TheocBase အဖွဲ့</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="286"/>
        <source>Last synchronized</source>
        <translation>နောက်ဆုံး တပြေးညီ ချိန်ညှိခဲ့</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="485"/>
        <source>Licensed under GPLv3.</source>
        <translation>Licensed under GPLv3.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="491"/>
        <source>Versions of Qt libraries </source>
        <translation>Versions of Qt libraries </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1266"/>
        <source>TheocBase data exchange</source>
        <translation>TheocBase အချက်အလက် ဖလှယ်ခြင်း</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="554"/>
        <source>New update available. Do you want to install?</source>
        <translation>အသစ် ထပ်ဆင့်ပြင်ဆင်မှု ရှိတယ်၊ သွင်းချင်လား?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="558"/>
        <source>No new update available</source>
        <translation>အသစ် ထပ်ဆင့်မြင့်မှု မရှိပါ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="584"/>
        <source>Save file</source>
        <translation>ဖိုင် သိမ်းမယ်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1636"/>
        <source>Select ePub file</source>
        <translation>ePub ဖိုင် ရွေးပါ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1555"/>
        <source>Send e-mail reminders?</source>
        <translation>အီးမေးလ် သတိပေးချက် ပို့မလား?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1589"/>
        <source>Updates available...</source>
        <translation>အသစ် ထပ်ဆင့်မြင့်မှုတွေ ရှိတယ်...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1169"/>
        <source>Error sending e-mail</source>
        <translation>အီးမေးလ် ပို့လို့မရဘူး ဖြစ်နေတယ်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="408"/>
        <source>WEEK STARTING %1</source>
        <translation>%1 နဲ့ စတဲ့ အပတ်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="573"/>
        <source>Exporting outgoing speakers not ready yet, sorry.</source>
        <translation>သွားလည် ဟောပြောသူတွေ တင်ပို့ဖို့ အဆင်သင့် မဖြစ်သေးပါ၊ တောင်းပန်ပါတယ်။</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="577"/>
        <source>Exporting study history to iCal is not supported</source>
        <translation>သင်ခန်းစာမှတ်တမ်းကို iCal ဆီ ပို့လို့မရပါ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="588"/>
        <source>Save folder</source>
        <translation>ဖိုင်တွဲ သိမ်းမယ်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1171"/>
        <source>E-mail sent successfully</source>
        <translation>အီးမေးလ် ရောက်သွားပြီ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="601"/>
        <source>Saved successfully</source>
        <translation>သိမ်းတာ အောင်မြင်တယ်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="661"/>
        <location filename="../mainwindow.cpp" line="666"/>
        <source>Counselor-Class II</source>
        <translation>သွန်သင်သူ အတန်း II</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="662"/>
        <location filename="../mainwindow.cpp" line="667"/>
        <source>Counselor-Class III</source>
        <translation>သွန်သင်သူ အတန်း III</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="682"/>
        <source>Assistant to %1</source>
        <comment>%1 is student&apos;s name</comment>
        <translation>%1 အတွက် အကူ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="749"/>
        <location filename="../mainwindow.cpp" line="964"/>
        <location filename="../mainwindow.cpp" line="989"/>
        <source>Kingdom Hall</source>
        <translation>နိုင်ငံတော်ခန်းမ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="660"/>
        <location filename="../mainwindow.cpp" line="665"/>
        <location filename="../mainwindow.cpp" line="755"/>
        <location filename="../mainwindow.cpp" line="766"/>
        <source>Chairman</source>
        <translation>သဘာပတိ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="689"/>
        <source>Reader for Congregation Bible Study</source>
        <translation>အသင်းတော် ကျမ်းစာလေ့လာမှု အတွက် စာဖတ်သူ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="674"/>
        <location filename="../mainwindow.cpp" line="690"/>
        <source>Source</source>
        <comment>short for Source material</comment>
        <translation>ရင်းမြစ်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="663"/>
        <location filename="../mainwindow.cpp" line="668"/>
        <location filename="../mainwindow.cpp" line="713"/>
        <location filename="../mainwindow.cpp" line="715"/>
        <source>Prayer</source>
        <translation>ဆုတောင်းချက်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="674"/>
        <source>Timing</source>
        <translation>အချိန်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="755"/>
        <location filename="../mainwindow.cpp" line="920"/>
        <source>Public Talk</source>
        <translation>လူထုဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="755"/>
        <source>Watchtower Study</source>
        <translation>ကင်းမျှော်စင် လေ့လာမှု</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="772"/>
        <source>Speaker</source>
        <translation>ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="777"/>
        <source>Watchtower Study Conductor</source>
        <translation>ကင်းမျှော်စင်လေ့လာမှု သင်တန်းကိုင်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="779"/>
        <source>Watchtower reader</source>
        <translation>ကင်းမျှော်စင် ဖတ်သူ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1025"/>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want to keep the local changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1066"/>
        <source>The cloud data has now been deleted.</source>
        <translation>အွန်လိုင်း ဒေတာ ဖျက်လိုက်ပြီ။</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1070"/>
        <source>Synchronize</source>
        <translation>တပြေးညီ ဆောင်ရွက်မယ်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1072"/>
        <source>Sign Out</source>
        <translation>ထွက်မယ်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1082"/>
        <source>The cloud data has been deleted. Your local data will be replaced. Continue?</source>
        <translation>အွန်လိုင်းဒေတာ ဖျက်လိုက်ပြီ။ သင့်ကွန်ပျူတာမှာရှိတဲ့ ဒေတာကို အစားထိုးတော့မယ်။ ဆက်လုပ်မလား?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1180"/>
        <source>Open file</source>
        <translation>ဖိုင် ဖွင့်မယ်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1207"/>
        <source>Open directory</source>
        <translation>ဖိုင်တွဲ ဖွင့်မယ်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1238"/>
        <source>Import Error</source>
        <translation>တင်သွင်းလို့ မရပါ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1238"/>
        <source>Could not import from Ta1ks. Files are missing:</source>
        <translation>Ta1ks ကနေ တင်သွင်းလို့ မရဘူး။ ဖိုင်တွေ ပျောက်နေတယ်၊</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1253"/>
        <source>Save unsaved data?</source>
        <translation>မသိမ်းရသေးတဲ့ အချက်အလက် သိမ်းမလား?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1262"/>
        <source>Import file?</source>
        <translation>ဖိုင် တင်သွင်းမလား?</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="691"/>
        <source>School assignment</source>
        <translation>ကျောင်းတာဝန်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="293"/>
        <source>Congregation Bible Study:</source>
        <translation>အသင်းတော် ကျမ်းစာလေ့လာမှု</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="327"/>
        <source>Theocratic Ministry School:</source>
        <translation>သီအိုကရက်တစ် ဓမ္မအမှုကျောင်း</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="358"/>
        <source>Service Meeting:</source>
        <translation>လုပ်ငန်းတော်စည်းဝေး</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="806"/>
        <source>Congregation Bible Study</source>
        <translation>အသင်းတော် ကျမ်းစာလေ့လာမှု</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="833"/>
        <source>Theocratic Ministry School</source>
        <translation>သီအိုကရက်တစ် ဓမ္မအမှုကျောင်း</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="860"/>
        <source>Service Meeting</source>
        <translation>လုပ်ငန်းတော်စည်းဝေး</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1103"/>
        <location filename="../mainwindow.ui" line="1368"/>
        <source>Export</source>
        <translation>ထုတ်ရန်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1280"/>
        <source>Public talks</source>
        <translation>လူထုဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1398"/>
        <source>Import</source>
        <translation>ထည့်သွင်းရန်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1432"/>
        <source>info</source>
        <translation>အချက်အလက်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2023"/>
        <source>Data exhange</source>
        <translation>အချက်အလက် ဖလှယ်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2076"/>
        <source>TheocBase Cloud</source>
        <translation>TheocBase Cloud</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1607"/>
        <location filename="../mainwindow.ui" line="1640"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1839"/>
        <source>Timeline</source>
        <translation>အချိန်ဇယား</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1900"/>
        <source>Print...</source>
        <translation>စာရွက်ပရင့်ထုတ်မယ်...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1903"/>
        <source>Print</source>
        <translation>စာရွက်ပရင့်ထုတ်မယ်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1912"/>
        <source>Settings...</source>
        <comment>This means the &apos;Options&apos; of TheocBase</comment>
        <translation>ချိန်ညှိစရာ...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1924"/>
        <source>Publishers...</source>
        <translation>ကြေညာသူ...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1223"/>
        <location filename="../mainwindow.ui" line="1927"/>
        <source>Publishers</source>
        <translation>ကြေညာသူများ</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1993"/>
        <source>Speakers...</source>
        <translation>ဟောပြောသူများ</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1260"/>
        <location filename="../mainwindow.ui" line="1996"/>
        <source>Speakers</source>
        <translation>ဟောပြောသူများ</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2020"/>
        <source>Data exhange...</source>
        <translation>အချက်အလက် ဖလှယ်...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2032"/>
        <source>TheocBase help...</source>
        <translation>TheocBase အကူအညီ...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2047"/>
        <source>History</source>
        <translation>မှတ်တမ်း</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2059"/>
        <location filename="../mainwindow.ui" line="2062"/>
        <source>Full Screen</source>
        <translation>မျက်နှာပြင်အပြည့် ဖွင့်/ပိတ်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2067"/>
        <source>Startup Screen</source>
        <translation>အစစာမျက်နှာ</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2106"/>
        <source>Reminders...</source>
        <translation>သတိပေးချက်များ...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="568"/>
        <source>Theme:</source>
        <translation>အကြောင်းအရာ:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="573"/>
        <source>Speaker:</source>
        <translation>ဟောပြောသူ:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="578"/>
        <source>Chairman:</source>
        <translation>သဘာပတိ:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="583"/>
        <source>Watchtower Study:</source>
        <translation>ကင်းမျှော်စင်လေ့လာမှု:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="527"/>
        <source>Data exchange</source>
        <translation>Data exchange</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1114"/>
        <source>Export Format</source>
        <translation>Export Format</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1120"/>
        <source>For sending data to another user</source>
        <translation>အသုံးပြုသူ နောက်တစ်ယောက်ကို အချက်အလက် ပို့ဖို့အတွက်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1139"/>
        <source>For easy import to Calendar programs</source>
        <translation>ပြက္ခဒိန် အစီအစဥ် တင်သွင်းရတာ လွယ်အောင်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1171"/>
        <source>Export Method</source>
        <translation>တင်ပို့မယ့် နည်းလမ်း</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1184"/>
        <source>Events grouped by date</source>
        <translation>ရက်စွဲအလိုက် အစီအစဥ်များ</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1177"/>
        <source>All day events</source>
        <translation>တစ်နေကုန် အစီအစဥ်များ</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1213"/>
        <source>Midweek Meeting</source>
        <translation>ကြားရက်စည်းဝေး</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1236"/>
        <source>Study History</source>
        <translation>သွန်သင်ချက် မှတ်တမ်း</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1287"/>
        <source>Outgoing Talks</source>
        <translation>သွားလည် ဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1298"/>
        <source>Date Range</source>
        <translation>ရက်စွဲ အကြာအရှည်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1305"/>
        <source>Previous Weeks</source>
        <translation>အရင် အပတ်တွေ</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1319"/>
        <source>From Date</source>
        <translation>ဒီရက်စွဲကစပြီး</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1336"/>
        <source>Thru Date</source>
        <translation>ဒီရက်စွဲအထိ</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1793"/>
        <source>File</source>
        <translation>ဖိုင်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1799"/>
        <source>Tools</source>
        <translation>ကိရိယာများ</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1812"/>
        <source>Help</source>
        <translation>အကူအညီ</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1888"/>
        <source>Today</source>
        <translation>ဒီနေ့</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1936"/>
        <source>Exit</source>
        <translation>ထွက်မယ်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1948"/>
        <source>Report bug...</source>
        <translation>ပြဿနာ သတင်းပို့မယ်...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1960"/>
        <source>Send feedback...</source>
        <translation>အကြံပေးစာ ပို့မယ်...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1978"/>
        <source>About TheocBase...</source>
        <translation>TheocBase အကြောင်း...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2005"/>
        <source>Check updates...</source>
        <translation>အသစ်တင်ထားတာတွေ စစ်မယ်...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2035"/>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2094"/>
        <source>Territories...</source>
        <translation>ရပ်ကွက်...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2097"/>
        <source>Territories</source>
        <translation>ရပ်ကွက်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1867"/>
        <source>Back</source>
        <translation>အနောက်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1879"/>
        <source>Next</source>
        <translation>အရှေ့</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2085"/>
        <source>Date</source>
        <translation>ရက်စွဲ</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1969"/>
        <source>TheocBase website</source>
        <translation>TheocBase ဝဘ်ဆိုက်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="433"/>
        <source>Convention week (no meeting) </source>
        <translation>ခရိုင်စည်းဝေး အပတ် (အသင်းတော်စည်းဝေး မရှိ) </translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="534"/>
        <source>Public Talk:</source>
        <translation>လူထုဟောပြောချက်</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <location filename="../qml/MeetingNotes.qml" line="41"/>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakerEdit</name>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="79"/>
        <source>Speaker</source>
        <translation>ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="108"/>
        <source>Theme</source>
        <translation>အကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="140"/>
        <source>Congregation</source>
        <translation>အသင်းတော်</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="192"/>
        <source>Meeting day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="207"/>
        <source>Meeting time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="225"/>
        <source>Info</source>
        <translation>အခ်က္အလက္</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="171"/>
        <source>Address</source>
        <translation>လိပ်စာ</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakersModel</name>
    <message>
        <location filename="../outgoingspeakersmodel.cpp" line="163"/>
        <source>From %1</source>
        <translation>%1 ကနေ</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakersModule</name>
    <message>
        <location filename="../qml/OutgoingSpeakersModule.qml" line="109"/>
        <source>Outgoing speakers</source>
        <translation>သွားလည် ဟောပြောသူ</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/OutgoingSpeakersModule.qml" line="194"/>
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>ဒီအပတ်ကုန်မှာ ဟောပြောသူ %1 ယောက် မရှိ</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakersModule.qml" line="195"/>
        <source>No speakers away this weekend</source>
        <translation>ဒီစနေတနင်္ဂနွေမှာ ဟောပြောသူအားလုံး ရှိ</translation>
    </message>
</context>
<context>
    <name>PrintCombination</name>
    <message>
        <location filename="../print/printcombination.cpp" line="31"/>
        <source>Select at least one option</source>
        <translation>အနည်းဆုံး တစ်ခုတော့ ရွေးပါ</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="41"/>
        <source>%1 Congregation</source>
        <comment>Congregation_Title Tag. %1 is Congregation Name</comment>
        <translation>%1 အသင်းတော်</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="39"/>
        <source>Congregation</source>
        <translation>အသင်းတော်</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="47"/>
        <source>Chairman</source>
        <translation>သဘာပတိ</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="48"/>
        <source>Counselor</source>
        <translation>အကြံပေးသူ</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="50"/>
        <source>Reader</source>
        <translation>စာဖတ်သူ</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="49"/>
        <source>Conductor</source>
        <translation>သင်တန်းကိုင်</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="51"/>
        <source>Speaker</source>
        <comment>Public talk speaker</comment>
        <translation>ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="44"/>
        <source>Public Meeting</source>
        <translation>လူထုစည်းဝေး</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="45"/>
        <source>Public Talk</source>
        <translation>လူထုဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="46"/>
        <source>Watchtower Study</source>
        <translation>ကင်းမျှော်စင်သင်တန်း</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>&lt;--</source>
        <translation>&lt;--</translation>
    </message>
</context>
<context>
    <name>PrintCombination</name>
    <message>
        <location filename="../print/printcombination.cpp" line="43"/>
        <source>Midweek Meeting</source>
        <translation>ကြားရက်စည်းဝေး</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="42"/>
        <source>Christian Life and Ministry Meeting</source>
        <translation>ခရစ်ယာန်အသက်တာနဲ့ အမှုဆောင်လုပ်ငန်းစည်းဝေး</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="53"/>
        <source>min.</source>
        <comment>Abbreviation of minutes</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="68"/>
        <location filename="../print/printcombination.cpp" line="69"/>
        <source>Combined Schedule</source>
        <translation>ပေါင်းထားတဲ့ အစီအစဥ်ဇယား</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="70"/>
        <source>Weekend Meeting</source>
        <translation>တနင်္ဂနွေစည်းဝေး</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="71"/>
        <source>Outgoing Speakers</source>
        <translation>သွားလည် ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="88"/>
        <source>Week Starting %1</source>
        <comment>%1 is Monday of the week</comment>
        <translation>%1 စမယ့်အပတ်</translation>
    </message>
</context>
<context>
    <name>PrintDocument</name>
    <message>
        <location filename="../print/printdocument.cpp" line="145"/>
        <source>Template not found</source>
        <comment>printing template not found</comment>
        <translation>ပုံစံခွက် မတွေ့ပါ</translation>
    </message>
    <message>
        <location filename="../print/printdocument.cpp" line="150"/>
        <source>Can&#x27;t read file</source>
        <comment>cannot read printing template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printdocument.cpp" line="235"/>
        <source>Service Talk</source>
        <translation>လုပ်ငန်းတော်ဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../print/printdocument.cpp" line="245"/>
        <source>Begins at</source>
        <comment>Used in print template, example &apos;Begins at 11:00&apos;</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintMidweekSchedule</name>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="37"/>
        <source>%1 Congregation</source>
        <comment>Congregation_Title Tag. %1 is Congregation Name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="38"/>
        <source>Congregation</source>
        <translation>အသင်းတော်</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="40"/>
        <source>Public Talk</source>
        <translation>လူထုဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="41"/>
        <source>Watchtower Study</source>
        <translation>ကင်းမျှော်စင်သင်တန်း</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="42"/>
        <location filename="../print/printmidweekschedule.cpp" line="80"/>
        <source>Treasures from God&#x27;s Word</source>
        <translation>ကျမ်းစာထဲက အဖိုးတန်ဘဏ္ဍာများ</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="43"/>
        <location filename="../print/printmidweekschedule.cpp" line="81"/>
        <source>Apply Yourself to the Field Ministry</source>
        <translation>အမှုဆောင်လုပ်ငန်းမှာ ကျင့်သုံးပါ</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="44"/>
        <location filename="../print/printmidweekschedule.cpp" line="82"/>
        <source>Living as Christians</source>
        <translation>ခရစ်ယာန်အသက်တာလမ်းစဥ်</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="69"/>
        <source>Midweek Meeting</source>
        <translation>ကြားရက်စည်းဝေး</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="70"/>
        <source>Worksheet</source>
        <translation>မှတ်တမ်း</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="71"/>
        <location filename="../print/printmidweekschedule.cpp" line="104"/>
        <source>Opening Comments</source>
        <comment>See Workbook</comment>
        <translation>နိဒါန်း</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="72"/>
        <location filename="../print/printmidweekschedule.cpp" line="105"/>
        <source>Concluding Comments</source>
        <comment>See Workbook</comment>
        <translation>အပိတ် ဆုတောင်းချက်</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="73"/>
        <source>Song</source>
        <translation>သီချင်း</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="74"/>
        <source>Prayer</source>
        <translation>ဆုတောင်းချက်</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="75"/>
        <source>Chairman</source>
        <translation>သဘာပတိ</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="76"/>
        <source>Counselor</source>
        <translation>အကြံပေးသူ</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="77"/>
        <source>Conductor</source>
        <translation>သင်တန်းမှူး</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="78"/>
        <source>Reader</source>
        <translation>စာဖတ်သူ</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="79"/>
        <source>Christian Life and Ministry Meeting</source>
        <translation>ခရစ်ယာန်အသက်တာနဲ့ အမှုဆောင်လုပ်ငန်းစည်းဝေး</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="83"/>
        <source>Congregation Bible Study</source>
        <translation>အသင်းတော်ကျမ်းစာလေ့လာမှု</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="84"/>
        <source>Circuit Overseer</source>
        <translation>တိုက်နယ်ကြီးကြပ်မှူး</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="85"/>
        <source>No regular meeting</source>
        <translation>ပုံမှန်အစည်းအဝေး မရှိ</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="86"/>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>အကူ</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="87"/>
        <source>Study</source>
        <comment>Counsel point</comment>
        <translation>သင်ခန်းစာ</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="88"/>
        <source>Theme</source>
        <comment>Talk Theme description from workbook</comment>
        <translation>အဓိကအချက်</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="89"/>
        <source>Source</source>
        <comment>Source information from workbook</comment>
        <translation>ရင်းမြစ်</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="90"/>
        <source>Exercises</source>
        <translation>လေ့ကျင့်ခန်း</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="91"/>
        <source>Timing</source>
        <comment>Assignment completed in time?</comment>
        <translation>အချိန်</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="92"/>
        <source>Notes</source>
        <translation>မှတ်စု</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="93"/>
        <source>Next study</source>
        <translation>နောက်သင်ခန်းစာ</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="94"/>
        <source>Setting</source>
        <comment>for sisters assignment</comment>
        <translation>ဇာတ်အိမ်</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="95"/>
        <location filename="../print/printmidweekschedule.cpp" line="96"/>
        <location filename="../print/printmidweekschedule.cpp" line="97"/>
        <location filename="../print/printmidweekschedule.cpp" line="392"/>
        <source>Class</source>
        <translation>သင်တန်း</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="98"/>
        <source>Auxiliary Classroom Counselor</source>
        <comment>See S-140</comment>
        <translation>အရန်သွန်သင်သူ</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="99"/>
        <source>Auxiliary Classroom</source>
        <comment>See S-140</comment>
        <translation>အရန်သင်တန်း</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="100"/>
        <source>Main Hall</source>
        <comment>See S-140</comment>
        <translation>ပင်မ ခန်းမ</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="101"/>
        <source>MH</source>
        <comment>Abbreviation for &apos;Main Hall&apos;</comment>
        <translation>၁</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="102"/>
        <source>A</source>
        <comment>Abbreviation for &apos;Auxiliary Classroom&apos;</comment>
        <translation>၂</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="103"/>
        <source>Student</source>
        <comment>See S-140</comment>
        <translation>ကျောင်းသား</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="106"/>
        <source>Today</source>
        <translation>ဒီနေ့</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="107"/>
        <source>Next week</source>
        <translation>နောက်အပတ်</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="389"/>
        <source>Other schools</source>
        <translation>တခြား ကျောင်းများ</translation>
    </message>
</context>
<context>
    <name>PrintMidweekSlip</name>
    <message>
        <location filename="../print/printmidweekslip.cpp" line="50"/>
        <source>Slip Template</source>
        <translation>မှတ်စု ပုံစံခွက်</translation>
    </message>
    <message>
        <location filename="../print/printmidweekslip.cpp" line="467"/>
        <source>Assistant</source>
        <translation>အကူ</translation>
    </message>
    <message>
        <location filename="../print/printmidweekslip.cpp" line="480"/>
        <location filename="../print/printmidweekslip.cpp" line="482"/>
        <source>Study %1</source>
        <comment>Text for study point on slip</comment>
        <translation>သွန်သင်ချက် %1</translation>
    </message>
    <message>
        <location filename="../print/printmidweekslip.cpp" line="524"/>
        <source>2nd talk</source>
        <comment>When printing slips: if the first talk is not &apos;Return Visit&apos;</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintMidweekWorksheet</name>
    <message>
        <location filename="../print/printmidweekworksheet.cpp" line="61"/>
        <source>Class </source>
        <translation>သင်တန်း </translation>
    </message>
</context>
<context>
    <name>PrintOutgoingSchedule</name>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="59"/>
        <source>%1 Congregation</source>
        <comment>Congregation_Title Tag. %1 is Congregation Name</comment>
        <translation>%1 အသင်းတော်</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="60"/>
        <location filename="../print/printoutgoingschedule.cpp" line="170"/>
        <source>Congregation</source>
        <translation>အသင်းတော်</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="62"/>
        <source>Public Talk</source>
        <translation>လူထုဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="63"/>
        <source>Watchtower Study</source>
        <translation>ကင်းမျှော်စင်သင်တန်း</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="163"/>
        <source>Talk</source>
        <translation>ဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="164"/>
        <source>Outgoing Speakers</source>
        <translation>သွားလည် ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="165"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="166"/>
        <source>Speaker</source>
        <translation>ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="167"/>
        <source>Theme</source>
        <translation>အဓိကအချက်</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="168"/>
        <source>Theme Number</source>
        <translation>အဓိကအကြောင်းအရာ နံပါတ်</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="169"/>
        <source>Circuit</source>
        <translation>တိုက်နယ်</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="171"/>
        <source>Start Time</source>
        <translation>စတင်ချိန်</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="173"/>
        <source>NOTE: Dear brother, in spite of careful database-maintenance, sometimes times or addresses might be out of date. So, please verify by looking those up via JW.ORG. Thank you!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintTalksOfSpeakersList</name>
    <message>
        <location filename="../print/printtalksofspeakerslist.cpp" line="37"/>
        <location filename="../print/printtalksofspeakerslist.cpp" line="38"/>
        <source>Public Talks</source>
        <translation>လူထုဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../print/printtalksofspeakerslist.cpp" line="39"/>
        <source>Contact info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printtalksofspeakerslist.cpp" line="40"/>
        <source>Talk numbers</source>
        <comment>Public talk numbers</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintTerritoryAssignmentRecord</name>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="35"/>
        <source>Territory Assignment Record</source>
        <translation>ရပ်ကွက် ပေးအပ် မှတ်တမ်း</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="36"/>
        <source>Territory Coverage</source>
        <translation>ရပ်ကွက်ခြုံငုံမှု</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="37"/>
        <source>Total number of territories</source>
        <translation>ရပ်ကွက်အရေအတွက်စုစုပေါင်း</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="38"/>
        <source>&lt; 6 months</source>
        <comment>territory worked</comment>
        <translation>&lt; ၆လ</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="39"/>
        <source>6 to 12 months</source>
        <comment>territory worked</comment>
        <translation>၆လ ကနေ ၁၂လ</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="40"/>
        <source>&gt; 12 months ago</source>
        <comment>territory worked</comment>
        <translation>လွန်ခဲ့တဲ့ ၁၂လ အထက်</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="41"/>
        <source>Average per year</source>
        <comment>Number of times territory has been worked per year on average</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="175"/>
        <source>Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="176"/>
        <source>Addresses</source>
        <comment>Addresses included in the territory</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="177"/>
        <source>Address type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="178"/>
        <source>Assigned to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="179"/>
        <source>Congregation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="181"/>
        <source>%1 Congregation</source>
        <comment>Congregation_Title Tag. %1 is Congregation Name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="182"/>
        <source>Date checked out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="183"/>
        <source>Date checked back in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="184"/>
        <source>Date last worked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="185"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="186"/>
        <source>City</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="187"/>
        <source>Country</source>
        <comment>Short name of country</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="188"/>
        <source>County</source>
        <comment>Name of administrative area level 2</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="189"/>
        <source>District</source>
        <comment>Sublocality, first-order civil entity below a locality</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="190"/>
        <source>From</source>
        <comment>From number; in number range</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="200"/>
        <source>No.</source>
        <comment>House or street number</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="201"/>
        <source>Locality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="202"/>
        <source>Map</source>
        <comment>Map of a territory</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="203"/>
        <source>Name</source>
        <comment>Name of person or building</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="204"/>
        <source>Postalcode</source>
        <comment>Mail code, ZIP</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="205"/>
        <source>Name of publisher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="206"/>
        <source>Remark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="207"/>
        <source>State</source>
        <comment>Short name of administrative area level 1</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="208"/>
        <source>Street</source>
        <comment>Streetname</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="209"/>
        <source>Territory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="210"/>
        <source>Terr. No.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="211"/>
        <source>Territory type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="212"/>
        <source>To</source>
        <comment>To number; in number range</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="213"/>
        <source>Type</source>
        <comment>Type of something</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="214"/>
        <source>Sum</source>
        <comment>Total amount</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintTerritoryCard</name>
    <message>
        <location filename="../print/printterritorycard.cpp" line="35"/>
        <source>Territory Map Card</source>
        <comment>Title tag for a S-12 or similar card</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritorycard.cpp" line="36"/>
        <source>Territory Map</source>
        <comment>Title tag for a sheet with a territory map</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritorycard.cpp" line="37"/>
        <source>Address List</source>
        <comment>Title tag for a sheet with a territory&apos;s address list</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritorycard.cpp" line="38"/>
        <source>Territory Map with Address List</source>
        <comment>Title tag for a sheet with a territory&apos;s map and address list</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritorycard.cpp" line="39"/>
        <source>Street List</source>
        <comment>Title tag for a sheet with a territory&apos;s street list</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritorycard.cpp" line="40"/>
        <source>Territory Map with Street List</source>
        <comment>Title tag for a sheet with a territory&apos;s map and street list</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritorycard.cpp" line="41"/>
        <source>Do-Not-Call List</source>
        <comment>Title tag for a sheet with a territory&apos;s Do-Not-Call list</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintTerritoryMapCard</name>
    <message>
        <location filename="../print/printterritorymapcard.cpp" line="53"/>
        <source>Template</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintWeekendSchedule</name>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="36"/>
        <source>Weekend Meeting</source>
        <translation>တနင်္ဂနွေစည်းဝေး</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="37"/>
        <source>Public Meeting</source>
        <translation>လူထုစည်းဝေး</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="40"/>
        <location filename="../print/printweekendschedule.cpp" line="161"/>
        <source>%1 Congregation</source>
        <comment>Congregation_Title Tag. %1 is Congregation Name</comment>
        <translation>%1 အသင်းတော်</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="41"/>
        <location filename="../print/printweekendschedule.cpp" line="162"/>
        <source>Congregation</source>
        <translation>အသင်းတော်</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="43"/>
        <source>Conductor</source>
        <translation>သင်တန်းမှူး</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="44"/>
        <location filename="../print/printweekendschedule.cpp" line="61"/>
        <source>Chairman</source>
        <translation>သဘာပတိ</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="46"/>
        <location filename="../print/printweekendschedule.cpp" line="63"/>
        <source>Reader</source>
        <translation>စာဖတ်သူ</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="47"/>
        <location filename="../print/printweekendschedule.cpp" line="100"/>
        <source>Public Talk</source>
        <translation>လူထုဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="48"/>
        <location filename="../print/printweekendschedule.cpp" line="102"/>
        <source>Watchtower Study</source>
        <translation>ကင်းမျှော်စင်သင်တန်း</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="49"/>
        <source>Congregation Bible Study</source>
        <translation>အသင်းတော်ကျမ်းစာလေ့လာမှု</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="50"/>
        <source>Christian Life and Ministry Meeting</source>
        <translation>ခရစ်ယာန်အသက်တာနဲ့ အမှုဆောင်လုပ်ငန်းစည်းဝေး</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="51"/>
        <location filename="../print/printweekendschedule.cpp" line="66"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="52"/>
        <source>Phone</source>
        <comment>Phone number title</comment>
        <translation>ဖုန်း</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="53"/>
        <source>Host</source>
        <comment>Host for incoming public speaker</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="60"/>
        <source>Speaker</source>
        <translation>ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="64"/>
        <source>WT Reader</source>
        <translation>ကင်:မျှော်စင် စာဖတ်သူ</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="67"/>
        <source>Theme</source>
        <translation>အဓိကအချက်</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="103"/>
        <source>Circuit Overseer</source>
        <translation>တိုက်နယ်ကြီးကြပ်မှူး</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="106"/>
        <source>Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="118"/>
        <source>Prayer</source>
        <translation>ဆုတောင်းချက်</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="121"/>
        <source>Song</source>
        <translation>သီချင်း</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="199"/>
        <source>Watchtower Conductor</source>
        <translation>ကင်းမျှော်စင်ဦးဆောင်သူ</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="202"/>
        <source>No regular meeting</source>
        <translation>ပုံမှန်အစည်းအဝေး မရှိ</translation>
    </message>
</context>
<context>
    <name>PrintWeekendWorksheet</name>
    <message>
        <location filename="../print/printweekendworksheet.cpp" line="47"/>
        <source>Weekend Meeting</source>
        <translation>တနင်္ဂနွေစည်းဝေး</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="52"/>
        <source>Theme</source>
        <translation>အကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="81"/>
        <source>Congregation</source>
        <translation>အသင်းတော်</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="109"/>
        <source>Speaker</source>
        <translation>ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="148"/>
        <source>Mobile</source>
        <translation>မိုဘိုင်းဖုန်း</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="166"/>
        <source>Phone</source>
        <translation>ဖုန်း</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="184"/>
        <source>Email</source>
        <translation>အီးမေးလ်</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="202"/>
        <source>Info</source>
        <translation>အချက်အလက်</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="221"/>
        <source>Host</source>
        <translation>အိမ်ရှင်</translation>
    </message>
</context>
<context>
    <name>QDialogButtonBox</name>
    <message>
        <location filename="../main.cpp" line="69"/>
        <source>Yes</source>
        <translation>ဟုတ်ကဲ့ပါ</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="70"/>
        <source>&amp;Yes</source>
        <translation>&amp;ဟုတ်ကဲ့ပါ</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="71"/>
        <source>No</source>
        <translation>မဟုတ်ပါ</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="72"/>
        <source>&amp;No</source>
        <translation>&amp;မဟုတ်ပါ</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="73"/>
        <source>Cancel</source>
        <translation>ဖျက်သိမ်းပါ</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="74"/>
        <source>&amp;Cancel</source>
        <translation>&amp;ဖျက်သိမ်းပါ</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="75"/>
        <source>Save</source>
        <translation>သိမ်းမယ်</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="76"/>
        <source>&amp;Save</source>
        <translation>&amp;သိမ်းမယ်</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="77"/>
        <source>Open</source>
        <translation>ဖိုင် ဖွင့်မယ်</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="167"/>
        <source>Wrong username and/or password</source>
        <translation>အသုံးပြုသူနာမည်/လျှို့ဝှက်စာသား မှားနေတယ်</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="275"/>
        <source>Database not found!</source>
        <translation>အချက်အလက်ရင်းမြစ် ရှာမတွေ့!</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="278"/>
        <source>Choose database</source>
        <translation>အချက်အလက်ရင်းမြစ် ရွေးပါ</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="278"/>
        <source>SQLite files (*.sqlite)</source>
        <translation>SQLite files (*.sqlite)</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="302"/>
        <source>Database restoring failed</source>
        <translation>Database restoring failed</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="338"/>
        <location filename="../mainwindow.cpp" line="232"/>
        <source>Save changes?</source>
        <translation>ပြောင်းထားတာတွေ သိမ်းမလား?</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="271"/>
        <source>Database copied to </source>
        <translation>အချက်အလက်ရင်းမြစ် ကူးလိုက်ပြီ </translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="37"/>
        <source>Database file not found! Searching path =</source>
        <translation>အချက်အလက်ရင်းမြစ် ရှာမတွေ့! ဖိုင်လမ်းကြောင်း ရှာနေ =</translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="56"/>
        <source>Database Error</source>
        <translation>အချက်အလက်ရင်းမြစ် ပြဿနာ</translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="702"/>
        <source>This version of the application (%1) is older than the database (%2). There is a strong probability that error messages will popup and changes may not be saved correctly. Please download and install the latest version for best results.</source>
        <translation>ဒီဗားရှင်း (%1) က ရင်းမြစ် (%2) ထက် နောက်ကျနေပြီ။ ချွတ်ယွင်းကြောင်းအသိပေးတဲ့ စာတွေတက်လာနိုင်ပြီး ပြောင်းလိုက်တာတွေကို သိမ်းလို့မရဘဲ ဖြစ်နေနိုင်တယ်။ အကောင်းဆုံးရလဒ် ရဖို့ နောက်ဆုံးထွက် ဗားရှင်းကို ကူးယူပြီး သွင်းလိုက်ပါ။</translation>
    </message>
    <message>
        <location filename="../cpublictalks.cpp" line="351"/>
        <source>Circuit</source>
        <translation>တိုက်နယ်</translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="746"/>
        <source>Database updated</source>
        <translation>အချက်အလက်ရင်းမြစ် အသစ်တင်ပြီးပြီ</translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="321"/>
        <location filename="../ccongregation.cpp" line="347"/>
        <source>Circuit Overseer&#x27;s visit</source>
        <translation>တိုက်နယ်ကြီးကြပ်မှူးလည်ပတ်မှု</translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="323"/>
        <location filename="../ccongregation.cpp" line="328"/>
        <source>%1 (No meeting)</source>
        <comment>no meeting exception type</comment>
        <translation>%1 (အစည်းအဝေး မရှိ)</translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="323"/>
        <location filename="../ccongregation.cpp" line="349"/>
        <source>Convention week</source>
        <translation>ခရိုင်စည်းဝေး အပတ်</translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="325"/>
        <location filename="../ccongregation.cpp" line="351"/>
        <source>Memorial</source>
        <translation>အောက်မေ့ရာပွဲ</translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="356"/>
        <source>Review</source>
        <comment>Theocratic ministry school review</comment>
        <translation>ပြန်လည်သုံးသပ်ခြင်း</translation>
    </message>
    <message>
        <location filename="../cpersoncombobox.cpp" line="56"/>
        <location filename="../cpublictalks.cpp" line="347"/>
        <source>Name</source>
        <translation>နာမည်</translation>
    </message>
    <message>
        <location filename="../cpersoncombobox.cpp" line="57"/>
        <location filename="../cpublictalks.cpp" line="266"/>
        <location filename="../cpublictalks.cpp" line="348"/>
        <source>Last</source>
        <translation>နောက်ဆုံး</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="257"/>
        <location filename="../school.cpp" line="424"/>
        <source>All</source>
        <translation>အားလုံး</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="258"/>
        <location filename="../school.cpp" line="425"/>
        <source>Highlights</source>
        <translation>ပေါ်လွင်ချက်</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="262"/>
        <location filename="../school.cpp" line="429"/>
        <source>WT Reader</source>
        <translation>ကင်:မျှော်စင် စာဖတ်သူ</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="264"/>
        <location filename="../school.cpp" line="431"/>
        <source>Selected</source>
        <translation>ရွေးထားတဲ့</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="323"/>
        <location filename="../school.cpp" line="486"/>
        <source>Assignment already has been made</source>
        <translation>တာဝန် ပေးပြီးသွားပြီ</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="327"/>
        <location filename="../school.cpp" line="490"/>
        <source>Assigned student has other meeting parts</source>
        <translation>တာဝန်ပေးထားတဲ့ ကျောင်းသားမှာ တခြားတာဝန်ရှိနေတယ်</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="331"/>
        <location filename="../school.cpp" line="494"/>
        <source>Student unavailable</source>
        <translation>ကျောင်းသား မအားပါ</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="335"/>
        <source>Student part of family</source>
        <translation>မိသားစုဝင် ကျောင်းသား</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="434"/>
        <source>Recently together</source>
        <translation>မကြာသေးခင်က အတူတူ</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="498"/>
        <source>Family member used in another TMS assignment</source>
        <translation>မိသားစုဝင်ကို တခြားကျောင်းတာဝန်မှာ သုံးထားတယ်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="67"/>
        <source>I/O</source>
        <comment>Incoming/Outgoing</comment>
        <translation>I/O</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="72"/>
        <source>Notes</source>
        <translation>မှတ်စုတိုများ</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="565"/>
        <source>Congregation Name</source>
        <translation>အသင်းတော် နာမည်:</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="265"/>
        <location filename="../historytable.cpp" line="314"/>
        <location filename="../school.cpp" line="854"/>
        <source>R</source>
        <comment>abbreviation of the &apos;reader&apos;</comment>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="314"/>
        <source>CBS</source>
        <translation>အသင်းတော်ကျမ်းစာလေ့လာမှု</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="314"/>
        <source>C</source>
        <comment>abbreviation of the &apos;conductor&apos;</comment>
        <translation>သင်တန်းမှူး</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="317"/>
        <source>Conductor</source>
        <translation>သင်တန်းမှူး</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="317"/>
        <source>Reader</source>
        <translation>စာဖတ်သူ</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="460"/>
        <source>#</source>
        <translation>#</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="461"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="222"/>
        <source>Assignment</source>
        <translation>တာဝန်</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="462"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="223"/>
        <source>Note</source>
        <translation>မှတ်ချက်</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="463"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="224"/>
        <source>Time</source>
        <translation>အချိန်</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="256"/>
        <location filename="../school.cpp" line="263"/>
        <location filename="../school.cpp" line="423"/>
        <location filename="../school.cpp" line="430"/>
        <source>Assistant</source>
        <translation>အကူ</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="256"/>
        <source>Student</source>
        <translation>ကျောင်းသား</translation>
    </message>
    <message>
        <location filename="../school_item.cpp" line="265"/>
        <source>Bible highlights:</source>
        <translation>ကျမ်းစာ ပေါ်လွင်ချက်</translation>
    </message>
    <message>
        <location filename="../school_item.cpp" line="268"/>
        <source>No. 1:</source>
        <translation>နံပါတ် ၁</translation>
    </message>
    <message>
        <location filename="../school_item.cpp" line="271"/>
        <source>No. 2:</source>
        <translation>နံပါတ် ၂</translation>
    </message>
    <message>
        <location filename="../school_item.cpp" line="274"/>
        <source>No. 3:</source>
        <translation>နံပါတ် ၃</translation>
    </message>
    <message>
        <location filename="../school_item.cpp" line="277"/>
        <source>Reader:</source>
        <translation>စာဖတ်သူ:</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="816"/>
        <location filename="../generatexml.cpp" line="107"/>
        <source>Default language not selected!</source>
        <translation>သုံးမယ့် ဘာသာစကား မရွေးရသေး!</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="937"/>
        <source>The header row of CSV file is not valid.</source>
        <translation>CSV ဖိုင်ရဲ့ ခေါင်းစည်းအတန်းက မရဘူး</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="293"/>
        <source>Confirm password!</source>
        <translation>လျှို့ဝှက်စာသား ထပ်ရိုက်ပါ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2232"/>
        <source>A user with the same E-mail address has already been added.</source>
        <translation>ဒီအီးမေးလ်လိပ်စာနဲ့ အသုံးပြုသူ ကို ပေါင်းထည့်ပြီးသား</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2448"/>
        <source>All talks have been added to this week</source>
        <translation>ဒီအပတ်မှာ ဟောပြောချက်အားလုံး ထည့်ပြီးပြီ</translation>
    </message>
    <message>
        <location filename="../cpublictalks.cpp" line="265"/>
        <location filename="../mainwindow.cpp" line="921"/>
        <location filename="../publictalkedit.cpp" line="71"/>
        <source>Theme</source>
        <translation>အဓိကအကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../importTa1ks.cpp" line="58"/>
        <location filename="../importkhs.cpp" line="42"/>
        <location filename="../importwintm.cpp" line="42"/>
        <source>Import</source>
        <translation>တင်သွင်း</translation>
    </message>
    <message>
        <location filename="../importTa1ks.cpp" line="58"/>
        <location filename="../importkhs.cpp" line="42"/>
        <location filename="../importwintm.cpp" line="42"/>
        <source>Import Complete</source>
        <translation>တင်သွင်းပြီးသွားပြီ</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="192"/>
        <source>Speaker</source>
        <comment>The todo list Speaker cell is in error</comment>
        <translation>ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="199"/>
        <source>Congregation</source>
        <comment>The todo list Congregation cell is in error</comment>
        <translation>အသင်းတော်</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="209"/>
        <source>Theme (could be a theme this speaker does not give)</source>
        <comment>The todo list Theme cell is in error</comment>
        <translation>အကြောင်းအရာ (ဒီဟောပြောသူ ဟောတာမဟုတ်တဲ့ အကြောင်းအရာ ဖြစ်နိုင်တယ်)</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="221"/>
        <location filename="../todo.cpp" line="232"/>
        <source>Date Already Scheduled</source>
        <translation>ရက်စွဲ အစီအစဥ် ဆွဲပြီးသား</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="89"/>
        <source>Unable to read new Workbook format</source>
        <translation>လေ့ကျင်းခန်းစာစောင် အသစ် ပုံစံကို ဖတ်လို့မရပါ</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="91"/>
        <source>Database not set to handle language &#x27;%1&#x27;</source>
        <translation>အချက်အလက်ရင်းမြစ်က &apos;%1&apos; ဘာသာစကားကို ကိုင်တွယ်လို့ မရပါ</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="93"/>
        <source>Unable to find year/month</source>
        <translation>ခုနှစ်/လ ရှာလို့မတွေ့</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="94"/>
        <source>Nothing imported (no dates recognized)</source>
        <translation>တင်သွင်းထားတာ ဘာမှမရှိပါ (ဘယ်ရက်စွဲမှ မသိပါ)</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="98"/>
        <location filename="../wtimport.cpp" line="31"/>
        <location filename="../wtimport.cpp" line="32"/>
        <source>Imported %1 weeks from %2 thru %3</source>
        <translation>%2 ကနေ %3 အထိ %1 အပတ်တွေ တင်သွင်းပြီးပြီ</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="417"/>
        <source>Please select the Talk Names to match the names we found in the workbook</source>
        <translation>လေ့ကျင့်ခန်းစာစောင်ထဲက နာမည်တွေနဲ့ ကိုက်အောင် ဟောပြောချက်နာမည်တွေကို ရွေးပေးပါ</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="7"/>
        <source>Ch</source>
        <comment>history table: abbreviation for the &apos;chairman&apos; of the Christian Life and Ministry Meeting</comment>
        <translation>သဘာပတိ</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="9"/>
        <source>H</source>
        <comment>history table: abbreviation for the &apos;highlights&apos;</comment>
        <translation>H</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="11"/>
        <source>D</source>
        <comment>history table: abbreviation for &apos;Diging spiritual gem&apos;</comment>
        <translation>D</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="13"/>
        <source>#R</source>
        <comment>history table: abbreviation for the &apos;reader&apos;</comment>
        <translation>#R</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="15"/>
        <source>#V</source>
        <comment>history table: abbreviation for &apos;prepare video presentation&apos;</comment>
        <translation>#V</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="17"/>
        <source>#A</source>
        <comment>history table: abbreviation for &apos;Apply yourself to Reading and Teaching&apos;</comment>
        <translation>#A</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="19"/>
        <source>Mem</source>
        <comment>history table: abbreviation for assistant/householder of &apos;Memorial Inviation&apos;</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="21"/>
        <source>Vid</source>
        <comment>history table: abbreviation for &apos;Extra Video Part&apos;</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="24"/>
        <source>♢1</source>
        <comment>history table: abbreviation for assignment 1, assistant/householder of &apos;Initial Visit&apos;</comment>
        <translation>♢၁</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="25"/>
        <source>#1</source>
        <comment>history table: abbreviation for assignment 1, &apos;Initial Visit&apos;</comment>
        <translation>၁</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="30"/>
        <source>♢2</source>
        <comment>history table: abbreviation for assignment 2, assistant/householder of &apos;Return Visit&apos;</comment>
        <translation>♢၂</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="31"/>
        <source>#2</source>
        <comment>history table: abbreviation for assignment 2, &apos;Return Visit&apos;</comment>
        <translation>၂</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="35"/>
        <source>♢3</source>
        <comment>history table: abbreviation for assignment 3, assistant/householder of &apos;Bible Study&apos;</comment>
        <translation>♢၃</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="36"/>
        <source>#3</source>
        <comment>history table: abbreviation for assignment 3, &apos;Bible Study&apos;</comment>
        <translation>၃</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="38"/>
        <source>CL1</source>
        <comment>history table: abbreviation for cristian life, talk 1</comment>
        <translation>CL1</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="40"/>
        <source>CL2</source>
        <comment>history table: abbreviation for cristian life, talk 2</comment>
        <translation>CL2</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="42"/>
        <source>CL3</source>
        <comment>history table: abbreviation for cristian life, talk 3</comment>
        <translation>CL3</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="45"/>
        <source>BS-R</source>
        <comment>history table: abbreviation for cristian life, Bible Study Reader</comment>
        <translation>ကျမ်းဖတ်</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="46"/>
        <source>BS</source>
        <comment>history table: abbreviation for cristian life, Bible Study</comment>
        <translation>BS</translation>
    </message>
    <message>
        <location filename="../slipscanner.cpp" line="32"/>
        <source>One-time Scanning of New Slip</source>
        <translation>မှတ်တမ်းအသစ်ကို တစ်ကြိမ်တည်းအပြီး scan ဖတ်ခြင်း</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="738"/>
        <source>All weekend meetings for</source>
        <comment>Filename prefix for weekend meetings iCal export</comment>
        <translation>တနင်္ဂနွေအစည်းအဝေးအားလုံး</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="750"/>
        <source>Weekend Meeting</source>
        <translation>တနင်္ဂနွေအစည်းအဝေး</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="858"/>
        <source>All outgoing talks for</source>
        <comment>File name prefix for outgoing talks iCal export</comment>
        <translation>သွားလည် ဟောပြောချက်အားလုံး</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="874"/>
        <location filename="../mainwindow.cpp" line="885"/>
        <source>Outgoing Talks</source>
        <translation>သွားလည် ဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="990"/>
        <source>Midweek Meeting</source>
        <translation>ကြားပတ်စည်းဝေး</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="459"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="220"/>
        <location filename="../publictalkedit.cpp" line="68"/>
        <location filename="../schoolreminder.cpp" line="183"/>
        <source>Date</source>
        <translation>ရက်စွဲ</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="465"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="226"/>
        <source>Together</source>
        <comment>The column header text to show partner in student assignment</comment>
        <translation>အတူ</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="167"/>
        <source>Sister</source>
        <translation>ညီမ</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="168"/>
        <source>Brother</source>
        <translation>ညီကို</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="172"/>
        <source>Dear %1 %2</source>
        <translation>ချစ်ခင်ရသော %1 %2</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="173"/>
        <source>Please find below details of your upcoming assignment:</source>
        <translation>လာမယ့် သင့်တာဝန် အသေးစိတ်တွေကို အောက်မှာ ရှာလိုက်ပါ</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="249"/>
        <source>Regards</source>
        <translation>လေးစားစွာဖြင့်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="209"/>
        <source>Publisher</source>
        <comment>Roles and access control</comment>
        <translation>ကြေညာသူ</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="98"/>
        <source>View midweek meeting schedule</source>
        <comment>Access Control</comment>
        <translation>ကြားရက်အစည်းအဝေး ဇယားကြည့်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="100"/>
        <source>Edit midweek meeting schedule</source>
        <comment>Access Control</comment>
        <translation>ကြားရက်အစည်းအဝေး ဇယားပြင်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="102"/>
        <source>View midweek meeting settings</source>
        <comment>Access Control</comment>
        <translation>ကြားရက်အစည်းအဝေး စက်တင်ကြည့်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="104"/>
        <source>Edit midweek meeting settings</source>
        <comment>Access Control</comment>
        <translation>ကြားရက်အစည်းအဝေး စက်တင်ပြင်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="106"/>
        <source>Send midweek meeting reminders</source>
        <comment>Access Control</comment>
        <translation>ကြားရက်အစည်းအဝေး အသိပေးချက်ပို့</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="108"/>
        <source>Print midweek meeting schedule</source>
        <comment>Access Control</comment>
        <translation>ကြားရက်အစည်းအဝေး ဇယား ပရင့်ထုတ်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="110"/>
        <source>Print midweek meeting assignment slips</source>
        <comment>Access Control</comment>
        <translation>ကြားရက်အစည်းအဝေး တာဝန် ပရင့်ထုတ်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="112"/>
        <source>Print midweek meeting worksheets</source>
        <comment>Access Control</comment>
        <translation>ကြားရက်အစည်းအဝေး လေ့ကျင့်ခန်း ပရင့်ထုတ်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="114"/>
        <source>View weekend meeting schedule</source>
        <comment>Access Control</comment>
        <translation>ကြားရက်အစည်းအဝေး ဇယားကြည့်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="116"/>
        <source>Edit weekend meeting schedule</source>
        <comment>Access Control</comment>
        <translation>ပတ်ဆုံးအစည်းအဝေး ဇယားပြင်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="118"/>
        <source>View weekend meeting settings</source>
        <comment>Access Control</comment>
        <translation>ပတ်ဆုံးအစည်းအဝေး စက်တင်ကြည့်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="120"/>
        <source>Edit weekend meeting settings</source>
        <comment>Access Control</comment>
        <translation>ပတ်ဆုံးအစည်းအဝေး စက်တင်ပြင်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="122"/>
        <source>View public talk list</source>
        <comment>Access Control</comment>
        <translation>ဟောပြောချက် စာရင်းကြည့်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="124"/>
        <source>Edit public talk list</source>
        <comment>Access Control</comment>
        <translation>ဟောပြောချက် စာရင်းပြင်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="126"/>
        <source>Schedule hospitality</source>
        <comment>Access Control</comment>
        <translation>ဧည့်ဝတ် စီစဥ်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="128"/>
        <source>Print weekend meeting schedule</source>
        <comment>Access Control</comment>
        <translation>ပတ်ဆုံးအစည်းအဝေး ဇယား ပရင့်ထုတ်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="130"/>
        <source>Print weekend meeting worksheets</source>
        <comment>Access Control</comment>
        <translation>ပတ်ဆုံးအစည်းအဝေး တာဝန်ဇယား စာရွက်ထုတ်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="132"/>
        <source>Print speakers schedule</source>
        <comment>Access Control</comment>
        <translation>ဟောပြောသူ အစီအစဥ်ဇယား ပရင့်ထုတ်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="134"/>
        <source>Print speakers assignments</source>
        <comment>Access Control</comment>
        <translation>ဟောပြောသူ တာဝန် စာရွက်ထုတ်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="136"/>
        <source>Print hospitality</source>
        <comment>Access Control</comment>
        <translation>ဧည့်ဝတ် အစီအစဥ် ပရင့်ထုတ်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="138"/>
        <source>Print public talk list</source>
        <comment>Access Control</comment>
        <translation>ဟောပြောချက် စာရင်း ပရင့်ထုတ်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="148"/>
        <source>View public speakers</source>
        <comment>Access Control</comment>
        <translation>ဟောပြောသူ ကြည့်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="150"/>
        <source>Edit public speakers</source>
        <comment>Access Control</comment>
        <translation>ဟောပြောသူ ပြင်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="140"/>
        <source>View publishers</source>
        <comment>Access Control</comment>
        <translation>ကြေညာသူ ကြည့်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="142"/>
        <source>Edit publishers</source>
        <comment>Access Control</comment>
        <translation>ကြေညာသူ ပြင်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="144"/>
        <source>View student data</source>
        <comment>Access Control</comment>
        <translation>အချက်အလက် ဖလှယ်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="146"/>
        <source>Edit student data</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="152"/>
        <source>View privileges</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="154"/>
        <source>Edit privileges</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="156"/>
        <source>View midweek meeting talk history</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="158"/>
        <source>View availabilities</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="160"/>
        <source>Edit availabilities</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="162"/>
        <source>View permissions</source>
        <comment>Access Control</comment>
        <translation>ခွင့်ပြုချက် ကြည့်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="164"/>
        <source>Edit permissions</source>
        <comment>Access Control</comment>
        <translation>ခွင့်ပြုချက် ပြင်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="166"/>
        <source>View territories</source>
        <comment>Access Control</comment>
        <translation>ရပ်ကွက် ကြည့်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="168"/>
        <source>Edit territories</source>
        <comment>Access Control</comment>
        <translation>ရပ်ကွက် ပြင်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="170"/>
        <source>Print territory record</source>
        <comment>Access Control</comment>
        <translation>ရပ်ကွက်ကဒ် ပရင့်ထုတ်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="172"/>
        <source>Print territory map card</source>
        <comment>Access Control</comment>
        <translation>ရပ်ကွက်မြေပုံကဒ် ပရင့်ထုတ်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="174"/>
        <source>Print territory map and address sheets</source>
        <comment>Access Control</comment>
        <translation>ရပ်ကွက်မြေပုံနဲ့ လိပ်စာစာရွက် ပရင့်ထုတ်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="176"/>
        <source>View territory settings</source>
        <comment>Access Control</comment>
        <translation>ရပ်ကွက်စက်တင် ကြည့်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="178"/>
        <source>Edit territory settings</source>
        <comment>Access Control</comment>
        <translation>ရပ်ကွက်စက်တင် ပြင်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="180"/>
        <source>View territory assignments</source>
        <comment>Access Control</comment>
        <translation>ရပ်ကွက်တာဝန် ကြည့်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="182"/>
        <source>View territory addresses</source>
        <comment>Access Control</comment>
        <translation>ရပ်ကွက်လိပ်စာ ကြည့်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="184"/>
        <source>View congregation settings</source>
        <comment>Access Control</comment>
        <translation>အသင်းတော် စက်တင် ကြည့်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="186"/>
        <source>Edit congregation settings</source>
        <comment>Access Control</comment>
        <translation>အသင်းတော် စက်တင် ပြင်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="188"/>
        <location filename="../accesscontrol.h" line="192"/>
        <source>View special events</source>
        <comment>Access Control</comment>
        <translation>အထူး အစီအစဥ် ကြည့်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="190"/>
        <location filename="../accesscontrol.h" line="194"/>
        <source>Edit special events</source>
        <comment>Access Control</comment>
        <translation>အထူး အစီအစဥ် ပြင်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="196"/>
        <source>View song list</source>
        <comment>Access Control</comment>
        <translation>သီချင်း စာရင်း ကြည့်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="198"/>
        <source>Edit song list</source>
        <comment>Access Control</comment>
        <translation>သီချင်း စာရင်း ပြင်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="200"/>
        <source>Delete cloud data</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="211"/>
        <source>Elder</source>
        <comment>Roles and access control</comment>
        <translation>အကြီးအကဲ</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="213"/>
        <source>LMM Chairman</source>
        <comment>Roles and access control</comment>
        <translation>ခသင သဘာပတိ</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="215"/>
        <source>LMM Overseer</source>
        <comment>Roles and access control</comment>
        <translation>ခသင ကြီးကြပ်မှူး</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="217"/>
        <source>Public Talk Coordinator</source>
        <comment>Roles and access control</comment>
        <translation>လူထုဟောပြောချက် စီစဥ်သူ</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="219"/>
        <source>Territory Servant</source>
        <comment>Roles and access control</comment>
        <translation>ရပ်ကွက် အမှုထမ်း</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="221"/>
        <source>Secretary</source>
        <comment>Roles and access control</comment>
        <translation>အတွင်းရေးမှူး</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="223"/>
        <source>Service Overseer</source>
        <comment>Roles and access control</comment>
        <translation>လုပ်ငန်းတော်ကြီးကြပ်မှူး</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="225"/>
        <source>Coordinator of BOE</source>
        <comment>Roles and access control</comment>
        <translation>အကြီးအကဲအဖွဲ့ ညှိနှိုင်းရေးမှူး</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="227"/>
        <source>Administrator</source>
        <comment>Roles and access control</comment>
        <translation>စီမံခန့်ခွဲသူ</translation>
    </message>
    <message>
        <location filename="../print/printdocument.cpp" line="497"/>
        <source>Width unit does not match height unit</source>
        <comment>while asking for custom paper size</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printdocument.cpp" line="525"/>
        <source>Invalid entry, sorry.</source>
        <comment>while asking for custom paper size</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../settings.ui" line="174"/>
        <location filename="../settings.ui" line="177"/>
        <location filename="../settings.ui" line="1641"/>
        <source>Exceptions</source>
        <translation>ခြွင်းချက်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="204"/>
        <location filename="../settings.ui" line="207"/>
        <source>Public Talks</source>
        <translation>လူထုဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="773"/>
        <source>Custom templates folder</source>
        <translation>စိတ်ကြိုက်ပုံစံခွက်ဖိုင်တွဲ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="917"/>
        <source>Open database location</source>
        <translation>ဒေတာရင်းမြစ် တည်နေရာ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="976"/>
        <source>Backup database</source>
        <translation>Backup database</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1010"/>
        <source>Restore database</source>
        <translation>Restore database</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1462"/>
        <source>Names display order</source>
        <translation>နာမည် ဖော်ပြမှု အစဥ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1397"/>
        <source>Color palette</source>
        <translation>အရောင်စပ်ဝိုင်း</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1474"/>
        <source>By last name</source>
        <translation>နောက်နာမည်ဖြင့်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1486"/>
        <source>By first name</source>
        <translation>ရှေ့နာမည်ဖြင့်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1385"/>
        <source>Light</source>
        <translation>လင်းလင်း</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1351"/>
        <source>Dark</source>
        <translation>မှောင်မှောင်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1576"/>
        <source>Show song titles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2522"/>
        <source>Public talks maintenance</source>
        <translation>ဟောပြောချက် ထိန်းသိမ်းမှု</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2614"/>
        <source>Schedule hospitality for public speakers</source>
        <translation>လူထုဟောပြောသူအတွက် ဧည့်ဝတ်စီစဥ်ပါ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3130"/>
        <source>Streets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3136"/>
        <source>Street types</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3386"/>
        <source>Map marker scale:</source>
        <translation>မြေပုံ အမှတ်အသား စကေး။  ။</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3412"/>
        <source>Geo Services</source>
        <translation>Geo ဝန်ဆောင်မှု</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3420"/>
        <source>Google:</source>
        <translation>Google:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3430"/>
        <location filename="../settings.ui" line="3433"/>
        <source>API Key</source>
        <translation>API Key</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3440"/>
        <source>Here:</source>
        <translation>ဒီမှာ။ ။</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3447"/>
        <source>Default:</source>
        <translation>အရှိအတိုင်း။  ။</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3457"/>
        <source>OpenStreetMap</source>
        <translation>OpenStreetMap</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3462"/>
        <source>Google</source>
        <translation>Google</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3467"/>
        <source>Here</source>
        <translation>ဒီမှာ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3482"/>
        <location filename="../settings.ui" line="3485"/>
        <source>App Id</source>
        <translation>App Id</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3492"/>
        <location filename="../settings.ui" line="3495"/>
        <source>App Code</source>
        <translation>App Code</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3578"/>
        <source>Send E-Mail Reminders</source>
        <translation>အီးမေးလ် သတိပေးချက် ပို့မယ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3541"/>
        <source>Send reminders when closing TheocBase</source>
        <translation>TheocBase ပိတ်နေတုန်း သတိပေးချက် ပို့ပါ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3556"/>
        <source>E-Mail Options</source>
        <translation>အီးမေးလ် ရွေးချယ်စရာ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3612"/>
        <source>Sender&#x27;s e-mail</source>
        <translation>ပို့သူ အီးမေးလ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3622"/>
        <source>Sender&#x27;s name</source>
        <translation>ပို့သူ နာမည်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3686"/>
        <source>Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3749"/>
        <source>Test Connection</source>
        <translation>ကွန်ရက် ဆက်သွယ်မှုကို စမ်းသပ်မယ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="706"/>
        <source>Printing</source>
        <translation>ပုံနှိပ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3782"/>
        <source>Users</source>
        <translation>အသုံးပြုသူများ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3798"/>
        <source>E-mail:</source>
        <translation>အီးမေးလ်။  ။</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3851"/>
        <source>Rules</source>
        <translation>စည်းမျဥ်း</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="159"/>
        <location filename="../settings.ui" line="162"/>
        <source>General</source>
        <translation>ယေဘုယျ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="839"/>
        <source>Backup</source>
        <translation>Backup</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="468"/>
        <source>Current congregation</source>
        <translation>လက်ရှိ အသင်းတော်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1302"/>
        <source>User interface</source>
        <translation>User interface</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1507"/>
        <source>User interface language</source>
        <translation>အသုံးပြု ဘာသာစကား</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1063"/>
        <source>Security</source>
        <translation>လုံခြုံရေး</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1242"/>
        <source>Enable password</source>
        <translation>Enable password</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1267"/>
        <source>Enable database encryption</source>
        <translation>Enable database encryption</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1168"/>
        <source>Confirm</source>
        <translation>အတည်ပြုမယ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="189"/>
        <location filename="../settings.ui" line="192"/>
        <location filename="../settings.ui" line="1845"/>
        <source>Life and Ministry Meeting</source>
        <translation>ခရစ်ယန်အသက်တာနဲ့ အမှုဆောင်လုပ်ငန်းစည်းဝေး</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2038"/>
        <source>Remove Duplicates</source>
        <translation>ထပ်နေတာတွေ ဖယ်လိုက်မယ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2045"/>
        <source>Meeting Items</source>
        <translation>အစည်းအဝေး အပိုင်း</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2144"/>
        <source>Schedule</source>
        <comment>Name of tab to edit Midweek Meeting schedule</comment>
        <translation>အချိန်ဇယား</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2624"/>
        <source>Hide discontinued</source>
        <translation>မဖျောက်ထားတော့ဘူး</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2806"/>
        <source>Add songs</source>
        <translation>သီချင်း ထည့်မယ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="219"/>
        <location filename="../settings.ui" line="2812"/>
        <source>Songs</source>
        <translation>သီချင်း</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2213"/>
        <location filename="../settings.ui" line="2363"/>
        <source>&lt;html&gt;&lt;head /&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; color:#540000;&quot;&gt;Errors&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head /&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; color:#540000;&quot;&gt;ပြဿနာ&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2884"/>
        <location filename="../settings.ui" line="2890"/>
        <source>Add song one at a time</source>
        <translation>သီချင်း တစ်ခုချင်း ထည့်ပါ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2905"/>
        <source>Song number</source>
        <translation>သီချင်း နံပါတ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2912"/>
        <source>Song title</source>
        <translation>သီချင်း ခေါင်းစဥ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2986"/>
        <source>Cities</source>
        <translation>မြို့</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3058"/>
        <source>Territory types</source>
        <translation>ရပ်ကွက်အမျိုးအစား</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3235"/>
        <source>Addresses</source>
        <translation>လိပ်စာ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3241"/>
        <source>Address types</source>
        <translation>လိပ်စာအမျိုးအစားတွေ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3349"/>
        <source>Configuration</source>
        <translation>ချိန်ညှိစရာ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="246"/>
        <location filename="../settings.cpp" line="2246"/>
        <source>Access Control</source>
        <translation>ဝင်ကြည့်နိုင်မှု ထိန်းချုပ်ရေး</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3247"/>
        <source>Type number:</source>
        <translation>အမျိုးအစားနံပါတ်။  ။</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3142"/>
        <location filename="../settings.ui" line="3257"/>
        <location filename="../settings.ui" line="3788"/>
        <source>Name:</source>
        <translation>အမည်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3211"/>
        <location filename="../settings.ui" line="3267"/>
        <source>Color:</source>
        <translation>အရောင်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3152"/>
        <location filename="../settings.ui" line="3277"/>
        <source>#0000ff</source>
        <translation>#0000ff</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3357"/>
        <source>Default address type:</source>
        <translation>အရှိအတိုင်း လိပ်စာအမျိုးအစား။ ။</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="228"/>
        <source>Territories</source>
        <translation>ရပ်ကွက်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="386"/>
        <location filename="../settings.cpp" line="1107"/>
        <source>Congregation</source>
        <translation>အသင်းတော်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="624"/>
        <source>Weekend Meeting</source>
        <translation>တနင်္ဂနွေစည်းဝေး</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="435"/>
        <source>Current Circuit Overseer</source>
        <translation>လက်ရှိ တိုက်နယ်ကြီးကြပ်မှုး</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="668"/>
        <source>Click to edit</source>
        <translation>ပြန်ပြင်ဖို့ နှိပ်ပါ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1111"/>
        <location filename="../settings.ui" line="3742"/>
        <source>Username</source>
        <translation>အသုံးပြုသူနာမည်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1132"/>
        <location filename="../settings.ui" line="3659"/>
        <source>Password</source>
        <translation>လျှို့ဝှက်စာသား</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="529"/>
        <location filename="../settings.ui" line="1863"/>
        <location filename="../settings.ui" line="1927"/>
        <source>Mo</source>
        <translation>တနင်္လာ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="534"/>
        <location filename="../settings.ui" line="1868"/>
        <location filename="../settings.ui" line="1932"/>
        <source>Tu</source>
        <translation>အင်္ဂါ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="539"/>
        <location filename="../settings.ui" line="1873"/>
        <location filename="../settings.ui" line="1937"/>
        <source>We</source>
        <translation>ဗုဒ္ဓဟူး</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="544"/>
        <location filename="../settings.ui" line="1878"/>
        <location filename="../settings.ui" line="1942"/>
        <source>Th</source>
        <translation>ကြာသပတေး</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="549"/>
        <location filename="../settings.ui" line="1883"/>
        <location filename="../settings.ui" line="1947"/>
        <source>Fr</source>
        <translation>သောကြာ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="554"/>
        <location filename="../settings.ui" line="1888"/>
        <location filename="../settings.ui" line="1952"/>
        <source>Sa</source>
        <translation>စနေ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="559"/>
        <location filename="../settings.ui" line="1893"/>
        <location filename="../settings.ui" line="1957"/>
        <source>Su</source>
        <translation>တနင်္ဂနွေ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1906"/>
        <source>Public Talk and Watchtower study</source>
        <translation>လူတုဟောပြောချက် နဲ့ ကင်းမျှော်စင်သင်တန်း</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1779"/>
        <location filename="../settings.ui" line="1808"/>
        <location filename="../settings.ui" line="2235"/>
        <location filename="../settings.ui" line="2296"/>
        <location filename="../settings.ui" line="2346"/>
        <location filename="../settings.ui" line="2385"/>
        <location filename="../settings.ui" line="2445"/>
        <location filename="../settings.ui" line="2468"/>
        <location filename="../settings.ui" line="2555"/>
        <location filename="../settings.ui" line="2755"/>
        <location filename="../settings.ui" line="2833"/>
        <location filename="../settings.ui" line="2856"/>
        <location filename="../settings.ui" line="2931"/>
        <location filename="../settings.ui" line="3007"/>
        <location filename="../settings.ui" line="3033"/>
        <location filename="../settings.ui" line="3079"/>
        <location filename="../settings.ui" line="3105"/>
        <location filename="../settings.ui" line="3174"/>
        <location filename="../settings.ui" line="3200"/>
        <location filename="../settings.ui" line="3296"/>
        <location filename="../settings.ui" line="3322"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1683"/>
        <source>Circuit Overseer&#x27;s visit</source>
        <translation>တိုက်နယ်ကြီးကြပ်မှူးလည်ပတ်မှု</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1688"/>
        <source>Convention</source>
        <translation>စည်းဝေးကြီး</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1693"/>
        <source>Memorial</source>
        <translation>အောက်မေ့ရာပွဲ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1698"/>
        <source>Zone overseer&#x27;s talk</source>
        <translation>ဇုံကြီးကြပ်မှုး ဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1703"/>
        <source>Other exception</source>
        <translation>တခြား ခြွင်းချက်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1716"/>
        <location filename="../settings.cpp" line="614"/>
        <source>Start date</source>
        <translation>ရက်စွဲကို စတင်ပါ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1744"/>
        <location filename="../settings.cpp" line="615"/>
        <source>End date</source>
        <translation>ရက်စွဲကို အဆုံးသတ်ပါ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1858"/>
        <location filename="../settings.ui" line="1922"/>
        <source>No meeting</source>
        <translation>အစည်းအဝေး မရှိပါ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1824"/>
        <source>Description</source>
        <translation>ဖော်ပြချက်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2078"/>
        <source>Import Christian Life and Ministry Meeting Workbook</source>
        <translation>ခရစ်ယာန်အသက်တာနဲ့ အမှုဆောင်လုပ်ငန်း လေ့ကျင့်ခန်းစာစောင် တင်သွင်းမယ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2006"/>
        <source>Main</source>
        <comment>Main Midweek meeting tab</comment>
        <translation>၁</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2157"/>
        <source>Year</source>
        <translation>နှစ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="486"/>
        <location filename="../settings.ui" line="2195"/>
        <source>Midweek Meeting</source>
        <translation>ကြားရက်စည်းဝေး</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2322"/>
        <source>Midweek Meeting Schedule for selected Meeting above</source>
        <translation>အပေါ်မှာ ရွေးထားတဲ့ စည်းဝေးအတွက် ကြားရက်စည်းဝေး အစီအစဥ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="237"/>
        <source>Reminders</source>
        <translation>သတိပေးချက်များ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3918"/>
        <source>Run a file provided by the Help Desk</source>
        <translation>Help Desk ကနေပေးတဲ့ ဖိုင်ကို run ပါ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="26"/>
        <source>Settings</source>
        <comment>This means the &apos;Options&apos; of TheocBase</comment>
        <translation>ချိန်ညှိစရာ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2012"/>
        <source>Number of classes</source>
        <translation>သင်တန်း အရေအတွက်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2753"/>
        <source>Studies</source>
        <translation>သင်အံမူများ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2638"/>
        <location filename="../settings.ui" line="2644"/>
        <source>Add subject one at a time</source>
        <translation>တစ်ချိန်မှာ အကြောင်းအရာ တစ်ခု ထည့်မယ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2678"/>
        <source>Public talk number</source>
        <translation>လူထုဟောပြောချက် နံပါတ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2685"/>
        <source>Public talk subject</source>
        <translation>လူတုဟောပြောချက် အကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2695"/>
        <location filename="../settings.ui" line="2942"/>
        <source>Language</source>
        <translation>ဘာသာစကား</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2727"/>
        <source>Add congregations and speakers</source>
        <translation>အသင်းတော် နဲ့ ဟောပြောသူ ထည့်မယ်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="834"/>
        <location filename="../settings.cpp" line="1109"/>
        <location filename="../settings.cpp" line="1177"/>
        <source>Number</source>
        <translation>နံပါတ်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2560"/>
        <source>Source</source>
        <translation>ရင်းမြစ်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="562"/>
        <source>Select a backup file</source>
        <translation>Backup ဖိုင် ရွေးပါ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="940"/>
        <location filename="../settings.cpp" line="1262"/>
        <location filename="../settings.cpp" line="1559"/>
        <location filename="../settings.cpp" line="1627"/>
        <location filename="../settings.cpp" line="1711"/>
        <location filename="../settings.cpp" line="1807"/>
        <source>Remove selected row?</source>
        <translation>ရွေးချယ်ထားတဲ့ အတန်းကို ဖယ်မလား?</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1038"/>
        <source>A public talk with the same number is already saved!
Do you want to discontinue the previous talk?

Scheduled talks will be moved to the To Do List.</source>
        <translation>ဟောပြောချက် နံပါတ်တူတာ သိမ်းပြီးသား ဖြစ်နေတယ်!
အရင်တစ်ခုကို ဖျက်လိုက်တော့မလား?

စီစဥ်ထားတဲ့ ဟောပြောချက်တွေ လုပ်ရမယ့်စာရင်းထဲ ရောက်သွားမယ်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1178"/>
        <source>Title</source>
        <translation>ခေါင်းစဥ်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1278"/>
        <source>Song number missing</source>
        <translation>သီချင်း နံပါတ် မပါ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1281"/>
        <source>Song title missing</source>
        <translation>သီချင်း ခေါင်းစဥ် မပါ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1291"/>
        <source>Song is already saved!</source>
        <translation>သီချင်းကို သိမ်းပြီးသား ဖြစ်နေတယ်!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1300"/>
        <source>Song added to database</source>
        <translation>အချက်အလက်ရင်းမြစ် ထဲ သီချင်းထည့်လိုက်ပြီ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1337"/>
        <source>City</source>
        <translation>မြို့</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1373"/>
        <location filename="../settings.cpp" line="1457"/>
        <source>Type</source>
        <translation>အမျိုးအစား</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1523"/>
        <source>City name missing</source>
        <translation>မြို့နာမည် လိုနေတယ်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1532"/>
        <source>City is already saved!</source>
        <translation>မြို့ ကို သိမ်းပြီးသား!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1540"/>
        <source>City added to database</source>
        <translation>Database ထဲ မြို့ ထည့်လိုက်ပြီ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1591"/>
        <source>Territory type name missing</source>
        <translation>ရပ်ကွက်အမျိုးအစားနာမည် လိုနေတယ်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1600"/>
        <source>Territory type is already saved!</source>
        <translation>ရပ်ကွက်အမျိုးအစား သိမ်းပြီးသား!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1608"/>
        <source>Territory type added to database</source>
        <translation>Database ထဲ ရပ်ကွက်အမျိုးအစား ထည့်လိုက်ပြီ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1677"/>
        <source>Name of the street type is missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1684"/>
        <source>Street type is already saved!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1692"/>
        <source>Street type added to database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2009"/>
        <source>This is no longer an option. Please request help in the forum if this is needed.</source>
        <translation>ရွေးချယ်စရာ မရှိတော့ပါ။ ဒါကို လိုအပ်မယ်ဆိုရင် ဖိုရမ်မှာ အကူအညီတောင်းပါ။</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2246"/>
        <source>Remove permissions for the selected user?</source>
        <translation>ရွေးထားတဲ့ အသုံးပြုသူကို ခွင့်ပြုချက်တွေ ဖယ်လိုက်မလား?</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2494"/>
        <source>Date</source>
        <translation>ရက်စွဲ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="835"/>
        <location filename="../settings.cpp" line="2559"/>
        <source>Theme</source>
        <translation>အကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="837"/>
        <source>Released on</source>
        <comment>Release date of the public talk outline</comment>
        <translation>ထုတ်တဲ့ရက်စွဲ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="838"/>
        <source>Discontinued on</source>
        <comment>Date after which the public talk outline should no longer be used</comment>
        <translation>ဆက်မထုတ်တော့တဲ့ ရက်စွဲ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="991"/>
        <source>Discontinuing this talk will move talks scheduled with this outline to the To Do List.

</source>
        <translation>ဒီဟောပြောချက်ကို ဆက်မလုပ်တော့ရင် ဒီအောက်လိုင်းနဲ့ ဟောပြောချက်တွေ လုပ်ရမယ့်စာရင်းထဲ ရောက်သွားမယ်

</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1110"/>
        <source>Revision</source>
        <translation>ပြန်သုံးသပ်ချက်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1414"/>
        <location filename="../settings.cpp" line="1458"/>
        <location filename="../settings.cpp" line="2123"/>
        <location filename="../settings.cpp" line="2179"/>
        <source>Name</source>
        <translation>နာမည်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1415"/>
        <location filename="../settings.cpp" line="1459"/>
        <source>Color</source>
        <translation>အရောင်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1764"/>
        <source>Number of address type is missing</source>
        <translation>လိပ်စာအမျိုးအစား နံပါတ် မရှိဘူးဖြစ်နေတယ်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1768"/>
        <source>Name of address type is missing</source>
        <translation>လိပ်စာအမျိုးအစား နာမည် မရှိဘူးဖြစ်နေတယ်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1777"/>
        <source>Address type is already saved!</source>
        <translation>လိပ်စာအမျိုးအစား သိမ်းပြီးသွားပြီ!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1787"/>
        <source>Address type added to database</source>
        <translation>လိပ်စာအမျိုးအစားကို ဒေတာရင်းမြစ်မှာ ထည့်လိုက်ပြီ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1900"/>
        <source>Error sending e-mail</source>
        <translation>အီးမေးလ် ပို့လို့ မရပါ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1985"/>
        <location filename="../settings.cpp" line="2695"/>
        <source>Select ePub file</source>
        <translation>ePub ဖိုင် ရွေးပါ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2014"/>
        <source>Warning: Make sure this file comes from a trusted source. Continue?</source>
        <translation>သတိပေးချက်။ ။ဒီဖိုင်က ယုံကြည်ရတဲ့နေရာက လာတယ်ဆိုတာ သေချာအောင် လုပ်ပါ။ ဆက်လုပ်မလား?</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2018"/>
        <source>Command File</source>
        <translation>ညွှန်ကြားချက် ဖိုင်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2411"/>
        <location filename="../settings.cpp" line="2460"/>
        <source>Meeting</source>
        <translation>အစည်းအဝေး</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2411"/>
        <source>Remove the whole meeting? (Use only to remove invalid data from database)</source>
        <translation>စည်းဝေးတစ်ခုလုံး ဖယ်လိုက်မလား? (Database က မဆိုင်တဲ့ အချက်အလက်တွေကို ဖယ်ဖို့အတွက်ပဲ သုံးပါ)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2452"/>
        <source>Enter source material here</source>
        <translation>ရင်းမြစ် အချက်အလက် ဒီနေရာမှာ ရိုက်ထည့်ပါ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2460"/>
        <source>Remove this talk? (Use only to remove invalid data from database)</source>
        <translation>ဒီဟောပြောချက်ကို ဖယ်လိုက်မလား? (Database က မဆိုင်တဲ့ အချက်အလက်တွေကို ဖယ်ဖို့အတွက်ပဲ သုံးပါ)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2495"/>
        <source>Bible Reading</source>
        <translation>ကျမ်းစာဖတ်ရှုခြင်း</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2496"/>
        <source>Song 1</source>
        <translation>သီချင်း ၁</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2497"/>
        <source>Song 2</source>
        <translation>သီချင်း ၂</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2498"/>
        <source>Song 3</source>
        <translation>သီချင်း ၃</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2558"/>
        <source>Meeting Item</source>
        <translation>စည်းဝေးသုံး</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2561"/>
        <source>Timing</source>
        <translation>အချိန်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2715"/>
        <source>Study Number</source>
        <translation>သင်ခန်းစာနံပါတ်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2716"/>
        <source>Study Name</source>
        <translation>သင်ခန်းစာခေါင်းစဥ်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="839"/>
        <location filename="../settings.cpp" line="1111"/>
        <location filename="../settings.cpp" line="1179"/>
        <location filename="../settings.cpp" line="1338"/>
        <location filename="../settings.cpp" line="1374"/>
        <location filename="../settings.cpp" line="1460"/>
        <source>Language id</source>
        <translation>ဘာသာစကား အမှတ်အသား</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1010"/>
        <source>Public talk number missing</source>
        <translation>ဟောပြောချက် နံပါတ် မထည့်ရသေး</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1013"/>
        <source>Public talk subject missing</source>
        <translation>ဟောပြောချက် အကြောင်းအရာ မထည့်ရသေး</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1034"/>
        <source>Public talk is already saved!</source>
        <translation>ဟောပြောချက် သိမ်းထားပြီးသား!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1068"/>
        <source>Public talk added to database</source>
        <translation>အချက်အလက် ရင်းမြစ်မှာ ဟောပြောချက် ထည့်လိုက်ပြီ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1075"/>
        <location filename="../settings.cpp" line="1307"/>
        <location filename="../settings.cpp" line="1546"/>
        <location filename="../settings.cpp" line="1614"/>
        <location filename="../settings.cpp" line="1698"/>
        <location filename="../settings.cpp" line="1794"/>
        <source>Adding failed</source>
        <translation>ပေါင်းထည့်တာ မအောင်မြင်ပါ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="613"/>
        <source>Exception</source>
        <translation>ခြွင်းချက်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="616"/>
        <source>Meeting 1</source>
        <translation>အစည်းအဝေး ၁</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="617"/>
        <source>Meeting 2</source>
        <translation>အစည်းအဝေး ၂</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="580"/>
        <source>Database restored. The program will be restarted.</source>
        <translation>Database restored.The program will be restarted.</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2754"/>
        <source>Remove ALL studies? (Use only to remove invalid data from database)</source>
        <translation>သွန်သင်ချက် အားလုံး ဖယ်လိုက်မလား? (အချက်အလက် ရင်းမြစ်က မမှန်တဲ့ အချက်အလက်တွေကို ဖျက်ဖို့အတွက်ပဲ သုံးပါ)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1106"/>
        <source>Speaker</source>
        <translation>ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1105"/>
        <source>Id</source>
        <translation>အမှတ်အသား</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1108"/>
        <source>Phone</source>
        <translation>ဖုန်း</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="544"/>
        <source>Save database</source>
        <translation>Save database</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="552"/>
        <source>Database backuped</source>
        <translation>Database backuped.</translation>
    </message>
</context>
<context>
    <name>StartSliderPage1.ui</name>
    <message>
        <location filename="../startup/StartSliderPage1.ui.qml" line="14"/>
        <source>Welcome to theocbase</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../startup/StartSliderPage1.ui.qml" line="38"/>
        <source>Do not show again</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StudentAssignmentPanel</name>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="71"/>
        <source>Theme</source>
        <translation>အကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="92"/>
        <source>Source</source>
        <translation>ရင်းမြစ်</translation>
    </message>
</context>
<context>
    <name>WEMeetingFinalTalkPanel</name>
    <message>
        <location filename="../qml/WEMeetingFinalTalkPanel.qml" line="48"/>
        <source>Final Talk</source>
        <comment>Circuit overseer&apos;s talk in the wekeend meeting</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StudentAssignmentPanel</name>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="112"/>
        <source>Student</source>
        <translation>ကျောင်းသား</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="120"/>
        <location filename="../qml/StudentAssignmentPanel.qml" line="171"/>
        <source>All</source>
        <comment>Dropdown column title</comment>
        <translation>အားလုံး</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="161"/>
        <source>Assistant</source>
        <translation>အကူ</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="170"/>
        <source>With Student</source>
        <comment>Dropdown column title</comment>
        <translation>ကျောင်းသားနဲ့</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="185"/>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>အကူက လိင်ခြားသူ မဖြစ်သင့်။</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="212"/>
        <source>Study point</source>
        <translation>သင်ခန်းစာအချက်</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="233"/>
        <source>Result</source>
        <translation>ရလဒ်</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="238"/>
        <source>Completed</source>
        <translation>ပြီးပြီ</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="260"/>
        <source>Volunteer</source>
        <translation>စေတနာ့ဝန်ထမ်း</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="309"/>
        <source>Timing</source>
        <translation>အချိန်</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="329"/>
        <source>Current Study</source>
        <translation>လက်ရှိသင်ခန်းစာ</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="345"/>
        <source>Exercise Completed</source>
        <translation>လေ့ကျင့်ခန်း ပြီးပြီ</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="355"/>
        <source>Next Study</source>
        <translation>နောက်သင်ခန်းစာ</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="378"/>
        <source>Note</source>
        <translation>မှတ်ချက်</translation>
    </message>
</context>
<context>
    <name>TerritoryAddAddressForm.ui</name>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="55"/>
        <source>Address</source>
        <translation>လိပ်စာ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="118"/>
        <source>Street:</source>
        <translation>လမ်း</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="128"/>
        <source>Postalcode:</source>
        <translation>စာပို့သင်္ကေတ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="150"/>
        <source>Search</source>
        <comment>Search address</comment>
        <translation>ရှာပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="165"/>
        <source>Country</source>
        <comment>Short name of country</comment>
        <translation>နိုင်ငံ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="182"/>
        <source>State</source>
        <comment>Short name of administrative area level 1</comment>
        <translation>ပြည်နယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="200"/>
        <source>County</source>
        <comment>Name of administrative area level 2</comment>
        <translation>တိုင်း/ပြည်နယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="77"/>
        <source>Country:</source>
        <translation>နိုင်ငံ:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="87"/>
        <source>State:</source>
        <comment>Administrative area level 1</comment>
        <translation>ပြည်နယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="97"/>
        <source>City:</source>
        <comment>Locality</comment>
        <translation>မြို့</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="107"/>
        <source>District:</source>
        <comment>Sublocality, first-order civil entity below a locality</comment>
        <translation>ခရိုင်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="138"/>
        <source>No.:</source>
        <translation>နံပါတ်:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="218"/>
        <source>City</source>
        <comment>Locality</comment>
        <translation>မြို့</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="235"/>
        <source>District</source>
        <comment>Sublocality, first-order civil entity below a locality</comment>
        <translation>ခရိုင်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="253"/>
        <source>Street</source>
        <comment>Streetname</comment>
        <translation>လမ်း</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="270"/>
        <source>No.</source>
        <comment>House or street number</comment>
        <translation>နံပါတ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="287"/>
        <source>Postal code</source>
        <comment>Mail code, ZIP</comment>
        <translation>စာပို့သင်္ကေတ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="304"/>
        <source>Latitude</source>
        <translation>လတ္တီတွဒ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="321"/>
        <source>Longitude</source>
        <translation>လောင်ဂျီတွဒ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="353"/>
        <source>OK</source>
        <translation>အိုကေ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="358"/>
        <source>Cancel</source>
        <translation>ဖျက်သိမ်းပါ</translation>
    </message>
</context>
<context>
    <name>TerritoryAddStreetForm.ui</name>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="32"/>
        <source>Select the streets to be added to the territory:</source>
        <comment>Add street names to territoy</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="41"/>
        <source>search</source>
        <comment>Search in the list of new street names</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="70"/>
        <source>Street</source>
        <comment>Street name</comment>
        <translation>လမ်း</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="88"/>
        <source>Hide already added streets</source>
        <comment>Hide those streets that are already in the territory&apos;s street list</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="104"/>
        <source>OK</source>
        <translation>အိုကေ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="109"/>
        <source>Cancel</source>
        <translation>ဖျက်သိမ်းပါ</translation>
    </message>
</context>
<context>
    <name>TerritoryAddressList</name>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="156"/>
        <source>Address</source>
        <translation>လိပ်စာ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="197"/>
        <source>No.</source>
        <comment>House or street number</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="243"/>
        <source>Name</source>
        <translation>နာမည်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="302"/>
        <source>Undefined [%1]</source>
        <comment>Undefined territory address type</comment>
        <translation>မသတ်မှတ်ရသေး [%1]</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="345"/>
        <source>Edit address</source>
        <translation>လိပ်စာ ပြင်မယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="397"/>
        <source>Add address</source>
        <comment>Add address to territory</comment>
        <translation>လိပ်စာ ထည့်ပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="398"/>
        <source>Please select an address in the list of search results.</source>
        <comment>Add address to territory</comment>
        <translation>ကျေးဇူးပြု၍ ရှာဖွေမှု ရလဒ်စာရင်းတွေထဲက လိပ်စာတစ်ခုကို ရွေးပါ။</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="457"/>
        <location filename="../qml/TerritoryAddressList.qml" line="467"/>
        <source>Search address</source>
        <comment>Add or edit territory address</comment>
        <translation>လိပ်စာ ရှာမယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="458"/>
        <source>No address found.</source>
        <comment>Add or edit territory address</comment>
        <translation>လိပ်စာ မတွေ့ပါ</translation>
    </message>
</context>
<context>
    <name>TerritoryAddressListForm.ui</name>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="39"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="61"/>
        <source>Territory-ID</source>
        <translation>ရက်ကွက် ID</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="83"/>
        <source>Country</source>
        <comment>Short name of country</comment>
        <translation>နိုင်ငံ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="106"/>
        <source>State</source>
        <comment>Short name of administrative area level 1</comment>
        <translation>ပြည်နယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="129"/>
        <source>County</source>
        <comment>Name of administrative area level 2</comment>
        <translation>တိုင်း/ပြည်နယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="173"/>
        <source>District</source>
        <comment>Sublocality</comment>
        <translation>ခရိုင်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="151"/>
        <source>City</source>
        <comment>Locality</comment>
        <translation>မြို့</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="196"/>
        <source>Street</source>
        <comment>Street name</comment>
        <translation>လမ်း</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="221"/>
        <source>No.</source>
        <comment>House or street number</comment>
        <translation>နံပါတ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="230"/>
        <source>Postal code</source>
        <comment>Mail code, ZIP</comment>
        <translation>စာပို့သင်္ကေတ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="253"/>
        <source>Geometry</source>
        <comment>Coordinate geometry of the address</comment>
        <translation>ဂျီသြမေထြီ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="275"/>
        <source>Name</source>
        <comment>Name of person or building</comment>
        <translation>နာမည်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="285"/>
        <source>Type</source>
        <comment>Type of address</comment>
        <translation>အမျိုးအစား</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="310"/>
        <source>Add new address</source>
        <translation>လိပ်စာအသစ် ထည့်ပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="321"/>
        <source>Edit selected address</source>
        <translation>ရွေးထားတဲ့ လိပ်စာကို ပြင်မယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="332"/>
        <source>Remove selected address</source>
        <translation>ရွေးချယ်ထားတဲ့လိပ်စာကို ဖယ်ရှားပါ</translation>
    </message>
</context>
<context>
    <name>TerritoryImportForm.ui</name>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="78"/>
        <source>Filename:</source>
        <translation>ဖိုင်နာမည်။  ။</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="133"/>
        <source>Match KML Fields</source>
        <comment>Choose which data fields in kml-file correspond to which territory properties</comment>
        <translation>KML အကွက် တိုက်ပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="55"/>
        <source>Territory boundaries</source>
        <comment>Territory data import option</comment>
        <translation>ရပ်ကွက် နယ်နိမိတ်မျဥ်းများ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="63"/>
        <source>Addresses</source>
        <comment>Territory data import option</comment>
        <translation>လိပ်စာများ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="163"/>
        <source>Description:</source>
        <comment>Territory data import KML Field</comment>
        <translation>ဖော်ပြချက်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="180"/>
        <source>Search by &quot;Description&quot; if territory is not found by &quot;Name&quot;</source>
        <translation>ရပ်ကွက်ကို &quot;နာမည်&quot; နဲ့ ရှာလို့မတွေ့ရင် &quot;ဖော်ပြချက်&quot; နဲ့ ရှာပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="195"/>
        <source>Territory No.</source>
        <comment>Territory number</comment>
        <translation>ရပ်ကွက် နံပါတ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="201"/>
        <source>Locality</source>
        <comment>Territory locality</comment>
        <translation>နယ်မြေ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="207"/>
        <source>Remark</source>
        <translation>မှတ်ချက်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="238"/>
        <source>Match Fields</source>
        <comment>Choose which fields in the import-file correspond to the address data</comment>
        <translation>အကွက်တွေ တိုက်ပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="255"/>
        <source>Address:</source>
        <comment>Territory address import field</comment>
        <translation>လိပ်စာ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="266"/>
        <source>Name:</source>
        <comment>Territory address import field</comment>
        <translation>နာမည်။  ။</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="304"/>
        <source>Address type:</source>
        <comment>Address type for territory address import</comment>
        <translation>လိပ်စာအမျိုးအစား</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="326"/>
        <source>Output filename for failed addresses:</source>
        <comment>Territory address import</comment>
        <translation>အသုံးပြုလို့မရတဲ့ လိပ်စာတွေအတွက် အထွက်ဖိုင်နာမည်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="396"/>
        <source>Import</source>
        <translation>တင်သွင်းမယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="405"/>
        <source>Close</source>
        <translation>ပိတ်မယ်</translation>
    </message>
</context>
<context>
    <name>TerritoryManagement</name>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="134"/>
        <source>Group by:</source>
        <translation>အုပ်စုအလိုက်စီမယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="141"/>
        <source>City</source>
        <comment>Group territories by city</comment>
        <translation>မြို့</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="143"/>
        <source>Type</source>
        <comment>Group territories by type of the territory</comment>
        <translation>အမျိုးအစား</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="144"/>
        <source>Worked through</source>
        <comment>Group territories by time frame they have been worked through</comment>
        <translation>ပြီးစီး</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="244"/>
        <source>Add new territory</source>
        <translation>ရပ်ကွက်အသစ်ထည့်မယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="259"/>
        <source>Remove selected territory</source>
        <translation>ရွေးထားတဲ့ ရပ်ကွက်ကို ဖယ်မယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="355"/>
        <source>Remark</source>
        <translation>မှတ်ချက်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="374"/>
        <source>Type:</source>
        <comment>Territory-type that is used to group territories</comment>
        <translation>အမျိူးအစား</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="401"/>
        <source>City:</source>
        <comment>Cityname that is used to group territories</comment>
        <translation>မြို့</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="466"/>
        <source>Assignments</source>
        <translation>တာဝန်များ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="662"/>
        <source>Publisher</source>
        <translation>ကြေညာသူ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="796"/>
        <source>Streets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="835"/>
        <source>Addresses</source>
        <translation>လိပ်စာများ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="300"/>
        <source>No.:</source>
        <translation>နံပါတ်:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="311"/>
        <source>Number</source>
        <translation>နံပါတ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="322"/>
        <source>Locality:</source>
        <translation>နယ်မြေ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="332"/>
        <source>Locality</source>
        <translation>နယ်မြေ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="436"/>
        <source>Map</source>
        <comment>Territory map</comment>
        <translation>မြေပုံ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="634"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="648"/>
        <source>Publisher-ID</source>
        <translation>ကြေညာသူ ID</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="717"/>
        <source>Checked out</source>
        <comment>Date when the publisher obtained the territory</comment>
        <translation>ရပ်ကွက်ထုတ်ယူတဲ့ ရက်စွဲ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="724"/>
        <source>Checked back in</source>
        <comment>Date when the territory is returned</comment>
        <translation>ရပ်ကွက်ပြန်အပ်တဲ့ ရက်စွဲ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="747"/>
        <source>Add new assignment</source>
        <translation>တာဝန်အသစ်ထည့်မယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="773"/>
        <source>Remove selected assignment</source>
        <translation>ရွေးထားတဲ့ တာဝန်ကို ဖယ်မယ်</translation>
    </message>
</context>
<context>
    <name>TerritoryMap</name>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="398"/>
        <source>Search address</source>
        <comment>Add or edit territory address</comment>
        <translation>လိပ်စာ ရှာမယ်</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/TerritoryMap.qml" line="851"/>
        <source>The new boundary overlaps %n territory(ies):</source>
        <comment>Add or edit territory boundary</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="853"/>
        <source>Do you want to assign overlapping areas to the current territory?
Select &#x27;No&#x27; if overlapping areas should remain in their territories and to add only the part, that doesn&#x27;t overlap other territories.</source>
        <comment>Add or edit territory boundary</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="887"/>
        <source>Join to the selected territory</source>
        <comment>Join two territories into one</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="916"/>
        <source>Add address to selected territory</source>
        <translation>ရွေးထားတဲ့ ရပ်ကွက်တွေကို လိပ်စာထည့်ပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="924"/>
        <source>Assign to selected territory</source>
        <comment>Reassign territory address</comment>
        <translation>ရွေးထားတဲ့ ရပ်ကွက် တာဝန်ပေးပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="932"/>
        <source>Remove address</source>
        <comment>Delete territory address</comment>
        <translation>လိပ်စာ ဖြုတ်မယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1022"/>
        <source>%1 of %2 address(es) imported.</source>
        <comment>Territory address import progress</comment>
        <translation>%2 ထဲက %1 လိပ်စာ(တွေ) တင်သွင်းပြီးပြီ။</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1037"/>
        <source>Import territory boundaries</source>
        <comment>Territory import dialog</comment>
        <translation>ရပ်ကွက် နယ်နိမိတ်မျဥ်းကို ထည့်သွင်းပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1047"/>
        <location filename="../qml/TerritoryMap.qml" line="1058"/>
        <location filename="../qml/TerritoryMap.qml" line="1073"/>
        <location filename="../qml/TerritoryMap.qml" line="1082"/>
        <location filename="../qml/TerritoryMap.qml" line="1115"/>
        <location filename="../qml/TerritoryMap.qml" line="1124"/>
        <source>Import territory data</source>
        <comment>Territory import dialog</comment>
        <translation>ရပ်ကွက်အချက်အလက်များကို ထည့်သွင်းပါ</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/TerritoryMap.qml" line="1048"/>
        <source>%n territory(ies) imported or updated.</source>
        <comment>Number of territories imported or updated</comment>
        <translation>
            <numerusform>%n ရပ်ကွက်(တွေ) တင်သွင်း/အသစ်ပြင်ပြီးပြီ။</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1074"/>
        <source>Please select the address and name fields.</source>
        <comment>Fields for territory address import from file</comment>
        <translation>လိပ်စာနဲ့ နာမည် အကွက်တွေကို ရွေးနှိပ်ပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1083"/>
        <source>The selected fields should be different.</source>
        <comment>Fields for territory address import from file</comment>
        <translation>ရွေးနှိပ်ထားတဲ့ အကွက်တွေ မတူရဘူး</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1091"/>
        <source>Import territory addresses</source>
        <comment>Territory import dialog</comment>
        <translation>ရပ်ကွက် လိပ်စာများကို ထည့်သွင်းပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1092"/>
        <source>The addresses will be added to the current territory. Please select a territory first.</source>
        <comment>Territory address import from file</comment>
        <translation>လိပ်စာတွေကို လက်ရှိရပ်ကွက်ထဲမှာ ထည့်မှာဖြစ်တယ်။ ကျေးဇူးပြု၍ ရပ်ကွက်ကို အရင်ရွေးချယ်ပါ။</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/TerritoryMap.qml" line="1116"/>
        <source>%n address(es) imported.</source>
        <comment>Number of addresses imported</comment>
        <translation>
            <numerusform>%n လိပ်စာ(တွေ) တင်သွင်းပြီးပြီ။</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1126"/>
        <source>No valid territory selected.</source>
        <comment>Territory boundary import</comment>
        <translation>အသုံးပြုလို့ရတဲ့ ရပ်ကွက် မရွေးထားပါ။</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1128"/>
        <source>The import file could not be read.</source>
        <comment>Territory boundary import</comment>
        <translation>တင်သွင်းလိုက်တဲ့ဖိုင်ကို ဖတ်လို့မရဘူး။</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1160"/>
        <source>Address</source>
        <comment>Default Address-field for territory address import</comment>
        <translation>လိပ်စာ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1162"/>
        <source>Name</source>
        <comment>Default Name-field for territory address import</comment>
        <translation>နာမည်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1191"/>
        <location filename="../qml/TerritoryMap.qml" line="1199"/>
        <source>Open file</source>
        <translation>ဖိုင် ဖွင့်မယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1192"/>
        <location filename="../qml/TerritoryMap.qml" line="1193"/>
        <source>KML files (*.kml)</source>
        <comment>Filedialog pattern</comment>
        <translation>KML ဖိုင်များ (*.kml)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1192"/>
        <location filename="../qml/TerritoryMap.qml" line="1200"/>
        <location filename="../qml/TerritoryMap.qml" line="1208"/>
        <source>All files (*)</source>
        <comment>Filedialog pattern</comment>
        <translation>ဖိုင် အားလုံး (*)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1200"/>
        <location filename="../qml/TerritoryMap.qml" line="1201"/>
        <location filename="../qml/TerritoryMap.qml" line="1208"/>
        <location filename="../qml/TerritoryMap.qml" line="1209"/>
        <source>CSV files (*.csv)</source>
        <comment>Filedialog pattern</comment>
        <translation>CSV ဖိုင်များ (*.csv)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1200"/>
        <location filename="../qml/TerritoryMap.qml" line="1208"/>
        <source>Text files (*.txt)</source>
        <comment>Filedialog pattern</comment>
        <translation>Text ဖိုင်များ (*.txt)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1207"/>
        <source>Save file</source>
        <translation>ဖိုင် သိမ်းမယ်</translation>
    </message>
</context>
<context>
    <name>TerritoryMapForm.ui</name>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="66"/>
        <source>search</source>
        <comment>Search address in territory map</comment>
        <translation>ရှာပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="152"/>
        <source>Import data</source>
        <comment>Import territory data from a file</comment>
        <translation>အချက်အလက်ကို ထည့်သွင်းပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="196"/>
        <source>Show/hide streets</source>
        <comment>Show/hide streets of territories</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="208"/>
        <source>Switch edit mode</source>
        <comment>Switch edit mode on territory map</comment>
        <translation>ပြင်ဆင်နည်း ပြောင်းမယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="222"/>
        <source>Create boundary</source>
        <comment>Create a new boundary for the territory</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="234"/>
        <source>Remove boundary</source>
        <comment>Remove boundary or geometry of the territory</comment>
        <translation>နယ်နိမိတ်မျဥ်းကို ပယ်ဖျက်ပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="246"/>
        <source>Split territory</source>
        <comment>Cut territory in two parts</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="163"/>
        <source>Zoom full</source>
        <comment>Zoom full to display all territories</comment>
        <translation>မျက်နှာပြင်အပြည့် ချဲ့ပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="173"/>
        <source>Show/hide territories</source>
        <comment>Show/hide boundaries of territories</comment>
        <translation>ရပ်ကွက်တွေကို ပြပါ/ဝှက်ပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="184"/>
        <source>Show/hide markers</source>
        <comment>Show/hide address markers of territories</comment>
        <translation>ညွှန်မှတ်တွေကို ပြပါ/ဝှက်ပါ</translation>
    </message>
</context>
<context>
    <name>TerritoryStreetList</name>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="102"/>
        <source>Street name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="210"/>
        <source>Undefined [%1]</source>
        <comment>Undefined street type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="278"/>
        <location filename="../qml/TerritoryStreetList.qml" line="289"/>
        <source>Add streets</source>
        <comment>Add streets to a territory</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="279"/>
        <source>No streets found.</source>
        <comment>Add streets to a territory</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TerritoryStreetListForm.ui</name>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="38"/>
        <source>ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="60"/>
        <source>Territory-ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="82"/>
        <source>Street name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="91"/>
        <source>From number</source>
        <comment>Number range of addresses in the street</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="100"/>
        <source>To number</source>
        <comment>Number range of addresses in the street</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="109"/>
        <source>Quantity</source>
        <comment>Quantity of addresses in the street</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="119"/>
        <source>Type</source>
        <comment>Type of street</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="128"/>
        <source>Geometry</source>
        <comment>Line geometry of the street</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="166"/>
        <source>Add new street</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="177"/>
        <source>Remove selected street</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TerritoryTreeModel</name>
    <message>
        <location filename="../territorymanagement.cpp" line="118"/>
        <source>&lt; 6 months</source>
        <comment>territory worked</comment>
        <translation>&lt; ၆လ</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="119"/>
        <source>6 to 12 months</source>
        <comment>territory worked</comment>
        <translation>၆လ ကနေ ၁၂လ</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="120"/>
        <source>&gt; 12 months ago</source>
        <comment>territory worked</comment>
        <translation>လွန်ခဲ့တဲ့ ၁၂လ အထက်</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="303"/>
        <source>Not worked</source>
        <comment>Group text for territories that are not worked yet.</comment>
        <translation>မလုပ်ရသေး</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="305"/>
        <location filename="../territorymanagement.cpp" line="457"/>
        <location filename="../territorymanagement.cpp" line="468"/>
        <source>Not assigned</source>
        <comment>Value of the field, the territories are grouped by, is empty.</comment>
        <translation>မချရသေး</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="316"/>
        <location filename="../territorymanagement.cpp" line="343"/>
        <location filename="../territorymanagement.cpp" line="485"/>
        <source>territory</source>
        <translation>ရပ်ကွက်</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="316"/>
        <location filename="../territorymanagement.cpp" line="343"/>
        <location filename="../territorymanagement.cpp" line="485"/>
        <source>territories</source>
        <translation>ရပ်ကွက်</translation>
    </message>
</context>
<context>
    <name>TodoPanel</name>
    <message>
        <location filename="../qml/TodoPanel.qml" line="114"/>
        <source>To Do List</source>
        <translation>လုပ်စရာ စာရင်း</translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="209"/>
        <source>Cannot schedule this item until these fields are fixed: %1</source>
        <translation>ဒီအပိုင်းကို စီစဥ်ဆွဲလို့ မရဘူး၊ ဒီအကွက်တွေကို မပြင်ဘူးဆိုရင် %1</translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="284"/>
        <source>Incoming</source>
        <comment>incoming speaker</comment>
        <translation>ဧည့်ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="284"/>
        <source>Outgoing</source>
        <comment>outgoing speaker</comment>
        <translation>သွားလည်ဟောပြောသူ</translation>
    </message>
</context>
<context>
    <name>ValidationTextInput</name>
    <message>
        <location filename="../qml/ValidationTextInput.qml" line="10"/>
        <source>Text</source>
        <translation>စာသား</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <location filename="../qml/WEMeetingChairmanPanel.qml" line="78"/>
        <source>Song</source>
        <translation>သီချင်း</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingChairmanPanel.qml" line="50"/>
        <source>Chairman</source>
        <translation>သဘာပတိ</translation>
    </message>
</context>
<context>
    <name>WEMeetingFinalTalkPanel</name>
    <message>
        <location filename="../qml/WEMeetingFinalTalkPanel.qml" line="48"/>
        <source>Service Talk</source>
        <comment>Circuit overseer&apos;s talk in the wekeend meeting</comment>
        <translation>လုပ်ငန်းတော်ဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingFinalTalkPanel.qml" line="66"/>
        <source>Speaker</source>
        <translation>ဟောပြောသူ</translation>
    </message>
</context>
<context>
    <name>WEMeetingModule</name>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="100"/>
        <location filename="../qml/WEMeetingModule.qml" line="120"/>
        <source>Weekend Meeting</source>
        <translation>အပတ်ဆုံး အစည်းအဝေး</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="116"/>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="132"/>
        <location filename="../qml/WEMeetingModule.qml" line="337"/>
        <source>Song and Prayer</source>
        <translation>သီချင်း နဲ့ ဆုတောင်းချက်</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="133"/>
        <location filename="../qml/WEMeetingModule.qml" line="338"/>
        <source>Song %1 and Prayer</source>
        <translation>သီချင်း %1 နဲ့ ဆုတောင်းချက်</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="167"/>
        <source>PUBLIC TALK</source>
        <translation>လူထုဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="184"/>
        <source>Send to To Do List</source>
        <translation>လုပ်စရာ စာရင်း ထဲကို ပို့မယ်</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="190"/>
        <source>Move to different week</source>
        <translation>တခြားအပတ်ကို ရွှေ့မယ်</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="199"/>
        <source>Clear Public Talk selections</source>
        <translation>လူထုဟောပြောချက် ရွေးချယ်မှု ဖျက်မယ်</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="248"/>
        <source>WATCHTOWER STUDY</source>
        <translation>ကင်းမျှော်စင်သင်တန်း</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="257"/>
        <source>Song %1</source>
        <translation>သီချင်း %1</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="278"/>
        <source>Import WT...</source>
        <translation>ကင်းမျှော်စင် တင်သွင်း...</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="306"/>
        <source>Conductor</source>
        <translation>သင်တန်းမှူး</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="307"/>
        <source>Reader</source>
        <translation>စာဖတ်သူ</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <location filename="../qml/WatchtowerSongPanel.qml" line="39"/>
        <source>Prayer</source>
        <translation>ဆုတောင်းချက်</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerSongPanel.qml" line="66"/>
        <source>Song</source>
        <translation>သီချင်း</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="51"/>
        <source>Watchtower Issue</source>
        <translation>ကင်းမျှော်စင်လထုတ်</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="77"/>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>ဆောင်းပါး</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="103"/>
        <source>Theme</source>
        <translation>အကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="129"/>
        <source>Conductor</source>
        <translation>သင်တန်းမှူး</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="154"/>
        <source>Reader</source>
        <translation>စာဖတ်သူ</translation>
    </message>
</context>
<context>
    <name>cPersonComboBox</name>
    <message>
        <location filename="../cpersoncombobox.cpp" line="238"/>
        <source>Brother has other meeting parts</source>
        <translation>ညီကို တခြားတာဝန် ရှိနေတယ်</translation>
    </message>
    <message>
        <location filename="../cpersoncombobox.cpp" line="233"/>
        <source>Person unavailable</source>
        <translation>သူ မအားပါ</translation>
    </message>
    <message>
        <location filename="../cpersoncombobox.cpp" line="228"/>
        <source>Outgoing speaker</source>
        <translation>သွားလည် ဟောပြောသူ</translation>
    </message>
</context>
<context>
    <name>ccongregation</name>
    <message>
        <location filename="../ccongregation.cpp" line="118"/>
        <source>(Missing Record)</source>
        <comment>database is now missing this entry</comment>
        <translation>(မှတ်တမ်း မရှိပါ)</translation>
    </message>
</context>
<context>
    <name>checkupdates</name>
    <message>
        <location filename="../checkupdates.cpp" line="100"/>
        <source>New version available</source>
        <translation>Version အသစ် ရပြီ</translation>
    </message>
    <message>
        <location filename="../checkupdates.cpp" line="102"/>
        <source>Download</source>
        <translation>ဒေါင်းလုပ် ဆွဲယူပါ</translation>
    </message>
    <message>
        <location filename="../checkupdates.cpp" line="105"/>
        <source>New version...</source>
        <translation>Version အသစ်...</translation>
    </message>
    <message>
        <location filename="../checkupdates.cpp" line="116"/>
        <source>No new update available</source>
        <translation>အသစ်တင်ထားတာ မရှိပါ</translation>
    </message>
</context>
<context>
    <name>csync</name>
    <message>
        <location filename="../csync.cpp" line="112"/>
        <source>File reading failed</source>
        <translation>ဖိုင် ဖတ်လို့မရပါ</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="450"/>
        <source>XML file generated in the wrong version.</source>
        <translation>XML file generated in the wrong version.</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="517"/>
        <source>Persons - added </source>
        <translation>လူ - ပေါင်းထည့်ပြီးပြီ </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="555"/>
        <source>Persons - updated </source>
        <translation>လူ - ပြင်ပြီးပြီ </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="586"/>
        <source>Theocratic ministry school - schedule added </source>
        <translation>သီအိုကရက်တစ် ဓမ္မအမှုကျောင်း - အစီအစဥ် ပေါင်းထည့်ပြီးပြီ </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="588"/>
        <source>Theocratic ministry school - schedule updated </source>
        <translation>သီအိုကရက်တစ် ဓမ္မအမှုကျောင်း - အစီအစဥ် အသစ်ပြင်ပြီးပြီ </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="784"/>
        <source>Theocratic Ministry School - Study updated</source>
        <translation>သီအိုကရက်တစ် ဓမ္မအမှုကျောင်း - သွန်သင်ချက် အသစ်တင်ပြီးပြီ</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="798"/>
        <source>Theocratic Ministry School - Study added</source>
        <translation>သီအိုကရက်တစ် ဓမ္မအမှုကျောင်း - သွန်သင်ချက် ထပ်ထည့်ထားတယ်</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="834"/>
        <source>Public talks - theme added </source>
        <translation>လူထုဟောပြောချက် - အကြောင်းအရာ ပေါင်းထည့်ပြီးပြီ </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="892"/>
        <source>Public Talk and WT - schedule updated </source>
        <translation>လူထုဟောပြောချက် နဲ့ ကင်းမျှော်စင် - အစီအစဥ် အသစ်ပြင်ပြီးပြီ </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="898"/>
        <source>Public Talk and WT - schedule added </source>
        <translation>လူထုဟောပြောချက် နဲ့ ကင်းမျှော်စင် - အစီအစဥ် ပေါင်းထည့်ပြီးပြီ </translation>
    </message>
</context>
<context>
    <name>cterritories</name>
    <message>
        <location filename="../cterritories.cpp" line="501"/>
        <source>Do not call</source>
        <comment>Territory address type</comment>
        <translation>မဟောပါနဲ့</translation>
    </message>
    <message>
        <location filename="../cterritories.cpp" line="1885"/>
        <source>Save failed addresses</source>
        <comment>Territory address import</comment>
        <translation>အသုံးပြုလို့မရတဲ့ လိပ်စာတွေ သိမ်းမယ်</translation>
    </message>
    <message>
        <location filename="../cterritories.cpp" line="1886"/>
        <source>The file is in read only mode</source>
        <comment>Save failed addresses in territory data import</comment>
        <translation>ဖိုင်ကို ဖတ်လို့ပဲ ရတယ်</translation>
    </message>
</context>
<context>
    <name>family</name>
    <message>
        <location filename="../family.cpp" line="124"/>
        <source>Not set</source>
        <translation>မချိန်/မသတ်မှတ်ရသေး</translation>
    </message>
</context>
<context>
    <name>googleMediator</name>
    <message>
        <location filename="../googlemediator.cpp" line="198"/>
        <source>OK</source>
        <translation>အိုကေ</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="200"/>
        <source>OK but JSON not available</source>
        <translation>အိုကေ ဒါပေမဲ့ JSON မရပါ</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="202"/>
        <source>Authorizing</source>
        <translation>လုပ်ပိုင်ခွင့် ပေးနေတယ်</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="204"/>
        <source>Authorization Failed</source>
        <translation>လုပ်ပိုင်ခွင့် ပေးတာ မအောင်မြင်ပါ</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="210"/>
        <source>Missing Client ID</source>
        <translation>သုံးစွဲသူရဲ့ အမှတ်အသား မရှိဘူး ဖြစ်နေတယ်</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="212"/>
        <source>Missing Client Secret</source>
        <translation>သုံးစွဲသူရဲ့ လျှို့ဝှက်ချက် မရှိဘူး ဖြစ်နေတယ်</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="214"/>
        <source>Need Authorization Code</source>
        <translation>လုပ်ပိုင်ခွင့် ရ ကုဒ်နံပါတ် လိုတယ်</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="216"/>
        <source>Need Token Refresh</source>
        <translation>Token Refresh လုပ်ဖို့လိုတယ်</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <location filename="../historytable.ui" line="196"/>
        <source>Number of weeks after selected date</source>
        <translation>ရွေးထားတဲ့ ရက်စွဲ နောက် အပတ်အရေအတွက်</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="246"/>
        <source>Number of weeks to gray after an assignment</source>
        <translation>တာဝန်တစ်ခုပြီးနောက် မီးခိုးရောင်ပြထားရမယ့် အပတ်အရေအတွက်</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="220"/>
        <source>weeks</source>
        <translation>အပတ်တွေ</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="122"/>
        <source>Timeline</source>
        <translation>အချိန်ဇယား</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="180"/>
        <source>Number of weeks before selected date</source>
        <translation>ရွေးထားတဲ့ ရက်စွဲ မတိုင်ခင် အပတ်အရေအတွက်</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="410"/>
        <source>Publishers</source>
        <comment>History Table (column title)</comment>
        <translation>ကြေညာသူ</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="577"/>
        <location filename="../historytable.cpp" line="584"/>
        <source>CBS</source>
        <translation>CBS</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="577"/>
        <location filename="../historytable.cpp" line="679"/>
        <source>C</source>
        <comment>abbreviation of the &apos;conductor&apos;</comment>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="584"/>
        <location filename="../historytable.cpp" line="609"/>
        <source>R</source>
        <comment>abbreviation of the &apos;reader&apos;</comment>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="606"/>
        <source>H</source>
        <comment>abbreviation of the &apos;highlights&apos;</comment>
        <translation>H</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="618"/>
        <source>TMS</source>
        <translation>TMS</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="740"/>
        <source>SM</source>
        <translation>SM</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="758"/>
        <source>PT</source>
        <comment>abbreviation of &apos;public talk&apos; for history table</comment>
        <translation>ဟောပြော</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="764"/>
        <source>Ch</source>
        <comment>abbreviation of public talk &apos;chairman&apos; for history table</comment>
        <translation>သဘာ</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="773"/>
        <source>WT-R</source>
        <comment>abbreviation of &apos;watchtower reader&apos; for history table</comment>
        <translation>စာဖတ်</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="990"/>
        <source>Date</source>
        <translation>ရက်စွဲ</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="991"/>
        <source>Theme</source>
        <translation>အဓိကအကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="992"/>
        <source>Speaker</source>
        <translation>ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="993"/>
        <source>Congregation</source>
        <translation>အသင်းတော်</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="994"/>
        <source>Chairman</source>
        <translation>သဘာပတိ</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="995"/>
        <source>Reader</source>
        <translation>စာဖတ်သူ</translation>
    </message>
</context>
<context>
    <name>importwizard</name>
    <message>
        <location filename="../importwizard.cpp" line="40"/>
        <source>Next &gt;</source>
        <translation>အရှေ့&gt;</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="39"/>
        <source>&lt; Back</source>
        <translation>&lt;အနောက်</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="41"/>
        <source>Cancel</source>
        <translation>ဖျက်သိမ်းပါ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="67"/>
        <source>Theocratic school schedule import. Copy full schedule from WTLibrary and paste below (Ctrl + V / cmd + V)</source>
        <translation>ဟောပြောသွန်သင်ခြင်းသင်တန်း အစီစဥ်ဇယားကို တင်သွင်းပါ။ ကင်းမျှော်စင်စာကြည့်တိုက်ကနေ အစီအစဥ်ဇယားကို ကူးယူပြီး အောက်မှာ ဖြည့်ပါ (Ctrl + V / cmd + V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="68"/>
        <source>Check schedule</source>
        <translation>အစီအစဥ်ဇယားကို စစ်ပါ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="71"/>
        <source>Studies import. Copy studies from WTLibrary and paste below (Ctrl + V / cmd + V)</source>
        <translation>သွန်သင်ချက်တွေ တင်သွင်းပါ။ ကင်းမျှော်စင်စာကြည့်တိုက် ကနေ သွန်သင်ချက်တွေ ကူးပြီး အောက်မှာ ဖြည့်ပါ (Ctrl + V / cmd + V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="72"/>
        <source>Check studies</source>
        <translation>သွန်သင်ချက်တွေ စစ်ပါ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="75"/>
        <source>Settings import. Copy settings from WTLibrary and paste below (Ctrl + V / cmd + V)</source>
        <translation>ဇာတ်အိမ်တွေ တင်သွင်းပါ။ WTLibrary ကနေ ဇာတ်အိမ်တွေ ကူးပြီး အောက်မှာ ဖြည့်ပါ (Ctrl + V / cmd + V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="76"/>
        <source>Check settings</source>
        <translation>ဇာတ်အိမ်တွေ စစ်ပါ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="80"/>
        <source>Check subjects</source>
        <translation>အကြောင်းအရာတွေ စစ်ပါ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="83"/>
        <source>Add speakers and congregations. Copy all data to clipboard and paste below (Ctrl + V)</source>
        <translation>ဟောပြောသူ နဲ့ အသင်းတော် ပေါင်းထည့်ပါ။ အချက်အလက်အားလုံး ကူးပြီး အောက်မှာ ဖြည့်ပါ (Ctrl + V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="84"/>
        <source>Check data</source>
        <translation>အချက်အလက် စစ်ပါ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="88"/>
        <source>Check songs</source>
        <translation>သီချင်း စစ်ပါ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="100"/>
        <source>No schedule to import.</source>
        <translation>တင်သွင်းဖို့ အစီအစဥ်ဇယား မရှိပါ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="142"/>
        <source>Date</source>
        <translation>ရက်စွဲ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="143"/>
        <source>Please add start date YYYY-MM-DD (eg. 2011-01-03)</source>
        <translation>စမယ့် ရက်စွဲ ထည့်ပါ နှစ်-လ-ရက် (ဥပမာ ၂၀၁၁-၀၁-၀၃)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="150"/>
        <source>The date is not first day of week (Monday)</source>
        <translation>ရက်စွဲက ရက်တတ်တစ်ပါတ်ရဲ့ ပထမနေ့ (တနင်္လာနေ့) မဟုတ်ဘူး</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="203"/>
        <source>Import songs</source>
        <translation>သီချင်း တင်သွင်းပါ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="245"/>
        <source>Only brothers</source>
        <translation>ညီကို များသာ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="476"/>
        <source>Subject</source>
        <translation>အကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="519"/>
        <source>id</source>
        <translation>id</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="522"/>
        <source>Congregation</source>
        <translation>အသင်းတော်</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="525"/>
        <source>Speaker</source>
        <translation>ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="528"/>
        <source>Phone</source>
        <translation>ဖုန်း</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="531"/>
        <source>Public talks</source>
        <translation>လူထုဟောပြောချက်များ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="534"/>
        <source>Language</source>
        <translation>ဘာသာစကား</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="551"/>
        <location filename="../importwizard.cpp" line="552"/>
        <source>First name</source>
        <translation>ရှေ့နာမည်</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="551"/>
        <location filename="../importwizard.cpp" line="552"/>
        <source>Last name</source>
        <translation>နောက်နာမည်</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="179"/>
        <source>Import subjects</source>
        <translation>အကြောင်းအရာ တင်သွင်းပါ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="180"/>
        <location filename="../importwizard.cpp" line="204"/>
        <source>Choose language</source>
        <translation>ဘာသာစကား ရွေးပါ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="38"/>
        <source>Save to database</source>
        <translation>Save to database</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="79"/>
        <source>Add public talk&#x27;s subjects. Copy themes and paste below (Ctrl + V / cmd + V).
Number should be in the first column and theme in the second.</source>
        <translation>လူထုဟောပြောချက် အကြောင်းအရာ ထည့်ပါ။ အကြောင်းအရာကို အောက်မှာ ကူးထည့်ပါ (Ctrl + V / cmd + V). 
ပထမတိုင်မှာ နံပါတ်၊ ဒုတိယတိုင်မှာ အကြောင်းအရာ ရှိသင့်တယ်။</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="87"/>
        <source>Add songs. Copy all data to clipboard and paste below (Ctrl + V / cmd + V).
Number should be in the first column and theme in the second.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="234"/>
        <source>date</source>
        <translation>ရက်စွဲ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="237"/>
        <source>number</source>
        <translation>နံပါတ်</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="240"/>
        <source>subject</source>
        <translation>အကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="243"/>
        <source>material</source>
        <translation>ရင်းမြစ်</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="354"/>
        <source>setting</source>
        <translation>ဇာတ်အိမ်</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="416"/>
        <source>study</source>
        <translation>သွန်သင်ချက်</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="602"/>
        <source>Title</source>
        <translation>ခေါင်းစဥ်</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="748"/>
        <source>A public talk with the same number is already saved!
Do you want to discontinue the previous talk?

Scheduled talks will be moved to the To Do List.</source>
        <translation>ဟောပြောချက် နံပါတ်တူတာ သိမ်းပြီးသား ဖြစ်နေတယ်!
အရင်တစ်ခုကို ဖျက်လိုက်တော့မလား?

စီစဥ်ထားတဲ့ ဟောပြောချက်တွေ လုပ်ရမယ့်စာရင်းထဲ ရောက်သွားမယ်</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="751"/>
        <source>Previous talk: </source>
        <translation>အရင် ဟောပြောချက်: </translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="752"/>
        <source>New talk: </source>
        <translation>ဟောပြောချက်အသစ်: </translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="873"/>
        <source>Public talk themes not found. Add themes and try again!</source>
        <translation>လူထုဟောပြောချက် အကြောင်းအရာ ရှာမတွေ့ပါ၊ အကြောင်းအရာ ထည့်ပြီး ပြန်ကြိုးစားပါ!</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="900"/>
        <source> rows added</source>
        <translation> rows added</translation>
    </message>
</context>
<context>
    <name>lmmWorksheetRegEx</name>
    <message>
        <location filename="../lmmworksheetregex.ui" line="203"/>
        <source>Language Options</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>lmmtalktypeedit</name>
    <message>
        <location filename="../lmmtalktypeedit.cpp" line="14"/>
        <source>Talk Name in the Workbook</source>
        <translation>လေ့ကျင့်ခန်းစာစောင်မှာ ဟောပြောချက်နာမည်</translation>
    </message>
    <message>
        <location filename="../lmmtalktypeedit.cpp" line="16"/>
        <source>Meeting Item</source>
        <translation>အစည်းအဝေး အပိုင်း</translation>
    </message>
    <message>
        <location filename="../lmmtalktypeedit.cpp" line="42"/>
        <source>Unknown</source>
        <comment>Unknown talk name</comment>
        <translation>မသိရ</translation>
    </message>
    <message>
        <location filename="../lmmtalktypeedit.ui" line="14"/>
        <source>Talk Type Editor</source>
        <comment>dialog name</comment>
        <translation>ဟောပြောချက် အမျိုးအစား အယ်ဒီတာ</translation>
    </message>
</context>
<context>
    <name>logindialog</name>
    <message>
        <location filename="../logindialog.ui" line="33"/>
        <source>Username</source>
        <translation>အသုံးပြုသူနာမည်</translation>
    </message>
    <message>
        <location filename="../logindialog.ui" line="50"/>
        <source>Password</source>
        <translation>လျှို့ဝှက်စာသား</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="317"/>
        <source>Select an assignment on the left to edit</source>
        <translation>ဘယ်ဘက်က တာဝန်ကို ရွေးပြီး ပြင်ပါ</translation>
    </message>
</context>
<context>
    <name>personsui</name>
    <message>
        <location filename="../personsui.ui" line="631"/>
        <source>Sister</source>
        <translation>ညီမ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="619"/>
        <source>Brother</source>
        <translation>ညီကို</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="645"/>
        <source>Servant</source>
        <translation>အမှုထမ်း</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="23"/>
        <source>Publishers</source>
        <translation>ကြေညာသူ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="811"/>
        <source>General</source>
        <translation>ယေဘုယျ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1133"/>
        <source>Assistant</source>
        <translation>အကူ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1246"/>
        <source>Public talks</source>
        <translation>လူထုဟောပြောချက်များ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="958"/>
        <location filename="../personsui.ui" line="1239"/>
        <source>Chairman</source>
        <translation>သဘာပတိ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1219"/>
        <source>Weekend Meeting</source>
        <translation>တနင်္ဂနွေစည်းဝေး</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1303"/>
        <source>Watchtower reader</source>
        <translation>ကင်းမျှော်စင် ဖတ်သူ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="657"/>
        <source>Prayer</source>
        <translation>ဆုတောင်းချက်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1177"/>
        <source>Cong. Bible Study reader</source>
        <translation>အသင်းတော်ကျမ်းစာလေ့လာမှု စာဖတ်သူ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1332"/>
        <source>Meeting for field ministry</source>
        <translation>လယ်ကွင်းစုဆုံမှု</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="368"/>
        <location filename="../personsui.cpp" line="561"/>
        <source>First name</source>
        <translation>ရှေ့နာမည်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="404"/>
        <location filename="../personsui.cpp" line="560"/>
        <source>Last name</source>
        <translation>နောက်နာမည်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="535"/>
        <source>Phone</source>
        <translation>ဖုန်း</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="599"/>
        <source>E-mail</source>
        <translation>အီးမေးလ်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1388"/>
        <source>Assignment</source>
        <translation>တာဝန်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1505"/>
        <source>Current Study</source>
        <translation>လက်ရှိ သွန်သင်ချက်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1773"/>
        <location filename="../personsui.ui" line="1796"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1269"/>
        <source>Watchtower Study Conductor</source>
        <translation>ကင်းမျှော်စင်လေ့လာမှု သင်တန်းကိုင်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="669"/>
        <source>Family Head</source>
        <translation>အိမ်ထောင်ဦးစီး</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="681"/>
        <source>Family member linked to</source>
        <translation>မိသားစုဝင်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="921"/>
        <source>Midweek Meeting</source>
        <translation>ကြားရက်စည်းဝေး</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1092"/>
        <source>Only Auxiliary Classes</source>
        <translation>အရန်သင်တန်းသာ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1065"/>
        <source>Only Main Class</source>
        <translation>ပင်မသင်တန်းသာ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="711"/>
        <source>Active</source>
        <translation>လှုပ်ရှား</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="982"/>
        <source>Treasures From God&#x27;s Word</source>
        <translation>ကျမ်းစာထဲက အဖိုးတန်ဘဏ္ဍာများ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1037"/>
        <source>Initial Call</source>
        <translation>ဦးဆုံးတွေ့ဆုံချိန်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="996"/>
        <source>Bible Reading</source>
        <translation>ကျမ်းစာဖတ်ရှုခြင်း</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1099"/>
        <source>All Classes</source>
        <translation>သင်တန်းအားလုံး</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1119"/>
        <source>Bible Study</source>
        <translation>ကျမ်းစာသင်အံမှု</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1170"/>
        <source>Congregation Bible Study</source>
        <translation>အသင်းတော်ကျမ်းစာလေ့လာမှု</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1163"/>
        <source>Living as Christians Talks</source>
        <translation>ခရစ်ယာန်အသက်တာလမ်းစဥ် ဟောပြောချက်တွေ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1112"/>
        <source>Return Visit</source>
        <translation>ပြန်လည်ပတ်မှု</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="295"/>
        <source>Personal Info</source>
        <translation>ကိုယ်ရေး အချက်အလက်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="723"/>
        <source>Host for Public Speakers</source>
        <translation>လူထုဟောပြောသူအတွက် အိမ်ရှင်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="471"/>
        <source>Mobile</source>
        <translation>မိုဘိုင်း</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="780"/>
        <source>Details</source>
        <translation>အသေးစိတ်များ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="989"/>
        <source>Spiritual Gems</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1126"/>
        <source>Talk</source>
        <translation>ဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1143"/>
        <source>Discussion with Video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1344"/>
        <source>History</source>
        <translation>မှတ်တမ်း</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1378"/>
        <source>date</source>
        <translation>ရက်စွဲ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1383"/>
        <source>no</source>
        <translation>နံပါတ်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1393"/>
        <source>Note</source>
        <translation>မှတ်ချက်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1398"/>
        <source>Time</source>
        <translation>အချိန်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1403"/>
        <source>Together</source>
        <comment>The column header text to show partner in student assignment</comment>
        <translation>အတူ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1412"/>
        <source>Studies</source>
        <translation>သွန်သင်ချက်တွေ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1477"/>
        <source>Study</source>
        <translation>သွန်သင်ချက်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1482"/>
        <source>Date assigned</source>
        <translation>တာဝန်ကျ ရက်စွဲ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1487"/>
        <source>Exercises</source>
        <translation>လေ့ကျင့်ခန်း</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1492"/>
        <source>Date completed</source>
        <translation>ပြီးတဲ့ ရက်စွဲ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1672"/>
        <source>Unavailable</source>
        <translation>မအားတဲ့ နေ့</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1706"/>
        <source>Start</source>
        <translation>အစ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1711"/>
        <source>End</source>
        <translation>အဆုံး</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="131"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="181"/>
        <location filename="../personsui.ui" line="1642"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="390"/>
        <source>A person with the same name already exists: &#x27;%1&#x27;. Do you want to change the name?</source>
        <translation>&apos;%1&apos; နာမည်တူ ရှိပြီးသား ဖြစ်နေတယ်၊ နာမည်ပြောင်းချင်လား?</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="611"/>
        <source>Remove student?</source>
        <translation>ကျောင်းသား ဖယ်မလား?</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="588"/>
        <source>Remove student</source>
        <translation>ကျောင်းသား ဖယ်မယ်</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="413"/>
        <source>%1 is scheduled for public talks! These talks will
be moved to the To Do List if you remove him as speaker.
Remove him as speaker?</source>
        <translation>%1 ကို ဟောပြောချက်အတွက် တာဝန်ပေးထားတယ်။ သူ့ကို ဟောပြောသူ အဖြစ်ကနေ ဖယ်မယ်ဆိုရင် ဒီဟောပြောချက်တွေကို လုပ်ရမယ့်စာရင်းထဲ ရွှေ့လိုက်မယ်။ သူ့ကို ဟောပြောသူ အဖြစ်ကနေ ဖယ်မလား?</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="602"/>
        <source>%1 is scheduled for public talks! These talks will
be moved to the To Do List if you remove the student.</source>
        <translation>%1 ကို ဟောပြောချက်အတွက် တာဝန်ပေးထားတယ်။ သူ့ကို ကျောင်းသား အဖြစ်ကနေ ဖယ်မယ်ဆိုရင် ဒီဟောပြောချက်တွေကို လုပ်ရမယ့်စာရင်းထဲ ရွှေ့လိုက်မယ်။</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="1085"/>
        <source>Remove study</source>
        <translation>သွန်သင်ချက် ဖယ်မယ်</translation>
    </message>
</context>
<context>
    <name>printui</name>
    <message>
        <location filename="../printui.ui" line="1109"/>
        <source>Copy to the clipboard</source>
        <translation>ကူးမယ်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="1112"/>
        <source>Copy</source>
        <translation>ကူးမယ်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="235"/>
        <location filename="../printui.ui" line="326"/>
        <source>Schedule</source>
        <translation>အချိန်ဇယား</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="14"/>
        <source>Print</source>
        <translation>စာရွက်ပရင့်ထုတ်မယ်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="390"/>
        <source>Call List and Hospitality Schedule</source>
        <translation>ဖုန်းနံပါတ်စာရင်းနဲ့ ဧည့်ဝတ်အစီအစဥ်ဇယား</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="783"/>
        <source>Midweek Meeting Title</source>
        <translation>ကြားရက်စည်းဝေးခေါင်းစဥ်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="685"/>
        <source>Weekend Meeting Title</source>
        <translation>တနင်္ဂနွေစည်းဝေးခေါင်းစဥ်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="793"/>
        <source>Show Section Titles</source>
        <translation>အပိုင်းလိုက် ခေါင်းစဥ် ပြပါ</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="764"/>
        <source>Opening Comments Title</source>
        <comment>See S-140</comment>
        <translation>နိဒါန်း ခေါင်းစဥ်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="258"/>
        <source>Assignment Slips for Assistants</source>
        <translation>အကူအတွက် တာဝန်စာရွက်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="361"/>
        <source>Outgoing Speakers Schedules</source>
        <translation>သွားလည်ဟောပြောသူ အစီအစဥ်ဇယား</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="377"/>
        <source>Outgoing Speakers Assignments</source>
        <translation>သွားလည်ဟောပြောသူ တာဝန်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="403"/>
        <source>Talks of Speakers</source>
        <translation>ဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="535"/>
        <source>Territory Map Card</source>
        <translation>ရပ်ကွက်မြေပုံကတ်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="545"/>
        <source>Map and Addresses Sheets</source>
        <translation>မြေပုံနဲ့ လိပ်စာ စာရွက်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="698"/>
        <source>Concluding Comments Title</source>
        <comment>See S-140</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.ui" line="824"/>
        <source>Show public talk revision date</source>
        <translation>လူထုဟောပြောချက် ပြန်သုံးရက်စွဲပြပါ</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="725"/>
        <source>Show duration</source>
        <translation>ကြာချိန် ပြပါ</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="705"/>
        <source>Show time</source>
        <translation>အချိန် ပြပါ</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="810"/>
        <source>Show Workbook Issue no.</source>
        <translation>လေ့ကျင့်ခန်းစာစောင် လထုတ်နံပါတ် ပြပါ</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="803"/>
        <source>Show Watchtower Issue no.</source>
        <translation>ကင်းမျှော်စင် လထုတ်နံပါတ် ပြပါ</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="817"/>
        <source>Own congregation only</source>
        <translation>ကိုယ့်အသင်းတော်သာ</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="968"/>
        <source>Territory number(s)</source>
        <translation>ရပ်ကွက် နံပါတ်(တွေ)</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="975"/>
        <source>Comma delimited; press Enter to refresh</source>
        <translation>ကော်မာ အကန့်အသတ် ရှိ။ စာမျက်တောင်ခတ်ဖို့ Enter နှိပ်ပါ</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="981"/>
        <source>1</source>
        <translation>၁</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="522"/>
        <source>Territory Record</source>
        <translation>ရက်ကွက်မှတ်တမ်း</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="870"/>
        <source>Template</source>
        <translation>ပုံစံခွက်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="896"/>
        <source>Paper Size</source>
        <translation>စာရွက်အရွယ်အစား</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="958"/>
        <source>Print From Date</source>
        <translation>ဒီရက်စွဲကနေ စာရွက်ထုတ်မယ်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="916"/>
        <source>Print Thru Date</source>
        <translation>ဒီရက်စွဲအထိ စာရွက်ထုတ်မယ်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="1039"/>
        <source>Printing</source>
        <translation>စာရွက်ပရင့်ထုတ်နေတယ်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="1042"/>
        <location filename="../printui.ui" line="1077"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="248"/>
        <source>Assignment Slips</source>
        <translation>တာဝန်ခန့် စာရွက်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="718"/>
        <source>Share in Dropbox</source>
        <translation>Dropbox မှာ ဝေမျှမယ်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="278"/>
        <location filename="../printui.ui" line="345"/>
        <source>Worksheets</source>
        <translation>မှတ်တမ်း</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="565"/>
        <source>Meetings for field ministry</source>
        <translation>လယ်ကွင်းစုဆုံမှု</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="423"/>
        <source>Combination</source>
        <translation>ပါဝင်ပေါင်းစပ်မှု</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="632"/>
        <source>Additional Options</source>
        <translation>နောက်ထပ် ရွေးချယ်စရာ</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="660"/>
        <source>Show Song Titles</source>
        <translation>သီချင်းခေါင်းစဥ် ပြပါ</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="735"/>
        <source>Show Counsel Text</source>
        <translation>သွန်သင်ချက် စာသား ပြပါ</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="474"/>
        <source>Outgoing Speakers Schedule</source>
        <translation>သွားလည် ဟောပြောသူ အစီအစဥ်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="268"/>
        <source>Print assigned only</source>
        <translation>တာဝန်ပေးထားတာကိုပဲ စာရွက်ထုတ်ပါ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="373"/>
        <source>Copied to the clipboard. Paste to word processing program (Ctrl+V/Cmd+V)</source>
        <translation>ကူးပြီးပြီ၊ တစ်နေရာရာမှာ ကူးထည့်ပါ (Ctrl+V/Cmd+V)</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="420"/>
        <location filename="../printui.cpp" line="512"/>
        <location filename="../printui.cpp" line="1148"/>
        <source>file created</source>
        <translation>ဖိုင် ဖန်တီးပြီးပြီ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="746"/>
        <source>Concluding Comments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="983"/>
        <source>Invalid entry, sorry.</source>
        <translation>အချက်အလက် ထည့်လို့မရပါ၊ တောင်းပန်ပါတယ်။</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="384"/>
        <location filename="../printui.cpp" line="432"/>
        <location filename="../printui.cpp" line="492"/>
        <source>Save file</source>
        <translation>ဖိုင် သိမ်းမယ်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="494"/>
        <source>Territories</source>
        <translation>ရပ်ကွက်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="356"/>
        <source>Custom...</source>
        <comment>pick custom paper size</comment>
        <translation>စိတ်ကြိုက်...</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="588"/>
        <source>Converting %1 to JPG file</source>
        <translation>%1 ကို JPG ဖိုင်အဖြစ် ပြောင်းနေတယ်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="207"/>
        <location filename="../printui.ui" line="461"/>
        <location filename="../printui.cpp" line="737"/>
        <source>Midweek Meeting</source>
        <translation>ကြားရက်စည်းဝေး</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="976"/>
        <source>New Custom Paper Size</source>
        <comment>title of dialog box</comment>
        <translation>စိတ်ကြိုက် စာရွက်အရွယ်အစား အသစ်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="976"/>
        <source>Format: width x height. Width and Height can be in or mm. Example 210mm x 297mm</source>
        <translation>Format: width x height. Width and Height can be in or mm. Example 210mm x 297mm</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="298"/>
        <location filename="../printui.ui" line="448"/>
        <location filename="../printui.cpp" line="738"/>
        <source>Weekend Meeting</source>
        <translation>တနင်္ဂနွေစည်းဝေး</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="745"/>
        <source>Opening Comments</source>
        <translation>နိဒါန်း</translation>
    </message>
</context>
<context>
    <name>publicmeeting_controller</name>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="193"/>
        <source>From %1</source>
        <translation>%1 ကနေ</translation>
    </message>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="221"/>
        <source>The destination date already has a talk scheduled.</source>
        <translation>ဒီရက်စွဲမှာ ဟောပြောချက် အစီအစဥ် ရှိပြီးသား ဖြစ်နေတယ်။</translation>
    </message>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="226"/>
        <source>Swap Talks</source>
        <comment>Button text</comment>
        <translation>ဟောပြောချက်တွေကို ဖလှယ်လိုက်</translation>
    </message>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="227"/>
        <source>Cancel</source>
        <comment>Button text</comment>
        <translation>ဖျက်သိမ်းပါ</translation>
    </message>
</context>
<context>
    <name>publictalkedit</name>
    <message>
        <location filename="../publictalkedit.ui" line="67"/>
        <source>Public Talk</source>
        <translation>လူထုဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="257"/>
        <location filename="../publictalkedit.ui" line="592"/>
        <location filename="../publictalkedit.ui" line="681"/>
        <location filename="../publictalkedit.ui" line="881"/>
        <location filename="../publictalkedit.cpp" line="47"/>
        <source>Theme</source>
        <translation>အဓိကအကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="169"/>
        <location filename="../publictalkedit.ui" line="866"/>
        <location filename="../publictalkedit.cpp" line="48"/>
        <source>Speaker</source>
        <translation>ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="144"/>
        <location filename="../publictalkedit.ui" line="328"/>
        <location filename="../publictalkedit.cpp" line="50"/>
        <source>Chairman</source>
        <translation>သဘာပတိ</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="232"/>
        <location filename="../publictalkedit.ui" line="617"/>
        <location filename="../publictalkedit.ui" line="642"/>
        <source>Song</source>
        <translation>သီချင်း</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="411"/>
        <source>Watchtower Study</source>
        <translation>ကင်းမျှော်စင်လေ့လာမှု</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="472"/>
        <location filename="../publictalkedit.cpp" line="52"/>
        <source>Reader</source>
        <translation>စာဖတ်သူ</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="542"/>
        <source>Source</source>
        <translation>ရင်းမြစ်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="285"/>
        <location filename="../publictalkedit.ui" line="932"/>
        <source>Move to different week</source>
        <translation>တခြားအပတ်ကို ရွေ့ပါ</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="118"/>
        <location filename="../publictalkedit.ui" line="906"/>
        <source>Send to To Do List</source>
        <translation>လုပ်စရာ စာရင်း ထဲကို ပို့မယ်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="311"/>
        <source>Clear Public Talk selections</source>
        <comment>Clear=Delete</comment>
        <translation>လူထုဟောပြောချက် ရွေးချယ်မှု ဖျက်မယ်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="567"/>
        <location filename="../publictalkedit.ui" line="658"/>
        <location filename="../publictalkedit.cpp" line="51"/>
        <source>Conductor</source>
        <translation>သင်တန်းကိုင်</translation>
    </message>
</context>
<context>
    <name>speakersui</name>
    <message>
        <location filename="../speakersui.ui" line="278"/>
        <source>qrc:/qml/CongregationMap.qml</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>publictalkedit</name>
    <message>
        <location filename="../publictalkedit.ui" line="497"/>
        <location filename="../publictalkedit.ui" line="671"/>
        <source>Service Talk</source>
        <translation>လုပ်ငန်းတော်ဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="759"/>
        <source>Outgoing Speakers This Weekend</source>
        <translation>သွားလည် ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="833"/>
        <source>Add to Outgoing List</source>
        <translation>သွားလည် ဟောပြောသူ စာရင်းထဲ ထည့်မယ်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="876"/>
        <source>Start time</source>
        <translation>စတင်ချိန်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="886"/>
        <source>Theme No.</source>
        <translation>အဓိကအကြောင်းအရာ နံပါတ်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="979"/>
        <source>To Do List</source>
        <translation>လုပ်စရာ စာရင်း</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="1019"/>
        <source>Add item to schedule</source>
        <translation>အစီအစဥ်ဇယားထဲကို တစ်ခုထည့်မယ်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="1100"/>
        <source>Add Outgoing To Do item</source>
        <comment>Add OUT item</comment>
        <translation>ထွက်သွားမယ့် လုပ်စရာ ထည့်မယ်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="793"/>
        <location filename="../publictalkedit.ui" line="1045"/>
        <source>Remove item</source>
        <translation>ဒီတစ်ခု ဖယ်မယ်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="1071"/>
        <source>Add Incoming To Do item</source>
        <translation>ဝင်လာမယ့် လုပ်စရာ ထည့်မယ်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="207"/>
        <location filename="../publictalkedit.ui" line="871"/>
        <location filename="../publictalkedit.cpp" line="49"/>
        <source>Congregation</source>
        <translation>အသင်းတော်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="89"/>
        <source>Watchtower Study Edition</source>
        <translation>ကင်းမျှော်စင် လေ့လာရန်ဆောင်းပါး</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="256"/>
        <location filename="../publictalkedit.cpp" line="291"/>
        <source>From %1; speaker removed</source>
        <comment>From [scheduled date]; speaker removed</comment>
        <translation>%1 ကစပြီး ဟောပြောသူကို ဖယ်ခဲ့</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="294"/>
        <source>From %1; speaker moved to %2</source>
        <comment>From [scheduled date]; speaker moved to [new congregation]</comment>
        <translation>%1 ကစပြီး ဟောပြောသူက %2 ကို ပြောင်းသွားတယ်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="340"/>
        <location filename="../publictalkedit.cpp" line="375"/>
        <source>From %1; talk discontinued</source>
        <comment>From [scheduled date]; talk discontinued</comment>
        <translation>%1 နေ့က ဟောပြောချက် ဆက်မထားတော့</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="695"/>
        <source>In</source>
        <comment>Incoming</comment>
        <translation>အဝင်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="695"/>
        <source>Out</source>
        <comment>Outgoing</comment>
        <translation>အထွက်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="817"/>
        <source>The selected speaker has already public talk on this calendar month. Do you want to add?</source>
        <translation>ရွေးချယ်ထားတဲ့ ဟောပြောသူက ဒီလမှာ ဟောပြောချက်တာဝန် ရှိပြီးပြီ။ ထည့်ချင်လား?</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="946"/>
        <location filename="../publictalkedit.cpp" line="997"/>
        <location filename="../publictalkedit.cpp" line="1037"/>
        <source>The destination date already has a talk scheduled. What to do?</source>
        <translation>ဒီရက်စွဲမှာ ဟောပြောချက် အစီအစဥ် ရှိပြီးသား ဖြစ်နေတယ်။ ဘာလုပ်မလဲ?</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="947"/>
        <location filename="../publictalkedit.cpp" line="998"/>
        <source>&amp;Swap Talks</source>
        <translation>&amp;ဟောပြောချက် လဲမယ်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="948"/>
        <location filename="../publictalkedit.cpp" line="999"/>
        <location filename="../publictalkedit.cpp" line="1038"/>
        <source>&amp;Move other talk to To Do List</source>
        <translation>&amp;တခြား ဟောပြောချက်ကို လုပ်စရာ စာရင်းထဲကို ရွှေ့မယ်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="949"/>
        <location filename="../publictalkedit.cpp" line="1000"/>
        <location filename="../publictalkedit.cpp" line="1039"/>
        <source>&amp;Cancel</source>
        <translation>&amp;ပယ်ဖျက်ပါ</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="966"/>
        <location filename="../publictalkedit.cpp" line="1012"/>
        <location filename="../publictalkedit.cpp" line="1048"/>
        <location filename="../publictalkedit.cpp" line="1057"/>
        <location filename="../publictalkedit.cpp" line="1079"/>
        <location filename="../publictalkedit.cpp" line="1112"/>
        <source>From %1</source>
        <translation>%1 ကနေ</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="1035"/>
        <source>Date Already Scheduled</source>
        <translation>ရက်စွဲ အစီအစဥ် ဆွဲပြီးသား</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="1066"/>
        <source>Error</source>
        <translation>ပြဿနာ</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="1066"/>
        <source>Cannot schedule this item until these fields are fixed: %1</source>
        <translation>ဒီအချက်ကို အစီစဥ်ဆွဲလို့ မရဘူး၊ ဒီအကွက်တွေကို မပြင်ဘူးဆိုရင် %1</translation>
    </message>
</context>
<context>
    <name>reminders</name>
    <message>
        <location filename="../reminders.ui" line="14"/>
        <source>Reminders</source>
        <translation>သတိပေးချက်</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="58"/>
        <source>Date range</source>
        <translation>ရက်စွဲ အကြာအရှည်</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="159"/>
        <source>Send selected reminders</source>
        <translation>ရွေးထားတဲ့ အသိပေးချက်တွေကို ပို့မယ်</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="177"/>
        <source>From</source>
        <translation>ကနေ</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="195"/>
        <source>To</source>
        <translation>အထိ</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="247"/>
        <source>Details</source>
        <translation>အချက်အလက်</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="298"/>
        <source>Date</source>
        <translation>ရက်စွဲ</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="303"/>
        <source>Name</source>
        <translation>နာမည်</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="313"/>
        <source>Subject</source>
        <translation>အကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="318"/>
        <source>Message</source>
        <translation>သတင်း</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="308"/>
        <source>E-Mail</source>
        <translation>အီးမေးလ်</translation>
    </message>
    <message>
        <location filename="../reminders.cpp" line="93"/>
        <source>Email sending...</source>
        <translation>အီးမေးလ် ပို့နေတယ်...</translation>
    </message>
    <message>
        <location filename="../reminders.cpp" line="94"/>
        <source>Cancel</source>
        <translation>ဖျက်သိမ်းပါ</translation>
    </message>
    <message>
        <location filename="../reminders.cpp" line="120"/>
        <source>Error sending e-mail</source>
        <translation>အီးမေးလ် ပို့လို့ မရပါ</translation>
    </message>
</context>
<context>
    <name>schoolreminder</name>
    <message>
        <location filename="../schoolreminder.cpp" line="57"/>
        <source>Chairman</source>
        <translation>သဘာပတိ</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="60"/>
        <source>Counselor-Class II</source>
        <translation>သွန်သင်သူ အတန်း II</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="63"/>
        <source>Counselor-Class III</source>
        <translation>သွန်သင်သူ အတန်း III</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="66"/>
        <source>Prayer I</source>
        <translation>ဆုတောင်းချက် ၁</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="69"/>
        <source>Prayer II</source>
        <translation>ဆုတောင်းချက် ၂</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="162"/>
        <source>Cancellation - Our Christian Life and Ministry Meeting Assignment</source>
        <translation>ပယ်ဖျက်ခြင်း - ခရစ်ယာန်အသက်တာနဲ့ အမှုဆောင်လုပ်ငန်း စည်းဝေး တာဝန်</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="162"/>
        <source>Our Christian Life and Ministry Meeting Assignment</source>
        <translation>ခရစ်ယာန်အသက်တာနဲ့ အမှုဆောင်လုပ်ငန်းစည်းဝေးတာဝန်</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="182"/>
        <source>Name</source>
        <translation>နာမည်</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="184"/>
        <source>Assignment</source>
        <translation>တာဝန်</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="187"/>
        <source>Theme</source>
        <translation>အဓိကအကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="189"/>
        <source>Source Material</source>
        <translation>ရင်းမြစ်</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="203"/>
        <source>Assistant</source>
        <translation>အကူ</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="213"/>
        <location filename="../schoolreminder.cpp" line="215"/>
        <source>Study</source>
        <translation>သွန်သင်ချက်</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="222"/>
        <source>Main hall</source>
        <translation>ပင်မ ခန်းမ</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="225"/>
        <source>Auxiliary classroom 1</source>
        <translation>အရန်စာသင်ခန်း ၁</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="228"/>
        <source>Auxiliary classroom 2</source>
        <translation>အရန်စာသင်ခန်း ၂</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="231"/>
        <source>To be given in</source>
        <comment>Refer to main hall or aux. classroom. See S-89</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="237"/>
        <source>Reader for Congregation Bible Study</source>
        <translation>အသင်းတော် ကျမ်းစာလေ့လာမှု အတွက် စာဖတ်သူ</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="239"/>
        <source>Assistant to %1</source>
        <comment>%1 is student&apos;s name</comment>
        <translation>%1 အတွက် အကူ</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="244"/>
        <source>Cancellation</source>
        <translation>ပြန်ရုပ်သိမ်းခြင်း</translation>
    </message>
</context>
<context>
    <name>schoolresult</name>
    <message>
        <location filename="../schoolresult.ui" line="23"/>
        <source>Volunteer</source>
        <translation>စေတနာ့ဝန်ထမ်း</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="33"/>
        <source>Study</source>
        <translation>သွန်သင်ချက်</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="45"/>
        <source>Exercise Completed</source>
        <translation>ပြီးသွားတဲ့ လေ့ကျင့်ခန်း</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="52"/>
        <source>Next study:</source>
        <translation>နောက်သွန်သင်ချက်:</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="69"/>
        <source>Setting:</source>
        <comment>for sisters assignment</comment>
        <translation>ဇာတ်အိမ်:</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="96"/>
        <source>OK</source>
        <translation>အိုကေ</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="130"/>
        <source>Cancel</source>
        <translation>ဖျက်သိမ်းပါ</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="137"/>
        <source>Completed</source>
        <translation>ပြီးသွားပြီ</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="151"/>
        <source>Timing:</source>
        <translation>အချိန်:</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="59"/>
        <source>Bible highlights</source>
        <translation>ကျမ်းစာ ပေါ်လွင်ချက်</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="61"/>
        <source>reading</source>
        <translation>ဖတ်ရှုခြင်း</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="65"/>
        <source>Assignment result</source>
        <translation>တာဝန် ရလဒ်</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="130"/>
        <source>Do not assign the next study</source>
        <translation>နောက်သွန်သင်ချက်အတွက် တာဝန် မပေးပါနဲ့</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="159"/>
        <location filename="../schoolresult.cpp" line="369"/>
        <source>Leave on current study</source>
        <translation>လက်ရှိ သွန်သင်ချက်အတိုင်း ဆက်ထားပါ</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="239"/>
        <source>Select setting</source>
        <translation>ဇာတ်အိမ် ရွေးချယ်ပါ</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="246"/>
        <source>Timing</source>
        <translation>အချိန်</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="247"/>
        <source>The timing is empty. Save?</source>
        <translation>အချိန် သတ်မှတ်ထားတာ မရှိဘူး။ သိမ်းထားမလား?</translation>
    </message>
</context>
<context>
    <name>schoolui</name>
    <message>
        <location filename="../schoolui.cpp" line="35"/>
        <source>Theme</source>
        <translation>အဓိကအကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="35"/>
        <location filename="../schoolui.cpp" line="36"/>
        <source>Source</source>
        <translation>ရင်းမြစ်</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="41"/>
        <source>Class</source>
        <translation>သင်တန်း</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="111"/>
        <source>Bible highlights:</source>
        <translation>ကျမ်းစာ ပေါ်လွင်ချက်:</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="115"/>
        <source>No. 1:</source>
        <translation>နံပါတ် ၁:</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="119"/>
        <source>No. 2:</source>
        <translation>နံပါတ် ၂:</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="123"/>
        <source>No. 3:</source>
        <translation>နံပါတ် ၃:</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="127"/>
        <source>Reader:</source>
        <translation>စာဖတ်သူ:</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="176"/>
        <source>No school</source>
        <translation>ကျောင်းမရှိ</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="329"/>
        <source>Nothing to display</source>
        <translation>ဘာမှ ပြစရာ မရှိပါ</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="495"/>
        <source>No assignment has been made!</source>
        <translation>တာဝန်မပေးရသေးပါ!</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="500"/>
        <source>Please import new school schedule from Watchtower Library (Settings-&gt;Theocratic Ministry School...)</source>
        <translation>ကင်းမျှော်စင် စာကြည့်တိုက်ကနေ ကျောင်းအချိန်ဇယား အသစ် တင်သွင်းပါ (ချိန်ညှိစရာ-&gt;သီအိုကရက်တစ် ဓမ္မအမှုကျောင်း...)</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="542"/>
        <source>Show Details...</source>
        <translation>အသေးစိတ် ပြပါ...</translation>
    </message>
</context>
<context>
    <name>speakersui</name>
    <message>
        <location filename="../speakersui.ui" line="14"/>
        <source>Congregations and Speakers</source>
        <translation>အသင်းတော် နဲ့ ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="400"/>
        <source>Congregation...</source>
        <translation>အသင်းတော်...</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="371"/>
        <source>Speaker...</source>
        <translation>ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1332"/>
        <source>Toggle Talks Editable</source>
        <translation>ဟောပြောချက် ပြင်နိုင်/မပြင်နိုင်</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1300"/>
        <source>Add Multiple Talks</source>
        <translation>ဟောပြောချက်တွေ ထည့်ပါ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="95"/>
        <location filename="../speakersui.ui" line="136"/>
        <location filename="../speakersui.ui" line="180"/>
        <location filename="../speakersui.ui" line="246"/>
        <location filename="../speakersui.ui" line="1303"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="494"/>
        <source>Select a Congregation or Speaker</source>
        <translation>အသင်းတော် သို့မဟုတ် ဟောပြောသူ ရွေးမယ်</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="725"/>
        <source>Info</source>
        <translation>အချက်အလက်</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="792"/>
        <source>Address</source>
        <translation>လိပ်စာ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="865"/>
        <location filename="../speakersui.cpp" line="180"/>
        <source>Circuit</source>
        <translation>တိုက်နယ်</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="667"/>
        <location filename="../speakersui.ui" line="1685"/>
        <location filename="../speakersui.cpp" line="188"/>
        <source>Congregation</source>
        <translation>အသင်းတော်</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="92"/>
        <source>Speakers</source>
        <translation>ဟောပြောသူများ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="133"/>
        <source>Group by congregation</source>
        <translation>အသင်းတော် အလိုက် အုပ်စုဖွဲ့ပါ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="177"/>
        <source>Group by circuit</source>
        <translation>တိုက်နယ်အလိုက် အုပ်စုဖွဲ့ပါ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="221"/>
        <source>Filter</source>
        <translation>စစ်ထုတ်ပါ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="243"/>
        <source>Configure Filter</source>
        <translation>စစ်ထုတ်မှုကို စီစဥ်ပါ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="882"/>
        <source>Congregation Details</source>
        <translation>အသင်းတော် အသေးစိတ်များ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="894"/>
        <source>Meeting Times</source>
        <translation>စည်းဝေးအချိန်များ</translation>
    </message>
</context>
<context>
    <name>printui</name>
    <message>
        <source>buttonGroup_2</source>
        <translation>buttonGroup_2</translation>
    </message>
</context>
<context>
    <name>speakersui</name>
    <message>
        <location filename="../speakersui.ui" line="999"/>
        <location filename="../speakersui.ui" line="1169"/>
        <source>Mo</source>
        <translation>တနင်္လာ</translation>
    </message>
</context>
<context>
    <name>printui</name>
    <message>
        <source>buttonGroup</source>
        <translation>buttonGroup</translation>
    </message>
</context>
<context>
    <name>speakersui</name>
    <message>
        <location filename="../speakersui.ui" line="1004"/>
        <location filename="../speakersui.ui" line="1174"/>
        <source>Tu</source>
        <translation>အင်္ဂါ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1009"/>
        <location filename="../speakersui.ui" line="1179"/>
        <source>We</source>
        <translation>ဗုဒ္ဓဟူး</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1014"/>
        <location filename="../speakersui.ui" line="1184"/>
        <source>Th</source>
        <translation>ကြာသပတေး</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1019"/>
        <location filename="../speakersui.ui" line="1189"/>
        <source>Fr</source>
        <translation>သောကြာ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1024"/>
        <location filename="../speakersui.ui" line="1194"/>
        <source>Sa</source>
        <translation>စနေ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1029"/>
        <location filename="../speakersui.ui" line="1199"/>
        <source>Su</source>
        <translation>တနင်္ဂနွေ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1363"/>
        <source>Personal Info</source>
        <translation>ကိုယ်ရေးအချက်အလက်</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1786"/>
        <location filename="../speakersui.cpp" line="842"/>
        <source>First Name</source>
        <translation>ရှေ့နာမည်</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1838"/>
        <source>Mobile</source>
        <translation>မိုဘိုင်းဖုန်း</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1576"/>
        <location filename="../speakersui.cpp" line="843"/>
        <source>Last Name</source>
        <translation>နောက်နာမည်</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1540"/>
        <source>Phone</source>
        <translation>ဖုန်း</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1377"/>
        <source>Public Talks</source>
        <translation>လူထုဟောပြောချက်များ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1646"/>
        <source>E-mail</source>
        <translation>အီးမေးလ်</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1467"/>
        <source>Notes</source>
        <translation>မှတ်စုများ</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="196"/>
        <source>Speaker</source>
        <translation>ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="370"/>
        <location filename="../speakersui.cpp" line="550"/>
        <source>Undefined</source>
        <translation>မသတ်မှတ်ရသေး</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="453"/>
        <location filename="../speakersui.cpp" line="458"/>
        <source>%1 Meeting Day/Time</source>
        <translation>စည်းဝေး နေ့စွဲ/အချိန် %1</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="583"/>
        <source>A speaker with the same name already exists: &#x27;%1&#x27;. Do you want to change the name?</source>
        <translation>&apos;%1&apos; ဟောပြောသူ နာမည်တူ ရှိပြီးသား ဖြစ်နေတယ်၊ နာမည်ပြောင်းချင်လား?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="715"/>
        <source>The congregation has speakers!</source>
        <translation>အသင်းတော်မှာ ဟောပြောသူတွေရှိတယ်။</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="720"/>
        <source>Remove the congregation?</source>
        <translation>ဒီအသင်းတော် ဖယ်လိုက်မလား?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="766"/>
        <source>Remove the speaker?</source>
        <translation>ဒီဟောပြောသူ ဖယ်လိုက်မလား?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="771"/>
        <source>The speaker is scheduled for talks! These talks will
be moved to the To Do List if you remove the speaker.</source>
        <translation>ဒီဟောပြောသူကို ဟောပြောချက်တွေအတွက် တာဝန်ပေးထားတယ်။ ဒီဟောပြောသူကို ဖယ်မယ်ဆိုရင် ဒီဟောပြောချက်တွေကို လုပ်ရမယ့်စာရင်းထဲ ရွှေ့လိုက်မယ်။</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="835"/>
        <source>Missing Information</source>
        <translation>အချက်အလက် မရှိ</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="835"/>
        <source>Select congregation first</source>
        <translation>အသင်းတော် အရင်ရွေးပါ</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="868"/>
        <source>New Congregation</source>
        <translation>အသင်းတော်အသစ်</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="936"/>
        <source>Add Talks</source>
        <translation>ဟောပြောချက် ထည့်ပါ</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="936"/>
        <source>Enter talk numbers separated by commas or periods</source>
        <translation>ဟောပြောချက် နံပါတ်ကို ပုဒ်ကလေး/မ ခံပြီး ရိုက်ထည့်ပါ</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="972"/>
        <source>Change congregation to &#x27;%1&#x27;?</source>
        <translation>&apos;%1&apos; အသင်းတော်ကို ပြောင်းမလား?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="974"/>
        <source>The speaker is scheduled for outgoing talks. These talks will
be moved to the To Do List if you change the congregation.</source>
        <translation>ဒီဟောပြောသူကို တခြား အသင်းတော်မှာ သွား ဟောပြောဖို့ တာဝန်ပေးထားတယ်။ အသင်းတော်ကို ပြောင်းလိုက်မယ်ဆိုရင် ဒီဟောပြောချက်တွေကို လုပ်ရမယ့်စာရင်းထဲ ရွှေ့လိုက်မယ်။</translation>
    </message>
</context>
<context>
    <name>startup</name>
    <message>
        <location filename="../startup.ui" line="35"/>
        <source>Start Page</source>
        <translation>အစစာမျက်နှာ</translation>
    </message>
</context>
<context>
    <name>sync_cloud</name>
    <message>
        <location filename="../sync_cloud.cpp" line="240"/>
        <source>Version conflict: The cloud changes have been made with a newer version!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sync_cloud.cpp" line="246"/>
        <source>Version conflict: The cloud data needs to be updated with the same version by an authorized user.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>territorymanagement</name>
    <message>
        <location filename="../territorymanagement.ui" line="14"/>
        <source>Territories</source>
        <translation>ရပ်ကွက်</translation>
    </message>
</context></TS>