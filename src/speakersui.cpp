/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "speakersui.h"
#include "ui_speakersui.h"
#include "general.h"
#include <ciso646>
#include <QMenu>

//
// enum is more easy to read on use
//

enum Kind : int {
    Circuit = 0,
    Congregation = 1,
    Speaker = 2
};

enum Role {
    Kind = Qt::UserRole,
    ID, // speaker or congregation ID
    ExpandedStateSav
};

//
// congregationTree
//

congregationTree::congregationTree(QWidget *parent)
    : QTreeWidget(parent)
{
    this->setDragDropMode(QAbstractItemView::DropOnly);
    this->setAcceptDrops(true);
    QObject::connect(&expandTimer, &QTimer::timeout, this, &congregationTree::expandCircuit);
    expandTimer.setSingleShot(true);
}

void congregationTree::dropEvent(QDropEvent *event)
{
    QTreeWidgetItem *item = this->itemAt(event->pos());
    auto speakerItem = this->currentItem(); //qobject_cast<QTreeWidgetItem *>(event->source());
    if (item == nullptr || speakerItem == nullptr || item->data(0, Role::Kind).toInt() != Kind::Congregation || // not a congregation
        speakerItem->data(0, Role::Kind).toInt() != Kind::Speaker || // not a speaker
        speakerItem->parent() == nullptr || // flat mode without congreg
        item == speakerItem->parent()) // same congreg
    {
        event->ignore();
        return;
    } else {
        int congregId = item->data(0, Role::ID).toInt();
        int speakerId = speakerItem->data(0, Role::ID).toInt();
        qDebug() << "current tree " << speakerItem->text(0);
        bool done;
        emit this->changeSpeakerCongregation(speakerId, congregId, done);
        if (done) {
            QTreeWidget::dropEvent(event);
            if (speakerItem->parent() == nullptr or speakerItem->parent()->isExpanded()) {
                this->setCurrentItem(speakerItem);
                return;
            }
            this->setCurrentItem(speakerItem->parent());
        }
    }
}

void congregationTree::expandCircuit()
{
    auto item = toExpandItem;
    if (item && item->childCount() > 0)
        item->setExpanded(true);
}

void congregationTree::dragMoveEvent(QDragMoveEvent *event)
{
    QTreeWidgetItem *item = this->itemAt(event->pos());
    auto speakerItem = this->currentItem(); //qobject_cast<QTreeWidgetItem *>(event->source());
    if (item == nullptr || speakerItem == nullptr) {
        QTreeWidget::dragMoveEvent(event);
        return;
    }

    // expand circuit automatically (after a little delay)
    if (item->data(0, Role::Kind).toInt() == Kind::Circuit) {
        if (item->childCount() > 0) {
            if (toExpandItem != item) {
                toExpandItem = item;
                expandTimer.start(500);
            }
        }
        QTreeWidget::dragMoveEvent(event);
        return;
    }

    toExpandItem = nullptr;
    expandTimer.stop();

    // this would be better, but when expanding is expanding/collapsing
    // unexpectedly (as a flickering effect)
    //this->setAutoExpandDelay(item->data(0, Role::Kind).toInt() == 0 ? 0 : -1);

    // tell when we accept the drop
    if (item->data(0, Role::Kind).toInt() == Kind::Congregation && speakerItem->data(0, Role::Kind).toInt() == Kind::Speaker && speakerItem->parent() != nullptr && // not flat mode without congreg
        item != speakerItem->parent()) // not same congreg
    {
        event->acceptProposedAction();
    }

    QTreeWidget::dragMoveEvent(event);
}

//
// speakersui
//

speakersui::speakersui(QWidget *parent, int openingCongregation_id)
    : QDialog(parent), ui(new Ui::speakersui), ownCongregationId(0), currentSpeaker(nullptr), sql(&Singleton<sql_class>::Instance())
{
    ui->setupUi(this);
    general::changeButtonColors(QList<QToolButton*>({ ui->btnFilter, ui->btnByCircuit, ui->btnBySpeaker, ui->btnByCongregation }),
                                this->palette().window().color());
    ui->treeWidget->hideColumn(1);
    ui->stackedWidget->setCurrentIndex(0);
    connect(ui->treeWidget, SIGNAL(changeSpeakerCongregation(int, int, bool &)),
            this, SLOT(changeCongregation(int, int, bool &)));

    QLocale local;
    qDebug() << "format" << local.timeFormat(local.ShortFormat);
    if (isRightToLeft()) {
        // workaround to show the time in right format when right-to-left user interface
        ui->meetingtime_now->setLayoutDirection(Qt::LeftToRight);
        ui->meetingtime_next_time->setLayoutDirection(Qt::LeftToRight);
    }
    ui->meetingtime_now_time->setDisplayFormat(local.timeFormat(QLocale::ShortFormat));
    ui->meetingtime_next_time->setDisplayFormat(local.timeFormat(QLocale::ShortFormat));
    ui->meetingtime_now->setLayoutDirection(layoutDirection());
    ui->meetingtime_next_time->setLayoutDirection(layoutDirection());

    lang_id = sql->getLanguageDefaultId();

    //if (openingCongregation_id == 0)
    //    loadCongregations();
    //else
    //    loadCongregations(QVariant(openingCongregation_id).toString(), true);

    if (openingCongregation_id && this->displayBy == DisplayBy::Speaker)
        this->displayBy = DisplayBy::Congregation;

    cpersons cp;
    speakerslist = cp.getAllPersons(1);
    reloadSpeakers(); // this wll also update btnBy* check states

    if (openingCongregation_id) {
        auto congregItem = findCongregation(openingCongregation_id);
        if (congregItem) {
            if (congregItem->parent())
                congregItem->parent()->setExpanded(true);
            congregItem->setExpanded(true);
        }
    }

    // filter config menu
    auto menu = new QMenu(this);
    actionFilterCircuit = new QAction(tr("Circuit"));
    actionFilterCircuit->setCheckable(true);
    actionFilterCircuit->setChecked(true); // TODO: remember/restore user state
    connect(actionFilterCircuit, &QAction::changed, [this]() {
        on_filter_textChanged(ui->filter->text());
    });
    menu->addAction(actionFilterCircuit);

    actionFilterCongregation = new QAction(tr("Congregation"));
    actionFilterCongregation->setCheckable(true);
    actionFilterCongregation->setChecked(true); // TODO: remember/restore user state
    connect(actionFilterCongregation, &QAction::changed, [this]() {
        on_filter_textChanged(ui->filter->text());
    });
    menu->addAction(actionFilterCongregation);

    actionFilterSpeaker = new QAction(tr("Speaker"));
    actionFilterSpeaker->setCheckable(true);
    actionFilterSpeaker->setChecked(true); // TODO: remember/restore user state
    connect(actionFilterSpeaker, &QAction::changed, [this]() {
        on_filter_textChanged(ui->filter->text());
    });
    menu->addAction(actionFilterSpeaker);

    ui->btnFilter->setMenu(menu);
    ui->quickWidget->setSource(QUrl("qrc:/qml/CongregationMap.qml"));

    // Map provider
    QObject *qmlRoot = qobject_cast<QObject *>(ui->quickWidget->rootObject());
    QSettings settings;
    int defaultGeoServiceProvider = settings.value("geo_service_provider/default", 0).toInt();
    QString defaultGeoServiceProviderName;
    switch (defaultGeoServiceProvider) {
    case 2: {
        defaultGeoServiceProviderName = "here";
        QString hereAppId = settings.value("geo_service_provider/here_app_id", "").toString();
        QString hereAppCode = settings.value("geo_service_provider/here_app_code", "").toString();
        qmlRoot->setProperty("hereAppId", hereAppId);
        qmlRoot->setProperty("hereToken", hereAppCode);
        break;
    }
    default:
        defaultGeoServiceProviderName = "osm";
        break;
    }
    qmlRoot->setProperty("mapProvider", defaultGeoServiceProviderName);
}

speakersui::~speakersui()
{
    qDeleteAll(speakerslist);
    delete ui;
}

void speakersui::closeEvent(QCloseEvent *event)
{
    if (save())
        event->accept();
    else
        event->ignore();
}

QTreeWidgetItem *speakersui::findCongregation(int congregId) const
{
    if (this->displayBy == DisplayBy::Speaker) {
        return nullptr;
    } else if (this->displayBy == DisplayBy::Congregation) {
        for (int i = 0; i < ui->treeWidget->topLevelItemCount(); ++i) {
            auto item = ui->treeWidget->topLevelItem(i);
            if (item->data(0, Role::ID).toInt() == congregId)
                return item;
        }
    } else if (this->displayBy == DisplayBy::Circuit) {
        for (int i = 0; i < ui->treeWidget->topLevelItemCount(); ++i) {
            auto circuit = ui->treeWidget->topLevelItem(i);
            for (int j = 0; j < circuit->childCount(); ++j) {
                auto item = circuit->child(j);
                if (item->data(0, Role::ID).toInt() == congregId)
                    return item;
            }
        }
    }
    return nullptr;
}

QTreeWidgetItem *speakersui::findSpeaker(int speakerId) const
{
    if (this->displayBy == DisplayBy::Speaker) {
        for (int i = 0; i < ui->treeWidget->topLevelItemCount(); ++i) {
            auto speakerItem = ui->treeWidget->topLevelItem(i);
            if (speakerItem->data(0, Role::ID).toInt() == speakerId)
                return speakerItem;
        }
    } else if (this->displayBy == DisplayBy::Congregation) {
        for (int i = 0; i < ui->treeWidget->topLevelItemCount(); ++i) {
            auto congregItem = ui->treeWidget->topLevelItem(i);
            for (int j = 0; j < congregItem->childCount(); ++j) {
                auto speakerItem = congregItem->child(j);
                if (speakerItem->data(0, Role::ID).toInt() == speakerId)
                    return speakerItem;
            }
        }
    } else if (this->displayBy == DisplayBy::Circuit) {
        for (int i = 0; i < ui->treeWidget->topLevelItemCount(); ++i) {
            auto circuit = ui->treeWidget->topLevelItem(i);
            for (int j = 0; j < circuit->childCount(); ++j) {
                auto congregItem = circuit->child(j);
                for (int k = 0; k < congregItem->childCount(); ++k) {
                    auto speakerItem = congregItem->child(k);
                    if (speakerItem->data(0, Role::ID).toInt() == speakerId)
                        return speakerItem;
                }
            }
        }
    }
    return nullptr;
}

void speakersui::reloadSpeakers()
{
    ui->treeWidget->clear();
    bool canDragDrop = (this->displayBy != DisplayBy::Speaker);
    ui->treeWidget->setDragEnabled(canDragDrop);
    ui->treeWidget->viewport()->setAcceptDrops(canDragDrop);
    ui->treeWidget->setDropIndicatorShown(canDragDrop);
    ui->treeWidget->setDragDropMode(canDragDrop ? QAbstractItemView::InternalMove : QAbstractItemView::NoDragDrop);

    ui->btnBySpeaker->setChecked(this->displayBy == DisplayBy::Speaker);
    ui->btnByCongregation->setChecked(this->displayBy == DisplayBy::Congregation);
    ui->btnByCircuit->setChecked(this->displayBy == DisplayBy::Circuit);

    QTreeWidgetItem *newSelectedItem = nullptr;
    ccongregation c;
    QList<ccongregation::congregation> clist = c.getAllCongregations(true);

    auto currentSpeakerID = currentSpeaker ? currentSpeaker->id() : -1;
    auto currentSpeakerCongregationID = currentSpeaker ? currentSpeaker->congregationid() : -1;

    if (this->displayBy == DisplayBy::Speaker) {
        // mode flat "all speakers"
        ui->treeWidget->setIndentation(0);
        ui->treeWidget->setSortingEnabled(true);
        ui->treeWidget->sortByColumn(0, Qt::SortOrder::AscendingOrder);

        for (person *speaker : speakerslist) {
            //if (c->id() > 0 && speaker->congregationid() != c->id()) continue;
            auto speakerItem = new QTreeWidgetItem(ui->treeWidget);
            speakerItem->setText(0, speaker->fullname("LastName, FirstName"));
            speakerItem->setData(0, Role::Kind, Kind::Speaker);
            speakerItem->setData(0, Role::ID, speaker->id());
            if (currentSpeakerID == speaker->id())
                newSelectedItem = speakerItem;
        }
    } else if (this->displayBy == DisplayBy::Congregation) {
        // tree mode "congregation/speaker"
        ui->treeWidget->resetIndentation();
        ui->treeWidget->setSortingEnabled(true);
        ui->treeWidget->sortByColumn(0, Qt::SortOrder::AscendingOrder);

        for (auto const &c : clist) {
            auto congregItem = new QTreeWidgetItem(ui->treeWidget);
            congregItem->setText(0, c.name);
            congregItem->setData(0, Role::Kind, Kind::Congregation); // means 'congreg' item
            congregItem->setData(0, Role::ID, c.id);
            congregItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDropEnabled);

            if (currentCongregation.isValid() && currentCongregation.id == c.id)
                newSelectedItem = congregItem;

            for (person *speaker : speakerslist) {
                if (speaker->congregationid() != c.id)
                    continue;
                auto speakerItem = new QTreeWidgetItem(congregItem);
                speakerItem->setText(0, speaker->fullname("LastName, FirstName"));
                speakerItem->setData(0, Role::Kind, Kind::Speaker);
                speakerItem->setData(0, Role::ID, speaker->id());
                speakerItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled);

                if (currentSpeakerID == speaker->id())
                    newSelectedItem = speakerItem;
            }
        }
    } else // this->displayBy == DisplayBy::Circuit
    {
        // tree mode "circuit/congregation/speaker"
        ui->treeWidget->resetIndentation();
        ui->treeWidget->setSortingEnabled(false); // or the "undefined" will not be the first one on the list

        QMap<QString, QTreeWidgetItem *> circuitItems;
        for (auto const &c : clist) {
            QString circuitname = c.circuit.isEmpty() ? tr("Undefined") : c.circuit;
            auto itCircuit = circuitItems.find(circuitname);
            if (itCircuit == circuitItems.end()) {
                auto circuitItem = new QTreeWidgetItem(ui->treeWidget);
                circuitItem->setText(0, circuitname);
                circuitItem->setData(0, Role::Kind, Kind::Circuit); // means 'circuit' item
                if (c.circuit.isEmpty()) {
                    QFont font = circuitItem->font(0);
                    font.setItalic(true);
                    circuitItem->setFont(0, font);
                }
                itCircuit = circuitItems.insert(circuitname, circuitItem);
            }

            auto congregItem = new QTreeWidgetItem(*itCircuit);
            congregItem->setText(0, c.name);
            congregItem->setData(0, Role::Kind, Kind::Congregation);
            congregItem->setData(0, Role::ID, c.id);
            congregItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDropEnabled);

            if (currentSpeakerID >= 0 && currentSpeakerCongregationID == c.id)
                newSelectedItem = congregItem;
            else if (currentCongregation.isValid() && currentCongregation.id == c.id)
                newSelectedItem = congregItem;

            for (person *speaker : speakerslist) {
                if (speaker->congregationid() != c.id)
                    continue;
                auto speakerItem = new QTreeWidgetItem(congregItem);
                speakerItem->setText(0, speaker->fullname("LastName, FirstName"));
                speakerItem->setData(0, Role::Kind, Kind::Speaker); // means 'speaker ' item
                speakerItem->setData(0, Role::ID, speaker->id());
                speakerItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled);

                if (currentSpeakerID == speaker->id())
                    newSelectedItem = speakerItem;
            }
        }
    }
    ui->treeWidget->expandAll(); // just for debug (must store states)
    ui->treeWidget->resizeColumnToContents(0);
    ui->treeWidget->setMinimumWidth(ui->treeWidget->columnWidth(0));
    ui->treeWidget->collapseAll(); // just for debug (must store states)
    //for (int i=0; i<ui->treeWidget->topLevelItemCount(); ++i)
    //    ui->treeWidget->topLevelWidget(i)->setExpanded(true);

    if (newSelectedItem) {
        newSelectedItem->setExpanded(true);
        ui->treeWidget->scrollToItem(newSelectedItem);
        ui->treeWidget->setCurrentItem(newSelectedItem);
        this->on_treeWidget_itemClicked(newSelectedItem, 0);
    }

    if (not ui->filter->text().isEmpty())
        on_filter_textChanged(ui->filter->text());
}

void speakersui::loadCongregationInfo(int id)
{
    ccongregation c;
    currentCongregation = c.getCongregationById(id);
    ui->lineAddress->setText(currentCongregation.address);
    ui->lineCongregation->setText(currentCongregation.name);
    ui->lineCircuit->setText(currentCongregation.circuit);
    ui->textCongregationInfo->setText(currentCongregation.info);

    if (currentCongregation.getPublicmeeting_now().getId() < 1)
        sql->updateThisYearMeetingTimes();

    setDayTimeCombo(
            ui->meetingtime_now_label, ui->meetingtime_now_day, ui->meetingtime_now_time,
            currentCongregation.getPublicmeeting_now());
    setDayTimeCombo(
            ui->meetingtime_next_label, ui->meetingtime_next_day, ui->meetingtime_next_time,
            currentCongregation.getPublicmeeting_next());

    showMap(ui->lineAddress->text());
}

void speakersui::setDayTimeCombo(QLabel *&label, QComboBox *&day, QTimeEdit *&time, ccongregation::meeting_dayandtime const &mdt)
{
    if (mdt.getIsvalid()) {
        QTime mtime = QTime::fromString(mdt.getMeetingtime(), "hh:mm");
        label->setText(tr("%1 Meeting Day/Time").arg(QString::number(mdt.getOfyear())));
        day->setCurrentIndex(mdt.getMeetingday() - 1);
        time->setTime(mtime);
    } else {
        QTime mtime = QTime::fromString("00:00", "hh:mm");
        label->setText(tr("%1 Meeting Day/Time").arg(QString::number(mdt.getOfyear())));
        day->setCurrentIndex(-1);
        time->setTime(mtime);
    }
}

void speakersui::loadSpeakerInfo()
{
    ui->lineFirstName->setText(currentSpeaker->firstname());
    ui->lineLastName->setText(currentSpeaker->lastname());
    ui->lineEmail->setText(currentSpeaker->email());
    ui->linePhone->setText(currentSpeaker->phone());
    ui->lineMobile->setText(currentSpeaker->mobile());
    ui->lineSpkrCongregation->setText(currentSpeaker->congregationName());
    ui->textSpkrNotes->setText(currentSpeaker->info());

    getPublicTalks();
}

void speakersui::showMap(QString address)
{
    QObject *qmlRoot = qobject_cast<QObject *>(ui->quickWidget->rootObject());
    qmlRoot->setProperty("address", address);
}

void speakersui::getPublicTalks()
{
    int speakerid = currentSpeaker->id();
    // clear selections
    ui->listWidgetPublicTalks->clear();
    sql_item s;
    s.insert("lang_id", lang_id);
    sql_items esitelmat = sql->selectSql("SELECT * FROM publictalks WHERE lang_id = :lang_id AND active AND (discontinue_date IS NULL OR discontinue_date='') ORDER BY theme_number", &s);
    //    sql_items esitelmat = sql->selectSql("publictalks","lang_id",
    //                                                    QString::number(lang_id),"theme_number");
    if (!esitelmat.empty()) {
        for (unsigned int i = 0; i < esitelmat.size(); i++) {
            QListWidgetItem *item = new QListWidgetItem(esitelmat[i].value("theme_number").toString() + ". " + esitelmat[i].value("theme_name").toString());
            item->setData(Qt::UserRole, esitelmat[i].value("id"));
            item->setCheckState(Qt::Unchecked);
            ui->listWidgetPublicTalks->addItem(item);
        }
    }

    clearPublicTalkSelection();

    sql_items pe = sql->selectSql("SELECT * FROM speaker_publictalks WHERE speaker_id = " + QString::number(speakerid) + " AND lang_id = " + QString::number(lang_id) + " AND active");
    if (!pe.empty()) {
        for (unsigned int i = 0; i < pe.size(); i++) {
            QString aihe_id = pe[i].value("theme_id").toString();
            for (int h = 0; h < ui->listWidgetPublicTalks->count(); h++) {
                QListWidgetItem *rowitem = ui->listWidgetPublicTalks->item(h);
                if (rowitem->data(Qt::UserRole).toString() == aihe_id) {
                    rowitem->setCheckState(Qt::Checked);
                    rowitem->setHidden(false);
                }
            }
        }
    }
}

void speakersui::clearPublicTalkSelection()
{
    for (int i = 0; i < ui->listWidgetPublicTalks->count(); i++) {
        QListWidgetItem *item = ui->listWidgetPublicTalks->item(i);
        item->setCheckState(Qt::Unchecked);
        item->setFlags(Qt::ItemIsEnabled);
        item->setHidden(true);
    }
}

bool speakersui::save()
{
    if (ui->stackedWidget->currentIndex() == 1) {
        // congregation
        qDebug() << "save congregation" << currentCongregation.id;
        if (currentCongregation.address != ui->lineAddress->text() || currentCongregation.name != ui->lineCongregation->text() || currentCongregation.circuit != ui->lineCircuit->text() || currentCongregation.info != ui->textCongregationInfo->toPlainText() || (currentCongregation.getPublicmeeting_now().getMeetingday() != ui->meetingtime_now_day->currentIndex() + 1) || (currentCongregation.getPublicmeeting_now().getMeetingtime() != ui->meetingtime_now_time->time().toString("hh:mm")) || (currentCongregation.getPublicmeeting_next().getMeetingday() != ui->meetingtime_next_day->currentIndex() + 1) || (currentCongregation.getPublicmeeting_next().getMeetingtime() != ui->meetingtime_next_time->time().toString("hh:mm"))) {
            currentCongregation.address = ui->lineAddress->text();
            currentCongregation.name = ui->lineCongregation->text();
            currentCongregation.circuit = ui->lineCircuit->text();
            currentCongregation.info = ui->textCongregationInfo->toPlainText();
            currentCongregation.getPublicmeeting_now().setMeetingday(ui->meetingtime_now_day->currentIndex() + 1);
            currentCongregation.getPublicmeeting_now().setMeetingtime(ui->meetingtime_now_time->time().toString("hh:mm"));
            currentCongregation.getPublicmeeting_next().setMeetingday(ui->meetingtime_next_day->currentIndex() + 1);
            currentCongregation.getPublicmeeting_next().setMeetingtime(ui->meetingtime_next_time->time().toString("hh:mm"));
            currentCongregation.save();

            auto congregItem = findCongregation(currentCongregation.id);
            if (congregItem) {
                // paranoiac test
                congregItem->setText(0, currentCongregation.name);

                QString circuitname = currentCongregation.circuit.isEmpty() ? tr("Undefined") : currentCongregation.circuit;
                if (this->displayBy == DisplayBy::Circuit && congregItem->parent()->text(0) != circuitname) {
                    // TODO: reparent when circuit changed !!!
                    QTreeWidgetItem *newCircuit = nullptr;
                    for (int i = 0; i < ui->treeWidget->topLevelItemCount(); ++i) {
                        auto circuit = ui->treeWidget->topLevelItem(i);
                        if (circuit->text(0) == circuitname) {
                            newCircuit = circuit;
                            break;
                        }
                    }
                    if (!newCircuit) {
                        newCircuit = new QTreeWidgetItem(ui->treeWidget);
                        newCircuit->setText(0, circuitname);
                        newCircuit->setData(0, Role::Kind, Kind::Circuit);
                    }
                    auto orginalCircuit = congregItem->parent();
                    congregItem = orginalCircuit->takeChild(orginalCircuit->indexOfChild(congregItem));
                    if (orginalCircuit->childCount() == 0)
                        delete orginalCircuit;
                    newCircuit->addChild(congregItem);
                }
            }
        }
        return true;
    } else if (ui->stackedWidget->currentIndex() == 2 && currentSpeaker) {
        // speaker
        qDebug() << "save speaker" << currentSpeaker->id();

        QSharedPointer<person> checkperson(cpersons::getPublicSpeaker(ui->lineFirstName->text() + " " + ui->lineLastName->text()));

        if (checkperson && checkperson->id() != currentSpeaker->id()) {
            if (QMessageBox::question(this, "",
                                      tr("A speaker with the same name already exists: '%1'. Do you want to change the name?").arg(ui->lineFirstName->text() + " " + ui->lineLastName->text()),
                                      QMessageBox::No, QMessageBox::Yes)
                == QMessageBox::Yes) {
                return false;
            }
        }

        currentSpeaker->setFirstname(ui->lineFirstName->text());
        currentSpeaker->setLastname(ui->lineLastName->text());
        currentSpeaker->setPhone(ui->linePhone->text());
        currentSpeaker->setMobile(ui->lineMobile->text());
        currentSpeaker->setEmail(ui->lineEmail->text());
        currentSpeaker->setInfo(ui->textSpkrNotes->toPlainText());
        if (currentSpeaker->usefor() == 0) {
            currentSpeaker->setUsefor(person::PublicTalk);
        }
        currentSpeaker->update();
        auto speakerItem = findSpeaker(currentSpeaker->id());
        if (speakerItem) // paranoiac test
            speakerItem->setText(0, currentSpeaker->fullname("LastName, FirstName"));
        else { // paranoiac test
            qDebug() << "  ERROR: Cannot retreive speaker ID(" << currentSpeaker->id() << ") after update (in function speakersui::save)";
        }

        this->on_buttonEditPublicTalks_clicked(false);
        ui->buttonEditPublicTalks->setChecked(false);
        return true;
    } else {
        return true;
    }
}

person *speakersui::GetSpeakerFromSpeakersList(int speakerid)
{
    person *result = nullptr;

    for (auto s : speakerslist) {
        if (s->id() == speakerid) {
            result = s;
            break;
        }
    }

    return result;
}

void speakersui::on_treeWidget_itemSelectionChanged()
{
    auto sel = ui->treeWidget->selectedItems();
    if (sel.empty())
        return;

    on_treeWidget_itemClicked(sel[0], 0);
}

int speakersui::GetOwnCongId()
{
    if (ownCongregationId == 0) {
        // we can cache because "my cong" can't be changed from this page
        ccongregation c;
        ownCongregationId = c.getMyCongregation().id;
    }

    return ownCongregationId;
}

bool speakersui::IsOwnCong(int congId)
{
    return GetOwnCongId() == congId;
}

void speakersui::on_treeWidget_itemClicked(QTreeWidgetItem *item, int column)
{
    if (item != nullptr && column >= 0 && save()) {
        ui->buttonRemove->setEnabled(false);

        bool hasData = false;
        int itemType = item->data(0, Role::Kind).toInt(&hasData);
        if (hasData) {
            switch (itemType) {
            case 0: // no parent
                currentCongregation = ccongregation::congregation(); // set to 'none'
                currentSpeaker = nullptr;
                ui->stackedWidget->setCurrentIndex(0);
                break;

            case 1: // congregation selected
                currentSpeaker = nullptr;
                {
                    int congId = item->data(0, Role::ID).toInt(&hasData);
                    qDebug() << congId;
                    if (congId > 0) {
                        loadCongregationInfo(congId);
                        ui->stackedWidget->setCurrentIndex(1);
                        ui->buttonRemove->setEnabled(item->childCount() == 0 && !IsOwnCong(congId));
                    }
                }
                break;

            case 2: // speaker selected
                int speakerid = item->data(0, Role::ID).toInt(&hasData);
                if (!currentSpeaker || speakerid != currentSpeaker->id()) {
                    currentSpeaker = GetSpeakerFromSpeakersList(speakerid);
                }
                qDebug() << "current speaker name2" << currentSpeaker->firstname();

                ui->stackedWidget->setCurrentIndex(2);

                if (currentSpeaker) {
                    ccongregation c;
                    currentCongregation = c.getCongregationById(currentSpeaker->congregationid());
                    loadSpeakerInfo();
                    qDebug() << "current speaker name3" << currentSpeaker->firstname();
                    ui->buttonRemove->setEnabled(true);
                }
                break;
            }

            ui->buttonAddSpeaker->setEnabled(currentCongregation.isValid());
        }
    }
}

void speakersui::on_lineAddress_editingFinished()
{
    // congregation address changed
    showMap(ui->lineAddress->text());
}

void speakersui::RemoveCongregation()
{
    if (ui->treeWidget->currentItem()->childCount() > 0) {
        QMessageBox::information(this, "", tr("The congregation has speakers!"));
    } else {
        int congId = currentCongregation.id;

        if (!IsOwnCong(congId)) {
            if (QMessageBox::question(this, "", tr("Remove the congregation?"),
                                      QMessageBox::Yes,
                                      QMessageBox::No)
                == QMessageBox::Yes) {
                ccongregation c;
                qDebug() << "removing congregation";
                if (c.removeCongregation(congId)) {
                    currentCongregation = ccongregation::congregation(); // set to 'none'
                }

                ui->stackedWidget->setCurrentIndex(0);
                ui->treeWidget->clearSelection();

                auto congItem = findCongregation(congId);
                delete congItem;
                // update tree TODO: just need to remove item
                //reloadSpeakers();
            } else {
                qDebug() << "cancelled congregation removal";
            }
        }
    }
}

bool speakersui::SpeakerHasScheduledTalks(int speakerId, PublicTalkInOut inOut, QDate startDate)
{
    QString table = inOut == In ? "publicmeeting" : "outgoing";
    return !sql->selectSql("SELECT * from " + table + " WHERE date > '" + startDate.toString(Qt::ISODate) + "' AND speaker_id = " + QVariant(speakerId).toString() + " AND active").empty();
}

bool speakersui::SpeakerHasScheduledTalks()
{
    bool hasTalks = false;

    if (currentSpeaker) {
        QDate startDate = QDateTime::currentDateTime().date().addDays(-7);

        int speakerId = currentSpeaker->id();
        hasTalks = SpeakerHasScheduledTalks(speakerId, In, startDate) || SpeakerHasScheduledTalks(speakerId, Out, startDate);
    }

    return hasTalks;
}

void speakersui::RemoveSpeaker()
{
    QString msg = tr("Remove the speaker?");

    // check for scheduled talks
    bool hasScheduledTalks = SpeakerHasScheduledTalks();
    if (hasScheduledTalks) {
        msg = tr("The speaker is scheduled for talks! These talks will\n"
                 "be moved to the To Do List if you remove the speaker.")
                + "\n" + msg;
    }

    if (QMessageBox::question(this, "", msg,
                              QMessageBox::Yes,
                              QMessageBox::No)
        == QMessageBox::Yes) {
        qDebug() << "removing speaker";

        int oldcongid = currentSpeaker->congregationid();
        int speakerId = currentSpeaker->id();
        if (IsOwnCong(oldcongid)) {
            // if speaker is from own congregation just remove speaker-flag
            int usefor = currentSpeaker->usefor();
            usefor &= ~person::PublicTalk;
            currentSpeaker->setUsefor(usefor);
            currentSpeaker->update();
            speakerslist.removeOne(currentSpeaker);
            delete currentSpeaker;
            currentSpeaker = nullptr;
        } else if (cpersons::removePerson(speakerId)) {
            speakerslist.removeOne(currentSpeaker);
            delete currentSpeaker;
            currentSpeaker = nullptr;
        }

        ui->stackedWidget->setCurrentIndex(0);
        ui->treeWidget->clearSelection();

        RemoveSpeakerFromUI(speakerId);
        if (hasScheduledTalks) {
            // move scheduled talks to To Do List
            emit updateScheduledTalks(speakerId, oldcongid, oldcongid, true);
        }
    } else {
        qDebug() << "Cancelled speaker removal";
    }
}

void speakersui::RemoveSpeakerFromUI(int speakerId)
{
    QTreeWidgetItem *speakerWidget = findSpeaker(speakerId);
    delete speakerWidget;
}

void speakersui::on_buttonRemove_clicked()
{
    // remove clicked
    switch (ui->stackedWidget->currentIndex()) {
    case 1: // congregation
        RemoveCongregation();
        break;

    case 2: // speaker
        RemoveSpeaker();
        break;
    }
}

void speakersui::on_buttonAddSpeaker_clicked()
{
    if (not currentCongregation.isValid()) {
        QMessageBox::information(this, tr("Missing Information"), tr("Select congregation first"));
        return;
    }
    if (!save())
        return;

    currentSpeaker = new person();
    currentSpeaker->setFirstname(tr("First Name"));
    currentSpeaker->setLastname(tr("Last Name"));
    currentSpeaker->setServant(true);
    currentSpeaker->setUsefor(person::PublicTalk);
    currentSpeaker->setGender(person::Male);
    currentSpeaker->setEmail("");
    currentSpeaker->setPhone("");
    currentSpeaker->setMobile("");
    currentSpeaker->setInfo("");
    currentSpeaker->setCongregationid(currentCongregation.id);
    currentSpeaker->setId(cpersons::addPerson(currentSpeaker));
    loadSpeakerInfo();

    speakerslist.push_back(currentSpeaker);
    reloadSpeakers(); //--TODO--// loadSpeakers(currentCongregation->id,currentSpeaker->id() );

    clearPublicTalkSelection();
    ui->stackedWidget->setCurrentIndex(2);
    ui->lineFirstName->setFocus();
}

void speakersui::on_buttonAddCongregation_clicked()
{
    if (!save())
        return;
    ccongregation c;
    currentCongregation = c.addCongregation(tr("New Congregation"));
    currentSpeaker = nullptr;
    reloadSpeakers(); //loadCongregations(QVariant(currentCongregation->id).toString());
    loadCongregationInfo(currentCongregation.id);
    ui->stackedWidget->setCurrentIndex(1);
    QTreeWidgetItem *item = findCongregation(currentCongregation.id);
    on_treeWidget_itemClicked(item, 1);
}

void speakersui::on_buttonEditPublicTalks_clicked(bool checked)
{
    if (!currentSpeaker)
        return;
    for (int i = 0; i < ui->listWidgetPublicTalks->count(); i++) {
        QListWidgetItem *item = ui->listWidgetPublicTalks->item(i);
        if (checked) {
            item->setHidden(false);
            item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsUserCheckable);
        } else {
            item->setHidden(item->checkState() != Qt::Checked);
            item->setFlags(Qt::ItemIsEnabled);
        }
    }

    if (!checked) {
        // save
        for (int i = 0; i < ui->listWidgetPublicTalks->count(); i++) {

            QListWidgetItem *item = ui->listWidgetPublicTalks->item(i);
            QString aihe_id = item->data(Qt::UserRole).toString();
            sql_items pe = sql->selectSql("SELECT * FROM speaker_publictalks WHERE speaker_id = " + QString::number(currentSpeaker->id()) + " AND lang_id = " + QString::number(lang_id) + " AND theme_id = '" + aihe_id + "' AND active"); //QString::number(m_ui->listWidgetPublicTalks->item(i))
            if (item->checkState() == Qt::Checked) {
                // selected
                if (pe.empty()) {
                    // add a new speaker
                    sql_item s;
                    s.insert("speaker_id", currentSpeaker->id());
                    s.insert("theme_id", aihe_id);
                    s.insert("lang_id", lang_id);
                    sql->insertSql("speaker_publictalks", &s, "id");
                }
            } else {
                // no selected
                if (!pe.empty()) {
                    // remove
                    sql_item s;
                    s.insert("speaker_id", currentSpeaker->id());
                    s.insert("theme_id", aihe_id);
                    s.insert("lang_id", lang_id);
                    s.insert("time_stamp", 0);
                    sql->execSql("UPDATE speaker_publictalks SET active = 0, time_stamp = :time_stamp WHERE speaker_id = :speaker_id "
                                 "AND theme_id = :theme_id AND lang_id = :lang_id",
                                 &s, true);
                    //                    sql->removeSql("speaker_publictalks","speaker_id = " + QString::number( currentSpeaker->id() ) +
                    //                                  " AND theme_number = '" + aihe_id + "' AND lang_id = " +
                    //                                  QString::number( lang_id ));
                }
            }
        }
    }
}

void speakersui::on_btnAddMultipleTalks_clicked()
{
    if (!save())
        return;

    bool ok;
    QString talks = QInputDialog::getText(this, tr("Add Talks"), tr("Enter talk numbers separated by commas or periods"), QLineEdit::Normal, "", &ok);
    if (!ok)
        return;

    sql_item values;
    values.insert(":speaker_id", currentSpeaker->id());
    values.insert(":lang_id", lang_id);
    values.insert(":talks", talks.replace(".", ",").replace(" ", ""));
    values.insert(":time_stamp", 0);
    sql->execSql("insert into speaker_publictalks (theme_id, speaker_id, lang_id, time_stamp, active) "
                 "select id, :speaker_id, lang_id, :time_stamp, 1 "
                 "from publictalks "
                 "where "
                 "lang_id = :lang_id and (discontinue_date IS NULL OR discontinue_date='') and "
                 "id not in (select theme_id from speaker_publictalks where speaker_id = :speaker_id and lang_id = :lang_id and active) and "
                 "',' || :talks || ',' like '%,' || cast(theme_number as text) || ',%'",
                 &values, true);
    getPublicTalks();
}

void speakersui::changeCongregation(int speakerId, int newcongid, bool &done)
{
    done = false;
    person *speaker = GetSpeakerFromSpeakersList(speakerId);
    if (speaker == nullptr || speaker->congregationid() == newcongid)
        return;

    int oldid = speaker->congregationid();
    ccongregation cclass;
    ccongregation::congregation cong(cclass.getCongregationById(newcongid));

    // check for outgoing talks
    QDate startDate = QDateTime::currentDateTime().date().addDays(-7);
    sql_items outgoingTalks;
    outgoingTalks = sql->selectSql("SELECT * from outgoing WHERE date > '" + startDate.toString(Qt::ISODate) + "' AND speaker_id = " + QVariant(speakerId).toString() + " AND active");

    QString msg = tr("Change congregation to '%1'?").arg(cong.name);
    if (!outgoingTalks.empty()) {
        msg = tr("The speaker is scheduled for outgoing talks. These talks will\n"
                 "be moved to the To Do List if you change the congregation.")
                + "\n" + msg;
    }

    if (QMessageBox::question(this, "", msg,
                              QMessageBox::Yes,
                              QMessageBox::No)
        == QMessageBox::Yes) {
        speaker->setUsefor(person::PublicTalk);
        speaker->setCongregationid(newcongid);
        speaker->update();
        currentSpeaker = nullptr;
        QTreeWidgetItem *item;
        if (ui->treeWidget->selectedItems().count() == 0) {
            item = findCongregation(oldid);
            qDebug() << "item find";
        } else {
            item = ui->treeWidget->selectedItems().at(0);
            qDebug() << "item selected";
        }
        on_treeWidget_itemClicked(item, 0);
        //item->setSelected(true);

        if (!outgoingTalks.empty()) {
            // move outgoing talks to To Do List
            emit updateScheduledTalks(speakerId, oldid, newcongid, false);
        }
        done = true;
    }
}

void speakersui::on_btnBySpeaker_clicked()
{
    if (!save()) // don't like this way of doing...
        return; // returning there could result on an unexpected UI state.

    this->displayBy = DisplayBy::Speaker;
    ui->stackedWidget->setCurrentIndex(0);
    reloadSpeakers();
}

void speakersui::on_btnByCongregation_clicked()
{
    if (!save()) // don't like this way of doing...
        return; // returning there could result on an unexpected UI state.

    this->displayBy = DisplayBy::Congregation;
    ui->stackedWidget->setCurrentIndex(0);
    reloadSpeakers();
}

void speakersui::on_btnByCircuit_clicked()
{
    if (!save()) // don't like this way of doing...
        return; // returning there could result on an unexpected UI state.

    this->displayBy = DisplayBy::Circuit;
    ui->stackedWidget->setCurrentIndex(0);
    reloadSpeakers();
}

bool showHideByFilter(QTreeWidgetItem *item, const QString &filter, int filterMask)
{
    // see if a child is visible
    bool visible = false;
    for (int i = 0; i < item->childCount(); ++i)
        visible |= showHideByFilter(item->child(i), filter, filterMask);

    // expand item if needed
    if (visible and not filter.isEmpty()) {
        auto savState = item->data(0, Role::ExpandedStateSav);
        if (not savState.isValid())
            item->setData(0, Role::ExpandedStateSav, item->isExpanded());
        item->setExpanded(true);
    }

    // test item
    bool filterMe = false;
    switch (item->data(0, Role::Kind).toInt()) {
    case Kind::Circuit:
        filterMe = filterMask bitand 1;
        break;
    case Kind::Congregation:
        filterMe = filterMask bitand 2;
        break;
    case Kind::Speaker:
        filterMe = filterMask bitand 4;
        break;
    default:
        break;
    }

    visible |= filter.isEmpty() or general::RemoveAccents(item->text(0)).contains(filter, Qt::CaseInsensitive);

    if (filterMe)
        item->setHidden(!visible);
    else
        item->setHidden(false);

    return visible;
}

void speakersui::on_filter_textChanged(const QString &userFilter)
{
    int filterMask =
            (actionFilterCircuit->isChecked() ? 1 : 0) + (actionFilterCongregation->isChecked() ? 2 : 0) + (actionFilterSpeaker->isChecked() ? 4 : 0);

    auto filterString = general::RemoveAccents(userFilter);
    for (int i = 0; i < ui->treeWidget->topLevelItemCount(); ++i) {
        auto item = ui->treeWidget->topLevelItem(i);
        showHideByFilter(item, filterString, filterMask);
    }

    if (userFilter.isEmpty()) {
        // remember current (see why later, on reuse)
        auto current = ui->treeWidget->currentItem();

        // restore expanded states and erase saved value
        for (int i = 0; i < ui->treeWidget->topLevelItemCount(); ++i) {
            auto item1 = ui->treeWidget->topLevelItem(i);
            auto savState1 = item1->data(0, Role::ExpandedStateSav);
            if (savState1.isValid()) {
                item1->setData(0, Role::ExpandedStateSav, QVariant());
                item1->setExpanded(savState1.toBool());
            }

            for (int j = 0; j < item1->childCount(); ++j) {
                auto item2 = item1->child(j);
                auto savState2 = item2->data(0, Role::ExpandedStateSav);
                if (savState2.isValid()) {
                    item2->setData(0, Role::ExpandedStateSav, QVariant());
                    item2->setExpanded(savState2.toBool());
                }
            }
        }

        // ensure current one stay visible (because can be is an originally unexpanded part of the tree)
        ui->treeWidget->scrollToItem(current);
    }
}
