/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import net.theocbase 1.0

ScrollView {
    id: wtStudyEdit
    property string title: "Watchtower Study"
    width: 500
    clip: true

    property CPTMeeting meeting
    property PublicMeetingController wt_controller: PublicMeetingController{}
    onMeetingChanged: {
        if (meeting)
            wt_controller.date = meeting.date
    }

    anchors.fill: parent

    ColumnLayout {
        id: layout
        x: 10
        y: 10
        width: wtStudyEdit.width - 20

        RowLayout {
            ToolButton {
                icon.source: "qrc:/icons/wt_source.svg"
                background: null
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                ToolTip.text: qsTr("Watchtower Issue")
                ToolTip.visible: hovered
                Layout.fillWidth: true
            }
            NumberSelector {
                maxValue: 12
                Layout.fillWidth: true
                Layout.preferredHeight: height
                selectedValue: meeting.wtIssue == "" ? 0 : parseInt(meeting.wtIssue)
                onSelectedValueChanged: {
                    var checkValue = 0
                    if (meeting.wtIssue != "")
                        checkValue = parseInt(meeting.wtIssue)
                    if (selectedValue !== checkValue) {
                        meeting.wtIssue = selectedValue
                        meeting.save()
                    }
                }
            }
        }

        RowLayout {
            ToolButton {
                icon.source: "qrc:/icons/numbers.svg"
                background: null
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                ToolTip.text: qsTr("Article", "The number of Watchtower article")
                ToolTip.visible: hovered
                visible: meeting.date > new Date("2019-03-03")
            }
            TextField {
                Layout.fillWidth: true
                background: null
                topPadding: 0
                bottomPadding: 0
                text: meeting.wtSource
                //enabled: false
                visible: meeting.date > new Date("2019-03-03")
                onEditingFinished: {
                    if (meeting.wtSource != text) {
                        meeting.wtSource = text
                        meeting.save()
                    }
                }
            }
        }

        RowLayout {
            ToolButton {
                icon.source: "qrc:/icons/title.svg"
                background: null
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                ToolTip.text: qsTr("Theme")
                ToolTip.visible: hovered
            }
            TextField {
                text: meeting.wtTheme
                font.bold: true
                background: null
                topPadding: 0
                bottomPadding: 0
                selectByMouse: true
                onEditingFinished: {
                    if (meeting.wtTheme != text) {
                        meeting.wtTheme = text
                        meeting.save()
                    }
                }
                Layout.fillWidth: true
                wrapMode: Text.WordWrap
            }
        }

        // WT Conductor
        RowLayout {
            ToolButton {
                icon.source: "qrc:/icons/servant.svg"
                background: null
                ToolTip.text: qsTr("Conductor")
                ToolTip.visible: hovered
            }
            ComboBoxTable {
                Layout.fillWidth: true
                currentText: meeting.wtConductor ? meeting.wtConductor.fullname : ""
                column4.visible: false
                onBeforeMenuShown: {
                    model = wt_controller.brotherList(Publisher.WtCondoctor)
                    column2.resizeToContents()
                }
                onRowSelected: {
                    console.log(id)
                    var wtconductor = CPersons.getPerson(id)
                    meeting.wtConductor = wtconductor
                    meeting.save()
                }
            }
        }

        // WT Reader
        RowLayout {
            ToolButton {
                icon.source: "qrc:/icons/reader.svg"
                background: null
                ToolTip.text: qsTr("Reader")
                ToolTip.visible: hovered
            }
            ComboBoxTable {
                Layout.fillWidth: true
                Layout.bottomMargin: 10
                currentText: meeting.wtReader ? meeting.wtReader.fullname : ""
                column4.visible: false
                onBeforeMenuShown: {
                    model = wt_controller.brotherList(Publisher.WtReader)
                    column2.resizeToContents()
                }
                onRowSelected: {
                    console.log(id)
                    var wtreader = CPersons.getPerson(id)
                    meeting.wtReader = wtreader
                    meeting.save()
                }
            }
        }
    }
}

