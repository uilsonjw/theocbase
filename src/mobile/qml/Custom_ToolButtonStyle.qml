import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2
import QtGraphicalEffects 1.0

ButtonStyle {
    readonly property ToolButton control: __control
    property string overlayColor:  control.enabled ? "transparent" : "grey"
    property Component panel: Item {
        id: styleitem

        //TODO: Maybe we want to add a descriptive text at the bottom of the icon?
        implicitWidth: 40
        implicitHeight: 40

        opacity: control.pressed ? 0.5 : 1

        Text {
            id: label
            color: enabled ? "#116bc6" : "grey"
            visible: icon.status != Image.Ready
            anchors.centerIn: parent
            width: parent.width
            text: control.text
            horizontalAlignment: Text.AlignHCenter
            elide: Text.ElideRight
        }
        Image {
            id: icon
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            anchors.margins: styleitem.height / 6
            source: control.iconSource
            opacity: control.enabled ? 1 : 0.5
            sourceSize: Qt.size(60,60)
            visible: false
            //z: 1
        }
        ColorOverlay {
            anchors.fill: icon
            source: icon
            color: overlayColor
        }

        Rectangle {
            //border.width: 1
            //border.color: "black"
            anchors.fill: parent
            //anchors.margins: -10
            //color: "orange"
            border.color: "orange"
            border.width: 1
            //radius: 2
            visible: false//control.checked
            opacity: 0.1
            z:2
        }
        Rectangle {
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            height: 3*dpi
            color: "#2f4870"
            visible: control.checked
        }
    }
}
