/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "outgoingspeakersmodel.h"

OutgoingSpeakersModel::OutgoingSpeakersModel(QObject *parent) : QAbstractTableModel(parent)
{
}

OutgoingSpeakersModel::~OutgoingSpeakersModel()
{
    qDeleteAll(_list);
    _list.clear();
}

int OutgoingSpeakersModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return _list.empty() ? 0 : _list.count();
}

int OutgoingSpeakersModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return 6;
}

QHash<int, QByteArray> OutgoingSpeakersModel::roleNames() const
{
    QHash<int, QByteArray> items;
    items[IdRole] = "id";
    items[SpeakerRole] = "speaker";
    items[SpeakerIdRole] = "speakerId";
    items[CongregationRole] = "congregation";
    items[CongregationIdRole] = "congregationId";
    items[CongregationInfo] = "congregationInfo";
    items[CongregationAddress] = "congregationAddress";
    items[TimeRole] = "time";
    items[ThemeRole] = "theme";
    items[ThemeNoRole] = "themeNo";
    items[ThemeIdRole] = "themeId";
    items[DateRole] = "date";
    return items;
}

QVariant OutgoingSpeakersModel::data(const QModelIndex &index, int role) const
{
    switch(role)
    {
    case IdRole:
        return _list[index.row()]->getId();
    case SpeakerRole:        
        return _list[index.row()]->getSpeaker() ? _list[index.row()]->getSpeaker()->fullname() : "";
    case SpeakerIdRole:
        return _list[index.row()]->getSpeaker() ? _list[index.row()]->getSpeaker()->id() : -1;
    case CongregationRole:
        return _list[index.row()]->getCongregation().id > 0 ? _list[index.row()]->getCongregation().name : "";
    case CongregationIdRole:
        return _list[index.row()]->getCongregation().id;
    case CongregationAddress:
        return _list[index.row()]->getCongregation().address;
    case CongregationInfo:
        return _list[index.row()]->getCongregation().info;
    case CongregationCircuit:
        return _list[index.row()]->getCongregation().circuit;
    case TimeRole:
        return _list[index.row()]->getCongregation().getPublicmeeting(_date).getMeetingtime();
    case ThemeRole:
        return _list[index.row()]->getTheme().theme;
    case ThemeNoRole:
        return _list[index.row()]->getTheme().number;
    case ThemeIdRole:                
        return _list[index.row()]->getTheme().id;
    case DateRole:
        return _list[index.row()]->date().startOfDay();
    default:
        return QVariant();
    }
}

QVariantMap OutgoingSpeakersModel::get(int row) const {
    QHash<int,QByteArray> names = roleNames();
    QHashIterator<int, QByteArray> i(names);
    QVariantMap res;
    QModelIndex idx = index(row, 0);
    while (i.hasNext()) {
        i.next();
        QVariant data = idx.data(i.key());
        res[i.value()] = data;
    }
    return res;
}

void OutgoingSpeakersModel::loadList(const QDate d)
{
    beginResetModel();
    qDeleteAll(_list);
    _list.clear();
    _date = QDate(d);
    cpublictalks c;
    _list = c.getOutgoingSpeakers(d);
    endResetModel();    
    emit modelChanged();
}

void OutgoingSpeakersModel::addRow(const int speakerId, const int congId, const int themeId)
{
    if (!_date.isValid())
        return;
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    cpublictalks c;
    _list.append(c.addOutgoingSpeaker(_date,speakerId,themeId,congId));
    endInsertRows();
    emit modelChanged();
}

void OutgoingSpeakersModel::removeRow(const int rowIndex)
{
    beginRemoveRows(QModelIndex(), rowIndex, rowIndex);
    cpublictalks c;
    c.removeOutgoingSpeaker(get(rowIndex)["id"].toInt());
    _list.removeAt(rowIndex);
    endRemoveRows();    
    emit modelChanged();
}

void OutgoingSpeakersModel::editRow(const int row, const int speakerId, const int themeId, const int congId)
{
    cpublictalks c;
    beginResetModel();
    _list[row]->setSpeaker(cpersons::getPerson(speakerId));
    _list[row]->setTheme(c.getThemeById(themeId));
    _list[row]->setCongregation(congId);
    _list[row]->save();
    endResetModel();
    emit modelChanged();
}

bool OutgoingSpeakersModel::moveToTodo(const int row)
{
    if (!_list[row]->getSpeaker())
        return false;
    todo t(false);
    t.setCongregation(_list[row]->getCongregation().name);
    t.setSpeaker(_list[row]->getSpeaker()->fullname());
    t.setTheme(QString("%1 (%2)").arg(_list[row]->getTheme().theme,
                                      QVariant(_list[row]->getTheme().number).toString()));
    t.setNotes(tr("From %1").arg(_list[row]->date().toString(Qt::ISODate)));
    t.save();
    removeRow(row);
    return true;
}
