#include "general.h"

QDate general::TextToDate(QString text)
{
    QString dateFormat;
    QDate date;
    date = QDate::fromString(text, Qt::ISODate);
    if (!date.isValid()) {
        dateFormat = QLocale().dateFormat(QLocale::ShortFormat);
        date = QDate::fromString(text, dateFormat);
    }
    if (!date.isValid()) {
        dateFormat = QLocale().dateFormat(QLocale::LongFormat);
        date = QDate::fromString(text, dateFormat);
    }
    if (!dateFormat.isEmpty() && !dateFormat.contains("yyyy"))
        date = date.addYears(99).year() > QDate::currentDate().year() ? date : date.addYears(100);
    return date;
}


QString general::RemoveAccents(QString const& s)
{
    static QString diacriticLetters_;
    static QStringList noDiacriticLetters_;

    if (diacriticLetters_.isEmpty()) {
        diacriticLetters_ = QString::fromUtf8("ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýÿ");
        noDiacriticLetters_ << "S"<<"OE"<<"Z"<<"s"<<"oe"<<"z"<<"Y"<<"Y"<<"u"<<"A"<<"A"<<"A"<<"A"<<"A"<<"A"<<"AE"<<"C"<<"E"<<"E"<<"E"<<"E"<<"I"<<"I"<<"I"<<"I"<<"D"<<"N"<<"O"<<"O"<<"O"<<"O"<<"O"<<"O"<<"U"<<"U"<<"U"<<"U"<<"Y"<<"s"<<"a"<<"a"<<"a"<<"a"<<"a"<<"a"<<"ae"<<"c"<<"e"<<"e"<<"e"<<"e"<<"i"<<"i"<<"i"<<"i"<<"o"<<"n"<<"o"<<"o"<<"o"<<"o"<<"o"<<"o"<<"u"<<"u"<<"u"<<"u"<<"y"<<"y";
    }

    QString output = "";
    for (int i = 0; i < s.length(); i++) {
        QChar c = s[i];
        int dIndex = diacriticLetters_.indexOf(c);
        if (dIndex < 0) {
            output.append(c);
        } else {
            QString replacement = noDiacriticLetters_[dIndex];
            output.append(replacement);
        }
    }

    return output;
}

void general::ShowInGraphicalShell(QWidget *parent, const QString &location)
{
    // Mac, Windows support folder or file.
#if defined(Q_OS_WIN)
    Q_UNUSED(parent);
    /*  This is commented out because I can't find the header to get "Environment".  However, most PCs will be setup correctly.
    const QString explorer = Environment::systemEnvironment().searchInPath(QLatin1String("explorer.exe"));
    if (explorer.isEmpty()) {
        QMessageBox::warning(parent,
                             tr("Launching Windows Explorer failed"),
                             tr("Could not find explorer.exe in path to launch Windows Explorer."));
        return;
    }
    */

    QString param;
    if (!QFileInfo(location).isDir())
        param = QLatin1String("/select,");
    param += QDir::toNativeSeparators(location);
    QProcess::startDetached("explorer.exe", { param });
#elif defined(Q_OS_MAC) && !defined(Q_OS_IOS)
    Q_UNUSED(parent)
    QStringList scriptArgs;
    scriptArgs << QLatin1String("-e")
               << QString::fromLatin1("tell application \"Finder\" to reveal POSIX file \"%1\"")
                                     .arg(location);
    QProcess::execute(QLatin1String("/usr/bin/osascript"), scriptArgs);
    scriptArgs.clear();
    scriptArgs << QLatin1String("-e")
               << QLatin1String("tell application \"Finder\" to activate");
    QProcess::execute("/usr/bin/osascript", scriptArgs);


#else
    Q_UNUSED(parent)
    // we cannot select a file here, because no file browser really supports it...
//    const QFileInfo fileInfo(location);
//    const QString folder = fileInfo.absoluteFilePath();
//    const QString app = Utils::UnixUtils::fileBrowser(Core::ICore::instance()->settings());
//    QProcess browserProc;
//    const QString browserArgs = Utils::UnixUtils::substituteFileBrowserParameters(app, folder);
//    if (debug)
//        qDebug() <<  browserArgs;
//    bool success = browserProc.startDetached(browserArgs);
//    const QString error = QString::fromLocal8Bit(browserProc.readAllStandardError());
//    success = success && error.isEmpty();
//    if (!success)
//        showGraphicalShellError(parent, app, error);

    // until we find a solution to open file browser with database file selected, just opon folder
    const QFileInfo fileInfo(location);
    const QDir folder = fileInfo.absoluteDir();
    QDesktopServices::openUrl(QUrl::fromLocalFile( folder.absolutePath() ));
#endif
}


QIcon general::changeIconColor(QIcon icon, QColor color)
{
    QPixmap pixmap = icon.pixmap(QSize(120,120));
    QPainter painter(&pixmap);
    painter.setCompositionMode(QPainter::CompositionMode_SourceIn);
    painter.fillRect(pixmap.rect(),color);
    painter.end();
    return QIcon(pixmap);
}

void general::changeButtonColors(QList<QToolButton*> items, QColor paletteBackgroundColor)
{
    int bgDelta = (paletteBackgroundColor.red() * 0.299) + (paletteBackgroundColor.green() * 0.587) + (paletteBackgroundColor.blue() * 0.114);

    if (255 - bgDelta > 105) {
        for (int i = 0; i < items.count(); i++) {
            QToolButton *btn = items[i];
            auto px = btn->icon().pixmap(QSize(24, 24));
            auto mask = px.createMaskFromColor(QColor("black"), Qt::MaskOutColor);
            px.fill(QColor("white"));
            px.setMask(mask);
            btn->setIcon(QIcon(px));
        }
    }
}

void general::changeListWidgetItemColor(QList<QListWidgetItem *> items, QColor paletteBackgroundColor)
{
    int bgDelta = (paletteBackgroundColor.red() * 0.299) + (paletteBackgroundColor.green() * 0.587) + (paletteBackgroundColor.blue() * 0.114);

    if (255 - bgDelta > 105) {
        for (int i = 0; i < items.count(); i++) {
            QListWidgetItem *lstItem = items[i];
            auto px = lstItem->icon().pixmap(QSize(24, 24));
            auto mask = px.createMaskFromColor(QColor("black"), Qt::MaskOutColor);
            px.fill(QColor("white"));
            px.setMask(mask);
            lstItem->setIcon(QIcon(px));
        }
    }
}
