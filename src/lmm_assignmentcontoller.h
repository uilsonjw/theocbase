#ifndef LMM_ASSIGNMENTCONTOLLER_H
#define LMM_ASSIGNMENTCONTOLLER_H

#include <QObject>
#include <QGuiApplication>
#include <QScreen>
#include <QCursor>
#include <QDebug>
#include "cpersons.h"
#include "school.h"
#include "lmm_assignment_ex.h"
#include "historytable.h"
#include <QTextDocument>
#include <QTextTable>

class LMM_AssignmentContoller : public QObject
{
    Q_OBJECT
    Q_PROPERTY(LMM_Assignment_ex *assignment READ assignment WRITE setAssignment)
    Q_PROPERTY(schoolStudentStudy *currentStudy READ getActiveStudy)
    Q_PROPERTY(int nextStudy READ nextStudy)
public:
    explicit LMM_AssignmentContoller(QObject *parent = 0);

    LMM_Assignment_ex *assignment() const;
    void setAssignment(LMM_Assignment_ex *a);

    Q_INVOKABLE QPoint getMousePosition() const;
    Q_INVOKABLE QRect getScreenAvailableGeometry() const;
    Q_INVOKABLE QPoint getDialogPosition(int dialogHeight, int dialogWidth, int controlPosX = 0, int controlPosY = 0) const;

    Q_INVOKABLE person *getPublisherById(int id) const;

    Q_INVOKABLE QVariant getStudies();
    schoolStudentStudy *getActiveStudy();
    int nextStudy() const;
    Q_INVOKABLE void saveStudy(bool exerciseCompleted, bool assignmentCompleted, int next_study_no);
    Q_INVOKABLE QString getHistoryTooltip(int userid, int fontsize = QFont().pointSize());

signals:

public slots:
private:
    LMM_Assignment_ex *m_assignment;
    schoolStudentStudy *m_currentstudy;
    schoolStudentStudy *m_nextstudy;
};

#endif // LMM_ASSIGNMENTCONTOLLER_H
