/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2017, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "territoryaddress.h"

TerritoryAddress::TerritoryAddress(QObject *parent)
    : QObject(parent), m_id(0), m_territoryId(-1), m_country(""), m_state(""), m_county(""), m_city(""), m_district(""), m_street(""), m_houseNumber(""), m_postalCode(""), m_wktGeometry(""), m_name(""), m_isDirty(false)
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    m_addressTypeNumber = QVariant(sql->getSetting("territory_default_addresstype", "1")).toInt();
}

TerritoryAddress::TerritoryAddress(const int territoryId, const QString country,
                                   const QString state, const QString county, const QString city,
                                   const QString district, const QString street, const QString houseNumber,
                                   const QString postalCode, const QString wktGeometry, QObject *parent)
    : QObject(parent), m_id(0), m_territoryId(territoryId), m_country(country), m_state(state), m_county(county), m_city(city), m_district(district), m_street(street), m_houseNumber(houseNumber), m_postalCode(postalCode), m_wktGeometry(wktGeometry), m_name(""), m_isDirty(true)
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    m_addressTypeNumber = QVariant(sql->getSetting("territory_default_addresstype", "1")).toInt();
    setLatLon();
}

TerritoryAddress::TerritoryAddress(const int id, const int territoryId, const QString country,
                                   const QString state, const QString county, const QString city,
                                   const QString district, const QString street, const QString houseNumber,
                                   const QString postalCode, const QString wktGeometry, const QString name,
                                   const int addressTypeNumber, QObject *parent)
    : QObject(parent), m_id(id), m_territoryId(territoryId), m_country(country), m_state(state), m_county(county), m_city(city), m_district(district), m_street(street), m_houseNumber(houseNumber), m_postalCode(postalCode), m_wktGeometry(wktGeometry), m_name(name), m_addressTypeNumber(addressTypeNumber), m_isDirty(false)
{
    setLatLon();
}

TerritoryAddress::~TerritoryAddress()
{
}

int TerritoryAddress::id() const
{
    return m_id;
}

int TerritoryAddress::territoryId() const
{
    return m_territoryId;
}

void TerritoryAddress::setTerritoryId(const int value)
{
    if (m_territoryId != value) {
        m_territoryId = value;
        setIsDirty(true);
    }
}

QString TerritoryAddress::country() const
{
    return m_country;
}

void TerritoryAddress::setCountry(const QString value)
{
    if (m_country != value) {
        m_country = value;
        setIsDirty(true);
    }
}

QString TerritoryAddress::state() const
{
    return m_state;
}

void TerritoryAddress::setState(const QString value)
{
    if (m_state != value) {
        m_state = value;
        setIsDirty(true);
    }
}

QString TerritoryAddress::county() const
{
    return m_county;
}

void TerritoryAddress::setCounty(const QString value)
{
    if (m_county != value) {
        m_county = value;
        setIsDirty(true);
    }
}

QString TerritoryAddress::city() const
{
    return m_city;
}

void TerritoryAddress::setCity(const QString value)
{
    if (m_city != value) {
        m_city = value;
        setIsDirty(true);
    }
}

QString TerritoryAddress::district() const
{
    return m_district;
}

void TerritoryAddress::setDistrict(const QString value)
{
    if (m_district != value) {
        m_district = value;
        setIsDirty(true);
    }
}

QString TerritoryAddress::street() const
{
    return m_street;
}

void TerritoryAddress::setStreet(const QString value)
{
    if (m_street != value) {
        m_street = value;
        setIsDirty(true);
    }
}

QString TerritoryAddress::houseNumber() const
{
    return m_houseNumber;
}

void TerritoryAddress::setHouseNumber(const QString value)
{
    if (m_houseNumber != value) {
        m_houseNumber = value;
        setIsDirty(true);
    }
}

QString TerritoryAddress::postalCode() const
{
    return m_postalCode;
}

void TerritoryAddress::setPostalCode(const QString value)
{
    if (m_postalCode != value) {
        m_postalCode = value;
        setIsDirty(true);
    }
}

QString TerritoryAddress::wktGeometry() const
{
    return m_wktGeometry;
}

void TerritoryAddress::setWktGeometry(const QString value)
{
    if (m_wktGeometry != value) {
        m_wktGeometry = value;
        setLatLon();
        setIsDirty(true);
    }
}

void TerritoryAddress::setLatLon()
{
    QRegularExpression regExp("((?:[-+]?\\.\\d+|[-+]?\\d+(?:\\.\\d*)?))\\s*((?:[-+]?\\.\\d+|[-+]?\\d+(?:\\.\\d*)?))");
    QRegularExpressionMatch match = regExp.match(m_wktGeometry);
    if (match.hasMatch()) {
        m_longitude = match.captured(1).toDouble();
        m_latitude = match.captured(2).toDouble();
    }
}

double TerritoryAddress::latitude() const
{
    return m_latitude;
}

double TerritoryAddress::longitude() const
{
    return m_longitude;
}

QString TerritoryAddress::name() const
{
    return m_name;
}

void TerritoryAddress::setName(const QString value)
{
    if (m_name != value) {
        m_name = value;
        setIsDirty(true);
    }
}

int TerritoryAddress::addressTypeNumber() const
{
    return m_addressTypeNumber;
}

void TerritoryAddress::setAddressTypeNumber(const int value)
{
    if (m_addressTypeNumber != value) {
        m_addressTypeNumber = value;
        setIsDirty(true);
    }
}

void TerritoryAddress::setIsDirty(const bool value)
{
    m_isDirty = value;
}

bool TerritoryAddress::save()
{
    if (street() == "")
        return false;

    // save changes to database
    sql_class *sql = &Singleton<sql_class>::Instance();

    int lang_id = sql->getLanguageDefaultId();

    sql_item queryitems;
    queryitems.insert(":id", m_id);
    int addressId = m_id > 0 ? sql->selectScalar("SELECT id FROM territory_address WHERE id = :id", &queryitems, -1).toInt() : -1;

    sql_item insertItems;
    insertItems.insert("territory_id", territoryId());
    insertItems.insert("country", country());
    insertItems.insert("state", state());
    insertItems.insert("county", county());
    insertItems.insert("city", city());
    insertItems.insert("district", district());
    insertItems.insert("street", street());
    insertItems.insert("housenumber", houseNumber());
    insertItems.insert("postalcode", postalCode());
    insertItems.insert("wkt_geometry", wktGeometry());
    insertItems.insert("name", name());
    insertItems.insert("addresstype_number", addressTypeNumber());
    insertItems.insert("lang_id", lang_id);

    bool ret = false;
    if (addressId > 0) {
        // update
        ret = sql->updateSql("territory_address", "id", QString::number(addressId), &insertItems);
    } else {
        // insert new row
        int newId = sql->insertSql("territory_address", &insertItems, "id");
        ret = newId > 0;
        if (newId > 0)
            m_id = newId;
    }
    setIsDirty(!ret);

    return ret;
}

TerritoryAddressModel::TerritoryAddressModel()
    : m_selectedAddress(nullptr)
{
}

TerritoryAddressModel::TerritoryAddressModel(QObject *parent)
    : QAbstractTableModel(parent), m_selectedAddress(nullptr)
{
}

QHash<int, QByteArray> TerritoryAddressModel::roleNames() const
{
    QHash<int, QByteArray> items;
    items[AddressIdRole] = "id";
    items[TerritoryIdRole] = "territoryId";
    items[CountryRole] = "country";
    items[StateRole] = "state";
    items[CountyRole] = "county";
    items[CityRole] = "city";
    items[DistrictRole] = "district";
    items[StreetRole] = "street";
    items[HouseNumberRole] = "houseNumber";
    items[PostalCodeRole] = "postalCode";
    items[WktGeometryRole] = "wktGeometry";
    items[NameRole] = "name";
    items[AddressTypeNumberRole] = "addressTypeNumber";
    items[LatitudeRole] = "latitude";
    items[LongitudeRole] = "longitude";
    return items;
}

int TerritoryAddressModel::rowCount(const QModelIndex & /*parent*/) const
{
    return territoryAddresses.count();
}

int TerritoryAddressModel::columnCount(const QModelIndex & /*parent*/) const
{
    return 13;
}

QVariantMap TerritoryAddressModel::get(int row)
{
    QHash<int, QByteArray> names = roleNames();
    QHashIterator<int, QByteArray> i(names);
    QVariantMap res;
    QModelIndex idx = index(row, 0);
    while (i.hasNext()) {
        i.next();
        QVariant data = idx.data(i.key());
        res[i.value()] = data;
    }
    return res;
}

QVariant TerritoryAddressModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() < 0 || index.row() > territoryAddresses.count())
        return QVariant();

    if (role == Qt::DisplayRole || role == Qt::EditRole) {
        switch (index.column()) {
        case 0:
            return territoryAddresses[index.row()]->id();
        case 1:
            return territoryAddresses[index.row()]->territoryId();
        case 2:
            return territoryAddresses[index.row()]->country();
        case 3:
            return territoryAddresses[index.row()]->state();
        case 4:
            return territoryAddresses[index.row()]->county();
        case 5:
            return territoryAddresses[index.row()]->city();
        case 6:
            return territoryAddresses[index.row()]->district();
        case 7:
            return territoryAddresses[index.row()]->street();
        case 8:
            return territoryAddresses[index.row()]->houseNumber();
        case 9:
            return territoryAddresses[index.row()]->postalCode();
        case 10:
            return territoryAddresses[index.row()]->wktGeometry();
        case 11:
            return territoryAddresses[index.row()]->name();
        case 12:
            return territoryAddresses[index.row()]->addressTypeNumber();
        case 13:
            return territoryAddresses[index.row()]->latitude();
        case 14:
            return territoryAddresses[index.row()]->longitude();
        }
    }

    switch (role) {
    case AddressIdRole:
        return territoryAddresses[index.row()]->id();
    case TerritoryIdRole:
        return territoryAddresses[index.row()]->territoryId();
    case CountryRole:
        return territoryAddresses[index.row()]->country();
    case StateRole:
        return territoryAddresses[index.row()]->state();
    case CountyRole:
        return territoryAddresses[index.row()]->county();
    case CityRole:
        return territoryAddresses[index.row()]->city();
    case DistrictRole:
        return territoryAddresses[index.row()]->district();
    case StreetRole:
        return territoryAddresses[index.row()]->street();
    case HouseNumberRole:
        return territoryAddresses[index.row()]->houseNumber();
    case PostalCodeRole:
        return territoryAddresses[index.row()]->postalCode();
    case WktGeometryRole:
        return territoryAddresses[index.row()]->wktGeometry();
    case NameRole:
        return territoryAddresses[index.row()]->name();
    case AddressTypeNumberRole:
        return territoryAddresses[index.row()]->addressTypeNumber();
    case LatitudeRole:
        return territoryAddresses[index.row()]->latitude();
    case LongitudeRole:
        return territoryAddresses[index.row()]->longitude();
    }

    return QVariant();
}

bool TerritoryAddressModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    TerritoryAddress *currTerritoryAddress = territoryAddresses[index.row()];

    if (role == Qt::EditRole) {
        int column = index.column();

        switch (column) {
        case 0:
            //addressId
            break;
        case 1:
            currTerritoryAddress->setTerritoryId(value.toInt());
            break;
        case 2:
            currTerritoryAddress->setCountry(value.toString());
            break;
        case 3:
            currTerritoryAddress->setState(value.toString());
            break;
        case 4:
            currTerritoryAddress->setCounty(value.toString());
            break;
        case 5:
            currTerritoryAddress->setCity(value.toString());
            break;
        case 6:
            currTerritoryAddress->setDistrict(value.toString());
            break;
        case 7:
            currTerritoryAddress->setStreet(value.toString());
            break;
        case 8:
            currTerritoryAddress->setHouseNumber(value.toString());
            break;
        case 9:
            currTerritoryAddress->setPostalCode(value.toString());
            break;
        case 10:
            currTerritoryAddress->setWktGeometry(value.toString());
            break;
        case 11:
            currTerritoryAddress->setName(value.toString());
            break;
        case 12:
            currTerritoryAddress->setAddressTypeNumber(value.toInt());
            break;
        default:
            break;
        }

        emit editCompleted();
    } else {
        switch (role) {
        case Roles::TerritoryIdRole:
            currTerritoryAddress->setTerritoryId(value.toInt());
            emit dataChanged(this->index(index.row(), 1), this->index(index.row(), 1));
            break;
        case Roles::CountryRole:
            currTerritoryAddress->setCountry(value.toString());
            emit dataChanged(this->index(index.row(), 2), this->index(index.row(), 2));
            break;
        case Roles::StateRole:
            currTerritoryAddress->setState(value.toString());
            emit dataChanged(this->index(index.row(), 3), this->index(index.row(), 3));
            break;
        case Roles::CountyRole:
            currTerritoryAddress->setCounty(value.toString());
            emit dataChanged(this->index(index.row(), 4), this->index(index.row(), 4));
            break;
        case Roles::CityRole:
            currTerritoryAddress->setCity(value.toString());
            emit dataChanged(this->index(index.row(), 5), this->index(index.row(), 5));
            break;
        case Roles::DistrictRole:
            currTerritoryAddress->setDistrict(value.toString());
            emit dataChanged(this->index(index.row(), 6), this->index(index.row(), 6));
            break;
        case Roles::StreetRole:
            currTerritoryAddress->setStreet(value.toString());
            emit dataChanged(this->index(index.row(), 7), this->index(index.row(), 7));
            break;
        case Roles::HouseNumberRole:
            currTerritoryAddress->setHouseNumber(value.toString());
            emit dataChanged(this->index(index.row(), 8), this->index(index.row(), 8));
            break;
        case Roles::PostalCodeRole:
            currTerritoryAddress->setPostalCode(value.toString());
            emit dataChanged(this->index(index.row(), 9), this->index(index.row(), 9));
            break;
        case Roles::WktGeometryRole:
            currTerritoryAddress->setWktGeometry(value.toString());
            emit dataChanged(this->index(index.row(), 10), this->index(index.row(), 10));
            break;
        case Roles::NameRole:
            currTerritoryAddress->setName(value.toString());
            emit dataChanged(this->index(index.row(), 11), this->index(index.row(), 11));
            break;
        case Roles::AddressTypeNumberRole:
            currTerritoryAddress->setAddressTypeNumber(value.toInt());
            emit dataChanged(this->index(index.row(), 12), this->index(index.row(), 12));
            break;
        default:
            break;
        }
    }

    return true;
}

Qt::ItemFlags TerritoryAddressModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;
    return Qt::ItemIsEditable | QAbstractTableModel::flags(index);
}

QModelIndex TerritoryAddressModel::index(int row, int column, const QModelIndex &parent)
        const
{
    if (hasIndex(row, column, parent)) {
        return createIndex(row, column);
    }
    return QModelIndex();
}

QModelIndex TerritoryAddressModel::getAddressIndex(int addressId) const
{
    for (int row = 0; row < this->rowCount(); ++row) {
        QModelIndex rowIndex = this->index(row, 0);
        if (rowIndex.data(AddressIdRole) == addressId)
            return rowIndex;
    }
    return QModelIndex();
}

int TerritoryAddressModel::addAddress(TerritoryAddress *territoryAddress)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    territoryAddresses << territoryAddress;
    endInsertRows();
    return territoryAddress->id();
}

int TerritoryAddressModel::addAddress(int territoryId, const QString country,
                                      const QString state, const QString county, const QString city,
                                      const QString district, const QString street, const QString houseNumber,
                                      const QString postalCode, QString wktGeometry)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    TerritoryAddress *newTerritoryAddress = new TerritoryAddress(territoryId, country, state, county,
                                                                 city, district, street, houseNumber,
                                                                 postalCode, wktGeometry);
    newTerritoryAddress->save();
    territoryAddresses << newTerritoryAddress;
    endInsertRows();
    return newTerritoryAddress->id();
}

void TerritoryAddressModel::removeAddress(int id)
{
    if (id > 0) {
        sql_class *sql = &Singleton<sql_class>::Instance();

        sql_item s;
        s.insert(":id", id);
        s.insert(":active", 0);
        if (sql->execSql("UPDATE territory_address SET active = :active WHERE id = :id", &s, true)) {
            QModelIndex addressIndex = getAddressIndex(id);
            if (addressIndex.isValid()) {
                int row = addressIndex.row();
                beginRemoveRows(QModelIndex(), row, row);
                territoryAddresses.erase(std::next(territoryAddresses.begin(), row));
                endRemoveRows();
            }
        }
    }
}

bool TerritoryAddressModel::removeRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent)
    if (row < 0 || count < 1 || (row + count) > territoryAddresses.size())
        return false;
    beginRemoveRows(QModelIndex(), row, row + count - 1);
    for (int i = 0; i < count; i++) {
        territoryAddresses.removeAt(row);
    }
    endRemoveRows();
    return true;
}

void TerritoryAddressModel::loadAddresses(int territoryId)
{
    removeRows(0, territoryAddresses.count());
    sql_class *sql = &Singleton<sql_class>::Instance();

    QString sqlQuery("SELECT * FROM territory_address WHERE ");
    if (territoryId != 0)
        sqlQuery.append("territory_id = " + QVariant(territoryId).toString() + " AND ");
    sqlQuery.append("active ORDER BY street, housenumber");
    sql_items territoryAddressRows = sql->selectSql(sqlQuery);

    if (!territoryAddressRows.empty()) {
        for (unsigned int i = 0; i < territoryAddressRows.size(); i++) {
            sql_item s = territoryAddressRows[i];

            addAddress(new TerritoryAddress(s.value("id").toInt(),
                                            s.value("territory_id").toInt(),
                                            s.value("country").toString(),
                                            s.value("state").toString(),
                                            s.value("county").toString(),
                                            s.value("city").toString(),
                                            s.value("district").toString(),
                                            s.value("street").toString(),
                                            s.value("housenumber").toString(),
                                            s.value("postalcode").toString(),
                                            s.value("wkt_geometry").toString(),
                                            s.value("name").toString(),
                                            s.value("addresstype_number").toInt()));
        }
    }
}

void TerritoryAddressModel::saveAddresses()
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    sql->startTransaction();
    for (int i = 0; i < territoryAddresses.count(); i++) {
        TerritoryAddress *t = territoryAddresses[i];
        if (t->isDirty()) {
            t->save();
        }
    }
    sql->commitTransaction();
}

bool TerritoryAddressModel::updateAddress(int addressId, QString wktGeometry)
{
    QModelIndex rowIndex = getAddressIndex(addressId);
    if (rowIndex.isValid()) {
        setData(rowIndex, wktGeometry, WktGeometryRole);
        return true;
    }
    return false;
}

bool TerritoryAddressModel::updateAddress(int addressId, int territoryId)
{
    QModelIndex rowIndex = getAddressIndex(addressId);
    if (rowIndex.isValid()) {
        setData(rowIndex, territoryId, TerritoryIdRole);
        return true;
    }
    return false;
}

bool TerritoryAddressModel::updateAddress(int addressId, int territoryId, const QString country,
                                          const QString state, const QString county,
                                          const QString city, const QString district,
                                          const QString street, const QString houseNumber,
                                          const QString postalCode, QString wktGeometry)
{
    QModelIndex rowIndex = getAddressIndex(addressId);
    if (rowIndex.isValid()) {
        setData(rowIndex, territoryId, TerritoryIdRole);
        setData(rowIndex, country, CountryRole);
        setData(rowIndex, state, StateRole);
        setData(rowIndex, county, CountyRole);
        setData(rowIndex, city, CityRole);
        setData(rowIndex, district, DistrictRole);
        setData(rowIndex, street, StreetRole);
        setData(rowIndex, houseNumber, HouseNumberRole);
        setData(rowIndex, postalCode, PostalCodeRole);
        setData(rowIndex, wktGeometry, WktGeometryRole);
        return true;
    }
    return false;
}

void TerritoryAddressModel::setSelectedAddress(const QModelIndex &selectedIndex)
{
    if (selectedIndex.isValid()) {
        if (m_selectedAddress != nullptr) {
            if (m_selectedAddress->isDirty()) {
                // save current address
                if (m_selectedAddress->save()) {
                    // address saved
                } else {
                    // address not saved
                }
            }
        }
        m_selectedAddress = getItem(selectedIndex);
    } else {
        m_selectedAddress = nullptr;
    }
    emit selectedChanged(selectedIndex);
}

double TerritoryAddressModel::getMarkerScale()
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    return QVariant(sql->getSetting("territory_map_markerscale", "0.5")).toDouble();
}

int TerritoryAddressModel::getDefaultAddressTypeNumber()
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    return QVariant(sql->getSetting("territory_default_addresstype", "1")).toInt();
}

TerritoryAddress *TerritoryAddressModel::getItem(const QModelIndex &index) const
{
    if (index.isValid()) {
        TerritoryAddress *item = territoryAddresses[index.row()];
        if (item)
            return item;
    }
    return nullptr;
}

TerritoryAddressSortFilterProxyModel::TerritoryAddressSortFilterProxyModel(QObject *parent)
    : QSortFilterProxyModel(parent)
{
}

void TerritoryAddressSortFilterProxyModel::setFilterTerritoryId(int value)
{
    m_territoryId = value;
    invalidateFilter();
}

bool TerritoryAddressSortFilterProxyModel::filterAcceptsRow(int sourceRow,
                                                            const QModelIndex &sourceParent) const
{
    QModelIndex indexTerritoryId = sourceModel()->index(sourceRow, 1, sourceParent);
    int territoryId = sourceModel()->data(indexTerritoryId).toInt();
    return (territoryId == m_territoryId);
}

bool TerritoryAddressSortFilterProxyModel::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
    QVariant leftData = sourceModel()->data(left);
    QVariant rightData = sourceModel()->data(right);

    return leftData.toString() < rightData.toString();
}
