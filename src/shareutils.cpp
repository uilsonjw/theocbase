/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "shareutils.h"

#ifdef Q_OS_MAC
#include "macos/macshareutils.h"
#endif

#ifdef Q_OS_WIN
#include "windows/windowsshareutils.h"
#endif

ShareUtils::ShareUtils(QObject *parent)
    : QObject(parent)
{
#if defined(Q_OS_MAC)
    mPShareUtils = new MacShareUtils(this);
#elif defined(Q_OS_WIN)
    auto version = QOperatingSystemVersion::current();
    if (version >= QOperatingSystemVersion::Windows10)
        mPShareUtils = new WindowsShareUtils(this);
    else
        mPShareUtils = new PlatformShareUtils(this);
#else
    mPShareUtils = new PlatformShareUtils(this);
#endif
}

void ShareUtils::copyToClipboard(const QString shareText)
{
    QClipboard *clipboard = QApplication::clipboard();
    clipboard->setText(shareText);
}

void ShareUtils::share(const QString shareText, const QString email, QPoint pos)
{
    mPShareUtils->share(shareText, email, pos);
}
