#ifndef LMM_SCHEDULE_H
#define LMM_SCHEDULE_H

#include <QObject>
#include <QDate>
#include <QDebug>
#include "sql_class.h"
#include "sharedlib_global.h"

class TB_EXPORT LMM_Schedule : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int talkId READ talkId CONSTANT)
    Q_PROPERTY(int sequence READ sequence CONSTANT)
    Q_PROPERTY(QDate date READ date WRITE setDate NOTIFY dateChanged)
    Q_PROPERTY(int time READ time WRITE setTime NOTIFY timeChanged)
    Q_PROPERTY(QString theme READ theme WRITE setTheme NOTIFY themeChanged)
    Q_PROPERTY(QString source READ source WRITE setSource NOTIFY sourceChanged)
    Q_PROPERTY(bool canMultiSchool READ canMultiSchool CONSTANT)
    Q_PROPERTY(bool canCounsel READ canCounsel CONSTANT)
    Q_PROPERTY(bool canHaveAssistant READ canHaveAssistant CONSTANT)
    Q_PROPERTY(int studyNumber READ study_number CONSTANT)
    Q_PROPERTY(QString studyName READ study_name CONSTANT)

public:
    enum TalkTypes { //!talkid verify
        TalkType_Chairman = 1,
        TalkType_Treasures = 2,
        TalkType_Digging = 3,
        TalkType_BibleReading = 4,
        TalkType_SampleConversationVideo = 5,
        TalkType_InitialCall = 6,
        TalkType_ReturnVisit1 = 7,
        TalkType_ReturnVisit2 = 8,
        TalkType_ReturnVisit3 = 9,
        TalkType_BibleStudy = 10,
        TalkType_StudentTalk = 11,
        TalkType_LivingTalk1 = 12,
        TalkType_LivingTalk2 = 13,
        TalkType_LivingTalk3 = 14,
        TalkType_CBS = 15,
        TalkType_COTalk = 16,
        TalkType_ApplyYourself = 17, // talk going over the new teaching brochure
        TalkType_MemorialInvitation = 27,
        TalkType_OtherFMVideoPart = 28
    };
    Q_ENUM(TalkTypes)

    LMM_Schedule(QObject *parent = nullptr);
    LMM_Schedule(int talkId, int sequence, QDate date, QString theme = "", QString source = "", int study_number = -1, int time = -1, int dbId = -1, QObject *parent = nullptr); //!talkid verify
    int talkId() const;
    int dbTalkId() const;
    int sequence() const;
    int scheduleId() const;
    static int dbTalkId(int talkId, int sequence);
    static void splitDbTalkId(int dbTalkId, int &talkId, int &sequence);
    static QString getStringTalkType(int src);
    static QString getFullStringTalkType(int src);
    static int getTalkTypeFromFullString(QString name);
    static QList<int> getExpectedTalks(QDate d);
    static void RemoveDuplicates();

    bool loadSchedule(QDate date, int dbIalkId); //!talkid verify

    QDate date() const;
    void setDate(QDate date);

    int time() const;
    void setTime(int time);

    QString theme() const;
    void setTheme(QString theme);

    QString source() const;
    void setSource(QString source);

    int study_number() const;
    void setStudy_number(int study_number);
    QString study_name() const;

    bool canMultiSchool() const;
    static bool canMultiSchool(int talkId);
    bool canCounsel() const;
    bool canHaveAssistant() const;

    QString talkName() const;

    int roworder() const;
    void setRoworder(int roworder);

    Q_INVOKABLE virtual bool save();

protected:
    int m_talkid;
    int m_sequence;
    int m_schedule_db_id;

signals:
    void dateChanged(QDate date);
    void timeChanged(int time);
    void themeChanged(QString theme);
    void sourceChanged(QString source);

private:
    int m_time;
    QDate m_date;
    QString m_theme;
    QString m_source;
    int m_study_number;
    int m_roworder;

    sql_class *sql;
};

#endif // LMM_SCHEDULE_H
