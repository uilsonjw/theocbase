import QtQuick 2.11
import QtQuick.Controls 2.4
import QtWebView 1.1

Page {
    width: 480
    height: 640
    property alias checkBox: checkBox
    property alias webView: webView

    header: Label {
        rightPadding: 56
        leftPadding: 56
        text: qsTr("Welcome to theocbase")
        topPadding: 66
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    WebView {
        id: webView
        anchors.rightMargin: 1
        anchors.leftMargin: 1
        anchors.bottomMargin: 1
        anchors.topMargin: 1
        anchors.fill: parent

        BusyIndicator {
            id: busyIndicator
            running: webView.loading
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
        }
    }

    footer: CheckBox {
        id: checkBox
        text: qsTr("Do not show again")
    }
}
