import QtQuick 2.3
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.2
import net.theocbase.mobile 1.0

Rectangle {
    width: parent.width
    height: visible ? rowHeight : 0

    property alias icontext: rectangeText.text
    property alias maintext: toptext.text
    property alias name1: nametext1.text
    property alias name2: nametext2.text
    property alias iconcolor: rectangeBox.color
    property bool done: false
    property bool selectable: true

    signal rowclicked(var index)

    color: listComponentMouseArea.pressed ? "#D9D9D9" : "white"

    RowLayout {
        id: row1
        anchors.fill: parent
        anchors.margins: 5*dpi
        spacing: 10

        Rectangle {
            id: rectangeBox
            width: 50*dpi
            height: 50*dpi
            color: "#618938"
            Text {
                id: rectangeText
                anchors.fill: parent
                anchors.margins: 5*dpi
                color: "white"
                width: parent.width
                elide: Text.ElideRight
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: rectangeBox.height/2
                renderType: Text.QtRendering
            }
        }
        ColumnLayout {
            Text {
                id: toptext
                Layout.fillWidth: true
                font.bold: true
                font.pointSize: app_info.fontsizeSmall
                elide: Text.ElideRight
                renderType: Text.QtRendering
            }
            RowLayout {
                spacing: 0// 10*dpi
                Item {
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                }
                Text {
                    Layout.fillHeight: true
                    id: nametext1
                    verticalAlignment: Text.AlignBottom
                    font.pointSize: app_info.fontsizeSmall
                    renderType: Text.QtRendering
                }
                Text {
                    id: nametext2
                    Layout.fillHeight: true
                    anchors.leftMargin: 10*dpi
                    verticalAlignment: Text.AlignBottom
                    font.pointSize: app_info.fontsizeSmall
                    color: "grey"
                    elide: Text.ElideRight
                    renderType: Text.QtRendering
                }
            }
        }
    }
    Image {
        source: done ? "qrc:///checkmark_outline_green.png" : "qrc:///chevron_right.svg"
        anchors.right: parent.right
        anchors.rightMargin: 5*dpi
        width: 20*dpi
        height: width
        anchors.verticalCenter: parent.verticalCenter
        visible: selectable
    }
    Rectangle {
        width: parent.width
        anchors.bottom: parent.top
        anchors.topMargin: 0
        height: app_info.linewidth
        color: "lightgrey"
    }
    Rectangle {
        width: parent.width
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        height: app_info.linewidth
        color: "lightgrey"
    }
    MouseArea {
        id: listComponentMouseArea
        anchors.fill: parent
        enabled: selectable
        onClicked: { rowclicked(index) }
    }
}

