<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="my-MM">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation type="unfinished">နာမည်</translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>နောက်သွန်သင်ချက်အတွက် တာဝန် မပေးပါနဲ့</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>လက်ရှိ သွန်သင်ချက်ကို ဆက်ထားပါ</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>အချက်အလက် ထည့်လို့မရ</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>အချိန်အကန့်အသတ်ထည့်မလား?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>အကြောင်းအရာ</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>ရင်းမြစ်</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>ကျောင်းသား</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>အကူ</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>ရလဒ်</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>ပြီးသွားပြီ</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>စေတနာ့ဝန်ထမ်း</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>စေတနာ့ဝန်ထမ်းတဦးရွေးပါ</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>လက်ရှိ သွန်သင်ချက်</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>ပြီးသွားတဲ့ လေ့ကျင့်ခန်း</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>နောက်သွန်သင်ချက်</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>နောက်သွန်သင်ချက် ရွေးချယ်ပါ</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>အချိန် ပိုင်းခြားမှု</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>မှတ်ချက်</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>ကျောင်းအချက်အလက်</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>အချိန်စမှတ်ပါ</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>အချိန်မှတ်ထားတာ ရပ်ပါ</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>ဇာတ်အိမ်</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>ဇာတ်အိမ် ရွေးချယ်ပါ</translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>No exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit overseer&apos;s visit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit assembly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Regional convention</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Memorial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Other exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation type="unfinished">%1 စမယ့်အပတ်</translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished">ကြားရက်စည်းဝေး</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished">တနင်္ဂနွေစည်းဝေး</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>အကြောင်းအရာ</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>ရင်းမြစ်</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>သင်တန်းကိုင်သူ</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>ဟောပြောသူ</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>စာဖတ်သူ</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>မှတ်ချက်</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>အသေးစိတ်</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <comment>Page title</comment>
        <translation>မှတ်စုများ</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&apos;S WORD</source>
        <translation>ကျမ်းစာထဲက အဖိုးတန်ဘဏ္ဍာများ</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>အမှုဆောင်လုပ်ငန်းမှာ ကျင့်သုံးပါ</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>ခရစ်ယာန်အသက်တာလမ်းစဥ်</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>သဘာပတိ</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>သွန်သင်ချက်ပေးသူ</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>သီချင်း %1 နဲ့ ဆုတောင်းချက်</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>သီချင်း</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>သင်တန်းကိုင်သူ</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>စာဖတ်သူ</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>အဖွင့် နိဒါန်း</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>ဆုတောင်းချက်</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation>အစီအစဥ်ဇယား တင်သွင်းမယ်...</translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished">ကြားရက်စည်းဝေး</translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>လက်ရှိ သွန်သင်ချက်ကို ဆက်ထားပါ</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>အချိန်စမှတ်ပါ</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>အချိန်မှတ်ထားတာ ရပ်ပါ</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>အချိန် အကန့်အသတ် ထည့်မလား?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>အကြောင်းအရာ</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>ရင်းမြစ်</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>ကျောင်းသား</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>ရလဒ်</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>ပြီးသွားပြီ</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>အချိန်</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>လက်ရှိ သွန်သင်ချက်</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>ပြီးခဲ့တဲ့ လေ့ကျင့်ခန်း</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>နောက်သွန်သင်ချက်</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>နောက်သွန်သင်ချက် ရွေးချယ်ပါ</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>မှတ်ချက်</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>အကူ</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>စေတနာ့ဝန်ထမ်း</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>အချိန်မှတ်</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>အကူက ကျောင်းသားနဲ့ ဆန့်ကျင်ဘက် လိင် မဖြစ်သင့်ဘူး။</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>သွန်သင်ချက်</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>အသေးစိတ်</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>မျက်တောင်ခတ်ဖို့ အောက်ကိုဆွဲချပါ</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>မျက်တောင်ခတ်ဖို့ လွှတ်လိုက်ပါ</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation>အသုံးပြုသူနာမည်</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>လျှို့ဝှက်စာသား</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>အဝင်</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>အသုံးပြုသူနာမည်/အီးမေးလ်</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>အီးမေးလ်</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>အကောင့်အသစ်ဖွင့်မယ်</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>လျှို့ဝှက်စာသား ပြန်ပြင်မယ်</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>အီးမေးလ်လိပ်စာ မတွေ့ပါ!</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>လျှို့ဝှက်စာသား မေ့သွားတယ်</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>အဝင် စာမျက်နှာ</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation>%1 စမယ့်အပတ်</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation>သွားလည် ဟောပြောသူများ</translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>%1 ဟောပြောသူတွေ ဒီအပတ်ထဲ မရှိဘူး</numerusform>
        </translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation>ဟောပြောသူ အားလုံး ဒီအပတ်ထဲ ရှိတယ်</translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation type="unfinished">အချိန်ဇယား</translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished">ကြားရက်စည်းဝေး</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished">တနင်္ဂနွေစည်းဝေး</translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Text size</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>သီချင်: နဲ့ ဆုတောင်းချက်</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>သီချင်း %1 နဲ့ ဆုတောင်းချက်</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>လူထုဟောပြောချက်</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>ကင်းမျှော်စင်သင်တန်း</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>သီချင်း %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>သင်တန်းကိုင်</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>စာဖတ်သူ</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation>WT တင်သွင်းမယ်...</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished">တနင်္ဂနွေစည်းဝေး</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished">ကြားရက်စည်းဝေး</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>အဓိကအချက်</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>အသင်းတော်</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>ဟောပြောသူ</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>မိုဘိုင်းဖုန်း</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>ဖုန်း</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>အီးမေးလ်</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>အချက်အလက်</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>အိမ်ရှင်</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>လူထုဟောပြောချက်</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>ရှေ့နာမည်</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>နောက်နာမည်</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>ညီအစ်ကို</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>ညီအစ်မ</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>အမှုထမ်း</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>မိသားစု</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>မိသားစုဝင်</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>ဆက်သွယ်ဖို့ အချက်အလက်</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>ဖုန်း</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>အီးမေးလ်</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>သင်တန်းအားလုံး</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>ပင်မသင်တန်းသာ</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>အရန်သင်တန်းသာ</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>အိမ်ထောင်ဦးစီး</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>အကူ</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>အချက်အလက်</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>လှုပ်ရှား</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>သဘာပတိ</translation>
    </message>
    <message>
        <source>Treasures from God&apos;s Word</source>
        <translation>ကျမ်းစာထဲက အဖိုးတန်ဘဏ္ဍာများ</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>ကျမ်းစာဖတ်ရှုခြင်း</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>ဦးဆုံးတွေ့ဆုံချိန်</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>ပြန်လည်ပတ်မှု</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>ကျမ်းစာသင်အံမှု</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>ခရစ်ယာန်အသက်တာလမ်းစဥ် ဟောပြောချက်</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>အသင်းတော်ကျမ်းစာလေ့လာမှု</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>အသင်းတော်ကျမ်းစာလေ့လာမှု စာဖတ်သူ</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>ကြေညာသူ အသစ်</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>ဆုတောင်းချက်</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation>လူထု ဟောပြောသူတွေအတွက် အိမ်ရှင်</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>မိုဘိုင်းဖုန်း</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>ကြားရက်စည်းဝေး</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>ဟောပြောချက်</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>တနင်္ဂနွေစည်းဝေး</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>ကင်းမျှော်စင်လေ့လာမှု သင်တန်းကိုင်</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation>စာဖတ်သူ</translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>ကြေညာသူ</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>ချိန်ညှိစရာ</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>ထွက်မယ်</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>အချက်အလက်</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>TheocBase ပင်မစာမျက်နှာ</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>အကြံပေးစာ</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>နောက်ဆုံး တပြေးညီ ချိန်ညှိခဲ့ %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>အချိန်ဇယား</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>အချိန် ပြပါ</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>ကြာချိန် ပြပါ</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>User Interface</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>ဘာသာစကား</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>အဝင်</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>အီးမေးလ်</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>နာမည်</translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Printing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>တပြေးညီ ချိန်ညှိနေ...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>သဘာပတိ</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>သီချင်း</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>သီချင်း</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation>ကင်းမျှော်စင် သီချင်း</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation type="unfinished">ဆုတောင်းချက်</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation>ကင်းမျှော်စင် လထုတ်</translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>ဆောင်းပါး</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>အဓိကအချက်</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>သင်တန်းမှူး</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>စာဖတ်သူ</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>အချိန်ဇယား</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>ရွေးထားတဲ့ ရက်စွဲ မတိုင်ခင် အပတ်အရေအတွက်</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>ရွေးထားတဲ့ ရက်စွဲ နောက် အပတ်အရေအတွက်</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>အပတ်တွေ</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation>တာဝန်တစ်ခုပြီးနောက် မီးခိုးရောင်ပြထားရမယ့် အပတ်အရေအတွက်</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>တူတဲ့ အပြောင်းအလဲတွေကို ဒီစက်ထဲမှာရော ကလောက်စတိုးရေ့ခ်ျ ထဲမှာပါ တွေ့ရတယ် (%1 rows). ဒီစက်ထဲက အပြောင်းအလဲတွေကို ဆက်ထားမလား?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>အွန်လိုင်းဒေတာ ပြင်လိုက်ပြီ။ သင့်ကွန်ပျူတာမှာရှိတဲ့ ဒေတာကို အစားထိုးတော့မယ်။ ဆက်လုပ်မလား?</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>နောက်သွန်သင်ချက်အတွက် တာဝန် မပေးပါနဲ့</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>မချိန်ရသေး</translation>
    </message>
</context>
</TS>
