/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "qmltranslator.h"

QmlTranslator::QmlTranslator(QObject *parent)
    : QObject(parent)
{
}

QmlTranslator::~QmlTranslator()
{
    mLanguages.clear();
}

void QmlTranslator::initTranslator()
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    mLanguageId = sql->getLanguageDefaultId();
    mLanguages = sql->getLanguages();

    QStringList uilanguages;

    QString selectedLang = "";
    if (sql->getSetting("theocbase_language", "") == "") {
        uilanguages = QLocale::system().uiLanguages();
    } else {
        uilanguages.append(sql->getSetting("theocbase_language", ""));
    }
    for (QString l : qAsConst(uilanguages)) {
        if (!l.startsWith(selectedLang))
            break;
        if (mTranslatorMobile.load("theocbase_mobile_" + l.replace("-", "_"), ":/translations/", "_", ".qm")) {
            qApp->installTranslator(&mTranslatorMobile);
            // install translations of desktop version
            if (mTranslatorDesktop.load("theocbase_" + l.replace("-", "_"), ":/translations/", "_", ".qm"))
                qApp->installTranslator(&mTranslatorDesktop);

            selectedLang = l.left(2);
            if (l.length() == 5)
                break;
        }
    }

    QLocale langLocale(selectedLang);
    if (QLocale::system().name().startsWith(selectedLang) || langLocale == QLocale::C)
        QLocale::setDefault(QLocale::system());
    else
        QLocale::setDefault(langLocale);

    // save language to database
    if (sql->getSetting("theocbase_language", "") == "")
        sql->saveSetting("theocbase_language", selectedLang);
}

void QmlTranslator::setTranslation(QString langCode)
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    if (mTranslatorMobile.load("theocbase_mobile_" + langCode.replace("-", "_"), ":/translations/", "_", ".qm")) {
        qApp->removeTranslator(&mTranslatorMobile);
        qApp->installTranslator(&mTranslatorMobile);
        if (mTranslatorDesktop.load("theocbase_" + langCode.replace("-", "_"), ":/translations/", "_", ".qm")) {
            qApp->removeTranslator(&mTranslatorDesktop);
            qApp->installTranslator(&mTranslatorDesktop);
        }
        mLanguageId = sql->getLanguageId(langCode);
        sql->saveSetting("theocbase_language", langCode);

        QLocale langLocale(langCode);
        if (QLocale::system().name().startsWith(langCode.split("_").first()) || langLocale == QLocale::C)
            QLocale::setDefault(QLocale::system());
        else
            QLocale::setDefault(langLocale);

        emit languageChanged();
    }
}

int QmlTranslator::getCurrentIndex()
{
    int index = 0;
    for (int i = 0; i < mLanguages.size(); ++i) {
        if (mLanguages[i].first == mLanguageId)
            index = i;
    }
    return index;
}

void QmlTranslator::setCurrentIndex(int index)
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    setTranslation(sql->getLanguageCode(mLanguages[index].first));
}

QStringList QmlTranslator::getLanguages()
{
    QStringList list;
    for (const QPair<int, QString> &p : qAsConst(mLanguages))
        list.append(p.second);
    return list;
}

QString QmlTranslator::getLanguageName()
{
    for (const QPair<int, QString> &p : qAsConst(mLanguages))
        if (p.first == mLanguageId)
            return p.second;
    return "";
}

QString QmlTranslator::emptyString() const
{
    return "";
}
