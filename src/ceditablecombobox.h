/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef cEditableComboBox_H
#define cEditableComboBox_H

#include <QComboBox>
#include <QElapsedTimer>
#include <QDebug>
#include <QSignalMapper>
// to be used when the combobox is editable. Not especially useful if it is just a normal dropdown-type.
class cEditableComboBox : public QComboBox
{
    Q_OBJECT
    //Q_PROPERTY(QString id READ id WRITE setId)

public:
    enum states {
        idle,
        popup,
        filtered,
        leaving
    };

    explicit cEditableComboBox(QWidget *parent = 0);
    virtual void hidePopup();
    virtual void showPopup();
    void setCurrentText(QString value);

    bool getAutoAddNewDuringSet() const;
    void setAutoAddNewDuringSet(const bool value);

    states getState() const;
    void setState(const states &value);

    bool okIgnore();
    void resetIgnoreTimer();

protected:
    void focusInEvent(QFocusEvent *e);
    void focusOutEvent(QFocusEvent *e);
    states state;
    QElapsedTimer ignoreTimer;
    bool _autoAddNewDuringSet;QSignalMapper p;

signals:
    void beforeShowPopup(QString objectname, cEditableComboBox *object, bool filterRequested);
    void popupClosed(cEditableComboBox *object);
    void focusIn(cEditableComboBox *object);
    void focusOut(cEditableComboBox *object, const cEditableComboBox::states state);

private slots:
    void _activated(int index);
    void _editTextChanged(const QString &arg1);

};

#endif // cEditableComboBox_H
