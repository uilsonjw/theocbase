﻿/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "settings.h"
#include "ui_settings.h"
#include <QCryptographicHash>
Settings::Settings(QWidget *parent)
    : QDialog(parent), ui(new Ui::Settings)
{
    sql = &Singleton<sql_class>::Instance();
    ac = &Singleton<AccessControl>::Instance();
    ui->setupUi(this);

    // Use white icon in navigation list if dark mode
    QList<QListWidgetItem*> items;
    for (int i = 0; i < ui->listWidget->count(); i++)
        items.append(ui->listWidget->item(i));
    general::changeListWidgetItemColor(items, this->palette().window().color());

    ui->widgetLMMTalkTypes->setWindowFlags(Qt::Widget);

    ui->listWidget->setMinimumWidth(ui->listWidget->sizeHintForColumn(0));

    ui->stackedWidget->setCurrentIndex(0);

    ui->colorPaletteGroupBox->setHidden(true);

    QHeaderView *congSpeakersHeaderView = ui->tableWidget->horizontalHeader();
    connect(congSpeakersHeaderView, SIGNAL(sectionClicked(int)), this, SLOT(on_tableWidgetSectionClicked(int)));

    saveGeneralPage = false;
    saveExceptionPage = false;
    saveLMMPage = false;
    savePublicTalkPage = false;
    saveSongPage = false;
    saveTerritoryPage = false;
    savePrintPage = false;
    saveRemindersPage = false;
    saveAccessControlPage = false;
    applyAuthorizationRules();
    showGeneralPage();
}

Settings::~Settings()
{
}

void Settings::setCloud(cloud_controller *c)
{
    this->cloud = c;
    connect(cloud, &cloud_controller::syncFinished, this, &Settings::reloadSettings);
    connect(cloud, &cloud_controller::loggedChanged, this, [=](bool logged) {
        Q_UNUSED(logged);
        // logged changed
        ui->listWidget->setCurrentRow(0);
        ui->stackedWidget->setCurrentIndex(0);
        saveAccessControlPage = false;
        this->applyAuthorizationRules();
    });
}

void Settings::closeEvent(QCloseEvent *event)
{
    on_buttonCloseDialog_clicked();
    event->accept();
}

void Settings::applyAuthorizationRules()
{
    // navigation list
    for (int i = 0; i < ui->listWidget->count(); i++) {
        bool hideItem = false;
        switch (i) {
        case 0:
            // general
            break;
        case 1:
            // exceptions
            if (!ac->user()->hasPermission(Permission::Rule::CanViewSpecialEvents))
                hideItem = true;
            break;
        case 2:
            // lmm
            if (!ac->user()->hasPermission(Permission::Rule::CanViewMidweekMeetingSettings))
                hideItem = true;
            break;
        case 3:
            if (!ac->user()->hasPermission(Permission::Rule::CanViewPublicTalkList))
                hideItem = true;
            break;
        case 4:
            if (!ac->user()->hasPermission(Permission::Rule::CanViewSongList))
                hideItem = true;
            break;
        case 5:
            if (!ac->user()->hasPermission(Permission::Rule::CanViewTerritorySettings))
                hideItem = true;
            break;
        case 6:
            // reminders
            if (!ac->user()->hasPermission(Permission::Rule::CanSendMidweekMeetingReminders))
                hideItem = true;
            break;
        case 7:
            // access control
            if (!ac->user()->hasPermission(Permission::Rule::CanViewPermissions) || !ac->isActive())
                hideItem = true;
            break;
        }
        QListWidgetItem *item = ui->listWidget->item(i);
        item->setHidden(hideItem);
    }

    // congregation settings
    if (!ac->user()->hasPermission(Permission::Rule::CanViewCongregationSettings)) {
        ui->label_42->setHidden(true);
        ui->line->setHidden(true);
        ui->widget_9->setHidden(true);
    } else if (!ac->user()->hasPermission(Permission::Rule::CanEditCongregationSettings))
        ui->widget_9->setEnabled(false);

    // exceptions
    if (!ac->user()->hasPermission(Permission::Rule::CanEditSpecialEvents))
        ui->exceptionsEditWidget->setHidden(true);

    // life and ministry meeting
    if (!ac->user()->hasPermission(Permission::Rule::CanEditMidweekMeetingSettings)) {
        ui->spinBoxClassCount->setEnabled(false);
        ui->btnImportLMM->setEnabled(false);
        ui->btnRemoveDuplicatesLMM->setEnabled(false);
        ui->widgetLMMTalkTypes->setEnabled(false);
        ui->gridLMMMeeting->setEditTriggers(QAbstractItemView::NoEditTriggers);
        ui->btnAddLMMMeeting->setHidden(true);
        ui->btnRemoveLMMMeeting->setHidden(true);
        ui->gridLMMSchedule->setEditTriggers(QAbstractItemView::NoEditTriggers);
        ui->btnAddLMMSchedule->setHidden(true);
        ui->btnRemoveLMMSchedule->setHidden(true);
        ui->tableWidget_SchoolStudyPoints->setEditTriggers(QAbstractItemView::NoEditTriggers);
        ui->buttonAddStudies->setHidden(true);
        ui->buttonRemoveStudies->setHidden(true);
    }

    // public talks
    if (!ac->user()->hasPermission(Permission::Rule::CanEditPublicTalkList)) {
        ui->tableViewPublicTalks->setEditTriggers(QAbstractItemView::NoEditTriggers);
        ui->buttonAddPublicTalks->setHidden(true);
        ui->toolButtonRemovePublicTalk->setHidden(true);
        ui->checkBoxHideDiscontinued->setHidden(true);
        ui->tabWidget_2->removeTab(2);
        ui->tabWidget_2->removeTab(1);
    }

    // hospitality
    if (!ac->user()->hasPermission(Permission::Rule::CanScheduleHospitality)) {
        ui->checkBoxScheduleHospitality->setHidden(true);
    }

    // songs
    if (!ac->user()->hasPermission(Permission::Rule::CanEditSongList)) {
        ui->tableWidgetSongs->setEditTriggers(QAbstractItemView::NoEditTriggers);
        ui->toolButtonAddSongs->setHidden(true);
        ui->toolButtonRemoveSong->setHidden(true);
        ui->tabWidgetSongs->removeTab(1);
    }

    // territories
    if (!ac->user()->hasPermission(Permission::Rule::CanEditTerritorySettings)) {
        ui->tableWidgetCities->setEditTriggers(QAbstractItemView::NoEditTriggers);
        ui->lineEditNewCity->setHidden(true);
        ui->toolButtonAddCity->setHidden(true);
        ui->toolButtonRemoveCity->setHidden(true);
        ui->tableWidgetTerritoryTypes->setEditTriggers(QAbstractItemView::NoEditTriggers);
        ui->lineEditNewTerritoryType->setHidden(true);
        ui->toolButtonAddTerritoryType->setHidden(true);
        ui->toolButtonRemoveTerritoryType->setHidden(true);
        ui->tableWidgetStreetTypes->setEditTriggers(QAbstractItemView::NoEditTriggers);
        ui->labelNewStreetTypeName->setHidden(true);
        ui->lineEditNewStreetTypeName->setHidden(true);
        ui->labelNewStreetTypeColor->setHidden(true);
        ui->pushButtonNewStreetTypeColor->setHidden(true);
        ui->toolButtonAddStreetType->setHidden(true);
        ui->toolButtonRemoveStreetType->setHidden(true);
        ui->tableWidgetAddressTypes->setEditTriggers(QAbstractItemView::NoEditTriggers);
        ui->labelNewAddressTypeNumber->setHidden(true);
        ui->lineEditNewAddressTypeNumber->setHidden(true);
        ui->labelNewAddressTypeName->setHidden(true);
        ui->lineEditNewAddressTypeName->setHidden(true);
        ui->labelNewAddressTypeColor->setHidden(true);
        ui->pushButtonNewAddressTypeColor->setHidden(true);
        ui->toolButtonAddAddressType->setHidden(true);
        ui->toolButtonRemoveAddressType->setHidden(true);
    }
    if (!ac->user()->hasPermission(Permission::Rule::CanEditTerritories)) {
        ui->labelDefaultAddressType->setHidden(true);
        ui->comboBoxDefaultAddressType->setHidden(true);
    }

    // access control
    if (!ac->user()->hasPermission(Permission::Rule::CanEditPermissions)) {
        ui->tableWidgetUsers->setEditTriggers(QAbstractItemView::NoEditTriggers);
        ui->labelNewUserName->setHidden(true);
        ui->lineEditNewUserName->setHidden(true);
        ui->labelNewUserEmail->setHidden(true);
        ui->lineEditNewUserEmail->setHidden(true);
        ui->toolButtonAddUser->setHidden(true);
        ui->toolButtonRemoveUser->setHidden(true);
    } else {
        // TODO: add and remove user in dropbox account programmatically and remove this block finally
        ui->labelNewUserName->setHidden(true);
        ui->lineEditNewUserName->setHidden(true);
        ui->labelNewUserEmail->setHidden(true);
        ui->lineEditNewUserEmail->setHidden(true);
        ui->toolButtonAddUser->setHidden(true);
        ui->toolButtonRemoveUser->setHidden(true);
    }

#ifndef QT_DEBUG
    ui->tabWidgetAccessControl->removeTab(1);
#endif
}

void Settings::on_listWidget_clicked(QModelIndex index)
{
    ui->stackedWidget->setCurrentIndex(index.row());

    int number = index.row();
    switch (number) {
    case 0:
        showGeneralPage();
        break;
    case 1:
        // exceptions
        showExceptionsPage();
        break;
    case 2:
        // lmm
        showLMMPage();
        break;
    case 3:
        showPublicTalkPage();
        break;
    case 4:
        showSongPage();
        break;
    case 5:
        showTerritoriesPage();
        break;
    case 6:
        // reminders
        showReminderPage();
        break;
    case 7:
        // access control
        showAccessControlPage();
        break;
    }
}

void Settings::on_buttonCloseDialog_clicked()
{
    // close button
    QSettings settings;
    if (saveGeneralPage) {
        //sql->saveSetting("groupLanguage", sql->getLanguageCode(l.at(ui->groupLanguage->currentIndex()).first));

        //sql->saveSetting("prayer", QVariant(ui->prayer->isChecked()).toString());
        //same reader
        //sql->saveSetting("sameReader", QVariant(ui->chkSameReader->isChecked()).toString());
        //muutamien ruksien tallennus ja login suljettaessa, salasanan tallennus editFinished tapahtumassa alempana
        sql->saveSetting("md5psw", QVariant(ui->checkBox_md5->isChecked()).toString());
        sql->saveSetting("cryptBase", QVariant(ui->checkBox_2_crypt_base->isChecked()).toString());
        QString login = ui->lineEdit_login->text();
        sql->saveSetting("login", login);

        if (psw_Changed && ui->checkBox_md5->isChecked()) {
            QMessageBox::information(0, "TheocBase",
                                     QObject::tr("Confirm password!"));
            ui->stackedWidget->setCurrentIndex(0);
            return;
        }
        // congregation save
        sql_item sitem;
        sitem.insert("name", ui->lineCongregation->text());
        if (ui->lineCongregation->property("original").toString() != ui->lineCongregation->text())
            sql->updateSql("congregations", "id", sql->getSetting("congregation_id"), &sitem);
        if (sql->getSetting("circuitoverseer", "") != ui->txtCOName->text())
            sql->saveSetting("circuitoverseer", ui->txtCOName->text());
        // dates
        // set school day
        school s;
        s.setSchoolDay(ui->cboMidweekMeetingDay->currentIndex() + 1);
        // times
        sitem.clear();
        sitem.insert("meeting1_time", ui->timeEdit->time().toString("hh:mm"));
        if (ui->timeEdit->time().toString("hh:mm") != ui->timeEdit->property("original").toString())
            sql->updateSql("congregations", "id", sql->getSetting("congregation_id"), &sitem);
        // printing
        sql->saveSetting("customTemplateDirectory", ui->txtCustomTemplates->text());
        // name format
        sql->saveSetting("nameFormat", ui->radioNameFormatFirst->isChecked() ? "%1 %2" : "%2, %1");
        // song titles
        settings.setValue("ui/show_song_titles", ui->chkShowSongTitles->isChecked());
    }
    if (saveExceptionPage) {
    }
    if (saveLMMPage) {
        LMM_Meeting m;
        m.setClasses(ui->spinBoxClassCount->value());
    }
    if (savePublicTalkPage) {
    }
    if (saveSongPage) {
    }
    if (saveTerritoryPage) {
        sql->saveSetting("territory_map_markerscale", QString::number(ui->doubleSpinBoxMarkerScale->value()));
        sql->saveSetting("territory_default_addresstype", ui->comboBoxDefaultAddressType->currentData().toString());
        settings.setValue("geo_service_provider/google_api_key", ui->lineEditGoogleAPIKey->text());
        settings.setValue("geo_service_provider/here_app_id", ui->lineEditHereAppId->text());
        settings.setValue("geo_service_provider/here_app_code", ui->lineEditHereAppCode->text());
        settings.setValue("geo_service_provider/default", ui->comboBoxDefaultGeoService->currentIndex());
    }
    if (saveRemindersPage) {
        // email reminders
        sql->saveSetting("reminder_send_onclosing", QVariant(ui->chkSendReminders->isChecked()).toString());
        sql->saveSetting("email_from_address", ui->lineEditEmailFromAddress->text());
        sql->saveSetting("email_from_name", ui->lineEditEmailFromName->text());

        settings.setValue("email/smtphost", ui->lineEditEmailSMTPServer->text());
        settings.setValue("email/smtpport", ui->lineEditEmailSMTPPort->text().toInt());
        settings.setValue("email/smtpconntype", ui->comboEmailConnType->currentIndex());
        SimpleCrypt crypt;
        crypt.setKey(constants::smtppasswd());
        settings.setValue("email/smtpusername", crypt.encryptToString(ui->lineEditEmailSMTPUsername->text()));
        settings.setValue("email/smtppassword", crypt.encryptToString(ui->lineEditEmailSMTPPasswd->text()));
    }
    if (saveAccessControlPage) {
        User *currentUser = ac->user();
        if (currentUser && cloud->logged() && currentUser->hasPermission(Permission::Rule::CanEditPermissions)) {
            // save access control file to Dropbox
            QProgressDialog progress("Uploading file to Dropbox...", "", 0, 0, this);
            progress.setCancelButton(nullptr);
            progress.setWindowModality(Qt::WindowModal);
            progress.show();
            cloud->uploadAccessControl();
            progress.close();
        }
    }
    this->close();
}

// --------------------------------------------------------------------------
// ----------------------------- GENERAL TAB --------------------------------
// --------------------------------------------------------------------------
void Settings::showGeneralPage()
{
    if (saveGeneralPage)
        return;
    ui->comboBoxUILanguage->blockSignals(true);
    //ui->groupLanguage->blockSignals(true);
    ui->comboBoxUILanguage->clear();
    //ui->groupLanguage->clear();
    l = sql->getLanguages();
    int defaultLangId = sql->getLanguageDefaultId();
    int setLangId = 0;
    for (int i = 0; i < l.count(); i++) {
        if (ui->comboBoxUILanguage->findText(l.at(i).second, Qt::MatchExactly) < 0)
            ui->comboBoxUILanguage->addItem(l.at(i).second);
        //        if(ui->groupLanguage->findText(l.at(i).second,Qt::MatchExactly) < 0)
        //            ui->groupLanguage->addItem(l.at(i).second);
        if (l.at(i).first == defaultLangId)
            setLangId = i;
    }
    ui->comboBoxUILanguage->setCurrentIndex(setLangId);

    ui->comboBoxUILanguage->blockSignals(false);
    //ui->groupLanguage->blockSignals(false);
    // congregation
    ccongregation c;
    auto myCongreg = c.getMyCongregation();
    ui->lineCongregation->setText(myCongreg.name);
    ui->lineCongregation->setProperty("original", myCongreg.name);
    ui->txtCOName->setText(sql->getSetting("circuitoverseer"));

    //login and password
    ui->checkBox_md5->setChecked(QVariant(sql->getSetting("md5psw")).toBool());
    ui->checkBox_2_crypt_base->setChecked(QVariant(sql->getSetting("cryptBase")).toBool());
    ui->lineEdit_login->setText(QString(sql->getSetting("login")));
    ui->lineEdit_psw->setText(QString(sql->getSetting("psw")));
    ui->lineEdit_psw2->setText("");
    ui->label_psw_ok->setVisible(ui->checkBox_md5->isChecked());
    ui->label_24_kuva->setVisible(false);
    ui->label_pswConfirm->setVisible(ui->checkBox_md5->isChecked());
    on_checkBox_md5_clicked();
    psw_Changed = false;
    connect(ui->lineEdit_psw, SIGNAL(textChanged(QString)), this, SLOT(on_lineEdit_psw2_editingFinished()));
    connect(ui->lineEdit_psw2, SIGNAL(textChanged(QString)), this, SLOT(on_lineEdit_psw2_editingFinished()));

    ui->lineCongregation->setText(myCongreg.name);
    // times
    QLocale local;
    qDebug() << "format" << local.timeFormat(local.ShortFormat);
    if (ui->timeEdit->isRightToLeft())
        // workaround to show the time in right format when right-to-left user interface
        ui->timeEdit->setLayoutDirection(Qt::LeftToRight);    
    ui->timeEdit->setDisplayFormat(local.timeFormat(QLocale::ShortFormat));
    ui->timeEdit->setLayoutDirection(this->layoutDirection());

    QTime schooltime = QTime::fromString(myCongreg.time_meeting1, "hh:mm");
    ui->timeEdit->setTime(schooltime);
    ui->timeEdit->setProperty("original", myCongreg.time_meeting1);    

    school s;
    ui->cboMidweekMeetingDay->setCurrentIndex(s.getSchoolDay() - 1);

    ui->txtCustomTemplates->setText(sql->getSetting("customTemplateDirectory"));

    // name format
    QString nameFormat = sql->getSetting("nameFormat", "%2, %1");
    if (nameFormat == "%1 %2")
        ui->radioNameFormatFirst->setChecked(true);
    else
        ui->radioNameFormatLast->setChecked(true);

    QSettings settings;
    ui->chkShowSongTitles->setChecked(settings.value("ui/show_song_titles", false).toBool());

    saveGeneralPage = true;
}

// general - language combobox changed
void Settings::on_comboBoxUILanguage_currentIndexChanged(int index)
{
    QString language = "";
    language = sql->getLanguageCode(l.at(ui->comboBoxUILanguage->currentIndex()).first);
    qDebug() << language;

    sql->saveSetting("theocbase_language", language);
    QLocale langLocale(language);
    if (QLocale::system().name().startsWith(langLocale.name().split("_").first()) || langLocale == QLocale::C)
        QLocale::setDefault(QLocale::system());
    else
        QLocale::setDefault(langLocale);

    // kokouspivt talteen, muuten nollautuu
    int schoolday = ui->cboMidweekMeetingDay->currentIndex();
#ifdef Q_OS_MAC
    translator.load(QString(theocbaseDirPath + "/../Resources/theocbase_" + language));
#else
    translator.load(QString(theocbaseDirPath + "/theocbase_" + language));
#endif

    qApp->installTranslator(&translator);
    qtTranslator.load("qt_" + language,
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    qApp->installTranslator(&qtTranslator);
    ui->comboBoxUILanguage->blockSignals(true);
    ui->retranslateUi(this);
    ui->comboBoxUILanguage->setCurrentIndex(index);
    ui->comboBoxUILanguage->blockSignals(false);

    // palautetaan kokouspivt
    ui->cboMidweekMeetingDay->setCurrentIndex(schoolday);
}

void Settings::on_btnJumpToCongregationDayTime_clicked()
{
    ccongregation c;
    speakersui dialog(this, c.getMyCongregation().id);
    dialog.exec();
}

// general - password selected
void Settings::on_checkBox_md5_clicked()
{
    if (ui->checkBox_md5->isChecked()) {
        ui->lineEdit_login->setVisible(true);
        ui->lineEdit_psw->setVisible(true);
        ui->lineEdit_psw2->setVisible(true);
        ui->label_24_login->setVisible(true);
        ui->label_24_psw->setVisible(true);
        ui->label_24_kuva->setVisible(true);
        ui->label_psw_ok->setVisible(false);
        ui->label_pswConfirm->setVisible(true);
        psw_Changed = true;
    } else {
        ui->lineEdit_login->setVisible(false);
        ui->lineEdit_psw->setVisible(false);
        ui->lineEdit_psw2->setVisible(false);
        ui->label_24_login->setVisible(false);
        ui->label_24_psw->setVisible(false);
        ui->label_24_kuva->setVisible(false);
        ui->label_psw_ok->setVisible(false);
        ui->label_pswConfirm->setVisible(false);
    }
}

// general - password edited
void Settings::on_lineEdit_psw2_editingFinished()
{
    QString pswd2 = ui->lineEdit_psw2->text();
    QByteArray hashpsw2(QCryptographicHash::hash(pswd2.toLatin1(), QCryptographicHash::Md5));
    QString md5pswd2(hashpsw2.toHex().constData());

    QString pswd = ui->lineEdit_psw->text();
    QByteArray hashpsw(QCryptographicHash::hash(pswd.toLatin1(), QCryptographicHash::Md5));
    QString md5pswd(hashpsw.toHex().constData());

    //int vertailu = QString::compare(md5pswd, md5pswd2);
    if (md5pswd == md5pswd2) {
        ui->label_24_kuva->setHidden(true);
        ui->label_psw_ok->setVisible(true);
        sql->saveSetting("psw", md5pswd);
        psw_Changed = false;
    } else {
        ui->label_24_kuva->setVisible(true);
        ui->label_psw_ok->setHidden(true);
        psw_Changed = true;
    }
}

// general - save backup file
void Settings::on_buttonSaveBackup_clicked()
{
    // write backup
    QSettings settings;
    QString d = settings.value("settingsui/backup", QDir::homePath()).toString();
    qDebug() << d;
    QString exportpath = QFileDialog::getSaveFileName(this, tr("Save database"),
                                                      d + "/theocbase_backup.sqlite",
                                                      "Database (*.sqlite)");
    if (exportpath == "")
        return;
    if (QFile::exists(exportpath))
        QFile::remove(exportpath);
    QFile::copy(sql->databasepath, exportpath);
    QMessageBox::information(this, "TheocBase", tr("Database backuped"));
}

// general - restore backup file
void Settings::on_buttonRestoreBackup_clicked()
{
    // restore backup file
    QString appPath = QApplication::applicationFilePath();
    QSettings settings;
    QString d = settings.value("settingsui/backup", QDir::homePath()).toString();
    QString restorefile = QFileDialog::getOpenFileName(this, tr("Select a backup file"),
                                                       d,
                                                       "Database (*.sqlite)");
    qDebug() << restorefile;
    if (restorefile == "")
        return;

    // selected file renamed to restore_backup.sqlite ==> restart use it

    QFileInfo info(sql->databasepath);

    if (!QFile::copy(restorefile, info.dir().path() + "/restore_backup.sqlite"))
        return;

    QFileInfo info2(restorefile);

    settings.setValue("settingsui/backup", info2.dir().path());

    QMessageBox::information(this, "TheocBase", tr("Database restored. The program will be restarted."));

    //QProcess *proc = new QProcess();

    // empty QStringList is used because of space in path C:\Program Files\...
    if (QProcess::startDetached(appPath, QStringList())) {
        qApp->quit();
    }
}

void Settings::on_btnBrowseCustomTemplates_clicked()
{
    QString dir = QFileDialog::getExistingDirectory(this, "Custom Templates", sql->getSetting("customTemplateDirectory", QDir::homePath()));
    if (!dir.isEmpty()) {
        ui->txtCustomTemplates->setText(dir);
    }
}

// --------------------------------------------------------------------------
// ------------------------------ EXCEPTION PAGE ----------------------------
// --------------------------------------------------------------------------
void Settings::showExceptionsPage()
{
    this->on_comboBoxException_currentIndexChanged(0);

    ui->dateEditException1->calendarWidget()->setFirstDayOfWeek(Qt::Monday);
    ui->dateEditException2->calendarWidget()->setFirstDayOfWeek(Qt::Monday);
    ui->dateEditException1->setDate(QDate::currentDate());
    ui->tableExceptions->clear();
    ui->tableExceptions->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableExceptions->setSelectionMode(QAbstractItemView::SingleSelection);
    sql_items exep = sql->selectSql("exceptions", "active", "1", "date DESC");
    ui->tableExceptions->setColumnCount(5);
    ui->tableExceptions->setHorizontalHeaderItem(0, new QTableWidgetItem(tr("Exception")));
    ui->tableExceptions->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("Start date")));
    ui->tableExceptions->setHorizontalHeaderItem(2, new QTableWidgetItem(tr("End date")));
    ui->tableExceptions->setHorizontalHeaderItem(3, new QTableWidgetItem(tr("Meeting 1")));
    ui->tableExceptions->setHorizontalHeaderItem(4, new QTableWidgetItem(tr("Meeting 2")));
    ui->tableExceptions->setRowCount(static_cast<int>(exep.size()));
    if (!exep.empty()) {
        for (unsigned int i = 0; i < exep.size(); i++) {
            sql_item s = exep[i];
            QTableWidgetItem *exception = new QTableWidgetItem;
            int pnro = s.value("type").toInt();
            if (pnro == 0) {
                exception->setText(ui->comboBoxException->itemText(0));
            } else if (pnro == 1) {
                exception->setText(ui->comboBoxException->itemText(1));
            } else {
                if (s.value("desc").toString() != "") { // now that "value" is variant, will the blank be ""?
                    exception->setText(s.value("desc").toString());
                } else {
                    exception->setText(ui->comboBoxException->itemText(4));
                }
            }

            ui->tableExceptions->setItem(i, 0, exception);
            QTableWidgetItem *date = new QTableWidgetItem;
            date->setText(s.value("date").toString());
            ui->tableExceptions->setItem(i, 1, date);
            QTableWidgetItem *date2 = new QTableWidgetItem;
            date2->setText(s.value("date2").toString());
            ui->tableExceptions->setItem(i, 2, date2);
            ui->tableExceptions->setItem(i, 3, new QTableWidgetItem(pnro == 0 ? s.value("cbsday") == "" ? "" : "2 / " + s.value("cbsday").toString() : s.value("schoolday").toString()));
            ui->tableExceptions->setItem(i, 4, new QTableWidgetItem(s.value("publicmeetingday").toString()));
            ui->tableExceptions->resizeColumnToContents(0);
            ui->tableExceptions->setColumnWidth(1, 100);
            ui->tableExceptions->setColumnWidth(2, 100);
            ui->tableExceptions->resizeColumnToContents(3);
            ui->tableExceptions->resizeColumnToContents(4);
            ui->tableExceptions->verticalHeader()->setVisible(false);
        }
    }
    // read-only
    ui->tableExceptions->setEditTriggers(QAbstractItemView::NoEditTriggers);
    saveExceptionPage = true;
}

// exception - date changed
void Settings::on_dateEditException1_dateChanged(QDate date)
{
    ui->dateEditException2->setDate(date);
    on_comboBoxException_currentIndexChanged(ui->comboBoxException->currentIndex());
}

// exception - data added
void Settings::on_buttonAddException_clicked()
{
    // add new exception
    sql_item s;

    s.insert("type", ui->comboBoxException->currentIndex());
    s.insert("date", ui->dateEditException1->date()); // was ".toString(Qt::ISODate)" <<verify we still get same data
    s.insert("date2", ui->dateEditException2->date()); // was ".toString(Qt::ISODate)" <<verify we still get same data

    if (ui->comboBoxException->currentIndex() == 0) {
        // CO visit - save CBS day
        s.insert("cbsday", 0);
        if (ui->dateEditException1->date().year() >= 2016) {
            // add co talk for lmm_schedule
            QDate startdate = ui->dateEditException1->date().addDays((ui->dateEditException1->date().dayOfWeek() - 1) * -1);
            LMM_Schedule cotalk(LMM_Schedule::TalkType_COTalk, 0, startdate, "", "", 30, -1);
            cotalk.setTheme(cotalk.talkName());
            cotalk.save();
            // remove assignments from aux. classes
            sql_item a;
            a.insert("assignee_id", -1);
            a.insert("assistant_id", -1);
            a.insert("date", startdate);
            sql->execSql("UPDATE lmm_assignment SET assignee_id = :assignee_id, assistant_id = :assistant_id,"
                         "time_stamp = strftime('%s','now') WHERE date = :date AND classnumber > 1",
                         &a);
        }
    } else if (ui->comboBoxException->currentIndex() == 1) {
        // convention --> clear meeting data
        QDate fd = ui->dateEditException1->date().addDays((ui->dateEditException1->date().dayOfWeek() - 1) * -1);
        sql_items si = sql->selectSql("school", "date", fd.toString(Qt::ISODate), "");
        if (!si.empty())
            sql->removeSql("school", "date = '" + fd.toString(Qt::ISODate) + "'");
        si = sql->selectSql("publicmeeting", "date", fd.toString(Qt::ISODate), "");
        if (!si.empty())
            sql->removeSql("publicmeeting", "date = '" + fd.toString(Qt::ISODate) + "'");
    } else {
        s.insert("schoolday", ui->comboBoxException1->currentIndex());
        s.insert("publicmeetingday", ui->comboBoxException2->currentIndex());
        s.insert("desc", ui->lineEditException->text());
    }
    sql->insertSql("exceptions", &s, "id");
    showExceptionsPage();
}

// exception - removed
void Settings::on_buttonRemoveException_clicked()
{
    // remove selected expcetion row
    if (ui->tableExceptions->selectedItems().count() < 1)
        return;

    QTableWidgetItem *selecteditem = ui->tableExceptions->selectedItems().first();

    // new
    sql_item s;
    s.insert("active", 0);
    s.insert("date", ui->tableExceptions->item(selecteditem->row(), 1)->text());
    s.insert("date2", ui->tableExceptions->item(selecteditem->row(), 2)->text());
    if (sql->execSql("UPDATE exceptions SET active = :active, time_stamp = strftime('%s','now') WHERE date = :date AND date2 = :date2", &s)) {
        if (ui->tableExceptions->item(selecteditem->row(), 0)->text() == ui->comboBoxException->itemText(0) && QDate::fromString(ui->tableExceptions->item(selecteditem->row(), 1)->text(), Qt::ISODate).year() >= 2016) {
            // co visit -> remove lmm_schedule
            QDate firstdayofweek = QDate::fromString(ui->tableExceptions->item(selecteditem->row(), 1)->text(), Qt::ISODate);
            firstdayofweek = firstdayofweek.addDays((firstdayofweek.dayOfWeek() - 1) * -1);
            s.clear();
            s.insert("date", firstdayofweek);
            s.insert("talk_id", LMM_Schedule::TalkType_COTalk);
            s.insert("active", 0);
            sql->execSql("update lmm_schedule set active = :active, time_stamp = strftime('%s','now') where "
                         "date=:date and talk_id=:talk_id",
                         &s);
        }
        this->showExceptionsPage();
    }
}

// exception - combobox changed
void Settings::on_comboBoxException_currentIndexChanged(int index)
{
    ui->labelException1->setEnabled(index > 1);
    ui->labelException2->setEnabled(index > 1);
    //ui->labelException3->setVisible(false);
    ui->comboBoxException1->setEnabled(index > 1);
    ui->comboBoxException2->setEnabled(index > 1);
    //ui->comboBoxException3->setVisible(false);
    ui->labelExceptionDesc->setVisible(index > 1);
    ui->lineEditException->setVisible(index > 1);
    // set default meeting days
    school s;
    int schoolday = s.getSchoolDay();
    ccongregation c;
    int publicmeetingday = c.getMyCongregation().getPublicmeeting(ui->dateEditException1->date()).getMeetingday();
    ui->comboBoxException1->setCurrentIndex(schoolday);
    ui->comboBoxException2->setCurrentIndex(publicmeetingday);

    switch (index) {
    case 0:
        // circuit overseer's visit
        ui->comboBoxException1->setCurrentIndex(2);
        //ui->comboBoxException3->setCurrentIndex(0);
        break;
    case 1:
        // convention
        ui->comboBoxException1->setCurrentIndex(0);
        ui->comboBoxException2->setCurrentIndex(0);
        break;
    case 2:
        // memorial exception
        // no other meeting on the same day
        if (ui->dateEditException1->date().dayOfWeek() <= 5) {
            ui->comboBoxException1->setCurrentIndex(0);
            ui->comboBoxException1->setEnabled(false);
        } else {
            ui->comboBoxException2->setCurrentIndex(0);
            ui->comboBoxException2->setEnabled(false);
        }
        break;
    }

    if (index > 1) {
        // exception text
        ui->lineEditException->setText(ui->comboBoxException->itemText(index));
    }
}

// --------------------------------------------------------------------------
// ----------------------------- PUBLIC TALK PAGE ---------------------------
// --------------------------------------------------------------------------
Settings::PublicTalksSFProxyModel::PublicTalksSFProxyModel(QObject *parent)
    : QSortFilterProxyModel(parent), isDiscontinuedHidden(true)
{
}

bool Settings::PublicTalksSFProxyModel::filterAcceptsRow(int sourceRow,
                                                         const QModelIndex &sourceParent) const
{
    if (isDiscontinuedHidden) {
        QModelIndex index5 = sourceModel()->index(sourceRow, 5, sourceParent);
        QString dateString = sourceModel()->data(index5).toString();
        QString dateFormat = QLocale().dateFormat(QLocale::ShortFormat);
        QDate discontinueDate = QDate::fromString(dateString, dateFormat);

        return !discontinueDate.isValid();
    } else
        return true;
}

void Settings::PublicTalksSFProxyModel::setDiscontinuedHidden(bool value)
{
    isDiscontinuedHidden = value;
}

void Settings::showPublicTalkPage()
{
    loadPublicTalks();
    loadSpeakers();
    savePublicTalkPage = true;
}

// public talk - load public talk table
void Settings::loadPublicTalks()
{
    esitelmatCellUpdate = false;
    sql_items prog = sql->selectSql("publictalks", "active", "1", "theme_number, lang_id, release_date, id");
    publicTalksTable = new QStandardItemModel();
    publicTalksTable->setColumnCount(7);
    publicTalksTable->setRowCount(static_cast<int>(prog.size()));
    publicTalksTable->setHorizontalHeaderItem(0, new QStandardItem("id"));
    publicTalksTable->setHorizontalHeaderItem(1, new QStandardItem(tr("Number")));
    publicTalksTable->setHorizontalHeaderItem(2, new QStandardItem(tr("Theme")));
    publicTalksTable->setHorizontalHeaderItem(3, new QStandardItem(tr("Revision", "Public talk outline revision")));
    publicTalksTable->setHorizontalHeaderItem(4, new QStandardItem(tr("Released on", "Release date of the public talk outline")));
    publicTalksTable->setHorizontalHeaderItem(5, new QStandardItem(tr("Discontinued on", "Date after which the public talk outline should no longer be used")));
    publicTalksTable->setHorizontalHeaderItem(6, new QStandardItem(tr("Language id")));

    if (!prog.empty()) {
        QLocale local;
        for (unsigned int i = 0; i < prog.size(); i++) {
            sql_item s = prog[i];
            QStandardItem *idItem = new QStandardItem(s.value("id").toString());
            idItem->setFlags(Qt::NoItemFlags);
            publicTalksTable->setItem(i, 0, idItem);
            QStandardItem *themeNumberItem = new QStandardItem(s.value("theme_number").toString());
            themeNumberItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
            publicTalksTable->setItem(i, 1, themeNumberItem);
            publicTalksTable->setItem(i, 2, new QStandardItem(s.value("theme_name").toString()));
            publicTalksTable->setItem(i, 3, new QStandardItem(s.value("revision").toString()));
            QDate release_date = s.value("release_date").toDate();
            QStandardItem *releaseDateItem = new QStandardItem();
            releaseDateItem->setData(release_date, DateEditBox::DateRole);
            if (!release_date.isNull()) {
                releaseDateItem->setText(release_date.toString(local.dateFormat(QLocale::ShortFormat)));
            }
            publicTalksTable->setItem(i, 4, releaseDateItem);
            QDate discontinue_date = s.value("discontinue_date").toDate();
            QStandardItem *discontinueDateItem = new QStandardItem();
            discontinueDateItem->setData(discontinue_date, DateEditBox::DateRole);
            if (!discontinue_date.isNull()) {
                discontinueDateItem->setText(discontinue_date.toString(local.dateFormat(QLocale::ShortFormat)));
            }
            publicTalksTable->setItem(i, 5, discontinueDateItem);
            QStandardItem *langItem = new QStandardItem(s.value("lang_id").toString());
            langItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
            publicTalksTable->setItem(i, 6, langItem);
        }
    }

    publicTalksProxyModel = new PublicTalksSFProxyModel();
    ui->tableViewPublicTalks->setModel(publicTalksProxyModel);
    ui->tableViewPublicTalks->setSelectionBehavior(QAbstractItemView::SelectRows);
    publicTalksProxyModel->setSourceModel(publicTalksTable);
    ui->tableViewPublicTalks->setColumnHidden(0, true);
    ui->tableViewPublicTalks->setColumnHidden(4, true);
    ui->tableViewPublicTalks->setColumnHidden(5, true);
    ui->tableViewPublicTalks->setColumnWidth(1, 70);
    ui->tableViewPublicTalks->resizeColumnToContents(2);
    ui->tableViewPublicTalks->setItemDelegateForColumn(3, new DateEditBox(this, "M/yy"));
    ui->tableViewPublicTalks->setItemDelegateForColumn(4, new DateEditBox(this));
    ui->tableViewPublicTalks->setItemDelegateForColumn(5, new DateEditBox(this));
    ui->tableViewPublicTalks->verticalHeader()->setVisible(false);

    QObject::connect(publicTalksTable, SIGNAL(itemChanged(QStandardItem *)), this, SLOT(on_publicTalksTable_itemChanged(QStandardItem *)));

    esitelmatCellUpdate = true;

    // languages
    ui->comboPublicTalkLang->clear();
    QList<QPair<int, QString>> l = sql->getLanguages();
    int defaultLangId = sql->getLanguageDefaultId();
    int setLangId = 0;
    for (int i = 0; i < l.count(); i++) {
        if (ui->comboPublicTalkLang->findText(l.at(i).second, Qt::MatchExactly) < 0)
            ui->comboPublicTalkLang->addItem(l.at(i).second);
        if (l.at(i).first == defaultLangId)
            setLangId = i;
    }
    ui->comboPublicTalkLang->setCurrentIndex(setLangId);

    ui->checkBoxHideDiscontinued->setCheckState(Qt::CheckState::Checked);
}

void Settings::on_checkBoxHideDiscontinued_stateChanged(int arg1)
{
    ui->tableViewPublicTalks->setColumnHidden(4, arg1);
    ui->tableViewPublicTalks->setColumnHidden(5, arg1);

    publicTalksProxyModel->setDiscontinuedHidden(arg1);
    publicTalksProxyModel->setFilterKeyColumn(5);
}

// public talk - add public talk themes
void Settings::on_buttonAddPublicTalks_clicked()
{
    importwizard dialog;
    connect(&dialog, &importwizard::discontinueTalk, this, &Settings::discontinueTalk);
    dialog.setType(3);
    dialog.show();
    dialog.exec();
    loadPublicTalks();
    //QFileDialog::getOpenFileName(this,
    //tr("Open SQL"), "/", tr("SQL Files (*.sql)"));
}

// public talk - remove public talk theme
void Settings::on_toolButtonRemovePublicTalk_clicked()
{
    // remove selected row from public talks
    QItemSelectionModel *select = ui->tableViewPublicTalks->selectionModel();
    if (!select->hasSelection())
        return;

    QModelIndexList rowList = select->selectedRows(0);

    if (QMessageBox::question(this, "",
                              tr("Remove selected row?"),
                              QMessageBox::No, QMessageBox::Yes)
        == QMessageBox::No)
        return;

    for (int i = rowList.count() - 1; i >= 0; i--) {
        sql_item s;
        s.insert("active", 0);
        if (sql->updateSql("publictalks", "id", publicTalksProxyModel->data(rowList.at(i), 0).toString(), &s))
            publicTalksProxyModel->removeRow(rowList.at(i).row());
    }
}

// public talk - edit public talk theme
void Settings::on_publicTalksTable_itemChanged(QStandardItem *item)
{
    if (!esitelmatCellUpdate)
        return;
    QString themeId = publicTalksTable->item(item->row(), 0)->text();
    sql_item al;
    switch (item->column()) {
    case 1:
        al.insert("theme_number", item->text());
        break;
    case 2:
        al.insert("theme_name", item->text());
        break;
    case 3: {
        QDate date = item->data(DateEditBox::DateRole).toDate();
        al.insert("revision", date.isValid() ? date.toString("M/yy") : "");
        break;
    }
    case 4: {
        QDate date = general::TextToDate(item->text());
        al.insert("release_date", date.toString(Qt::ISODate));
        break;
    }
    case 5: {
        QDate date = item->data(DateEditBox::DateRole).toDate();
        if (date.isValid()) {
            // check only current or future dates
            QDate startDate = QDateTime::currentDateTime().date().addDays(-7);
            if (date < startDate)
                date = startDate;

            sql_items incomingTalks;
            incomingTalks = sql->selectSql("SELECT * from publicmeeting WHERE date > '" + date.toString(Qt::ISODate) + "' AND theme_id = " + QVariant(themeId).toString() + " AND active");
            sql_items outgoingTalks;
            outgoingTalks = sql->selectSql("SELECT * from outgoing WHERE date > '" + date.toString(Qt::ISODate) + "' AND theme_id = " + QVariant(themeId).toString() + " AND active");
            if (!incomingTalks.empty() || !outgoingTalks.empty()) {
                QMessageBox::information(this, "TheocBase",
                                         tr("Discontinuing this talk will move talks scheduled with this outline to the To Do List.\n\n"));

                emit discontinueTalk(themeId.toInt());
            }
        }

        al.insert("discontinue_date", date.toString(Qt::ISODate));
        break;
    }
    }

    sql->updateSql("publictalks", "id", themeId, &al);
    qDebug() << "Cell updated to database (" + themeId + ")";
}

// public talk - add single public talk theme
void Settings::on_buttonAddOnePublicTalk_clicked()
{
    if (ui->txtPublicTalkNo->text() == "") {
        QMessageBox::information(this, "TheocBase", tr("Public talk number missing"));
        return;
    } else if (ui->txtPublicTalkTheme->text() == "") {
        QMessageBox::information(this, "TheocBase", tr("Public talk subject missing"));
        return;
    }

    QList<QPair<int, QString>> l = sql->getLanguages();
    int selectedLanguage = l.at(ui->comboPublicTalkLang->currentIndex()).first;

    QDate date = QDate::currentDate();
    // add a single theme
    sql_items e = sql->selectSql("SELECT * FROM publictalks WHERE theme_number = '" + ui->txtPublicTalkNo->text() + "' AND lang_id = '" + QVariant(selectedLanguage).toString() + "' AND active "
                                                                                                                                                                                  "AND (discontinue_date IS NULL OR discontinue_date='' OR discontinue_date > '"
                                 + date.toString(Qt::ISODate) + "') "
                                                                "AND (release_date IS NULL OR release_date='' OR release_date <= '"
                                 + date.toString(Qt::ISODate) + "') ");

    if (!e.empty()) {
        QString existingTheme = e[0].value("theme_name").toString();
        QString newTheme = ui->txtPublicTalkTheme->text();

        // similarity check
        if (existingTheme.compare(newTheme, Qt::CaseInsensitive) == 0) {
            QMessageBox::information(this, "TheocBase", tr("Public talk is already saved!"));
            return;
        } else {
            if (QMessageBox::question(this, "TheocBase",
                                      tr("A public talk with the same number is already saved!\n"
                                         "Do you want to discontinue the previous talk?\n\n"
                                         "Scheduled talks will be moved to the To Do List."),
                                      QMessageBox::Ok, QMessageBox::Cancel)
                == QMessageBox::Cancel)
                return;

            // discontinue previous talk
            sql_item s;
            s.insert("discontinue_date", QDate::currentDate().addDays(-1).toString(Qt::ISODate));
            sql->updateSql("publictalks", "id", e[0].value("id").toString(), &s);

            emit discontinueTalk(e[0].value("id").toInt());
        }
    }

    sql_item ne;
    ne.insert("theme_number", ui->txtPublicTalkNo->text());
    ne.insert("theme_name", ui->txtPublicTalkTheme->text());
    ne.insert("lang_id", selectedLanguage);

    // check for discontinued talks
    e = sql->selectSql("SELECT * FROM publictalks WHERE theme_number = '" + ui->txtPublicTalkNo->text() + "' AND lang_id = '" + QVariant(selectedLanguage).toString() + "' AND active");
    if (!e.empty()) {
        // discontined talk exists; add current date as release date,
        // to make sure no concurrent talks exist at the same time
        ne.insert("release_date", date.toString(Qt::ISODate));
    }

    if (sql->insertSql("publictalks", &ne, "id") != -1) {
        QMessageBox::information(this, "TheocBase", tr("Public talk added to database"));
        // update list
        loadPublicTalks();
        //kenttien tyhjennys
        ui->txtPublicTalkNo->clear();
        ui->txtPublicTalkTheme->clear();
    } else {
        QMessageBox::warning(this, "TheocBase", tr("Adding failed"));
    }
}

// public talk - add public speakers and congregations
void Settings::on_buttonAddCongSpeakers_clicked()
{
    importwizard dialog;
    dialog.setType(4);
    dialog.show();
    dialog.exec();
    loadSpeakers();
}

void Settings::on_tableWidgetSectionClicked(int index)
{
    ui->tableWidget->sortByColumn(index, Qt::AscendingOrder);
}

// public talk - show speakers and congregations
void Settings::loadSpeakers()
{

    cpersons pobject;
    person *pj;
    sql_items e_henkilot = sql->selectSql("SELECT spt.id, spt.speaker_id, spt.lang_id, pt.theme_number, pt.revision FROM speaker_publictalks spt "
                                          "LEFT JOIN publictalks pt ON spt.theme_id = pt.id WHERE spt.active");
    if (!e_henkilot.empty()) {

        ui->tableWidget->setColumnCount(6);
        ui->tableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem(tr("Id")));
        ui->tableWidget->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("Speaker")));
        ui->tableWidget->setHorizontalHeaderItem(2, new QTableWidgetItem(tr("Congregation")));
        ui->tableWidget->setHorizontalHeaderItem(3, new QTableWidgetItem(tr("Phone")));
        ui->tableWidget->setHorizontalHeaderItem(4, new QTableWidgetItem(tr("Number")));
        ui->tableWidget->setHorizontalHeaderItem(5, new QTableWidgetItem(tr("Revision")));
        ui->tableWidget->setHorizontalHeaderItem(6, new QTableWidgetItem(tr("Language id")));
        ui->tableWidget->setRowCount(static_cast<int>(e_henkilot.size()));
        ui->tableWidget->setColumnHidden(0, true);
        for (unsigned int i = 0; i < e_henkilot.size(); i++) {
            sql_item s = e_henkilot[i];

            QTableWidgetItem *id = new QTableWidgetItem;
            id->setFlags(Qt::NoItemFlags);
            id->setText(s.value("id").toString());
            ui->tableWidget->setItem(i, 0, id);

            QTableWidgetItem *puhuja = new QTableWidgetItem;
            puhuja->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
            pj = pobject.getPerson(e_henkilot[i].value("speaker_id").toInt());
            if (pj)
                puhuja->setText(pj->fullname());
            ui->tableWidget->setItem(i, 1, puhuja);

            QTableWidgetItem *seurakunta = new QTableWidgetItem;
            seurakunta->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
            if (pj)
                seurakunta->setText(pj->congregationName());
            ui->tableWidget->setItem(i, 2, seurakunta);

            QTableWidgetItem *puh = new QTableWidgetItem;
            puh->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
            if (pj)
                puh->setText(pj->phone());
            ui->tableWidget->setItem(i, 3, puh);

            QTableWidgetItem *esitelmat = new QTableWidgetItem;
            esitelmat->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
            //esitelmat->setText(s.value("theme_number").toString());
            esitelmat->setData(Qt::EditRole, s.value("theme_number").toInt());
            ui->tableWidget->setItem(i, 4, esitelmat);

            QTableWidgetItem *revision = new QTableWidgetItem;
            revision->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
            revision->setData(Qt::EditRole, s.value("revision").toString());
            ui->tableWidget->setItem(i, 5, revision);

            QTableWidgetItem *kieli = new QTableWidgetItem;
            kieli->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
            kieli->setText(s.value("lang_id").toString());
            ui->tableWidget->setItem(i, 6, kieli);
        }
    }
}

// --------------------------------------------------------------------------
// ----------------------------- SONGS PAGE ---------------------------------
// --------------------------------------------------------------------------
void Settings::showSongPage()
{
    loadSongs();
    saveSongPage = true;
}

void Settings::loadSongs()
{
    songCellUpdate = false;
    ui->tableWidgetSongs->clear();
    sql_items songs = sql->selectSql("song", "active", "1", "song_number, lang_id");
    ui->tableWidgetSongs->setColumnCount(4);
    ui->tableWidgetSongs->setRowCount(static_cast<int>(songs.size()));
    ui->tableWidgetSongs->setHorizontalHeaderItem(0, new QTableWidgetItem("id"));
    ui->tableWidgetSongs->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("Number")));
    ui->tableWidgetSongs->setHorizontalHeaderItem(2, new QTableWidgetItem(tr("Title")));
    ui->tableWidgetSongs->setHorizontalHeaderItem(3, new QTableWidgetItem(tr("Language id")));

    if (!songs.empty()) {
        for (unsigned int i = 0; i < songs.size(); i++) {
            sql_item s = songs[i];
            QTableWidgetItem *id = new QTableWidgetItem;
            id->setText(s.value("id").toString());

            QTableWidgetItem *song_number = new QTableWidgetItem;
            song_number->setText(s.value("song_number").toString());
            ui->tableWidgetSongs->setItem(i, 1, song_number);
            song_number->setFlags(Qt::NoItemFlags);

            QTableWidgetItem *title = new QTableWidgetItem;
            title->setText(s.value("title").toString());

            QTableWidgetItem *lang_id = new QTableWidgetItem;
            lang_id->setText(s.value("lang_id").toString());
            lang_id->setFlags(Qt::NoItemFlags);

            ui->tableWidgetSongs->setColumnHidden(0, true);
            ui->tableWidgetSongs->setItem(i, 0, id);
            ui->tableWidgetSongs->setItem(i, 1, song_number);
            ui->tableWidgetSongs->setItem(i, 2, title);
            ui->tableWidgetSongs->setItem(i, 3, lang_id);
            ui->tableWidgetSongs->setColumnWidth(1, 70);
            ui->tableWidgetSongs->resizeColumnToContents(2);
            ui->tableWidgetSongs->verticalHeader()->setVisible(false);
        }
    }
    songCellUpdate = true;

    // languages
    ui->comboSongLang->clear();
    QList<QPair<int, QString>> l = sql->getLanguages();
    int defaultLangId = sql->getLanguageDefaultId();
    int setLangId = 0;
    for (int i = 0; i < l.count(); i++) {
        if (ui->comboSongLang->findText(l.at(i).second, Qt::MatchExactly) < 0)
            ui->comboSongLang->addItem(l.at(i).second);
        if (l.at(i).first == defaultLangId)
            setLangId = i;
    }
    ui->comboSongLang->setCurrentIndex(setLangId);
}

void Settings::on_tableWidgetSongs_cellChanged(int row, int column)
{
    if (!songCellUpdate)
        return;
    QString id = ui->tableWidgetSongs->item(row, 0)->text();
    sql_item updateItem;
    switch (column) {
    case 1:
        updateItem.insert("song_number", ui->tableWidgetSongs->item(row, column)->text());
        break;
    case 2:
        updateItem.insert("title", ui->tableWidgetSongs->item(row, column)->text());
        break;
    }

    sql->updateSql("song", "id", id, &updateItem);
    qDebug() << "Cell updated to database (" + id + ")";
}

void Settings::on_toolButtonAddSongs_clicked()
{
    importwizard dialog;
    dialog.setType(5);
    dialog.show();
    dialog.exec();
    loadSongs();
}

void Settings::on_toolButtonRemoveSong_clicked()
{
    // remove selected row from songs
    if (ui->tableWidgetSongs->selectedItems().count() < 1)
        return;

    QList<QTableWidgetItem *> rowList = ui->tableWidgetSongs->selectedItems();

    if (QMessageBox::question(this, "",
                              tr("Remove selected row?"),
                              QMessageBox::No, QMessageBox::Yes)
        == QMessageBox::No)
        return;

    for (int i = rowList.count() - 1; i >= 0; i--) {
        sql_item s;
        s.insert("active", 0);
        if (sql->updateSql("song", "id", ui->tableWidgetSongs->item(rowList.at(i)->row(), 0)->text(), &s))
            ui->tableWidgetSongs->removeRow(rowList.at(i)->row());
    }
}

void Settings::on_toolButtonAddOneSong_clicked()
{
    if (ui->lineEditSongNo->text() == "") {
        QMessageBox::information(this, "TheocBase", tr("Song number missing"));
        return;
    } else if (ui->lineEditSongTitle->text() == "") {
        QMessageBox::information(this, "TheocBase", tr("Song title missing"));
        return;
    }

    QList<QPair<int, QString>> l = sql->getLanguages();
    int selectedLanguage = l.at(ui->comboSongLang->currentIndex()).first;

    // add a single theme
    sql_items e = sql->selectSql("SELECT * FROM song WHERE song_number = '" + ui->lineEditSongNo->text() + "' AND lang_id = '" + QVariant(selectedLanguage).toString() + "' AND active");
    if (!e.empty()) {
        QMessageBox::information(this, "TheocBase", tr("Song is already saved!"));
        return;
    }

    sql_item ns;
    ns.insert("song_number", ui->lineEditSongNo->text());
    ns.insert("title", ui->lineEditSongTitle->text());
    ns.insert("lang_id", selectedLanguage);
    if (sql->insertSql("song", &ns, "id") != -1) {
        QMessageBox::information(this, "TheocBase", tr("Song added to database"));
        // update list
        loadSongs();
        // clear controls
        ui->lineEditSongNo->clear();
        ui->lineEditSongTitle->clear();
    } else {
        QMessageBox::warning(this, "TheocBase", tr("Adding failed"));
    }
}

// --------------------------------------------------------------------------
// -------------------------- TERRITORIES PAGE ------------------------------
// --------------------------------------------------------------------------
void Settings::showTerritoriesPage()
{
    QSettings settings;
    ui->lineEditGoogleAPIKey->setText(settings.value("geo_service_provider/google_api_key", "").toString());
    ui->lineEditHereAppId->setText(settings.value("geo_service_provider/here_app_id", "").toString());
    ui->lineEditHereAppCode->setText(settings.value("geo_service_provider/here_app_code", "").toString());
    ui->comboBoxDefaultGeoService->setCurrentIndex(settings.value("geo_service_provider/default", 0).toInt());
    loadCities();
    loadTerritoryTypes();
    loadStreetTypes();
    loadAddressTypes();
    ui->doubleSpinBoxMarkerScale->setValue(QVariant(sql->getSetting("territory_map_markerscale", "0.5")).toDouble());
    saveTerritoryPage = true;
}

void Settings::loadCities()
{
    citiesCellUpdate = false;
    ui->tableWidgetCities->clear();
    sql_items cities = sql->selectSql("territory_city", "active", "1", "city, lang_id");
    ui->tableWidgetCities->setColumnCount(3);
    ui->tableWidgetCities->setRowCount(static_cast<int>(cities.size()));
    ui->tableWidgetCities->setHorizontalHeaderItem(0, new QTableWidgetItem("id"));
    ui->tableWidgetCities->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("City")));
    ui->tableWidgetCities->setHorizontalHeaderItem(2, new QTableWidgetItem(tr("Language id")));
    if (!cities.empty()) {
        for (unsigned int i = 0; i < cities.size(); i++) {
            sql_item c = cities[i];
            QTableWidgetItem *id = new QTableWidgetItem;
            id->setText(c.value("id").toString());

            QTableWidgetItem *cityName = new QTableWidgetItem;
            cityName->setText(c.value("city").toString());
            //ui->tableWidgetCities->setItem(i,1,cityName);
            //cityName->setFlags(Qt::NoItemFlags);

            QTableWidgetItem *lang_id = new QTableWidgetItem;
            lang_id->setText(c.value("lang_id").toString());
            lang_id->setFlags(Qt::NoItemFlags);

            ui->tableWidgetCities->setColumnHidden(0, true);
            ui->tableWidgetCities->setItem(i, 0, id);
            ui->tableWidgetCities->setItem(i, 1, cityName);
            ui->tableWidgetCities->setItem(i, 2, lang_id);
            ui->tableWidgetCities->setColumnWidth(1, 250);
            ui->tableWidgetCities->setColumnWidth(2, 100);
        }
    }
    citiesCellUpdate = true;
}

void Settings::loadTerritoryTypes()
{
    territoryTypesCellUpdate = false;
    ui->tableWidgetTerritoryTypes->clear();
    sql_items territoryTypes = sql->selectSql("territory_type", "active", "1", "type_name, lang_id");
    ui->tableWidgetTerritoryTypes->setColumnCount(3);
    ui->tableWidgetTerritoryTypes->setRowCount(static_cast<int>(territoryTypes.size()));
    ui->tableWidgetTerritoryTypes->setHorizontalHeaderItem(0, new QTableWidgetItem("id"));
    ui->tableWidgetTerritoryTypes->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("Type")));
    ui->tableWidgetTerritoryTypes->setHorizontalHeaderItem(2, new QTableWidgetItem(tr("Language id")));
    if (!territoryTypes.empty()) {
        for (unsigned int i = 0; i < territoryTypes.size(); i++) {
            sql_item t = territoryTypes[i];
            QTableWidgetItem *id = new QTableWidgetItem;
            id->setText(t.value("id").toString());

            QTableWidgetItem *typeName = new QTableWidgetItem;
            typeName->setText(t.value("type_name").toString());
            //ui->tableWidgetTerritoryTypes->setItem(i,1,typeName);
            //typeName->setFlags(Qt::NoItemFlags);

            QTableWidgetItem *lang_id = new QTableWidgetItem;
            lang_id->setText(t.value("lang_id").toString());
            lang_id->setFlags(Qt::NoItemFlags);

            ui->tableWidgetTerritoryTypes->setColumnHidden(0, true);
            ui->tableWidgetTerritoryTypes->setItem(i, 0, id);
            ui->tableWidgetTerritoryTypes->setItem(i, 1, typeName);
            ui->tableWidgetTerritoryTypes->setItem(i, 2, lang_id);
            ui->tableWidgetTerritoryTypes->setColumnWidth(1, 250);
            ui->tableWidgetTerritoryTypes->setColumnWidth(2, 100);
        }
    }
    territoryTypesCellUpdate = true;
}

void Settings::loadStreetTypes()
{
    streetTypesCellUpdate = false;

    // initialize street types
    cterritories *t = new cterritories;
    t->getStreetTypes();

    ui->tableWidgetStreetTypes->clear();
    sql_items streetTypes = sql->selectSql("territory_streettype", "active", "1", "streettype_name, color");
    ui->tableWidgetStreetTypes->setColumnCount(3);
    ui->tableWidgetStreetTypes->setRowCount(static_cast<int>(streetTypes.size()));
    ui->tableWidgetStreetTypes->setHorizontalHeaderItem(0, new QTableWidgetItem("id"));
    ui->tableWidgetStreetTypes->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("Name")));
    ui->tableWidgetStreetTypes->setHorizontalHeaderItem(2, new QTableWidgetItem(tr("Color")));
    if (!streetTypes.empty()) {
        for (unsigned int i = 0; i < streetTypes.size(); i++) {
            sql_item t = streetTypes[i];

            QTableWidgetItem *id = new QTableWidgetItem;
            id->setText(t.value("id").toString());

            QTableWidgetItem *streetTypeName = new QTableWidgetItem;
            streetTypeName->setText(t.value("streettype_name").toString());

            QTableWidgetItem *color = new QTableWidgetItem;
            color->setData(Qt::DecorationRole, QColor(t.value("color").toString()));
            color->setText(t.value("color").toString());

            ui->tableWidgetStreetTypes->setColumnHidden(0, true);
            ui->tableWidgetStreetTypes->setItem(i, 0, id);
            ui->tableWidgetStreetTypes->setItem(i, 1, streetTypeName);
            ui->tableWidgetStreetTypes->setItem(i, 2, color);
            ui->tableWidgetStreetTypes->setColumnWidth(1, 240);
            ui->tableWidgetStreetTypes->setColumnWidth(2, 100);
        }
    }
    streetTypesCellUpdate = true;
}

void Settings::loadAddressTypes()
{
    addressTypesCellUpdate = false;

    // initialize address types
    cterritories *t = new cterritories;
    t->getAddressTypes();

    QString defaultAddressType = sql->getSetting("territory_default_addresstype");
    ui->comboBoxDefaultAddressType->clear();

    ui->tableWidgetAddressTypes->clear();
    sql_items addressTypes = sql->selectSql("territory_addresstype", "active", "1", "addresstype_number, addresstype_name, color, lang_id");
    ui->tableWidgetAddressTypes->setColumnCount(5);
    ui->tableWidgetAddressTypes->setRowCount(static_cast<int>(addressTypes.size()));
    ui->tableWidgetAddressTypes->setHorizontalHeaderItem(0, new QTableWidgetItem("id"));
    ui->tableWidgetAddressTypes->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("Type")));
    ui->tableWidgetAddressTypes->setHorizontalHeaderItem(2, new QTableWidgetItem(tr("Name")));
    ui->tableWidgetAddressTypes->setHorizontalHeaderItem(3, new QTableWidgetItem(tr("Color")));
    ui->tableWidgetAddressTypes->setHorizontalHeaderItem(4, new QTableWidgetItem(tr("Language id")));
    if (!addressTypes.empty()) {
        for (unsigned int i = 0; i < addressTypes.size(); i++) {
            sql_item t = addressTypes[i];

            QTableWidgetItem *id = new QTableWidgetItem;
            id->setText(t.value("id").toString());

            QTableWidgetItem *addressTypeNumber = new QTableWidgetItem;
            QString currentAddressType = t.value("addresstype_number").toString();
            addressTypeNumber->setText(currentAddressType);

            QTableWidgetItem *addressTypeName = new QTableWidgetItem;
            addressTypeName->setText(t.value("addresstype_name").toString());

            QTableWidgetItem *color = new QTableWidgetItem;
            color->setData(Qt::DecorationRole, QColor(t.value("color").toString()));
            color->setText(t.value("color").toString());

            QTableWidgetItem *lang_id = new QTableWidgetItem;
            lang_id->setText(t.value("lang_id").toString());
            lang_id->setFlags(Qt::NoItemFlags);

            ui->tableWidgetAddressTypes->setColumnHidden(0, true);
            ui->tableWidgetAddressTypes->setItem(i, 0, id);
            ui->tableWidgetAddressTypes->setItem(i, 1, addressTypeNumber);
            ui->tableWidgetAddressTypes->setItem(i, 2, addressTypeName);
            ui->tableWidgetAddressTypes->setItem(i, 3, color);
            ui->tableWidgetAddressTypes->setItem(i, 4, lang_id);
            ui->tableWidgetAddressTypes->setColumnWidth(1, 70);
            ui->tableWidgetAddressTypes->setColumnWidth(2, 240);
            ui->tableWidgetAddressTypes->setColumnWidth(3, 100);
            ui->tableWidgetAddressTypes->setColumnWidth(4, 100);

            // default address type selection
            ui->comboBoxDefaultAddressType->addItem(t.value("addresstype_name").toString(), currentAddressType);
            if (currentAddressType == defaultAddressType) {
                ui->comboBoxDefaultAddressType->setCurrentIndex(i);
            }
        }
    }
    addressTypesCellUpdate = true;
}

void Settings::on_tableWidgetCities_cellChanged(int row, int column)
{
    if (!citiesCellUpdate)
        return;
    QString id = ui->tableWidgetCities->item(row, 0)->text();
    sql_item updateItem;
    switch (column) {
    case 1:
        updateItem.insert("city", ui->tableWidgetCities->item(row, column)->text());
        break;
    }

    sql->updateSql("territory_city", "id", id, &updateItem);
    qDebug() << "Cell updated to database (" + id + ")";
}

void Settings::on_toolButtonAddCity_clicked()
{
    if (ui->lineEditNewCity->text() == "") {
        QMessageBox::information(this, "TheocBase", tr("City name missing"));
        return;
    }

    int defaultlang = sql->getLanguageDefaultId();

    // add new city
    sql_items e = sql->selectSql("SELECT * FROM territory_city WHERE city = '" + ui->lineEditNewCity->text() + "' AND lang_id = '" + QVariant(defaultlang).toString() + "' AND active");
    if (!e.empty()) {
        QMessageBox::information(this, "TheocBase", tr("City is already saved!"));
        return;
    }

    sql_item nc;
    nc.insert("city", ui->lineEditNewCity->text());
    nc.insert("lang_id", defaultlang);
    if (sql->insertSql("territory_city", &nc, "id") != -1) {
        QMessageBox::information(this, "TheocBase", tr("City added to database"));
        // update list
        loadCities();
        // clear controls
        ui->lineEditNewCity->clear();
    } else {
        QMessageBox::warning(this, "TheocBase", tr("Adding failed"));
    }
}

void Settings::on_toolButtonRemoveCity_clicked()
{
    // remove selected row from cities
    if (ui->tableWidgetCities->selectedItems().count() < 1)
        return;

    QList<QTableWidgetItem *> rowList = ui->tableWidgetCities->selectedItems();

    if (QMessageBox::question(this, "",
                              tr("Remove selected row?"),
                              QMessageBox::No, QMessageBox::Yes)
        == QMessageBox::No)
        return;

    for (int i = rowList.count() - 1; i >= 0; i--) {
        sql_item s;
        s.insert("active", 0);
        if (sql->updateSql("territory_city", "id", ui->tableWidgetCities->item(rowList.at(i)->row(), 0)->text(), &s))
            ui->tableWidgetCities->removeRow(rowList.at(i)->row());
    }
}

void Settings::on_tableWidgetTerritoryTypes_cellChanged(int row, int column)
{
    if (!territoryTypesCellUpdate)
        return;
    QString id = ui->tableWidgetTerritoryTypes->item(row, 0)->text();
    sql_item updateItem;
    switch (column) {
    case 1:
        updateItem.insert("type_name", ui->tableWidgetTerritoryTypes->item(row, column)->text());
        break;
    }

    sql->updateSql("territory_type", "id", id, &updateItem);
    qDebug() << "Cell updated to database (" + id + ")";
}

void Settings::on_toolButtonAddTerritoryType_clicked()
{
    if (ui->lineEditNewTerritoryType->text() == "") {
        QMessageBox::information(this, "TheocBase", tr("Territory type name missing"));
        return;
    }

    int defaultlang = sql->getLanguageDefaultId();

    // add new territory type
    sql_items e = sql->selectSql("SELECT * FROM territory_type WHERE type_name = '" + ui->lineEditNewTerritoryType->text() + "' AND lang_id = '" + QVariant(defaultlang).toString() + "' AND active");
    if (!e.empty()) {
        QMessageBox::information(this, "TheocBase", tr("Territory type is already saved!"));
        return;
    }

    sql_item nt;
    nt.insert("type_name", ui->lineEditNewTerritoryType->text());
    nt.insert("lang_id", defaultlang);
    if (sql->insertSql("territory_type", &nt, "id") != -1) {
        QMessageBox::information(this, "TheocBase", tr("Territory type added to database"));
        // update list
        loadTerritoryTypes();
        // clear controls
        ui->lineEditNewTerritoryType->clear();
    } else {
        QMessageBox::warning(this, "TheocBase", tr("Adding failed"));
    }
}

void Settings::on_toolButtonRemoveTerritoryType_clicked()
{
    // remove selected row from territory types
    if (ui->tableWidgetTerritoryTypes->selectedItems().count() < 1)
        return;

    QList<QTableWidgetItem *> rowList = ui->tableWidgetTerritoryTypes->selectedItems();

    if (QMessageBox::question(this, "",
                              tr("Remove selected row?"),
                              QMessageBox::No, QMessageBox::Yes)
        == QMessageBox::No)
        return;

    for (int i = rowList.count() - 1; i >= 0; i--) {
        sql_item s;
        s.insert("active", 0);
        if (sql->updateSql("territory_type", "id", ui->tableWidgetTerritoryTypes->item(rowList.at(i)->row(), 0)->text(), &s))
            ui->tableWidgetTerritoryTypes->removeRow(rowList.at(i)->row());
    }
}

void Settings::on_tableWidgetStreetTypes_cellChanged(int row, int column)
{
    if (!streetTypesCellUpdate)
        return;
    QString id = ui->tableWidgetStreetTypes->item(row, 0)->text();
    sql_item updateItem;
    switch (column) {
    case 1:
        updateItem.insert("streettype_name", ui->tableWidgetStreetTypes->item(row, column)->text());
        break;
    case 2: {
        QColor color = QColor(ui->tableWidgetStreetTypes->item(row, column)->text());
        ui->tableWidgetStreetTypes->blockSignals(true);
        ui->tableWidgetStreetTypes->item(row, column)->setData(Qt::DecorationRole, color);
        ui->tableWidgetStreetTypes->item(row, column)->setData(Qt::DisplayRole, color.name(QColor::HexRgb));
        ui->tableWidgetStreetTypes->blockSignals(false);
        updateItem.insert("color", color.name(QColor::HexRgb));
        break;
    }
    }

    sql->updateSql("territory_streettype", "id", id, &updateItem);
    qDebug() << "Cell updated to database (" + id + ")";
}

void Settings::on_pushButtonNewStreetTypeColor_clicked()
{
    QColor color = QColorDialog::getColor(Qt::black, this);
    if (color.isValid()) {
        ui->pushButtonNewStreetTypeColor->setStyleSheet("background-color:" + color.name());
        ui->pushButtonNewStreetTypeColor->setText(color.name());
    }
}

void Settings::on_toolButtonAddStreetType_clicked()
{
    if (ui->lineEditNewStreetTypeName->text() == "") {
        QMessageBox::information(this, "TheocBase", tr("Name of the street type is missing"));
        return;
    }

    // add new address type
    sql_items e = sql->selectSql("SELECT * FROM territory_streettype WHERE streettype_name = '" + ui->lineEditNewStreetTypeName->text() + "' AND active");
    if (!e.empty()) {
        QMessageBox::information(this, "TheocBase", tr("Street type is already saved!"));
        return;
    }

    sql_item nt;
    nt.insert("streettype_name", ui->lineEditNewStreetTypeName->text());
    nt.insert("color", ui->pushButtonNewStreetTypeColor->text());
    if (sql->insertSql("territory_streettype", &nt, "id") != -1) {
        QMessageBox::information(this, "TheocBase", tr("Street type added to database"));
        // update list
        loadStreetTypes();
        // clear controls
        ui->lineEditNewStreetTypeName->clear();
    } else {
        QMessageBox::warning(this, "TheocBase", tr("Adding failed"));
    }
}

void Settings::on_toolButtonRemoveStreetType_clicked()
{
    // remove selected row from street types
    if (ui->tableWidgetStreetTypes->selectedItems().count() < 1)
        return;

    QList<QTableWidgetItem *> rowList = ui->tableWidgetStreetTypes->selectedItems();

    if (QMessageBox::question(this, "",
                              tr("Remove selected row?"),
                              QMessageBox::No, QMessageBox::Yes)
        == QMessageBox::No)
        return;

    for (int i = rowList.count() - 1; i >= 0; i--) {
        sql_item s;
        s.insert("active", 0);
        if (sql->updateSql("territory_streettype", "id", ui->tableWidgetStreetTypes->item(rowList.at(i)->row(), 0)->text(), &s))
            ui->tableWidgetStreetTypes->removeRow(rowList.at(i)->row());
    }
}

void Settings::on_tableWidgetAddressTypes_cellChanged(int row, int column)
{
    if (!addressTypesCellUpdate)
        return;
    QString id = ui->tableWidgetAddressTypes->item(row, 0)->text();
    sql_item updateItem;
    switch (column) {
    case 1:
        updateItem.insert("addresstype_number", ui->tableWidgetAddressTypes->item(row, column)->text());
        break;
    case 2:
        updateItem.insert("addresstype_name", ui->tableWidgetAddressTypes->item(row, column)->text());
        break;
    case 3: {
        QColor color = QColor(ui->tableWidgetAddressTypes->item(row, column)->text());
        ui->tableWidgetAddressTypes->blockSignals(true);
        ui->tableWidgetAddressTypes->item(row, column)->setData(Qt::DecorationRole, color);
        ui->tableWidgetAddressTypes->item(row, column)->setData(Qt::DisplayRole, color.name(QColor::HexRgb));
        ui->tableWidgetAddressTypes->blockSignals(false);
        updateItem.insert("color", color.name(QColor::HexRgb));
        break;
    }
    }

    sql->updateSql("territory_addresstype", "id", id, &updateItem);
    qDebug() << "Cell updated to database (" + id + ")";
}

void Settings::on_pushButtonNewAddressTypeColor_clicked()
{
    QColor color = QColorDialog::getColor(Qt::black, this);
    if (color.isValid()) {
        ui->pushButtonNewAddressTypeColor->setStyleSheet("background-color:" + color.name());
        ui->pushButtonNewAddressTypeColor->setText(color.name());
    }
}

void Settings::on_toolButtonAddAddressType_clicked()
{
    if (ui->lineEditNewAddressTypeNumber->text() == "") {
        QMessageBox::information(this, "TheocBase", tr("Number of address type is missing"));
        return;
    }
    if (ui->lineEditNewAddressTypeName->text() == "") {
        QMessageBox::information(this, "TheocBase", tr("Name of address type is missing"));
        return;
    }

    int defaultlang = sql->getLanguageDefaultId();

    // add new address type
    sql_items e = sql->selectSql("SELECT * FROM territory_addresstype WHERE addresstype_number = '" + ui->lineEditNewAddressTypeNumber->text() + "' AND lang_id = '" + QVariant(defaultlang).toString() + "' AND active");
    if (!e.empty()) {
        QMessageBox::information(this, "TheocBase", tr("Address type is already saved!"));
        return;
    }

    sql_item nt;
    nt.insert("addresstype_number", ui->lineEditNewAddressTypeNumber->text());
    nt.insert("addresstype_name", ui->lineEditNewAddressTypeName->text());
    nt.insert("color", ui->pushButtonNewAddressTypeColor->text());
    nt.insert("lang_id", defaultlang);
    if (sql->insertSql("territory_addresstype", &nt, "id") != -1) {
        QMessageBox::information(this, "TheocBase", tr("Address type added to database"));
        // update list
        loadAddressTypes();
        // clear controls
        ui->lineEditNewAddressTypeName->clear();
        ui->lineEditNewAddressTypeNumber->clear();
    } else {
        QMessageBox::warning(this, "TheocBase", tr("Adding failed"));
    }
}

void Settings::on_toolButtonRemoveAddressType_clicked()
{
    // remove selected row from address types
    if (ui->tableWidgetAddressTypes->selectedItems().count() < 1)
        return;

    QList<QTableWidgetItem *> rowList = ui->tableWidgetAddressTypes->selectedItems();

    if (QMessageBox::question(this, "",
                              tr("Remove selected row?"),
                              QMessageBox::No, QMessageBox::Yes)
        == QMessageBox::No)
        return;

    for (int i = rowList.count() - 1; i >= 0; i--) {
        sql_item s;
        s.insert("active", 0);
        if (sql->updateSql("territory_addresstype", "id", ui->tableWidgetAddressTypes->item(rowList.at(i)->row(), 0)->text(), &s))
            ui->tableWidgetAddressTypes->removeRow(rowList.at(i)->row());
    }
}

// --------------------------------------------------------------------------
// --------------------------- REMINDERS PAGE -------------------------------
// --------------------------------------------------------------------------
void Settings::showReminderPage()
{
    if (saveRemindersPage)
        return;
    ui->chkSendReminders->setChecked(QVariant(sql->getSetting("reminder_send_onclosing")).toBool());
    ui->lineEditEmailFromAddress->setText(sql->getSetting("email_from_address"));
    ui->lineEditEmailFromName->setText(sql->getSetting("email_from_name"));

    QSettings settings;

    emailAccounts.clear();
    emailAccounts
            << EmailAccount("Gmail", "smtp.gmail.com", 465, SmtpClient::SslConnection)
            << EmailAccount("iCloud", "smtp.mail.me.com", 587, SmtpClient::TlsConnection)
            << EmailAccount("Microsoft 365", "smtp.office365.com", 587, SmtpClient::TlsConnection)
            << EmailAccount("Outlook", "smtp-mail.outlook.com", 587, SmtpClient::TlsConnection);

    ui->comboEmailAccounts->clear();
    ui->comboEmailAccounts->addItem("");

    QString host = settings.value("email/smtphost", "smtp.example.com").toString();
    for (int i = 0; i < emailAccounts.count(); i++) {
        ui->comboEmailAccounts->addItem(emailAccounts[i].name);

        if (emailAccounts[i].host == host)
            ui->comboEmailAccounts->setCurrentIndex(i + 1);
    }

    ui->lineEditEmailSMTPServer->setText(host);
    ui->lineEditEmailSMTPPort->setText(settings.value("email/smtpport", 465).toString());
    ui->comboEmailConnType->setCurrentIndex(settings.value("email/smtpconntype", 1).toInt());
    SimpleCrypt crypt;
    crypt.setKey(constants::smtppasswd());
    ui->lineEditEmailSMTPUsername->setText(crypt.decryptToString(settings.value("email/smtpusername", "").toString()));
    ui->lineEditEmailSMTPPasswd->setText(crypt.decryptToString(settings.value("email/smtppassword", "").toString()));

    saveRemindersPage = true;
}

// reminders - button for test smtp server
void Settings::on_pushButtonEmailTest_clicked()
{
    SmtpClient::ConnectionType c = SmtpClient::TcpConnection;
    switch (ui->comboEmailConnType->currentIndex()) {
    case 0:
        c = SmtpClient::TcpConnection;
        break;
    case 1:
        c = SmtpClient::SslConnection;
        break;
    case 2:
        c = SmtpClient::TlsConnection;
        break;
    }

    SmtpClient smtp(ui->lineEditEmailSMTPServer->text(),
                    ui->lineEditEmailSMTPPort->text().toInt(), c);
    qDebug() << "server:" << ui->lineEditEmailSMTPServer->text()
             << "port:" << ui->lineEditEmailSMTPPort->text().toInt()
             << "conntype:" << c;
    smtp.setUser(ui->lineEditEmailSMTPUsername->text());
    qDebug() << "user:" << ui->lineEditEmailSMTPUsername->text();
    smtp.setPassword(ui->lineEditEmailSMTPPasswd->text());
    qDebug() << "password:" << ui->lineEditEmailSMTPPasswd->text();

    bool ok = smtp.connectToHost();
    connect(&smtp, &SmtpClient::smtpError, [](SmtpClient::SmtpError e) {
        qDebug() << "smtp error" << e;
    });
    qDebug() << "connected to host" << ok;
    if (ok)
        ok = smtp.login(smtp.getUser(), smtp.getPassword(), SmtpClient::AuthLogin);
    smtp.quit();

    if (ok)
        QMessageBox::information(this, "TheocBase", "OK");
    else
        QMessageBox::warning(this, "TheocBase", tr("Error sending e-mail"));
}

void Settings::on_comboEmailAccounts_activated(int index)
{
    EmailAccount selected = emailAccounts[index - 1];
    ui->lineEditEmailSMTPServer->setText(selected.host);
    ui->lineEditEmailSMTPPort->setText(QString::number(selected.port));
    switch(selected.connType) {
    case SmtpClient::TcpConnection:
        ui->comboEmailConnType->setCurrentIndex(0);
        break;
    case SmtpClient::SslConnection:
        ui->comboEmailConnType->setCurrentIndex(1);
        break;
    case SmtpClient::TlsConnection:
        ui->comboEmailConnType->setCurrentIndex(2);
        break;
    }
}

void Settings::on_btnImportLMM_clicked()
{
    if (QGuiApplication::keyboardModifiers() & Qt::ControlModifier) {
        // back door to enter regex editor
        lmmWorksheetRegEx *dialog = new lmmWorksheetRegEx(this);
        dialog->show();
        dialog->exec();
        return;
    } else if (QGuiApplication::keyboardModifiers() & Qt::ShiftModifier) {
        // export most recent assist records - all in one file
        {
            QString assist(sql->databasepath.mid(0, sql->databasepath.lastIndexOf('/')) + "/lmmassist.txt");
            QFile file(assist);
            if (file.open(QIODevice::ReadWrite)) {
                QTextStream stream(&file);
                sql_item param;
                param.insert(":date", QDate::currentDate().addMonths(-12).toString("yyyy-MM-dd"));
                sql_items data = sql->selectSql("select * from lmm_schedule_assist where date > :date order by date, position", &param);
                for (sql_item row : data) {
                    stream << row.value("date").toString() << '\t' << row.value("position").toString() << '\t' << row.value("talk_id").toString() << "\n";
                }
                file.close();
            }
        }

        // export most recent assist records
        {
            QFile *file(0);
            QTextStream *stream(0);
            sql_item param;
            param.insert(":date", QDate::currentDate().addMonths(-12).toString("yyyy-MM-dd"));
            sql_items data = sql->selectSql("select * from lmm_schedule_assist where date > :date order by date, position", &param);
            QDate lastDt;
            for (sql_item row : data) {
                QDate dt(row.value("date").toDate());
                if (!lastDt.isValid() || lastDt.year() != dt.year() || lastDt.month() != dt.month()) {
                    if (file) {
                        delete stream;
                        file->close();
                        delete file;
                    }
                    QString assist(sql->databasepath.mid(0, sql->databasepath.lastIndexOf('/')) + "/lmmassist" + dt.toString("yyMM") + ".txt");
                    file = new QFile(assist);
                    if (!file->open(QIODevice::ReadWrite)) {
                        QMessageBox::information(0, "TheocBase", "Could not create lmmassist.txt");
                        delete file;
                        return;
                    }
                    stream = new QTextStream(file);
                }
                (*stream) << dt.toString("yyyy-MM-dd") << '\t' << row.value("position").toString() << '\t' << row.value("talk_id").toString() << "\n";
                lastDt = dt;
            }

            if (file) {
                delete stream;
                file->close();
                delete file;
                QMessageBox::information(0, "TheocBase", "lmmassist files created created");
            }
        }
        return;
    }

    QString filepath = QFileDialog::getOpenFileName(this, tr("Select ePub file"),
                                                    QStandardPaths::writableLocation(QStandardPaths::DesktopLocation),
                                                    "ePub -file (*.epub)");

    if (filepath != "") {
        QSharedPointer<importlmmworkbook> mwb(new importlmmworkbook(filepath));
        QString results = mwb->Import();
        while (results.startsWith("#ERR#")) {
            lmmtalktypeedit talktypeedit;
            talktypeedit.Init();
            if (talktypeedit.exec() == QDialog::Rejected)
                break;
            mwb.reset(new importlmmworkbook(filepath));
            results = mwb->Import();
        }
        results = results.replace("#ERR#", "");
        ui->widgetLMMTalkTypes->Init(false);

        QMessageBox::information(this, "", results);
    }
}

void Settings::on_btnCopyTMStoLMM_clicked()
{
    QMessageBox::information(this, "", tr("This is no longer an option. Please request help in the forum if this is needed."));
}

void Settings::on_btnImportSQL_clicked()
{
    QMessageBox::StandardButton ok = QMessageBox::information(this, "", tr("Warning: Make sure this file comes from a trusted source. Continue?"), QMessageBox::Yes | QMessageBox::No, QMessageBox::No);
    if (ok != QMessageBox::Yes)
        return;

    QString sqlFileName = QFileDialog::getOpenFileName(this, tr("Command File"),
                                                       QDir::homePath(),
                                                       "Command File (*.txt)");
    if (sqlFileName == "")
        return;

    QFile sqlFile(sqlFileName);
    sqlFile.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream stream(&sqlFile);
    QString results;
    bool hasReturnData(false);
    while (!stream.atEnd()) {
        QString cmd(stream.readLine());

        QPair<sql_items, QList<QString>> dbResults = sql->selectSql2(cmd, 0);
        sql_items data(dbResults.first);
        QList<QString> cols(dbResults.second);
        bool isFirst(true);
        for (sql_item row : data) {
            if (isFirst) {
                for (QString header : cols) {
                    if (isFirst)
                        isFirst = false;
                    else
                        results.append("\t");
                    results.append(header);
                }
                results.append("\r\n");
            }
            isFirst = true;
            for (QString header : cols) {
                if (isFirst) {
                    isFirst = false;
                    hasReturnData = true;
                } else
                    results.append("\t");
                results.append(row[header].toString());
            }
            results.append("\r\n");
        }
        results.append("\r\n");
    }
    sqlFile.close();

    if (hasReturnData) {
        QClipboard *clipboard = QApplication::clipboard();
        clipboard->setText(results);
        QMessageBox::information(this, "TheocBase", "Results in Clipboard");
    } else {
        QMessageBox::information(this, "TheocBase", "If you saw no errors, then the command completed successfully");
    }
}

// --------------------------------------------------------------------------
// ------------------------- Access Control PAGE ----------------------------
// --------------------------------------------------------------------------

void Settings::showAccessControlPage()
{
    if (saveAccessControlPage)
        return;
    loadUsers();
    loadRules();
    saveAccessControlPage = true;
}

void Settings::loadUsers()
{
    usersCellUpdate = false;
    ui->tableWidgetUsers->clear();

    bool isReadOnly = !ac->user()->hasPermission(Permission::Rule::CanEditPermissions);

    const QList<Role> *roles = &ac->roles();

    QProgressDialog progress("Loading...", "", 0, 0, this);
    progress.setCancelButton(nullptr);
    progress.setWindowModality(Qt::WindowModal);
    progress.show();
    User *currentUser = ac->user();
    QList<User *> *users = &ac->users();
    if (currentUser && cloud->logged() && currentUser->hasPermission(Permission::Rule::CanEditPermissions)) {
        // admin
        cloud->loadAccessControl();
        QFileInfo fi = cloud->authentication()->getAccountInfo()->getSyncFile();
        // get dropbox folder members
        QList<DBUser> dbUsers = cloud->authentication()->listFolderMembers(fi.path());
        // check the users
        for (auto dbUsr : dbUsers) {
            if (ac->findUser(dbUsr.email) == nullptr)
                ac->addUser(dbUsr.displayName, dbUsr.email);
        }
        QList<User *> templist = ac->users();
        for (int i = 0; i < templist.size(); i++) {
            QString e = templist[i]->email();
            if (std::find_if(dbUsers.constBegin(), dbUsers.constEnd(), [&e](const DBUser &dbu) { return !dbu.email.compare(e); }) == dbUsers.constEnd()) {
                if (e.compare(currentUser->email(), Qt::CaseInsensitive) != 0)
                    ac->removeUser(templist[i]->id());
            }
        }
    }

    ui->tableWidgetUsers->setColumnCount(roles->size() + 2);
    ui->tableWidgetUsers->setRowCount(users->size());
    ui->tableWidgetUsers->setHorizontalHeaderItem(0, new QTableWidgetItem("id"));
    ui->tableWidgetUsers->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("Name")));
    for (qint32 i = 0, count = roles->size(); i < count; i++) {
        ui->tableWidgetUsers->setHorizontalHeaderItem(i + 2, new QTableWidgetItem(Permission::toString(roles->at(i).id())));
    }
    if (!users->empty()) {
        for (int i = 0; i < users->size(); i++) {
            const User *u = users->at(i);
            QTableWidgetItem *idWidgetItem = new QTableWidgetItem;
            idWidgetItem->setText(QVariant(u->id()).toString());
            idWidgetItem->setFlags(Qt::NoItemFlags);

            QTableWidgetItem *userNameWidgetItem = new QTableWidgetItem;
            userNameWidgetItem->setText(u->name());
            userNameWidgetItem->setToolTip(u->email());
            userNameWidgetItem->setFlags(userNameWidgetItem->flags() & ~Qt::ItemIsEditable);

            ui->tableWidgetUsers->setColumnHidden(0, true);
            ui->tableWidgetUsers->setItem(i, 0, idWidgetItem);
            ui->tableWidgetUsers->setItem(i, 1, userNameWidgetItem);
            ui->tableWidgetUsers->setColumnWidth(1, 250);

            for (qint32 j = 0, count = roles->size(); j < count; j++) {
                Role r = roles->at(j);
                bool hasRole = u->hasRole(r);

                int index = i * roles->size() + j;
                QWidget *checkBoxWidget = new QWidget();
                QCheckBox *checkBox = new QCheckBox();
                checkBox->setCheckState(hasRole ? (Qt::Checked) : (Qt::Unchecked));
                // set read-only
                if (isReadOnly || r.id() == Permission::RoleId::Publisher) {
                    checkBox->setAttribute(Qt::WA_TransparentForMouseEvents);
                    checkBox->setFocusPolicy(Qt::NoFocus);
                }
                connect(checkBox, &QCheckBox::clicked, [=] { on_userRoleAssignmentWidget_stateChanged(index); });

                QHBoxLayout *layoutCheckBox = new QHBoxLayout(checkBoxWidget);
                layoutCheckBox->addWidget(checkBox);
                layoutCheckBox->setAlignment(Qt::AlignCenter);
                layoutCheckBox->setContentsMargins(0, 0, 0, 0);

                ui->tableWidgetUsers->setCellWidget(i, j + 2, checkBoxWidget);
            }
        }
    }
    progress.close();
    usersCellUpdate = true;
}

void Settings::loadRules()
{
    rulesCellUpdate = false;
    ui->tableWidgetRules->clear();
    const QList<Role> *roles = &ac->roles();
    ui->tableWidgetRules->setColumnCount(roles->size() + 2);
    ui->tableWidgetRules->setHorizontalHeaderItem(0, new QTableWidgetItem("id"));
    ui->tableWidgetRules->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("Name")));

    QMetaEnum metaEnum = QMetaEnum::fromType<Permission::Rule>();
    ui->tableWidgetRules->setRowCount(metaEnum.keyCount());

    ui->tableWidgetRules->setHorizontalHeaderItem(0, new QTableWidgetItem("id"));
    for (qint32 i = 0, count = roles->size(); i < count; i++) {
        ui->tableWidgetRules->setHorizontalHeaderItem(i + 2, new QTableWidgetItem(Permission::toString(roles->at(i).id())));
    }

    for (qint32 i = 0, count = metaEnum.keyCount(); i < count; i++) {
        QTableWidgetItem *idWidgetItem = new QTableWidgetItem;
        idWidgetItem->setText(QVariant(metaEnum.key(i)).toString());
        idWidgetItem->setFlags(Qt::NoItemFlags);

        QTableWidgetItem *ruleNameWidgetItem = new QTableWidgetItem;
        Permission::Rule rule(Permission::Rule(metaEnum.keyToValue(metaEnum.key(i))));
        ruleNameWidgetItem->setText(Permission::toString(rule));
        ruleNameWidgetItem->setFlags(ruleNameWidgetItem->flags() & ~Qt::ItemIsEditable);

        ui->tableWidgetRules->setColumnHidden(0, true);
        ui->tableWidgetRules->setItem(i, 0, idWidgetItem);
        ui->tableWidgetRules->setItem(i, 1, ruleNameWidgetItem);
        ui->tableWidgetRules->setColumnWidth(1, 250);

        for (qint32 j = 0, count = roles->size(); j < count; j++) {
            Role r = roles->at(j);
            bool hasPermission = r.hasPermission(rule);

            QWidget *checkBoxWidget = new QWidget();
            QCheckBox *checkBox = new QCheckBox();
            checkBox->setCheckState(hasPermission ? (Qt::Checked) : (Qt::Unchecked));
            // set read-only
            checkBox->setAttribute(Qt::WA_TransparentForMouseEvents);
            checkBox->setFocusPolicy(Qt::NoFocus);

            QHBoxLayout *layoutCheckBox = new QHBoxLayout(checkBoxWidget);
            layoutCheckBox->addWidget(checkBox);
            layoutCheckBox->setAlignment(Qt::AlignCenter);
            layoutCheckBox->setContentsMargins(0, 0, 0, 0);

            ui->tableWidgetRules->setCellWidget(i, 2 + j, checkBoxWidget);
        }
    }

    rulesCellUpdate = true;
}

void Settings::on_toolButtonAddUser_clicked()
{
    if (ui->lineEditNewUserName->text() != "" && ui->lineEditNewUserEmail->text() != "") {
        if (ac->findUser(ui->lineEditNewUserEmail->text())) {
            QMessageBox::information(nullptr, "Access Control",
                                     QObject::tr("A user with the same E-mail address has already been added."));
            return;
        }
        if (ac->addUser(ui->lineEditNewUserName->text(), ui->lineEditNewUserEmail->text())) {
            loadUsers();
            ui->lineEditNewUserName->clear();
            ui->lineEditNewUserEmail->clear();
        }
    }
}

void Settings::on_toolButtonRemoveUser_clicked()
{
    int row(ui->tableWidgetUsers->currentRow());
    if (row < 0 || QMessageBox::question(this, tr("Access Control"), tr("Remove permissions for the selected user?"), QMessageBox::No, QMessageBox::Yes) == QMessageBox::No)
        return;

    int id = ui->tableWidgetUsers->item(row, 0)->text().toInt();
    ac->removeUser(id);
    loadUsers();
}

void Settings::on_userRoleAssignmentWidget_stateChanged(int index)
{
    int row = index / (ui->tableWidgetUsers->columnCount() - 2);
    int col = index % (ui->tableWidgetUsers->columnCount() - 2);

    const QList<Role> *roles = &ac->roles();
    QList<User *> *users = &ac->users();
    const Role *role = &roles->at(col);
    User *user = (*users)[row];

    QWidget *widget = qobject_cast<QWidget *>(ui->tableWidgetUsers->cellWidget(row, col + 2));
    if (widget) {
        for (qint32 i = 0, count = widget->children().count(); i < count; i++) {
            QCheckBox *cellCheckBox = qobject_cast<QCheckBox *>(widget->children().at(i));
            if (cellCheckBox) {
                Qt::CheckState state = cellCheckBox->checkState();
                if (state) {
                    user->addRole(role);
                } else {
                    user->removeRole(role);
                }
            }
        }
    }
}

// --------------------------------------------------------------------------
// --------------------------- LMM PAGE -------------------------------
// --------------------------------------------------------------------------

void Settings::showLMMPage()
{
    if (saveLMMPage)
        return;
    // Set quantity of classes
    lmmYear = QDate::currentDate().year();
    int minLMMYear(lmmYear - 2);
    if (minLMMYear < 2016)
        minLMMYear = 2016;
    int maxLMMYear(lmmYear + 1);
    int idx = 0;
    nextLMMDate.setDate(2016, 1, 4);
    ui->cboLMMYear->clear();
    ui->cboLMMYear->addItem("");
    for (int y = minLMMYear; y <= maxLMMYear; y++, idx++) {
        ui->cboLMMYear->addItem(QString::number(y));
        if (y == lmmYear) {
            ui->cboLMMYear->setCurrentIndex(idx + 1);
            loadLMMMeeting();
        }
    }
    ui->errLMMMeeting->setText("");
    ui->errLMMSchedule->setText("");
    LMM_Meeting m;
    ui->spinBoxClassCount->setValue(m.classes());
    loadSchoolStudies();
    ui->widgetLMMTalkTypes->Init(false);
    saveLMMPage = true;
}

void Settings::reloadSettings()
{
    saveGeneralPage = false;
    saveExceptionPage = false;
    saveLMMPage = false;
    savePublicTalkPage = false;
    saveSongPage = false;
    saveTerritoryPage = false;
    saveRemindersPage = false;
    switch (ui->listWidget->currentRow()) {
    case 0:
        showGeneralPage();
        break;
    case 1:
        showExceptionsPage();
        break;
    case 2:
        showLMMPage();
        break;
    case 3:
        showPublicTalkPage();
        break;
    case 4:
        showSongPage();
        break;
    case 5:
        showTerritoriesPage();
        break;
    case 6:
        showReminderPage();
        break;
    case 7:
        // access control
        break;
    }
}

void Settings::addMidweekMeetingSchedule(QAction *action)
{
    QDate dt = action->property("date").toDate();
    if (!dt.isValid())
        return;
    // second stage of on_btnAddLMMMeeting_clicked()
    LMM_Meeting mtg(nullptr, true);
    mtg.loadMeeting(dt);
    mtg.save();
    mtg.createAssignments();
    loadLMMMeeting();
    if (lmmdateToIdx.contains(dt))
        ui->gridLMMMeeting->selectRow(lmmdateToIdx[dt]);
}

void Settings::on_cboLMMYear_activated(const QString &arg1)
{
    lmmYear = arg1.toInt();
    loadLMMMeeting();
}

void Settings::on_btnAddLMMMeeting_clicked()
{
    auto menu = ui->btnAddLMMMeeting->menu();
    if (menu == nullptr) {
        menu = new QMenu(this);
        connect(menu, &QMenu::triggered, this, &Settings::addMidweekMeetingSchedule);
    }
    menu->clear();

    QString sYear = ui->cboLMMYear->currentText();
    int year = QVariant(sYear).toInt();
    if (year < 2016)
        return;
    QDate d = QDate(year, 1, 1);
    d = d.addDays((d.dayOfWeek() - 1) * -1);
    if (d.year() < year)
        d = d.addDays(7);
    int month = d.month();
    auto subMenu = menu->addMenu(QLocale().standaloneMonthName(month));
    do {
        if (d.month() > month) {
            subMenu = menu->addMenu(QLocale().standaloneMonthName(d.month()));
            month = d.month();
        }
        auto item = new QAction(QLocale().toString(d, QLocale::ShortFormat));
        item->setProperty("date", d);
        subMenu->addAction(item);

        d = d.addDays(7);
    } while (d.year() == year);

    QPoint p = ui->btnAddLMMMeeting->geometry().bottomRight();
    p.setX(p.x() - ui->btnAddLMMMeeting->width());
    menu->popup(ui->btnAddLMMMeeting->parentWidget()->mapToGlobal(p));
}

void Settings::on_btnRemoveLMMMeeting_clicked()
{
    int row(ui->gridLMMMeeting->currentRow());
    if (row < 0 || QMessageBox::question(this, tr("Meeting"), tr("Remove the whole meeting? (Use only to remove invalid data from database)"), QMessageBox::No, QMessageBox::Yes) == QMessageBox::No)
        return;

    QString dt = ui->gridLMMMeeting->item(row, 0)->text();
    sql_item s;
    s.insert("date", dt);
    sql->execSql("update lmm_meeting set active = 0, time_stamp = strftime('%s','now') where date = :date", &s, true);
    sql->execSql("update lmm_schedule set active = 0, time_stamp = strftime('%s','now') where date = :date", &s, true);
    loadLMMMeeting();
}

void Settings::on_btnAddLMMSchedule_clicked()
{
    if (!currentLMMMeeting.date().isValid())
        return;

    QList<LMM_Assignment *> assignments = currentLMMMeeting.getAssignments();
    LMM_Schedule s;
    QList<int> expected = s.getExpectedTalks(currentLMMMeeting.date());
    int targetDbTalkID(-1);
    int targetTalkID(-1);
    int targetSequence(-1);
    for (int t : expected) {
        bool found(false);
        for (LMM_Assignment *a : assignments) {
            if (t == a->dbTalkId()) {
                found = true;
                break;
            }
        }
        if (!found) {
            targetDbTalkID = t;
            break;
        }
    }
    if (targetDbTalkID == -1) {
        QMessageBox::information(this, "TheocBase",
                                 QObject::tr("All talks have been added to this week"));
        return;
    }
    LMM_Schedule::splitDbTalkId(targetDbTalkID, targetTalkID, targetSequence);
    LMM_Schedule sch(targetTalkID, targetSequence, currentLMMMeeting.date(), LMM_Schedule::getFullStringTalkType(targetTalkID), tr("Enter source material here"), 1); //!talkid refactor once we initialize with dbTalkId
    sch.save();
    loadLMMSchedule();
}

void Settings::on_btnRemoveLMMSchedule_clicked()
{
    int row(ui->gridLMMSchedule->currentRow());
    if (row < 0 || QMessageBox::question(this, tr("Meeting"), tr("Remove this talk? (Use only to remove invalid data from database)"), QMessageBox::No, QMessageBox::Yes) == QMessageBox::No)
        return;

    int scheduleId = ui->gridLMMSchedule->item(row, 0)->data(Qt::UserRole).toInt();
    sql->execSql("update lmm_schedule set active = 0,time_stamp = strftime('%s','now') where id = " + QString::number(scheduleId));
    loadLMMSchedule();
}

void Settings::on_gridLMMMeeting_currentItemChanged(QTableWidgetItem *, QTableWidgetItem *)
{
    qDebug() << "on_gridLMMMeeting_currentItemChanged";
    loadLMMSchedule();
}

void Settings::on_gridLMMMeeting_itemSelectionChanged()
{
    ui->btnRemoveLMMMeeting->setEnabled(ui->gridLMMMeeting->selectedItems().count() > 0);
}

void Settings::loadLMMMeeting()
{
    this->setCursor(Qt::WaitCursor);
    ui->gridLMMMeeting->blockSignals(true);
    ui->gridLMMMeeting->clear();

    QStringList lmmMeetingErrs;
    QString select("select date from lmm_meeting where date like '%1%' and active order by date");
    sql_items dates = sql->selectSql(select.arg(QString::number(lmmYear))); //,sql.arg(QString::number(lmmYear))));

    ui->gridLMMMeeting->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->gridLMMMeeting->setSelectionMode(QAbstractItemView::ContiguousSelection);

    ui->gridLMMMeeting->setColumnCount(5);
    ui->gridLMMMeeting->setRowCount(static_cast<int>(dates.size()));
    ui->gridLMMMeeting->setHorizontalHeaderItem(0, new QTableWidgetItem(tr("Date")));
    ui->gridLMMMeeting->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("Bible Reading")));
    ui->gridLMMMeeting->setHorizontalHeaderItem(2, new QTableWidgetItem(tr("Song 1")));
    ui->gridLMMMeeting->setHorizontalHeaderItem(3, new QTableWidgetItem(tr("Song 2")));
    ui->gridLMMMeeting->setHorizontalHeaderItem(4, new QTableWidgetItem(tr("Song 3")));
    ui->gridLMMMeeting->horizontalHeader()->setStyleSheet("color: #213d5f");
    ui->gridLMMMeeting->setColumnWidth(0, 100);
    ui->gridLMMMeeting->setColumnWidth(1, 200);
    ui->gridLMMMeeting->setColumnWidth(2, 60);
    ui->gridLMMMeeting->setColumnWidth(3, 60);
    ui->gridLMMMeeting->setColumnWidth(4, 60);

    bool isFirst(true);
    lmmdateToIdx.clear();
    for (int i = 0; i < static_cast<int>(dates.size()); i++) {
        sql_item s = dates[static_cast<uint>(i)];
        QDate dt = s.value("date").toDate();
        lmmdateToIdx.insert(dt, i);
        if (isFirst)
            isFirst = false;
        else if ((dt.toJulianDay() - nextLMMDate.toJulianDay() != 7))
            lmmMeetingErrs.append("Missing dates between " + nextLMMDate.toString(Qt::ISODate) + " and " + dt.toString(Qt::ISODate));
        QSharedPointer<LMM_Meeting> mtg(new LMM_Meeting(nullptr, true));
        mtg->loadMeeting(dt);
        nextLMMDate = dt;

        QTableWidgetItem *dtCell = new QTableWidgetItem(mtg->date().toString(Qt::ISODate));
        dtCell->setFlags(dtCell->flags() ^ Qt::ItemIsEditable);
        ui->gridLMMMeeting->setItem(i, 0, dtCell);
        ui->gridLMMMeeting->setItem(i, 1, new QTableWidgetItem(mtg->bibleReading()));
        ui->gridLMMMeeting->setItem(i, 2, new QTableWidgetItem(QString::number(mtg->songBeginning())));
        ui->gridLMMMeeting->setItem(i, 3, new QTableWidgetItem(QString::number(mtg->songMiddle())));
        ui->gridLMMMeeting->setItem(i, 4, new QTableWidgetItem(QString::number(mtg->songEnd())));
    }
    if (!isFirst)
        nextLMMDate = nextLMMDate.addDays(7);
    ui->gridLMMMeeting->blockSignals(false);
    ui->errLMMMeeting->setText("<html><head/><body><p><span style=\" color:#540000;\">" + lmmMeetingErrs.join(", ") + "</span></p></body></html>");
    ui->gridLMMSchedule->setRowCount(0);
    ui->btnRemoveLMMMeeting->setEnabled(false);
    ui->btnAddLMMSchedule->setEnabled(false);
    ui->btnRemoveLMMSchedule->setEnabled(false);
    this->setCursor(Qt::ArrowCursor);
}

void Settings::loadLMMSchedule()
{
    this->setCursor(Qt::WaitCursor);
    ui->gridLMMSchedule->blockSignals(true);

    QStringList lmmScheduleErrs;
    currentLMMMeeting.loadMeeting(QDate::fromString(ui->gridLMMMeeting->item(ui->gridLMMMeeting->currentRow(), 0)->text(), "yyyy-MM-dd"));

    QList<LMM_Assignment *> assignments = currentLMMMeeting.getAssignments();
    isFirstWeek = currentLMMMeeting.date().day() < 8;

    ui->gridLMMSchedule->setItemDelegateForColumn(0, new talkTypeComboBox(this));
    ui->gridLMMSchedule->setItemDelegateForColumn(2, new talkTypeTextEdit(this));

    ui->gridLMMSchedule->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->gridLMMSchedule->setSelectionMode(QAbstractItemView::ContiguousSelection);

    ui->gridLMMSchedule->setColumnCount(4);
    ui->gridLMMSchedule->setRowCount(0);
    ui->gridLMMSchedule->setHorizontalHeaderItem(0, new QTableWidgetItem(tr("Meeting Item")));
    ui->gridLMMSchedule->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("Theme")));
    ui->gridLMMSchedule->setHorizontalHeaderItem(2, new QTableWidgetItem(tr("Source")));
    ui->gridLMMSchedule->setHorizontalHeaderItem(3, new QTableWidgetItem(tr("Timing")));
    ui->gridLMMSchedule->horizontalHeader()->setStyleSheet("color: #213d5f");
    ui->gridLMMSchedule->setColumnWidth(0, 210);
    ui->gridLMMSchedule->setColumnWidth(1, 300);
    ui->gridLMMSchedule->setColumnWidth(2, 600);
    ui->gridLMMSchedule->setColumnWidth(3, 70);
    QHeaderView *verticalHeader = ui->gridLMMSchedule->verticalHeader();
    verticalHeader->sectionResizeMode(QHeaderView::Fixed);
    verticalHeader->setDefaultSectionSize(60);

    int totalTime(0);
    for (int i = 0; i < assignments.size(); i++) {
        LMM_Assignment *a = assignments[i];

        if (a->classnumber() > 1)
            continue;
        int row = ui->gridLMMSchedule->rowCount();
        ui->gridLMMSchedule->setRowCount(row + 1);
        ui->gridLMMSchedule->setItem(row, 0, newAlignedTableWidgetItem(a->talkName(), Qt::AlignLeft | Qt::AlignTop));
        ui->gridLMMSchedule->setItem(row, 1, newAlignedTableWidgetItem(a->theme(), Qt::AlignLeft | Qt::AlignTop));
        ui->gridLMMSchedule->setItem(row, 2, newAlignedTableWidgetItem(a->source(), Qt::AlignLeft | Qt::AlignTop));
        ui->gridLMMSchedule->setItem(row, 3, newAlignedTableWidgetItem(QString::number(a->time()), Qt::AlignLeft | Qt::AlignTop));
        ui->gridLMMSchedule->item(row, 0)->setData(Qt::UserRole, a->scheduleId());
        totalTime += a->time();
    }
    int targetTime(isFirstWeek ? 82 : 79);
    if (totalTime != targetTime) {
        QString msg("Total Time is %1 but expected %2");
        lmmScheduleErrs.append(msg.arg(QString::number(totalTime), QString::number(targetTime)));
    }
    ui->gridLMMSchedule->blockSignals(false);
    ui->errLMMSchedule->setText("<html><head/><body><p><span style=\" color:#540000;\">" + lmmScheduleErrs.join(", ") + "</span></p></body></html>");
    ui->btnAddLMMSchedule->setEnabled(true);
    ui->btnRemoveLMMSchedule->setEnabled(false);
    this->setCursor(Qt::ArrowCursor);
}

QTableWidgetItem *Settings::newAlignedTableWidgetItem(QString data, int alignment)
{
    QTableWidgetItem *i = new QTableWidgetItem(data);
    i->setTextAlignment(alignment);
    return i;
}

void Settings::on_gridLMMMeeting_itemChanged(QTableWidgetItem *item)
{
    if (!item)
        return;

    sql_item sitem;
    switch (item->column()) {
    case 1:
        sitem.insert("bible_reading", item->text());
        break;
    case 2:
        sitem.insert("song_beginning", item->text());
        break;
    case 3:
        sitem.insert("song_middle", item->text());
        break;
    case 4:
        sitem.insert("song_end", item->text());
        break;
    default:
        return;
    }
    sql->updateSql("lmm_meeting", "date",
                   ui->gridLMMMeeting->item(item->row(), 0)->text(),
                   &sitem);
}
void Settings::on_gridLMMSchedule_itemChanged(QTableWidgetItem *item)
{
    if (!item) {
        ui->btnRemoveLMMSchedule->setEnabled(false);
        return;
    }

    sql_item sitem;
    int talkID(0);
    int sequence(0);
    bool okModify(true);
    sql_item parameters;
    switch (item->column()) {
    case 0:
        talkID = LMM_Schedule::getTalkTypeFromFullString(item->text());
        if (talkID < 1)
            okModify = false;

        // check for duplication
        parameters.insert(":date", currentLMMMeeting.date());
        parameters.insert(":talk_id", talkID);
        sequence = sql->selectScalar("select count(*) from lmm_schedule where date = :date and (talk_id / 10) = :talk_id", &parameters).toInt();

        sitem.insert("talk_id", LMM_Schedule::dbTalkId(talkID, sequence)); // new talkid
        if (sequence > 0 && talkID != LMM_Schedule::TalkType_InitialCall && talkID != LMM_Schedule::TalkType_ReturnVisit1 && talkID != LMM_Schedule::TalkType_ReturnVisit2 && talkID != LMM_Schedule::TalkType_ReturnVisit3)
            okModify = false;
        break;
    case 1:
        sitem.insert("theme", item->text());
        break;
    case 2:
        sitem.insert("source", item->text());
        break;
    case 3:
        if (item->text().toInt() > 0)
            sitem.insert("time", item->text());
        break;
    default:
        return;
    }
    if (okModify) {
        sql->updateSql("lmm_schedule",
                       "id", QString::number(ui->gridLMMSchedule->item(item->row(), 0)->data(Qt::UserRole).toInt()),
                       &sitem);
    }
    int hPos = ui->gridLMMSchedule->horizontalScrollBar()->value();
    int vPos = ui->gridLMMSchedule->verticalScrollBar()->value();
    int row = item->row();
    loadLMMSchedule();
    ui->gridLMMSchedule->repaint();
    ui->gridLMMSchedule->horizontalScrollBar()->setValue(hPos);
    ui->gridLMMSchedule->verticalScrollBar()->setValue(vPos);
    ui->gridLMMSchedule->selectRow(row);
    ui->btnRemoveLMMSchedule->setEnabled(true);
}

void Settings::on_gridLMMSchedule_itemSelectionChanged()
{
    ui->btnRemoveLMMSchedule->setEnabled(ui->gridLMMSchedule->selectedItems().count() > 0);
}

// school - add studies
void Settings::on_buttonAddStudies_clicked()
{
    QString filepath = QFileDialog::getOpenFileName(this, tr("Select ePub file"),
                                                    QStandardPaths::writableLocation(QStandardPaths::DesktopLocation),
                                                    "th book ePub -file (th*.epub)");
    if (filepath != "") {
        applyyourselfimport a(filepath);
        if (a.Import())
            loadSchoolStudies();
    }
}

// load lmm studies
void Settings::loadSchoolStudies()
{
    school s;
    ui->tableWidget_SchoolStudyPoints->blockSignals(true);
    ui->tableWidget_SchoolStudyPoints->clear();
    sql_items studies = sql->selectSql("lmm_studies", "active", "1", "study_number, lang");
    ui->tableWidget_SchoolStudyPoints->setColumnCount(3);
    ui->tableWidget_SchoolStudyPoints->setRowCount(static_cast<int>(studies.size()));
    ui->tableWidget_SchoolStudyPoints->setHorizontalHeaderItem(0, new QTableWidgetItem("id"));
    ui->tableWidget_SchoolStudyPoints->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("Study Number")));
    ui->tableWidget_SchoolStudyPoints->setHorizontalHeaderItem(2, new QTableWidgetItem(tr("Study Name")));
    ui->tableWidget_SchoolStudyPoints->verticalHeader()->setVisible(false);

    if (!studies.empty()) {
        for (unsigned int i = 0; i < studies.size(); i++) {
            int index = static_cast<int>(i);
            sql_item s = studies[i];
            auto numberItem = new QTableWidgetItem(s.value("study_number").toString());
            numberItem->setFlags(Qt::NoItemFlags);
            ui->tableWidget_SchoolStudyPoints->setItem(index, 0, new QTableWidgetItem(s.value("id").toString()));
            ui->tableWidget_SchoolStudyPoints->setItem(index, 1, numberItem);
            ui->tableWidget_SchoolStudyPoints->setItem(index, 2, new QTableWidgetItem(s.value("study_name").toString()));
        }
    }
    ui->tableWidget_SchoolStudyPoints->hideColumn(0);
    ui->tableWidget_SchoolStudyPoints->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
    ui->tableWidget_SchoolStudyPoints->resizeColumnToContents(1);
    ui->tableWidget_SchoolStudyPoints->blockSignals(false);
}

// lmm studies table - name cell changed
void Settings::on_tableWidget_SchoolStudyPoints_itemChanged(QTableWidgetItem *item)
{
    if (!item || item->column() != 2)
        return;

    qDebug() << "Study name edited" << item->text();

    sql_item sitem;
    sitem.insert("study_name", item->text());
    sql->updateSql("lmm_studies", "id", ui->tableWidget_SchoolStudyPoints->item(item->row(), 0)->text(), &sitem);
}

// lmm studies - remove studies
void Settings::on_buttonRemoveStudies_clicked()
{
    // remove all school studies
    if (QMessageBox::question(this, tr("Studies"),
                              tr("Remove ALL studies? (Use only to remove invalid data from database)"),
                              QMessageBox::No, QMessageBox::Yes)
        == QMessageBox::No)
        return;

    sql_item item;
    item.insert("active", 0);
    sql->execSql("update lmm_studies set active = :active, time_stamp = strftime('%s','now')", &item);
    this->loadSchoolStudies();
}

void Settings::on_btnOpenDBLocation_clicked()
{
    general::ShowInGraphicalShell(this, sql->databasepath);
}

void Settings::on_btnRemoveDuplicatesLMM_clicked()
{
    LMM_Schedule::RemoveDuplicates();
    QMessageBox::information(this, "Finish", "Please re-import your 2018 workbooks");
}
