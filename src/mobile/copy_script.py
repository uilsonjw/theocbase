#
# Created by Niko Hallikainen 3.1.2016
#
#
import shutil
import os.path
import sys, string

import ntpath

#sys.path.append('C:\Program Files (x86)\Windows Kits\8.1\bin\arm\')
sys.path.append('C:\\Program Files (x86)\\Windows Kits\\8.1\\bin\\x86\\')

os.chdir("E:\\projektit\\theocbase\\src\\mobile\\winrt_build")
#os.system('D:\\Qt\\5.5\winphone_arm\bin\\qmake.exe -tp vc E:\\projektit\\theocbase\\src\\mobile\\theocbase_mobile.pro "CONFIG+=windeployqt"')
os.system('D:\\Qt\\5.6\\winphone_arm\\bin\\qmake.exe -tp vc E:\\projektit\\theocbase\\src\\mobile\\theocbase_mobile.pro "CONFIG+=windeployqt"')

#D:\Qt\Qt5.5.1_winRT\5.5\winphone_arm\bin\qmake.exe -tp vc ../theocbase_mobile.pro "CONFIG+=windeployqt"

src = "E:\\projektit\\theocbase\\src\\mobile\\winrt\\assets\\"
dest = "E:\\projektit\\theocbase\\src\\mobile\\winrt_build\\assets\\"
src_files = os.listdir(src)
for file_name in src_files:
    full_file_name = os.path.join(src, file_name)
    if (os.path.isfile(full_file_name)):
        shutil.copy(full_file_name, dest)
