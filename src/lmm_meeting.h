#ifndef LMM_MEETING_H
#define LMM_MEETING_H

#include <QObject>
#include <QList>
#include <QMap>
#include <QVariant>
#include <QStandardItemModel>
#include <QDebug>
#include "lmm_assignment.h"
#include "lmm_assignment_ex.h"
#include "lmm_schedule.h"
#include "cpersons.h"
#include "person.h"
#include "ccongregation.h"
#include "sharedlib_global.h"
#include "talkinfo.h"

//-----------------------------------------------------------------------------
// color disaply on the main window (measured by picking colors)
// used on history table, and student history
namespace LMM_Color {
const QColor spiritual_gem_text(90, 106, 112); // 5a6a70
const QColor ministry_text(193, 134, 38); // c18626
const QColor christian_life_text(150, 21, 38); // 961526
const QColor public_talk_text(47, 72, 112); // 2f4870
const QColor watchtower_text(77, 101, 77); // 4d654d

const QColor spiritual_gem_bg(239, 240, 241); // eff0f1
const QColor ministry_bg(249, 243, 234); // f9f3ea
const QColor christian_life_bg(245, 232, 234); // f5e8ea
const QColor public_talk_bg(235, 237, 241); // ebedf1
const QColor watchtower_bg(238, 240, 238); // eef0ee

const QColor duration_box_bg(204, 204, 204); // cccccc
}

// In order to preserve the DB, this value could be 'in memory' only
// (computed from DB values)... For the moment, LMM_* value are the
// very same values as in the DB...
// Note: these enum raw values are used for sorting in history table
//       None is used for 'invisible' to sort
enum Talk_ID {
    None = -100,

    // old meeting
    old_OpeningPrayer = -11,
    old_CBS_Conductor = -10,
    old_CBS_Reader = -9,
    old_School_Highlights = -8,
    old_School_Assignment1 = -7,
    old_School_Assignment2 = -6,
    old_School_Assignment3 = -5,
    old_ServiceMeeting1 = -4,
    old_ServiceMeeting2 = -3,
    old_ServiceMeeting3 = -2,
    old_ClosingPrayer = -1,

    // LMM_OpeningPrayer missing entry or is this 0 ? Note: I wound prefer to have prayer index after chairman
    LMM_Chairman = 1, // same as LMM_Schedule::TalkType_Chairman
    LMM_Treasures = 2,
    LMM_Digging = 3,
    LMM_BibleReading = 4,
    LMM_SampleConversationVideo = 5,
    LMM_InitialCall = 6,
    LMM_ReturnVisit1 = 7,
    LMM_ReturnVisit2 = 8,
    LMM_ReturnVisit3 = 9,
    LMM_BibleStudy = 10,
    LMM_StudentTalk = 11,
    LMM_LivingTalk1 = 12,
    LMM_LivingTalk2 = 13,
    LMM_LivingTalk3 = 14,
    LMM_CBS = 15,
    LMM_COTalk = 16,
    LMM_ApplyYourself = 17, // talk going over the new teaching brochure
    //LMM_ClosingPrayer = 15

    // Public Meeting
    PublicTalk_Chairman = 21, // == PT_OpningPrayer
    PublicTalk_Speaker = 22, // PM_CircuitOverseer = 22 or other number,
    //
    Watchtower_Conductor = 23,
    Watchtower_Reader = 24,
    // PublicMeeting_ClosingTalk = 25, // for CircuitOverseer visit?
    // PublicMeeting_ClosingPrayer = 26, // should be the speaker (do we need such index?)

    LMM_MemorialInvitation = 27,
    LMM_OtherFMVideoPart = 28
};

QString LMM_historyTableAbbreviation(int talk_id, bool assistant_or_reader = false);
QColor LMM_foregroundColor(int talk_id, QColor const &defCol = Qt::black);
QColor LMM_backgroundColor(int talk_id, QColor const &defCol = Qt::white);

typedef LMM_Assignment::MySortFilterProxyModel MySortFilterProxyModel;

//-----------------------------------------------------------------------------
class TB_EXPORT LMM_Meeting : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QDate date READ date NOTIFY dateChanged)
    Q_PROPERTY(person *chairman READ chairman WRITE setChairman NOTIFY chairmanChanged)
    Q_PROPERTY(person *counselor2 READ counselor2 WRITE setCounselor2 NOTIFY counselor2Changed)
    Q_PROPERTY(person *counselor3 READ counselor3 WRITE setCounselor3 NOTIFY counselor3Changed)
    Q_PROPERTY(person *prayerBeginning READ prayerBeginning WRITE setPrayerBeginning NOTIFY prayerBeginningChanged)
    Q_PROPERTY(person *prayerEnd READ prayerEnd WRITE setPrayerEnd NOTIFY prayerEndChanged)
    Q_PROPERTY(int songBeginning READ songBeginning WRITE setSongBeginning NOTIFY songBeginningChanged)
    Q_PROPERTY(int songMiddle READ songMiddle WRITE setSongMiddle NOTIFY songMiddleChanged)
    Q_PROPERTY(int songEnd READ songEnd WRITE setSongEnd NOTIFY songEndChanged)
    Q_PROPERTY(QString songBeginningTitle READ songBeginningTitle NOTIFY songBeginningChanged)
    Q_PROPERTY(QString songMiddleTitle READ songMiddleTitle NOTIFY songMiddleChanged)
    Q_PROPERTY(QString songEndTitle READ songEndTitle NOTIFY songEndChanged)
    Q_PROPERTY(int classes READ classes WRITE setClasses NOTIFY classesChanged)
    Q_PROPERTY(QString bibleReading READ bibleReading WRITE setBibleReading NOTIFY bibleReadingChanged)
    Q_PROPERTY(QString openingComments READ openingComments WRITE setOpeningComments NOTIFY openingCommentsChanged)
    Q_PROPERTY(QString closingComments READ closingComments WRITE setClosingComments NOTIFY closingCommentsChanged)
    Q_PROPERTY(QDateTime startTime READ startTime NOTIFY startTimeChanged)
    Q_PROPERTY(QString notes READ notes WRITE setNotes NOTIFY notesChanged)

public:
    explicit LMM_Meeting(QObject *parent = nullptr, bool delayLoadingDetails = false);
    ~LMM_Meeting();

    QDate date() const;
    Q_INVOKABLE bool loadMeeting(QDate date, bool includeAllOptions = false);

    person *chairman() const;
    void setChairman(person *chairman);

    person *counselor2() const;
    void setCounselor2(person *counselor);

    person *counselor3() const;
    void setCounselor3(person *counselor);

    person *prayerBeginning() const;
    void setPrayerBeginning(person *prayer);

    person *prayerEnd() const;
    void setPrayerEnd(person *prayer);

    int songBeginning() const;
    QString songBeginningTitle() const;
    void setSongBeginning(int songBeginning);

    int songMiddle() const;
    QString songMiddleTitle() const;
    void setSongMiddle(int songMiddle);

    int songEnd() const;
    QString songEndTitle() const;
    void setSongEnd(int songEnd);

    QString bibleReading() const;
    void setBibleReading(QString reading);

    QString openingComments() const;
    void setOpeningComments(QString comments);

    QString closingComments() const;
    void setClosingComments(QString comments);

    int classes();
    void setClasses(int classes);

    QDateTime startTime();

    QString notes() const;
    void setNotes(QString notes);

    Q_INVOKABLE bool save();
    Q_INVOKABLE bool saveNotes();

    Q_INVOKABLE LMM_Assignment *getAssignment(int talkId, int sequence = 0, int classnumber = 1); //!talkid verify
    Q_INVOKABLE QList<LMM_Assignment *> getAssignments();
    Q_INVOKABLE QVariant getAssignmentsVariant();

    Q_INVOKABLE SortFilterProxyModel *getChairmanList();
    Q_INVOKABLE SortFilterProxyModel *getPrayerList();

    void loadAssignments(bool includeAllOptions = false);
    void createAssignments();
    QList<LMM_Schedule *> findSchedules(QDate firstDayOfWeek = QDate());

    Q_INVOKABLE QColor monthlyColor();
    Q_INVOKABLE QString monthlyRGBColor();
    Q_INVOKABLE QColor monthlyLightColor();

    static talkinfo *getTalkInfo(int talkId)
    {
        static QMap<int, talkinfo *> talkSectionMap;
        if (talkSectionMap.count() == 0) {
            // TODO: pull this from talkinfo table
            talkSectionMap[LMM_Chairman] = new talkinfo(0, MeetingSection::TreasuresFromGodsWord, 0);
            talkSectionMap[LMM_Treasures] = new talkinfo(0, MeetingSection::TreasuresFromGodsWord, 0);
            talkSectionMap[LMM_Digging] = new talkinfo(0, MeetingSection::TreasuresFromGodsWord, 0);
            talkSectionMap[LMM_BibleReading] = new talkinfo(0, MeetingSection::TreasuresFromGodsWord, 1);
            talkSectionMap[LMM_SampleConversationVideo] = new talkinfo(0, MeetingSection::ApplyYourselfToTheFieldMinistry, 0);
            talkSectionMap[LMM_InitialCall] = new talkinfo(0, MeetingSection::ApplyYourselfToTheFieldMinistry, 1);
            talkSectionMap[LMM_ReturnVisit1] = new talkinfo(0, MeetingSection::ApplyYourselfToTheFieldMinistry, 1);
            talkSectionMap[LMM_ReturnVisit2] = new talkinfo(0, MeetingSection::ApplyYourselfToTheFieldMinistry, 1);
            talkSectionMap[LMM_ReturnVisit3] = new talkinfo(0, MeetingSection::ApplyYourselfToTheFieldMinistry, 1);
            talkSectionMap[LMM_BibleStudy] = new talkinfo(0, MeetingSection::ApplyYourselfToTheFieldMinistry, 1);
            talkSectionMap[LMM_StudentTalk] = new talkinfo(0, MeetingSection::ApplyYourselfToTheFieldMinistry, 1);
            talkSectionMap[LMM_MemorialInvitation] = new talkinfo(0, MeetingSection::ApplyYourselfToTheFieldMinistry, 1);
            talkSectionMap[LMM_OtherFMVideoPart] = new talkinfo(0, MeetingSection::ApplyYourselfToTheFieldMinistry, 0);
            talkSectionMap[LMM_LivingTalk1] = new talkinfo(0, MeetingSection::LivingAsChristians, 0);
            talkSectionMap[LMM_LivingTalk2] = new talkinfo(0, MeetingSection::LivingAsChristians, 0);
            talkSectionMap[LMM_LivingTalk3] = new talkinfo(0, MeetingSection::LivingAsChristians, 0);
            talkSectionMap[LMM_CBS] = new talkinfo(0, MeetingSection::LivingAsChristians, 0);
            talkSectionMap[LMM_COTalk] = new talkinfo(0, MeetingSection::LivingAsChristians, 0);
            talkSectionMap[LMM_ApplyYourself] = new talkinfo(0, MeetingSection::TreasuresFromGodsWord, 0);
            talkSectionMap[PublicTalk_Chairman] = new talkinfo(0, MeetingSection::PublicTalk, 0);
            talkSectionMap[PublicTalk_Speaker] = new talkinfo(0, MeetingSection::PublicTalk, 0);
            talkSectionMap[Watchtower_Conductor] = new talkinfo(1, MeetingSection::Watchtower, 0);
            talkSectionMap[Watchtower_Reader] = new talkinfo(1, MeetingSection::Watchtower, 0);
        }
        return (talkSectionMap.contains(talkId) ? talkSectionMap[talkId] : nullptr);
    }

signals:
    void dateChanged(QDate date);
    void chairmanChanged(person *chairman);
    void counselor2Changed(person *counselor);
    void counselor3Changed(person *counselor);
    void prayerBeginningChanged(person *prayer);
    void prayerEndChanged(person *prayer);
    void songBeginningChanged(int song);
    void songMiddleChanged(int song);
    void songEndChanged(int song);
    void bibleReadingChanged(QString source);
    void openingCommentsChanged(QString source);
    void closingCommentsChanged(QString source);
    void classesChanged(int classes);
    void startTimeChanged();
    void notesChanged(QString notes);

public slots:

private:
    QDate m_date;
    int m_meeting_id;
    int m_songBeginning;
    int m_songMiddle;
    int m_songEnd;
    int m_classes;
    bool m_delayLoadingDetails;
    QList<LMM_Assignment *> m_talks;
    person *m_chairman;
    person *m_counselor2;
    person *m_counselor3;
    person *m_prayer_beginning;
    person *m_prayer_end;
    QString m_biblereading;
    QString m_openingcomments;
    QString m_closingcomments;
    QString m_notes;
    QMap<int, talkinfo *> talkSectionMap;

    SortFilterProxyModel *getBrotherList(int usefor);
    QString getSongTitle(int song) const;
};

#endif // LMM_MEETING_H
