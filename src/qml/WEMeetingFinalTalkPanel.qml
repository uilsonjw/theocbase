/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import net.theocbase 1.0

Item {
    id: wmFinalTalkEdit
    property string title: "Final Talk Edit"
    width: 500
    height: 700

    property CPTMeeting meeting
    onMeetingChanged: {
        if (meeting)
            controller.date = meeting.date
    }

    PublicMeetingController { id: controller }

    anchors.fill: parent

    ColumnLayout {
        id: layout
        anchors.fill: parent
        anchors.margins: 10

        // Theme
        Label {
            text: qsTr("Service Talk", "Circuit overseer's talk in the wekeend meeting")
            font.capitalization: Font.AllUppercase
        }
        TextArea {
            id: textTheme
            text: meeting ? meeting.finalTalk : ""
            wrapMode: Text.WordWrap
            Layout.fillWidth: true
            font.bold: true
            onEditingFinished: {
                // save theme
                if (text != meeting.finalTalk) {
                    meeting.finalTalk = text
                    meeting.save()
                }
            }
        }
        Label {
            text: qsTr("Speaker")
            font.capitalization: Font.AllUppercase
        }
        ComboBoxTable {
            id: comboSpeaker
            currentText: meeting ? meeting.finalTalkSpeakerName : ""
            Layout.fillWidth: true
            enabled: false
        }

        Item { Layout.fillHeight: true }
    }
}

