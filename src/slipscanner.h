#ifndef SLIPSCANNER_H
#define SLIPSCANNER_H

#include <QImage>
#include <QColor>
#include <QList>
#include <QString>
#include <QFileInfo>
#include <QCoreApplication>
#include <QProgressDialog>
#include <QOperatingSystemVersion>
#include <cmath>
#include "sql_class.h"

#define colorThreshold 400

class SlipScanner
{
public:
    class region
    {
    public:
        static int nextId;
        region(int x1, int x2, int y1, int y2, int absorptionMargin);
        bool intersects(region *other);
        bool absorb(region *other);
        void update();
        void applyToImg(QImage *img);
        int id;
        int x1;
        int x2;
        int y1;
        int y2;
        int absorptionMargin;
        int width;
        int height;
        int x1outer;
        int x2outer;
        int y1outer;
        int y2outer;

        // used for sorting
        int quadrant;
        int half;

        // used for selecting checkboxes
        int sectionSummary[3][3]; // general color of the region
        int rating; // higher the rating, the more it belongs
        // state info for the quality sort
        int _ref;
        int _x;
        int _y;
    };

    SlipScanner(QWidget *parent, QString filename, bool forceScan, int regionAbsorptionMargin = 4);
    virtual ~SlipScanner();
    void FinalizeBoxes(int maxCheckboxes);
    QImage img;
    int pageWidth;
    int pageHeight;
    int is4up;
    QList<region *> textBoxes;
    QList<region *> checkBoxes;
    QList<region *> blankAreas;
    int layout;
    QList<region *> regions;
    bool needFinalized;
    bool saveRegions; // not used yet
private:
    void scan();
    int m_regionAbsorptionMargin;
    QString m_filename;
    int slipsPerPage;
    QList<region *> potentialCheckBoxes;
    QProgressDialog *progressDialog;
    QString settingsKey;
    int usedPageX1;
    int usedPageX2;
    int usedPageY1;
    int usedPageY2;
    int workingPageWidth;
    int workingPageHeight;
    void SaveCompiledSlips();
};

bool compareCheckboxArea(const SlipScanner::region *a, const SlipScanner::region *b);
bool compareCheckboxRatingAsc(const SlipScanner::region *a, const SlipScanner::region *b); // lower rating is better
bool compareCheckboxRatingDec(const SlipScanner::region *a, const SlipScanner::region *b); // higher rating is better
bool compareBox(const SlipScanner::region *a, const SlipScanner::region *b);

#endif // SLIPSCANNER_H
