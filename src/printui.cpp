/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "printui.h"
#include "ui_printui.h"

printui::printui(QDate date, QWidget *parent)
    : QDialog(parent), ui(new Ui::printui), c(), myCongregation(c.getMyCongregation())
{
    isLoaded = false; // part of attempt to fix blank preview issue
    ui->setupUi(this);

    // Use white icons if dark mode
    QList<QToolButton *> buttons({ ui->toolButton, ui->toolButtonCopy, ui->toolButtonPrint });
    general::changeButtonColors(buttons, this->palette().window().color());

    sql = &Singleton<sql_class>::Instance();
    ac = &Singleton<AccessControl>::Instance();
    mWebView = new QWebEngineView(this);
    mWebView->resize(0, 0);
    mWebView->show();
    NoUIEvents = 0;

    templatePath1 = qApp->applicationDirPath();
#ifdef Q_OS_MAC
    ui->splitter->setStyleSheet("");
    templatePath1 += "/../Resources";
#endif
    templatePath1 += "/templates/";

    pr = QSharedPointer<QPrinter>(new QPrinter(QPrinter::ScreenResolution));
    if (!pr->isValid()) {
        if (!QPrinterInfo::defaultPrinter().isNull()) {
            pr.reset(new QPrinter(QPrinterInfo::defaultPrinter(), QPrinter::ScreenResolution));
        } else if (QPrinterInfo::availablePrinters().count() > 0) {
            pr.reset(new QPrinter(QPrinterInfo::availablePrinters().at(0), QPrinter::ScreenResolution));
        }
    }

    QDir myDir(templatePath1);
    QStringList nameFilter;
    nameFilter << "*.htm*"
               << "*.pdf"
               << "*.jp*"
               << "*.png";
    templateList = myDir.entryList(nameFilter);
    templatePath2 = sql->getSetting("customTemplateDirectory");
    if (!templatePath2.isEmpty()) {
        if (!templatePath2.endsWith("/") && !templatePath2.endsWith("\\"))
            templatePath2 += "/";
        myDir.setPath(templatePath2);
        QStringList customTemplates = myDir.entryList(nameFilter);
        for (QString f : customTemplates) {

            // pdf to jpg
            bool include(true);

            if (f.contains(QRegularExpression("S-(12|89)", QRegularExpression::CaseInsensitiveOption)) && f.endsWith(".pdf")) {
                include = false; // don't include the pdf if we have the jpg
                QString jpgname = QFileInfo(f).completeBaseName() + ".jpg";
                if (!customTemplates.contains(jpgname)) {
                    if (generateSlipPdfToJpg(myDir.path() + "/" + f, myDir.path() + "/" + jpgname)) {
                        templateList.append(jpgname);
                    }
                }
            }

            if (!templateList.contains(f) && include)
                templateList.append(f);
        }
        std::sort(templateList.begin(), templateList.end());
    }

    ui->btnAdditionalOptions->setChecked(true);
    ui->grpAdditionalOptions->hide();

    fillCustomPaperSizes();

    // button menu for pdf and csv
    QMenu *m = new QMenu(this);
    QAction *apdf = new QAction("PDF File", this);
    connect(apdf, &QAction::triggered, this, &printui::toolButtonPdf_clicked);
    m->addAction(apdf);
    QAction *ahtm = new QAction("HTML File", this);
    connect(ahtm, &QAction::triggered, this, &printui::toolButtonHtml_clicked);
    m->addAction(ahtm);
    QAction *aodf = new QAction("ODF File", this);
    connect(aodf, &QAction::triggered, this, &printui::toolButtonOdf_clicked);
    m->addAction(aodf);
    ui->toolButton->setMenu(m);

    // temp work-around
    individualSlips = sql->getIntSetting("printIndividualSlips", 0);

    // update preview when radio button changed
    connect(ui->radioButtonSchool, &QRadioButton::clicked, this, &printui::radioButtonClicked);
    connect(ui->radioButtonProgram, &QRadioButton::clicked, this, &printui::radioButtonClicked);
    connect(ui->radioButtonTaskOrder, &QRadioButton::clicked, this, &printui::radioButtonClicked);
    connect(ui->checkBoxAssistantSlip, &QCheckBox::clicked, this, &printui::radioButtonClicked);
    connect(ui->checkBoxBlank, &QCheckBox::clicked, this, &printui::radioButtonClicked);
    connect(ui->radioButtonWorkSheet, &QRadioButton::clicked, this, &printui::radioButtonClicked);

    connect(ui->radioButtonCombination, &QRadioButton::clicked, this, &printui::radioButtonClicked);
    connect(ui->chk_Comb_LMM, &QCheckBox::clicked, this, &printui::radioButtonClicked);
    connect(ui->chk_Comb_PublicMeeting, &QCheckBox::clicked, this, &printui::radioButtonClicked);
    connect(ui->chk_Comb_PublicMeetingOut, &QCheckBox::clicked, this, &printui::radioButtonClicked);

    connect(ui->radioButtonPublicMeeting, &QRadioButton::clicked, this, &printui::radioButtonClicked);
    connect(ui->radioButton_PublicMeetingProgram, &QRadioButton::clicked, this, &printui::radioButtonClicked);
    connect(ui->radioButton_PublicMeetingWorkSheet, &QRadioButton::clicked, this, &printui::radioButtonClicked);
    connect(ui->radioButton_OutgoingSpeakers, &QRadioButton::clicked, this, &printui::radioButtonClicked);
    connect(ui->radioButton_OutgoingSpeakersSlips, &QRadioButton::clicked, this, &printui::radioButtonClicked);
    connect(ui->radioButton_TalksOfSpeakers, &QRadioButton::clicked, this, &printui::radioButtonClicked);
    connect(ui->radioButtonTerritories, &QRadioButton::clicked, this, &printui::radioButtonClicked);
    connect(ui->radioButton_TerritoryRecord, &QRadioButton::clicked, this, &printui::radioButtonClicked);
    connect(ui->radioButton_TerritoryCard, &QRadioButton::clicked, this, &printui::radioButtonClicked);
    connect(ui->radioButton_TerritoryMapCard, &QRadioButton::clicked, this, &printui::radioButtonClicked);
    connect(ui->radioButton_Hospitality, &QRadioButton::clicked, this, &printui::radioButtonClicked);

    connect(ui->radioButtonFieldMinistry, &QRadioButton::clicked, this, &printui::radioButtonClicked);

    connect(ui->previewPdf, &PrintPreview::textSizeDecrease, this, &printui::decreaseTextSize);
    connect(ui->previewPdf, &PrintPreview::textSizeIncrease, this, &printui::increaseTextSize);
    connect(ui->previewPdf, &PrintPreview::textSizeReset, this, &printui::resetTextSize);
    connect(ui->previewPdf, &PrintPreview::previewReady, this, &printui::clearBusy);

    connect(ui->previewPdf, &PrintPreview::mapSizeDecrease, this, &printui::decreaseTextSize);
    connect(ui->previewPdf, &PrintPreview::mapSizeIncrease, this, &printui::increaseTextSize);
    connect(ui->previewPdf, &PrintPreview::mapSizeReset, this, &printui::resetTextSize);

    ui->checkBoxBlank->setVisible(false);
    ui->checkBoxAssistantSlip->setVisible(false);
    ui->grpAdditionalOptions->setVisible(false);

    ui->radioButtonFieldMinistry->setVisible(false);

    ui->labelTerritoryNumber->setVisible(false);
    ui->lineEditTerritoryNumber->setVisible(false);
    QRegularExpression rx("([1-9]\\d*,[ ]?)*[1-9]\\d*");
    QValidator *validator = new QRegularExpressionValidator(rx, this);
    ui->lineEditTerritoryNumber->setValidator(validator);

    // page margins
    QString pm = sql->getSetting("template_pagemargins");
    if (pm == "") {
        pmlist.setLeft(15);
        pmlist.setTop(20);
        pmlist.setRight(15);
        pmlist.setBottom(20);
    } else {
        QStringList temp(pm.split(","));
        pmlist.setLeft(temp[0].toDouble());
        pmlist.setTop(temp[1].toDouble());
        pmlist.setRight(temp[2].toDouble());
        pmlist.setBottom(temp[3].toDouble());
    }
    // page size

    QSettings settings;
    int svalue = settings.value("printui/weeks", 0).toInt();
    qDebug() << QVariant(svalue).toString();
    //ui->spinBox->setValue(svalue);

    // default value for font size factory
    //textSizeFactory = settings.value("printui/textsize",1).toReal();

    // default value for font size factory on assignment slips
    textSizeFactorySlips = settings.value("print/fontsize", 0).toInt();

    mapSizeFactory = settings.value("print/mapsize", 1).toInt();

    mPage = nullptr;

    applyAuthorizationRules();

    isLoaded = true;

    currentTemplate = QSharedPointer<PrintDocument>(new PrintMidweekSchedule());
    currentTemplate->setFromDate(date);
    NoUIEvents++;
    ui->editDateRangeFrom->setDate(currentTemplate->getFromDate());
    ui->editDateRangeThru->setDate(currentTemplate->getToDate());
    NoUIEvents--;
    printRequest();
}

printui::~printui()
{
    if (tmpHtmlFile.exists())
        tmpHtmlFile.remove();
    delete mPage;
    delete ui;
}

void printui::setCloud(cloud_controller *c)
{
    this->cloud = c;
    ui->checkBoxQRCode->setEnabled(cloud->logged());
    connect(cloud, &cloud_controller::loggedChanged, this, [=](bool logged) {
        ui->checkBoxQRCode->setEnabled(logged);
        if (!logged)
            ui->checkBoxQRCode->setChecked(false);
    });
}

void printui::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void printui::resizeEvent(QResizeEvent *)
{
    resetZooming();
}

void printui::printRequest()
{
    // part of attempt to fix blank preview issue
    if (!isLoaded)
        return;
    setBusy();

    ui->previewPdf->pdfLoadingStarted();

    if (ui->radioButtonSchool->isChecked()) {
        if (ui->radioButtonTaskOrder->isChecked()) {
            if (currentTemplate->getTemplateData()->getTemplateType() != TemplateData::MidweekSlipTemplate)
                currentTemplate.reset(new PrintMidweekSlip());
            static_cast<PrintMidweekSlip *>(currentTemplate.data())->setOnlyAssigned(ui->checkBoxBlank->isChecked());
            static_cast<PrintMidweekSlip *>(currentTemplate.data())->setPrintAssistant(ui->checkBoxAssistantSlip->isChecked());
            static_cast<PrintMidweekSlip *>(currentTemplate.data())->setDebugBoxes(QGuiApplication::keyboardModifiers() & Qt::ControlModifier);
            static_cast<PrintMidweekSlip *>(currentTemplate.data())->setForceRescan(QGuiApplication::keyboardModifiers() & Qt::ShiftModifier);
        } else if (ui->radioButtonProgram->isChecked()) {
            if (currentTemplate->getTemplateData()->getTemplateType() != TemplateData::MidweekScheduleTemplate)
                currentTemplate.reset(new PrintMidweekSchedule());
        } else if (ui->radioButtonWorkSheet->isChecked()) {
            if (currentTemplate->getTemplateData()->getTemplateType() != TemplateData::MidweekWorksheetTemplate)
                currentTemplate.reset(new PrintMidweekWorksheet());
        }
    } else if (ui->radioButtonCombination->isChecked()) {
        if (currentTemplate->getTemplateData()->getTemplateType() != TemplateData::CombinationTemplate)
            currentTemplate.reset(new PrintCombination());
        dynamic_cast<PrintCombination *>(currentTemplate.data())->setMidweek(ui->chk_Comb_LMM->isChecked());
        dynamic_cast<PrintCombination *>(currentTemplate.data())->setPublicMeeting(ui->chk_Comb_PublicMeeting->isChecked());
        dynamic_cast<PrintCombination *>(currentTemplate.data())->setPublicMeetingOut(ui->chk_Comb_PublicMeetingOut->isChecked());
    } else if (ui->radioButtonPublicMeeting->isChecked()) {
        if (ui->radioButton_PublicMeetingProgram->isChecked()) {
            if (currentTemplate->getTemplateData()->getTemplateType() != TemplateData::PublicMeetingTemplate)
                currentTemplate.reset(new PrintWeekendSchedule());
        } else if (ui->radioButton_PublicMeetingWorkSheet->isChecked()) {
            if (currentTemplate->getTemplateData()->getTemplateType() != TemplateData::PublicMeetingWorksheet)
                currentTemplate.reset(new PrintWeekendWorksheet());
        } else if (ui->radioButton_OutgoingSpeakers->isChecked()) {
            if (currentTemplate->getTemplateData()->getTemplateType() != TemplateData::OutgoingScheduleTemplate)
                currentTemplate.reset(new PrintOutgoingSchedule());
        } else if (ui->radioButton_OutgoingSpeakersSlips->isChecked()) {
            if (currentTemplate->getTemplateData()->getTemplateType() != TemplateData::OutgoingSlipTemplate)
                currentTemplate.reset(new PrintOutgoingAssignment());
        } else if (ui->radioButton_Hospitality->isChecked()) {
            if (currentTemplate->getTemplateData()->getTemplateType() != TemplateData::HospitalityTemplate)
                currentTemplate.reset(new PrintHospitality());
        } else if (ui->radioButton_TalksOfSpeakers->isChecked()) {
            if (currentTemplate->getTemplateData()->getTemplateType() != TemplateData::TalksOfSpeakersTemplate)
                currentTemplate.reset(new PrintTalksOfSpeakersList());
        }
    } else if (ui->radioButtonTerritories->isChecked()) {
        if (ui->radioButton_TerritoryRecord->isChecked()) {
            if (currentTemplate->getTemplateData()->getTemplateType() != TemplateData::TerritoryAssignmentRecordTemplate)
                currentTemplate.reset(new PrintTerritoryAssignmentRecord());
        } else if (ui->radioButton_TerritoryCard->isChecked()) {
            if (currentTemplate->getTemplateData()->getTemplateType() != TemplateData::TerritoryCardTemplate)
                currentTemplate.reset(new PrintTerritoryCard());
            dynamic_cast<PrintTerritoryCard *>(currentTemplate.data())->setTerritoryNumberList(territoryNumberList);
        } else if (ui->radioButton_TerritoryMapCard->isChecked()) {
            if (currentTemplate->getTemplateData()->getTemplateType() != TemplateData::TerritoryMapCardTemplate)
                currentTemplate.reset(new PrintTerritoryMapCard());
            dynamic_cast<PrintTerritoryMapCard *>(currentTemplate.data())->setTerritoryNumberList(territoryNumberList);
            dynamic_cast<PrintTerritoryMapCard *>(currentTemplate.data())->setForceScan(QGuiApplication::keyboardModifiers() & Qt::ShiftModifier);
        }
    }

    currentTemplate->setFromDate(ui->editDateRangeFrom->date());
    currentTemplate->setToDate(ui->editDateRangeThru->date());

    QVariant filledTemplate = currentTemplate->fillTemplate();
    if (filledTemplate.type() == QVariant::String)
        setHTML(filledTemplate.toString());
    else if (filledTemplate.type() == QVariant::ByteArray)
        ui->previewPdf->setPdf(filledTemplate.toByteArray());

    pickTemplateEditor();
    // reset zooming
    resetZooming();
}

void printui::pickTemplateEditor()
{
    NoUIEvents++;
    ui->cboTemplate->clear();
    auto templateData = currentTemplate->getTemplateData();
    if (templateData) {
        int idx = 0;
        for (QString file : templateList) {
            if (
                    file.contains(templateData->fileFilter, Qt::CaseInsensitive) && (templateData->notFileFilter.isEmpty() || !file.contains(templateData->notFileFilter, Qt::CaseInsensitive))) {
                ui->cboTemplate->insertItem(idx, file);
                if (file.compare(templateData->getTemplateName()) == 0)
                    ui->cboTemplate->setCurrentIndex(idx);
                idx++;
            }
        }
        if (ui->cboTemplate->currentIndex() < 0 && idx > 0)
            ui->cboTemplate->setCurrentIndex(0);
        ui->cboPaperSize->setCurrentText(templateData->paperSize);
        showCurrentTemplateOptions();
    }
    NoUIEvents--;
}

void printui::fillCustomPaperSizes()
{
    paperSizes.clear();
    paperSizes.append("A4");
    paperSizes.append("Letter");
    paperSizes.append("Legal");
    paperSizes.append("Tabloid");
    paperSizes.append("A6");
    //TODO: paper sizes
    //    for (templateData *t : templates) {
    //        if (!paperSizes.contains(t->paperSize))
    //            paperSizes.append(t->paperSize);
    //    }
    paperSizes.append(tr("Custom...", "pick custom paper size"));
    NoUIEvents++;
    ui->cboPaperSize->clear();
    ui->cboPaperSize->addItems(paperSizes);
    NoUIEvents--;
}

void printui::on_toolButtonCopy_clicked()
{
    // copy text to clipboard
    QTextEdit *copypastetext = new QTextEdit();
    QTextDocument *d = new QTextDocument(copypastetext);
    d->setHtml(activeHtml);
    copypastetext->setDocument(d);

    copypastetext->selectAll();
    copypastetext->copy();
    QMessageBox::information(this, "", tr("Copied to the clipboard. Paste to word processing program (Ctrl+V/Cmd+V)"));
    delete copypastetext;
}

void printui::toolButtonPdf_clicked()
{
    setBusy();
    // save pdf file
    bool debugBoxes(QGuiApplication::keyboardModifiers() & Qt::ControlModifier);
    bool findBoxNames(QGuiApplication::keyboardModifiers() & Qt::ShiftModifier);
    QSettings settings;
    QString exportpath = QFileDialog::getSaveFileName(this, tr("Save file"),
                                                      settings.value("printui/pdf/path", QDir::homePath()).toString(),
                                                      "PDF (*.pdf)");
    if (exportpath != "") {
        pr->setOutputFormat(QPrinter::PdfFormat);
        pr->setOutputFileName(exportpath);
        QFileInfo fileInfo(exportpath);
        settings.setValue("printui/pdf/path", fileInfo.dir().path());
        QByteArray output;
        if (currentTemplate->getTemplateData()->getTemplateType() == TemplateData::MidweekSlipTemplate) {
            dynamic_cast<PrintMidweekSlip *>(currentTemplate.data())->setOnlyAssigned(ui->checkBoxBlank->isChecked());
            dynamic_cast<PrintMidweekSlip *>(currentTemplate.data())->setPrintAssistant(ui->checkBoxAssistantSlip->isChecked());
            dynamic_cast<PrintMidweekSlip *>(currentTemplate.data())->setDebugBoxes(debugBoxes);
            dynamic_cast<PrintMidweekSlip *>(currentTemplate.data())->setForceRescan(findBoxNames);
            output = currentTemplate->fillTemplate().toByteArray();
        } else if (currentTemplate->getTemplateData()->getTemplateType() == TemplateData::TerritoryMapCardTemplate) {
            dynamic_cast<PrintTerritoryMapCard *>(currentTemplate.data())->setTerritoryNumberList(territoryNumberList);
            dynamic_cast<PrintTerritoryMapCard *>(currentTemplate.data())->setForceScan(findBoxNames);
            output = currentTemplate->fillTemplate().toByteArray();
        } else {
            if (ui->checkBoxQRCode->isChecked())
                uploadSharedFile();
            mPage->printToPdf(exportpath, pr->pageLayout());
        }
        if (!output.isEmpty()) {
            QFile file(exportpath);
            if (file.exists())
                if (!file.remove()) {
                    clearBusy();
                    return;
                }
            if (file.open(QFile::WriteOnly)) {
                file.write(output);
                file.close();
            }
        }
        QMessageBox::information(this, "", exportpath + " " + tr("file created") + (sharedLink.isEmpty() ? "" : QString("<br>"
                                                                                                                        "<br>"
                                                                                                                        "Shared link: <a href='%1'>%1</a>")
                                                                                                                        .arg(sharedLink)));
    }
    clearBusy();
    sharedLink = "";
}

void printui::toolButtonHtml_clicked()
{
    QSettings settings;
    QString exportpath = QFileDialog::getSaveFileName(this, tr("Save file"),
                                                      settings.value("printui/html/path", QDir::homePath()).toString(),
                                                      "HTML (*.html)");
    if (!exportpath.isEmpty()) {
        QFileInfo fileInfo(exportpath);
        settings.setValue("printui/html/path", fileInfo.dir().path());

        mPage->save(exportpath);
    }
}

void printui::on_toolButtonPrint_clicked()
{
    setBusy();
    // print to native printer
    pr->setOutputFormat(QPrinter::NativeFormat);
    QPrintDialog printDialog(pr.data(), this);
    printDialog.setOption(QAbstractPrintDialog::PrintShowPageSize, true);
    if (printDialog.exec() == QDialog::Accepted) {
        if (currentTemplate->getTemplateData()->getTemplateType() == TemplateData::MidweekSlipTemplate) {
            bool debugBoxes(QGuiApplication::keyboardModifiers() & Qt::ControlModifier);
            bool findBoxNames(QGuiApplication::keyboardModifiers() & Qt::ShiftModifier);
            dynamic_cast<PrintMidweekSlip *>(currentTemplate.data())->setOnlyAssigned(ui->checkBoxBlank->isChecked());
            dynamic_cast<PrintMidweekSlip *>(currentTemplate.data())->setPrintAssistant(ui->checkBoxAssistantSlip->isChecked());
            dynamic_cast<PrintMidweekSlip *>(currentTemplate.data())->setDebugBoxes(debugBoxes);
            dynamic_cast<PrintMidweekSlip *>(currentTemplate.data())->setForceRescan(findBoxNames);
            dynamic_cast<PrintMidweekSlip *>(currentTemplate.data())->setPrinter(pr.data());
            currentTemplate->fillTemplate();
            dynamic_cast<PrintMidweekSlip *>(currentTemplate.data())->setPrinter(nullptr);
        } else if (currentTemplate->getTemplateData()->getTemplateType() == TemplateData::TerritoryMapCardTemplate) {
            dynamic_cast<PrintTerritoryMapCard *>(currentTemplate.data())->setTerritoryNumberList(territoryNumberList);
            dynamic_cast<PrintTerritoryMapCard *>(currentTemplate.data())->setForceScan(false);
            dynamic_cast<PrintTerritoryMapCard *>(currentTemplate.data())->setPrinter(pr.data());
            currentTemplate->fillTemplate();
            dynamic_cast<PrintTerritoryMapCard *>(currentTemplate.data())->setPrinter(nullptr);
        } else {
            if (ui->checkBoxQRCode->isChecked())
                uploadSharedFile();
            QEventLoop event;
            mPage->print(pr.data(), [&event](bool ok) { Q_UNUSED(ok); event.quit(); });
            event.exec();
        }
        // This is a workaround to reset page orientation after printing.
        QPrinterInfo info(*pr);
        pr.reset(new QPrinter(info));
    }

    clearBusy();
    if (!sharedLink.isEmpty()) {
        QMessageBox::information(this, "", QString("Shared link: <a href='%1'>%1</a>").arg(sharedLink));
        sharedLink = "";
    }
}

void printui::toolButtonOdf_clicked()
{
    // open document format file

    QSettings settings;
    qDebug() << settings.value("printui/odf/path").toString();
    QString exportpath = QFileDialog::getSaveFileName(this, tr("Save file"),
                                                      settings.value("printui/odf/path", QDir::homePath() + "/export.odt").toString(),
                                                      "Open Document Format (*.odt)");
    if (exportpath == "")
        return;

    QTextDocumentWriter *w = new QTextDocumentWriter(exportpath, "odf");

    QFile file(exportpath);
    if (QFile::exists(file.fileName())) {
        QFile::remove(file.fileName());
    }

    QTextDocument *d = new QTextDocument();
    d->setHtml(activeHtml);
    bool ok = w->write(d);

    QFileInfo info(exportpath);
    if (ok)
        settings.setValue("printui/odf/path", info.dir().path());
    QMessageBox::information(this, "", exportpath + " " + tr("file created"));
}

void printui::radioButtonClicked()
{
    printRequest();
}

void printui::applyAuthorizationRules()
{
    // midweek meeting
    if (!ac->user()->hasPermission(Permission::Rule::CanPrintMidweekMeetingSchedule) && !ac->user()->hasPermission(Permission::Rule::CanPrintMidweekMeetingAssignmentSlips) && !ac->user()->hasPermission(Permission::Rule::CanPrintMidweekMeetingSchedule)) {
        ui->radioButtonSchool->setHidden(true);
        ui->mwTemplateTypesWidget->setHidden(true);
        ui->radioButtonCombination->setHidden(true);
        ui->combinationTemplateTypesWidget->setHidden(true);
    } else {
        if (!ac->user()->hasPermission(Permission::Rule::CanPrintMidweekMeetingSchedule)) {
            ui->radioButtonProgram->setHidden(true);
        }
        if (!ac->user()->hasPermission(Permission::Rule::CanPrintMidweekMeetingAssignmentSlips)) {
            ui->radioButtonTaskOrder->setHidden(true);
            ui->checkBoxAssistantSlip->setHidden(true);
            ui->checkBoxBlank->setHidden(true);
        }
        if (!ac->user()->hasPermission(Permission::Rule::CanPrintMidweekMeetingWorksheets)) {
            ui->radioButtonWorkSheet->setHidden(true);
        }
    }

    // weekend meeting
    if (!ac->user()->hasPermission(Permission::Rule::CanPrintWeekendMeetingSchedule) && !ac->user()->hasPermission(Permission::Rule::CanPrintWeekendMeetingWorksheets) && !ac->user()->hasPermission(Permission::Rule::CanPrintSpeakersSchedule) && !ac->user()->hasPermission(Permission::Rule::CanPrintSpeakersAssignments) && !ac->user()->hasPermission(Permission::Rule::CanPrintHospitality) && !ac->user()->hasPermission(Permission::Rule::CanPrintPublicTalkList)) {
        ui->radioButtonPublicMeeting->setHidden(true);
        ui->weTemplateTypesWidget->setHidden(true);
        ui->radioButtonCombination->setHidden(true);
        ui->combinationTemplateTypesWidget->setHidden(true);
    } else {
        if (!ac->user()->hasPermission(Permission::Rule::CanPrintWeekendMeetingSchedule)) {
            ui->radioButton_PublicMeetingProgram->setHidden(true);
        }
        if (!ac->user()->hasPermission(Permission::Rule::CanPrintWeekendMeetingWorksheets)) {
            ui->radioButton_PublicMeetingWorkSheet->setHidden(true);
        }
        if (!ac->user()->hasPermission(Permission::Rule::CanPrintSpeakersSchedule)) {
            ui->radioButton_OutgoingSpeakers->setHidden(true);
        }
        if (!ac->user()->hasPermission(Permission::Rule::CanPrintSpeakersAssignments)) {
            ui->radioButton_OutgoingSpeakersSlips->setHidden(true);
        }
        if (!ac->user()->hasPermission(Permission::Rule::CanPrintHospitality)) {
            ui->radioButton_Hospitality->setHidden(true);
        }
        if (!ac->user()->hasPermission(Permission::Rule::CanPrintPublicTalkList)) {
            ui->radioButton_TalksOfSpeakers->setHidden(true);
        }
    }

    // territory
    if (!ac->user()->hasPermission(Permission::Rule::CanPrintTerritoryRecord) && !ac->user()->hasPermission(Permission::Rule::CanPrintTerritoryMapCard) && !ac->user()->hasPermission(Permission::Rule::CanPrintTerritoryMapAndAddressSheets)) {
        ui->radioButtonTerritories->setHidden(true);
        ui->territoryTemplateTypesWidget->setHidden(true);
    } else {
        if (!ac->user()->hasPermission(Permission::Rule::CanPrintTerritoryRecord)) {
            ui->radioButton_TerritoryRecord->setHidden(true);
        }
        if (!ac->user()->hasPermission(Permission::Rule::CanPrintTerritoryMapCard)) {
            ui->radioButton_TerritoryMapCard->setHidden(true);
        }
        if (!ac->user()->hasPermission(Permission::Rule::CanPrintTerritoryMapAndAddressSheets)) {
            ui->radioButton_TerritoryCard->setHidden(true);
        }
    }
}

bool printui::generateSlipPdfToJpg(QString pdfpath, QString jpgpath)
{
    QProgressDialog progressdialog(QString(tr("Converting %1 to JPG file")).arg(pdfpath), "", 0, 0);
    progressdialog.setCancelButton(nullptr);
    progressdialog.setWindowModality(Qt::ApplicationModal);
    progressdialog.show();
    ccloud cl;
    bool ret = cl.convertPdfToJpg(pdfpath, jpgpath);
    progressdialog.close();
    return ret;
}

QString printui::intListJoin(QList<int> list, QString delim)
{
    QString ret;
    bool isFirst(true);
    for (int n : list) {
        if (isFirst)
            isFirst = false;
        else
            ret.append(delim);
        ret.append(QString::number(n));
    }
    return ret;
}

QString printui::addQrCode(QString context)
{
    qDebug() << "adding QR code";
    QString qrcodeTag = "";
    QString sharedLinkTag = "!SHARED_LINK!";
    QRegularExpression rx("!QRCODE_[0-9]*!");
    if (context.indexOf(rx) > -1) {
        QRegularExpressionMatch match = rx.match(context);
        qrcodeTag = match.capturedTexts().first();
    }
    if (ui->checkBoxQRCode->isChecked()) {

        QString sharedLinkTest = getSharedFilePath();

        if (sharedLink.isEmpty() || sharedLinkTest != sharedLink) {
            QString dropboxPath = getSharedFilePath();

            bool fileExists = cloud->authentication()->fileExists(dropboxPath);
            if (fileExists)
                sharedLink = cloud->authentication()->getSharedLink(dropboxPath);
        }

        if (qrcodeTag.isEmpty()) {
            qrcodeTag = "!QRCODE_100!";
            context.append("<div align=\"center\" style=\"margin:20px; padding:10px; border:1px solid  black\">" + qrcodeTag + "<br>" + sharedLinkTag + "</div>");
        }
        QString size = qrcodeTag.mid(qrcodeTag.indexOf("_") + 1, qrcodeTag.length() - (qrcodeTag.indexOf("_") + 2));

        context.replace(qrcodeTag, QString("<img src=\"%1\" height=\"%2\" width=\"%2\">").arg(sharedLink.isEmpty() ? QString("data:image/svg+xml;utf8,"
                                                                                                                             "%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width"
                                                                                                                             "%3D%22100%22%20height%3D%22100%22%3E%3Crect%20x%3D%225%22%20y%3D%225%22%20"
                                                                                                                             "width%3D%2290%22%20height%3D%2290%22%20rx%3D%2220%22%20fill%3D%22none%22%20"
                                                                                                                             "stroke%3D%22black%22%20stroke-width%3D%225%22%20stroke-dasharray"
                                                                                                                             "%3D%2210%22%2F%3E%3C%2Fsvg%3E")
                                                                                                                   : QString("https://api.theocbase.net/api.php?qrcode&data=%1").arg(sharedLink),
                                                                                              size));
        context.replace(sharedLinkTag, QString("<a href=\"%1\">%1</a>").arg(sharedLink.isEmpty() ? "https://dropbox.com/..." : sharedLink));
    } else {
        context.replace(qrcodeTag, "");
        context.replace(sharedLinkTag, "");
    }
    return context;
}

QString printui::addHtmlImage(QString path, QString tagname)
{
    QFile f(path);
    if (!f.exists()) {
        qDebug() << "image path does not found";
        return "";
    }
    f.open(QIODevice::ReadOnly);
    QByteArray bytearray = f.readAll();
    f.close();
    QString size = tagname.mid(tagname.indexOf("_") + 1, tagname.length() - (tagname.indexOf("_") + 2));
    QString imgtag = QString("<img src=data:image/png;base64,%1 height=\"%2\" width=\"%2\">").arg(QString::fromUtf8(bytearray.toBase64()), size);
    qDebug() << imgtag;
    return imgtag;
}

QString printui::uploadSharedFile()
{
    QProgressDialog progressdialog("Uploading shared file...", "", 0, 0);
    progressdialog.setCancelButton(nullptr);
    progressdialog.setWindowModality(Qt::ApplicationModal);
    progressdialog.show();

    QString dropboxPath = getSharedFilePath();
    QDateTime dt = cloud->authentication()->upload(bContent, dropboxPath);
    if (dt.isValid()) {
        if (sharedLink.isEmpty()) {
            sharedLink = cloud->authentication()->getSharedLink(dropboxPath);
            qDebug() << "shared link" << sharedLink;
            printRequest();
            QEventLoop loop;
            connect(ui->previewPdf, &PrintPreview::previewReady, &loop, &QEventLoop::quit);
            loop.exec();
            cloud->authentication()->upload(bContent, dropboxPath);
        }
    }
    progressdialog.close();
    return sharedLink;
}

QString printui::getSharedFilePath()
{
    const QFileInfo syncFileInfo(cloud->authentication()->getAccountInfo()->getSyncFile());
    const QFileInfo templateFileInfo(currentTemplate->getTemplateData()->getTemplateName());

    return syncFileInfo.path() + "/public/" + templateFileInfo.completeBaseName() + ".pdf";
}

void printui::showCurrentTemplateOptions()
{
    auto templateData = currentTemplate->getTemplateData();
    bool hasSongTitle(templateData->optionValue("song-titles").length() > 0);
    bool hasWorkbookNumber(templateData->optionValue("mwb-number").length() > 0);
    bool hasWatchtowerNumber(templateData->optionValue("wt-number").length() > 0);
    bool hasDuration(templateData->optionValue("duration").length() > 0);
    bool hasCounselName(templateData->optionValue("counsel-name").length() > 0);
    bool hasSectionTitle(templateData->optionValue("section-titles").length() > 0);
    bool canFilterOwnCongregationOnly(templateData->optionValue("own-congregation-only").length() > 0);
    bool hasTalkRevisionDate(templateData->optionValue("revision-date").length() > 0);

    NoUIEvents++;
    ui->ckOptionShowSongTitles->setVisible(hasSongTitle);
    ui->ckOptionShowSongTitles->setChecked(templateData->optionValue("song-titles") == "yes");
    ui->ckOptionShowWorkbookNumber->setVisible(hasWorkbookNumber);
    ui->ckOptionShowWorkbookNumber->setChecked(templateData->optionValue("mwb-number") == "yes");
    ui->ckOptionShowWatchtowerNumber->setVisible(hasWatchtowerNumber);
    ui->ckOptionShowWatchtowerNumber->setChecked(templateData->optionValue("wt-number") == "yes");
    ui->ckOptionIncludeCounselText->setVisible(hasCounselName);
    ui->ckOptionIncludeCounselText->setChecked(templateData->optionValue("counsel-name") == "yes");
    ui->ckOptionOwnCongregation->setVisible(canFilterOwnCongregationOnly);
    ui->ckOptionOwnCongregation->setChecked(templateData->optionValue("own-congregation-only") == "yes");
    ui->ckOptionShowTalkRevision->setVisible(hasTalkRevisionDate);
    ui->ckOptionShowTalkRevision->setChecked(templateData->optionValue("revision-date") == "yes");
    ui->rbOptionShowTime->setVisible(hasDuration);
    ui->rbOptionShowDuration->setVisible(hasDuration);
    if (templateData->optionValue("duration") == "duration")
        ui->rbOptionShowDuration->setChecked(true);
    else
        ui->rbOptionShowTime->setChecked(true);
    ui->ckOptionSectionTitles->setVisible(hasSectionTitle);
    ui->ckOptionSectionTitles->setChecked(templateData->optionValue("section-titles") == "yes");
    ui->txtMWTitle->setText(sql->getSetting("print_mw_title", tr("Midweek Meeting")));
    ui->txtWETitle->setText(sql->getSetting("print_we_title", tr("Weekend Meeting")));

    bool worksheet = ui->radioButtonSchool->isChecked() && ui->radioButtonWorkSheet->isChecked();
    ui->lblMW_OC->setVisible(worksheet);
    ui->lblMW_CC->setVisible(worksheet);
    ui->txtMW_OC_Title->setVisible(worksheet);
    ui->txtMW_CC_Title->setVisible(worksheet);
    ui->txtMW_OC_Title->setText(sql->getSetting("print_mw_oc_title", tr("Opening Comments")));
    ui->txtMW_CC_Title->setText(sql->getSetting("print_mw_cc_title", tr("Concluding Comments")));

    ui->toolButton->menu()->actions().at(1)->setEnabled(
            currentTemplate->getTemplateData()->getTemplateType() != TemplateData::MidweekSlipTemplate && currentTemplate->getTemplateData()->getTemplateType() != TemplateData::TerritoryMapCardTemplate);
    ui->toolButton->menu()->actions().at(2)->setEnabled(
            currentTemplate->getTemplateData()->getTemplateType() != TemplateData::MidweekSlipTemplate && currentTemplate->getTemplateData()->getTemplateType() != TemplateData::TerritoryMapCardTemplate);

    NoUIEvents--;
}

void printui::setHTML(QString html)
{
    if (currentTemplate->qrCodeSupported())
        html = addQrCode(html);
    pr->setOutputFormat(QPrinter::PdfFormat);
    pr->setPageLayout(currentTemplate->getLayout());

    pr->setPageOrientation(currentTemplate->getLayout().orientation());
    pr->setPageMargins(currentTemplate->getLayout().margins());
    pr->setFullPage(true);

    if (mPage)
        delete mPage;
    mPage = new QWebEnginePage(this);
    mWebView->setPage(mPage);
    mPage->settings()->globalSettings()->setAttribute(QWebEngineSettings::WebAttribute::LocalContentCanAccessFileUrls, true);
    mPage->settings()->globalSettings()->setAttribute(QWebEngineSettings::WebAttribute::LocalContentCanAccessRemoteUrls, true); //
    mPage->settings()->globalSettings()->setAttribute(QWebEngineSettings::WebAttribute::AllowRunningInsecureContent, true);

    connect(mPage->profile(), &QWebEngineProfile::downloadRequested, this, &printui::htmlDownloadRequest);

    bool waitForLoadedEvent = currentTemplate->getTemplateData()->metaValue("tb-send-loaded-event", "no") == "yes";

    if (!waitForLoadedEvent)
        connect(mPage, &QWebEnginePage::loadFinished, [=](bool ok) {
            qDebug() << "Page loaded" << ok;
            mPage->printToPdf([this](const QByteArray &result) {
                bContent = result;
                qDebug() << "print pdf ready";
                ui->previewPdf->setPdf(result);
            },
                              pr->pageLayout());
        });
    else {
        channel = new QWebChannel(mPage);
        mPage->setWebChannel(channel);
        channel->registerObject(QString("printChannel"), &printChannel);

        QMetaObject::Connection *const connection = new QMetaObject::Connection;
        *connection = connect(&printChannel, &PrintChannel::loadFinished, [=](QString msg) {
            QObject::disconnect(*connection);
            delete connection;
            qDebug() << "Page loaded event" << msg;
            mPage->printToPdf([this](const QByteArray &result) {
                bContent = result;
                qDebug() << "print pdf ready";
                ui->previewPdf->setPdf(result);
            },
                              pr->pageLayout());
        });
    }

    // load html code from temporary file because setHtml has 2MB limit
    tmpHtmlFile.setFileTemplate(QDir::tempPath() + QDir::separator() + "TheocBase_XXXXXX.html");
    if (tmpHtmlFile.exists())
        tmpHtmlFile.remove();
    tmpHtmlFile.setAutoRemove(false);
    if (tmpHtmlFile.open()) {
        QByteArray tempContent;
        tempContent.append(html.toUtf8());
        tmpHtmlFile.write(tempContent);
        tmpHtmlFile.close();

        QUrl qurl = QUrl::fromLocalFile(tmpHtmlFile.fileName());
        mPage->load(qurl);
    }

    activeHtml = html;
    //    // font size
    //    webFrame->setTextSizeMultiplier(textSizeFactory);
}

void printui::resetZooming()
{
    // resize event -> reset preview zooming to 'FitInView'
    //pv->fitInView();
    // update zoom factory in print preview because it depens of window size
    //defaultZoomFactor = pv->zoomFactor();
    //ui->zoomSlider->setValue(100);
}

void printui::on_checkBoxQRCode_clicked(bool checked)
{
    if (checked) {
        ccloud cld;
        cld.generateQRCode("school");
        cld.generateQRCode("combination");
        cld.generateQRCode("publicmeeting");
    }
    printRequest();
}

void printui::increaseTextSize()
{
    QSettings settings;
    // increase text size
    if (ui->radioButtonSchool->isChecked() && ui->radioButtonTaskOrder->isChecked()) {
        textSizeFactorySlips += 1;
        settings.setValue("print/fontsize", textSizeFactorySlips);
        printRequest();
    } else if (ui->radioButtonTerritories->isChecked()) {
        textSizeFactorySlips += 1;
        settings.setValue("print/fontsize", textSizeFactorySlips);
        printRequest();
    } else {
        mPage->runJavaScript("document.documentElement.style.fontSize = (parseInt(window.getComputedStyle"
                             "(document.documentElement).getPropertyValue('font-size') )+1).toString()+'px'",
                             [this](const QVariant &v) {
                                 Q_UNUSED(v);
                                 mPage->printToPdf([this](const QByteArray &result) {
                                     ui->previewPdf->setPdf(result);
                                 },
                                                   pr->pageLayout());
                             });
    }
}

void printui::decreaseTextSize()
{
    QSettings settings;
    // decrease font size
    if (ui->radioButtonSchool->isChecked() && ui->radioButtonTaskOrder->isChecked()) {
        textSizeFactorySlips -= 1;
        settings.setValue("print/fontsize", textSizeFactorySlips);
        printRequest();
    } else if (ui->radioButtonTerritories->isChecked()) {
        mapSizeFactory -= 1;
        settings.setValue("print/mapsize", textSizeFactorySlips);
        printRequest();
    } else {
        mPage->runJavaScript("document.documentElement.style.fontSize = (parseInt(window.getComputedStyle"
                             "(document.documentElement).getPropertyValue('font-size') )-1).toString()+'px'",
                             [this](const QVariant &v) {
                                 Q_UNUSED(v);
                                 mPage->printToPdf([this](const QByteArray &result) {
                                     ui->previewPdf->setPdf(result);
                                 },
                                                   pr->pageLayout());
                             });
    }
}

void printui::on_editDateRangeFrom_dateChanged(const QDate &date)
{
    static int noUI(0);
    if (noUI > 0)
        return;
    noUI++;

    QDate fromDate = date.addDays(1 - date.dayOfWeek());
    QDate thruDate = ui->editDateRangeThru->date();
    if (fromDate.month() != date.month())
        fromDate = fromDate.addDays(7);
    if (fromDate != date)
        ui->editDateRangeFrom->setDate(fromDate);

    if (fromDate > thruDate) {
        thruDate = fromDate.addMonths(1);
        thruDate = thruDate.addDays(-thruDate.day());
        thruDate = thruDate.addDays(1 - thruDate.dayOfWeek());
        ui->editDateRangeThru->setDate(thruDate);
    }
    if (!NoUIEvents)
        printRequest();

    noUI--;
}

void printui::on_editDateRangeThru_dateChanged(const QDate &date)
{
    static int noUI(0);
    if (noUI > 0)
        return;
    noUI++;

    QDate d = date.addDays(1 - date.dayOfWeek());
    if (ui->editDateRangeFrom->date() > d)
        d = ui->editDateRangeFrom->date();
    if (d != date)
        ui->editDateRangeThru->setDate(d);
    if (!NoUIEvents)
        printRequest();

    noUI--;
}

void printui::on_lineEditTerritoryNumber_returnPressed()
{
    territoryNumberList = ui->lineEditTerritoryNumber->text();
    if (territoryNumberList.endsWith(","))
        territoryNumberList.remove(territoryNumberList.size() - 1, 1);
    printRequest();
}

void printui::resetTextSize()
{
    QSettings settings;
    textSizeFactorySlips = 0;
    settings.setValue("print/fontsize", textSizeFactorySlips);
    mapSizeFactory = 1;
    settings.setValue("print/mapsize", 1);
    printRequest();
}

void printui::on_cboTemplate_activated(const QString &arg)
{
    if (NoUIEvents)
        return;

    currentTemplate->setTemplateName(arg);
    printRequest();
}

void printui::on_cboPaperSize_activated(int index)
{
    if (NoUIEvents)
        return;

    if (index == ui->cboPaperSize->count() - 1) {
        bool ok(false);
        QString size = QInputDialog::getText(this, tr("New Custom Paper Size", "title of dialog box"), tr("Format: width x height. Width and Height can be in or mm. Example 210mm x 297mm"), QLineEdit::Normal, "", &ok);
        if (ok && !size.isEmpty()) {
            QString msg = currentTemplate->getTemplateData()->setPaperSize(size);
            if (msg.isEmpty()) {
                ok = true;
                fillCustomPaperSizes();
            } else {
                QMessageBox::information(this, "", tr("Invalid entry, sorry."));
            }
        }
        ui->cboPaperSize->setCurrentText(currentTemplate->getTemplateData()->paperSize);
        if (!ok)
            return;
    } else {
        currentTemplate->getTemplateData()->setPaperSize(ui->cboPaperSize->currentText());
    }
    radioButtonClicked();
}

void printui::on_ckOptionShowSongTitles_clicked(bool checked)
{
    if (NoUIEvents || !ui->ckOptionShowSongTitles->isVisible())
        return;
    NoUIEvents++;
    currentTemplate->getTemplateData()->setOptionValue("song-titles", checked ? "yes" : "no", false, true);
    radioButtonClicked();
    NoUIEvents--;
}

void printui::on_ckOptionShowWorkbookNumber_clicked(bool checked)
{
    if (NoUIEvents || !ui->ckOptionShowWorkbookNumber->isVisible())
        return;
    NoUIEvents++;
    currentTemplate->getTemplateData()->setOptionValue("mwb-number", checked ? "yes" : "no", false, true);
    radioButtonClicked();
    NoUIEvents--;
}

void printui::on_ckOptionShowWatchtowerNumber_clicked(bool checked)
{
    if (NoUIEvents || !ui->ckOptionShowWatchtowerNumber->isVisible())
        return;
    NoUIEvents++;
    currentTemplate->getTemplateData()->setOptionValue("wt-number", checked ? "yes" : "no", false, true);
    radioButtonClicked();
    NoUIEvents--;
}

void printui::on_ckOptionIncludeCounselText_clicked(bool checked)
{
    if (NoUIEvents || !ui->ckOptionIncludeCounselText->isVisible())
        return;
    NoUIEvents++;
    currentTemplate->getTemplateData()->setOptionValue("counsel-name", checked ? "yes" : "no", false, true);
    radioButtonClicked();
    NoUIEvents--;
}

void printui::on_rbOptionShowTime_clicked(bool checked)
{
    if (NoUIEvents || !checked || !ui->rbOptionShowTime->isVisible())
        return;
    NoUIEvents++;
    currentTemplate->getTemplateData()->setOptionValue("duration", "time", false, true);
    radioButtonClicked();
    NoUIEvents--;
}

void printui::on_rbOptionShowDuration_clicked(bool checked)
{
    if (NoUIEvents || !checked || !ui->rbOptionShowDuration->isVisible())
        return;
    NoUIEvents++;
    currentTemplate->getTemplateData()->setOptionValue("duration", "duration", false, true);
    radioButtonClicked();
    NoUIEvents--;
}

void printui::on_ckOptionSectionTitles_clicked(bool checked)
{
    if (NoUIEvents || !ui->ckOptionSectionTitles->isVisible())
        return;
    NoUIEvents++;
    currentTemplate->getTemplateData()->setOptionValue("section-titles", checked ? "yes" : "no", false, true);
    radioButtonClicked();
    NoUIEvents--;
}

void printui::on_ckOptionOwnCongregation_clicked(bool checked)
{
    if (NoUIEvents || !ui->ckOptionOwnCongregation->isVisible())
        return;
    NoUIEvents++;
    currentTemplate->getTemplateData()->setOptionValue("own-congregation-only", checked ? "yes" : "no", false, true);
    radioButtonClicked();
    NoUIEvents--;
}

void printui::on_ckOptionShowTalkRevision_clicked(bool checked)
{
    if (NoUIEvents || !ui->ckOptionShowTalkRevision->isVisible())
        return;
    NoUIEvents++;
    currentTemplate->getTemplateData()->setOptionValue("revision-date", checked ? "yes" : "no", false, true);
    radioButtonClicked();
    NoUIEvents--;
}

void printui::on_btnAdditionalOptions_clicked(bool checked)
{
    ui->grpAdditionalOptions->setVisible(!checked);
}

void printui::on_txtMWTitle_editingFinished()
{
    if (NoUIEvents || !ui->txtMWTitle->isVisible() || ui->txtMWTitle->text().isEmpty())
        return;
    NoUIEvents++;
    sql->saveSetting("print_mw_title", ui->txtMWTitle->text());
    radioButtonClicked();
    NoUIEvents--;
}

void printui::on_txtWETitle_editingFinished()
{
    if (NoUIEvents || !ui->txtWETitle->isVisible() || ui->txtWETitle->text().isEmpty())
        return;
    NoUIEvents++;
    sql->saveSetting("print_we_title", ui->txtWETitle->text());
    radioButtonClicked();
    NoUIEvents--;
}

void printui::on_txtMW_OC_Title_editingFinished()
{
    if (NoUIEvents || !ui->txtMW_OC_Title->isVisible() || ui->txtMW_OC_Title->text().isEmpty())
        return;
    NoUIEvents++;
    sql->saveSetting("print_mw_oc_title", ui->txtMW_OC_Title->text());
    radioButtonClicked();
    NoUIEvents--;
}

void printui::on_txtMW_CC_Title_editingFinished()
{
    if (NoUIEvents || !ui->txtMW_CC_Title->isVisible() || ui->txtMW_CC_Title->text().isEmpty())
        return;
    NoUIEvents++;
    sql->saveSetting("print_mw_cc_title", ui->txtMW_CC_Title->text());
    radioButtonClicked();
    NoUIEvents--;
}

void printui::htmlDownloadRequest(QWebEngineDownloadItem *item)
{
    if (item->state() != QWebEngineDownloadItem::DownloadRequested)
        return;

    QString exportpath = QDir(item->downloadDirectory()).filePath(item->downloadFileName());

    item->disconnect();
    connect(item, &QWebEngineDownloadItem::stateChanged, [=](QWebEngineDownloadItem::DownloadState state) {
        if (state == QWebEngineDownloadItem::DownloadRequested || state == QWebEngineDownloadItem::DownloadInProgress) {
            return;
        } else if (state == QWebEngineDownloadItem::DownloadCompleted) {
            setBusy();
            if (ui->checkBoxQRCode->isChecked() && ((ui->radioButtonSchool->isChecked() && ui->radioButtonProgram->isChecked()) || (ui->radioButtonPublicMeeting->isChecked() && ui->radioButton_PublicMeetingProgram->isChecked()) || ui->radioButtonCombination->isChecked())) {
                uploadSharedFile();
            }
            clearBusy();

            QString message = QString("%1 %2 %3").arg(exportpath, tr("file created"), sharedLink.isEmpty() ? "" : QString("<br><br>Shared link: <a href='%1'>%1</a>").arg(sharedLink));
            QMessageBox::information(this, "", message);
        }

        item->deleteLater();
        sharedLink = "";
    });

    item->setSavePageFormat(QWebEngineDownloadItem::SingleHtmlSaveFormat);
    item->accept();
}

void printui::setBusy()
{
    this->setCursor(Qt::WaitCursor);
    ui->frameSidebar->setEnabled(false);
}

void printui::clearBusy()
{
    this->setCursor(Qt::ArrowCursor);
    ui->frameSidebar->setEnabled(true);
}
