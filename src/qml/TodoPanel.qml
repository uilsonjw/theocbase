/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.3
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2
import Qt.labs.calendar 1.0
import net.theocbase 1.0

Popup {
    id: todoEdit
    x: Math.round((parent.width - width) / 2)
    y: Math.round((parent.height - height) / 2)
    width: 500
    height: 400
    opacity: 0.6
    modal: false
    margins: 0
    padding: 0
    Material.theme: Material.Dark

    property int length: todo_model ? todo_model.length : 0

    signal movedToSchedule(var date, var incoming)

    function reload() {
        todo_model.loadList()
    }        

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        anchors.margins: 5
        property point clickPos: Qt.point(1,1)
        onPressed: clickPos = Qt.point(mouse.x,mouse.y)
        onPositionChanged: {            
            var delta = Qt.point(mouse.x-clickPos.x, mouse.y-clickPos.y)
            todoEdit.x += delta.x
            todoEdit.x = Math.min(todoEdit.x, todoEdit.parent.width - todoEdit.width)
            todoEdit.y += delta.y
        }
        MouseArea {
            anchors.horizontalCenter: parent.right
            anchors.verticalCenter: parent.bottom
            width: 10
            height: width
            cursorShape: Qt.SizeFDiagCursor
            drag { target: parent; axis: Drag.XAndYAxis }
            onMouseXChanged: {
                if (drag.active){
                    todoEdit.width = Math.min(todoEdit.parent.width, todoEdit.width + mouseX)
                    if (todoEdit.width < 50)
                        todoEdit.width = 50
                    else if (todoEdit.width > parent.width)
                        todoEdit.width = parent.width
                }
            }
            onMouseYChanged: {
                if (drag.active) {
                    todoEdit.height = Math.min(todoEdit.parent.height, todoEdit.height + mouseY)
                    if (todoEdit.height < 50)
                        todoEdit.height = 50
                }
            }
        }
    }

    PublicMeetingController { id: pmController }

    TodoModel {
        id: todo_model
        property int inComings: 0
        property int outGoings: 0
        onModelChanged: {
            inComings = 0
            outGoings = 0
            var i
            for (i = 0; i < todo_model.rowCount(); i++) {
                if (todo_model.get(i).isIncoming)
                    inComings++
                else
                    outGoings++
            }
        }
    }

    MessageDialog {
        id: warningDialog
    }

    GridLayout {
        id: grid
        columns: 2
        anchors.fill: parent
        anchors.margins: 10
        Label {
            text: qsTr("To Do List")
            Layout.fillWidth: true
            font.capitalization: Font.AllUppercase
            font.pointSize: 12
        }

        RoundButton {
            id: roundButton
            icon.source: "qrc:/icons/remove.svg"
            onClicked: todoEdit.close()
        }

        ScrollView {
            Layout.columnSpan: 2
            Layout.fillWidth: true
            Layout.fillHeight: true
            contentHeight: listView.height
            clip: true
            ListView {
                id: listView
                model: todo_model
                width: grid.width

                header: Item {
                    height: 30
                    width: parent.width

                    Loader {
                        sourceComponent: headerRow
                        property bool inout: true
                        anchors.left: parent.left
                        anchors.verticalCenter: parent.top
                        anchors.verticalCenterOffset: 15
                    }
                    Loader {
                        sourceComponent: headerRow
                        property bool inout: false
                        anchors.left: parent.left
                        anchors.verticalCenter: parent.top
                        anchors.verticalCenterOffset: todo_model.inComings * 30 + 45
                    }
                }

                delegate: Item {
                    id: itemDelegate
                    width: listView.width
                    property bool addHeader: !isIncoming && (index == 0 || index > 0 && todo_model.get(index-1).isIncoming)
                    property bool buttonsHovered: buttonDelete.hovered || buttonMove.hovered || buttonEdit.hovered
                    height: (itemDelegate.ListView.isCurrentItem ? 220 : 30) + (addHeader ? 30 : 0)
                    Behavior on height { PropertyAnimation{ duration: 500 } }

                    Rectangle {
                        anchors.fill: parent
                        anchors.topMargin: parent.addHeader ? 30 : 0
                        color: mouseRowArea.containsMouse || parent.buttonsHovered ? "grey" : "transparent"
                        Label {
                            anchors.fill: parent
                            text: "[%1] %2 %3- %4".arg(date.toLocaleDateString(Qt.locale(), Locale.ShortFormat))
                                .arg(speaker).arg(isIncoming ? "(" + congregation + ")" : "").arg(theme)
                            elide: Text.ElideRight
                            color: Material.foreground
                            verticalAlignment: Text.AlignVCenter
                        }

                        MouseArea {
                            id: mouseRowArea
                            anchors.fill: parent
                            anchors.topMargin: parent.addHeader ? 30 : 0
                            hoverEnabled: true
                            enabled: !itemDelegate.ListView.isCurrentItem
                        }

                        RowLayout {
                            anchors.verticalCenter: parent.top
                            anchors.right: parent.right
                            spacing: 0
                            visible: mouseRowArea.containsMouse || itemDelegate.buttonsHovered

                            ToolButton {
                                id: buttonDelete
                                icon.source: "qrc:/icons/delete.svg"
                                Layout.alignment: Qt.AlignVCenter
                                onClicked: todo_model.removeRow(index)
                            }
                            ToolButton {
                                id: buttonMove
                                icon.source: "qrc:/icons/move_to_week.svg"
                                Layout.alignment: Qt.AlignVCenter
                                enabled: speaker != "" && theme != "" && congregation != ""
                                onClicked: {
                                    var inout = isIncoming
                                    var retVal = todo_model.moveToSchedule(index)
                                    if (retVal === "") {                                        
                                        movedToSchedule(date, inout)
                                    } else {
                                        warningDialog.text = qsTr("Cannot schedule this item until these fields are fixed: %1").arg(retVal)
                                        warningDialog.icon = StandardIcon.Warning
                                        warningDialog.open()
                                    }
                                }
                            }
                            ToolButton {
                                id: buttonEdit
                                icon.source: "qrc:/icons/edit.svg"
                                Layout.alignment: Qt.AlignVCenter
                                onClicked: {
                                    listView.currentIndex = index
                                }
                            }
                        }
                    }

                    Loader {
                        id: editBox
                        anchors.fill: parent
                        anchors.topMargin: itemDelegate.addHeader ? 30 : 0
                        sourceComponent: itemDelegate.ListView.isCurrentItem ? editPopup : null
                        onLoaded: {
                            item.editDate = date
                            item.editSpeaker = speaker
                            item.editCongregation = congregation                            
                            item.editNotes = notes
                            // theme format in edit box is "number name"
                            var themeText = theme
                            if (themeText !== "") {
                                var re = "\\((\\d+)\\)"
                                var matches = themeText.match(re)
                                themeText = matches[1] + " " + themeText.replace(matches[0],"")
                            }
                            item.editTheme = themeText
                        }

                        Connections {
                            target: editBox.item
                            function onEditDateChanged() { date = editBox.item.editDate }
                            function onEditSpeakerChanged() { speaker = editBox.item.editSpeaker }
                            function onEditCongregationChanged() { congregation = editBox.item.editCongregation }
                            function onEditThemeChanged() {
                                var text = editBox.item.editTheme
                                if (text.trim() !== "") {
                                    // save in format "name (number)"
                                    var i = text.indexOf(" ")
                                    var number = text.substring(0, i)
                                    text = text.substring(i+1)
                                    text = text + " (" + number + ")"
                                }
                                theme = text
                            }
                            function onEditNotesChanged() { notes = editBox.item.editNotes }

                            function onSaveChanges() {
                                listView.currentIndex = -1
                                todo_model.saveRow(index)
                            }
                        }
                    }
                }
            }
        }
    }

    Component {
        id: headerRow
        RowLayout {
            width: parent.width
            Label {
                font.bold: true
                font.pixelSize: 16
                font.capitalization: Font.AllUppercase
                verticalAlignment: TextEdit.AlignVCenter
                text: inout ? qsTr("Incoming", "incoming speaker") : qsTr("Outgoing", "outgoing speaker")
                Layout.fillHeight: true
            }

            ToolButton {
                Layout.alignment: Qt.AlignVCenter
                icon.source: "qrc:/icons/add_circle.svg"
                onClicked: {
                    var newIndex = todo_model.addRow(inout)
                    listView.currentIndex = newIndex
                }
            }
        }
    }

    Component {
        id: editPopup
        Rectangle {
            id: editRoot
            width: 300
            height: 220
            border.width: 1
            border.color: "white"
            color: Material.background
            clip: true

            property date editDate
            property string editSpeaker
            property string editCongregation
            property string editTheme
            property string editNotes

            signal saveChanges()

            GridLayout {
                anchors.fill: parent
                anchors.leftMargin: 10
                anchors.rightMargin: 10
                columns: 2

                Button {
                    id: buttonDate
                    text: editRoot.editDate.toLocaleDateString(Qt.locale(), Locale.ShortFormat)
                    onClicked: cal.open()
                    CalendarPopup {
                        id: cal                        
                        onClosed: editRoot.editDate = convertDateToUTC(selectedDate)
                        function convertDateToUTC(date) {
                            return new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
                        }
                    }
                }
                ToolButton {
                    Layout.alignment: Qt.AlignRight
                    icon.source: "qrc:/icons/done.svg"
                    onClicked: {
                        editRoot.saveChanges()
                    }
                }

                ComboBoxTable {
                    id: speakerCombo
                    currentText: editRoot.editSpeaker
                    Layout.fillWidth: true
                    Layout.columnSpan: 2
                    onBeforeMenuShown: {
                        model = todo_model.get(listView.currentIndex).isIncoming ?
                                    pmController.speakerList(-1,-1) :
                                    pmController.speakerListLocal()
                    }
                    onRowSelected: {
                        if (id < 1) {
                            editRoot.editSpeaker = ""
                        } else {
                            var p = CPersons.getPerson(id)
                            editRoot.editSpeaker = p.fullname;
                        }
                    }
                }

                ComboBoxTable {
                    id: congregationCombo
                    currentText: editRoot.editCongregation
                    Layout.fillWidth: true
                    Layout.columnSpan: 2
                    onRowSelected: editRoot.editCongregation = currentText
                    onBeforeMenuShown: model = pmController.congregationList()
                }

                ComboBoxTable {
                    id: themeCombo
                    currentText: editRoot.editTheme
                    Layout.fillWidth: true
                    Layout.columnSpan: 2
                    onBeforeMenuShown: {
                        model = pmController.themeList(-1)
                    }
                    onRowSelected: editRoot.editTheme = currentText
                }

                TextArea {
                    id: textAreaNotes
                    text: editRoot.editNotes
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    onEditingFinished: editRoot.editNotes = text
                    Layout.columnSpan: 2
                }
            }
        }
    }

}
