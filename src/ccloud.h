/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CCLOUD_H
#define CCLOUD_H

#include <QThread>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QHttpPart>
#include <QCryptographicHash>
#include <QEventLoop>
#include <QDebug>
#include <QSettings>
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include "ccongregation.h"
#include "csync.h"

class ccloud : public QThread
{
    Q_OBJECT

public:
    ccloud(QObject *parent = 0);
    bool isLogged();

    QString generateQRCode(QString scheduleName);
    QString uploadHtmlSchedule(QString htmlpath);

    bool convertPdfToJpg(QString input, QString output);

signals:

private:
    int userId();
    int mUserId;
};

#endif // CCLOUD_H
