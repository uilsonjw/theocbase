#ifndef ZIPPER_H
#define ZIPPER_H

#include <QApplication>
#include <QString>
#include <QFile>
#include <QTemporaryDir>
#include <QProcess>
#include <QDebug>
#include "zip/qzipreader_p.h"

class zipper
{    
public:
    zipper();
    QString Zip() {return "We'll do this later";}
    QString UnZip(QString filePath);
    QString errMessage;

private:
    void setup(QString filePath, QString work);
    QTemporaryDir tempdir;
    bool ready;
    QString setupErr;
    QString zipCmd;
    QStringList argList;
};

#endif // ZIPPER_H
