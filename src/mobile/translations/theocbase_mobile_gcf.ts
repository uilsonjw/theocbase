<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cpf-gcf">
<context>
    <name>CBStudySchedule</name>
    <message>
        <source>S</source>
        <comment>abbreviation of the &apos;study&apos; (Congregation Bible Study)</comment>
        <translation type="vanished">ÉBL</translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Pa bay pwochen lèson-la</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Kontinyé èvè menm lèson-la</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Enfòwmasyon-la pa bon</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Ès ou ka mèt tan-la?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Référans-la</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Zélèv</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Moun-la ki ka fè-y èvè-y</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Rézilta</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Ja fèt</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Volontè</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Chwazi on volontè</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Lèson pou travay</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Ègzèsis-la fin</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Pwochen lèson</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Chwazi pwochen lèson-la</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Kantité tan</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Maké</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Détay si lékòl-la</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Démaré kwonomèt-la</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Arété kronomèt-la</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Kad</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Chwazi kad-la</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Référans-la</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Kondiktè</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Oratè</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lèktè</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Maké</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Détay</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <comment>Page title</comment>
        <translation>Maké</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation type="unfinished">Maké</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&apos;S WORD</source>
        <translation>TRÉZÒ KI AN PAWÒL A BONDYÉ</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>PRAN MINISTÈ-LA OSÉRYÉ</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>VI A ON KRÉTYEN</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Prézidan</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Konséyé</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Kantik %1 é priyè-la</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Kantik</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Kondiktè</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lèktè</translation>
    </message>
    <message>
        <source>Main</source>
        <comment>Main class when auxiliary classes</comment>
        <translation type="vanished">Prensipal</translation>
    </message>
    <message>
        <source>Second</source>
        <comment>Auxiliary Class</comment>
        <translation type="vanished">Dézyèm</translation>
    </message>
    <message>
        <source>Third</source>
        <comment>Auxiliary Class</comment>
        <translation type="vanished">Twazyèm</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Pawòl pou koumansé</translation>
    </message>
    <message>
        <source>Review Followed by Preview of Next Week</source>
        <translation>Révizyon é sa nou ké vwè pwochen simenn-la</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Priyè</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation>Enpòté pwogram-la...</translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Travay menm lèson-la</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Démaré kwonomèt-la</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Arété kronomèt-la</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Ès ou ka mèt tan-la?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Référans-la</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Zélèv</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation type="vanished">Kad</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Rézilta</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Ja fèt</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Kantité tan</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Lèson-la pou travay</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Ègzèsis-la fin</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Pwochen lèson</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Chazi pwochen lèson-la</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Maké</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Moun-la ki ka fè-y èvè-y</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Volontè</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>Arété kronomèt-la</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>Moun-la ki ka fè sijè-la èvè-y dwèt mèm sèks ki-y.</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>Lèson</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Détay</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>Ralé-y désann pou mété-y ajou...</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Lésé-y monté pou mété-y ajou...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Loading...</source>
        <translation type="vanished">Chajman...</translation>
    </message>
    <message>
        <source>Login Failed</source>
        <translation type="vanished">Konèksyon-la pa maché</translation>
    </message>
    <message>
        <source>TheocBase Login</source>
        <translation type="vanished">Konèkté-w a TheocBase</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Non a itilizatè-la</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Modpas</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Konèkté</translation>
    </message>
    <message>
        <source>New User / Forgot password</source>
        <translation type="vanished">Nouvo itilizatè / Modpas pèd</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Non a itilizatè-la ou imèl-ay</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>imèl</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Kréyé on kont</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>Roukréyé modpas-la</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>Imèl-la pala!</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Modpas pèd</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>Paj pou konèkté-w</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Main</source>
        <translation type="vanished">Sal prensipal</translation>
    </message>
    <message>
        <source>Second</source>
        <translation type="vanished">Dézyèm</translation>
    </message>
    <message>
        <source>Third</source>
        <translation type="vanished">Twazyèm sal</translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation>Simenn ki koumansé lè %1</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation type="vanished">Étid biblik a lasanblé-la</translation>
    </message>
    <message>
        <source>Theocratic Ministry School</source>
        <translation type="vanished">Lékòl pou Ministè Téokratik</translation>
    </message>
    <message>
        <source>Service Meeting</source>
        <translation type="vanished">Réyinyon sèvis</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <translation type="vanished">Diskou piblik</translation>
    </message>
    <message>
        <source>Watchtower Study</source>
        <translation type="vanished">Étid a Tou dè Gad la</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="vanished">Réinyon a simenn-la</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="vanished">Réinyon a wikenn-la</translation>
    </message>
    <message>
        <source>Midweek</source>
        <comment>Midweek Meeting</comment>
        <translation>Simenn-la</translation>
    </message>
    <message>
        <source>Weekend</source>
        <comment>Weekend Meeting</comment>
        <translation>Wikenn</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation>ORATÈ KI KA SÒTI</translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>%1 oratè ki ka sòti wikenn-lasa</numerusform>
        </translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation>Pani pon oratè ki ka sòti wikenn-lasa</translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Kantik é priyè</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Kantik %1 é priyè-la</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>DISKOU PIBLIK</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>ÉTID A TOU DÈ GAD LA</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Kantik %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Kondiktè</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lèktè</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation>Enpòté TG-la...</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>Lasanblé</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Oratè</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Pòwtab</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Téléfòn</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Imèl</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Enfo</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Moun ki ka risivouè</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>Diskou piblik</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>Tinon</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Fanmi</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Frè</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Sè</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Frè nomé</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Fanmi</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>I sé fanmi a</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Enfòwmasyon si moun-la</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Téléfòn</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Imèl</translation>
    </message>
    <message>
        <source>Use for School</source>
        <translation type="vanished">Itilizé pou lékòl-la</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>Tout sé sal-la</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Sèlman sal prensipal-la</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Sèlman dézièm sal-la</translation>
    </message>
    <message>
        <source>Highlights</source>
        <translation type="vanished">Pwen entérésan</translation>
    </message>
    <message>
        <source>No 1</source>
        <translation type="vanished">N°1</translation>
    </message>
    <message>
        <source>No 2</source>
        <translation type="vanished">N°2</translation>
    </message>
    <message>
        <source>No 3</source>
        <translation type="vanished">N°3</translation>
    </message>
    <message>
        <source>Break</source>
        <translation type="vanished">Pòz</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Chèf a fanmi</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Moun-la ki ka fè-y èvè-y</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Enfo</translation>
    </message>
    <message>
        <source>Call %1?</source>
        <comment>Call to phone number</comment>
        <translation type="vanished">Kriyé %1?</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>Aktif</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Prézidan</translation>
    </message>
    <message>
        <source>Treasures from God&apos;s Word</source>
        <translation>Trézò ki an Pawòl a Bondyé</translation>
    </message>
    <message>
        <source>Digging for Spiritual Gems</source>
        <translation>An-nou fouyé pou jwenn trézò éspirityèl</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Lèkti a Labib</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Prèmyé kontak</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Nouvèl vizit</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Étid biblik</translation>
    </message>
    <message>
        <source>Prepare This Month&apos;s Presentations</source>
        <translation type="vanished">Préparé prézantasyon a mwa lasa</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>Diskou a Vi a on krétyen</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Étid biblik a lasanblé-la</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Lèktè a létid biblik a lasanblé la</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>Nouvo pwoklamatè</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Priyè</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation>Ka risivouè sé oratè-la</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Pòwtab</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Réinyon a simenn-la</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>Diskou</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Réinyon a wikenn-la</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>Kondiktè a Tou dè Gad la</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation>Lèktè a létid a Tou dè Gad la</translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Sé pwoklamatè-la</translation>
    </message>
</context>
<context>
    <name>SchoolSchedule</name>
    <message>
        <source>Main</source>
        <translation type="vanished">Prensipal</translation>
    </message>
    <message>
        <source>Second</source>
        <translation type="vanished">Dézyèm</translation>
    </message>
    <message>
        <source>Third</source>
        <translation type="vanished">Twazyèm</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation>Sa ki chwazi</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Réglaj-la</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Dékonèkté</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Enfo</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Vèwsyon</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>Paj akèy a TheocBase</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Sa ou ka pansé</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>I senkwonizé lè: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Pwogram</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Montré lè-la</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Montré diré-la</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Anviwònman pou itilizatè-la</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Lang-la</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Konèkté</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Imèl</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Non</translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Senkwonizasiyon-la...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>Prézidan</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Kantik</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation>Prézidan a réinyon a simenn-la</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>Kantik</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation>Kantik</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation>Édisyon a Tou dè Gad la</translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>Awtik</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>Kondiktè</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>Lèktè</translation>
    </message>
</context>
<context>
    <name>family</name>
    <message>
        <source>Not set</source>
        <translation type="vanished">Pòkò chwazi</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>Sa ki ja fèt</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>Kantité simenn avan dat-la ki chwazi-la</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>Kantité simenn apré dat-la ki chwazi-la</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>Simenn</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation>Kantite simenn pou mèt an gri apré on sijé</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>Updates available. Do you want to synchronize?</source>
        <translation type="vanished">Ni on mizajou ki disponib. Ès ou vlé senkwonizé-y?</translation>
    </message>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>Ni sé menm chanjman la adan Klaoud-la (%1 ligne). Èskè ou vlé vrèman anrèjistré tout sé chanjman-la?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>Yo roumèt sé enfòwmasyon-la adan klaoud-la. Sé-la ki adan òdinatè-la kay chanjé osi. Ès ou vlé kontinyé?</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Pa bay pwochen lèson-la</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>Pòkò chwazi</translation>
    </message>
</context>
<context>
    <name>schoolview</name>
    <message>
        <source>H</source>
        <comment>abbreviation of the &apos;highlights&apos;</comment>
        <translation type="vanished">PI</translation>
    </message>
    <message>
        <source>R</source>
        <comment>abbreviation of the &apos;reader&apos;</comment>
        <translation type="vanished">L</translation>
    </message>
</context>
</TS>
