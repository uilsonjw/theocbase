#include "googlemediator.h"

googleMediator::googleMediator(QObject *parent, QString ClientID, QString ClientSecret)
    : QObject(parent), networkReply(nullptr), state(State_Authorizing), clientID(ClientID), clientSecret(ClientSecret), refreshToken(""), accessToken(""), tokenExpiration(QDateTime::currentDateTime()), currentJsonValid(false)
{
    QSettings settings;
    if (settings.contains("googlerefreshtoken")) {
        SimpleCrypt crypt(0);
        crypt.setKey(clientSecret);
        refreshToken = crypt.decryptToString(settings.value("googlerefreshtoken").toString());
    }
    googleAuth = new QOAuth2AuthorizationCodeFlow(this);
    if (!googleAuth->networkAccessManager())
        googleAuth->setNetworkAccessManager(new QNetworkAccessManager(googleAuth));
    googleAuth->setAuthorizationUrl(QUrl("https://accounts.google.com/o/oauth2/auth"));
    googleAuth->setAccessTokenUrl(QUrl("https://accounts.google.com/o/oauth2/token"));
    googleAuth->setClientIdentifier(clientID);
    googleAuth->setClientIdentifierSharedKey(clientSecret);
    googleAuth->setScope("https:%2F%2Fwww.googleapis.com%2Fauth%2Fgmail.send");
    googleAuth->setRefreshToken(refreshToken);

    accessToken = settings.value("google/token").toString();
    googleAuth->setToken(accessToken);
    connect(googleAuth, &QOAuth2AuthorizationCodeFlow::authorizeWithBrowser, this, &QDesktopServices::openUrl);
    connect(googleAuth, &QOAuth2AuthorizationCodeFlow::granted, this, [=]() {
        qDebug() << "Google authentication granted";
        accessToken = googleAuth->token();
        QSettings settings;
        settings.setValue("google/token", accessToken);
        this->setRefreshToken(googleAuth->refreshToken());
        tokenExpiration = googleAuth->expirationAt();        
        setState();
        emit statusChanged(googleAuth->status());
    });    
    connect(googleAuth, &QOAuth2AuthorizationCodeFlow::error, this, [=](const QString &error, const QString &errorDescription, const QUrl &uri) {
        qDebug() << "error" << error << errorDescription << uri;
    });
    googleAuth->setModifyParametersFunction([&](QAbstractOAuth::Stage stage, QVariantMap *parameters) {
        if (stage == QAbstractOAuth::Stage::RefreshingAccessToken) {
            parameters->insert("client_id", clientID);
            parameters->insert("client_secret", clientSecret);
        }
    });
    connect(googleAuth, &QOAuth2AuthorizationCodeFlow::statusChanged, this, &googleMediator::statusChanged);

    auto replyHandler = new QOAuthHttpServerReplyHandler(55739, this);
    replyHandler->setCallbackPath("google");    
    googleAuth->setReplyHandler(replyHandler);

    setState();
}

googleMediator::~googleMediator()
{
}

googleMediator::States googleMediator::GetUserAuthorization()
{
    googleAuth->grant();
    state = State_Authorizing;
    return state;
}

void googleMediator::setRefreshToken(QString token)
{
    if (token == nullptr || token.isEmpty())
        return;

    refreshToken = token;

    QSettings settings;
    SimpleCrypt crypt(0);
    crypt.setKey(clientSecret);
    settings.setValue("googlerefreshtoken", crypt.encryptToString(refreshToken));
}

QString googleMediator::getRefreshToken()
{
    return refreshToken;
}

googleMediator::States googleMediator::SendRequest(Tasks Task, QString Url, QByteArray *PostData)
{
    if (accessToken.isEmpty()) {
        return State_NeedCode;
    } else if (QDateTime::currentDateTime() > tokenExpiration && !refreshToken.isEmpty()) {
        if (refreshToken.isEmpty())
            return State_NeedTokenRefresh;

        accessToken.clear();

        QTimer timer;
        timer.setSingleShot(true);
        QEventLoop loop;

        connect(googleAuth, &QOAuth2AuthorizationCodeFlow::statusChanged, &loop, [&loop](QAbstractOAuth::Status status){
            qDebug() << "status changed";
            if (status == QAbstractOAuth::Status::Granted || status == QAbstractOAuth::Status::NotAuthenticated)
                loop.quit();
        });
        connect(googleAuth, &QOAuth2AuthorizationCodeFlow::requestFailed, &loop, [&loop](){
            qDebug() << "requesting failed";
            loop.quit();
        });
        connect(&timer, &QTimer::timeout, &loop, [this, &loop](){
            // cannot get a new refresh token
            accessToken = "";
            refreshToken = "";
            QSettings settings;
            settings.setValue("google/token", accessToken);
            settings.setValue("googlerefreshtoken", "");
            state = State_AuthorizationFailed;
            loop.quit();
        });

        googleAuth->refreshAccessToken();
        timer.start(10000);
        loop.exec();       
        timer.stop();
        if (state != State_OK)
            return state;
    }

    return _SendRequest(Task, Url, PostData);
}

googleMediator::States googleMediator::_SendRequest(Tasks Task, QString Url, QByteArray *PostData)
{
    currentJsonValid = false;
    QNetworkRequest request;
    request.setUrl(QUrl(Url));

    if (Task != AquireToken) {
        QByteArray auth = QString("Bearer %1").arg(accessToken).toLatin1();
        request.setRawHeader("Authorization", auth);
        qDebug() << "auth header" << auth;
    }

    switch (Task) {
    case Get:
        networkReply = googleAuth->networkAccessManager()->get(request);        
        break;
    case AquireToken:
    case Post:
        networkReply = googleAuth->networkAccessManager()->post(request, *PostData);        
        break;
    case PostWithJsonWrapper:
        QByteArray wrapper;
        wrapper.append("{\"raw\": \"");
        // The entire email message in an RFC 2822 formatted and base64url encoded string.
        // https://developers.google.com/gmail/api/v1/reference/users/messages/send
        wrapper.append(PostData->toBase64(QByteArray::Base64UrlEncoding));
        wrapper.append("\"}");
        request.setRawHeader("Content-type", "application/json");
        QByteArray bodySize;
        bodySize.append(QVariant(wrapper.size()).toString().toUtf8());
        request.setRawHeader("Content-length", bodySize);
        networkReply = googleAuth->networkAccessManager()->post(request, wrapper);
        break;
    }

    QEventLoop loop;
    connect(networkReply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
    loop.exec();

    QByteArray ret = networkReply->readAll();
    currentNetworkReplyErr = networkReply->errorString();    
    if (networkReply->error() != QNetworkReply::NoError) {
        qDebug() << networkReply->errorString();
        state = State_ReplyError;
    } else {
        currentJsonDoc = QJsonDocument::fromJson(ret, &currentJsonErr);
        if (currentJsonDoc.isNull()) {
            state = State_JsonError;
        } else {
            currentJson = currentJsonDoc.object();
            currentJsonValid = true;
            state = State_OK;
        }
    }
    networkReply->deleteLater();
    return state;
}

googleMediator::States googleMediator::getState()
{    
    return state;
}

QString googleMediator::getStateString()
{
    switch (state) {
    case State_OK:
        if (currentJsonValid) {
            if (currentJson.contains("error"))
                return currentJson.value("error").toObject().value("message").toString();
            else
                return tr("OK");
        } else
            return tr("OK but JSON not available");
    case State_Authorizing:
        return tr("Authorizing");
    case State_AuthorizationFailed:
        return tr("Authorization Failed");
    case State_ReplyError:
        return currentNetworkReplyErr;
    case State_JsonError:
        return currentJsonErr.errorString();
    case State_MissingClientID:
        return tr("Missing Client ID");
    case State_MissingClientSecret:
        return tr("Missing Client Secret");
    case State_NeedCode:
        return tr("Need Authorization Code");
    case State_NeedTokenRefresh:
        return tr("Need Token Refresh");
    }
    return "Unknown";
}

bool googleMediator::getCurrentJsonValid()
{
    return currentJsonValid;
}

QJsonObject *googleMediator::getCurrentJson()
{
    if (currentJsonValid)
        return &currentJson;
    else
        return nullptr;
}

QString googleMediator::getCurrentNetworkReplyError()
{
    return currentNetworkReplyErr;
}

QString googleMediator::getCurrentJsonErr()
{
    return currentJsonErr.errorString();
}

void googleMediator::setState()
{
    if (clientID.isEmpty()) {
        state = State_MissingClientID;
    } else if (clientSecret.isEmpty()) {
        state = State_MissingClientSecret;
    } else if (refreshToken.isEmpty()) {
        if (accessToken.isEmpty())
            state = State_NeedTokenRefresh;
        else
            state = State_NeedCode;
    } else {
        state = State_OK;
    }
}
