#include "lmm_assignment.h"
#include "lmm_schedule.h"
#include <QSet>
#include "sqlcombo.h"
LMM_Assignment::LMM_Assignment(QObject *parent)
    : LMM_Schedule(parent), m_classnumber(1), m_assistant_id(-1), m_volunteer_id(-1), m_assignment_db_id(-1), m_completed(false)
{
    m_schedule_db_id = -1;
}

LMM_Assignment::LMM_Assignment(int talkId, int sequence, QObject *parent)
    : LMM_Schedule(parent), m_classnumber(1), m_assistant_id(-1), m_volunteer_id(-1), m_assignment_db_id(-1), m_completed(false)
{
    m_talkid = talkId;
    m_sequence = sequence;
    m_schedule_db_id = -1;
}

LMM_Assignment::LMM_Assignment(int talkId, int sequence, int scheduleDbId, int assignmentDbId, QObject *parent)
    : LMM_Schedule(talkId, sequence, QDate(), "", "", 0, 0, scheduleDbId, parent), m_classnumber(1), m_assistant_id(-1), m_volunteer_id(-1), m_assignment_db_id(assignmentDbId), m_completed(false)
{
}

LMM_Assignment::~LMM_Assignment()
{
}

int LMM_Assignment::classnumber() const
{
    return m_classnumber < 1 ? 1 : m_classnumber;
}

void LMM_Assignment::setClassnumber(int n)
{
    m_classnumber = n < 1 ? 1 : n;
    emit classnumberChanged(m_classnumber);
}

person *LMM_Assignment::speaker() const
{
    return m_speaker;
}

void LMM_Assignment::setSpeaker(person *speaker)
{
    m_speaker = speaker;
    if (m_speaker)
        m_speaker->setParent(this);
    emit speakerChanged();
}

QString LMM_Assignment::speakerFullName() const
{
    if (talkId() == LMM_Schedule::TalkType_COTalk) {
        sql_class *sql = &Singleton<sql_class>::Instance();
        return sql->getSetting("circuitoverseer");
    } else {
        return (speaker() ? speaker()->fullname() : "");
    }
}

QString LMM_Assignment::note() const
{
    return m_note;
}

void LMM_Assignment::setNote(QString note)
{
    m_note = note;
    emit noteChanged(note);
}

SortFilterProxyModel *LMM_Assignment::getSpeakerList()
{
    QHash<int, QByteArray> roles;
    roles[MySortFilterProxyModel::MyRoles::id] = "id";
    roles[MySortFilterProxyModel::MyRoles::name] = "name";
    roles[MySortFilterProxyModel::MyRoles::date] = "date";
    roles[MySortFilterProxyModel::MyRoles::date_val] = "date_val";
    roles[MySortFilterProxyModel::MyRoles::date2] = "date2";
    roles[MySortFilterProxyModel::MyRoles::date2_val] = "date2_val";
    roles[MySortFilterProxyModel::MyRoles::icon] = "icon";

    QStandardItemModel *itemmodel = new QStandardItemModel(0, 5, this);
    itemmodel->setItemRoleNames(roles);
    itemmodel->setHorizontalHeaderItem(0, new QStandardItem("Id"));
    itemmodel->setHorizontalHeaderItem(1, new QStandardItem("Name"));
    itemmodel->setHorizontalHeaderItem(2, new QStandardItem("Date"));
    itemmodel->setHorizontalHeaderItem(3, new QStandardItem("Date2"));
    itemmodel->setHorizontalHeaderItem(4, new QStandardItem("Icon"));

    sql_class *sql = &Singleton<sql_class>::Instance();
    SqlComboHelper sch;

    sch.set_talk_conductor(static_cast<LMM_Schedule::TalkTypes>(talkId()));
    sch.set_classnumber(static_cast<short>(m_classnumber));
    sch.set_only_brothers(!canHaveAssistant());
    sch.set_date(LMM_Schedule::date());

    sql_items items = sql->selectSql(sch.get_sql());

    // empty row
    itemmodel->setRowCount(1);

    QVector<QList<int>> lists;
    lists.resize(3);
    loadPersonsOnBreak(lists[0]);
    loadMultiAssignedPersons(lists[1]);
    loadFamilyMembers(lists[2]);

    QVector<int> person_ids;
    QVector<short> pris;
    getPersonIdsFromQryResult(person_ids, items, "id");
    getPersonsIconAlertValues(pris, person_ids, lists);

    ccongregation c;

    QString nameFormat = sql->getSetting("nameFormat", "%2, %1");

    for (unsigned int i = 0; i < items.size(); i++) {
        QStandardItem *item = new QStandardItem();
        item->setData(items[i].value("id").toInt(), MySortFilterProxyModel::MyRoles::id);
        item->setData(nameFormat.arg(items[i].value("firstname").toString(),
                                     items[i].value("lastname").toString()),
                      MySortFilterProxyModel::MyRoles::name);
        QDate dateany = items[i].value("date_any").toDate();
        dateany = dateany.addDays(c.getMeetingDay(dateany, ccongregation::tms) - 1);
        QString str = dateany.toString("yyyy-MM-dd");
        if (str != "") {
            str += " " + LMM_Schedule::getStringTalkType(items[i].value("talk_id").toString().split(" ")[0].toInt());
            str += "c";
            str += items[i].value("classnumber_any").toString();
            str += (items[i].value("assignee_id").toInt() == 0 ? "S" : "A");
        }
        item->setData(str, MySortFilterProxyModel::MyRoles::date2);
        item->setData(dateany, MySortFilterProxyModel::MyRoles::date2_val);

        QDate datespecific = items[i].value("date_specific").toDate();
        datespecific = datespecific.addDays(c.getMeetingDay(datespecific, ccongregation::tms) - 1);
        str = datespecific.toString("yyyy-MM-dd");
        if (str != "") {
            str += " c";
            str += items[i].value("classnumber_specific").toString();
        }
        item->setData(str, MySortFilterProxyModel::MyRoles::date);
        item->setData(datespecific, MySortFilterProxyModel::MyRoles::date_val);
        item->setData(items[i].value("firstname"), MySortFilterProxyModel::MyRoles::h_firstname);
        item->setData(items[i].value("lastname"), MySortFilterProxyModel::MyRoles::h_lastname);
        item->setData(items[i].value("date_offset_any"), MySortFilterProxyModel::MyRoles::h_offset_any);
        item->setData(items[i].value("date_offset_specific"), MySortFilterProxyModel::MyRoles::h_offset_specific);
        switch (pris[static_cast<int>(i)]) {
        case 0:
            break;
        case 1:
            item->setData("qrc:///icons/warning_inactive.svg", MySortFilterProxyModel::MyRoles::icon);
            break;
        case 2:
            item->setData("qrc:///icons/warning_busy.svg", MySortFilterProxyModel::MyRoles::icon);
            break;
        case 3:
            item->setData("qrc:///icons/warning_family_member.svg", MySortFilterProxyModel::MyRoles::icon);
            break;
        }

        itemmodel->setRowCount(itemmodel->rowCount() + 1);
        itemmodel->setItem(static_cast<int>(i) + 1, item);
    }

    SortFilterProxyModel *sortmodel = new MySortFilterProxyModel(this, itemmodel, MySortFilterProxyModel::MyRoles::name, "speaker");
    return sortmodel;
}

QString LMM_Assignment::getReminderText() const
{
    QDate d = date();
    ccongregation c;
    d = d.addDays(c.getMeetingDay(d, ccongregation::tms) - 1);

    QString rowTemplate("%1 : %2\n");
    QString text(tr("Please find below details of your assignment:") + "\n\n");
    text.append(rowTemplate.arg(tr("Date"), d.toString(Qt::DefaultLocaleShortDate)));
    text.append(rowTemplate.arg(tr("Name"), (speaker() ? speakerFullName() : "")));
    text.append(rowTemplate.arg(tr("Assignment"), LMM_Schedule::getFullStringTalkType(talkId())) + "\n");
    text.append(rowTemplate.arg(tr("Theme"), theme()) + "\n");
    text.append(rowTemplate.arg(tr("Source material"), source()));
    return text;
}

bool LMM_Assignment::save()
{
    // should to save schedule??
    // LLM_Schedule::save();

    sql_class *sql = &Singleton<sql_class>::Instance();
    qDebug() << "save init" << date() << talkId() << m_schedule_db_id;

    if (!date().isValid() || talkId() < 1 || m_schedule_db_id < 1)
        return false;
    qDebug() << "save";

    if (talkId() == LMM_Schedule::TalkType_COTalk) {
        LMM_Schedule::save();
    }

    sql_item queryitems;
    queryitems.insert(":schedule_id", m_schedule_db_id);
    queryitems.insert(":classnumber", m_classnumber);
    int id = sql->selectScalar("SELECT id FROM lmm_assignment WHERE lmm_schedule_id = :schedule_id AND classnumber = :classnumber", &queryitems, -1).toInt();

    sql_item insertitems;
    insertitems.insert("date", date());
    insertitems.insert("lmm_schedule_id", m_schedule_db_id);
    insertitems.insert("classnumber", m_classnumber);
    insertitems.insert("assignee_id", speaker() ? speaker()->id() : -1);
    insertitems.insert("volunteer_id", m_volunteer_id);
    insertitems.insert("assistant_id", m_assistant_id);
    insertitems.insert("completed", m_completed);
    insertitems.insert("note", note());
    insertitems.insert("timing", m_timing);
    insertitems.insert("setting", m_setting);

    if (id > 0) {
        // update
        return sql->updateSql("lmm_assignment", "id", QString::number(id), &insertitems);
    } else {
        // insert new row
        return (sql->insertSql("lmm_assignment", &insertitems, "id") > 0);
    }
}

QList<int> &LMM_Assignment::loadPersonsOnBreak(QList<int> &ret, const QDate &dt)
{
    QString s = "SELECT person_id FROM unavailables WHERE start_date <='%1' AND end_date >= '%1' AND active";

    if (!ret.empty())
        ret.clear();

    sql_class *sql = &Singleton<sql_class>::Instance();
    s.replace("%1", dt.toString(Qt::ISODate), Qt::CaseSensitive);
    sql_items items = sql->selectSql(s);

    for (unsigned int i = 0; i < items.size(); i++) {
        ret.append(items.at(i).value("person_id").toInt());
    }

    return ret;
}

QList<int> &LMM_Assignment::loadMultiAssignedPersons(QList<int> &ret, const QDate &dt)
{
    if (!ret.empty())
        ret.clear();

    QString s = "";
    s += "select a.assignee_id person_id from (";
    s += "  select a.id,a.classnumber,a.assignee_id from lmm_schedule s,lmm_assignment a";
    s += "  where a.lmm_schedule_id = s.id and s.date = '%1'";
    s += "  union all";
    s += "  select a.id,a.classnumber,a.assistant_id from lmm_schedule s,lmm_assignment a";
    s += "  where a.lmm_schedule_id = s.id and s.date = '%1' and a.assistant_id > -1 and a.volunteer_id = -1";
    s += "  union all";
    s += "  select a.id,a.classnumber,a.volunteer_id from lmm_schedule s,lmm_assignment a";
    s += "  where a.lmm_schedule_id = s.id and s.date = '%1' and a.assistant_id = -1 and a.volunteer_id > -1";
    s += "  ) a";
    s += "  group by a.assignee_id";
    //s += "  having count(a.assignee_id) > 1";
    sql_class *sql = &Singleton<sql_class>::Instance();
    s.replace("%1", dt.toString(Qt::ISODate), Qt::CaseSensitive);
    sql_items items = sql->selectSql(s);

    for (unsigned int i = 0; i < items.size(); i++) {
        ret.append(items.at(i).value("person_id").toInt());
    }

    return ret;
}

QList<int> &LMM_Assignment::loadFamilyMembers(QList<int> &ret, const QDate &dt)
{
    if (!ret.empty())
        ret.clear();
    QString s = "";
    QString w = "(4, 6, 7, 8, 9, 10, 11)";
    sql_class *sql = &Singleton<sql_class>::Instance();

    s += "select f2.person_id person_id from ( ";
    s += " select a.id,a.classnumber,a.assignee_id ";
    s += " from lmm_schedule s,lmm_assignment a where ";
    s += " talk_id/10 in ";
    s += w;
    s += " and a.lmm_schedule_id = s.id and s.date = '%1' ";
    s += " union all ";
    s += " select a.id,a.classnumber,a.assistant_id from ";
    s += " lmm_schedule s,lmm_assignment a where talk_id/10 in ";
    s += w;
    s += " and a.lmm_schedule_id = s.id and s.date = '%1'";
    s += " and a.assistant_id > -1 and a.volunteer_id = -1";
    s += " union all ";
    s += " select a.id,a.classnumber,a.volunteer_id from ";
    s += " lmm_schedule s,lmm_assignment a where talk_id/10 in ";
    s += w;
    s += " and a.lmm_schedule_id = s.id and s.date = '%1' ";
    s += " and a.assistant_id = -1 and a.volunteer_id > -1";
    s += " ) a ,families f,families f2 where f.person_id = a.assignee_id ";
    s += " and f2.family_head = f.family_head";

    s.replace("%1", dt.toString(Qt::ISODate), Qt::CaseSensitive);

    sql_items items = sql->selectSql(s);

    for (unsigned int i = 0; i < items.size(); i++) {
        ret.append(items.at(i).value("person_id").toInt());
    }

    return ret;
}

void LMM_Assignment::getPersonsIconAlertValues(QVector<short> &dst, const QVector<int> &person_ids, const QVector<QList<int>> &pri_person_ids)
{
    QMap<int, short> person_map;
    dst.clear();
    for (auto i = pri_person_ids.begin(); i != pri_person_ids.end(); ++i)
        for (auto &x : *i)
            if (!person_map.contains(x))
                person_map.insert(x, static_cast<short>(i - pri_person_ids.begin() + 1));
    for (auto &x : person_ids)
        if (person_map.contains(x))
            dst.push_back(person_map[x]);
        else
            dst.push_back(0);
}

void LMM_Assignment::getPersonIdsFromQryResult(QVector<int> &person_ids, const sql_items &items, const QString &person_fldnm)
{
    person_ids.clear();
    for (auto &x : items)
        person_ids.push_back(x.value(person_fldnm).toInt());
}

bool LMM_Assignment::MySortFilterProxyModel::lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const
{
    int row1 = source_left.row();
    int row2 = source_right.row();

    if (row1 == 0 || row2 == 0)
        return row1 < row2;
    bool ret = false;

    switch (this->sortRole()) {
    case MySortFilterProxyModel::MyRoles::name:
        break;
    case MySortFilterProxyModel::MyRoles::date2: {
        auto val1 = sourceModel()->data(source_left, MySortFilterProxyModel::MyRoles::date2_val).toDate();
        auto val2 = sourceModel()->data(source_right, MySortFilterProxyModel::MyRoles::date2_val).toDate();
        if (val1 > val2)
            return false;
        ret = val1 < val2;
        break;
    }
    case MySortFilterProxyModel::MyRoles::date: {
        auto val1 = sourceModel()->data(source_left, MySortFilterProxyModel::MyRoles::date_val).toDate();
        auto val2 = sourceModel()->data(source_right, MySortFilterProxyModel::MyRoles::date_val).toDate();
        if (val1 > val2)
            return false;
        ret = val1 < val2;
        break;
    }
    default:
        return SortFilterProxyModel::lessThan(source_left, source_right);
    }
    if (!ret) {
        QVariant val1 = sourceModel()->data(source_left, MySortFilterProxyModel::MyRoles::h_lastname);
        QVariant val2 = sourceModel()->data(source_right, MySortFilterProxyModel::MyRoles::h_lastname);
        if (val1.toString() > val2.toString())
            return false;
        ret = val1.toString() < val2.toString();
    }
    if (!ret) {
        QVariant val1 = sourceModel()->data(source_left, MySortFilterProxyModel::MyRoles::h_firstname);
        QVariant val2 = sourceModel()->data(source_right, MySortFilterProxyModel::MyRoles::h_firstname);
        if (val1.toString() > val2.toString())
            return false;
        ret = val1.toString() < val2.toString();
    }
    if (!ret) {
        QVariant val1 = sourceModel()->data(source_left, MySortFilterProxyModel::MyRoles::id);
        QVariant val2 = sourceModel()->data(source_right, MySortFilterProxyModel::MyRoles::id);
        if (val1.toString() > val2.toString())
            return false;
        ret = val1.toString() < val2.toString();
    }
    return ret;
}
