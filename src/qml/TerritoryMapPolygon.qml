import QtQuick 2.5
import QtLocation 5.6
import QtGraphicalEffects 1.0
import QtPositioning 5.8

MapPolygon {
    id: myMapPolygon
    border.color: isSelected ? "#ff0000" : "#ff00ff"
    border.width: 2
    //color: "#ff0000"
    smooth: true
    opacity: 0.25
    visible: false

    property int territoryId
    property bool isSelected: false
    property bool isHole: false
    property variant boundingBox: QtPositioning.rectangle();
    property int showBoundary

    states: [
//        State {
//            name: "IsEnclave"; when: isHole
//            PropertyChanges { target: myMapPolygon; color: "#00ffff" }
//        }
        State {
            name: "IsSelected"; when: isSelected && showBoundary >= 1
            PropertyChanges { target: myMapPolygon; opacity: 0.75; border.width: 4; visible: true }
        },
        State {
            name: "IsNotSelected"; when: !isSelected && showBoundary === 2
            PropertyChanges { target: myMapPolygon; opacity: 0.5; border.width: 3; visible: true }
        },
        State {
            name: "Hidden"; when: !isSelected && showBoundary < 2
            PropertyChanges { target: myMapPolygon; opacity: 0; border.width: 10; visible: false }
        }
        ]

    function destroy(){
        myMapPolygon.destroy();
    }

    function refresh(showBoundaries){
        showBoundary = showBoundaries
    }

    Connections {
        target: territoryTreeModel
        function onSelectedChanged() {
            if (territoryTreeModel.selectedTerritory)
            {
                isSelected = territoryTreeModel.selectedTerritory.territoryId === territoryId ? true : false
                if (isSelected)
                {
                    map.currentBoundary = myMapPolygon
                }
            }
            else
                isSelected = false
        }
    }

    Component.onCompleted: {
        if (territoryTreeModel.selectedTerritory)
        {
            isSelected = territoryTreeModel.selectedTerritory.territoryId === territoryId ? true : false
            map.currentBoundary = myMapPolygon
        }
        else
            isSelected = false
    }

    MouseArea {
        id: mousearea
        anchors.fill:parent
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        //drag.target: parent
        propagateComposedEvents: true

        onClicked: {
            if (map.isDigitizing)
            {
                mouse.accepted = false;
                return;
            }

            if (mouse.button === Qt.RightButton) {
                contextMenu.show(null, territoryId, -1);
                return;
            } else if (!isHole) {
                var index = territoryTreeModel.index(territoryId)
                territoryTreeModel.setSelectedTerritory(index)
                map.currentBoundary = myMapPolygon
            }
        }
    }
}
