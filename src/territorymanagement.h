/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2016, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TERRITORYMANAGEMENT_H
#define TERRITORYMANAGEMENT_H

#include <QDialog>
#include <QQmlContext>
#include "accesscontrol.h"
#include "cpersons.h"
#include "cterritories.h"
#include "territory.h"
#include "territoryassignment.h"
#include "territorystreet.h"
#include "territoryaddress.h"

namespace Ui {
class territorymanagement;
}

class TerritoryTreeItem
{
public:
    TerritoryTreeItem(const QList<QVariant> &data, TerritoryTreeItem *parent = nullptr);
    ~TerritoryTreeItem();

    TerritoryTreeItem *child(int row);
    int childCount() const;
    int columnCount() const;
    QVariant data(int column) const;
    int row() const;
    void appendChild(TerritoryTreeItem *child);
    bool insertChildren(int position, int count);
    bool removeChildren(int position, int count);
    TerritoryTreeItem *parent();
    bool setData(int column, const QVariant &value);

private:
    QList<TerritoryTreeItem *> childItems;
    TerritoryTreeItem *parentItem;
    QList<QVariant> itemData;
};

class TerritoryTreeModel : public QAbstractItemModel
{
    Q_OBJECT
    Q_PROPERTY(int groupByIndex READ groupByIndex WRITE setGroupByIndex NOTIFY notification)
    Q_PROPERTY(territory *selectedTerritory READ selectedTerritory NOTIFY notification)

public:
    enum Role {
        TerritoryRole = Qt::UserRole,
        TerritoryIdRole,
        TerritoryNumberRole,
        LocalityRole,
        CityIdRole
    };

    enum GroupByTypes {
        GroupByCity,
        GroupByPublisher,
        GroupByTerritoryType,
        GroupByWorkedThrough
    };

    TerritoryTreeModel(QObject *parent = nullptr);
    ~TerritoryTreeModel();

    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;
    QHash<int, QByteArray> groupByNames() const;

    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;
    Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) Q_DECL_OVERRIDE;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    Q_INVOKABLE QModelIndex index(int territoryId) const;
    QModelIndex parent(const QModelIndex &index) const Q_DECL_OVERRIDE;

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;

    bool insertRows(int position, int rows,
                    const QModelIndex &parent = QModelIndex()) Q_DECL_OVERRIDE;

    bool removeRows(int position, int rows,
                    const QModelIndex &parent = QModelIndex()) Q_DECL_OVERRIDE;

    int groupByIndex() { return m_groupByIndex; }
    void setGroupByIndex(int value);
    Q_INVOKABLE void updateModel();

    territory *selectedTerritory() const { return m_selectedTerritory; }
    Q_INVOKABLE void setSelectedTerritory(const QModelIndex &selectedIndex);

    Q_INVOKABLE void addNewTerritory();
    Q_INVOKABLE void removeTerritory(int territoryId);
    QModelIndex getGroupNodeIndex(QVariant value) const;

signals:
    void notification();
    void saveSelected();
    void selectedChanged(const QModelIndex &value = QModelIndex());
    void territoryRemoved();

private slots:
    void changeTerritoryNumber(int territoryId, int territoryNumber);
    void changeLocality(int territoryId, QString locality);
    void changeCity(int territoryId, int cityId);
    void changeType(int territoryId, int typeId);

private:
    void setupModelData(TerritoryTreeItem *parent);
    TerritoryTreeItem *getItem(const QModelIndex &index) const;
    TerritoryTreeItem *rootItem;
    int m_groupByIndex;
    territory *m_selectedTerritory;
    QHash<int, QString> workedThroughNames;
};

class TerritoryTreeSFProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(QObject *source READ source WRITE setSource)
    Q_PROPERTY(QByteArray sortRole READ sortRole WRITE setSortRole NOTIFY sortChanged)
    Q_PROPERTY(Qt::SortOrder sortOrder READ sortOrder WRITE setSortOrder NOTIFY sortChanged)

public:
    explicit TerritoryTreeSFProxyModel(QObject *parent = nullptr);

    QObject *source() const;
    void setSource(QObject *source);

    QByteArray sortRole() const;
    void setSortRole(const QByteArray &role);

    void setSortOrder(Qt::SortOrder order);

    Q_INVOKABLE QVariantMap get(int row);

signals:
    void sortChanged();

protected:
    int roleKey(const QByteArray &role) const;
    QHash<int, QByteArray> roleNames() const;
};

class territorymanagement : public QDialog
{
    Q_OBJECT

public:
    explicit territorymanagement(QWidget *parent = nullptr);
    ~territorymanagement();

protected:
    void closeEvent(QCloseEvent *event);

private slots:
    void on_buttonBox_clicked(QAbstractButton *button);

private:
    Ui::territorymanagement *ui;
    sql_class *sql;
    AccessControl *ac;
    TerritoryTreeModel *territoryTreeModel;
    TerritoryAssignmentModel *assignmentModel;
    TerritoryAddressModel *addressModel;
    DataObjectListModel *typeListModel;
    DataObjectListModel *cityListModel;
    TerritoryStreetModel *streetModel;
};

#endif // TERRITORYMANAGEMENT_H
