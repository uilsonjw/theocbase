<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fi">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation>Nimi</translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation>Päiväys</translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Älä määrää seuraavaa tutkielmaa</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Jätä nykyiseen tutkielmaan</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Puutteellisia tietoja</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Lisätäänkö ajoitus?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Teema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Lähdeaineisto</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Oppilas</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Avustaja</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Tulokset</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Suoritettu</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Sijainen</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Valitse sijainen</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Nykyinen tutkielma</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Harjoitukset tehty</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Seuraava tutkielma</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Valitse seuraava tutkielma</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Ajoitus</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Huomautuksia</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Koulun tiedot</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Käynnistä ajanotto</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Sammuta ajanotto</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Asetelma</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Valitse asetelma</translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>No exception</source>
        <translation>Ei poikkeusta</translation>
    </message>
    <message>
        <source>Circuit overseer&#x27;s visit</source>
        <translation>Kierrosvalvojan vierailu</translation>
    </message>
    <message>
        <source>Circuit assembly</source>
        <translation>Kierroskonventti</translation>
    </message>
    <message>
        <source>Regional convention</source>
        <translation>Aluekonventti</translation>
    </message>
    <message>
        <source>Memorial</source>
        <translation>Muistojuhla</translation>
    </message>
    <message>
        <source>Other exception</source>
        <translation>Muu poikkeus</translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation>%1 alkava viikko</translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation>Kokouspäivät</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Kokous viikolla</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Kokous viikonloppuna</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Teema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Aineisto</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Johtaja</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Puhuja</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lukija</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Huomautuksia</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Tiedot</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <comment>Page title</comment>
        <translation>Huomatukset</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>JUMALAN SANAN AARTEITA</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>VALMENNUSTA KENTTÄPALVELUKSEEN</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>ELÄMÄ KRISTITTYNÄ</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Puheenjohtaja</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Neuvoja</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Laulu %1 ja rukous</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Laulu</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Johtaja</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lukija</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Alkusanat</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Rukous</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation>Tuo aikataulu...</translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation>PS</translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation>R1</translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation>R2</translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation>Loppusanat</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Kokous viikolla</translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Jätä nykyiseen tutkielmaan</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Käynnistä ajanotto</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Pysäytä ajanotto</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Lisätäänkö ajoitus?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Teema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Aineisto</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Oppilas</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Tulokset</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Suoritettu</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Ajoitus</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Nykyinen tutkielma</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Harjoitukset tehty</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Seuraava tutkielma</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Valitse seuraava tutkielma</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Huomatukset</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Avustaja</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Sijainen</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>Ajanotto</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>Avustajan ei tulisi olla vastakkaista sukupuolta.</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>Tutkielma</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Tiedot</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>Päivitä vetämällä...</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Päivitä vapauttamalla...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation>Käyttäjänimi</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Salasana</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Kirjaudu sisään</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Käyttäjänimi tai sähköposti</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Sähköposti</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Luo tili</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>Nollaa salasana</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>Sähköpostiosoitetta ei löydy!</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Unohdin salasanan</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>Kirjautumissivu</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation>%1 alkava viikko</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation>Huomiot</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation>PUHUJAT ESITELMÄMATKALLA</translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>%1 puhujamatkalla tänä viikonloppuna</numerusform>
            <numerusform>%1 puhujamatkalla tänä viikonloppuna</numerusform>
        </translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation>Ei ketään puhujamatkalla tänä viikonloppuna</translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation>Tulostusasetukset</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Ohjelma</translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation>Työsivut</translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation>Puhujat esitelmämatkalla</translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation>Puhujat esitelmämatkalla - tehtävämääräys</translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation>Soittolista ja vieraanvaraisuus aikataulu</translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation>Puhujien esitelmät</translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Tulosta</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Kokous viikolla</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Kokous viikonloppuna</translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation>Yhdistelmä</translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation>Tehtävämääräykset</translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation>Tulostuspohja</translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation>Avustajan tehtävämääräykset</translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation>Älä tulosta tyhjiä</translation>
    </message>
    <message>
        <source>Text size</source>
        <translation>Tekstin koko</translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation>Muokatut tulostuspohjat</translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Laulu ja rukous</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Laulu %1 ja rukous</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>ESITELMÄ</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>VARTIOTORNIN TUTKISTELU</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Laulu %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Johtaja</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lukija</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation>Tuo VT...</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Kokous viikonloppuna</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Kokous viikolla</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>Teema</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>Seurakunta</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Puhuja</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Mobiili</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Puhelin</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Sähköposti</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Tietoja</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Vieraanvaraisuus</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>Esitelmä</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>Etunimi</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Sukunimi</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Veli</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Sisar</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Nimitetty</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Perhe</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>Liitetty perheenjäseneen:</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Yhteystiedot</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Puhelin</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Sähköposti</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>Kaikki luokat</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Vain pääsali</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Vain rinnakkaisluokat</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Perheenpää</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Avustaja</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Tietoja</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>Aktiivinen</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Puheenjohtaja</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>Jumalan sanan aarteita</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Raamatun lukeminen</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Ensimmäinen käynti</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Uusintakäynti</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Raamattukurssi</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>Elämä kristittynä puheet</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Seurakunnan raamatuntutkistelu</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Srk raamatuntutkistelun lukija</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>Uusi julistaja</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Rukous</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation>Vieraanvaraisuus</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Mobiili</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Kokous viikolla</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>Puhe</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Kokous viikonloppuna</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>Vartiotornin tutkistelun johtaja</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation>Vartiotornin tutkistelun lukija</translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation>Hengellisiä helmiä</translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation>Keskustelu videosta</translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Julistajat</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation>Valintalista</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Asetukset</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Kirjaudu ulos</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Tietoja</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Versio</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>TheocBase kotisivu</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Palaute</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>Viimeksi synkronoitu %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Aikataulu</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Näytä ajat</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Näytä kesto</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Käyttöliittymä</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Kieli</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Kirjaudu</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Sähköposti</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nimi</translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation>Näytä laulujen teemat</translation>
    </message>
    <message>
        <source>Printing</source>
        <translation>Tulostus</translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation>Muokatut tulostuspohjat</translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Synkronoidaan...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>Puheenjohtaja</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Laulu</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation>Viikonloppukokouksen puheenjohtaja</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>Laulu</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation>Vartiotornin laulu</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Rukous</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation>Vartiotornin numero</translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>Artikkeli</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Teema</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>Johtaja</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>Lukija</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>Aikajana</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>Viikot ennen valittua päivää</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>Viikot valitun päivän jälkeen</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>viikkoa</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation>Viikot harmaana tehtävän jälkeen</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>Samoja muutoksia löytyy sekä paikallisesti että pilvessä (%1 riviä). Haluatko säilyttää paikalliset muutokset?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>Pilvessä olevat tiedot on poistettu. Paikalliset tiedot korvataan pilvessä olevilla tiedoilla. Haluatko jatkaa?</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Älä määrää seuraavaa tutkielmaa</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>Ei asetettu</translation>
    </message>
</context></TS>