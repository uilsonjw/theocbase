/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2016, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import QtQuick.Window 2.1
import QtQuick.Controls 1.5
import QtQml.Models 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import QtPositioning 5.8
import QtLocation 5.8
import net.theocbase 1.0

Rectangle {
    id: territoryManagement

    property bool canEditTerritories: accessControl.user && accessControl.user.hasPermission(PermissionRule.CanEditTerritories)
    property bool canViewTerritoryAssignments: accessControl.user && accessControl.user.hasPermission(PermissionRule.CanViewTerritoryAssignments)
    property bool canViewTerritoryAddresses: accessControl.user && accessControl.user.hasPermission(PermissionRule.CanViewTerritoryAddresses)
    property string territoryNumber: ""
    property string territoryLocality: ""
    property int cityIndex: 0
    property int territoryTypeIndex: 0
    property string territoryRemark: ""
    property string dateFormat: Qt.locale().dateFormat(Locale.ShortFormat)
    property string dateRegexFormat: "^" + dateFormat.replace(/[./-]/g, "[$&]").replace(/(dd|d|MM|M)/g,"\\d{1,2}").replace(/(yy)/g,"\\d{2}") + "$"
    //property int fontsize: Qt.platform.os === "windows" ? appinfo.fontsize+1 : appinfo.fontsize
    property bool isTerritorySelected: false
    property DataObjectListModel addressTypeListModel: territories.getAddressTypes()
    property DataObjectListModel streetTypeListModel: territories.getStreetTypes()
    property DataObjectListModel publisherListModel: assignmentModel.getPublisherList()

    signal addTerritory()
    signal updateMarkers(var dataChanged)
    signal zoomSelectedAddress()
    signal updateStreets(var dataChanged)
    signal zoomSelectedStreet()

    function saveSelectedTerritory(){
        if (territoryTreeModel.selectedTerritory)
        {
            territoryTreeModel.selectedTerritory.save()
            assignmentModel.saveAssignments()
            streetModel.saveStreets()
            addressModel.saveAddresses()
        }
    }

    visible: true
    color: myPalette.window

    SystemPalette { id: myPalette; colorGroup: SystemPalette.Active }

    Connections {
        target: territoryTreeModel
        function onSaveSelected() {
            saveSelectedTerritory()
        }
        function onSelectedChanged(value) {
            territoryLocality = territoryTreeModel.selectedTerritory ? territoryTreeModel.selectedTerritory.locality : ""
            territoryNumber = territoryTreeModel.selectedTerritory ? territoryTreeModel.selectedTerritory.territoryNumber : ""
            cityIndex = territoryTreeModel.selectedTerritory ? cityListModel.getIndex(territoryTreeModel.selectedTerritory.cityId) : 0
            territoryTypeIndex = territoryTreeModel.selectedTerritory ? typeListModel.getIndex(territoryTreeModel.selectedTerritory.typeId) : 0
            territoryRemark = territoryTreeModel.selectedTerritory ? territoryTreeModel.selectedTerritory.remark : ""

            assignmentModel.saveAssignments()
            assignmentModel.loadAssignments(territoryTreeModel.selectedTerritory ? territoryTreeModel.selectedTerritory.territoryId : 0)

            streetModel.saveStreets()
            streetProxyModel.setFilterTerritoryId(territoryTreeModel.selectedTerritory ? territoryTreeModel.selectedTerritory.territoryId : 0)
            if (!streetModel.selectedStreet || !territoryTreeModel.selectedTerritory || streetModel.selectedStreet.territoryId !== territoryTreeModel.selectedTerritory.id)
                streetModel.setSelectedStreet();
            updateStreets(false)

            addressModel.saveAddresses()
            addressProxyModel.setFilterTerritoryId(territoryTreeModel.selectedTerritory ? territoryTreeModel.selectedTerritory.territoryId : 0)
            if (!addressModel.selectedAddress || !territoryTreeModel.selectedTerritory || addressModel.selectedAddress.territoryId !== territoryTreeModel.selectedTerritory.id)
                addressModel.setSelectedAddress();
            updateMarkers(false)

            treeViewSelection.setCurrentIndex(territoryTreeProxyModel.mapFromSource(value), ItemSelectionModel.ClearAndSelect)
            isTerritorySelected = territoryTreeModel.selectedTerritory ? true : false
        }
    }

    NumberAnimation {
        id: invalidDataAnimation
        property: "opacity";
        duration: 300;
        loops: 4;
        easing.type: Easing.InOutElastic;}

    Territories {
        id: territories
    }

    SplitView {
        id: splitView1
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0
        anchors.fill: parent
        orientation: Qt.Horizontal

        ColumnLayout {
            id:col1
            width: 300
            spacing: 10
            Layout.fillHeight: true
            Layout.rightMargin: 5

            RowLayout{
                Layout.fillWidth: true
                spacing: 10
                Label{
                    id: lbl
                    text:qsTr("Group by:")}
                ComboBox{
                    id: cbTerritoryGroupBy
                    width: 300

                    model: ListModel {
                        id: cbItems
                        ListElement { text: qsTr("City", "Group territories by city"); fieldName: "city" }
                        ListElement { text: qsTr("Publisher", "Group territories by publisher"); fieldName: "publisher" }
                        ListElement { text: qsTr("Type", "Group territories by type of the territory"); fieldName: "type_id" }
                        ListElement { text: qsTr("Worked through", "Group territories by time frame they have been worked through"); fieldName: "workedthrough" }
                    }

                    visible: true

                    onCurrentIndexChanged: {
                        territoryTreeModel.groupByIndex = cbTerritoryGroupBy.currentIndex
                    }
                    Connections {
                        target: territoryTreeModel
                        function onNotification() { cbTerritoryGroupBy.currentIndex = groupByIndex; }
                    }
                }
            }

            TreeView {
                id: territoryTreeView

                Layout.fillWidth: true
                Layout.fillHeight: true
                sortIndicatorVisible: true

                model: TerritoryTreeSFProxyModel {
                    id: territoryTreeProxyModel
                    source: territoryTreeModel //sourceModel.count > 0 ? sourceModel : null

                    sortOrder: territoryTreeView.sortIndicatorOrder
                    sortCaseSensitivity: Qt.CaseInsensitive
                    sortRole: territoryTreeView.getColumn(territoryTreeView.sortIndicatorColumn).role
                }

                Connections {
                    target: territoryTreeProxyModel
                    function onSortOrderChanged() {
                        var selIndex = territoryTreeProxyModel.mapToSource(treeViewSelection.currentIndex)
                        treeViewSelection.clear()
                        territoryTreeModel.setSelectedTerritory(selIndex)
                    }
                }

                selection: ItemSelectionModel {
                    model: territoryTreeProxyModel
                    id : treeViewSelection
                    //currentIndex: territoryTreeIndex
                    //currentIndex: territoryTreeModel.index(territoryTreeIndex, 0)

                    onCurrentIndexChanged: {
                        //console.log("selected territory-id: " + currentIndex.parent.row)
                        //territoryTreeView.expand(currentIndex)
                        //territoryTreeIndex = currentIndex
                        //console.log("selected territoryTreeIndex: " + territoryTreeIndex)
                    }
                }

                itemDelegate: Item {
                    Text {
                        anchors.left: parent.left
                        anchors.verticalCenter: parent.verticalCenter
                        width: parent.width
                        color: styleData.textColor
                        elide: styleData.elideMode
                        text: styleData.value
                        renderType: Text.NativeRendering
                    }
                }

                rowDelegate: Rectangle {
                    height: 25
                    color: styleData.selected ? myPalette.highlight : (styleData.alternate? myPalette.alternateBase : myPalette.base)
                }

                TableViewColumn {
                    title: qsTr("Number", "Territory number according to the S-12 Territory Map Card")
                    role: "territory_number"
                    width: 150
                    elideMode: Text.ElideRight
                }
                TableViewColumn {
                    title: qsTr("Locality", "Locality according to the S-12 Territory Map Card")
                    role: "locality"
                    width: 150
                    elideMode: Text.ElideRight
                }
                onClicked: {
                    if (index.parent.row >= 0) {
                        territoryTreeModel.setSelectedTerritory(territoryTreeProxyModel.mapToSource(index))
                    }
                }
            }

            RowLayout{
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                spacing: 10
                visible: canEditTerritories

                ToolButton{
                    id: addTerritoryButton

                    Layout.minimumWidth: 24
                    Layout.minimumHeight: 24
                    tooltip: qsTr("Add new territory")
                    iconSource: "qrc:///icons/territory_add.svg"
                    opacity: parent.enabled ? 1.0 :0.5

                    onClicked: {
                        saveSelectedTerritory()
                        territoryTreeModel.addNewTerritory()
                    }
                }
                ToolButton{
                    id: removeTerritoryButton

                    enabled: isTerritorySelected
                    Layout.minimumWidth: 24
                    Layout.minimumHeight: 24
                    tooltip: qsTr("Remove selected territory")
                    iconSource: "qrc:///icons/territory_remove.svg"
                    opacity: parent.enabled ? 1.0 :0.5

                    onClicked: {
                        if (territoryTreeModel.selectedTerritory)
                            territoryTreeModel.removeTerritory(territoryTreeModel.selectedTerritory.territoryId)
                    }
                }
            }
        }

        ColumnLayout{
            Layout.fillHeight: true
            Layout.leftMargin: 10
            Layout.rightMargin: 10
            Layout.topMargin: 10

            spacing: 10

            GroupBox {
                id: groupBox
                title: "Territory"
                Layout.fillWidth: true
                Layout.minimumWidth: 100
                //Layout.fillHeight: true
                enabled: isTerritorySelected

                GridLayout{
//                        Layout.fillHeight: true
//                        Layout.fillWidth: true
                    columnSpacing: 10
                    rowSpacing: 10
                    anchors.rightMargin: 5
                    anchors.leftMargin: 5
                    anchors.bottomMargin: 5
                    anchors.topMargin: 10
                    anchors.fill: parent
                    rows: 2
                    columns: 5
                    Label{
                        text:qsTr("No.:")
                    }

                    TextField {
                        id: territoryNumberTextField

                        width: 150
                        Layout.fillWidth: true
                        Layout.maximumWidth: 150
                        text: territoryNumber
                        validator: IntValidator {bottom: 1; top: 999;}
                        placeholderText: qsTr("Number")
                        readOnly: !canEditTerritories

                        onTextChanged:{
                            territoryNumber = text
                            if (territoryTreeModel.selectedTerritory)
                                territoryTreeModel.selectedTerritory.territoryNumber = text
                        }
                    }

                    Label{
                        text:qsTr("Locality:")
                    }

                    TextField {
                        id: localityTextField

                        width: 200
                        text: territoryLocality
                        Layout.maximumWidth: 300
                        Layout.fillWidth: true
                        placeholderText: qsTr("Locality")
                        readOnly: !canEditTerritories

                        onTextChanged: {
                            territoryLocality = text
                            if (territoryTreeModel.selectedTerritory)
                                territoryTreeModel.selectedTerritory.locality = text
                        }
                    }

                    TextArea {
                        id: remarkTextField
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                        Layout.rowSpan: 2
                        textMargin: 2
                        readOnly: !canEditTerritories

                        //width: 80
                        text: territoryRemark

                        Text{
                            text: qsTr("Remark")
                            color: myPalette.mid
                            renderType: Text.NativeRendering
                            anchors{
                                top: parent.top
                                topMargin: 2
                                left: parent.left
                                leftMargin: 4
                            }
                            visible: (remarkTextField.activeFocus || remarkTextField.text.length > 0) ? false : true;
                        }

                        onTextChanged: {
                            territoryRemark = text
                            if (territoryTreeModel.selectedTerritory)
                                territoryTreeModel.selectedTerritory.remark = text
                        }
                    }

                    Label{text:qsTr("Type:", "Territory-type that is used to group territories")}

                    ComboBox{
                        id: cbTerritoryType

                        textRole: "name"
                        width: 150
                        Layout.fillWidth: true
                        Layout.maximumWidth: 150

                        model: typeListModel
                        currentIndex: territoryTypeIndex
                        enabled: canEditTerritories

                        onCurrentIndexChanged: {
                            territoryTypeIndex = currentIndex
                            if (territoryTreeModel.selectedTerritory !== null)
                            {
                                if (typeof model.get(currentIndex).id == "undefined"){
                                    territoryTreeModel.selectedTerritory.typeId = -1
                                }else{
                                    territoryTreeModel.selectedTerritory.typeId = model.get(currentIndex).id
                                }
                            }
                        }
                    }

                    Label{text:qsTr("City:", "Cityname that is used to group territories")}

                    ComboBox{
                        id: cbCity

                        width: 200
                        Layout.fillWidth: true
                        Layout.maximumWidth: 300

                        model: cityListModel
                        currentIndex: cityIndex
                        textRole: "name"
                        enabled: canEditTerritories

                        onCurrentIndexChanged: {
                            cityIndex = currentIndex
                            if (territoryTreeModel.selectedTerritory !== null)
                            {
                                if (typeof model.get(currentIndex).id == "undefined"){
                                    territoryTreeModel.selectedTerritory.cityId = -1
                                }else{
                                    territoryTreeModel.selectedTerritory.cityId = model.get(currentIndex).id
                                }
                            }
                        }
                    }
                }
            }

            TabView {
                id: tabView1
                Layout.fillHeight: true
                Layout.fillWidth: true

                Tab {
                    title: qsTr("Map", "Territory map")

                    Item {
                        anchors.fill: parent

                        Loader {
                            id: territoryMapFormLoader

                            anchors.fill: parent
                            source: "TerritoryMap.qml"
                            active: true
                            asynchronous: true
                            visible: status == Loader.Ready
                        }

                        BusyIndicator {
                            id: busyIndicator
                            anchors.centerIn: parent
                            running: territoryMapFormLoader.status === Loader.Loading
                        }
                    }
                }

                Tab {
                    id: assignmentTab
                    anchors.rightMargin: 12
                    anchors.leftMargin: 12
                    anchors.bottomMargin: 12
                    anchors.topMargin: 12

                    title: qsTr("Assignments")
                    enabled: isTerritorySelected && canViewTerritoryAssignments

                    ColumnLayout{
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        anchors.left: parent.left
                        anchors.right: parent.right
                        anchors.fill: parent
                        visible: true

                        Component {
                            id: dateInputItemDelegate

                            ValidationTextInput {
                                id: dateInputDelegate

                                function validateDate(currentRow, date, role)
                                {
                                    var currCheckedOut = assignmentModel.get(currentRow).checkedOutDate
                                    var currCheckedBackIn = assignmentModel.get(currentRow).checkedBackInDate

                                    if (isNaN(currCheckedOut) && role === "checkedOutDate" ||
                                            !isNaN(currCheckedOut) && !isNaN(currCheckedBackIn) && currCheckedOut > currCheckedBackIn)
                                        return false;

                                    for (var i = 0; i < assignmentModel.rowCount(); ++i)
                                    {
                                        if (i !== currentRow)
                                        {
                                            var checkedOut = assignmentModel.get(i).checkedOutDate
                                            var checkedBackIn = assignmentModel.get(i).checkedBackInDate

                                            if ((isNaN(date) && isNaN(checkedBackIn)) ||
                                                    (checkedOut <= date && (isNaN(checkedBackIn) || date <= checkedBackIn)) ||
                                                    (currCheckedOut <= checkedOut && (isNaN(currCheckedBackIn) || checkedBackIn <= currCheckedBackIn)))
                                            {
                                                return false;
                                            }
                                        }
                                    }
                                    return true;
                                }

                                function checkDate(senderIndex, checkedOut, checkedBackIn)
                                {
                                    if (typeof styleData !== "undefined")
                                    {
                                        var currIndex = assignmentModel.index(styleData.row, styleData.column);
                                        var currDate = assignmentModel.data(currIndex, styleData.role)

                                        if (senderIndex.row !== currIndex.row)
                                        {
                                            if (isTextValid)
                                            {
                                                // compare with current changes
                                                isTextValid = isNaN(checkedOut) || !(checkedOut <= currDate && (isNaN(checkedBackIn) || currDate <= checkedBackIn))
                                            }
                                            else
                                            {
                                                // test whether overlaps with any other
                                                isTextValid = validateDate(styleData.row, currDate, styleData.role);
                                            }
                                        }
                                        else
                                        {
                                            if (senderIndex !== currIndex)
                                            {
                                                // test whether range of current row overlaps
                                                isTextValid = validateDate(styleData.row, currDate, styleData.role);

                                                if (styleData.role === "checkedBackInDate")
                                                {
                                                    // checkedOut set or deleted
                                                    isReadOnly = !canEditTerritories || isNaN(checkedOut)
                                                }
                                            }
                                        }
                                    }
                                }

                                horizontalAlignment: TextInput.AlignHCenter

                                text: isNaN(styleData.value) ? "" : new Date(styleData.value).toLocaleDateString(Qt.locale(), dateFormat)
                                validator: RegExpValidator { regExp: new RegExp(dateRegexFormat, "i") }
                                isReadOnly: false
                                textInput.selectByMouse: true
                                placeholderText: dateFormat

                                Component.onCompleted: {
                                    var currIndex = assignmentModel.index(styleData.row, styleData.column)

                                    assignmentTableView.datesChanged.connect(checkDate)

                                    switch (styleData.role) {
                                    case "checkedOutDate":
                                        isReadOnly = false;
                                        isTextValid = isNaN(assignmentModel.data(currIndex, styleData.role)) ? false : validateDate(styleData.row, assignmentModel.data(currIndex, styleData.role), styleData.role);
                                        break;
                                    case "checkedBackInDate":
                                        isReadOnly = !canEditTerritories || isNaN(assignmentModel.get(styleData.row).checkedOutDate) && isNaN(assignmentModel.get(styleData.row).checkedBackInDate);
                                        isTextValid = validateDate(styleData.row, assignmentModel.data(currIndex, styleData.role), styleData.role);
                                        break;
                                    }
                                    isUntouched = assignmentModel.get(styleData.row).id < 1;
                                }

                                Component.onDestruction: {
                                    assignmentTableView.datesChanged.disconnect(checkDate)
                                }

                                onTextChanged: {
                                    var currIndex = assignmentModel.index(styleData.row, styleData.column);
                                    if (currIndex.row < 0 || currIndex.column < 0)
                                        return

                                    if (text === "")
                                    {
                                        // delete input
                                        assignmentModel.setData(currIndex, "");
                                        isTextValid = styleData.role === "checkedBackInDate";
                                        assignmentTableView.datesChanged(currIndex, assignmentModel.get(styleData.row).checkedOutDate, assignmentModel.get(styleData.row).checkedBackInDate)
                                    }
                                    else
                                    {
                                        var currDate = Date.fromLocaleDateString(Qt.locale(), text, dateFormat)
                                        if (isNaN(currDate))
                                        {
                                            color = "red"
                                        }
                                        else
                                        {
                                            // set date
                                            color = myPalette.text
                                            assignmentModel.setData(currIndex, text)
                                            currDate = assignmentModel.data(currIndex, styleData.role)
                                            isTextValid = validateDate(styleData.row, currDate, styleData.role)
                                            assignmentTableView.datesChanged(currIndex, assignmentModel.get(styleData.row).checkedOutDate, assignmentModel.get(styleData.row).checkedBackInDate)
                                        }
                                    }
                                }
                            }
                        }

                        TableView {
                            id: assignmentTableView

                            signal datesChanged(var senderIndex, date checkedOut, date checkedBackIn)
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            model: assignmentModel

                            selectionMode: SelectionMode.SingleSelection

                            onRowCountChanged:
                            {
                                if (selection.count > rowCount)
                                    assignmentTableView.selection.clear()
                            }

                            onCurrentRowChanged:
                            {
                                assignmentTableView.selection.clear()
                                assignmentTableView.selection.select(currentRow)
                            }

                            TableViewColumn {
                                role: "id"
                                title: qsTr("ID")
                                width: 30
                                visible: false

                                delegate: Item {
                                    Text{
                                        text: styleData.value
                                        renderType: Text.NativeRendering
                                    }
                                }
                            }

                            TableViewColumn {
                                role: "personId"
                                title: qsTr("Publisher-ID")
                                width: 30
                                visible: false

                                delegate: Item {
                                    Text {
                                        text: styleData.value
                                        renderType: Text.NativeRendering
                                    }
                                }
                            }

                            TableViewColumn {
                                role: "publisherIndex"
                                title: qsTr("Publisher")
                                width: 200

                                delegate: ValidationComboBox {
                                    id: publisherComboBox
                                    textRole: "name"
                                    model: publisherListModel
                                    showComboAlways: false
                                    property int assignmentRow: styleData.row
                                    enabled: canEditTerritories

                                    onAssignmentRowChanged: {
                                        if (assignmentRow > -1)
                                        {
                                            var newPublisherIndex = assignmentModel.get(styleData.row).publisherIndex;
                                            if (newPublisherIndex !== undefined && currentIndex !== newPublisherIndex)
                                                currentIndex = newPublisherIndex;
                                        }
                                        else
                                            currentIndex = -1;
                                        isDataValid = model.get(currentIndex).id > 0 && model.get(currentIndex).isValid;
                                    }

                                    onActivated: {
                                        isUntouched = false;
                                        var assignmentIndex = assignmentModel.index(styleData.row, styleData.column);
                                        assignmentModel.setData(assignmentIndex, currentIndex);
                                        isDataValid = model.get(currentIndex).id > 0 && model.get(currentIndex).isValid;
                                    }

                                    onSelectedRowChanged: {
                                        assignmentTableView.selection.clear();
                                        if (assignmentTableView.rowCount > row)
                                        {
                                            assignmentTableView.selection.select(row);
                                            assignmentTableView.currentRow = row;
                                        }
                                    }

                                    Component.onCompleted: {
                                        if (styleData.row > -1)
                                        {
                                            var newPublisherIndex = assignmentModel.get(styleData.row).publisherIndex;
                                            if (newPublisherIndex !== undefined && currentIndex !== newPublisherIndex)
                                                currentIndex = newPublisherIndex;
                                        }
                                        else
                                            currentIndex = -1;
                                        isDataValid = model.get(currentIndex).id > 0 && model.get(currentIndex).isValid;
                                    }
                                }
                            }

                            TableViewColumn {
                                role: "checkedOutDate"
                                title: qsTr("Checked out", "Date when the publisher obtained the territory")
                                width: 100
                                delegate: dateInputItemDelegate
                            }

                            TableViewColumn {
                                role: "checkedBackInDate"
                                title: qsTr("Checked back in", "Date when the territory is returned")
                                width: 100
                                delegate: dateInputItemDelegate
                            }

                            rowDelegate: Rectangle {
                                id: tableViewRowDelegate
                                height: 25
                                color: styleData.selected ? myPalette.highlight : (styleData.alternate? myPalette.alternateBase : myPalette.base)
                            }
                        }

                        RowLayout{
                            width: 50
                            Layout.fillWidth: false
                            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                            spacing: 10

                            ToolButton{
                                id: addAssignmentButton

                                Layout.minimumWidth: 24
                                Layout.minimumHeight: 24
                                tooltip: qsTr("Add new assignment")
                                iconSource: "qrc:///icons/territory_check_out.svg"
                                opacity: parent.enabled ? 1.0 :0.5
                                visible: canEditTerritories

                                function onAssignmentAdded()
                                {
                                    assignmentTableView.onRowCountChanged.disconnect(onAssignmentAdded)
                                    assignmentTableView.selection.clear();
                                    assignmentTableView.selection.select(assignmentTableView.rowCount - 1);
                                }

                                onClicked: {
                                    if (territoryTreeModel.selectedTerritory)
                                    {
                                        assignmentTableView.onRowCountChanged.connect(onAssignmentAdded);
                                        assignmentModel.addAssignment(territoryTreeModel.selectedTerritory.territoryId);
                                    }
                                }
                            }

                            ToolButton{
                                id: removeAssignmentButton

                                Layout.minimumWidth: 24
                                Layout.minimumHeight: 24
                                tooltip: qsTr("Remove selected assignment")
                                enabled: assignmentTableView.rowCount > 0 && assignmentTableView.selection.count > 0
                                iconSource: "qrc:///icons/territory_discard.svg"
                                opacity: parent.enabled ? 1.0 :0.5
                                visible: canEditTerritories

                                onClicked: {
                                    assignmentTableView.selection.forEach(
                                                function(rowIndex) {
                                                    var assignmentId = assignmentModel.get(rowIndex).id
                                                    if (assignmentId > 0)
                                                        assignmentModel.removeAssignment(assignmentId)
                                                    else
                                                        assignmentModel.removeRows(rowIndex, 1) // no id because it isn't saved yet
                                                })
                                }
                            }
                        }
                    }
                }

                Tab {
                    id: streetTab
                    title: qsTr("Streets")
                    enabled: isTerritorySelected && canViewTerritoryAssignments

                    Item {
                        anchors.fill: parent

                        Loader {
                           id: territoryStreetListFormLoader

                           anchors.fill: parent
                           source: "TerritoryStreetList.qml"

                           onLoaded: streetTab.onActiveFocusChanged.connect(item.streetTabFocusChanged)

                           Component.onDestruction: {
                               if (item) {
                                   streetTab.onActiveFocusChanged.disconnect(item.selectStreet)
                               }
                           }
                        }

                        Connections {
                            target: territoryStreetListFormLoader.item
                            function onUpdateStreets() {
                                updateStreets(true)
                            }
                        }

                        Connections {
                            target: territoryStreetListFormLoader.item
                            function onStreetDoubleClicked() {
                                zoomSelectedStreet()
                            }
                        }
                    }
                }

                Tab {
                    id: addressTab
                    title: qsTr("Addresses")
                    enabled: isTerritorySelected && canViewTerritoryAssignments

                    Item {
                        anchors.fill: parent

                        Loader {
                           id: territoryAddressListFormLoader

                           anchors.fill: parent
                           source: "TerritoryAddressList.qml"

                           onLoaded: addressTab.onActiveFocusChanged.connect(item.addressTabFocusChanged)

                           Component.onDestruction: {
                               if (item) {
                                   addressTab.onActiveFocusChanged.disconnect(item.selectAddress)
                               }
                           }
                        }

                        Connections {
                            target: territoryAddressListFormLoader.item
                            function onUpdateMarkers() {
                                updateMarkers(true)
                            }
                        }

                        Connections {
                            target: territoryAddressListFormLoader.item
                            function onAddressDoubleClicked() {
                                zoomSelectedAddress()
                            }
                        }
                    }
                }
            }
        }
    }
}
