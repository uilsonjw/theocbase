#include "zipper.h"

zipper::zipper()
    : ready(false)
{
}

QString zipper::UnZip(QString filePath)
{
    if (tempdir.isValid()) {
        QString work(tempdir.path() + "/" + filePath.midRef(filePath.lastIndexOf('/') + 1));
        setup(filePath, work);
        if (!ready) {
            errMessage = setupErr;
            return "";
        }
        errMessage = "";
#if !defined(Q_OS_ANDROID) && !defined(Q_OS_IOS)
        int zipResults(QProcess::execute(zipCmd, argList));
        switch (zipResults) {
        case 0:
            return work;
        case 1:
            errMessage = "7z: Warning (Non fatal error(s)). For example, one or more files were locked by some other application, so they were not compressed.";
            break;
        case 2:
            errMessage = "7z: Fatal error";
            break;
        case 7:
            errMessage = "7z: Command line error";
            break;
        case 8:
            errMessage = "7z: Not enough memory for operation";
            break;
        case 255:
            errMessage = "7z: User stopped the process";
            break;
        default:
            errMessage = "7z returned unknown code: " + QString::number(zipResults);
            break;
        }
#else
        QDir destDir(work);
        if (!destDir.exists())
            destDir.mkdir(work);
        QZipReader zipReader(filePath);
        for (const QZipReader::FileInfo &item : zipReader.fileInfoList()) {
            qDebug() << item.filePath;
            if (item.isFile) {
                const QString absPath = work + QDir::separator() + item.filePath;
                if (item.filePath.contains("/")) {
                    QFileInfo fi(absPath);
                    if (!destDir.exists(fi.path()))
                        destDir.mkpath(fi.path());
                }
                QFile f(absPath);
                if (!f.open(QIODevice::WriteOnly)) {
                    return "";
                }
                f.write(zipReader.fileData(item.filePath));
                f.setPermissions(item.permissions);
                f.close();
            }
        }
        qDebug() << "extracted";
        return work;
#endif
    }
    return "";
}

void zipper::setup(QString filePath, QString work)
{
    argList.clear();
#if defined(Q_OS_MACOS) || defined(Q_OS_LINUX)
    zipCmd = "unzip";
    argList.append("-o");
    argList.append(filePath);
    argList.append("-d");
    argList.append(work);
#elif defined(Q_OS_WIN)
    QString sevenzip = qApp->applicationDirPath() + "/7za.exe";
    if (!QFile(sevenzip).exists()) {
        setupErr = sevenzip + " not found!";
        ready = false;
        return;
    }
    zipCmd = sevenzip;
    argList.append("-y");
    argList.append("x");
    argList.append(filePath);
    argList.append("-o" + work);
#endif
    setupErr = "";
    ready = true;
}
