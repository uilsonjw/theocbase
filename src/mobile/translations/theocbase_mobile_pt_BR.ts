<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt-BR">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation>Nome</translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation>Data</translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Não designar a próxima característica</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Ficar na característica atual</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Dados inválidos</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>cronometrar?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Fonte de matéria</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Estudante</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Ajudante</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Resultado</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Concluido</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Substituto</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Escolha um substituto</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Característica atual</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Exercício Concluido</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Próximo estudo</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Escolha o próximo estudo</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Controle de tempo</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notas</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Detalhes da Escola</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Iniciar cronômetro</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Parar cronômetro</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Cena</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Selecione cena</translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>No exception</source>
        <translation>Sem exceção</translation>
    </message>
    <message>
        <source>Circuit overseer&#x27;s visit</source>
        <translation>Visita do Sup. de Circuito</translation>
    </message>
    <message>
        <source>Circuit assembly</source>
        <translation>Assembleia de circuito</translation>
    </message>
    <message>
        <source>Regional convention</source>
        <translation>Congresso Regional</translation>
    </message>
    <message>
        <source>Memorial</source>
        <translation>Celebração</translation>
    </message>
    <message>
        <source>Other exception</source>
        <translation>Outra exceção</translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation>Semana começando em %1</translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation>Dias de reunião</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Reunião do Meio de Semana</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Reunião do Fim de Semana</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Fonte da matéria</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Dirigente</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Orador</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Leitor</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notas</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Detalhes</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <comment>Page title</comment>
        <translation>Notas</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>TESOUROS DA PALAVRA DE DEUS</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>FAÇA SEU MELHOR NO MINISTÉRIO</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>NOSSA VIDA CRISTÃ</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Presidente</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Conselheiro</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Cântico %1 e Oração</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Cântico</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Dirigente</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Leitor</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Comentários iniciais</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Oração</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation>Importar Programa...</translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation>SP</translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation>S1</translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation>S2</translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation>Comentários finais</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Reunião do Meio de Semana</translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Permanecer na característica atual</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Iniciar cronômetro</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Parar cronômetro</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Adicionar tempo?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Fonte da matéria</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Estudante</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Resultado</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Concluído</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Tempo</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Característica atual</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Exercícios Concluídos</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Próxima característica</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Escolha a próxima característica</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notas</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Ajudante</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Substituto</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>Cronômetro</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>O ajudante não pode ser alguém do sexo oposto.</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>Melhore lição</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Detalhes</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>Deslize para atualizar...</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Soltar para atualizar...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation>Nome do usuário</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Senha</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Entrar</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Nome do usuário ou Email</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>EMail</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Criar Conta</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>Redefinir senha</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>email não encontrado</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Esqueceu a senha</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>Iniciar Sessão</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation>Semana começando %1</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation>Notas</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation>SAÍDA DE ORADORES</translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>%1 Orador saindo neste fim de semana</numerusform>
            <numerusform>%1 Oradores saindo neste fim de semana</numerusform>
        </translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation>Não há orador saindo neste fim de semana</translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation>Opções de impressão</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Programa</translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation>Folhas do dirigente</translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation>Programação de discursos fora</translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation>Designações de Saídas de Oradores</translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation>Lista de Chamadas e Programas para Hospitalidade</translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation>Temas dos Oradores</translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Imprimir</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Reunião do Meio de Semana</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Reunião do Fim de Semana</translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation>Combinação</translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation>Folha de designação</translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation>Modelo</translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation>Folha de designação para o ajudante</translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation>Imprimir apenas a designação</translation>
    </message>
    <message>
        <source>Text size</source>
        <translation>Tamanho do texto</translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation>Modelos personalizados</translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Cântico e Oração</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Cântico %1 e Oração</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>DISCURSO PÚBLICO</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>ESTUDO DE A SENTINELA</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Cântico %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Dirigente</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Leitor</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation>Importar WT...</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Reunião do Fim de Semana</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Reunião do Meio de Semana</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>Congregação</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Orador</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Celular</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefone</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Hospitalidade</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>Discurso Público</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Sobrenome</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Irmão</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Irmã</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Servo</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Familia</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>Membro da família de</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Informações do Contato</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefone</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>EMail</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>Todas as salas</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Apenas Salão Principal</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Apenas 2ª Sala</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Chefe de Família</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Ajudante</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>Ativo</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Presidente</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>Tesouros da Palavra de Deus</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Leitura da Bíblia</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Primeira conversa</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Revisita</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Estudo Bíblico</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>Discursos de Nossa Vida Cristã</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Estudo Bíblico de Congregação</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Leitor do Estudo Bíblico de Congregação</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>Novo Publicador</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Oração</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation>Anfitrião para Oradores Públicos</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Celular</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Reunião do Meio de Semana</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>Discurso</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Reunião do Fim de Semana</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>Dirigente Estudo Sentinela</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation>Leitor de A Sentinela</translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation>Joias espirituais</translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation>Consideração com vídeo</translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Publicadores</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation>lista de seleção</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Configurações</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Sair</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Informações</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Versão</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>Site do TheocBase</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Comentários</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>Ultima vez que foi sincronizado</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Programa</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Mostrar horário</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Mostrar duração</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Interfaces de Usuário</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Idioma</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Iniciar Sessão</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation>Mostrar temos dos cânticos</translation>
    </message>
    <message>
        <source>Printing</source>
        <translation>Impimindo</translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation>Modelos personalizados</translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Sincronizando</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>Presidente</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Cântico</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation>Presidente da Reunião de final de Semana</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>Cântico</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation>A Sentinela Cantico</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Oração</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation>Edição de A Sentinela</translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>Artigo</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>Dirigente</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>Leitor</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>Cronograma</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>Número de semanas antes da data selecionada</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>Número de semanas depois da data selecionada</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>semanas</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation>Número de semanas em cinza depois de uma designação</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>As mesmas mudanças podem ser encontrada tanto localmente quanto na Nuvem, (%1Rows ) Você quer manter as alterações locais?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>Os dados da nuvem foram redefinidos. Seus dados locais serão substituídos. Continuar?</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Não designar nova característica</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>Não definido</translation>
    </message>
</context></TS>