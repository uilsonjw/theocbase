/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "js/moment.js" as Moment

Item {
    property int start: 1
    property int end: 1
    property date weekDate: moment().startOf('isoweek').toDate()
    property real _dpi: typeof dpi == "undefined" ? 1 : dpi

    RowLayout {
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        Repeater {
            model: 7
            Rectangle {
                width: 40 * _dpi
                Layout.preferredHeight: 60 * _dpi
                color: "transparent"
                ColumnLayout {
                    anchors.fill: parent
                    Text {
                        text: Qt.locale().dayName(index == 6 ? 0 : index+1, Locale.ShortFormat)
                        horizontalAlignment: Text.AlignHCenter
                        Layout.fillWidth: true
                    }
                    Rectangle {
                        property bool selected: start > -1 && end > -1 && start <= index && end >= index
                        Layout.fillWidth: true
                        Layout.preferredHeight: width
                        radius: width / 2
                        border.width: 1
                        border.color: "black"
                        color: selected ? "black" : "transparent"
                        Text {
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            text: moment(weekDate).add(index, 'days').toDate().getDate()
                            color: parent.selected ? "white" : "grey"
                        }
                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                var v1 = Math.abs(start - index)
                                var v2 = Math.abs(end - index)                               
                                if (start == index || end == index) {                                    
                                    start = index
                                    end = index
                                } else if (v2 <= v1) {
                                    end = index
                                    if (end < start)
                                        start = end
                                } else {
                                    start = index
                                }
                                if (end == -1)
                                    end = start                               
                            }
                        }
                    }
                }
            }
        }
    }
}
