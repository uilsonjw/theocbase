#ifndef SYNC_CLOUD_H
#define SYNC_CLOUD_H

#include <QThread>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QUrlQuery>
#include <QEventLoop>
#include <QDebug>
#include <QString>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>
#include <QJsonObject>
#include <QUuid>
#include <QSettings>
#include <QTime>
#include <QStandardPaths>
#include "sql_class.h"
#include "cloud/dropbox.h"
#include "accesscontrol.h"

class sync_cloud : public QObject
{
    Q_OBJECT
public:
    sync_cloud(QObject *parent = nullptr);
    void setAuthentication(dropbox *db);
    void sync(bool ignoredbuser = false);
    void continueSync(bool keepLocalChanges);
    bool cloudUpdateAvailable();
    void resetCloudData();
public slots:
    void runTest();

signals:
    void syncConflict(int duplicateValues);
    void progressed(int value);
    void ready();
    void error(QString message);
    void differentLastDbUser();
    void cloudResetFound();
    void cloudResetReady();

private slots:
    void getChangesFromCloudFinished(QJsonDocument doc);
    void pushChangesToCloudFinished();
    void networkError(QNetworkReply::NetworkError errorcode);

private:
    // Change this every time when changes in database structure
    const int versionnumber = 18;

    sql_class *sql;
    dropbox *mDropbox;
    void getLocalChangeset(QHash<QString, sql_item> &localChangeset, QHash<QString, sql_item> &localUnpermittedChangeset);
    bool getRemoteChangeSets();
    void getChangesFromCloud();
    void pushChangesToCloud(QJsonDocument doc);
    void saveChanges();
    void saveChangesToDatabase(QHash<QString, sql_item> changeset);
    void deleteChangesFromDatabase(QHash<QString, sql_item> changeset);
    void compareChangeSets();
    bool compareChangeSetValues(const sql_item first, const sql_item second);
    QByteArray cleanHttpResponse(QByteArray value);

    QString m_useremail;
    int m_changeid;
    QString idMapping(QString fromTable, QString uuid);
    QHash<QString, sql_item> mRemoteChangeSet1;
    QHash<QString, sql_item> mRemoteChangeSet2;
    QHash<QString, sql_item> mRemoteChangeSet3;
    QHash<QString, sql_item> mLocalChangeSet;
    QHash<QString, sql_item> mLocalUnpermittedChangeSet;
    QHash<QString, sql_item> mLocalUnpermittedOriginalSet;
    QHash<QString, sql_item> mLocalUnpermittedDeleteSet;
    QHash<QString, sql_item> mAllRows;
    QHash<QString, QString> mCachedValues;
    QStringList mDuplicateValues;
    bool mError;
    bool mResetStarted;
    uint mResetTime;
    int mSyncFileModified;
    QString mCloudPath;
};

#endif // SYNC_CLOUD_H
