#ifndef COMMON_H
#define COMMON_H

#include <QObject>
#include <QDir>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QNetworkReply>
#include "../../src/sql_class.h"
#include "../../src/internet.h"
#include "../../src/sharedlib_global.h"

QString theocbaseDirPath;
QString localdatabasedir;
bool transactionStarted;

class TB_EXPORT common : public QObject
{
    Q_OBJECT

public:
    explicit common(QObject *parent = nullptr) { Q_UNUSED(parent) }
    ~common() {}
    static void initDataBase()
    {
        qDebug() << "init test";
        QDir d;
        QString localDbPath = QDir::tempPath() + QDir::separator() +  "theocbase_test.sqlite";
        if (!QFile::exists(localDbPath)) {
            QFile::copy(":/database/theocbase.sqlite", localDbPath);
            QFile::setPermissions(localDbPath, QFile::ReadOwner | QFile::WriteOwner | QFile::ReadUser | QFile::WriteUser | QFile::ReadGroup | QFile::WriteOwner | QFile::ReadOther | QFile::WriteOther);
        }
        sql_class *sql = &Singleton<sql_class>::Instance();
        sql->databasepath = localDbPath;        
        sql->createConnection();
        sql->updateDatabase("2021.06.0");
        sql->updateNextYearMeetingTimes();
    }

    static void clearDatabase()
    {
        sql_class *sql = &Singleton<sql_class>::Instance();
        sql->clearDatabase();
    }

    static bool downloadTestFile(const QString filename, const QString urlString,
                                 const QString destDir)
    {
        // Download test files
        QString epubFullPath = destDir + QDir::separator() + filename;
        if (!QFile::exists(epubFullPath)) {
            internet i;
            QNetworkReply *reply = i.download(QUrl(urlString), 30);
            if (reply->error() == QNetworkReply::NoError) {
                auto content = reply->readAll();
                QFile file(epubFullPath);
                file.open(QIODevice::WriteOnly);
                file.write(content);
                file.close();
            } else {
                return false;
            }
        }
        return true;
    }
};

#endif // COMMON_H
