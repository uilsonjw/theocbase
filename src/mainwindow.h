/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets>
#include <QPrinter>
#include <QPrintDialog>
#include <QPainter>
#include <QTextEdit>
#include <QComboBox>
#include <QUrl>
#include <QDebug>
#include <QDesktopServices>
#include <QSysInfo>
#include "settings.h"
#include "printui.h"
#include "sql_class.h"
#include "personsui.h"
#include "speakersui.h"
#include "checkupdates.h"
#include "csync.h"
#include "smtp/SmtpMime"
#include "historytable.h"
#include "startup.h"
#include "reminders.h"
#include "logindialog.h"
#include "ccongregation.h"
#include "importwintm.h"
#include "importTa1ks.h"
#include "lmmworksheetregex.h"
#include "lmm_meeting.h"
#include "lmm_schedule.h"
#include "lmm_assignmentcontoller.h"
#include "sortfilterproxymodel.h"
#include "publicmeeting_controller.h"
#include "outgoingspeakersmodel.h"
#include "todomodel.h"
#include "importlmmworkbook.h"
#include "lmmtalktypeedit.h"
#include "ical.h"
#include "territorymanagement.h"
#include "territory.h"
#include "cterritories.h"
#include "cloud/cloud_controller.h"
#include "dropboxsyncbutton.h"
#include "zipper.h"
#include "wtimport.h"
#include "mapSettings.h"
#include "shareutils.h"

#include <QQmlContext>
#include <QQuickItem>
#include <QQmlPropertyMap>

#ifdef Q_OS_MAC
#include <QtMacExtras>
#include "macos/machelper.h"
#endif

extern QTranslator translator;
class HelpViewer;

namespace Ui {
class MainWindow;
}

/**
 * @brief The MainWindow class - Main Window User Interface Class
 *                               This class is used to show main window of TheocBase
 *                               User interface is done by Qt Creator's visual editor (see file mainwindow.ui)
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    QString openfile;

private:
    Ui::MainWindow *ui;

    /**
     * @brief infoinit - Function to initialize main window
     * @param resetDate - Optional parameter to reset date to current date. Default value is true.
     */
    void infoinit(bool resetDate = true);

    /**
     * @brief updateinfo - Function to update data on main screen
     *                     This function is used when load current date or change date
     */
    void updateinfo();

    /**
     * @brief importXml - Import data exchange file into TheocBase. File can be xml or thb file.
     * @param filePath - Full path of transfer file
     */
    void importXml(QString filePath);

    /**
     * @brief createCalendarPopup - Create a calendar popup dialog. Dialog is shown when a user clicks calendar icon on toolbar.
     */
    void createCalendarPopup();

    void exportXML(QString filename);
    void exportICal(QString filePath);
    void addToCalendarsByPerson(QHash<QString, iCal::VCALENDAR *> *calendars, person *assigned, QDateTime startDate, QDateTime endDate, QString summary, QString description);
    void addToCalendarsByDate(QHash<QString, iCal::VCALENDAR *> *calendars, person *assigned, person *assistant, QDateTime startDate, QDateTime endDate, QString summary);
    void initCloud();
    void applyAuthorizationRules();
    void setCloudIcon(bool logged, bool localChanges, bool cloudChanges);
    void cloudStateChanged(cloud_controller::SyncState state);

    // Member variables
    sql_class *sql;
    zipper *zipr;
    QDate firstdayofweek;
    ccongregation::congregation myCongregation;
    historytable *hw;
    QString themestyle;
    QFrame *calPopup;
    QDateEdit *oDate;
    QLabel *lblCloudStatus;
    bool loggedCloud;
    QAction *activeButton;
    cloud_controller *cloud;
    bool cloudLoginOpen = false;
    HelpViewer *helpViewer;
    int sidebarWidth = 0;

private slots:
    void changeActivePage(QWidget *w);
    bool saveCloseActivePage();
    void on_button_ReturnSchool_clicked();
    void on_button_ReturnPublicTalk_clicked();
    void on_buttonImportXml_clicked();
    void buttonImportXmlTa1ks_clicked();
    void on_pushButton_lahetaTiedot_clicked();

    void on_actionHome_triggered();
    void on_toolButtonPreviousWeek_clicked();
    void on_toolButtonNextWeek_clicked();
    void on_actionExit_triggered();
    void on_actionSettings_triggered();
    void on_actionPublishers_triggered();
    void on_actionPrint_triggered();
    void on_actionHelp_triggered();
    void on_actionSpeakers_triggered();
    void on_actionDataExchange_triggered();
    void on_actionHistory_triggered();
    void on_actionAbout_triggered();
    void on_actionTheocBase_net_triggered();
    void on_actionFeedback_triggered();
    void on_actionReport_bug_triggered();
    void on_button_EditSchool_clicked();
    void on_actionCheckUpdates_triggered();
    void calendarClicked(QDate date);
    void on_button_EditPublicMeeting_clicked();
    void showPersonsList(QString defaultname, bool speaker = false);
    void addSyncReportRow(QString text, csync::SyncType typ);
    void on_spPreviousWeeks_valueChanged(int arg1);
    void mailState(QString stateStr);
    void on_button_ImportOK_clicked();
    void on_button_ImportCancel_clicked();
    void on_chk_ExportPublishers_clicked(bool checked);
    void on_chk_ExportSpeakers_clicked(bool checked);
    void on_chk_ExportStudyHistory_clicked(bool checked);
    void on_chk_ExportPublicTalks_clicked(bool checked);
    void changeFullScreen();
    void receiveMessage(QString msg);
    void on_actionStartup_Screen_triggered();
    void on_actionReminders_triggered();
    void updatesFound();
    void on_toolButtonCloud_clicked();
    void onScheduleImportClicked(QString filepath = "");
    void onFileDropped(QUrl url);
    void on_rbXML_clicked(bool checked);
    void on_rbiCal_clicked(bool checked);
    void on_chk_ExportOutgoing_clicked(bool checked);
    void on_fromDate_dateChanged(const QDate &date);
    void on_thruDate_dateChanged(const QDate &date);

    void on_chk_MidweekMeeting_clicked(bool checked);
    void databaseChanged(QString tablename);

    void on_actionTerritories_triggered();

    void on_toolButtonCalendar_clicked();

    void on_buttonPreviousWeek_clicked();

    void on_buttonNextWeek_clicked();
    void historyWindowVisibleChanged(bool visible);
    void reloadActivePage();
    void sidebarWidthChanged(const int width);

protected:
    void showEvent(QShowEvent *event);
    void closeEvent(QCloseEvent *event);
    void dropEvent(QDropEvent *event);
    void dragEnterEvent(QDragEnterEvent *event);
    void resizeEvent(QResizeEvent *event);
    bool eventFilter(QObject *watched, QEvent *event);
};

#endif // MAINWINDOW_H
