import QtQuick 2.9
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Window 2.0
import net.theocbase 1.0

TerritoryAddressListForm {
    id: territoryAddressListForm

    property bool canEditTerritories: accessControl.user && accessControl.user.hasPermission(PermissionRule.CanEditTerritories)
    property DataObjectListModel addressTypeListModel: territories.getAddressTypes()

    signal updateMarkers()
    signal addressDoubleClicked()

    function addAddress()
    {
        if (territoryTreeModel.selectedTerritory)
        {
            addAddressDialog.x = mainWindow.x + mainWindow.width / 2 - width / 2
            addAddressDialog.y = mainWindow.y + mainWindow.height / 2 - height / 2

            var cityIndex = cityListModel.getIndex(territoryTreeModel.selectedTerritory.cityId)
            var cityName = cityListModel.get(cityIndex).name
            if (cityName !== "undefined")
                addAddressDialog.cityTextField.text = cityName

            addAddressDialog.isEditAddressMode = false
            addAddressDialog.show();
        }
    }

    function editAddress()
    {
        if (territoryTreeModel.selectedTerritory)
        {
            addAddressDialog.x = mainWindow.x + mainWindow.width / 2 - width / 2
            addAddressDialog.y = mainWindow.y + mainWindow.height / 2 - height / 2
            var countryName ="";
            var stateName ="";
            var cityName ="";
            var districtName ="";
            var streetName ="";
            var houseNumberName ="";
            var postalCodeName ="";

            addressTableView.selection.forEach(
                        function(rowIndex) {
                            var addressProxyIndex = addressProxyModel.index(rowIndex, 0);
                            var addressIndex = addressProxyModel.mapToSource(addressProxyIndex);

                            if (addressModel.get(addressIndex.row).id !== "undefined")
                                addAddressDialog.editAddressId = addressModel.get(addressIndex.row).id;

                            if (addressModel.get(addressIndex.row).country !== "undefined")
                                countryName = addressModel.get(addressIndex.row).country;
                            if (addressModel.get(addressIndex.row).state !== "undefined")
                                stateName = addressModel.get(addressIndex.row).state;
                            if (addressModel.get(addressIndex.row).city !== "undefined")
                                cityName = addressModel.get(addressIndex.row).city;
                            if (addressModel.get(addressIndex.row).district !== "undefined")
                                districtName = addressModel.get(addressIndex.row).district;
                            if (addressModel.get(addressIndex.row).street !== "undefined")
                                streetName = addressModel.get(addressIndex.row).street;
                            if (addressModel.get(addressIndex.row).houseNumber !== "undefined")
                                houseNumberName = addressModel.get(addressIndex.row).houseNumber;
                            if (addressModel.get(addressIndex.row).postalCode !== "undefined")
                                postalCodeName = addressModel.get(addressIndex.row).postalCode;
                        })

            addAddressDialog.countryTextField.text = countryName
            addAddressDialog.stateTextField.text = stateName
            addAddressDialog.cityTextField.text = cityName
            addAddressDialog.districtTextField.text = districtName
            addAddressDialog.streetTextField.text = streetName
            addAddressDialog.houseNumberTextField.text = houseNumberName
            addAddressDialog.postalCodeTextField.text = postalCodeName
            addAddressDialog.isEditAddressMode = true
            addAddressDialog.show();
        }
    }

    function removeAddress()
    {
        addressTableView.selection.forEach(
                    function(rowIndex) {
                        var addressProxyIndex = addressProxyModel.index(rowIndex, 0);
                        var addressIndex = addressProxyModel.mapToSource(addressProxyIndex);
                        var addressId = addressModel.get(addressIndex.row).id

                        if (addressId > 0)
                            addressModel.removeAddress(addressId)
                        else
                            addressModel.removeRows(addressIndex, 1) // no id because it isn't saved yet
                    })
        updateMarkers()
    }

    function selectAddress()
    {
        addressTableView.selection.clear();
        if (addressModel.selectedAddress !== null)
        {
            var addressIndex = addressModel.getAddressIndex(addressModel.selectedAddress.id);
            var addressProxyIndex = addressProxyModel.mapFromSource(addressIndex)
            if (addressTableView.rowCount > addressProxyIndex.row)
            {
                addressTableView.selection.select(addressProxyIndex.row);
                addressTableView.currentRow = addressProxyIndex.row;
            }
        }
    }

    function addressTabFocusChanged(activeFocus)
    {
        if (activeFocus)
            selectAddress();
    }

    addAddressButton.onClicked: addAddress()
    editAddressButton.onClicked: editAddress()
    removeAddressButton.onClicked: removeAddress()

    addressTableView.model: addressProxyModel
    addressTableView.onSortIndicatorColumnChanged: addressProxyModel.sort(
                                                       addressTableView.sortIndicatorColumn,
                                                       addressTableView.sortIndicatorOrder)
    addressTableView.onSortIndicatorOrderChanged: addressProxyModel.sort(
                                                      addressTableView.sortIndicatorColumn,
                                                      addressTableView.sortIndicatorOrder)
    addressTableView.onCurrentRowChanged: {
        var currProxyIndex = addressProxyModel.index(addressTableView.currentRow, 0)
        var currIndex = addressProxyModel.mapToSource(currProxyIndex)
        addressModel.setSelectedAddress(currIndex)
    }
    addressTableView.onDoubleClicked: addressDoubleClicked()

    addAddressButton.enabled: territoryAddressListForm.enabled
    editAddressButton.enabled: addressTableView.rowCount > 0 && addressTableView.selection.count > 0
    removeAddressButton.enabled: addressTableView.rowCount > 0 && addressTableView.selection.count > 0

    NumberAnimation {
        id: invalidDataAnimation
        property: "opacity";
        duration: 300;
        loops: 4;
        easing.type: Easing.InOutElastic;
    }

    Component {
        id: addressItemDelegate
        ValidationTextInput{
            id: addressTextInput
            text: styleData.value
            placeholderText: qsTr("Address")

            function validateAddress(currentRow, address)
            {
                return false;
            }

            Component.onCompleted: {
                var currProxyIndex = addressProxyModel.index(styleData.row, styleData.column)
                var currIndex = addressProxyModel.mapToSource(currProxyIndex);
                isTextValid = validateAddress(currIndex.row, addressModel.data(currIndex, styleData.role));
                isUntouched = addressModel.get(currIndex.row).id < 1;
            }

            onTextChanged: {
                var currProxyIndex = addressProxyModel.index(styleData.row, styleData.column);
                var currIndex = addressProxyModel.mapToSource(currProxyIndex);
                if (currIndex.row < 0 || currIndex.column < 0)
                    return

                if (text === "")
                {
                    // delete input
                    addressModel.setData(currIndex, "");
                    isTextValid = false;
                }
                else
                {
                    //geocode
                    addressModel.setData(currIndex, text)
                    isTextValid = validateAddress(currIndex.row, addressModel.data(currIndex, styleData.role))
                }
            }
        }
    }

    Component {
        id: houseNumberItemDelegate
        ValidationTextInput{
            id: houseNumberTextInput
            text: typeof styleData.value === "undefined" ? "" : styleData.value
            placeholderText: qsTr("No.", "House or street number")
            color: myPalette.text
            isReadOnly: !canEditTerritories

            Component.onCompleted: {
                var currProxyIndex = addressProxyModel.index(styleData.row, styleData.column);
                var currIndex = addressProxyModel.mapToSource(currProxyIndex);
                isTextValid = addressModel.data(currIndex, styleData.role) === "undefined" ? false : (addressModel.data(currIndex, styleData.role) === "" ? false : true);
                isUntouched = addressModel.get(currIndex.row).id < 1;
            }

            onTextChanged: {
                var currProxyIndex = addressProxyModel.index(styleData.row, styleData.column);
                if (currProxyIndex.row < 0 || currProxyIndex.column < 0)
                    return;

                var currIndex = addressProxyModel.mapToSource(currProxyIndex);
                if (text === "")
                {
                    // delete input
                    addressModel.setData(currIndex, "");
                    isTextValid = false;
                }
                else
                {
                    addressModel.setData(currIndex, text);
                    isTextValid = addressModel.data(currIndex, styleData.role) === "undefined" ? false : true;
                }
            }

            onSelectedRowChanged: {
                addressTableView.selection.clear();
                if (addressTableView.rowCount > row)
                {
                    addressTableView.selection.select(row);
                    addressTableView.currentRow = row;
                }
            }
        }
    }

    Component {
        id: nameItemDelegate
        ValidationTextInput{
            id: nameTextInput
            text: typeof styleData.value === "undefined" ? "" : styleData.value
            placeholderText: qsTr("Name")
            color: myPalette.text
            isReadOnly: !canEditTerritories

            Component.onCompleted: {
                var currProxyIndex = addressProxyModel.index(styleData.row, styleData.column)
                var currIndex = addressProxyModel.mapToSource(currProxyIndex);
                isTextValid = addressModel.data(currIndex, styleData.role) === "undefined" ? false : (addressModel.data(currIndex, styleData.role) === "" ? false : true);
                isUntouched = addressModel.get(currIndex.row).id < 1;
            }

            onTextChanged: {
                var currProxyIndex = addressProxyModel.index(styleData.row, styleData.column);
                if (currProxyIndex.row < 0 || currProxyIndex.column < 0)
                    return

                var currIndex = addressProxyModel.mapToSource(currProxyIndex)
                if (text === "")
                {
                    // delete input
                    addressModel.setData(currIndex, "");
                    isTextValid = false;
                }
                else
                {
                    addressModel.setData(currIndex, text)
                    isTextValid = addressModel.data(currIndex, styleData.role) === "undefined" ? false : true;
                }
            }

            onSelectedRowChanged: {
                addressTableView.selection.clear();
                if (addressTableView.rowCount > row)
                {
                    addressTableView.selection.select(row);
                    addressTableView.currentRow = row;
                }
            }
        }
    }

    Component{
        id: addressTypeItemDelegate

        ValidationComboBox {
            id: addressTypeComboBox
            textRole: "name"
            model: addressTypeListModel
            showComboAlways: false
            property int addressTypeNumber: styleData.value
            property int addressTypeIndex
            enabled: canEditTerritories

            onAddressTypeNumberChanged: {
                addressTypeIndex = model.getIndex(addressTypeNumber)

                if (currentIndex === -1)
                {
                    model.addDataObject(addressTypeNumber,
                                        qsTr("Undefined [%1]", "Undefined territory address type").arg(addressTypeNumber),
                                        "#000000",
                                        false)
                    addressTypeIndex = model.getIndex(addressTypeNumber)
                }
                isDataValid = model.get(currentIndex).id > 0 && model.get(currentIndex).isValid
            }

            onAddressTypeIndexChanged: {
                if (currentIndex === addressTypeIndex)
                    return
                currentIndex = addressTypeIndex
            }

            onCurrentIndexChanged: {
                if (currentIndex === addressTypeIndex)
                    return
                isUntouched = false;
                var currProxyIndex = addressProxyModel.index(styleData.row, styleData.column)
                var currIndex = addressProxyModel.mapToSource(currProxyIndex)
                var savedAddressTypeIndex = model.getIndex(addressModel.get(currIndex.row).addressTypeNumber)
                if (savedAddressTypeIndex !== currentIndex)
                {
                    addressModel.setData(currIndex, model.get(currentIndex).id)
                    updateMarkers()
                }
                isDataValid = model.get(currentIndex).id > 0 && model.get(currentIndex).isValid
                addressTypeIndex = currentIndex
            }

            onSelectedRowChanged: {
                addressTableView.selection.clear();
                if (addressTableView.rowCount > row)
                {
                    addressTableView.selection.select(row);
                    addressTableView.currentRow = row;
                }
            }
        }
    }

    Window {
        id: addAddressDialog
        title: isEditAddressMode ? qsTr("Edit address") : qsTr("Add address")
        width: 800
        height: 600
        flags: Qt.Popup | Qt.Dialog
        modality: Qt.WindowModal
        color: myPalette.window

        property alias countryTextField: addAddressForm.countryTextField
        property alias stateTextField: addAddressForm.stateTextField
        property alias cityTextField: addAddressForm.cityTextField
        property alias districtTextField: addAddressForm.districtTextField
        property alias streetTextField: addAddressForm.streetTextField
        property alias houseNumberTextField: addAddressForm.houseNumberTextField
        property alias postalCodeTextField: addAddressForm.postalCodeTextField
        property alias isEditAddressMode: addAddressForm.isEditAddressMode
        property alias territoryId: addAddressForm.territoryId
        property int editAddressId: 0

        onVisibleChanged: {
            x = mainWindow.x + mainWindow.width / 2 - width / 2;
            y = mainWindow.y + mainWindow.height / 2 - height / 2;

            addAddressForm.locationsModel.clear();

            if (visible && territoryTreeModel.selectedTerritory)
                territoryId = territoryTreeModel.selectedTerritory.territoryId;
        }

        MessageDialog {
            id: message
            standardButtons: StandardButton.Ok
            icon: StandardIcon.Warning
            modality: Qt.ApplicationModal
        }

        TerritoryAddAddressForm {
            id: addAddressForm
            anchors.leftMargin: 10
            anchors.rightMargin: 10
            anchors.topMargin: 10
            anchors.bottomMargin: 10

            property var address
            property int currentTableViewRow: -1
            locationsTableView.onCurrentRowChanged: currentTableViewRow = locationsTableView.currentRow

            okButton.onClicked:
            {
                address = locationsModel.get(locationsTableView.currentRow);

                if (typeof address === "undefined")
                {
                    message.title = qsTr("Add address", "Add address to territory");
                    message.text = qsTr("Please select an address in the list of search results.", "Add address to territory");
                    message.visible = true;
                    return;
                }

                if (isEditAddressMode)
                {
                    if (addAddressDialog.editAddressId > 0)
                        if (addressModel.updateAddress(addAddressDialog.editAddressId,
                                                       addressModel.get(addressModel.getAddressIndex(addAddressDialog.editAddressId).row).territoryId,
                                                       address.country, address.state, address.county,
                                                       address.city, address.district, address.street,
                                                       address.houseNumber, address.postalCode,
                                                       "POINT(" + address.longitude + " " + address.latitude + ")"));
                            updateMarkers();
                    addAddressDialog.close();
                }
                else
                {
                    if (territoryId > 0)
                    {
                        var newAddressId = addressModel.addAddress(territoryId, address.country,
                                                    address.state, address.county, address.city, address.district,
                                                    address.street, address.houseNumber, address.postalCode,
                                                    "POINT(" + address.longitude + " " + address.latitude + ")");
                        if (newAddressId > 0)
                        {
                            var addressIndex = addressModel.getAddressIndex(newAddressId);
                            var addressProxyIndex = addressProxyModel.mapFromSource(addressIndex);
                            if (addressProxyIndex)
                            {
                                addressTableView.currentRow = addressProxyIndex.row;
                                addressTableView.selection.clear();
                                addressTableView.selection.select(addressProxyIndex.row);
                            }

                            addAddressDialog.close();
                            updateMarkers();
                        }
                    }
                }
            }

            cancelButton.onClicked: addAddressDialog.close();
            searchButton.onClicked: {
                territories.onGeocodeFinished.connect(geocodeFinished);
                territories.onGeocodeError.connect(geocodeFailed);
                territories.geocodeAddress(searchAddress.text.replace("<br/>",","));
            }

            function geocodeFinished(geocodeResults) {
                territories.onGeocodeFinished.disconnect(geocodeFinished);
                territories.onGeocodeError.disconnect(geocodeFailed);
                if (geocodeResults.length > 0) {
                    for (var i=0; i<geocodeResults.length; i++)
                        addAddressForm.locationsModel.append(geocodeResults[i]);
                }
                else
                {
                    message.title = qsTr("Search address", "Add or edit territory address");
                    message.text = qsTr("No address found.", "Add or edit territory address");
                    message.visible = true;
                    return
                }
            }

            function geocodeFailed(errorMessage) {
                territories.onGeocodeFinished.disconnect(geocodeFinished);
                territories.onGeocodeError.disconnect(geocodeFailed);
                message.title = qsTr("Search address", "Add or edit territory address");
                message.text = errorMessage;
                message.visible = true;
                return;
            }
        }
    }
}
