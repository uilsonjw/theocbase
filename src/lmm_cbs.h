#ifndef LMM_CBS_H
#define LMM_CBS_H

#include "lmm_assignment.h"
#include "person.h"
#include "sortfilterproxymodel.h"

class LMM_CBS : public LMM_Assignment
{
    Q_OBJECT
    Q_PROPERTY(person *reader READ reader WRITE setReader NOTIFY readerChanged)
public:
    LMM_CBS(QObject *parent = 0);
    LMM_CBS(int scheduleDbId, int assignmentDbId, QObject *parent = 0);

    person *reader() const;
    void setReader(person *reader);

    //Q_INVOKABLE SortFilterProxyModel *getSpeakerList();
    Q_INVOKABLE SortFilterProxyModel *getReaderList();

    Q_INVOKABLE bool save();

signals:
    void readerChanged();

private:
    person *m_reader;
};

#endif // LMM_CBS_H
