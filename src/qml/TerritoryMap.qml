import QtQuick 2.4
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Window 2.0
import QtPositioning 5.8
import QtLocation 5.8
import net.theocbase 1.0

TerritoryMapForm {
    id: territoryMapForm

    property variant territoryBoundaries
    property variant territoryAddresses
    property int showBoundaries: 0
    property int showMarkers: 0
    property int showStreets: 0
    property int geocodeRetryCount: 0
    property string geocodeQuery: ""
    property variant pluginParameters
    property bool isLoadingBoundaries: false
    property bool isLoadingStreets: false

    signal refreshTerritoryMapPolygon(var showAllBoundaries)

    function geocodeAddress(query){
        // send the geocode request
        geocodeRetryCount = 0;
        geocodeQuery = query;
        territories.onGeocodeFinished.connect(geocodeFinished);
        territories.onGeocodeError.connect(geocodeFailed);
        territories.geocodeAddress(geocodeQuery);
    }

    function displayTerritory(boundary, isSelected, territoryId, isHole)
    {
        var component = Qt.createComponent('TerritoryMapPolygon.qml');
        if (component.status === Component.Ready)
        {
            map.polygon = component.createObject(map);
            map.polygon.path = boundary.coordinates;
            map.polygon.boundingBox = boundary.path.boundingGeoRectangle();
            map.polygon.isSelected = isSelected;
            map.polygon.territoryId = territoryId;
            map.polygon.isHole = isHole;
            refreshTerritoryMapPolygon.connect(map.polygon.refresh);

            map.boundaries.push(map.polygon);
            map.addMapItem(map.polygon);

            if (isSelected)
                map.currentBoundary.path = map.polygon.path;

            //map.fitViewportToMapItems();
        } else
        {
            console.log("Polygon not created");
        }
    }

    function importBoundaries() {
        importDialog.x = mainWindow.x + mainWindow.width / 2 - width / 2
        importDialog.y = mainWindow.y + mainWindow.height / 2 - height / 2
        importDialog.show();
    }

    function createBoundary() {
        digitizeMode = 1
        lineComplete = false
        map.isDigitizing = true;
    }

    function splitTerritory() {
        digitizeMode = 0
        lineComplete = false
        map.isDigitizing = true;
    }

    function setBoundary(overlappedTerritoryIds, annexOverlappingAreas){
        territories.setTerritoryBoundary(territoryTreeModel.selectedTerritory.territoryId, newPolygon.path, true, overlappedTerritoryIds, annexOverlappingAreas)

        removeBoundary(false);
        reloadBoundaries(false, annexOverlappingAreas);

        map.removeMapItem(newPolygon);
        newPolygon = null;
    }

    function removeBoundary(removeCompletely) {
        if (territoryTreeModel.selectedTerritory)
        {
            var count = map.boundaries.length
            var newBoundaries = []
            for (var i = 0; i<count; i++){
                if (map.boundaries[i].territoryId === territoryTreeModel.selectedTerritory.territoryId){
                    refreshTerritoryMapPolygon.disconnect(map.boundaries[i].refresh)
                    map.removeMapItem(map.boundaries[i]);
                    map.boundaries[i].destroy();
                }
                else
                    newBoundaries.push(map.boundaries[i])
            }
            map.boundaries = newBoundaries;

            map.currentBoundary = null;
            if (removeCompletely)
                territoryTreeModel.selectedTerritory.wktGeometry = "";
        }
    }

    function zoomSelectedTerritory() {
        if (territoryTreeModel.selectedTerritory &&
                map.currentBoundary != null &&
                map.currentBoundary.boundingBox.isValid)
            map.visibleRegion = map.currentBoundary.boundingBox;
    }

    function zoomSelectedStreet() {
        if (streetModel.selectedStreet)
        {
            map.visibleRegion = streetModel.selectedStreet.boundingGeoRectangle();
        }
    }

    function zoomSelectedAddress() {
        if (addressModel.selectedAddress)
        {
            var regExp = /((?:[-+]?\.\d+|[-+]?\d+(?:\.\d*)?))\s*((?:[-+]?\.\d+|[-+]?\d+(?:\.\d*)?))/g, match;
            var lat
            var lon
            while (match = regExp.exec(addressModel.selectedAddress.wktGeometry))
            {
                lat = match[2]
                lon = match[1]
            }

            map.center = QtPositioning.coordinate(lat, lon);
            if (map.zoomLevel < 17)
                map.zoomLevel = 17
        }
    }

    function zoomFull() {
        if (map.markers.length == 0 && map.boundaries.length ==0)
        {
            // zoom to congregation's address
            var congrAddress = territories.getCongregationAddress();
            if (congrAddress !== "")
                geocodeAddress(congrAddress);
        }
        else
            map.fitViewportToMapItems();
    }

    function reloadBoundaries(zoom, loadStreets)
    {
        isLoadingBoundaries = true;
        // remove boundaries
        var count = map.boundaries.length;
        for (var i = 0; i < count; i++) {
            refreshTerritoryMapPolygon.disconnect(map.boundaries[i].refresh);
            map.removeMapItem(map.boundaries[i]);
            map.boundaries[i].destroy();
        }
        map.currentBoundary = null;
        map.boundaries = [];

        // load boundaries
        if (zoom)
            territories.boundariesLoaded.connect(zoomAndDisplayLoadedBoundaries);
        else
            territories.boundariesLoaded.connect(displayLoadedBoundaries);
        territories.getTerritoryBoundaries();

        if (loadStreets)
            refreshStreets(2);
    }

    function zoomAndDisplayLoadedBoundaries(boundaries)
    {
        territories.boundariesLoaded.disconnect(zoomAndDisplayLoadedBoundaries)
        for (var boundary in boundaries)
        {
            displayTerritory(boundaries[boundary],
                             territoryTreeModel.selectedTerritory !== null ? boundaries[boundary].territoryId === territoryTreeModel.selectedTerritory.territoryId : false,
                             boundaries[boundary].territoryId,
                             boundaries[boundary].isHole);
        }
        refreshTerritoryMapPolygon(showBoundaries);
        zoomFull();
        isLoadingBoundaries = false;
    }

    function displayLoadedBoundaries(boundaries)
    {
        territories.boundariesLoaded.disconnect(displayLoadedBoundaries)
        for (var boundary in boundaries)
        {
            displayTerritory(boundaries[boundary],
                             territoryTreeModel.selectedTerritory !== null ? boundaries[boundary].territoryId === territoryTreeModel.selectedTerritory.territoryId : false,
                             boundaries[boundary].territoryId,
                             boundaries[boundary].isHole);
        }
        refreshTerritoryMapPolygon(showBoundaries);
        isLoadingBoundaries = false;
    }

    function refreshMarkers(reload)
    {
        if (!canViewTerritoryAddresses)
            return
        if (reload > 0)
        {
            var count = map.markers.length
            for (var i = 0; i<count; i++){
                map.removeMapItem(map.markers[i]);
                map.markers[i].destroy();
            }
            map.markers = []

            if (reload === 2)
                addressModel.loadAddresses();

            var markerScale = addressModel.getMarkerScale();

            // display addresses
            for (var j = 0; j<addressModel.rowCount(); j++){
                var territoryId = addressModel.get(j).territoryId
                var addressTypeNumber = addressModel.get(j).addressTypeNumber
                var addressTypeIndex = addressTypeListModel.getIndex(addressTypeNumber)
                var color = addressTypeListModel.get(addressTypeIndex).color

                var lat = addressModel.get(j).latitude
                var lon = addressModel.get(j).longitude

                var component = Qt.createComponent('TerritoryMapMarker.qml');
                var incubator = component.incubateObject(map, {
                                                             territoryId: territoryId,
                                                             isTerritorySelected: territoryTreeModel.selectedTerritory !== null ? territoryId === territoryTreeModel.selectedTerritory.territoryId : false,
                                                             addressId: addressModel.get(j).id,
                                                             coordinate: QtPositioning.coordinate(lat, lon),
                                                             color: typeof color !== "undefined" ? color : "#000000",
                                                             markerScale: markerScale
                                                         });

                if (incubator.status === Component.Ready) {
                    var marker = incubator.object
                    map.markers.push(marker)
                    map.addMapItem(marker)
                }
            }
        }
        map.refreshTerritoryMapMarker(showMarkers)
    }

    function refreshStreets(reload)
    {
        if (reload > 0)
        {
            isLoadingStreets = true;
            // remove streets
            var count = map.streets.length;
            for (var i = 0; i < count; i++) {
                map.removeMapItem(map.streets[i]);
                map.streets[i].destroy();
            }
            //map.currentStreet = null;
            map.streets = [];

            // save current changes before reloading
            streetModel.saveStreets();

            if (reload === 2)
                streetModel.loadStreets();

            // load streets
            territories.streetsLoaded.connect(displayLoadedStreets);
            territories.getTerritoryStreets();
        }
        else
            map.refreshTerritoryMapStreets(showStreets);
    }

    function displayLoadedStreets(streets)
    {
        territories.streetsLoaded.disconnect(displayLoadedStreets)
        for (var street in streets)
        {
            displayStreet(streets[street],
                          territoryTreeModel.selectedTerritory !== null ? streets[street].territoryId === territoryTreeModel.selectedTerritory.territoryId : false,
                          streets[street].territoryId);
        }

        isLoadingStreets = false;
        map.refreshTerritoryMapStreets(showStreets)
    }

    function displayStreet(street, isSelected, territoryId)
    {
        var streetTypeId = street.streetTypeId;
        var streetTypeIndex = streetTypeListModel.getIndex(streetTypeId);
        var color = streetTypeListModel.get(streetTypeIndex).color;

        var component = Qt.createComponent('TerritoryMapPolyline.qml');
        var incubator = component.incubateObject(map, {
                                                     territoryId: territoryId,
                                                     isTerritorySelected: territoryTreeModel.selectedTerritory !== null ? territoryId === territoryTreeModel.selectedTerritory.territoryId : false,
                                                     color: typeof color !== "undefined" ? color : "#000000",
                                                     path: street.coordinates
                                                 });

        if (incubator.status === Component.Ready) {
            var newStreet = incubator.object;
            map.streets.push(newStreet);
            map.addMapItem(newStreet);
        }
    }

    function getRoot() {
        var par = parent;
        while ( par.parent !== null ) {
            par = par.parent;
        }
        return par;
    }

    function clearAddressSearchMarkers()
    {
        // remove previous markers
        var count = map.addressSearchMarkers.length
        for (var i = 0; i<count; i++){
            map.removeMapItem(map.addressSearchMarkers[i]);
            map.addressSearchMarkers[i].destroy();
        }
        map.addressSearchMarkers = []
    }

    function geocodeFinished(geocodeResults) {
        if (geocodeResults.length < 1 && geocodeRetryCount < 3 && geocodeQuery !== "") {
            // try again
            geocodeRetryCount++;
            territories.geocodeAddress(geocodeQuery);
            return;
        }

        territories.onGeocodeFinished.disconnect(geocodeFinished);
        territories.onGeocodeError.disconnect(geocodeFailed);

        clearAddressSearchMarkers();

        var count = geocodeResults.length
        if (count > 0) {
            var markerScale = addressModel.getMarkerScale();
            var minLat = Number.POSITIVE_INFINITY;
            var minLon = Number.POSITIVE_INFINITY;
            var maxLat = Number.NEGATIVE_INFINITY;
            var maxLon = Number.NEGATIVE_INFINITY;

            for (var j=0; j<geocodeResults.length; j++)
            {
                var territoryId = 0
                var addressTypeNumber = 0
                var addressTypeIndex = 0
                var color = "#000000"

                var lat = geocodeResults[j].latitude
                var lon = geocodeResults[j].longitude
                minLat = Math.min(lat, minLat);
                minLon = Math.min(lon, minLon);
                maxLat = Math.max(lat, maxLat);
                maxLon = Math.max(lon, maxLon);

                var component = Qt.createComponent('TerritoryMapMarker.qml');
                if (component.status === Component.Ready)
                {
                    map.addressSearchMarker = component.createObject(map)
                    map.addressSearchMarker.territoryId = 0;
                    map.addressSearchMarker.isTerritorySelected = false
                    map.addressSearchMarker.addressId = 0
                    map.addressSearchMarker.coordinate = QtPositioning.coordinate(lat, lon)
                    map.addressSearchMarker.color = "#000000"
                    map.addressSearchMarker.markerScale = markerScale < 1 ? 1 : markerScale
                    map.addressSearchMarker.geocodeResult = geocodeResults[j]

                    map.addressSearchMarkers.push(map.addressSearchMarker)
                    map.addMapItem(map.addressSearchMarker)
                }
            }

            map.visibleRegion = QtPositioning.rectangle(QtPositioning.coordinate(maxLat, minLon), QtPositioning.coordinate(minLat, maxLon));
            if (count > 1)
                map.zoomLevel = map.zoomLevel - 1;
        }
    }

    function geocodeFailed(errorMessage) {
        territories.onGeocodeFinished.disconnect(geocodeFinished);
        territories.onGeocodeError.disconnect(geocodeFailed);
        message.title = qsTr("Search address", "Add or edit territory address");
        message.text = errorMessage;
        message.visible = true;
        return
    }

    function addAddress(geocodeResult)
    {
        var newAddressId = addressModel.addAddress(territoryTreeModel.selectedTerritory.territoryId, geocodeResult.country,
                                    geocodeResult.state, geocodeResult.county, geocodeResult.city, geocodeResult.district,
                                    geocodeResult.street, geocodeResult.houseNumber, geocodeResult.postalCode,
                                    "POINT(" + geocodeResult.longitude + " " + geocodeResult.latitude + ")");
        if (newAddressId > 0)
        {
            clearAddressSearchMarkers();
            refreshMarkers(1);
            var addressIndex = addressModel.getAddressIndex(newAddressId)
            addressModel.setSelectedAddress(addressIndex)
        }
    }

    function reassignAddress(addressId)
    {
        if (addressId > 0 && territoryTreeModel.selectedTerritory !== null)
        {
            addressModel.updateAddress(addressId, territoryTreeModel.selectedTerritory.territoryId)
            refreshMarkers(1);
        }
    }

    function removeAddress(addressId)
    {
        if (addressId > 0)
        {
            addressModel.removeAddress(addressId);
            refreshMarkers(1);
        }
    }

    onShowBoundariesChanged: {
        refreshTerritoryMapPolygon(showBoundaries)
    }

    onShowMarkersChanged: {
        map.refreshTerritoryMapMarker(showMarkers)
    }

    onShowStreetsChanged: {
        map.refreshTerritoryMapStreets(showStreets)
    }

    onIsLoadingBoundariesChanged: busyIndicator.running = isLoadingBoundaries || isLoadingStreets
    onIsLoadingStreetsChanged: busyIndicator.running = isLoadingBoundaries || isLoadingStreets

    addressTextField.onAccepted: {
        geocodeAddress(addressTextField.text)
    }

    importBoundariesButton.onClicked: importBoundaries()

    createBoundaryButton.onClicked: createBoundary()
    splitTerritoryButton.onClicked: splitTerritory()
    removeBoundaryButton.onClicked: removeBoundary(true)

    showBoundariesButton.states:
        [
        State {
            name: "ShowAllBoundaries"; when: showBoundaries === 2
            PropertyChanges { target: showBoundariesButton; iconSource: "qrc:///icons/borders-all.svg"; }
        },
        State {
            name: "ShowSelectedBoundaries"; when: showBoundaries === 1
            PropertyChanges { target: showBoundariesButton; iconSource: "qrc:///icons/borders-selected.svg"; }
        },
        State {
            name: "HideBoundaries"; when: showBoundaries === 0
            PropertyChanges { target: showBoundariesButton; iconSource: "qrc:///icons/borders-off.svg"; }
        }
    ]

    showBoundariesButton.onClicked: showBoundaries = (showBoundaries + 1) % 3

    showMarkersButton.states: [
        State {
            name: "ShowAllAddresses"; when: showMarkers === 2
            PropertyChanges { target: showMarkersButton; iconSource: "qrc:///icons/map-marker-multiple.svg"; }
        },
        State {
            name: "ShowTerritoryAddresses"; when: showMarkers === 1
            PropertyChanges { target: showMarkersButton; iconSource: "qrc:///icons/contact_address.svg"; }
        },
        State {
            name: "HideAddresses"; when: showMarkers === 0
            PropertyChanges { target: showMarkersButton; iconSource: "qrc:///icons/map-marker-off.svg"; }
        }
    ]
    showMarkersButton.onClicked: showMarkers = (showMarkers + 1) % 3

    showStreetsButton.states: [
        State {
            name: "ShowAllStreets"; when: showStreets === 2
            PropertyChanges { target: showStreetsButton; iconSource: "qrc:///icons/street_all.svg"; }
        },
        State {
            name: "ShowTerritoryStreets"; when: showStreets === 1
            PropertyChanges { target: showStreetsButton; iconSource: "qrc:///icons/street.svg"; }
        },
        State {
            name: "HideStreets"; when: showStreets === 0
            PropertyChanges { target: showStreetsButton; iconSource: "qrc:///icons/street_off.svg"; }
        }
    ]
    showStreetsButton.onClicked: showStreets = (showStreets + 1) % 3

    editModeButton.states: [
        State {
            name: "View"; when: map.editMode === 0
            PropertyChanges { target: editModeButton; iconSource: "qrc:///icons/territory_view.svg"; }
        },
        State {
            name: "EditAddresses"; when: map.editMode === 1
            PropertyChanges { target: editModeButton; iconSource: "qrc:///icons/map-marker-edit.svg"; }
        },
        State {
            name: "EditBoundaries"; when: map.editMode === 2
            PropertyChanges { target: editModeButton; iconSource: "qrc:///icons/territory_edit.svg"; }
        }
    ]
    editModeButton.onClicked: map.editMode = (map.editMode + 1) % 3

    zoomFullButton.onClicked: zoomFull()

    zoomSlider.onValueChanged: {
        map.zoomLevel = zoomSlider.value
    }

    //map.plugin: osmPlugin

    Connections {
        target: territoryTreeModel
        function onSelectedChanged(value) {
            if (territoryTreeModel.selectedTerritory)
            {
                if (!territoryTreeModel.selectedTerritory.wktGeometry || territoryTreeModel.selectedTerritory.wktGeometry === "")
                {
                    map.currentBoundary = null;
                }
            }
        }
        function onTerritoryRemoved() {
            reloadBoundaries(false, true);
            refreshMarkers(2);
        }
    }

    Connections {
        target: territoryTreeView
        function onDoubleClicked() { zoomSelectedTerritory(); }
    }

    Connections {
        target: territoryManagement
        function onUpdateMarkers(dataChanged) { refreshMarkers(dataChanged ? 1 : 0); }
        function onUpdateStreets(dataChanged) { refreshStreets(dataChanged ? 1 : 0); }
        function onZoomSelectedAddress() { zoomSelectedAddress(); }
        function onZoomSelectedStreet() { zoomSelectedStreet(); }
    }

    MessageDialog {
        id: addBoundaryMessageDialog
        standardButtons: MessageDialog.Yes | MessageDialog.No | MessageDialog.Cancel
        icon: StandardIcon.Question
        modality: Qt.ApplicationModal
        onYes: setBoundary(overlappedTerritoryIds, true)
        onNo: setBoundary(overlappedTerritoryIds, false)
        onRejected: {
            map.removeMapItem(newPolygon);
            newPolygon = null;
        }

        property var overlappedTerritoryIds
    }

    // This property indicates the mode
    //   0 = polyline
    //   1 = polygon
    property int digitizeMode: 1
    property bool lineComplete: false
    property SimpleMarker snapMarker
    property SimplePolygon newPolygon
    property SimplePolyline newPolyline
    property SimplePolyline rubberBand

    function createElements(point) {
        if (snapMarker === null)
        {
            var componentMarker = Qt.createComponent("SimpleMarker.qml");

            if (componentMarker.status === Component.Ready) {
                snapMarker = componentMarker.createObject(map);
                snapMarker.coordinate = point
                snapMarker.markerType = 3
                snapMarker.visible = false
                snapMarker.isClickable = false
                map.addMapItem(snapMarker)
            }else{
                console.log("Marker not created")
            }
        }

        switch (digitizeMode)
        {
        case 0:
            //Polyline mode
            if(newPolyline === null){
                createLine(point)
                createRubberBand(point)
            }
            newPolyline.mainPolyline.insertCoordinate(newPolyline.path.length - 1, point)
            break;
        default:
            //Polygon mode
            if(newPolygon === null){
                createPolygon(point)
                createRubberBand(point)
            }
            newPolygon.mainPolygon.addCoordinate(point)
            break;
        }
    }

    function createLine(point){
        var componentLine = Qt.createComponent("SimplePolyline.qml")

        if (componentLine.status === Component.Ready) {
            newPolyline = componentLine.createObject(map);
            newPolyline.lineWidth = 2
            newPolyline.mainPolyline.addCoordinate(point)

            map.addMapItem(newPolyline)
        }else{
            console.log("Line not created")
        }
    }

    function createPolygon(point){
        var componentPolygon = Qt.createComponent("SimplePolygon.qml")

        if (componentPolygon.status === Component.Ready) {
            newPolygon = componentPolygon.createObject(map);
            newPolygon.mainPolygon.addCoordinate(point)

            map.addMapItem(newPolygon)
        }else{
            console.log("Polygon not created")
        }
    }

    function createRubberBand(point){
        map.removeMapItem(rubberBand);
        var component = Qt.createComponent("SimplePolyline.qml")

        if (component.status === Component.Ready) {
            rubberBand = component.createObject(map);
            rubberBand.mainPolyline.addCoordinate(point)
            rubberBand.mainPolyline.addCoordinate(point)
            rubberBand.mainPolyline.addCoordinate(point)
            map.addMapItem(rubberBand)
        }else{
            console.log("Rubber band not created")
        }
    }

    Keys.onPressed: {
        if (event.key === Qt.Key_Plus) {
            map.zoomLevel++;
        } else if (event.key === Qt.Key_Minus) {
            map.zoomLevel--;
        } else if (event.key === Qt.Key_Left || event.key === Qt.Key_Right ||
                   event.key === Qt.Key_Up   || event.key === Qt.Key_Down) {
            var dx = 0;
            var dy = 0;

            switch (event.key) {
            case Qt.Key_Left: dx = map.width / 4; break;
            case Qt.Key_Right: dx = -map.width / 4; break;
            case Qt.Key_Up: dy = map.height / 4; break;
            case Qt.Key_Down: dy = -map.height / 4; break;
            }

            var mapCenterPoint = Qt.point(map.width / 2.0 - dx, map.height / 2.0 - dy);
            map.center = map.toCoordinate(mapCenterPoint);
        } else if (event.key === Qt.Key_Escape) {
            if (map.isDigitizing){
                // cancel digitizing
                lineComplete = true
                map.isDigitizing = false
                if (rubberBand !== null)
                {
                    map.removeMapItem(rubberBand);
                    rubberBand = null
                }
                switch (digitizeMode)
                {
                case 0:
                    if (newPolyline !== null)
                    {
                        map.removeMapItem(newPolyline);
                        newPolyline = null;
                    }
                    break;
                default:
                    if (newPolygon !== null)
                    {
                        map.removeMapItem(newPolygon);
                        newPolygon = null;
                    }
                    break;
                }
            }
        } else if (event.key === Qt.Key_Backspace) {
            if (map.isDigitizing){
                var point
                switch (digitizeMode)
                {
                case 0:
                    // remove last but one point (last point = first point)
                    if (newPolyline !== null && newPolyline.mainPolyline.pathLength() > 2)
                    {
                        newPolyline.mainPolyline.removeCoordinate(newPolyline.mainPolyline.pathLength() - 2)
                        point = newPolyline.mainPolyline.path[newPolyline.mainPolyline.pathLength() - 2]
                    }
                    break;
                default:
                    if (newPolygon !== null && newPolygon.mainPolygon.path.length > 2)
                    {
                        point = newPolygon.mainPolygon.path[newPolygon.mainPolygon.path.length - 1]
                        newPolygon.mainPolygon.removeCoordinate(point)
                        point = newPolygon.mainPolygon.path[newPolygon.mainPolygon.path.length - 1]
                    }
                    break;
                }
                if (rubberBand !== null)
                {
                    // update rubber band
                    rubberBand.mainPolyline.replaceCoordinate(0, point);
                }
            }
        }
    }

    Connections {
        target:  mapMouseArea

        function onPositionChanged(mouse) {
            if (!map.isDigitizing)
                return

            var point = map.toCoordinate(Qt.point(mouse.x, mouse.y));
            var tstPoint = map.toCoordinate(Qt.point(mouse.x - 10, mouse.y));
            var delta = map.toCoordinate(Qt.point(0, 0)).distanceTo(map.toCoordinate(Qt.point(10, 0)))

            var snapPoint = territories.getClosestPoint(point, delta);
            if (snapMarker !== null)
            {
                if(snapPoint.isValid)
                {
                    point = snapPoint
                    snapMarker.coordinate = snapPoint
                    snapMarker.visible = true
                }
                else
                    snapMarker.visible = false
            }

            if (rubberBand !== null)
                rubberBand.mainPolyline.replaceCoordinate(1, point);
        }

        function onClicked(mouse) {
            map.forceActiveFocus();
            if (!map.isDigitizing)
                return

            if (mouse.button === Qt.LeftButton)
            {
                if (lineComplete)
                    return;
                var point = map.toCoordinate(Qt.point(mouse.x, mouse.y));
                // replace clicked point if a point is snapped
                if (snapMarker !== null && snapMarker.visible)
                    point = snapMarker.coordinate

                if (rubberBand !== null)
                {
                    // update rubber band
                    rubberBand.mainPolyline.replaceCoordinate(0, point);
                    rubberBand.mainPolyline.replaceCoordinate(1, point);
                }
                createElements(point)
            }
            if (mouse.button === Qt.RightButton)
            {
                lineComplete = true
                map.isDigitizing = false
                if (rubberBand !== null)
                {
                    map.removeMapItem(rubberBand);
                    rubberBand = null
                }
                switch (digitizeMode)
                {
                case 0:
                    if (newPolyline !== null)
                    {
                        if (territoryTreeModel.selectedTerritory)
                        {
                            territoryTreeModel.selectedTerritory.save();
                            var newTerritoryId = territories.splitTerritory(territoryTreeModel.selectedTerritory.territoryId, newPolyline.path);
                            if (newTerritoryId !== 0)
                            {
                                removeBoundary(false);
                                territoryTreeModel.updateModel();
                                var index = territoryTreeModel.index(newTerritoryId)
                                territoryTreeModel.setSelectedTerritory(index)
                                reloadBoundaries(false, true);
                            }

                            map.removeMapItem(newPolyline);
                            newPolyline = null;
                        }
                    }
                    break;
                default:
                    if (newPolygon !== null)
                    {
                        if (territoryTreeModel.selectedTerritory)
                        {
                            territoryTreeModel.selectedTerritory.save();
                            // check for overlapping territories
                            var territoryList = territories.getAllTerritories();
                            var count = territoryList.length;
                            var overlappedTerritories = [];
                            var overlappedTerritoryNumbers = [];
                            for (var i = 0; i < count; i++){
                                if (typeof territoryList[i] !== "undefined")
                                    if (territoryList[i].territoryId !== territoryTreeModel.selectedTerritory.territoryId && territoryList[i].crosses(newPolygon.path)) {
                                        overlappedTerritories.push(territoryList[i].territoryId)
                                        overlappedTerritoryNumbers.push(territoryList[i].territoryNumber)
                                    }
                            }

                            if (overlappedTerritories.length > 0) {
                                addBoundaryMessageDialog.text = qsTr("The new boundary overlaps %n territory(ies):", "Add or edit territory boundary", overlappedTerritories.length) +
                                        "\n" + overlappedTerritoryNumbers.join(",") + "\n" +
                                        qsTr("Do you want to assign overlapping areas to the current territory?\nSelect 'No' if overlapping areas should remain in their territories and to add only the part, that doesn't overlap other territories.", "Add or edit territory boundary")
                                addBoundaryMessageDialog.overlappedTerritoryIds = overlappedTerritories;
                                addBoundaryMessageDialog.visible = true
                            } else
                                setBoundary(overlappedTerritories, false)
                        }
                    }
                }
            }
        }
    }

    Menu {
        id: contextMenu
        property var marker;
        property int featureTerritoryId;
        property int markerAddressId;

        function show(sender, territoryId, addressId)
        {
            marker = sender;
            featureTerritoryId = territoryId
            markerAddressId = addressId
            popup()
        }

        onAboutToShow: {
            addAddressMenuItem.enabled = territoryTreeModel.selectedTerritory
            reassignAddressMenuItem.enabled = territoryTreeModel.selectedTerritory !== null &&
                    territoryTreeModel.selectedTerritory.territoryId !== featureTerritoryId
        }

        MenuItem {
            id: annexAreaMenuItem
            text: qsTr("Join to the selected territory", "Join two territories into one")
            iconSource: "qrc:///icons/boundary_join.svg"
            visible: canEditTerritories && contextMenu.featureTerritoryId !== 0 && contextMenu.markerAddressId === -1 &&
                     territoryTreeModel.selectedTerritory !== null && territoryTreeModel.selectedTerritory.territoryId !== contextMenu.featureTerritoryId
            onTriggered: {
                var territoryList = territories.getAllTerritories();
                var count = territoryList.length;
                for (var i = 0; i < count; i++){
                    if (typeof territoryList[i] !== "undefined")
                        if (territoryList[i].territoryId === contextMenu.featureTerritoryId) {
                            var overlappedTerritoryNumbers = [];
                            overlappedTerritoryNumbers.push(contextMenu.featureTerritoryId);
                            if (territories.setTerritoryBoundary(territoryTreeModel.selectedTerritory.territoryId, territoryList[i].wktGeometry, true, overlappedTerritoryNumbers, true))
                            {
                                // delete territory
                                territoryTreeModel.removeTerritory(contextMenu.featureTerritoryId);
                            }
                            break;
                        }
                }

                removeBoundary(false);
                reloadBoundaries(false, true);
                refreshMarkers(2);
            }
        }

        MenuItem {
            id: addAddressMenuItem
            text: qsTr("Add address to selected territory")
            iconSource: "qrc:///icons/territory_add_address.svg"
            visible: canEditTerritories && contextMenu.featureTerritoryId === 0 && contextMenu.markerAddressId > -1
            onTriggered: territoryMapForm.addAddress(contextMenu.marker.geocodeResult)
        }

        MenuItem {
            id: reassignAddressMenuItem
            text: qsTr("Assign to selected territory", "Reassign territory address")
            iconSource: "qrc:///icons/exchange.svg"
            visible: canEditTerritories && contextMenu.featureTerritoryId > 0 && contextMenu.markerAddressId > -1
            onTriggered: territoryMapForm.reassignAddress(contextMenu.markerAddressId)
        }

        MenuItem {
            id: removeAddressMenuItem
            text: qsTr("Remove address", "Delete territory address")
            iconSource: "qrc:///icons/territory_remove_address.svg"
            visible: canEditTerritories && contextMenu.featureTerritoryId > 0 && contextMenu.markerAddressId > -1
            onTriggered: territoryMapForm.removeAddress(contextMenu.markerAddressId)
        }
    }

    Component.onCompleted: {
        // set map plugin
        var parameters = new Array()
        for (var prop in geoServiceParameters){
            var parameter = Qt.createQmlObject('import QtLocation 5.8; PluginParameter{ name: "'+ prop + '"; value: "' + geoServiceParameters[prop] + '"}', territoryMapForm)
            parameters.push(parameter)
        }
        territoryMapForm.pluginParameters = parameters
        var provider = defaultGeoServiceProvider
        var plugin
        if (parameters && parameters.length > 0)
            plugin = Qt.createQmlObject ('import QtLocation 5.8; Plugin{ name:"' + provider + '"; parameters: territoryMapForm.pluginParameters}', territoryMapForm)
        else
            plugin = Qt.createQmlObject ('import QtLocation 5.8; Plugin{ name:"' + provider + '"}', territoryMapForm)
        map.plugin = plugin;

        reloadBoundaries(true, true);
        refreshMarkers(2);
        showBoundaries = 2;
        showMarkers = 2;
    }

    Window {
        id: importDialog
        title: qsTr("Import territory data", "Territory data import dialog")
        width: 400
        minimumWidth: 400
        minimumHeight: 330
        height: 270
        flags: Qt.Popup | Qt.Dialog
        modality: Qt.WindowModal
        color: myPalette.window

        onVisibleChanged: {
            x = mainWindow.x + mainWindow.width / 2 - width / 2
            y = mainWindow.y + mainWindow.height / 2 - height / 2
        }

        MessageDialog {
            id: message
            standardButtons: StandardButton.Ok
            icon: StandardIcon.Warning
            modality: Qt.ApplicationModal

            onAccepted: {
                importForm.displayProgress = false
            }
        }

        TerritoryImportForm {
            id: importForm
            anchors.leftMargin: 10
            anchors.rightMargin: 10
            anchors.topMargin: 10
            anchors.bottomMargin: 10

            property DataObjectListModel addressTypeListModel: territories.getAddressTypes()
            property url fileName: importOption == 1 ? Qt.resolvedUrl(boundariesFileDialog.fileUrl) : Qt.resolvedUrl(addressesFileDialog.fileUrl)
            property url failedAddressesFileName: Qt.resolvedUrl(failedAddressesFileDialog.fileUrl)
            property string csvDelimiter: ""
            property int importedItemCount: 0
            property int itemCount: 0

            importButton.enabled: false
            addressTypeComboBox.model: addressTypeListModel
            addressTypeComboBox.currentIndex: addressTypeListModel.getIndex(addressModel.getDefaultAddressTypeNumber())

            function selectFile() {
                switch (importOption)
                {
                case 1:
                    boundariesFileDialog.open();
                    break;
                case 2:
                    addressesFileDialog.open();
                    break;
                }
            }

            function addressImportProgressChanged(rowCount, importCount)
            {
                itemCount = rowCount
                importedItemCount = importCount
                progressLabel.text = qsTr("%1 of %2 address(es) imported.", "Territory address import progress").arg(importCount).arg(rowCount);
            }

            importButton.onClicked:
            {
                switch (importOption)
                {
                case 1:
                {
                    var nameMatchField = importForm.boundaryNameComboBox.currentIndex;
                    var descriptionMatchField = importForm.boundaryDescriptionComboBox.currentIndex;
                    var searchByDescription = importForm.searchByDescriptionCheckBox.checked;

                    if (nameMatchField !== 0 && descriptionMatchField !== 0 && nameMatchField === descriptionMatchField)
                    {
                        message.title = qsTr("Import territory boundaries", "Territory import dialog");
                        message.text = qsTr("The selected fields should be different.", "Territory import from KML file");
                        message.icon = StandardIcon.Warning;
                        message.visible = true;
                        return;
                    }

                    var importRes = territories.importKmlGeometry(importForm.fileName, nameMatchField, descriptionMatchField, searchByDescription);
                    if (importRes >= 0)
                    {
                        message.title = qsTr("Import territory data", "Territory import dialog");
                        message.text = qsTr("%n territory(ies) imported or updated.", "Number of territories imported or updated", importRes);
                        message.icon = StandardIcon.Information;
                        message.visible = true;

                        reloadBoundaries(true, false);
                        map.fitViewportToMapItems();
                        territoryTreeModel.updateModel();
                    }
                    else
                    {
                        message.title = qsTr("Import territory data", "Territory import dialog");
                        message.text = qsTr("The import file could not be read.", "Territory address import from file");
                        message.icon = StandardIcon.Critical;
                        message.visible = true;
                    }

                    break;
                }
                case 2:
                {
                    var addressField = importForm.addressComboBox.currentIndex;
                    var nameField = importForm.addressNameComboBox.currentIndex;

                    if (addressField === -1 || nameField === -1)
                    {
                        message.title = qsTr("Import territory data", "Territory import dialog");
                        message.text = qsTr("Please select the address and name fields.", "Fields for territory address import from file");
                        message.icon = StandardIcon.Warning;
                        message.visible = true;
                        return;
                    }

                    if (addressField >= 0 && nameField >= 0 && addressField === nameField)
                    {
                        message.title = qsTr("Import territory data", "Territory import dialog");
                        message.text = qsTr("The selected fields should be different.", "Fields for territory address import from file");
                        message.icon = StandardIcon.Warning;
                        message.visible = true;
                        return;
                    }

                    if (territoryTreeModel.selectedTerritory == null)
                    {
                        message.title = qsTr("Import territory addresses", "Territory import dialog");
                        message.text = qsTr("The addresses will be added to the current territory. Please select a territory first.", "Territory address import from file");
                        message.icon = StandardIcon.Warning;
                        message.visible = true;
                        return;
                    }

                    var addressTypeNumber = addressTypeListModel.get(importForm.addressTypeComboBox.currentIndex).id;
                    if (addressTypeNumber < 1)
                        addressTypeNumber = 1

                    territories.onAddressImportProgressChanged.connect(addressImportProgressChanged);

                    progressLabel.text = "";
                    displayProgress = true;
                    var addressImportRes = territories.importAddresses(
                                importForm.fileName, addressField, nameField, csvDelimiter,
                                territoryTreeModel.selectedTerritory.territoryId,
                                addressTypeNumber, importForm.failedAddressesFileName);

                    territories.onAddressImportProgressChanged.disconnect(addressImportProgressChanged);

                    if (addressImportRes >= 0)
                    {
                        message.title = qsTr("Import territory data", "Territory import dialog");
                        message.text = qsTr("%n address(es) imported.", "Number of addresses imported", addressImportRes);
                        message.icon = StandardIcon.Information;
                        message.visible = true;

                        refreshMarkers(2);
                    }
                    else
                    {
                        message.title = qsTr("Import territory data", "Territory import dialog");
                        if (addressImportRes === -2)
                            message.text = qsTr("No valid territory selected.", "Territory boundary import");
                        else
                            message.text = qsTr("The import file could not be read.", "Territory boundary import");
                        message.icon = StandardIcon.Critical;
                        message.visible = true;
                    }

                    break;
                }
                }
            }

            closeButton.onClicked: importDialog.close();

            onFileNameChanged: {
                var path = fileName.toString();
                importButton.enabled = path !== ""
                if (Qt.platform.os === "windows")
                    path = path.replace(/^(file:\/{3})|(qrc:\/{2})|(http:\/{2})/,"");
                else
                    path = path.replace(/^(file:\/{2})|(qrc:\/{2})|(http:\/{2})/,"");

                var pathText = decodeURIComponent(path);
                if (fileNameTextField.text !== pathText)
                {
                    fileNameTextField.text = pathText;
                    if (importOption == 2 && pathText !== "")
                    {
                        var schema = territories.getCSVschema(importForm.fileName, "");
                        csvDelimiter = schema.delimiter;
                        importForm.csvFieldModel.clear();
                        var i = 0;
                        for (var fieldName in schema.fields){
                            importForm.csvFieldModel.append({"name":schema.fields[fieldName]});
                            if (schema.fields[fieldName].toLocaleLowerCase() === qsTr("Address", "Default Address-field for territory address import").toLocaleLowerCase())
                                importForm.addressComboBox.currentIndex = i;
                            if (schema.fields[fieldName].toLocaleLowerCase() === qsTr("Name", "Default Name-field for territory address import").toLocaleLowerCase())
                                importForm.addressNameComboBox.currentIndex = i;
                            i++;
                        }
                    }
                }
            }

            onFailedAddressesFileNameChanged: {
                var path = failedAddressesFileName.toString();
                if (Qt.platform.os === "windows")
                    path = path.replace(/^(file:\/{3})|(qrc:\/{2})|(http:\/{2})/,"");
                else
                    path = path.replace(/^(file:\/{2})|(qrc:\/{2})|(http:\/{2})/,"");

                var pathText = decodeURIComponent(path);
                if (failedFileNameTextField.text !== pathText)
                {
                    failedFileNameTextField.text = pathText;
                }
            }

            selectFileButton.onClicked: selectFile()

            selectFailedFileButton.onClicked: failedAddressesFileDialog.open()
        }

        FileDialog {
            id: boundariesFileDialog
            title: qsTr("Open file")
            nameFilters: [ qsTr("KML files (*.kml)", "Filedialog pattern"), qsTr("All files (*)", "Filedialog pattern") ]
            selectedNameFilter: qsTr("KML files (*.kml)", "Filedialog pattern")
            selectMultiple: false
        }

        FileDialog {
            id: addressesFileDialog
            title: qsTr("Open file")
            nameFilters: [ qsTr("CSV files (*.csv)", "Filedialog pattern"), qsTr("Text files (*.txt)", "Filedialog pattern"), qsTr("All files (*)", "Filedialog pattern") ]
            selectedNameFilter: qsTr("CSV files (*.csv)", "Filedialog pattern")
            selectMultiple: false
        }

        FileDialog {
            id: failedAddressesFileDialog
            title: qsTr("Save file")
            nameFilters: [ qsTr("CSV files (*.csv)", "Filedialog pattern"), qsTr("Text files (*.txt)", "Filedialog pattern"), qsTr("All files (*)", "Filedialog pattern") ]
            selectedNameFilter: qsTr("CSV files (*.csv)", "Filedialog pattern")
            selectMultiple: false
            selectExisting: false
        }
    }

    Plugin {
           id: osmPlugin
           name: "osm"
           // specify plugin parameters if necessary
           // PluginParameter {
           //     name:
           //     value:
           // }
           //PluginParameter{ name: "style"; value: "CycleMap"}
       }
}
