﻿delete from lmm_workbookregex where lang = 'fr'
insert into lmm_workbookregex (lang, key, value) select 'fr', 'date', '^(?<fromday>\d+)[er\s]*(?<month1>[^-–]*)[-–](?<thruday>\d+)e*r*\s(?<month2>\D+)$'
insert into lmm_workbookregex (lang, key, value) select 'fr', 'song', 'Cantique\s*(\d+)(.*)'
insert into lmm_workbookregex (lang, key, value) select 'fr', 'timing', '[(]•*\s*(?<timing>\d+)\smin(?<timingextra>[^)]*)[)]'
insert into lmm_workbookregex (lang, key, value) select 'fr', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
delete from lmm_workbookregex where lang = 'no'
insert into lmm_workbookregex (lang, key, value) select 'no', 'date', '^(?<fromday>\d+)[.]\s*(?<month1>[^\s]*)\s*(–|–|til)\s*(?<thruday>\d+)[.]\s(?<month2>\D+)$'
insert into lmm_workbookregex (lang, key, value) select 'no', 'song', 'Sang nr.\s*(\d+)(.*)'
insert into lmm_workbookregex (lang, key, value) select 'no', 'timing', '[(](?<timing>\d+)\smin(?<timingextra>[^[)]*)[)]'
insert into lmm_workbookregex (lang, key, value) select 'no', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
INSERT INTO languages (id,language, code) SELECT (SELECT MAX(id)+1 FROM languages), 'Ukrainian', 'uk' WHERE NOT EXISTS (SELECT 1 FROM languages WHERE code = 'uk')
delete from congregationmeetingtimes where id not in (select id from congregationmeetingtimes group by congregation_id,mtg_year)
