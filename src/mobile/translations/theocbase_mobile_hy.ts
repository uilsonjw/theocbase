<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hy">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation>Ամսաթիվ</translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Մյուս ուսումնական առաջադրանքը չհանձնարարել</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Ուսումնական առաջադրանքը մնում է նույնը</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Սխալ տվյալներ</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Ավելացնե՞լ ժամանակ։</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Թեմա</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Աղբյուր</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Սովորող</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Օգնական</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Արդյունքը</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Ավարտվեց</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Կամավոր</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Ընտրել կամավոր</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Ընթացիկ ուսումնասիրություն</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Վարժություններ կատարված են</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Հաջորդ դասը</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Ընտրեք հաջորդ ուսումնասիրությունը</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Ժամանակ</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Գրառումներ</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Դպրոցի մասին տեղեկություն</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Սկսել վայրկյանաչափը</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Կանգնեցնել վայրկյանաչափը</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Կարգավորում</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Ընտրիր իրադրությունը</translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>No exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit overseer&#x27;s visit</source>
        <translation>Շրջանային վերակացուի այցելություն</translation>
    </message>
    <message>
        <source>Circuit assembly</source>
        <translation>Շրջանային համաժողով</translation>
    </message>
    <message>
        <source>Regional convention</source>
        <translation>Տարածաշրջանային համաժողով</translation>
    </message>
    <message>
        <source>Memorial</source>
        <translation>Հիշաըակի Երեկո</translation>
    </message>
    <message>
        <source>Other exception</source>
        <translation>Այլ բացառություններ</translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation>Ժողովի օրեր</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Միջշաբաթյա հանդիպում</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Շաբաթավեջի հանդիպում</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Թեմա</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Աղբյուր</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Հանդիպում անցկացնող</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Հռետոր</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Ընթերցող</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Գրառումներ</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Մանրամասներ</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <comment>Page title</comment>
        <translation>Գրառումներ</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>ԳԱՆՁԵՐ ԱՍՏԾՈՒ ԽՈՍՔԻՑ</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>ՀՄՏԱՆԱՆՔ ՔԱՐՈԶՉԱԿԱՆ ԾԱՌԱՅՈՒԹՅԱՆ ՄԵՋ</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>ԱՊՐԵՆՔ ՈՐՊԵՍ ՔՐԻՍՏՈՆՅԱ</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Նախագահող</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Հատուկ խորհրդատու</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Երգ %1 և աղոթք</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Երգ</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Անցկացնող</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Ընթերցող</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Բացման խոսք</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Աղոթք</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation>Ներմուծել ծրագիրը...</translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation>Եզրափակիչ խոսք</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Ուսումնական առաջադրանքը մնում է նույնը</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Սկսել վայրկյանաչափը</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Կանգնեցնել վայրկյանաչափը</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Ավելացնե՞լ ժամանակ։</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Թեմա</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Աղբյուր</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Սովորող</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Արդյունքը</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Ավարտված է</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>ժամանակի ընտրություն</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Ընթացիկ ուսումնասիրություն</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Վարժությունները կատարված են</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Հաջորդ դասը</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Ընտրեք հաջորդ դասը</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Գրառումներ</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Օգնական</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Կամավոր</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>Վայրկյանաչափ</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>Օգնականը չպետք է լինի հակառակ սեռի նեռկայացուցիչ</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>Դաս</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Մանրամասնությունները</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>Քաշեք թարմացնելու համար ...</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Ազատ արձակեք թարմացնելու համար․․․</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation>Օգտագործողի անունը</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Գաղտնաբառ</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Մուտք</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>օգտվողի անունը կամ էլ. հասցե</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Էլ․ հասցե</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Ստեղծել հաշիվ</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>Վերականգնել գաղտնաբառը</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>Էլեկտր․ փոստի հասցեն չգնվեց։</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Մոռացել եք գաղտնաբառը</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>Մուտքի էջ</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation>Շաբաթ` սկսած %1</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation>ԱՐՏԱԳՆՅԱ ՀՌԵՏՈՐՆԵՐ</translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>%1 հռետոր արտագնյա է</numerusform>
            <numerusform>%1 հռետոներ արտագնա են</numerusform>
        </translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation>Չկան արտագնա հռետորներ այս շաբաթավերջ</translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Ծրագիր</translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation>Աշխատանքային թերթիկ</translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation>Արտագնա հռետորների ցուցակ</translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Տպել</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Միջշաբաթյա հանդիպում</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Շաբաթավեջի հանդիպում</translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation>Համակցություն</translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation>Հանձնարարության արդյունքներ</translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation>Ձևանմուշ</translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Text size</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Երգ և աղոթք</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Երգ %1 և աղոթք</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>ՀԱՆՐԱՅԻՆ ԵԼՈՒՅԹ</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>ԴԻՏԱՐԱՆԻ ՈՒՍՈՒՄՆԱՍԻՐՈՒԹՅՈՒՆ</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Երգ %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Անցկացնող</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Ընթերցող</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation>Ներմուծել Դիտ․․․</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>Թեմա</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>Ժողով</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Հռետոր</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Բջջային</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Հեռախոս</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Էլ․ հասցե</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Տեղեկատվություն</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Հյուրընկալ</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>Հանրային ելույթ</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>Անուն</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Ազգանուն</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Եղբայր</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Քույր</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Ծառայող</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Ընտանիք</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>Ընտանիքի անդամը</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Կոնտակտային տվյալներ</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Հեռախոս</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Էլ․ հասցե</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>Բոլոր դասարանները</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Միայն գլխավոր դասը</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Միայն օժանդակ դասարաններ</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Ընտանիքի գլուխ</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Օգնական</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Տեղեկություն</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>Գործունյա</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Նախագահող</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>ԳԱՆՁԵՐ ԱՍՏԾՈՒ ԽՈՍՔԻՑ</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Աստվածաշնչի ընթերցանություն</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Առաջին այցելություն</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Վերայցելություն</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Աստվածաշնչի ուսումնասիրություն</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>ԱՊՐԵՆՔ ՈՐՊԵՍ ՔՐԻՍՏՈՆՅԱ բաժնի ելույթեր</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Ժողովի՝ Աստվածաշնչի ուսումնասիրություն</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Ժողովի՝ Աստվածաշնչի ուսումնասիրություն</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>Նոր քարոզիչ</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Աղոթք</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation>Հանրային ելույթի հռետորին հյուրընկալող</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Բջջային</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Միջշաբաթյա հանդիպում</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>Ելույթ</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Շաբաթավերջի հանդիպում</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>Դիտարանի ուսումնասիրություն անցկացնող</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation>Դիտարանի ուսումնասիրության ընթերցող</translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Քարոզիչներ</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation>Ընտրության ցուցակ</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Կարգավորումներ</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Ելք</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Տեղեկատվություն</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Տարբերակ</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>TheocBase գլխավոր էջ</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Խորհուրդ</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>Վերջին սինխրոնիզացումը՝ %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Ծրագիր</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Սկսելու ժամանակը</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Ցույց տալ տևողությունը</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Օգտագօրծողի ինտերֆեյս</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Լեզու</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Մուտք</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Էլ․ հասցե</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Անուն</translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Printing</source>
        <translation>Տպել</translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Սինքրոնիզացում</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>Նախագահող</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Երգ</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation>Շաբաթավերջի հանդիպման նախագահող</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>Երգ</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation>Դիտարանի երգը</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Աղոթք</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation>Դիտարանի թողարկում</translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>Հոդված</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Թեմա</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>Անցկացնող</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>Ընթերցող</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>Ժամանակագիծ</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>Ընտրված ամսաթվից առաջ շաբաթների թիվը</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>Ընտրված ամսաթվից հետո շաբաթների թիվը </translation>
    </message>
    <message>
        <source>weeks</source>
        <translation> շաբաթներ</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation>Ընգծված շաբաթների թիվը առաջադրանքներից հետո</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>Նույն փոփոխությունները գտնվում են և տեղային և առցանց պահեստում (% 1 տող): Ցանկանու՞մ եք պահպանել տեղային փոփոխությունները:</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>Արցանց տվյալները վերականգնվել են: Ձեր տեղական տվյալները կփոխարինվեն: Շարունակե՞լ</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Հաջորդ ուսումնական դասը մի նշանակեք</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>Սահմանված չէ</translation>
    </message>
</context></TS>