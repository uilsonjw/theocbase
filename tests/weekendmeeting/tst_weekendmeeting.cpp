#include <QtTest>
#include "../common/common.h"
#include "cpublictalks.h"
#include "cpersons.h"

// add necessary includes here

class weekendmeeting : public QObject
{
    Q_OBJECT

public:
    weekendmeeting();
    ~weekendmeeting();

private slots:
    void initTestCase();
    void cleanupTestCase();    
    void test1_meetingNotes();
};

weekendmeeting::weekendmeeting()
{
}

weekendmeeting::~weekendmeeting()
{
}

void weekendmeeting::initTestCase()
{
    common::initDataBase();
}

void weekendmeeting::cleanupTestCase()
{
    common::clearDatabase();
}

void weekendmeeting::test1_meetingNotes()
{
    // chairman
    auto *chairman = new person();
    chairman->setGender(person::Male);
    chairman->setFirstname("C");
    chairman->setLastname("Chairman");
    chairman->setUsefor(person::Chairman);
    cpersons::addPerson(chairman);

    QDate d1(2021, 2, 1);
    QDate d2 = d1.addDays(7);

    QString meetingNotes = "Zoom=Brother1\nAV=Brother2";

    cpublictalks cpt;
    // save some meeting info
    QSharedPointer<cptmeeting> meeting1(cpt.getMeeting(d1));
    meeting1->setChairman(chairman);
    meeting1->save();
    meeting1->saveNotes();
    // load existing meeting and save notes
    meeting1.reset(cpt.getMeeting(d1));
    meeting1->notes = meetingNotes;
    meeting1->saveNotes();

    // load empty meeting and save notes
    QSharedPointer<cptmeeting> meeting2(cpt.getMeeting(d2));
    meeting2->notes = meetingNotes;
    meeting2->saveNotes();

    QSharedPointer<cptmeeting> tstMeeting(cpt.getMeeting(d1));
    QCOMPARE(tstMeeting->notes, meetingNotes);
    tstMeeting.reset(cpt.getMeeting(d2));
    QCOMPARE(tstMeeting->notes, meetingNotes);

    // clear notes
    meeting1->notes = "";
    meeting1->saveNotes();
    // check that notes are empty
    tstMeeting.reset(cpt.getMeeting(d1));
    QVERIFY(tstMeeting->notes.isEmpty());
}

QTEST_MAIN(weekendmeeting)

#include "tst_weekendmeeting.moc"
