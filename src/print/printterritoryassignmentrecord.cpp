/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "printterritoryassignmentrecord.h"

PrintTerritoryAssignmentRecord::PrintTerritoryAssignmentRecord()
{
    currentTemplateData = QSharedPointer<TemplateData>(
            new TemplateData(TemplateData::TerritoryAssignmentRecordTemplate, "territoryAssignmentRecordTemplate", "TR-AssignmentRecord.htm", "tr-"));
}

QVariant PrintTerritoryAssignmentRecord::fillTemplate()
{
    QString str = getTemplate();
    str = initLayout(str);
    str = ifthen.tokenize(str);

    // titles
    str = fillTemplateBlockTerritoryOverall(str);
    str.replace("!TITLE!", tr("Territory Assignment Record"));
    str.replace("!TITLE_TERRITORYCOVERAGE!", tr("Territory Coverage"));
    str.replace("!TOTALNUMBER_TERRITORY!", tr("Total number of territories"));
    str.replace("!WORKED_LT6M!", tr("< 6 months", "territory worked"));
    str.replace("!WORKED_6TO12M!", tr("6 to 12 months", "territory worked"));
    str.replace("!WORKED_GT12M!", tr("> 12 months ago", "territory worked"));
    str.replace("!AVERAGE_PER_YEAR!", tr("Average per year", "Number of times territory has been worked per year on average"));

    sql_items territories = sql->selectSql("SELECT t.*,tc.city FROM territories t"
                                           " LEFT JOIN territory_city tc ON t.city_id = tc.id"
                                           " ORDER BY territory_number");

    str.replace("!TR_TOTALNUMBER_TERRITORY!", QString::number(territories.size()));

    QString basepage = "";
    QString pagesec = "";
    QString tr_sec = "";
    QString adr_sec = "";
    QPair<QString, QString> sec = templateGetSection(str, "!TERRITORY_START!", "!TERRITORY_END!", "territory_class");
    basepage = sec.first;
    sec = templateGetSection(sec.second, "!REPEAT_START!", "!REPEAT_END!", "repeat_class");
    tr_sec = sec.second;
    sec = templateGetSection(sec.first, "!ADDRESS_START!", "!ADDRESS_END!", "address_class");
    pagesec = sec.first;
    adr_sec = sec.second;

    cpersons cp;
    QString pages = "";

    for (unsigned int i = 0; i < territories.size(); i++) {
        QString filledpage(pagesec);

        filledpage.replace("!TR_TERRITORY_NUMBER!", territories[i].value("territory_number").toString());
        filledpage.replace("!TR_LOCALITY!", territories[i].value("locality").toString());
        filledpage.replace("!TR_TERRITORY_TYPE!", territories[i].value("type_name").toString());
        filledpage.replace("!TR_CITY!", territories[i].value("city").toString());
        filledpage.replace("!TR_DATE_LAST_WORKED!", territories[i].value("lastworked_date").toString());
        filledpage.replace("!TR_TERRITORY_REMARK!", territories[i].value("remark").toString());
        filledpage.replace("!TR_TERRITORY_GEOMETRY!", territories[i].value("wkt_geometry").toString());
        person *personAssignedTo = cp.getPerson(territories[i].value("person_id").toInt());
        QString assignedTo("");
        if (personAssignedTo)
            assignedTo = personAssignedTo->fullname();
        filledpage.replace("!TR_ASSIGNED_TO!", assignedTo);

        sql_items assignments = sql->selectSql("SELECT * FROM territoryassignments WHERE territory_id = " + territories[i].value("id").toString() + " ORDER BY checkedout_date");
        const unsigned int evaluatedYears = 5;
        double calcTable[evaluatedYears][6];
        double xSum = 0;
        double ySum = 0;
        for (unsigned int j = 0; j < evaluatedYears; j++) {
            calcTable[j][0] = j;
            calcTable[j][1] = 0;
            calcTable[j][2] = 0;
            calcTable[j][3] = 0;
            calcTable[j][4] = 0;
            calcTable[j][5] = 0;
        }

        QString filledsection = "";
        for (unsigned int j = 0; j < assignments.size(); j++) {
            QString currentSection(tr_sec);
            currentSection = fillCommonItems(currentSection, false, ccongregation::None);

            person *publisher = cp.getPerson(assignments[j].value("person_id").toInt());
            currentSection.replace("!TR_PUBLISHER!", publisher->fullname());

            QDate checkedOut_date = assignments[j].value("checkedout_date").toDate();
            currentSection = replaceDateTag(currentSection, "TR_DATE_CHECKED_OUT", checkedOut_date);

            QDate checkedBackIn_date = assignments[j].value("checkedbackin_date").toDate();
            currentSection = replaceDateTag(currentSection, "TR_DATE_CHECKED_BACK_IN", checkedBackIn_date);

            unsigned int x = evaluatedYears + 1;
            if (checkedBackIn_date.isValid())
                x = evaluatedYears - uint(checkedBackIn_date.daysTo(QDate::currentDate()) / 365) - 1;
            if (x < evaluatedYears)
                calcTable[x][1] += 1;

            filledsection.append(currentSection);
        }
        for (unsigned int j = 0; j < evaluatedYears; j++) {
            xSum += calcTable[j][0];
            ySum += calcTable[j][1];
        }

        double deltaX = xSum / evaluatedYears;
        double deltaY = ySum / evaluatedYears;
        double bc = 0, bd = 0;
        for (unsigned int j = 0; j < evaluatedYears; j++) {
            calcTable[j][2] = calcTable[j][0] - deltaX;
            calcTable[j][3] = calcTable[j][1] - deltaY;
            calcTable[j][4] = calcTable[j][2] * calcTable[j][3];
            calcTable[j][5] = calcTable[j][2] * calcTable[j][2];
            bc += calcTable[j][4];
            bd += calcTable[j][5];
        }
        double b = bd == 0.0 ? 0.0 : bc / bd;
        double a = deltaY - b * deltaX;
        double averagePerYear = a + b * (evaluatedYears - 1);
        filledpage.replace("!TR_AVERAGE_PER_YEAR!", QString::number(averagePerYear));

        if (filledsection != "")
            filledpage.replace("<div class=\"repeat_class\"></div>", "<div class=\"repeat_class\">" + filledsection + "</div>");

        sql_items addresses = sql->selectSql("SELECT * FROM territoryaddresses WHERE territory_id = " + territories[i].value("id").toString() + " ORDER BY street, housenumber, name");
        filledsection = "";
        for (unsigned int j = 0; j < addresses.size(); j++) {
            QString currentSection(adr_sec);
            currentSection = fillCommonItems(currentSection, false, ccongregation::None);

            currentSection.replace("!TA_COUNTRY!", addresses[j].value("country").toString());
            currentSection.replace("!TA_STATE!", addresses[j].value("state").toString());
            currentSection.replace("!TA_COUNTY!", addresses[j].value("county").toString());
            currentSection.replace("!TA_CITY!", addresses[j].value("city").toString());
            currentSection.replace("!TA_DISTRICT!", addresses[j].value("district").toString());
            currentSection.replace("!TA_STREET!", addresses[j].value("street").toString());
            currentSection.replace("!TA_HOUSENUMBER!", addresses[j].value("housenumber").toString());
            currentSection.replace("!TA_POSTALCODE!", addresses[j].value("postalcode").toString());
            currentSection.replace("!TA_POINT_GEOMETRY!", addresses[j].value("wkt_geometry").toString());
            currentSection.replace("!TA_NAME!", addresses[j].value("name").toString());
            currentSection.replace("!TA_ADDRESSTYPE_NUMBER!", addresses[j].value("addresstype_number").toString());
            currentSection.replace("!TA_ADDRESSTYPE_NAME!", addresses[j].value("addresstype_name").toString());
            currentSection.replace("!TA_ADDRESSTYPE_COLOR!", addresses[j].value("color").toString());

            filledsection.append(currentSection);
        }

        if (filledsection != "")
            filledpage.replace("<div class=\"address_class\"></div>", "<div class=\"address_class\">" + filledsection + "</div>");

        pages.append(filledpage);
    }

    basepage.replace("<div class=\"territory_class\"></div>", "<div class=\"territory_class\">" + pages + "</div>");
    return ifthen.compute(basepage);
}

QString PrintTerritoryAssignmentRecord::fillTemplateBlockTerritoryOverall(QString context)
{
    context.replace("!ADDRESS!", tr("Address"));
    context.replace("!ADDRESSES!", tr("Addresses", "Addresses included in the territory"));
    context.replace("!ADDRESSTYPE!", tr("Address type"));
    context.replace("!ASSIGNED_TO!", tr("Assigned to"));
    context.replace("!CONGREGATION!", tr("Congregation"));
    context.replace("!CONGREGATION_NAME!", myCongregation.name);
    context.replace("!CONGREGATION_TITLE!", tr("%1 Congregation", "Congregation_Title Tag. %1 is Congregation Name").arg(myCongregation.name));
    context.replace("!DATE_CHECKED_OUT!", tr("Date checked out"));
    context.replace("!DATE_CHECKED_BACK_IN!", tr("Date checked back in"));
    context.replace("!DATE_LAST_WORKED!", tr("Date last worked"));
    context.replace("!DATE_TITLE!", tr("Date"));
    context.replace("!CITY!", tr("City"));
    context.replace("!COUNTRY!", tr("Country", "Short name of country"));
    context.replace("!COUNTY!", tr("County", "Name of administrative area level 2"));
    context.replace("!DISTRICT!", tr("District", "Sublocality, first-order civil entity below a locality"));
    context.replace("!FROM_NUMBER!", tr("From", "From number; in number range"));

    QSettings settings;
    QString googleAPIkey = settings.value("geo_service_provider/google_api_key", "").toString();
    context.replace("!GOOGLE_API_KEY!", googleAPIkey.isEmpty() ? "" : googleAPIkey);
    QString hereAppId = settings.value("geo_service_provider/here_app_id", "").toString();
    context.replace("!HERE_APP_ID!", hereAppId.isEmpty() ? "" : hereAppId);
    QString hereAppCode = settings.value("geo_service_provider/here_app_code", "").toString();
    context.replace("!HERE_APP_CODE!", hereAppCode.isEmpty() ? "" : hereAppCode);

    context.replace("!HOUSENUMBER!", tr("No.", "House or street number"));
    context.replace("!LOCALITY!", tr("Locality"));
    context.replace("!MAP!", tr("Map", "Map of a territory"));
    context.replace("!NAME!", tr("Name", "Name of person or building"));
    context.replace("!POSTALCODE!", tr("Postalcode", "Mail code, ZIP"));
    context.replace("!PUBLISHER!", tr("Name of publisher"));
    context.replace("!REMARK!", tr("Remark"));
    context.replace("!STATE!", tr("State", "Short name of administrative area level 1"));
    context.replace("!STREET!", tr("Street", "Streetname"));
    context.replace("!TERRITORY!", tr("Territory"));
    context.replace("!TERRITORY_NUMBER!", tr("Terr. No."));
    context.replace("!TERRITORY_TYPE!", tr("Territory type"));
    context.replace("!TO_NUMBER!", tr("To", "To number; in number range"));
    context.replace("!TYPE!", tr("Type", "Type of something"));
    context.replace("!SUM!", tr("Sum", "Total amount"));
    return context;
}
