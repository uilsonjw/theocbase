/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import net.theocbase 1.0

Item {
    id: notesDialog
    property string title: "Notes"
    width: 300
    height: 350

    property bool openComments: true
    property LMM_Meeting meeting
    property alias notes : texteditNote.text

    AssignmentController { id: myController }

    ColumnLayout {
        id: layout
        anchors.fill: parent
        anchors.margins: 10

        RowLayout {
            ToolButton {
                icon.source: "qrc:/icons/notes.svg"
                background: null
                ToolTip.text: qsTr("Notes")
                ToolTip.visible: hovered
            }
            TextArea{
                id: texteditNote
                Layout.fillWidth: true
                selectByMouse: true
                wrapMode: Text.WordWrap
                text: openComments ? meeting.openingComments : meeting.closingComments
                font.pointSize: 11

                onEditingFinished: {
                    if (openComments) {
                        if (meeting.openingComments === text)
                            return
                        meeting.openingComments = text
                    } else {
                        if (meeting.closingComments === text)
                            return
                        meeting.closingComments = text
                    }
                    meeting.save()
                }
            }
        }

        Item { Layout.fillHeight: true }

    }
}
