/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2019, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.1
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.3
import net.theocbase.mobile 1.0
import net.theocbase 1.0
import "js/moment.js" as Moment

Page {
    id: mainpage

    property date currentdate
    property int tabHeight: 45 * dpi
    property int tabMargin: 0
    property string exceptionText: weekInfo.exceptionDisplayText
    property string weekStartingText: ""
    property date mwDate
    property date wkDate

    function activateTabByDay() {
        var d = new Date()
        swipe.currentIndex = (d.getDay() === 0 || d.getDay() === 6 ? 1 : 0)
    }

    function refreshList(addDays){
        stackView.pop(null)
        if (typeof addDays === 'undefined' || !moment(currentdate).isValid()){
            currentdate = moment().startOf('isoweek').toDate()
        }else{
            currentdate = moment(currentdate).add(addDays,'days').toDate()
        }

        console.log("refresh list " + currentdate.toString())
        lmMeeting.loadSchedule(currentdate)
        publicMeeting.loadSchedule(currentdate)
        outgoingSpeakers.loadList(currentdate)
        congCtrl.clearExceptionCache()
        weekInfo.load()
        weekStartingText = qsTr("Week starting %1").arg(currentdate.toLocaleDateString(Qt.locale(), Locale.ShortFormat))
        // meeting days
        var mDay = congCtrl.getMeetingDay(currentdate, 1)
        var mDDay = congCtrl.getMeetingDay(new Date(0), 1)
        var wDay = congCtrl.getMeetingDay(currentdate, 3)
        var wDDay = congCtrl.getMeetingDay(new Date(0), 3)
        lmMeeting.editpossible = mDay > 0
        publicMeeting.editpossible = wDay > 0

        var mDate = new Date(currentdate)
        mDate.setDate(mDate.getDate() + (mDay === 0 ? mDDay : mDay) -1)
        mwDate = mDate
        var wDate = new Date(currentdate)
        wDate.setDate(wDate.getDate() + (wDay === 0 ? wDDay : wDay) -1)
        wkDate = wDate
    }

    Component.onCompleted: {        
        refreshList(0)
    }

    WeekInfo {
        id: weekInfo
        date: currentdate
    }

    CongregationCtrl { id: congCtrl }

    header: ToolBar {
        id: toolbarbase        
        height: 45 * dpi + toolbarTopMargin + exceptionRect.height        
        RowLayout {
            id: topRow
            height: 45 * dpi
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.topMargin: toolbarTopMargin
            anchors.leftMargin:  10*dpi + (Qt.platform.os === "ios" ? mainpage.leftPadding : 0)
            anchors.rightMargin: 10*dpi + (Qt.platform.os === "ios" ? mainpage.rightPadding : 0)

            ToolButton {
                Layout.alignment: Qt.AlignVCenter
                icon.source: "qrc:/chevron_left.svg"
                icon.color: "white"
                onClicked: { refreshList(-7) }
            }
            ToolButton {
                Layout.alignment: Qt.AlignVCenter
                icon.source: "qrc:/chevron_right.svg"
                icon.color: "white"
                onClicked: { refreshList(7) }
            }
            GridLayout {
                id: weekStartingColRect
                Layout.margins: 2*dpi
                Layout.fillHeight: true
                columns: 2
                Text {
                    text: weekStartingText
                    color: "white"
                    Layout.fillWidth: true
                    Layout.maximumWidth: 200*dpi
                    font.pointSize: app_info.fontsizeSmall
                    fontSizeMode: Text.Fit
                }
                Image {
                    source: "qrc:/expanded_white.svg"
                    width: weekStartingColRect.height / 2
                    sourceSize.height: 30*dpi
                    sourceSize.width: 30*dpi
                    height: width                    
                    fillMode: Image.PreserveAspectFit
                    Layout.rowSpan: 2
                }
                Text {                    
                    text: lmMeeting.bibleReadering
                    color: "#b6b6b6"
                    Layout.fillWidth: true
                    Layout.maximumWidth: 200*dpi
                    font.pointSize: app_info.fontsizeSmall
                    fontSizeMode: Text.Fit
                    elide: Text.ElideMiddle
                }
                MouseArea {
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    width: parent.width
                    height: parent.height
                    onClicked: {
                        if (!calendar.visible) {
                            calendar.selectedDate = currentdate
                            calendar.open()
                        }
                    }
                }
            }

            Item {
                Layout.fillWidth: true
                Layout.minimumWidth: 10
            }
            ToolButton {
                icon.source: ccloud.syncState === Cloud.Synchronized ?
                                 "qrc:/cloud_done.svg" : ccloud.syncState === Cloud.Download ?
                                     "qrc:/cloud_download.svg" : "qrc:/cloud_upload.svg"
                icon.color: "white"                
                onClicked: syncpage.runSync()
            }
            ToolButton {
                icon.source: "qrc:/more.svg"
                icon.color: "white"
                onClicked: moremenu.open()
                Menu {
                    id: moremenu
                    y: parent.height
                    MenuItem {
                        text: "Exceptions"
                        icon.source: "qrc:/exceptions.svg"                        
                        enabled: accessControl.user && (accessControl.user.hasPermission(PermissionRule.CanViewSpecialEvents))
                        onClicked: {
                            if (exceptionPopup.active) {
                                exceptionPopup.item.weekDate = currentdate
                                exceptionPopup.item.weekInfo = weekInfo
                                exceptionPopup.item.open()
                            } else
                                exceptionPopup.active = true
                        }
                        Loader {
                            id: exceptionPopup
                            active: false
                            source: "Exceptions.qml"
                            focus: true
                            onLoaded: {
                                item.weekDate = currentdate
                                item.weekInfo = weekInfo
                                item.open()
                            }
                        }
                    }
                    MenuItem {
                        text: "Print"
                        icon.source: "qrc:/print.svg"                        
                        onClicked: {
                            printLoader.source = ""
                            printLoader.source = "PrintOptions.qml"
                        }
                        Loader {
                            id: printLoader
                            onLoaded: {
                                item.weekDate = currentdate
                                item.open()
                            }
                        }
                    }
                }
            }
        }
        Rectangle {
            id: exceptionRect
            anchors.top: topRow.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            visible: exceptionText != ""
            height: exceptionText == "" ? 0 : 30*dpi
            color: "red"
            Text {
                anchors.fill: parent
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                text: exceptionText
                color: "white"
                visible: text != ""
            }
        }
        CalendarPopup {
            id: calendar
            transformOrigin: Popup.Top
            x: (Qt.platform.os == "ios" ? mainpage.leftPadding : 0)
            y: toolbarbase.height
            onSelectedDateChanged: {
                currentdate = moment(selectedDate).startOf('isoWeek').toDate()
                refreshList(0)
            }
        }
    }


    Rectangle {
        id: blueRect
        anchors.fill: parent
        implicitHeight: 600
        implicitWidth: 400

        color: currentdate.getFullYear() >= 2016 ? "white" : "#EFEFEF"

        SwipeView {
            id: swipe
            anchors.fill: parent
            spacing: 10


            Flickable {
                id: fl1
                contentHeight: lmMeeting.height
                onDragEnded: if (header1.refresh) syncpage.runSync()
                ListHeader {
                    id: header1
                    flickable: fl1
                    y: -height

                }

                LMMSchedule_Mobile {
                    id: lmMeeting
                    width: swipe.width
                    visible: currentdate.getFullYear() >= 2016 && canViewMidweekMeetingSchedule
                }
            }
            Flickable {
                id: fl2
                contentHeight: weLayout.height
                onDragEnded: if (header2.refresh) syncpage.runSync()
                ListHeader {
                    id: header2
                    flickable: fl2
                    y: -height
                }

                ColumnLayout {
                    id: weLayout
                    spacing: 10 * dpi
                    width: swipe.width
                    PublicMeetingSchedule_Mobile {
                        id: publicMeeting
                        Layout.fillWidth: true
                        visible: canViewWeekendMeetingSchedule
                    }

                    OutgoingSpeakers {
                        id: outgoingSpeakers
                        Layout.fillWidth: true
                        visible: canViewWeekendMeetingSchedule
                    }
                    Item { Layout.fillHeight: true }
                }
            }
        }
    }

    PageIndicator {
        count: swipe.count
        currentIndex: swipe.currentIndex
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        Material.theme: Material.Light
    }
}
