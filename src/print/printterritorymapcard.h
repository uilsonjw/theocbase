/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PRINTTERRITORYMAPCARD_H
#define PRINTTERRITORYMAPCARD_H

#include "printdocument.h"

class PrintTerritoryMapCard : public PrintDocument
{
    Q_DECLARE_TR_FUNCTIONS(PrintTerritoryMapCard)
public:
    PrintTerritoryMapCard();
    QVariant fillTemplate() override;

    void setTerritoryNumberList(QString numbers);
    void setForceScan(bool rescan);
    void setPrinter(QPagedPaintDevice *d);

private:
    QString encodePointValue(double value);

    bool _forceRescan = false;
    int mapSizeFactory = 1;
    QString territoryNumberList = "1";
    QPagedPaintDevice *_printer;
};

#endif // PRINTTERRITORYMAPCARD_H
