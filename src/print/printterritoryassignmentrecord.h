/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PRINTTERRITORYASSIGNMENTRECORD_H
#define PRINTTERRITORYASSIGNMENTRECORD_H

#include "printdocument.h"

class PrintTerritoryAssignmentRecord : public PrintDocument
{
    Q_DECLARE_TR_FUNCTIONS(PrintTerritoryAssignmentRecord)
public:
    PrintTerritoryAssignmentRecord();
    QVariant fillTemplate() override;

protected:
     QString fillTemplateBlockTerritoryOverall(QString context);
};

#endif // PRINTTERRITORYASSIGNMENTRECORD_H
