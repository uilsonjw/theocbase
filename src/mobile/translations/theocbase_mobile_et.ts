<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="et">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation>Nimi</translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation>Kuupäev</translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Ära määra järgmist õppetundi</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Jää praeguse õppetunni juurde</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Vigased andmed</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Lisa aeg?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Pealkiri</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Allikmaterjal</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Õpilane</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Abiline</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Tulemused</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Tehtud</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Asenduskõneleja</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Vali asenduskõneleja</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Praegune õppetund</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Harjutused tehtud</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Järgmine õppetund</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Vali järgmine õppetund</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Ajajaotus</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Märkused</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Kooli andmed</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Alusta ajamõõtmist</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Lõpeta ajamõõtmine</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Tinglik olukord</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Vali tinglik olukord</translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>No exception</source>
        <translation>Erandit ei ole</translation>
    </message>
    <message>
        <source>Circuit overseer&#x27;s visit</source>
        <translation>Ringkonnaülevaataja külastus</translation>
    </message>
    <message>
        <source>Circuit assembly</source>
        <translation>Ringkondlik kokkutulek</translation>
    </message>
    <message>
        <source>Regional convention</source>
        <translation>Üldkokkutulek</translation>
    </message>
    <message>
        <source>Memorial</source>
        <translation>Mälestusõhtu</translation>
    </message>
    <message>
        <source>Other exception</source>
        <translation>Muu erand</translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation>%1 algav nädal</translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation>Koosoleku päevad</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Nädalasisene koosolek</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Nädalavahetuse koosolek</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Teema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Allikas</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Juhataja</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Kõneleja</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lugeja</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Märkmed</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Lisainfo</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <comment>Page title</comment>
        <translation>Märkmed</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>AARDEID JUMALA SÕNAST</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>VALMISTUME KUULUTUSTÖÖKS</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>KRISTLIK ELUVIIS</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Juhataja</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Nõuandja</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Laul %1 ja palve</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Laul</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Juhataja</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lugeja</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Sissejuhatus</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Palve</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation>Impordi kava...</translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation>Lõppsõna</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Nädalasisene koosolek</translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Jää praeguse õppetunni juurde</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Alusta ajamõõtmist</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Lõpeta ajamõõtmine</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Lisa ajastus?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Teema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Allikas</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Õpilane</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Tulemus</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Tehtud</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Ajajaotus</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Praegune õppetund</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Harjutused tehtud</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Järgmine õppetund</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Vali järgmine õppetund</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Märkmed</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Abiline</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Asenduskõneleja</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>Taimer</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>Abiline ei tohiks olla vastassoost isik.</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>Õppetükk</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Detailid</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>Värskenda tõmmates...</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Värskenda vabastades...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation>Kasutajanimi</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Salasõna</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Logi sisse</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Kasutajatunnus või e-postiaadress</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-post</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Loo konto</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>Uuenda parooli</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>E-posti aadressi ei leitud!</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Unustasin parooli</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>Sisselogimise lehekülg</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation>%1 algav nädal</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation>Märkmed</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation>Väljasõitvad kõnepidajad</translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>%1 kõneleja ära sellel nädalavahetusel</numerusform>
            <numerusform>%1 kõnelejat ära sellel nädalavahetusel</numerusform>
        </translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation>Ühtegi kõnelejat ei ole sellel nädalavahetusel kõnel</translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation>Printimise sätted</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Kava</translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation>Töölehed</translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation>Väljasõitvad kõnepidajad - kava</translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation>Kõnel olevate vendade ülesanded</translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation>Kõneleja võõrustamise ajakava</translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation>Kõnepidajate kõned</translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Trüki</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Nädalasisene koosolek</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Nädalalõpu koosolek</translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation>Kombineeritud</translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation>Ülesandeleht</translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation>Ülesandeleht abilisele</translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation>Prindi ainult määratud ülesanded</translation>
    </message>
    <message>
        <source>Text size</source>
        <translation>Teksti suurus</translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Laul ja palve</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Laul %1 ja Palve</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>AVALIK KÕNE</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>VAHITORNI UURIMINE</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Laul %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Juhataja</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lugeja</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation>Impordi VT...</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Nädalavahetuse koosolek</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Nädalasisene koosolek</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>Teema</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>Kogudus</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Kõneleja</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Mobiil</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Võõrustaja</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>Avalik kõne</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>Eesnimi</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Perekonnanimi</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Vend</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Õde</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Ametisse määratud</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Perekond</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>Pereliige seotud</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Kontaktandmed</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-post</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>Kõik klassid</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Ainult peasaal</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Ainult abisaal</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Perepea</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Abiline</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>Aktiivne</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Juhataja</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>AARDEID JUMALA SÕNAST</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Piiblilugemine</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Esmavestlus</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Korduskülastus</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Piibliuurimine</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>Kristlik eluviis programmiosad</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Koguduse piibliuurimine</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Kog. piibliuurimise lugeja</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>Uus kuulutaja</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Palve</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation>Avaliku kõnepidaja võõrustaja</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Mobiil</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Nädalasisene koosolek</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>Kõne</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Nädalavahetuse koosolek</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>Vahitorni uurimise juhataja</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation>Vahitorni uurimise lugeja</translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation>Vaimsed vääriskivid</translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation>Arutelu videoga</translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Kuulutajad</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation>Valikud</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Seaded</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Logi välja</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Teave</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Versioon</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>TheocBase kodulehekülg</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Tagasiside</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>Viimati sünkroniseeritud: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Kava</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Näita aega</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Näita kestust</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Kasutajaliides</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Keel</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Logi sisse</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-post</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nimi</translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation>Näita laulude pealkirju</translation>
    </message>
    <message>
        <source>Printing</source>
        <translation>Printimine</translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Sünkroniseeritakse...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>Juhataja</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Laul</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation>Nädalavahetusel toimuva koosoleku juhataja</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>Laul</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation>Vahitorni uurimise laul</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Palve</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation>Vahitorni number</translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>Artikkel</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Teema</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>Juhataja</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>Lugeja</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>Ajajoon</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>Nädalate arv enne valitud kuupäeva</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>Nädalate arv pärast valitud kuupäeva</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>Nädalat</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation>Nädalate arv, mille taust pärast ülesande tegemist muudetakse halliks</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>Samu muudatusi on nii kohalikult kui ka pilves (%1 riviä). Soovid jätta alles kohalikud?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>Pilves olevad andmed on kustutatud. Seadmes olevad andmed asendatakse. Kas jätkad?</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Ära määra järgmist õppetundi</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>Määramata</translation>
    </message>
</context></TS>