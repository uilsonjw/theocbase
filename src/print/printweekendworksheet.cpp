/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "printweekendworksheet.h"

PrintWeekendWorksheet::PrintWeekendWorksheet()
{
    currentTemplateData = QSharedPointer<TemplateData>(
            new TemplateData(TemplateData::PublicMeetingWorksheet, "publicmeetingWorksheet", "WE-Worksheet.htm", "we-worksheet"));
}

QVariant PrintWeekendWorksheet::fillTemplate()
{
    fillWeekList(publicmeetingday - 1);
    qDebug() << weekList[0]->week.toString(Qt::ISODate);

    int id = sql->getLanguageDefaultId();
    sql_items esitelmat = sql->selectSql("SELECT * FROM publictalks WHERE lang_id = " + QVariant(id).toString() + " ORDER BY theme_number");
    QString html;
    html.append(
            "<html>"
            "<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">"
            "<title>Weekend Meeting worksheet</title>"
            "<style>"
            "table { page-break-inside:auto }"
            "tr    { page-break-inside:avoid; page-break-after:auto }"
            "thead { display:table-header-group }"
            "tfoot { display:table-footer-group }"
            "</style>"
            "</head>"
            "<body>");
    html.append("<p align=center>" + sql->getSetting("print_we_title", tr("Weekend Meeting")) + "</p>");
    html.append("<table border=1 cellpadding=2 cellspacing=0 width=100%>");
    for (unsigned int i = 0; i < esitelmat.size(); i++) {

        html.append("<tr><td>");
        html.append(esitelmat[i].value("theme_number").toString());
        html.append("  ");
        html.append(esitelmat[i].value("theme_name").toString());
        QString aihe_id = esitelmat[i].value("id").toString();
        sql_items pvm = sql->selectSql("SELECT date, theme_id FROM publicmeeting WHERE theme_id = " + aihe_id + " ORDER BY date");
        html.append("</td><td>");
        for (unsigned int j = 0; j < pvm.size(); j++) {
            QDate date = pvm[j].value("date").toDate();
            html.append(date.addDays(publicmeetingday - 1).toString(Qt::DefaultLocaleShortDate));
            html.append(" ");
            html.append("</tr>");
        }
    }
    html.append("</body></html>");
    return html;
}
