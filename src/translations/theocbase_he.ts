<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="he">
<context>
    <name>AssignmentPanel</name>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="51"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="82"/>
        <source>Source</source>
        <translation>מקור</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="109"/>
        <source>Chairman</source>
        <translation>יושב ראש</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="109"/>
        <source>Speaker</source>
        <translation>נואם</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="120"/>
        <source>Selected</source>
        <comment>Dropdown column title</comment>
        <translation>נבחר</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="121"/>
        <source>All</source>
        <comment>Dropdown column title</comment>
        <translation>את כל</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="146"/>
        <source>Note</source>
        <translation>הערה</translation>
    </message>
</context>
<context>
    <name>CBSPanel</name>
    <message>
        <location filename="../qml/CBSPanel.qml" line="43"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="64"/>
        <source>Source</source>
        <translation>מקור</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="84"/>
        <source>Conductor</source>
        <translation>מדריך</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="90"/>
        <source>CBS conductor</source>
        <comment>Dropdown column title</comment>
        <translation>מדריך CBS</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="91"/>
        <source>Any CL assignment</source>
        <comment>Dropdown column title</comment>
        <translation>כל משימות CL</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="108"/>
        <source>Reader</source>
        <translation>קרין</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="131"/>
        <source>Note</source>
        <translation>הערה</translation>
    </message>
</context>
<context>
    <name>ComboBoxTable</name>
    <message>
        <location filename="../qml/ComboBoxTable.qml" line="194"/>
        <source>Name</source>
        <translation>שם</translation>
    </message>
    <message>
        <location filename="../qml/ComboBoxTable.qml" line="200"/>
        <source>Date</source>
        <translation>תאריך</translation>
    </message>
</context>
<context>
    <name>CongregationMap</name>
    <message>
        <location filename="../qml/CongregationMap.qml" line="68"/>
        <source>Display congregation address</source>
        <comment>Display marker at the location of the congregation on the map</comment>
        <translation>הצג כתובת קהילה</translation>
    </message>
</context>
<context>
    <name>DropboxSettings</name>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="79"/>
        <source>Are you sure you want to permanently delete your cloud data?</source>
        <translation>האם אתה בטוח שברצונך למחוק לצמיתות את נתוני הענן שלך?</translation>
    </message>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="217"/>
        <source>Last synchronized: %1</source>
        <translation>סונכרן לאחרונה: %1</translation>
    </message>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="221"/>
        <source>Synchronize</source>
        <translation>לסנכרן</translation>
    </message>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="233"/>
        <source>Delete Cloud Data</source>
        <translation>מחק נתוני ענן</translation>
    </message>
</context>
<context>
    <name>HelpViewer</name>
    <message>
        <location filename="../helpviewer.cpp" line="81"/>
        <source>TheocBase Help</source>
        <translation>העזרה של TheocBase</translation>
    </message>
    <message>
        <location filename="../helpviewer.cpp" line="82"/>
        <source>Unable to launch the help viewer (%1)</source>
        <translation>לא ניתן להפעיל את מציג העזרה (%1)</translation>
    </message>
</context>
<context>
    <name>LMMNotesPanel</name>
    <message>
        <location filename="../qml/LMMNotesPanel.qml" line="45"/>
        <source>Notes</source>
        <translation>הערות</translation>
    </message>
</context>
<context>
    <name>LMM_Assignment</name>
    <message>
        <location filename="../lmm_assignment.cpp" line="180"/>
        <source>Please find below details of your assignment:</source>
        <translation>אנא ראה למטה את פרטי המשימה שלך:</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="181"/>
        <source>Date</source>
        <translation>תאריך</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="182"/>
        <source>Name</source>
        <translation>שם</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="183"/>
        <source>Assignment</source>
        <translation>משימה</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="184"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="185"/>
        <source>Source material</source>
        <translation>חומר מקור</translation>
    </message>
</context>
<context>
    <name>LMM_AssignmentContoller</name>
    <message>
        <location filename="../lmm_assignmentcontoller.cpp" line="65"/>
        <source>Do not assign the next study</source>
        <translation>אל תקצה את השיעור הבא</translation>
    </message>
</context>
<context>
    <name>LMM_Assignment_ex</name>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="179"/>
        <source>Please find below details of your assignment:</source>
        <translation>אנא ראה למטה את פרטי המשימה שלך:</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="180"/>
        <source>Date</source>
        <translation>תאריך</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="181"/>
        <source>Name</source>
        <translation>שם</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="184"/>
        <source>Reader</source>
        <translation>קרין</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="186"/>
        <source>Assistant</source>
        <translation>עוזר</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="188"/>
        <source>Assignment</source>
        <translation>משימה</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="191"/>
        <source>Study</source>
        <translation>שיעור</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="193"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="194"/>
        <source>Source material</source>
        <translation>חומר מקור</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="199"/>
        <source>Main hall</source>
        <translation>אולם ראשי</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="202"/>
        <source>Auxiliary classroom 1</source>
        <translation>כיתה משנה  א</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="205"/>
        <source>Auxiliary classroom 2</source>
        <translation>כיתה משנה  ב</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="208"/>
        <source>To be given in</source>
        <comment>Refer to main hall or aux. classroom. See S-89</comment>
        <translation>מקום הגשת המשימה</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="253"/>
        <source>See your be book</source>
        <comment>Counsel point is not known yet. See your &apos;Ministry School&apos; book.</comment>
        <translation>ראה את הספר be שלך</translation>
    </message>
</context>
<context>
    <name>LMM_Meeting</name>
    <message>
        <location filename="../lmm_meeting.cpp" line="621"/>
        <source>Enter source material here</source>
        <translation>הזן כאן חומר מקור</translation>
    </message>
</context>
<context>
    <name>LMM_Schedule</name>
    <message>
        <location filename="../lmm_schedule.cpp" line="138"/>
        <location filename="../lmm_schedule.cpp" line="183"/>
        <source>Chairman</source>
        <comment>talk title</comment>
        <translation>יושב ראש</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="140"/>
        <location filename="../lmm_schedule.cpp" line="184"/>
        <source>Treasures From God’s Word</source>
        <comment>talk title</comment>
        <translation>אוצרות מדבר־אלוהים</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="185"/>
        <source>Digging for Spiritual Gems</source>
        <comment>talk title</comment>
        <translation>חיפוש פנינים רוחניות</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="144"/>
        <location filename="../lmm_schedule.cpp" line="187"/>
        <source>Bible Reading</source>
        <comment>talk title</comment>
        <translation>קריאת המקרא</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="150"/>
        <location filename="../lmm_schedule.cpp" line="190"/>
        <source>Initial Call</source>
        <comment>talk title</comment>
        <translation>שיחה ראשונה</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="146"/>
        <location filename="../lmm_schedule.cpp" line="188"/>
        <source>Sample Conversation Video</source>
        <comment>talk title</comment>
        <translation>סרטוני השיחות לדוגמה</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="142"/>
        <location filename="../lmm_schedule.cpp" line="186"/>
        <source>Spiritual Gems</source>
        <comment>talk title</comment>
        <translation>פנינים רוחניות</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="152"/>
        <location filename="../lmm_schedule.cpp" line="191"/>
        <source>First Return Visit</source>
        <comment>talk title</comment>
        <translation>ביקור חוזר ראשון</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="154"/>
        <location filename="../lmm_schedule.cpp" line="192"/>
        <source>Second Return Visit</source>
        <comment>talk title</comment>
        <translation>ביקור חוזר שני</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="156"/>
        <location filename="../lmm_schedule.cpp" line="193"/>
        <source>Third Return Visit</source>
        <comment>talk title</comment>
        <translation>ביקור חוזר שלישי</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="158"/>
        <location filename="../lmm_schedule.cpp" line="194"/>
        <source>Bible Study</source>
        <comment>talk title</comment>
        <translation>שיעור מקרא</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="160"/>
        <location filename="../lmm_schedule.cpp" line="195"/>
        <source>Talk</source>
        <comment>talk title</comment>
        <translation>נאום</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="162"/>
        <location filename="../lmm_schedule.cpp" line="196"/>
        <source>Living as Christians Talk 1</source>
        <comment>talk title</comment>
        <translation>לחיות כמשיחיים נאום 1</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="164"/>
        <location filename="../lmm_schedule.cpp" line="197"/>
        <source>Living as Christians Talk 2</source>
        <comment>talk title</comment>
        <translation>לחיות כמשיחיים נאום 2</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="166"/>
        <location filename="../lmm_schedule.cpp" line="198"/>
        <source>Living as Christians Talk 3</source>
        <comment>talk title</comment>
        <translation>לחיות כמשיחיים נאום 3</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="168"/>
        <location filename="../lmm_schedule.cpp" line="199"/>
        <source>Congregation Bible Study</source>
        <comment>talk title</comment>
        <translation>שיעור המקרא הקהילתי</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="170"/>
        <location filename="../lmm_schedule.cpp" line="200"/>
        <source>Circuit Overseer&#x27;s Talk</source>
        <comment>talk title</comment>
        <translation>נאום משגיח הנפה</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="172"/>
        <location filename="../lmm_schedule.cpp" line="201"/>
        <source>Memorial Invitation</source>
        <comment>talk title</comment>
        <translation>הזמנה לערב הזיכרון</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="174"/>
        <location filename="../lmm_schedule.cpp" line="202"/>
        <source>Other Video Part</source>
        <comment>talk title</comment>
        <translation>חלק וידאו אחר</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="189"/>
        <source>Apply Yourself to Reading and Teaching</source>
        <comment>talk title</comment>
        <translation>שקוד על קריאה והוראה</translation>
    </message>
</context>
<context>
    <name>Login</name>
    <message>
        <location filename="../qml/Login.qml" line="61"/>
        <source>Username or Email</source>
        <translation>שם משתמש או דוא&quot;ל</translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="69"/>
        <source>Password</source>
        <translation>סיסמה</translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="78"/>
        <source>Login</source>
        <translation>התחברות</translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="97"/>
        <source>Forgot Password</source>
        <translation>שכחת ססמא</translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="111"/>
        <source>Create Account</source>
        <translation>צור חשבון</translation>
    </message>
</context>
<context>
    <name>MAC_APPLICATION_MENU</name>
    <message>
        <location filename="../main.cpp" line="62"/>
        <source>Services</source>
        <translation>שירותים</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="63"/>
        <source>Hide %1</source>
        <translation>הסתר את%1</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="64"/>
        <source>Hide Others</source>
        <translation>הסתר אחרים</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="65"/>
        <source>Show All</source>
        <translation>הצג הכול</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="66"/>
        <source>Preferences...</source>
        <translation>העדפות ...</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="67"/>
        <source>Quit %1</source>
        <translation>צא מ-%1</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="68"/>
        <source>About %1</source>
        <translation>בערך:%1</translation>
    </message>
</context>
<context>
    <name>MWMeetingChairmanPanel</name>
    <message>
        <location filename="../qml/MWMeetingChairmanPanel.qml" line="81"/>
        <source>Chairman</source>
        <translation>יושב ראש</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingChairmanPanel.qml" line="102"/>
        <source>Auxiliary Classroom Counselor II</source>
        <translation>יועץ כיתה משנית ב&apos;</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingChairmanPanel.qml" line="125"/>
        <source>Auxiliary Classroom Counselor III</source>
        <translation>יועץ כיתת לימוד שלישי</translation>
    </message>
</context>
<context>
    <name>MWMeetingModule</name>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="223"/>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>אוצרות מדבר־אלוהים</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="233"/>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>התייעל בשירות השדה</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="238"/>
        <source>LIVING AS CHRISTIANS</source>
        <translation>לחיות כמשיחיים</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="253"/>
        <location filename="../qml/MWMeetingModule.qml" line="274"/>
        <source>Midweek Meeting</source>
        <translation>אסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="270"/>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation>הערות</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="284"/>
        <source>Import Schedule...</source>
        <translation>לוח הזמנים לייבא ...</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="332"/>
        <location filename="../qml/MWMeetingModule.qml" line="531"/>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation>MH</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="333"/>
        <location filename="../qml/MWMeetingModule.qml" line="532"/>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation>A1</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="334"/>
        <location filename="../qml/MWMeetingModule.qml" line="533"/>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation>A2</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="343"/>
        <source>Chairman</source>
        <translation>יושב ראש</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="343"/>
        <source>Counselor</source>
        <translation>יועצת</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="369"/>
        <location filename="../qml/MWMeetingModule.qml" line="420"/>
        <source>Song %1 and Prayer</source>
        <translation>שיר %1 ותפילה</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="383"/>
        <source>Opening Comments</source>
        <translation>דברי פתיחה</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="403"/>
        <source>Concluding Comments</source>
        <translation>דברי סיכום</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="496"/>
        <source>Song %1</source>
        <translation>שיר %1</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="517"/>
        <source>Conductor</source>
        <translation>מדריך</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="519"/>
        <source>Reader</source>
        <translation>קרין</translation>
    </message>
</context>
<context>
    <name>MWMeetingPrayerPanel</name>
    <message>
        <location filename="../qml/MWMeetingPrayerPanel.qml" line="47"/>
        <source>Prayer</source>
        <translation>תפלה</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="437"/>
        <source>No meeting</source>
        <translation>אין אסיפה</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="617"/>
        <source>Conductor:</source>
        <translation>מדריך</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="622"/>
        <source>Reader:</source>
        <translation>קרין:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="482"/>
        <source>Copyright</source>
        <translation>זכויות יוצרים</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="490"/>
        <source>Qt libraries licensed under the GPL.</source>
        <translation>ספריות Qt המורשות תחת ה- GPL.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="482"/>
        <source>TheocBase Team</source>
        <translation>צוות TheocBase</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="286"/>
        <source>Last synchronized</source>
        <translation>סונכרן לאחרונה</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="485"/>
        <source>Licensed under GPLv3.</source>
        <translation>מורשה תחת GPLv3.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="491"/>
        <source>Versions of Qt libraries </source>
        <translation>גרסאות ספריות Qt </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1266"/>
        <source>TheocBase data exchange</source>
        <translation>חילופי נתונים של TheocBase</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="554"/>
        <source>New update available. Do you want to install?</source>
        <translation>עדכון חדש זמין. האם אתה רוצה להתקין?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="558"/>
        <source>No new update available</source>
        <translation>אין עדכון חדש זמין</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="584"/>
        <source>Save file</source>
        <translation>שמור את הקובץ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1636"/>
        <source>Select ePub file</source>
        <translation>בחר קובץ ePub</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1555"/>
        <source>Send e-mail reminders?</source>
        <translation>לשלוח תזכורות בדוא&apos;ל?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1589"/>
        <source>Updates available...</source>
        <translation>עדכונים זמינים...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1169"/>
        <source>Error sending e-mail</source>
        <translation>שגיאה בשליחת דוא&quot;ל</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="408"/>
        <source>WEEK STARTING %1</source>
        <translation>שבוע החל %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="573"/>
        <source>Exporting outgoing speakers not ready yet, sorry.</source>
        <translation>ייצוא נואמים יוצאים עדיין לא מוכן, מצטער.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="577"/>
        <source>Exporting study history to iCal is not supported</source>
        <translation>אין תמיכה בייצוא היסטוריית השיעורים ל- iCal</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="588"/>
        <source>Save folder</source>
        <translation>שמור תיקיה</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1171"/>
        <source>E-mail sent successfully</source>
        <translation>דוא&quot;ל נשלח בהצלחה</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="601"/>
        <source>Saved successfully</source>
        <translation>נשמר בהצלחה</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="661"/>
        <location filename="../mainwindow.cpp" line="666"/>
        <source>Counselor-Class II</source>
        <translation>יועץ-כתה ב&apos;</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="662"/>
        <location filename="../mainwindow.cpp" line="667"/>
        <source>Counselor-Class III</source>
        <translation>יועץ-כתה ד&apos;</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="682"/>
        <source>Assistant to %1</source>
        <comment>%1 is student&apos;s name</comment>
        <translation>עוזר/ת ל-%1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="749"/>
        <location filename="../mainwindow.cpp" line="964"/>
        <location filename="../mainwindow.cpp" line="989"/>
        <source>Kingdom Hall</source>
        <translation>אולם הממלכה</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="660"/>
        <location filename="../mainwindow.cpp" line="665"/>
        <location filename="../mainwindow.cpp" line="755"/>
        <location filename="../mainwindow.cpp" line="766"/>
        <source>Chairman</source>
        <translation>יושב ראש</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="689"/>
        <source>Reader for Congregation Bible Study</source>
        <translation>קורין לשיעור המקרא של הקהילה</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="674"/>
        <location filename="../mainwindow.cpp" line="690"/>
        <source>Source</source>
        <comment>short for Source material</comment>
        <translation>מקור</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="663"/>
        <location filename="../mainwindow.cpp" line="668"/>
        <location filename="../mainwindow.cpp" line="713"/>
        <location filename="../mainwindow.cpp" line="715"/>
        <source>Prayer</source>
        <translation>תפלה</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="674"/>
        <source>Timing</source>
        <translation>תזמון</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="755"/>
        <location filename="../mainwindow.cpp" line="920"/>
        <source>Public Talk</source>
        <translation>נאום פומבי</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="755"/>
        <source>Watchtower Study</source>
        <translation>שיעור מצפה</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="772"/>
        <source>Speaker</source>
        <translation>נאום</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="777"/>
        <source>Watchtower Study Conductor</source>
        <translation>מדריך שיעור המצפה</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="779"/>
        <source>Watchtower reader</source>
        <translation>קרין המצפה</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1025"/>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want to keep the local changes?</source>
        <translation>את אותם שינויים ניתן למצוא הן באופן מקומי והן בענן (%1 שורות). האם אתה רוצה לשמור על השינויים המקומיים?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1066"/>
        <source>The cloud data has now been deleted.</source>
        <translation>נתוני הענן נמחקו כעת.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1070"/>
        <source>Synchronize</source>
        <translation>לסנכרן</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1072"/>
        <source>Sign Out</source>
        <translation>התנתק</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1082"/>
        <source>The cloud data has been deleted. Your local data will be replaced. Continue?</source>
        <translation>נתוני הענן נמחקו. הנתונים המקומיים שלך יוחלפו. לְהמשיך?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1180"/>
        <source>Open file</source>
        <translation>לפתוח קובץ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1207"/>
        <source>Open directory</source>
        <translation>פתח את הספרייה</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1238"/>
        <source>Import Error</source>
        <translation>שגיאת ייבוא</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1238"/>
        <source>Could not import from Ta1ks. Files are missing:</source>
        <translation>לא ניתן היה לייבא מנאומים. חסרים קבצים:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1253"/>
        <source>Save unsaved data?</source>
        <translation>לשמור נתונים שלא נשמרו?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1262"/>
        <source>Import file?</source>
        <translation>לייבא קובץ?</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="691"/>
        <source>School assignment</source>
        <translation>משימה בית הספר</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="293"/>
        <source>Congregation Bible Study:</source>
        <translation>שיעור המקרא הקהילתי:‏</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="327"/>
        <source>Theocratic Ministry School:</source>
        <translation>בית־הספר לשירות התיאוקרטי:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="358"/>
        <source>Service Meeting:</source>
        <translation>אסיפת השירות:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="806"/>
        <source>Congregation Bible Study</source>
        <translation>שיעור המקרא הקהילתי</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="833"/>
        <source>Theocratic Ministry School</source>
        <translation>בית־הספר לשירות התיאוקרטי</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="860"/>
        <source>Service Meeting</source>
        <translation>אסיפת השירות</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1103"/>
        <location filename="../mainwindow.ui" line="1368"/>
        <source>Export</source>
        <translation>יוצא</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1280"/>
        <source>Public talks</source>
        <translation>נאומים פומבים</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1398"/>
        <source>Import</source>
        <translation>יבוא</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1432"/>
        <source>info</source>
        <translation>מידע</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2023"/>
        <source>Data exhange</source>
        <translation>חילופי נתונים</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2076"/>
        <source>TheocBase Cloud</source>
        <translation>ענן TheocBase</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1607"/>
        <location filename="../mainwindow.ui" line="1640"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1839"/>
        <source>Timeline</source>
        <translation>ציר זמן</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1900"/>
        <source>Print...</source>
        <translation>הדפס...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1903"/>
        <source>Print</source>
        <translation>הדפס</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1912"/>
        <source>Settings...</source>
        <comment>This means the &apos;Options&apos; of TheocBase</comment>
        <translation>הגדרות ...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1924"/>
        <source>Publishers...</source>
        <translation>מבשרים...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1223"/>
        <location filename="../mainwindow.ui" line="1927"/>
        <source>Publishers</source>
        <translation>מבשר</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1993"/>
        <source>Speakers...</source>
        <translation>נואמים...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1260"/>
        <location filename="../mainwindow.ui" line="1996"/>
        <source>Speakers</source>
        <translation>נואמים</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2020"/>
        <source>Data exhange...</source>
        <translation>חילופי נתונים...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2032"/>
        <source>TheocBase help...</source>
        <translation>עזרת TheocBase ...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2047"/>
        <source>History</source>
        <translation>היסטוריה</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2059"/>
        <location filename="../mainwindow.ui" line="2062"/>
        <source>Full Screen</source>
        <translation>מסך מלא</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2067"/>
        <source>Startup Screen</source>
        <translation>עמוד פתיחה</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2106"/>
        <source>Reminders...</source>
        <translation>תזכורות ...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="568"/>
        <source>Theme:</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="573"/>
        <source>Speaker:</source>
        <translation>נואם:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="578"/>
        <source>Chairman:</source>
        <translation>יושב ראש:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="583"/>
        <source>Watchtower Study:</source>
        <translation>שיעור המצפה</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="527"/>
        <source>Data exchange</source>
        <translation>חילופי נתונים</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1114"/>
        <source>Export Format</source>
        <translation>פורמט ייצוא</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1120"/>
        <source>For sending data to another user</source>
        <translation>למשלוח נתונים למשתמש אחר</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1139"/>
        <source>For easy import to Calendar programs</source>
        <translation>לייבוא קל לתוכניות לוח שנה</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1171"/>
        <source>Export Method</source>
        <translation>שיטת ייצוא</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1184"/>
        <source>Events grouped by date</source>
        <translation>אירועים מקובצים לפי תאריך</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1177"/>
        <source>All day events</source>
        <translation>אירועי כל היום</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1213"/>
        <source>Midweek Meeting</source>
        <translation>אסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1236"/>
        <source>Study History</source>
        <translation>היסטוריה שיעורים</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1287"/>
        <source>Outgoing Talks</source>
        <translation>נאומים יוצאים</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1298"/>
        <source>Date Range</source>
        <translation>טווח תאריכים</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1305"/>
        <source>Previous Weeks</source>
        <translation>שבועות קודמים</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1319"/>
        <source>From Date</source>
        <translation>מתאריך</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1336"/>
        <source>Thru Date</source>
        <translation>עד התאריך</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1793"/>
        <source>File</source>
        <translation>קובץ</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1799"/>
        <source>Tools</source>
        <translation>כלים</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1812"/>
        <source>Help</source>
        <translation>עזרה</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1888"/>
        <source>Today</source>
        <translation>היום</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1936"/>
        <source>Exit</source>
        <translation>יציאה</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1948"/>
        <source>Report bug...</source>
        <translation>דווח על באג ...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1960"/>
        <source>Send feedback...</source>
        <translation>שלח משוב...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1978"/>
        <source>About TheocBase...</source>
        <translation>אודות TheocBase ...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2005"/>
        <source>Check updates...</source>
        <translation>בדוק עדכונים...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2035"/>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2094"/>
        <source>Territories...</source>
        <translation>שטחים ...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2097"/>
        <source>Territories</source>
        <translation>שטחים</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1867"/>
        <source>Back</source>
        <translation>חזור</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1879"/>
        <source>Next</source>
        <translation>הבא</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2085"/>
        <source>Date</source>
        <translation>תאריך</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1969"/>
        <source>TheocBase website</source>
        <translation>אתר TheocBase</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="433"/>
        <source>Convention week (no meeting) </source>
        <translation>שבוע של כינוס (אין אסיפה) </translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="534"/>
        <source>Public Talk:</source>
        <translation>נאום פומבי:</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <location filename="../qml/MeetingNotes.qml" line="41"/>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation>הערות</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakerEdit</name>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="79"/>
        <source>Speaker</source>
        <translation>נואם</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="108"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="140"/>
        <source>Congregation</source>
        <translation>קהילה</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="192"/>
        <source>Meeting day</source>
        <translation>יום אסיפה</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="207"/>
        <source>Meeting time</source>
        <translation>שעה אסיפה</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="225"/>
        <source>Info</source>
        <translation>מידע</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="171"/>
        <source>Address</source>
        <translation>כתובת</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakersModel</name>
    <message>
        <location filename="../outgoingspeakersmodel.cpp" line="163"/>
        <source>From %1</source>
        <translation>מ - %1</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakersModule</name>
    <message>
        <location filename="../qml/OutgoingSpeakersModule.qml" line="109"/>
        <source>Outgoing speakers</source>
        <translation>נואמים יוצאים</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/OutgoingSpeakersModule.qml" line="194"/>
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>הנואם %1 נמצא בסוף השבוע</numerusform>
            <numerusform>הנואמים %1 נמצא בסוף השבוע</numerusform>
            <numerusform>הנואמים %1 נמצא בסוף השבוע</numerusform>
            <numerusform>הנואמים %1 נמצא בסוף השבוע</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakersModule.qml" line="195"/>
        <source>No speakers away this weekend</source>
        <translation>בסופ&apos;ח זה אין נואמים  פומבים שיוצים</translation>
    </message>
</context>
<context>
    <name>PrintCombination</name>
    <message>
        <location filename="../print/printcombination.cpp" line="31"/>
        <source>Select at least one option</source>
        <translation>בחר אפשרות אחת לפחות</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="41"/>
        <source>%1 Congregation</source>
        <comment>Congregation_Title Tag. %1 is Congregation Name</comment>
        <translation>קהילה %1</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="39"/>
        <source>Congregation</source>
        <translation>קהילה</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="47"/>
        <source>Chairman</source>
        <translation>יושב ראש</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="48"/>
        <source>Counselor</source>
        <translation>יועץ</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="50"/>
        <source>Reader</source>
        <translation>קרין</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="49"/>
        <source>Conductor</source>
        <translation>מדריך</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="51"/>
        <source>Speaker</source>
        <comment>Public talk speaker</comment>
        <translation>נואם</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="44"/>
        <source>Public Meeting</source>
        <translation>אסיפת פומבי</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="45"/>
        <source>Public Talk</source>
        <translation>נאום פומבי</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="46"/>
        <source>Watchtower Study</source>
        <translation>שיעור מצפה</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>&lt;--</source>
        <translation>&lt;--</translation>
    </message>
</context>
<context>
    <name>PrintCombination</name>
    <message>
        <location filename="../print/printcombination.cpp" line="43"/>
        <source>Midweek Meeting</source>
        <translation>אסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="42"/>
        <source>Christian Life and Ministry Meeting</source>
        <translation>אסיפת אורח חיינו ושירותנו המשיחיים</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="53"/>
        <source>min.</source>
        <comment>Abbreviation of minutes</comment>
        <translation>דקה/ות</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="68"/>
        <location filename="../print/printcombination.cpp" line="69"/>
        <source>Combined Schedule</source>
        <translation>לוח זמנים משולב</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="70"/>
        <source>Weekend Meeting</source>
        <translation>אסיפת סוף השבוע</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="71"/>
        <source>Outgoing Speakers</source>
        <translation>נאומים יוצים</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="88"/>
        <source>Week Starting %1</source>
        <comment>%1 is Monday of the week</comment>
        <translation>שבוע החל %1</translation>
    </message>
</context>
<context>
    <name>PrintDocument</name>
    <message>
        <location filename="../print/printdocument.cpp" line="145"/>
        <source>Template not found</source>
        <comment>printing template not found</comment>
        <translation>התבנית לא נמצאה</translation>
    </message>
    <message>
        <location filename="../print/printdocument.cpp" line="150"/>
        <source>Can&#x27;t read file</source>
        <comment>cannot read printing template</comment>
        <translation>לא ניתן לקרוא את הקובץ</translation>
    </message>
    <message>
        <location filename="../print/printdocument.cpp" line="235"/>
        <source>Service Talk</source>
        <translation>נאום שירות</translation>
    </message>
    <message>
        <location filename="../print/printdocument.cpp" line="245"/>
        <source>Begins at</source>
        <comment>Used in print template, example &apos;Begins at 11:00&apos;</comment>
        <translation>מתחיל ב</translation>
    </message>
</context>
<context>
    <name>PrintMidweekSchedule</name>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="37"/>
        <source>%1 Congregation</source>
        <comment>Congregation_Title Tag. %1 is Congregation Name</comment>
        <translation>קהילה %1</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="38"/>
        <source>Congregation</source>
        <translation>קהילה</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="40"/>
        <source>Public Talk</source>
        <translation>נאום פומבי</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="41"/>
        <source>Watchtower Study</source>
        <translation>שיעור מצפה</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="42"/>
        <location filename="../print/printmidweekschedule.cpp" line="80"/>
        <source>Treasures from God&#x27;s Word</source>
        <translation>אוצרות מדבר־אלוהים</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="43"/>
        <location filename="../print/printmidweekschedule.cpp" line="81"/>
        <source>Apply Yourself to the Field Ministry</source>
        <translation>התייעל בשירות השדה</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="44"/>
        <location filename="../print/printmidweekschedule.cpp" line="82"/>
        <source>Living as Christians</source>
        <translation>לחיות כמשיחיים</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="69"/>
        <source>Midweek Meeting</source>
        <translation>אסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="70"/>
        <source>Worksheet</source>
        <translation>גיליון פעילות</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="71"/>
        <location filename="../print/printmidweekschedule.cpp" line="104"/>
        <source>Opening Comments</source>
        <comment>See Workbook</comment>
        <translation>דברי פתיחה</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="72"/>
        <location filename="../print/printmidweekschedule.cpp" line="105"/>
        <source>Concluding Comments</source>
        <comment>See Workbook</comment>
        <translation>דברי סיכום</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="73"/>
        <source>Song</source>
        <translation>שיר</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="74"/>
        <source>Prayer</source>
        <translation>תפילה</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="75"/>
        <source>Chairman</source>
        <translation>יושב ראש</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="76"/>
        <source>Counselor</source>
        <translation>יועץ</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="77"/>
        <source>Conductor</source>
        <translation>מדריך</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="78"/>
        <source>Reader</source>
        <translation>קרין</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="79"/>
        <source>Christian Life and Ministry Meeting</source>
        <translation>אסיפת אורח חיינו ושירותנו המשיחיים</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="83"/>
        <source>Congregation Bible Study</source>
        <translation>שיעור המקרא הקהילתי</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="84"/>
        <source>Circuit Overseer</source>
        <translation>משגיח נפה</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="85"/>
        <source>No regular meeting</source>
        <translation>אין אסיפה קבועה</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="86"/>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>עוזר</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="87"/>
        <source>Study</source>
        <comment>Counsel point</comment>
        <translation>שיעור</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="88"/>
        <source>Theme</source>
        <comment>Talk Theme description from workbook</comment>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="89"/>
        <source>Source</source>
        <comment>Source information from workbook</comment>
        <translation>מקור</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="90"/>
        <source>Exercises</source>
        <translation>תרגילים</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="91"/>
        <source>Timing</source>
        <comment>Assignment completed in time?</comment>
        <translation>תזמון</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="92"/>
        <source>Notes</source>
        <translation>הערות</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="93"/>
        <source>Next study</source>
        <translation>השיעור הבא</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="94"/>
        <source>Setting</source>
        <comment>for sisters assignment</comment>
        <translation>רקע</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="95"/>
        <location filename="../print/printmidweekschedule.cpp" line="96"/>
        <location filename="../print/printmidweekschedule.cpp" line="97"/>
        <location filename="../print/printmidweekschedule.cpp" line="392"/>
        <source>Class</source>
        <translation>כתה</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="98"/>
        <source>Auxiliary Classroom Counselor</source>
        <comment>See S-140</comment>
        <translation>יועצת כיתת משנית</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="99"/>
        <source>Auxiliary Classroom</source>
        <comment>See S-140</comment>
        <translation>כיתה משנית</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="100"/>
        <source>Main Hall</source>
        <comment>See S-140</comment>
        <translation>אולם ראשי</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="101"/>
        <source>MH</source>
        <comment>Abbreviation for &apos;Main Hall&apos;</comment>
        <translation>MH</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="102"/>
        <source>A</source>
        <comment>Abbreviation for &apos;Auxiliary Classroom&apos;</comment>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="103"/>
        <source>Student</source>
        <comment>See S-140</comment>
        <translation>תלמיד</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="106"/>
        <source>Today</source>
        <translation>היום</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="107"/>
        <source>Next week</source>
        <translation>שבוע הבא</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="389"/>
        <source>Other schools</source>
        <translation>כתות אחרות</translation>
    </message>
</context>
<context>
    <name>PrintMidweekSlip</name>
    <message>
        <location filename="../print/printmidweekslip.cpp" line="50"/>
        <source>Slip Template</source>
        <translation>תבנית לפתק</translation>
    </message>
    <message>
        <location filename="../print/printmidweekslip.cpp" line="467"/>
        <source>Assistant</source>
        <translation>עוזר</translation>
    </message>
    <message>
        <location filename="../print/printmidweekslip.cpp" line="480"/>
        <location filename="../print/printmidweekslip.cpp" line="482"/>
        <source>Study %1</source>
        <comment>Text for study point on slip</comment>
        <translation>שיעור %1</translation>
    </message>
    <message>
        <location filename="../print/printmidweekslip.cpp" line="524"/>
        <source>2nd talk</source>
        <comment>When printing slips: if the first talk is not &apos;Return Visit&apos;</comment>
        <translation>שיחה שנייה</translation>
    </message>
</context>
<context>
    <name>PrintMidweekWorksheet</name>
    <message>
        <location filename="../print/printmidweekworksheet.cpp" line="61"/>
        <source>Class </source>
        <translation>כתה </translation>
    </message>
</context>
<context>
    <name>PrintOutgoingSchedule</name>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="59"/>
        <source>%1 Congregation</source>
        <comment>Congregation_Title Tag. %1 is Congregation Name</comment>
        <translation>קהילה %1</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="60"/>
        <location filename="../print/printoutgoingschedule.cpp" line="170"/>
        <source>Congregation</source>
        <translation>קהילה</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="62"/>
        <source>Public Talk</source>
        <translation>נאום פומבי</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="63"/>
        <source>Watchtower Study</source>
        <translation>שיעור מצפה</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="163"/>
        <source>Talk</source>
        <translation>נואם</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="164"/>
        <source>Outgoing Speakers</source>
        <translation>נאומים יוצים</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="165"/>
        <source>Date</source>
        <translation>תאריך</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="166"/>
        <source>Speaker</source>
        <translation>נואם</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="167"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="168"/>
        <source>Theme Number</source>
        <translation>מספר נושא</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="169"/>
        <source>Circuit</source>
        <translation>נפה</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="171"/>
        <source>Start Time</source>
        <translation>שעת התחלה</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="173"/>
        <source>NOTE: Dear brother, in spite of careful database-maintenance, sometimes times or addresses might be out of date. So, please verify by looking those up via JW.ORG. Thank you!</source>
        <translation>הערה: אח יקר, למרות תחזוקה מדוקדקת של מסדי נתונים, לפעמים זמנים או כתובות עשויים להיות מיושנים. אז אנא אמת זאת על ידי חיפוש אלה דרך JW.ORG. תודה!</translation>
    </message>
</context>
<context>
    <name>PrintTalksOfSpeakersList</name>
    <message>
        <location filename="../print/printtalksofspeakerslist.cpp" line="37"/>
        <location filename="../print/printtalksofspeakerslist.cpp" line="38"/>
        <source>Public Talks</source>
        <translation>נאומים פומבים</translation>
    </message>
    <message>
        <location filename="../print/printtalksofspeakerslist.cpp" line="39"/>
        <source>Contact info</source>
        <translation>פרטים ליצירת קשר</translation>
    </message>
    <message>
        <location filename="../print/printtalksofspeakerslist.cpp" line="40"/>
        <source>Talk numbers</source>
        <comment>Public talk numbers</comment>
        <translation>מספרים נאומים</translation>
    </message>
</context>
<context>
    <name>PrintTerritoryAssignmentRecord</name>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="35"/>
        <source>Territory Assignment Record</source>
        <translation>שיא הקצאת שטח</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="36"/>
        <source>Territory Coverage</source>
        <translation>כיסוי שטח</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="37"/>
        <source>Total number of territories</source>
        <translation>סה&quot;כ שטחים</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="38"/>
        <source>&lt; 6 months</source>
        <comment>territory worked</comment>
        <translation>&lt;6 חודשים</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="39"/>
        <source>6 to 12 months</source>
        <comment>territory worked</comment>
        <translation>6 עד 12 חודשים</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="40"/>
        <source>&gt; 12 months ago</source>
        <comment>territory worked</comment>
        <translation>לפני 12 חודשים</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="41"/>
        <source>Average per year</source>
        <comment>Number of times territory has been worked per year on average</comment>
        <translation>ממוצע לשנה</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="175"/>
        <source>Address</source>
        <translation>כתובת</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="176"/>
        <source>Addresses</source>
        <comment>Addresses included in the territory</comment>
        <translation>כתובות</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="177"/>
        <source>Address type</source>
        <translation>סוג כתובת</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="178"/>
        <source>Assigned to</source>
        <translation>שהוקצה ל</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="179"/>
        <source>Congregation</source>
        <translation>קהילה</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="181"/>
        <source>%1 Congregation</source>
        <comment>Congregation_Title Tag. %1 is Congregation Name</comment>
        <translation>קהילה %1</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="182"/>
        <source>Date checked out</source>
        <translation>תאריך לקיחת</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="183"/>
        <source>Date checked back in</source>
        <translation>תאריך החזרת</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="184"/>
        <source>Date last worked</source>
        <translation>התאריך האחרון עבד</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="185"/>
        <source>Date</source>
        <translation>תאריך</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="186"/>
        <source>City</source>
        <translation>עיר</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="187"/>
        <source>Country</source>
        <comment>Short name of country</comment>
        <translation>מדינה</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="188"/>
        <source>County</source>
        <comment>Name of administrative area level 2</comment>
        <translation>מחוז</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="189"/>
        <source>District</source>
        <comment>Sublocality, first-order civil entity below a locality</comment>
        <translation>מחוז</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="190"/>
        <source>From</source>
        <comment>From number; in number range</comment>
        <translation>מ</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="200"/>
        <source>No.</source>
        <comment>House or street number</comment>
        <translation>מס&apos;</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="201"/>
        <source>Locality</source>
        <translation>מקום</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="202"/>
        <source>Map</source>
        <comment>Map of a territory</comment>
        <translation>מפה</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="203"/>
        <source>Name</source>
        <comment>Name of person or building</comment>
        <translation>שם</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="204"/>
        <source>Postalcode</source>
        <comment>Mail code, ZIP</comment>
        <translation>מיקוד</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="205"/>
        <source>Name of publisher</source>
        <translation>שם המבשר</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="206"/>
        <source>Remark</source>
        <translation>הערה</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="207"/>
        <source>State</source>
        <comment>Short name of administrative area level 1</comment>
        <translation>מדינה</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="208"/>
        <source>Street</source>
        <comment>Streetname</comment>
        <translation>רחוב</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="209"/>
        <source>Territory</source>
        <translation>שטח</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="210"/>
        <source>Terr. No.</source>
        <translation>שטח מס&apos;</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="211"/>
        <source>Territory type</source>
        <translation>סוג שטח</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="212"/>
        <source>To</source>
        <comment>To number; in number range</comment>
        <translation>ל</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="213"/>
        <source>Type</source>
        <comment>Type of something</comment>
        <translation>זוג</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="214"/>
        <source>Sum</source>
        <comment>Total amount</comment>
        <translation>סכום</translation>
    </message>
</context>
<context>
    <name>PrintTerritoryCard</name>
    <message>
        <location filename="../print/printterritorycard.cpp" line="35"/>
        <source>Territory Map Card</source>
        <comment>Title tag for a S-12 or similar card</comment>
        <translation>כרטיס שטח</translation>
    </message>
    <message>
        <location filename="../print/printterritorycard.cpp" line="36"/>
        <source>Territory Map</source>
        <comment>Title tag for a sheet with a territory map</comment>
        <translation>מפת שטח</translation>
    </message>
    <message>
        <location filename="../print/printterritorycard.cpp" line="37"/>
        <source>Address List</source>
        <comment>Title tag for a sheet with a territory&apos;s address list</comment>
        <translation>רשימת כתובות</translation>
    </message>
    <message>
        <location filename="../print/printterritorycard.cpp" line="38"/>
        <source>Territory Map with Address List</source>
        <comment>Title tag for a sheet with a territory&apos;s map and address list</comment>
        <translation>מפת שטח עם רשימת כתובות</translation>
    </message>
    <message>
        <location filename="../print/printterritorycard.cpp" line="39"/>
        <source>Street List</source>
        <comment>Title tag for a sheet with a territory&apos;s street list</comment>
        <translation>רשימת רחובות</translation>
    </message>
    <message>
        <location filename="../print/printterritorycard.cpp" line="40"/>
        <source>Territory Map with Street List</source>
        <comment>Title tag for a sheet with a territory&apos;s map and street list</comment>
        <translation>מפת שטח עם רשימת רחובות</translation>
    </message>
    <message>
        <location filename="../print/printterritorycard.cpp" line="41"/>
        <source>Do-Not-Call List</source>
        <comment>Title tag for a sheet with a territory&apos;s Do-Not-Call list</comment>
        <translation>רשימת אסור להתקשר</translation>
    </message>
</context>
<context>
    <name>PrintTerritoryMapCard</name>
    <message>
        <location filename="../print/printterritorymapcard.cpp" line="53"/>
        <source>Template</source>
        <translation>תבנית</translation>
    </message>
</context>
<context>
    <name>PrintWeekendSchedule</name>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="36"/>
        <source>Weekend Meeting</source>
        <translation>אסיפת סוף השבוע</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="37"/>
        <source>Public Meeting</source>
        <translation>אסיפת פומבי</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="40"/>
        <location filename="../print/printweekendschedule.cpp" line="161"/>
        <source>%1 Congregation</source>
        <comment>Congregation_Title Tag. %1 is Congregation Name</comment>
        <translation>קהילה %1</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="41"/>
        <location filename="../print/printweekendschedule.cpp" line="162"/>
        <source>Congregation</source>
        <translation>קהלה</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="43"/>
        <source>Conductor</source>
        <translation>מדריך</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="44"/>
        <location filename="../print/printweekendschedule.cpp" line="61"/>
        <source>Chairman</source>
        <translation>יושב ראש</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="46"/>
        <location filename="../print/printweekendschedule.cpp" line="63"/>
        <source>Reader</source>
        <translation>קרין</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="47"/>
        <location filename="../print/printweekendschedule.cpp" line="100"/>
        <source>Public Talk</source>
        <translation>נאום פומבי</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="48"/>
        <location filename="../print/printweekendschedule.cpp" line="102"/>
        <source>Watchtower Study</source>
        <translation>שיעור מצפה</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="49"/>
        <source>Congregation Bible Study</source>
        <translation>שיעור המקרא הקהילתי</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="50"/>
        <source>Christian Life and Ministry Meeting</source>
        <translation>אסיפת אורח חיינו ושירותנו המשיחיים</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="51"/>
        <location filename="../print/printweekendschedule.cpp" line="66"/>
        <source>Date</source>
        <translation>תאריך</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="52"/>
        <source>Phone</source>
        <comment>Phone number title</comment>
        <translation>טלפון</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="53"/>
        <source>Host</source>
        <comment>Host for incoming public speaker</comment>
        <translation>מארח</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="60"/>
        <source>Speaker</source>
        <translation>נואם</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="64"/>
        <source>WT Reader</source>
        <translation>קורא WT</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="67"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="103"/>
        <source>Circuit Overseer</source>
        <translation>משגיח נפה</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="106"/>
        <source>Notes</source>
        <translation>הערות</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="118"/>
        <source>Prayer</source>
        <translation>תפלה</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="121"/>
        <source>Song</source>
        <translation>שיר</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="199"/>
        <source>Watchtower Conductor</source>
        <translation>מדריך שיעור המצפה</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="202"/>
        <source>No regular meeting</source>
        <translation>אין אסיפה קבועה</translation>
    </message>
</context>
<context>
    <name>PrintWeekendWorksheet</name>
    <message>
        <location filename="../print/printweekendworksheet.cpp" line="47"/>
        <source>Weekend Meeting</source>
        <translation>אסיפת סוף השבוע</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="52"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="81"/>
        <source>Congregation</source>
        <translation>קהלה</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="109"/>
        <source>Speaker</source>
        <translation>נואם</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="148"/>
        <source>Mobile</source>
        <translation>נייד</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="166"/>
        <source>Phone</source>
        <translation>טלפון</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="184"/>
        <source>Email</source>
        <translation>דוא&apos;ל</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="202"/>
        <source>Info</source>
        <translation>מידע</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="221"/>
        <source>Host</source>
        <translation>מארח</translation>
    </message>
</context>
<context>
    <name>QDialogButtonBox</name>
    <message>
        <location filename="../main.cpp" line="69"/>
        <source>Yes</source>
        <translation>כן</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="70"/>
        <source>&amp;Yes</source>
        <translation>&amp;כן</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="71"/>
        <source>No</source>
        <translation>לא</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="72"/>
        <source>&amp;No</source>
        <translation>&amp;לא</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="73"/>
        <source>Cancel</source>
        <translation>לבטל</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="74"/>
        <source>&amp;Cancel</source>
        <translation>&amp;לבטל</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="75"/>
        <source>Save</source>
        <translation>לשמור</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="76"/>
        <source>&amp;Save</source>
        <translation>&amp;לשמור</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="77"/>
        <source>Open</source>
        <translation>לפתוח</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="167"/>
        <source>Wrong username and/or password</source>
        <translation>שם משתמש ו / או סיסמה שגויים</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="275"/>
        <source>Database not found!</source>
        <translation>מסד נתונים לא נמצא!</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="278"/>
        <source>Choose database</source>
        <translation>בחר מסד נתונים</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="278"/>
        <source>SQLite files (*.sqlite)</source>
        <translation>קבצי SQLite (* .sqlite)</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="302"/>
        <source>Database restoring failed</source>
        <translation>שחזור מסד הנתונים נכשל</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="338"/>
        <location filename="../mainwindow.cpp" line="232"/>
        <source>Save changes?</source>
        <translation>שמור שינויים?</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="271"/>
        <source>Database copied to </source>
        <translation>מסד הנתונים הועתק אל</translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="37"/>
        <source>Database file not found! Searching path =</source>
        <translation>קובץ מסד הנתונים לא נמצא! נתיב חיפוש =</translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="56"/>
        <source>Database Error</source>
        <translation>שגיאה במסד נתונים</translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="702"/>
        <source>This version of the application (%1) is older than the database (%2). There is a strong probability that error messages will popup and changes may not be saved correctly. Please download and install the latest version for best results.</source>
        <translation>גרסה זו של היישום (%1) ישנה יותר ממסד הנתונים (%2). יש סבירות חזקה שהודעות שגיאה יופיעו וייתכן שהשינויים לא יישמרו כהלכה. אנא הורד והתקן את הגרסה האחרונה לקבלת התוצאות הטובות ביותר.</translation>
    </message>
    <message>
        <location filename="../cpublictalks.cpp" line="351"/>
        <source>Circuit</source>
        <translation>נפה</translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="746"/>
        <source>Database updated</source>
        <translation>מסד הנתונים עודכן</translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="321"/>
        <location filename="../ccongregation.cpp" line="347"/>
        <source>Circuit Overseer&#x27;s visit</source>
        <translation>ביקור משגיח נפה</translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="323"/>
        <location filename="../ccongregation.cpp" line="328"/>
        <source>%1 (No meeting)</source>
        <comment>no meeting exception type</comment>
        <translation>%1 (אין אסיפה)</translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="323"/>
        <location filename="../ccongregation.cpp" line="349"/>
        <source>Convention week</source>
        <translation>שבוע של כינוס</translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="325"/>
        <location filename="../ccongregation.cpp" line="351"/>
        <source>Memorial</source>
        <translation>ערב הזיכרון</translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="356"/>
        <source>Review</source>
        <comment>Theocratic ministry school review</comment>
        <translation>סקירה</translation>
    </message>
    <message>
        <location filename="../cpersoncombobox.cpp" line="56"/>
        <location filename="../cpublictalks.cpp" line="347"/>
        <source>Name</source>
        <translation>שם</translation>
    </message>
    <message>
        <location filename="../cpersoncombobox.cpp" line="57"/>
        <location filename="../cpublictalks.cpp" line="266"/>
        <location filename="../cpublictalks.cpp" line="348"/>
        <source>Last</source>
        <translation>אחרון</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="257"/>
        <location filename="../school.cpp" line="424"/>
        <source>All</source>
        <translation>כל</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="258"/>
        <location filename="../school.cpp" line="425"/>
        <source>Highlights</source>
        <translation>עיקרי הדברים</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="262"/>
        <location filename="../school.cpp" line="429"/>
        <source>WT Reader</source>
        <translation>קורא WT</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="264"/>
        <location filename="../school.cpp" line="431"/>
        <source>Selected</source>
        <translation>נבחר</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="323"/>
        <location filename="../school.cpp" line="486"/>
        <source>Assignment already has been made</source>
        <translation>המשימה כבר בוצעה</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="327"/>
        <location filename="../school.cpp" line="490"/>
        <source>Assigned student has other meeting parts</source>
        <translation>לתלמיד שהוקצה יש חלקי פגישה אחרים</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="331"/>
        <location filename="../school.cpp" line="494"/>
        <source>Student unavailable</source>
        <translation>התלמיד אינו זמין</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="335"/>
        <source>Student part of family</source>
        <translation>תלמיד חלק מהמשפחה</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="434"/>
        <source>Recently together</source>
        <translation>לאחרונה ביחד</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="498"/>
        <source>Family member used in another TMS assignment</source>
        <translation>בן משפחה המשמש במשימה TMS אחר</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="67"/>
        <source>I/O</source>
        <comment>Incoming/Outgoing</comment>
        <translation>I/O</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="72"/>
        <source>Notes</source>
        <translation>הערות</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="565"/>
        <source>Congregation Name</source>
        <translation>שם הקהילה</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="265"/>
        <location filename="../historytable.cpp" line="314"/>
        <location filename="../school.cpp" line="854"/>
        <source>R</source>
        <comment>abbreviation of the &apos;reader&apos;</comment>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="314"/>
        <source>CBS</source>
        <translation>CBS</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="314"/>
        <source>C</source>
        <comment>abbreviation of the &apos;conductor&apos;</comment>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="317"/>
        <source>Conductor</source>
        <translation>מדריך</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="317"/>
        <source>Reader</source>
        <translation>קרין</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="460"/>
        <source>#</source>
        <translation>#</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="461"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="222"/>
        <source>Assignment</source>
        <translation>משימה</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="462"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="223"/>
        <source>Note</source>
        <translation>הערה</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="463"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="224"/>
        <source>Time</source>
        <translation>זמן</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="256"/>
        <location filename="../school.cpp" line="263"/>
        <location filename="../school.cpp" line="423"/>
        <location filename="../school.cpp" line="430"/>
        <source>Assistant</source>
        <translation>עוזר</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="256"/>
        <source>Student</source>
        <translation>תךמיד</translation>
    </message>
    <message>
        <location filename="../school_item.cpp" line="265"/>
        <source>Bible highlights:</source>
        <translation>עיקרי הפרקים:</translation>
    </message>
    <message>
        <location filename="../school_item.cpp" line="268"/>
        <source>No. 1:</source>
        <translation>מס&apos; 1:</translation>
    </message>
    <message>
        <location filename="../school_item.cpp" line="271"/>
        <source>No. 2:</source>
        <translation>מס&apos; 2:</translation>
    </message>
    <message>
        <location filename="../school_item.cpp" line="274"/>
        <source>No. 3:</source>
        <translation>מס&apos; 3:</translation>
    </message>
    <message>
        <location filename="../school_item.cpp" line="277"/>
        <source>Reader:</source>
        <translation>קרין:</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="816"/>
        <location filename="../generatexml.cpp" line="107"/>
        <source>Default language not selected!</source>
        <translation>שפת ברירת המחדל לא נבחרה!</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="937"/>
        <source>The header row of CSV file is not valid.</source>
        <translation>שורת הכותרת של קובץ ה- CSV אינה חוקית.</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="293"/>
        <source>Confirm password!</source>
        <translation>אשר סיסמה!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2232"/>
        <source>A user with the same E-mail address has already been added.</source>
        <translation>משתמש עם אותה כתובת דוא&apos;ל כבר התווסף.</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2448"/>
        <source>All talks have been added to this week</source>
        <translation>כל השיחות התווספו השבוע</translation>
    </message>
    <message>
        <location filename="../cpublictalks.cpp" line="265"/>
        <location filename="../mainwindow.cpp" line="921"/>
        <location filename="../publictalkedit.cpp" line="71"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../importTa1ks.cpp" line="58"/>
        <location filename="../importkhs.cpp" line="42"/>
        <location filename="../importwintm.cpp" line="42"/>
        <source>Import</source>
        <translation>יבוא</translation>
    </message>
    <message>
        <location filename="../importTa1ks.cpp" line="58"/>
        <location filename="../importkhs.cpp" line="42"/>
        <location filename="../importwintm.cpp" line="42"/>
        <source>Import Complete</source>
        <translation>הייבוא הושלם</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="192"/>
        <source>Speaker</source>
        <comment>The todo list Speaker cell is in error</comment>
        <translation>נואם</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="199"/>
        <source>Congregation</source>
        <comment>The todo list Congregation cell is in error</comment>
        <translation>קהילה</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="209"/>
        <source>Theme (could be a theme this speaker does not give)</source>
        <comment>The todo list Theme cell is in error</comment>
        <translation>נושא (יכול להיות נושא שהנואם לא נותן)</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="221"/>
        <location filename="../todo.cpp" line="232"/>
        <source>Date Already Scheduled</source>
        <translation>התאריך כבר מתוזמן</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="89"/>
        <source>Unable to read new Workbook format</source>
        <translation>לא ניתן לקרוא פורמט חדש של גיליון פעילות</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="91"/>
        <source>Database not set to handle language &#x27;%1&#x27;</source>
        <translation>מסד הנתונים לא מוגדר לטפל בשפה &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="93"/>
        <source>Unable to find year/month</source>
        <translation>לא ניתן למצוא שנה / חודש</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="94"/>
        <source>Nothing imported (no dates recognized)</source>
        <translation>שום דבר לא מיובא (לא מזוהים תאריכים)</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="98"/>
        <location filename="../wtimport.cpp" line="31"/>
        <location filename="../wtimport.cpp" line="32"/>
        <source>Imported %1 weeks from %2 thru %3</source>
        <translation>מיובא %1 שבועות מ-%2 עד %3</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="417"/>
        <source>Please select the Talk Names to match the names we found in the workbook</source>
        <translation>בחר את שמות השיחה כדי להתאים לשמות שמצאנו בגיליון פעילות</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="7"/>
        <source>Ch</source>
        <comment>history table: abbreviation for the &apos;chairman&apos; of the Christian Life and Ministry Meeting</comment>
        <translation>Ch</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="9"/>
        <source>H</source>
        <comment>history table: abbreviation for the &apos;highlights&apos;</comment>
        <translation>H</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="11"/>
        <source>D</source>
        <comment>history table: abbreviation for &apos;Diging spiritual gem&apos;</comment>
        <translation>D</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="13"/>
        <source>#R</source>
        <comment>history table: abbreviation for the &apos;reader&apos;</comment>
        <translation>#R</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="15"/>
        <source>#V</source>
        <comment>history table: abbreviation for &apos;prepare video presentation&apos;</comment>
        <translation>#V</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="17"/>
        <source>#A</source>
        <comment>history table: abbreviation for &apos;Apply yourself to Reading and Teaching&apos;</comment>
        <translation>#A</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="19"/>
        <source>Mem</source>
        <comment>history table: abbreviation for assistant/householder of &apos;Memorial Inviation&apos;</comment>
        <translation>ערב הזיכרון</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="21"/>
        <source>Vid</source>
        <comment>history table: abbreviation for &apos;Extra Video Part&apos;</comment>
        <translation>וידאו</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="24"/>
        <source>♢1</source>
        <comment>history table: abbreviation for assignment 1, assistant/householder of &apos;Initial Visit&apos;</comment>
        <translation>♢1</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="25"/>
        <source>#1</source>
        <comment>history table: abbreviation for assignment 1, &apos;Initial Visit&apos;</comment>
        <translation>#1</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="30"/>
        <source>♢2</source>
        <comment>history table: abbreviation for assignment 2, assistant/householder of &apos;Return Visit&apos;</comment>
        <translation>♢2</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="31"/>
        <source>#2</source>
        <comment>history table: abbreviation for assignment 2, &apos;Return Visit&apos;</comment>
        <translation>#2</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="35"/>
        <source>♢3</source>
        <comment>history table: abbreviation for assignment 3, assistant/householder of &apos;Bible Study&apos;</comment>
        <translation>♢3</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="36"/>
        <source>#3</source>
        <comment>history table: abbreviation for assignment 3, &apos;Bible Study&apos;</comment>
        <translation>#3</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="38"/>
        <source>CL1</source>
        <comment>history table: abbreviation for cristian life, talk 1</comment>
        <translation>CL1</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="40"/>
        <source>CL2</source>
        <comment>history table: abbreviation for cristian life, talk 2</comment>
        <translation>CL2</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="42"/>
        <source>CL3</source>
        <comment>history table: abbreviation for cristian life, talk 3</comment>
        <translation>CL3</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="45"/>
        <source>BS-R</source>
        <comment>history table: abbreviation for cristian life, Bible Study Reader</comment>
        <translation>BS-R</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="46"/>
        <source>BS</source>
        <comment>history table: abbreviation for cristian life, Bible Study</comment>
        <translation>BS</translation>
    </message>
    <message>
        <location filename="../slipscanner.cpp" line="32"/>
        <source>One-time Scanning of New Slip</source>
        <translation>סריקה חד פעמית של הפתק חדש</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="738"/>
        <source>All weekend meetings for</source>
        <comment>Filename prefix for weekend meetings iCal export</comment>
        <translation>כל האסיפות סוף השבוע עבור</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="750"/>
        <source>Weekend Meeting</source>
        <translation>אסיפת סוף השבוע</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="858"/>
        <source>All outgoing talks for</source>
        <comment>File name prefix for outgoing talks iCal export</comment>
        <translation>כל הנאומים היוצאות ל</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="874"/>
        <location filename="../mainwindow.cpp" line="885"/>
        <source>Outgoing Talks</source>
        <translation>נאומים יוצאים</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="990"/>
        <source>Midweek Meeting</source>
        <translation>אסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="459"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="220"/>
        <location filename="../publictalkedit.cpp" line="68"/>
        <location filename="../schoolreminder.cpp" line="183"/>
        <source>Date</source>
        <translation>תאריך</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="465"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="226"/>
        <source>Together</source>
        <comment>The column header text to show partner in student assignment</comment>
        <translation>יחד</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="167"/>
        <source>Sister</source>
        <translation>אחות</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="168"/>
        <source>Brother</source>
        <translation>אח</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="172"/>
        <source>Dear %1 %2</source>
        <translation>%1 %2 היקר/ה</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="173"/>
        <source>Please find below details of your upcoming assignment:</source>
        <translation>אנא ראה למטה פרטים על המשימות הקרובות שלך:</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="249"/>
        <source>Regards</source>
        <translation>בברכה</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="209"/>
        <source>Publisher</source>
        <comment>Roles and access control</comment>
        <translation>מבשר/ת</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="98"/>
        <source>View midweek meeting schedule</source>
        <comment>Access Control</comment>
        <translation>צפה בלוח הזמנים של אסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="100"/>
        <source>Edit midweek meeting schedule</source>
        <comment>Access Control</comment>
        <translation>ערוך את לוח הזמנים לאסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="102"/>
        <source>View midweek meeting settings</source>
        <comment>Access Control</comment>
        <translation>הצג את הגדרות האסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="104"/>
        <source>Edit midweek meeting settings</source>
        <comment>Access Control</comment>
        <translation>ערוך את הגדרות האסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="106"/>
        <source>Send midweek meeting reminders</source>
        <comment>Access Control</comment>
        <translation>שלח תזכורות לאסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="108"/>
        <source>Print midweek meeting schedule</source>
        <comment>Access Control</comment>
        <translation>הדפס לוח זמנים לאסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="110"/>
        <source>Print midweek meeting assignment slips</source>
        <comment>Access Control</comment>
        <translation>הדפס פתקי משימות לאסיפות אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="112"/>
        <source>Print midweek meeting worksheets</source>
        <comment>Access Control</comment>
        <translation>הדפס דפי עבודה של אסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="114"/>
        <source>View weekend meeting schedule</source>
        <comment>Access Control</comment>
        <translation>צפה בלוח הזמנים של אסיפת סוף השבוע</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="116"/>
        <source>Edit weekend meeting schedule</source>
        <comment>Access Control</comment>
        <translation>ערוך את לוח הזמנים לאסיפת סוף השבוע</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="118"/>
        <source>View weekend meeting settings</source>
        <comment>Access Control</comment>
        <translation>הצג את הגדרות האסיפת סוף השבוע</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="120"/>
        <source>Edit weekend meeting settings</source>
        <comment>Access Control</comment>
        <translation>ערוך את הגדרות האסיפת סוף השבוע</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="122"/>
        <source>View public talk list</source>
        <comment>Access Control</comment>
        <translation>צפה ברשימת הנאומים פומבים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="124"/>
        <source>Edit public talk list</source>
        <comment>Access Control</comment>
        <translation>ערוך את רשימת הנאומים פומבים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="126"/>
        <source>Schedule hospitality</source>
        <comment>Access Control</comment>
        <translation>תזמן אירוח</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="128"/>
        <source>Print weekend meeting schedule</source>
        <comment>Access Control</comment>
        <translation>הדפס לוח זמנים לאסיפות סוף השבוע</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="130"/>
        <source>Print weekend meeting worksheets</source>
        <comment>Access Control</comment>
        <translation>הדפיסו גיליון פעילות של אסיפות סוף השבוע</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="132"/>
        <source>Print speakers schedule</source>
        <comment>Access Control</comment>
        <translation>הדפס לוח זמנים של נואמים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="134"/>
        <source>Print speakers assignments</source>
        <comment>Access Control</comment>
        <translation>הדפס משימות של נואמים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="136"/>
        <source>Print hospitality</source>
        <comment>Access Control</comment>
        <translation>הדפם הכנסת אורחים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="138"/>
        <source>Print public talk list</source>
        <comment>Access Control</comment>
        <translation>הדפיסו רשימת נאומים פומבים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="148"/>
        <source>View public speakers</source>
        <comment>Access Control</comment>
        <translation>צפו בנואמים פומבים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="150"/>
        <source>Edit public speakers</source>
        <comment>Access Control</comment>
        <translation>ערוך נואמים פומבים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="140"/>
        <source>View publishers</source>
        <comment>Access Control</comment>
        <translation>צפו במבשרים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="142"/>
        <source>Edit publishers</source>
        <comment>Access Control</comment>
        <translation>ערוך מבשרים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="144"/>
        <source>View student data</source>
        <comment>Access Control</comment>
        <translation>צפו בנתוני התלמידים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="146"/>
        <source>Edit student data</source>
        <comment>Access Control</comment>
        <translation>ערוך את נתוני התלמידים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="152"/>
        <source>View privileges</source>
        <comment>Access Control</comment>
        <translation>הצג זכויות</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="154"/>
        <source>Edit privileges</source>
        <comment>Access Control</comment>
        <translation>ערוך זכויות</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="156"/>
        <source>View midweek meeting talk history</source>
        <comment>Access Control</comment>
        <translation>צפו בהיסטוריית השיחות של אסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="158"/>
        <source>View availabilities</source>
        <comment>Access Control</comment>
        <translation>צפה בזמינות</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="160"/>
        <source>Edit availabilities</source>
        <comment>Access Control</comment>
        <translation>ערוך זמינות</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="162"/>
        <source>View permissions</source>
        <comment>Access Control</comment>
        <translation>הצג הרשאות</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="164"/>
        <source>Edit permissions</source>
        <comment>Access Control</comment>
        <translation>ערוך הרשאות</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="166"/>
        <source>View territories</source>
        <comment>Access Control</comment>
        <translation>צפו בשטחים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="168"/>
        <source>Edit territories</source>
        <comment>Access Control</comment>
        <translation>ערוך שטחים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="170"/>
        <source>Print territory record</source>
        <comment>Access Control</comment>
        <translation>הדפסת שיא שטח</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="172"/>
        <source>Print territory map card</source>
        <comment>Access Control</comment>
        <translation>הדפסת כרטיס מפת שטח</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="174"/>
        <source>Print territory map and address sheets</source>
        <comment>Access Control</comment>
        <translation>הדפסת מפת שטח וגיליונות כתובת</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="176"/>
        <source>View territory settings</source>
        <comment>Access Control</comment>
        <translation>הצג הגדרות שטח</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="178"/>
        <source>Edit territory settings</source>
        <comment>Access Control</comment>
        <translation>ערוך הגדרות שטח</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="180"/>
        <source>View territory assignments</source>
        <comment>Access Control</comment>
        <translation>צפו בהקצאות שטחים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="182"/>
        <source>View territory addresses</source>
        <comment>Access Control</comment>
        <translation>הצג כתובות שטח</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="184"/>
        <source>View congregation settings</source>
        <comment>Access Control</comment>
        <translation>צפו בהגדרות הקהילה</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="186"/>
        <source>Edit congregation settings</source>
        <comment>Access Control</comment>
        <translation>ערוך את הגדרות הקהילה</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="188"/>
        <location filename="../accesscontrol.h" line="192"/>
        <source>View special events</source>
        <comment>Access Control</comment>
        <translation>צפו באירועים מיוחדים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="190"/>
        <location filename="../accesscontrol.h" line="194"/>
        <source>Edit special events</source>
        <comment>Access Control</comment>
        <translation>ערוך אירועים מיוחדים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="196"/>
        <source>View song list</source>
        <comment>Access Control</comment>
        <translation>צפה ברשימת השירים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="198"/>
        <source>Edit song list</source>
        <comment>Access Control</comment>
        <translation>ערוך את רשימת השירים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="200"/>
        <source>Delete cloud data</source>
        <comment>Access Control</comment>
        <translation>מחק נתוני ענן</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="211"/>
        <source>Elder</source>
        <comment>Roles and access control</comment>
        <translation>זקן</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="213"/>
        <source>LMM Chairman</source>
        <comment>Roles and access control</comment>
        <translation>יו&quot;ר LMM</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="215"/>
        <source>LMM Overseer</source>
        <comment>Roles and access control</comment>
        <translation>משגיח LMM</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="217"/>
        <source>Public Talk Coordinator</source>
        <comment>Roles and access control</comment>
        <translation>מתאמ נאומים פומבים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="219"/>
        <source>Territory Servant</source>
        <comment>Roles and access control</comment>
        <translation>משגיח שטחים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="221"/>
        <source>Secretary</source>
        <comment>Roles and access control</comment>
        <translation>מזכיר</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="223"/>
        <source>Service Overseer</source>
        <comment>Roles and access control</comment>
        <translation>משגיח שירות</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="225"/>
        <source>Coordinator of BOE</source>
        <comment>Roles and access control</comment>
        <translation>מתאם BOE</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="227"/>
        <source>Administrator</source>
        <comment>Roles and access control</comment>
        <translation>מנהל</translation>
    </message>
    <message>
        <location filename="../print/printdocument.cpp" line="497"/>
        <source>Width unit does not match height unit</source>
        <comment>while asking for custom paper size</comment>
        <translation>יחידת הרוחב אינה תואמת את יחידת הגובה</translation>
    </message>
    <message>
        <location filename="../print/printdocument.cpp" line="525"/>
        <source>Invalid entry, sorry.</source>
        <comment>while asking for custom paper size</comment>
        <translation>ערך לא חוקי, מצטער.</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../settings.ui" line="174"/>
        <location filename="../settings.ui" line="177"/>
        <location filename="../settings.ui" line="1641"/>
        <source>Exceptions</source>
        <translation>חריגים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="204"/>
        <location filename="../settings.ui" line="207"/>
        <source>Public Talks</source>
        <translation>נאומים פומבים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="773"/>
        <source>Custom templates folder</source>
        <translation>תיקיית תבניות מותאמות אישית</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="917"/>
        <source>Open database location</source>
        <translation>פתח מיקום מסד נתונים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="976"/>
        <source>Backup database</source>
        <translation>גיבוי מסד נתונים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1010"/>
        <source>Restore database</source>
        <translation>שחזר מסד נתונים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1462"/>
        <source>Names display order</source>
        <translation>שמות מציגים סדר</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1397"/>
        <source>Color palette</source>
        <translation>פלטת צבעים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1474"/>
        <source>By last name</source>
        <translation>לפי שם המשפחה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1486"/>
        <source>By first name</source>
        <translation>לפי שם פרטי</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1385"/>
        <source>Light</source>
        <translation>בהיר</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1351"/>
        <source>Dark</source>
        <translation>כהה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1576"/>
        <source>Show song titles</source>
        <translation>הראה כותרות שירים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2522"/>
        <source>Public talks maintenance</source>
        <translation>תחזוקת נאומים פומבים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2614"/>
        <source>Schedule hospitality for public speakers</source>
        <translation>קבע אירוח לנואמים פומבים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3130"/>
        <source>Streets</source>
        <translation>רחובות</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3136"/>
        <source>Street types</source>
        <translation>סוגי רחובות</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3386"/>
        <source>Map marker scale:</source>
        <translation>קנה מידה של סמן מפה:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3412"/>
        <source>Geo Services</source>
        <translation>שירותי גיאוגרפיה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3420"/>
        <source>Google:</source>
        <translation>Google:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3430"/>
        <location filename="../settings.ui" line="3433"/>
        <source>API Key</source>
        <translation>מפתח API</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3440"/>
        <source>Here:</source>
        <translation>פה:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3447"/>
        <source>Default:</source>
        <translation>ברירת מחדל:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3457"/>
        <source>OpenStreetMap</source>
        <translation>OpenStreetMap</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3462"/>
        <source>Google</source>
        <translation>Google</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3467"/>
        <source>Here</source>
        <translation>פה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3482"/>
        <location filename="../settings.ui" line="3485"/>
        <source>App Id</source>
        <translation>מזהה אפליקציה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3492"/>
        <location filename="../settings.ui" line="3495"/>
        <source>App Code</source>
        <translation>קוד אפליקציה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3578"/>
        <source>Send E-Mail Reminders</source>
        <translation>לשלוח תזכורות בדוא&apos;ל</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3541"/>
        <source>Send reminders when closing TheocBase</source>
        <translation>שלח תזכורות בעת סגירת TheocBase</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3556"/>
        <source>E-Mail Options</source>
        <translation>אפשרויות דוא&apos;ל</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3612"/>
        <source>Sender&#x27;s e-mail</source>
        <translation>הדוא&apos;ל של השולח</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3622"/>
        <source>Sender&#x27;s name</source>
        <translation>שם השולח</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3686"/>
        <source>Account</source>
        <translation>חשבון</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3749"/>
        <source>Test Connection</source>
        <translation>בדיקת חיבור</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="706"/>
        <source>Printing</source>
        <translation>הדפסה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3782"/>
        <source>Users</source>
        <translation>משתמשים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3798"/>
        <source>E-mail:</source>
        <translation>דוא&apos;ל:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3851"/>
        <source>Rules</source>
        <translation>כללים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="159"/>
        <location filename="../settings.ui" line="162"/>
        <source>General</source>
        <translation>כללי</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="839"/>
        <source>Backup</source>
        <translation>גיבוי</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="468"/>
        <source>Current congregation</source>
        <translation>הקהילה הנוכחית</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1302"/>
        <source>User interface</source>
        <translation>ממשק משתמש</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1507"/>
        <source>User interface language</source>
        <translation>שפת ממשק משתמש</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1063"/>
        <source>Security</source>
        <translation>הגנה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1242"/>
        <source>Enable password</source>
        <translation>אפשר סיסמה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1267"/>
        <source>Enable database encryption</source>
        <translation>אפשר הצפנת מסד נתונים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1168"/>
        <source>Confirm</source>
        <translation>לאשר</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="189"/>
        <location filename="../settings.ui" line="192"/>
        <location filename="../settings.ui" line="1845"/>
        <source>Life and Ministry Meeting</source>
        <translation>אסיפת אורח חיינו ושירותנו המשיחיים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2038"/>
        <source>Remove Duplicates</source>
        <translation>הסר כפילויות</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2045"/>
        <source>Meeting Items</source>
        <translation>פריטי אסיפות</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2144"/>
        <source>Schedule</source>
        <comment>Name of tab to edit Midweek Meeting schedule</comment>
        <translation>לוח זמנים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2624"/>
        <source>Hide discontinued</source>
        <translation>הסתר הופסק</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2806"/>
        <source>Add songs</source>
        <translation>הוסף שירים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="219"/>
        <location filename="../settings.ui" line="2812"/>
        <source>Songs</source>
        <translation>שירים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2213"/>
        <location filename="../settings.ui" line="2363"/>
        <source>&lt;html&gt;&lt;head /&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; color:#540000;&quot;&gt;Errors&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt; &lt;head /&gt; &lt;body&gt; &lt;p&gt; &lt;span style = &quot;color: # 540000;&quot;&gt; שגיאות &lt;/ span&gt; &lt;/p&gt; &lt;/body&gt; &lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2884"/>
        <location filename="../settings.ui" line="2890"/>
        <source>Add song one at a time</source>
        <translation>הוסף שיר אחד בכל פעם</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2905"/>
        <source>Song number</source>
        <translation>מספר השיר</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2912"/>
        <source>Song title</source>
        <translation>כותרת השיר</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2986"/>
        <source>Cities</source>
        <translation>ערים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3058"/>
        <source>Territory types</source>
        <translation>סוגי שטחים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3235"/>
        <source>Addresses</source>
        <translation>כתובות</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3241"/>
        <source>Address types</source>
        <translation>סוגי כתובות</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3349"/>
        <source>Configuration</source>
        <translation>תצורה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="246"/>
        <location filename="../settings.cpp" line="2246"/>
        <source>Access Control</source>
        <translation>בקרת גישה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3247"/>
        <source>Type number:</source>
        <translation>מספר סוג:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3142"/>
        <location filename="../settings.ui" line="3257"/>
        <location filename="../settings.ui" line="3788"/>
        <source>Name:</source>
        <translation>שם:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3211"/>
        <location filename="../settings.ui" line="3267"/>
        <source>Color:</source>
        <translation>צבע:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3152"/>
        <location filename="../settings.ui" line="3277"/>
        <source>#0000ff</source>
        <translation>#0000ff</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3357"/>
        <source>Default address type:</source>
        <translation>סוג כתובת ברירת מחדל:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="228"/>
        <source>Territories</source>
        <translation>שטחים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="386"/>
        <location filename="../settings.cpp" line="1107"/>
        <source>Congregation</source>
        <translation>קהילה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="624"/>
        <source>Weekend Meeting</source>
        <translation>אסיפת סוף השבוע</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="435"/>
        <source>Current Circuit Overseer</source>
        <translation>משגיח נפה נוכחי</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="668"/>
        <source>Click to edit</source>
        <translation>לחץ כדי לערוך</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1111"/>
        <location filename="../settings.ui" line="3742"/>
        <source>Username</source>
        <translation>שם משתמש</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1132"/>
        <location filename="../settings.ui" line="3659"/>
        <source>Password</source>
        <translation>סיסמה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="529"/>
        <location filename="../settings.ui" line="1863"/>
        <location filename="../settings.ui" line="1927"/>
        <source>Mo</source>
        <translation>ב&apos;</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="534"/>
        <location filename="../settings.ui" line="1868"/>
        <location filename="../settings.ui" line="1932"/>
        <source>Tu</source>
        <translation>ג&apos;</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="539"/>
        <location filename="../settings.ui" line="1873"/>
        <location filename="../settings.ui" line="1937"/>
        <source>We</source>
        <translation>ד&apos;</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="544"/>
        <location filename="../settings.ui" line="1878"/>
        <location filename="../settings.ui" line="1942"/>
        <source>Th</source>
        <translation>ה&apos;</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="549"/>
        <location filename="../settings.ui" line="1883"/>
        <location filename="../settings.ui" line="1947"/>
        <source>Fr</source>
        <translation>ו&apos;</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="554"/>
        <location filename="../settings.ui" line="1888"/>
        <location filename="../settings.ui" line="1952"/>
        <source>Sa</source>
        <translation>שבת</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="559"/>
        <location filename="../settings.ui" line="1893"/>
        <location filename="../settings.ui" line="1957"/>
        <source>Su</source>
        <translation>א&apos;</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1906"/>
        <source>Public Talk and Watchtower study</source>
        <translation>נאום פומבי ושיעור מצפה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1779"/>
        <location filename="../settings.ui" line="1808"/>
        <location filename="../settings.ui" line="2235"/>
        <location filename="../settings.ui" line="2296"/>
        <location filename="../settings.ui" line="2346"/>
        <location filename="../settings.ui" line="2385"/>
        <location filename="../settings.ui" line="2445"/>
        <location filename="../settings.ui" line="2468"/>
        <location filename="../settings.ui" line="2555"/>
        <location filename="../settings.ui" line="2755"/>
        <location filename="../settings.ui" line="2833"/>
        <location filename="../settings.ui" line="2856"/>
        <location filename="../settings.ui" line="2931"/>
        <location filename="../settings.ui" line="3007"/>
        <location filename="../settings.ui" line="3033"/>
        <location filename="../settings.ui" line="3079"/>
        <location filename="../settings.ui" line="3105"/>
        <location filename="../settings.ui" line="3174"/>
        <location filename="../settings.ui" line="3200"/>
        <location filename="../settings.ui" line="3296"/>
        <location filename="../settings.ui" line="3322"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1683"/>
        <source>Circuit Overseer&#x27;s visit</source>
        <translation>ביקור משגיח הנפה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1688"/>
        <source>Convention</source>
        <translation>כינוס</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1693"/>
        <source>Memorial</source>
        <translation>ערב הזיכרון</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1698"/>
        <source>Zone overseer&#x27;s talk</source>
        <translation>Zone overseer&apos;s talk</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1703"/>
        <source>Other exception</source>
        <translation>חריג אחר</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1716"/>
        <location filename="../settings.cpp" line="614"/>
        <source>Start date</source>
        <translation>תאריך התחלה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1744"/>
        <location filename="../settings.cpp" line="615"/>
        <source>End date</source>
        <translation>תאריך סיום</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1858"/>
        <location filename="../settings.ui" line="1922"/>
        <source>No meeting</source>
        <translation>אין אסיפה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1824"/>
        <source>Description</source>
        <translation>תיאור</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2078"/>
        <source>Import Christian Life and Ministry Meeting Workbook</source>
        <translation>ייבא את חגיליון הפעילות לאסיפת אורח חיינו ושירותנו המשיחיים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2006"/>
        <source>Main</source>
        <comment>Main Midweek meeting tab</comment>
        <translation>ראשי</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2157"/>
        <source>Year</source>
        <translation>שנה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="486"/>
        <location filename="../settings.ui" line="2195"/>
        <source>Midweek Meeting</source>
        <translation>אסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2322"/>
        <source>Midweek Meeting Schedule for selected Meeting above</source>
        <translation>לוח אסיפות אמצע השבוע לאסיפה שנבחרה לעיל</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="237"/>
        <source>Reminders</source>
        <translation>תזכורות</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3918"/>
        <source>Run a file provided by the Help Desk</source>
        <translation>הפעל קובץ המסופק על ידי דלפק העזרה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="26"/>
        <source>Settings</source>
        <comment>This means the &apos;Options&apos; of TheocBase</comment>
        <translation>הגדרות</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2012"/>
        <source>Number of classes</source>
        <translation>מספר קיטות</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2753"/>
        <source>Studies</source>
        <translation>שיעורים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2638"/>
        <location filename="../settings.ui" line="2644"/>
        <source>Add subject one at a time</source>
        <translation>הוסף נושא אחד בכל פעם</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2678"/>
        <source>Public talk number</source>
        <translation>מספר נאום פומבי</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2685"/>
        <source>Public talk subject</source>
        <translation>נושא נאום פומבי</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2695"/>
        <location filename="../settings.ui" line="2942"/>
        <source>Language</source>
        <translation>שפה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2727"/>
        <source>Add congregations and speakers</source>
        <translation>הוסף קהילות ונואמים</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="834"/>
        <location filename="../settings.cpp" line="1109"/>
        <location filename="../settings.cpp" line="1177"/>
        <source>Number</source>
        <translation>מספר</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2560"/>
        <source>Source</source>
        <translation>מקור</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="562"/>
        <source>Select a backup file</source>
        <translation>בחר קובץ גיבוי</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="940"/>
        <location filename="../settings.cpp" line="1262"/>
        <location filename="../settings.cpp" line="1559"/>
        <location filename="../settings.cpp" line="1627"/>
        <location filename="../settings.cpp" line="1711"/>
        <location filename="../settings.cpp" line="1807"/>
        <source>Remove selected row?</source>
        <translation>להסיר את השורה שנבחרה?</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1038"/>
        <source>A public talk with the same number is already saved!
Do you want to discontinue the previous talk?

Scheduled talks will be moved to the To Do List.</source>
        <translation>נאום פומבית עם אותו מספר כבר נשמרת!
האם אתה רוצה להפסיק את הנאום הקודמת?
 
נאום מתוזמנות יועברו לרשימת המטלות.</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1178"/>
        <source>Title</source>
        <translation>כותרת</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1278"/>
        <source>Song number missing</source>
        <translation>מספר השיר חסר</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1281"/>
        <source>Song title missing</source>
        <translation>כותרת השיר חסרה</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1291"/>
        <source>Song is already saved!</source>
        <translation>השיר כבר נשמר!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1300"/>
        <source>Song added to database</source>
        <translation>השיר נוסף למסד הנתונים</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1337"/>
        <source>City</source>
        <translation>עִיר</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1373"/>
        <location filename="../settings.cpp" line="1457"/>
        <source>Type</source>
        <translation>סוג</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1523"/>
        <source>City name missing</source>
        <translation>חסר שם עיר</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1532"/>
        <source>City is already saved!</source>
        <translation>עיר כבר נשמרה!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1540"/>
        <source>City added to database</source>
        <translation>עיר נוספה למסד הנתונים</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1591"/>
        <source>Territory type name missing</source>
        <translation>חסר שם סוג השטח</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1600"/>
        <source>Territory type is already saved!</source>
        <translation>סוג השטח כבר נשמר!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1608"/>
        <source>Territory type added to database</source>
        <translation>סוג השטח נוסף למסד הנתונים</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1677"/>
        <source>Name of the street type is missing</source>
        <translation>חסר שם סוג הרחוב</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1684"/>
        <source>Street type is already saved!</source>
        <translation>סוג הרחוב כבר נשמר!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1692"/>
        <source>Street type added to database</source>
        <translation>סוג רחוב נוסף למסד הנתונים</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2009"/>
        <source>This is no longer an option. Please request help in the forum if this is needed.</source>
        <translation>זו כבר לא אפשרות. אנא בקש עזרה בפורום אם יש צורך בכך.</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2246"/>
        <source>Remove permissions for the selected user?</source>
        <translation>להסיר הרשאות עבור המשתמש שנבחר?</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2494"/>
        <source>Date</source>
        <translation>תאריך</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="835"/>
        <location filename="../settings.cpp" line="2559"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="837"/>
        <source>Released on</source>
        <comment>Release date of the public talk outline</comment>
        <translation>שוחרר ב</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="838"/>
        <source>Discontinued on</source>
        <comment>Date after which the public talk outline should no longer be used</comment>
        <translation>הופסק ב</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="991"/>
        <source>Discontinuing this talk will move talks scheduled with this outline to the To Do List.

</source>
        <translation>הפסקת נאום זו תעביר את הנאומים שנקבעו עם ראשי פרקים זה לרשימת המטלות.

</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1110"/>
        <source>Revision</source>
        <translation>תיקון</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1414"/>
        <location filename="../settings.cpp" line="1458"/>
        <location filename="../settings.cpp" line="2123"/>
        <location filename="../settings.cpp" line="2179"/>
        <source>Name</source>
        <translation>שם</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1415"/>
        <location filename="../settings.cpp" line="1459"/>
        <source>Color</source>
        <translation>צבע</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1764"/>
        <source>Number of address type is missing</source>
        <translation>מספר סוג הכתובת חסר</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1768"/>
        <source>Name of address type is missing</source>
        <translation>חסר שם סוג הכתובת</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1777"/>
        <source>Address type is already saved!</source>
        <translation>סוג הכתובת כבר נשמר!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1787"/>
        <source>Address type added to database</source>
        <translation>סוג כתובת נוסף למסד הנתונים</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1900"/>
        <source>Error sending e-mail</source>
        <translation>שגיאה בשליחת דוא&apos;ל</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1985"/>
        <location filename="../settings.cpp" line="2695"/>
        <source>Select ePub file</source>
        <translation>בחר קובץ ePub</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2014"/>
        <source>Warning: Make sure this file comes from a trusted source. Continue?</source>
        <translation>אזהרה: ודא שקובץ זה מקור ממקור מהימן. להמשיך?</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2018"/>
        <source>Command File</source>
        <translation>קובץ פקודה</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2411"/>
        <location filename="../settings.cpp" line="2460"/>
        <source>Meeting</source>
        <translation>אסיפה</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2411"/>
        <source>Remove the whole meeting? (Use only to remove invalid data from database)</source>
        <translation>להסיר את כל האסיפה? (השתמש רק בכדי להסיר נתונים לא חוקיים ממסד הנתונים)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2452"/>
        <source>Enter source material here</source>
        <translation>הזן כאן חומר מקור</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2460"/>
        <source>Remove this talk? (Use only to remove invalid data from database)</source>
        <translation>להסיר את השיחה הזו? (השתמש רק בכדי להסיר נתונים לא חוקיים ממסד הנתונים)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2495"/>
        <source>Bible Reading</source>
        <translation>קריאת המקרא</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2496"/>
        <source>Song 1</source>
        <translation>שיר 1</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2497"/>
        <source>Song 2</source>
        <translation>שיר 2</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2498"/>
        <source>Song 3</source>
        <translation>שיר 3</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2558"/>
        <source>Meeting Item</source>
        <translation>פריט אסיפה</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2561"/>
        <source>Timing</source>
        <translation>תזמון</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2715"/>
        <source>Study Number</source>
        <translation>מספר שיעור</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2716"/>
        <source>Study Name</source>
        <translation>שם השיעור</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="839"/>
        <location filename="../settings.cpp" line="1111"/>
        <location filename="../settings.cpp" line="1179"/>
        <location filename="../settings.cpp" line="1338"/>
        <location filename="../settings.cpp" line="1374"/>
        <location filename="../settings.cpp" line="1460"/>
        <source>Language id</source>
        <translation>מזהה שפה</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1010"/>
        <source>Public talk number missing</source>
        <translation>מספר נאום פומבי חסר</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1013"/>
        <source>Public talk subject missing</source>
        <translation>נושא נאום פומבי חסר</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1034"/>
        <source>Public talk is already saved!</source>
        <translation>נאום פומבי כבר נשמרה!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1068"/>
        <source>Public talk added to database</source>
        <translation>נאום פומבי נוספה למסד הנתונים</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1075"/>
        <location filename="../settings.cpp" line="1307"/>
        <location filename="../settings.cpp" line="1546"/>
        <location filename="../settings.cpp" line="1614"/>
        <location filename="../settings.cpp" line="1698"/>
        <location filename="../settings.cpp" line="1794"/>
        <source>Adding failed</source>
        <translation>ההוספה נכשלה</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="613"/>
        <source>Exception</source>
        <translation>חריגה</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="616"/>
        <source>Meeting 1</source>
        <translation>אסיפה 1</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="617"/>
        <source>Meeting 2</source>
        <translation>אסיפה 2</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="580"/>
        <source>Database restored. The program will be restarted.</source>
        <translation>מסד הנתונים שוחזר. התוכנית תופעל מחדש.</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2754"/>
        <source>Remove ALL studies? (Use only to remove invalid data from database)</source>
        <translation>להסיר את כל השיעורים? (השתמש רק בכדי להסיר נתונים לא חוקיים ממסד הנתונים)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1106"/>
        <source>Speaker</source>
        <translation>נואם</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1105"/>
        <source>Id</source>
        <translation>Id</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1108"/>
        <source>Phone</source>
        <translation>טלפון</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="544"/>
        <source>Save database</source>
        <translation>שמור מסד נתונים</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="552"/>
        <source>Database backuped</source>
        <translation>מסד הנתונים מגובה</translation>
    </message>
</context>
<context>
    <name>StartSliderPage1.ui</name>
    <message>
        <location filename="../startup/StartSliderPage1.ui.qml" line="14"/>
        <source>Welcome to theocbase</source>
        <translation>ברוך הבא ל- theocbase</translation>
    </message>
    <message>
        <location filename="../startup/StartSliderPage1.ui.qml" line="38"/>
        <source>Do not show again</source>
        <translation>אל תציג שוב</translation>
    </message>
</context>
<context>
    <name>StudentAssignmentPanel</name>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="71"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="92"/>
        <source>Source</source>
        <translation>מקור</translation>
    </message>
</context>
<context>
    <name>WEMeetingFinalTalkPanel</name>
    <message>
        <location filename="../qml/WEMeetingFinalTalkPanel.qml" line="48"/>
        <source>Final Talk</source>
        <comment>Circuit overseer&apos;s talk in the wekeend meeting</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StudentAssignmentPanel</name>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="112"/>
        <source>Student</source>
        <translation>תלמיד</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="120"/>
        <location filename="../qml/StudentAssignmentPanel.qml" line="171"/>
        <source>All</source>
        <comment>Dropdown column title</comment>
        <translation>הכל</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="161"/>
        <source>Assistant</source>
        <translation>עוזר</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="170"/>
        <source>With Student</source>
        <comment>Dropdown column title</comment>
        <translation>עם תלמיד</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="185"/>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>העוזר לא אמור להיות מישהו מהמין השני.</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="212"/>
        <source>Study point</source>
        <translation>נקודת שיעור</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="233"/>
        <source>Result</source>
        <translation>תוצאה</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="238"/>
        <source>Completed</source>
        <translation>הושלם</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="260"/>
        <source>Volunteer</source>
        <translation>מתנדב</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="309"/>
        <source>Timing</source>
        <translation>תזמון</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="329"/>
        <source>Current Study</source>
        <translation>השיעור הנוכחי</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="345"/>
        <source>Exercise Completed</source>
        <translation>התרגיל הושלם</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="355"/>
        <source>Next Study</source>
        <translation>השיעור הבא</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="378"/>
        <source>Note</source>
        <translation>הערה</translation>
    </message>
</context>
<context>
    <name>TerritoryAddAddressForm.ui</name>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="55"/>
        <source>Address</source>
        <translation>כתובת</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="118"/>
        <source>Street:</source>
        <translation>רחוב:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="128"/>
        <source>Postalcode:</source>
        <translation>מיקוד:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="150"/>
        <source>Search</source>
        <comment>Search address</comment>
        <translation>לחפש</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="165"/>
        <source>Country</source>
        <comment>Short name of country</comment>
        <translation>מדינה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="182"/>
        <source>State</source>
        <comment>Short name of administrative area level 1</comment>
        <translation>מדינה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="200"/>
        <source>County</source>
        <comment>Name of administrative area level 2</comment>
        <translation>מחוז</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="77"/>
        <source>Country:</source>
        <translation>מדינה:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="87"/>
        <source>State:</source>
        <comment>Administrative area level 1</comment>
        <translation>מדינה:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="97"/>
        <source>City:</source>
        <comment>Locality</comment>
        <translation>עיר:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="107"/>
        <source>District:</source>
        <comment>Sublocality, first-order civil entity below a locality</comment>
        <translation>מחוז:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="138"/>
        <source>No.:</source>
        <translation>מס&apos;</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="218"/>
        <source>City</source>
        <comment>Locality</comment>
        <translation>עיר</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="235"/>
        <source>District</source>
        <comment>Sublocality, first-order civil entity below a locality</comment>
        <translation>מחוז</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="253"/>
        <source>Street</source>
        <comment>Streetname</comment>
        <translation>רחוב</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="270"/>
        <source>No.</source>
        <comment>House or street number</comment>
        <translation>מס&apos;</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="287"/>
        <source>Postal code</source>
        <comment>Mail code, ZIP</comment>
        <translation>מיקוד</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="304"/>
        <source>Latitude</source>
        <translation>קו רוחב</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="321"/>
        <source>Longitude</source>
        <translation>קו אורך</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="353"/>
        <source>OK</source>
        <translation>בסדר</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="358"/>
        <source>Cancel</source>
        <translation>לבטל</translation>
    </message>
</context>
<context>
    <name>TerritoryAddStreetForm.ui</name>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="32"/>
        <source>Select the streets to be added to the territory:</source>
        <comment>Add street names to territoy</comment>
        <translation>בחר את הרחובות שיתווספו לשטח:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="41"/>
        <source>search</source>
        <comment>Search in the list of new street names</comment>
        <translation>לחפש</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="70"/>
        <source>Street</source>
        <comment>Street name</comment>
        <translation>רחוב</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="88"/>
        <source>Hide already added streets</source>
        <comment>Hide those streets that are already in the territory&apos;s street list</comment>
        <translation>הסתר רחובות שכבר נוספו</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="104"/>
        <source>OK</source>
        <translation>אשור</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="109"/>
        <source>Cancel</source>
        <translation>לבטל</translation>
    </message>
</context>
<context>
    <name>TerritoryAddressList</name>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="156"/>
        <source>Address</source>
        <translation>כתובת</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="197"/>
        <source>No.</source>
        <comment>House or street number</comment>
        <translation>מס&apos;</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="243"/>
        <source>Name</source>
        <translation>שם</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="302"/>
        <source>Undefined [%1]</source>
        <comment>Undefined territory address type</comment>
        <translation>לא מוגדר [%1]</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="345"/>
        <source>Edit address</source>
        <translation>ערוך כתובת</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="397"/>
        <source>Add address</source>
        <comment>Add address to territory</comment>
        <translation>הוסף כתובת</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="398"/>
        <source>Please select an address in the list of search results.</source>
        <comment>Add address to territory</comment>
        <translation>אנא בחר כתובת ברשימת תוצאות החיפוש.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="457"/>
        <location filename="../qml/TerritoryAddressList.qml" line="467"/>
        <source>Search address</source>
        <comment>Add or edit territory address</comment>
        <translation>כתובת חיפוש</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="458"/>
        <source>No address found.</source>
        <comment>Add or edit territory address</comment>
        <translation>לא נמצאה כתובת.</translation>
    </message>
</context>
<context>
    <name>TerritoryAddressListForm.ui</name>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="39"/>
        <source>ID</source>
        <translation>זיהוי</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="61"/>
        <source>Territory-ID</source>
        <translation>זיהוי-שטח</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="83"/>
        <source>Country</source>
        <comment>Short name of country</comment>
        <translation>מדינה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="106"/>
        <source>State</source>
        <comment>Short name of administrative area level 1</comment>
        <translation>מדינה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="129"/>
        <source>County</source>
        <comment>Name of administrative area level 2</comment>
        <translation>מחוז</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="173"/>
        <source>District</source>
        <comment>Sublocality</comment>
        <translation>מחוז</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="151"/>
        <source>City</source>
        <comment>Locality</comment>
        <translation>עיר</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="196"/>
        <source>Street</source>
        <comment>Street name</comment>
        <translation>רחוב</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="221"/>
        <source>No.</source>
        <comment>House or street number</comment>
        <translation>מס&apos;</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="230"/>
        <source>Postal code</source>
        <comment>Mail code, ZIP</comment>
        <translation>מיקוד</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="253"/>
        <source>Geometry</source>
        <comment>Coordinate geometry of the address</comment>
        <translation>גאומטריה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="275"/>
        <source>Name</source>
        <comment>Name of person or building</comment>
        <translation>שם</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="285"/>
        <source>Type</source>
        <comment>Type of address</comment>
        <translation>סוג</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="310"/>
        <source>Add new address</source>
        <translation>הוסף כתובת חדשה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="321"/>
        <source>Edit selected address</source>
        <translation>ערוך את הכתובת שנבחרה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="332"/>
        <source>Remove selected address</source>
        <translation>הסר את הכתובת שנבחרה</translation>
    </message>
</context>
<context>
    <name>TerritoryImportForm.ui</name>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="78"/>
        <source>Filename:</source>
        <translation>שם קובץ:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="133"/>
        <source>Match KML Fields</source>
        <comment>Choose which data fields in kml-file correspond to which territory properties</comment>
        <translation>התאם שדות KML</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="55"/>
        <source>Territory boundaries</source>
        <comment>Territory data import option</comment>
        <translation>גבולות שטח</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="63"/>
        <source>Addresses</source>
        <comment>Territory data import option</comment>
        <translation>כתובות</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="163"/>
        <source>Description:</source>
        <comment>Territory data import KML Field</comment>
        <translation>תיאור:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="180"/>
        <source>Search by &quot;Description&quot; if territory is not found by &quot;Name&quot;</source>
        <translation>חפש לפי &quot;תיאור&quot; אם השטח לא נמצא לפי &quot;שם&quot;</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="195"/>
        <source>Territory No.</source>
        <comment>Territory number</comment>
        <translation>שטח מס&apos;:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="201"/>
        <source>Locality</source>
        <comment>Territory locality</comment>
        <translation>מקום</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="207"/>
        <source>Remark</source>
        <translation>הערה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="238"/>
        <source>Match Fields</source>
        <comment>Choose which fields in the import-file correspond to the address data</comment>
        <translation>התאמת שדות</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="255"/>
        <source>Address:</source>
        <comment>Territory address import field</comment>
        <translation>כתובת:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="266"/>
        <source>Name:</source>
        <comment>Territory address import field</comment>
        <translation>שם:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="304"/>
        <source>Address type:</source>
        <comment>Address type for territory address import</comment>
        <translation>סוג כתובת:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="326"/>
        <source>Output filename for failed addresses:</source>
        <comment>Territory address import</comment>
        <translation>פלט שם קובץ לכתובות שנכשלו:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="396"/>
        <source>Import</source>
        <translation>יבוא</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="405"/>
        <source>Close</source>
        <translation>סגור</translation>
    </message>
</context>
<context>
    <name>TerritoryManagement</name>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="134"/>
        <source>Group by:</source>
        <translation>קבץ לפי:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="141"/>
        <source>City</source>
        <comment>Group territories by city</comment>
        <translation>עִיר</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="143"/>
        <source>Type</source>
        <comment>Group territories by type of the territory</comment>
        <translation>סוג</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="144"/>
        <source>Worked through</source>
        <comment>Group territories by time frame they have been worked through</comment>
        <translation>עבד</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="244"/>
        <source>Add new territory</source>
        <translation>הוסף שטח חדש</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="259"/>
        <source>Remove selected territory</source>
        <translation>הסר שטח שנבחרה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="355"/>
        <source>Remark</source>
        <translation>הערה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="374"/>
        <source>Type:</source>
        <comment>Territory-type that is used to group territories</comment>
        <translation>סוג:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="401"/>
        <source>City:</source>
        <comment>Cityname that is used to group territories</comment>
        <translation>עיר:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="466"/>
        <source>Assignments</source>
        <translation>משימות</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="662"/>
        <source>Publisher</source>
        <translation>מבשר/ת</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="796"/>
        <source>Streets</source>
        <translation>רחובות</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="835"/>
        <source>Addresses</source>
        <translation>כתובות</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="300"/>
        <source>No.:</source>
        <translation>מס&apos;:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="311"/>
        <source>Number</source>
        <translation>מספר</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="322"/>
        <source>Locality:</source>
        <translation>מקום:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="332"/>
        <source>Locality</source>
        <translation>מקום</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="436"/>
        <source>Map</source>
        <comment>Territory map</comment>
        <translation>מפה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="634"/>
        <source>ID</source>
        <translation>זיהוי</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="648"/>
        <source>Publisher-ID</source>
        <translation>מבשר-זיהוי</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="717"/>
        <source>Checked out</source>
        <comment>Date when the publisher obtained the territory</comment>
        <translation>לקיחת</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="724"/>
        <source>Checked back in</source>
        <comment>Date when the territory is returned</comment>
        <translation>החזרת</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="747"/>
        <source>Add new assignment</source>
        <translation>הוסף משימה חדשה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="773"/>
        <source>Remove selected assignment</source>
        <translation>הסר את המשימה שנבחרה</translation>
    </message>
</context>
<context>
    <name>TerritoryMap</name>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="398"/>
        <source>Search address</source>
        <comment>Add or edit territory address</comment>
        <translation>כתובת חיפוש</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/TerritoryMap.qml" line="851"/>
        <source>The new boundary overlaps %n territory(ies):</source>
        <comment>Add or edit territory boundary</comment>
        <translation>
            <numerusform>הגבול החדש חופף את %n שטח:</numerusform>
            <numerusform>הגבול החדש חופף את %n השטחים:</numerusform>
            <numerusform>הגבול החדש חופף את %n השטחים:</numerusform>
            <numerusform>הגבול החדש חופף את %n השטחים:</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="853"/>
        <source>Do you want to assign overlapping areas to the current territory?
Select &#x27;No&#x27; if overlapping areas should remain in their territories and to add only the part, that doesn&#x27;t overlap other territories.</source>
        <comment>Add or edit territory boundary</comment>
        <translation>האם ברצונך להקצות שטחים חופפים לשטח הנוכחי?
בחר &apos;לא&apos; אם אזורים חופפים צריכים להישאר בשטחים שלהם ולהוסיף רק את החלק, שאינו חופף לשטחים אחרים.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="887"/>
        <source>Join to the selected territory</source>
        <comment>Join two territories into one</comment>
        <translation>הצטרף לשטח שנבחרה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="916"/>
        <source>Add address to selected territory</source>
        <translation>הוסף כתובת לשטח שנבחרה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="924"/>
        <source>Assign to selected territory</source>
        <comment>Reassign territory address</comment>
        <translation>הקצה לשטח שנבחרה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="932"/>
        <source>Remove address</source>
        <comment>Delete territory address</comment>
        <translation>הסר כתובת</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1022"/>
        <source>%1 of %2 address(es) imported.</source>
        <comment>Territory address import progress</comment>
        <translation>%1 מכתובת/ות %2 מיובאות.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1037"/>
        <source>Import territory boundaries</source>
        <comment>Territory import dialog</comment>
        <translation>יבוא גבולות שטח</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1047"/>
        <location filename="../qml/TerritoryMap.qml" line="1058"/>
        <location filename="../qml/TerritoryMap.qml" line="1073"/>
        <location filename="../qml/TerritoryMap.qml" line="1082"/>
        <location filename="../qml/TerritoryMap.qml" line="1115"/>
        <location filename="../qml/TerritoryMap.qml" line="1124"/>
        <source>Import territory data</source>
        <comment>Territory import dialog</comment>
        <translation>ייבא נתוני שטחים</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/TerritoryMap.qml" line="1048"/>
        <source>%n territory(ies) imported or updated.</source>
        <comment>Number of territories imported or updated</comment>
        <translation>
            <numerusform>%n שטח מיובא או מעודכן.</numerusform>
            <numerusform>%n שטחים מיובאים או מעודכנים.</numerusform>
            <numerusform>%n שטחים מיובאים או מעודכנים.</numerusform>
            <numerusform>%n שטחים מיובאים או מעודכנים.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1074"/>
        <source>Please select the address and name fields.</source>
        <comment>Fields for territory address import from file</comment>
        <translation>אנא בחר את שדות הכתובת והשם.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1083"/>
        <source>The selected fields should be different.</source>
        <comment>Fields for territory address import from file</comment>
        <translation>השדות שנבחרו צריכים להיות שונים.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1091"/>
        <source>Import territory addresses</source>
        <comment>Territory import dialog</comment>
        <translation>ייבא כתובות שטחים</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1092"/>
        <source>The addresses will be added to the current territory. Please select a territory first.</source>
        <comment>Territory address import from file</comment>
        <translation>הכתובות יתווספו לשטח הנוכחי. אנא בחר שטח תחילה.</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/TerritoryMap.qml" line="1116"/>
        <source>%n address(es) imported.</source>
        <comment>Number of addresses imported</comment>
        <translation>
            <numerusform>%n כתובת מיובא.</numerusform>
            <numerusform>%n כתובות מיובאות.</numerusform>
            <numerusform>%n כתובות מיובאות.</numerusform>
            <numerusform>%n כתובות מיובאות.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1126"/>
        <source>No valid territory selected.</source>
        <comment>Territory boundary import</comment>
        <translation>לא נבחרה שטח חוקית.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1128"/>
        <source>The import file could not be read.</source>
        <comment>Territory boundary import</comment>
        <translation>לא ניתן היה לקרוא את קובץ הייבוא.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1160"/>
        <source>Address</source>
        <comment>Default Address-field for territory address import</comment>
        <translation>כתובת</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1162"/>
        <source>Name</source>
        <comment>Default Name-field for territory address import</comment>
        <translation>שם</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1191"/>
        <location filename="../qml/TerritoryMap.qml" line="1199"/>
        <source>Open file</source>
        <translation>קובץ פתוח</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1192"/>
        <location filename="../qml/TerritoryMap.qml" line="1193"/>
        <source>KML files (*.kml)</source>
        <comment>Filedialog pattern</comment>
        <translation>קבצי KML (* .kml)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1192"/>
        <location filename="../qml/TerritoryMap.qml" line="1200"/>
        <location filename="../qml/TerritoryMap.qml" line="1208"/>
        <source>All files (*)</source>
        <comment>Filedialog pattern</comment>
        <translation>כל הקבצים (*)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1200"/>
        <location filename="../qml/TerritoryMap.qml" line="1201"/>
        <location filename="../qml/TerritoryMap.qml" line="1208"/>
        <location filename="../qml/TerritoryMap.qml" line="1209"/>
        <source>CSV files (*.csv)</source>
        <comment>Filedialog pattern</comment>
        <translation>קבצי CSV (* .csv)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1200"/>
        <location filename="../qml/TerritoryMap.qml" line="1208"/>
        <source>Text files (*.txt)</source>
        <comment>Filedialog pattern</comment>
        <translation>קבצי טקסט (* .txt)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1207"/>
        <source>Save file</source>
        <translation>שמור את הקובץ</translation>
    </message>
</context>
<context>
    <name>TerritoryMapForm.ui</name>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="66"/>
        <source>search</source>
        <comment>Search address in territory map</comment>
        <translation>לחפש</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="152"/>
        <source>Import data</source>
        <comment>Import territory data from a file</comment>
        <translation>ייבא נתונים</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="196"/>
        <source>Show/hide streets</source>
        <comment>Show/hide streets of territories</comment>
        <translation>הראה / הסתיר רחובות</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="208"/>
        <source>Switch edit mode</source>
        <comment>Switch edit mode on territory map</comment>
        <translation>החלף מצב עריכה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="222"/>
        <source>Create boundary</source>
        <comment>Create a new boundary for the territory</comment>
        <translation>צור גבול</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="234"/>
        <source>Remove boundary</source>
        <comment>Remove boundary or geometry of the territory</comment>
        <translation>הסר את הגבול</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="246"/>
        <source>Split territory</source>
        <comment>Cut territory in two parts</comment>
        <translation>פיצול שטח</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="163"/>
        <source>Zoom full</source>
        <comment>Zoom full to display all territories</comment>
        <translation>זום מלא</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="173"/>
        <source>Show/hide territories</source>
        <comment>Show/hide boundaries of territories</comment>
        <translation>הצג / הסתר שטחים</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="184"/>
        <source>Show/hide markers</source>
        <comment>Show/hide address markers of territories</comment>
        <translation>הצג / הסתר סמנים</translation>
    </message>
</context>
<context>
    <name>TerritoryStreetList</name>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="102"/>
        <source>Street name</source>
        <translation>שם רחוב</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="210"/>
        <source>Undefined [%1]</source>
        <comment>Undefined street type</comment>
        <translation>לא מוגדר [%1]</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="278"/>
        <location filename="../qml/TerritoryStreetList.qml" line="289"/>
        <source>Add streets</source>
        <comment>Add streets to a territory</comment>
        <translation>הוסף רחובות</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="279"/>
        <source>No streets found.</source>
        <comment>Add streets to a territory</comment>
        <translation>לא נמצאו רחובות.</translation>
    </message>
</context>
<context>
    <name>TerritoryStreetListForm.ui</name>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="38"/>
        <source>ID</source>
        <translation>זהוי</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="60"/>
        <source>Territory-ID</source>
        <translation>זהוי-שטח</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="82"/>
        <source>Street name</source>
        <translation>שם רחוב</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="91"/>
        <source>From number</source>
        <comment>Number range of addresses in the street</comment>
        <translation>ממספר</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="100"/>
        <source>To number</source>
        <comment>Number range of addresses in the street</comment>
        <translation>למספר</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="109"/>
        <source>Quantity</source>
        <comment>Quantity of addresses in the street</comment>
        <translation>כמות</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="119"/>
        <source>Type</source>
        <comment>Type of street</comment>
        <translation>זוג</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="128"/>
        <source>Geometry</source>
        <comment>Line geometry of the street</comment>
        <translation>גאומטריה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="166"/>
        <source>Add new street</source>
        <translation>הוסף רחוב חדש</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="177"/>
        <source>Remove selected street</source>
        <translation>הסר את הרחוב שנבחר</translation>
    </message>
</context>
<context>
    <name>TerritoryTreeModel</name>
    <message>
        <location filename="../territorymanagement.cpp" line="118"/>
        <source>&lt; 6 months</source>
        <comment>territory worked</comment>
        <translation>&lt;6 חודשים</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="119"/>
        <source>6 to 12 months</source>
        <comment>territory worked</comment>
        <translation>6 עד 12 חודשים</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="120"/>
        <source>&gt; 12 months ago</source>
        <comment>territory worked</comment>
        <translation>לפני 12 חודשים</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="303"/>
        <source>Not worked</source>
        <comment>Group text for territories that are not worked yet.</comment>
        <translation>לא עבד</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="305"/>
        <location filename="../territorymanagement.cpp" line="457"/>
        <location filename="../territorymanagement.cpp" line="468"/>
        <source>Not assigned</source>
        <comment>Value of the field, the territories are grouped by, is empty.</comment>
        <translation>לא הוקצה</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="316"/>
        <location filename="../territorymanagement.cpp" line="343"/>
        <location filename="../territorymanagement.cpp" line="485"/>
        <source>territory</source>
        <translation>שטח</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="316"/>
        <location filename="../territorymanagement.cpp" line="343"/>
        <location filename="../territorymanagement.cpp" line="485"/>
        <source>territories</source>
        <translation>שטחים</translation>
    </message>
</context>
<context>
    <name>TodoPanel</name>
    <message>
        <location filename="../qml/TodoPanel.qml" line="114"/>
        <source>To Do List</source>
        <translation>רשימת משימות</translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="209"/>
        <source>Cannot schedule this item until these fields are fixed: %1</source>
        <translation>לא ניתן לתזמן פריט זה עד לתיקון שדות אלה:%1</translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="284"/>
        <source>Incoming</source>
        <comment>incoming speaker</comment>
        <translation>נכנס</translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="284"/>
        <source>Outgoing</source>
        <comment>outgoing speaker</comment>
        <translation>יוצא</translation>
    </message>
</context>
<context>
    <name>ValidationTextInput</name>
    <message>
        <location filename="../qml/ValidationTextInput.qml" line="10"/>
        <source>Text</source>
        <translation>טקסט</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <location filename="../qml/WEMeetingChairmanPanel.qml" line="78"/>
        <source>Song</source>
        <translation>שיר</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingChairmanPanel.qml" line="50"/>
        <source>Chairman</source>
        <translation>יושב ראש</translation>
    </message>
</context>
<context>
    <name>WEMeetingFinalTalkPanel</name>
    <message>
        <location filename="../qml/WEMeetingFinalTalkPanel.qml" line="48"/>
        <source>Service Talk</source>
        <comment>Circuit overseer&apos;s talk in the wekeend meeting</comment>
        <translation>נאום השירות</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingFinalTalkPanel.qml" line="66"/>
        <source>Speaker</source>
        <translation>נואם</translation>
    </message>
</context>
<context>
    <name>WEMeetingModule</name>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="100"/>
        <location filename="../qml/WEMeetingModule.qml" line="120"/>
        <source>Weekend Meeting</source>
        <translation>אסיפת סוף השבוע</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="116"/>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation>הערות</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="132"/>
        <location filename="../qml/WEMeetingModule.qml" line="337"/>
        <source>Song and Prayer</source>
        <translation>שיר ותפילה</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="133"/>
        <location filename="../qml/WEMeetingModule.qml" line="338"/>
        <source>Song %1 and Prayer</source>
        <translation>שיר %1 ותפילה</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="167"/>
        <source>PUBLIC TALK</source>
        <translation>נאום פומבי</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="184"/>
        <source>Send to To Do List</source>
        <translation>שלח לרשימת המטלות</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="190"/>
        <source>Move to different week</source>
        <translation>עוברים לשבוע אחר</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="199"/>
        <source>Clear Public Talk selections</source>
        <translation>נקה את הבחירות בנאום פומבי</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="248"/>
        <source>WATCHTOWER STUDY</source>
        <translation>שיעור המצפה</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="257"/>
        <source>Song %1</source>
        <translation>שיר %1</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="278"/>
        <source>Import WT...</source>
        <translation>ייבא WT ...</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="306"/>
        <source>Conductor</source>
        <translation>מדריך</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="307"/>
        <source>Reader</source>
        <translation>קרין</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <location filename="../qml/WatchtowerSongPanel.qml" line="39"/>
        <source>Prayer</source>
        <translation>תפילה</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerSongPanel.qml" line="66"/>
        <source>Song</source>
        <translation>שיר</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="51"/>
        <source>Watchtower Issue</source>
        <translation>הוצאה המצפה</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="77"/>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>מאמר</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="103"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="129"/>
        <source>Conductor</source>
        <translation>מדריך</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="154"/>
        <source>Reader</source>
        <translation>קרין</translation>
    </message>
</context>
<context>
    <name>cPersonComboBox</name>
    <message>
        <location filename="../cpersoncombobox.cpp" line="238"/>
        <source>Brother has other meeting parts</source>
        <translation>לאח יש חלקי אסיפה אחרים</translation>
    </message>
    <message>
        <location filename="../cpersoncombobox.cpp" line="233"/>
        <source>Person unavailable</source>
        <translation>האדם לא זמין</translation>
    </message>
    <message>
        <location filename="../cpersoncombobox.cpp" line="228"/>
        <source>Outgoing speaker</source>
        <translation>נואם יוצא</translation>
    </message>
</context>
<context>
    <name>ccongregation</name>
    <message>
        <location filename="../ccongregation.cpp" line="118"/>
        <source>(Missing Record)</source>
        <comment>database is now missing this entry</comment>
        <translation>(חסר תיעוד)</translation>
    </message>
</context>
<context>
    <name>checkupdates</name>
    <message>
        <location filename="../checkupdates.cpp" line="100"/>
        <source>New version available</source>
        <translation>גרסה חדשה זמינה</translation>
    </message>
    <message>
        <location filename="../checkupdates.cpp" line="102"/>
        <source>Download</source>
        <translation>הורד</translation>
    </message>
    <message>
        <location filename="../checkupdates.cpp" line="105"/>
        <source>New version...</source>
        <translation>גרסה חדשה...</translation>
    </message>
    <message>
        <location filename="../checkupdates.cpp" line="116"/>
        <source>No new update available</source>
        <translation>אין עדכון חדש זמין</translation>
    </message>
</context>
<context>
    <name>csync</name>
    <message>
        <location filename="../csync.cpp" line="112"/>
        <source>File reading failed</source>
        <translation>קריאת הקבצים נכשלה</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="450"/>
        <source>XML file generated in the wrong version.</source>
        <translation>קובץ XML שנוצר בגרסה הלא נכונה.</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="517"/>
        <source>Persons - added </source>
        <translation>אנשים - נוסף</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="555"/>
        <source>Persons - updated </source>
        <translation>אנשים - עודכן</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="586"/>
        <source>Theocratic ministry school - schedule added </source>
        <translation>בית ספר התיאוקרטי - נוספה לוח זמנים </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="588"/>
        <source>Theocratic ministry school - schedule updated </source>
        <translation>בית ספר התיאוקרטי - לוח הזמנים עודכן </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="784"/>
        <source>Theocratic Ministry School - Study updated</source>
        <translation>בית הספר התיאוקרטי - עדכון השיעור</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="798"/>
        <source>Theocratic Ministry School - Study added</source>
        <translation>בית ספר התאוקרטי - הוסיף שיעור</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="834"/>
        <source>Public talks - theme added </source>
        <translation>נומים פובים - הנושא נוסף </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="892"/>
        <source>Public Talk and WT - schedule updated </source>
        <translation>נאום פומבי ושיור המצפה - לוח הזמנים עודכן </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="898"/>
        <source>Public Talk and WT - schedule added </source>
        <translation>נאום פומבי ושיור המצפה - לוח זמנים נוסף </translation>
    </message>
</context>
<context>
    <name>cterritories</name>
    <message>
        <location filename="../cterritories.cpp" line="501"/>
        <source>Do not call</source>
        <comment>Territory address type</comment>
        <translation>אל תתקשר</translation>
    </message>
    <message>
        <location filename="../cterritories.cpp" line="1885"/>
        <source>Save failed addresses</source>
        <comment>Territory address import</comment>
        <translation>שמור כתובות שנכשלו</translation>
    </message>
    <message>
        <location filename="../cterritories.cpp" line="1886"/>
        <source>The file is in read only mode</source>
        <comment>Save failed addresses in territory data import</comment>
        <translation>הקובץ נמצא במצב קריאה בלבד</translation>
    </message>
</context>
<context>
    <name>family</name>
    <message>
        <location filename="../family.cpp" line="124"/>
        <source>Not set</source>
        <translation>לא מוכן</translation>
    </message>
</context>
<context>
    <name>googleMediator</name>
    <message>
        <location filename="../googlemediator.cpp" line="198"/>
        <source>OK</source>
        <translation>אשור</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="200"/>
        <source>OK but JSON not available</source>
        <translation>בסדר אבל JSON לא זמין</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="202"/>
        <source>Authorizing</source>
        <translation>מאשר</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="204"/>
        <source>Authorization Failed</source>
        <translation>ההרשאה נכשלה</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="210"/>
        <source>Missing Client ID</source>
        <translation>חסר מזהה לקוח</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="212"/>
        <source>Missing Client Secret</source>
        <translation>חסר סוד לקוח</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="214"/>
        <source>Need Authorization Code</source>
        <translation>צריך קוד אישור</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="216"/>
        <source>Need Token Refresh</source>
        <translation>צריך רענון אסימון</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <location filename="../historytable.ui" line="196"/>
        <source>Number of weeks after selected date</source>
        <translation>מספר שבועות לאחר התאריך שנבחר</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="246"/>
        <source>Number of weeks to gray after an assignment</source>
        <translation>מספר שבועות לאפור לאחר משימה</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="220"/>
        <source>weeks</source>
        <translation>שבועות</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="122"/>
        <source>Timeline</source>
        <translation>ציר זמן</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="180"/>
        <source>Number of weeks before selected date</source>
        <translation>מספר שבועות לפני התאריך שנבחר</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="410"/>
        <source>Publishers</source>
        <comment>History Table (column title)</comment>
        <translation>מבשר</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="577"/>
        <location filename="../historytable.cpp" line="584"/>
        <source>CBS</source>
        <translation>CBS</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="577"/>
        <location filename="../historytable.cpp" line="679"/>
        <source>C</source>
        <comment>abbreviation of the &apos;conductor&apos;</comment>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="584"/>
        <location filename="../historytable.cpp" line="609"/>
        <source>R</source>
        <comment>abbreviation of the &apos;reader&apos;</comment>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="606"/>
        <source>H</source>
        <comment>abbreviation of the &apos;highlights&apos;</comment>
        <translation>H</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="618"/>
        <source>TMS</source>
        <translation>TMS</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="740"/>
        <source>SM</source>
        <translation>SM</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="758"/>
        <source>PT</source>
        <comment>abbreviation of &apos;public talk&apos; for history table</comment>
        <translation>PT</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="764"/>
        <source>Ch</source>
        <comment>abbreviation of public talk &apos;chairman&apos; for history table</comment>
        <translation>Ch</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="773"/>
        <source>WT-R</source>
        <comment>abbreviation of &apos;watchtower reader&apos; for history table</comment>
        <translation>WT-R</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="990"/>
        <source>Date</source>
        <translation>תאריך</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="991"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="992"/>
        <source>Speaker</source>
        <translation>נואם</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="993"/>
        <source>Congregation</source>
        <translation>קהילה</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="994"/>
        <source>Chairman</source>
        <translation>יושב ראש</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="995"/>
        <source>Reader</source>
        <translation>קרין</translation>
    </message>
</context>
<context>
    <name>importwizard</name>
    <message>
        <location filename="../importwizard.cpp" line="40"/>
        <source>Next &gt;</source>
        <translation>הבא&gt;</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="39"/>
        <source>&lt; Back</source>
        <translation>&lt;חזרה</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="41"/>
        <source>Cancel</source>
        <translation>לבטל</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="67"/>
        <source>Theocratic school schedule import. Copy full schedule from WTLibrary and paste below (Ctrl + V / cmd + V)</source>
        <translation>ייבוא לוח הזמנים של בתי הספר התיאוקרטיים. העתק את לוח הזמנים המלא מ- WTLibrary והדבק למטה (Ctrl + V / cmd + V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="68"/>
        <source>Check schedule</source>
        <translation>בדוק את לוח הזמנים</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="71"/>
        <source>Studies import. Copy studies from WTLibrary and paste below (Ctrl + V / cmd + V)</source>
        <translation>יבוא שיעורים. העתק מחקרים מ- WTLibrary והדבק למטה (Ctrl + V / cmd + V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="72"/>
        <source>Check studies</source>
        <translation>לבדוק שיעורים</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="75"/>
        <source>Settings import. Copy settings from WTLibrary and paste below (Ctrl + V / cmd + V)</source>
        <translation>ייבוא הגדרות. העתק הגדרות מ- WTLibrary והדבק למטה (Ctrl + V / cmd + V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="76"/>
        <source>Check settings</source>
        <translation>בדוק הגדרות</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="80"/>
        <source>Check subjects</source>
        <translation>בדוק נושאים</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="83"/>
        <source>Add speakers and congregations. Copy all data to clipboard and paste below (Ctrl + V)</source>
        <translation>הוסף נואמים וקהילות. העתק את כל הנתונים ללוח והדבק למטה (Ctrl + V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="84"/>
        <source>Check data</source>
        <translation>בדוק נתונים</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="88"/>
        <source>Check songs</source>
        <translation>בדוק שירים</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="100"/>
        <source>No schedule to import.</source>
        <translation>אין לוח זמנים לייבוא.</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="142"/>
        <source>Date</source>
        <translation>תאריך</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="143"/>
        <source>Please add start date YYYY-MM-DD (eg. 2011-01-03)</source>
        <translation>אנא הוסף תאריך התחלה YYYY-MM-DD (למשל, 2011-01-03)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="150"/>
        <source>The date is not first day of week (Monday)</source>
        <translation>התאריך אינו היום הראשון בשבוע (שני)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="203"/>
        <source>Import songs</source>
        <translation>ייבא שירים</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="245"/>
        <source>Only brothers</source>
        <translation>רק אחים</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="476"/>
        <source>Subject</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="519"/>
        <source>id</source>
        <translation>id</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="522"/>
        <source>Congregation</source>
        <translation>קהילה</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="525"/>
        <source>Speaker</source>
        <translation>נואם</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="528"/>
        <source>Phone</source>
        <translation>טלפון</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="531"/>
        <source>Public talks</source>
        <translation>נאומים פומבים</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="534"/>
        <source>Language</source>
        <translation>שפה</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="551"/>
        <location filename="../importwizard.cpp" line="552"/>
        <source>First name</source>
        <translation>שם פרטי</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="551"/>
        <location filename="../importwizard.cpp" line="552"/>
        <source>Last name</source>
        <translation>שם משפחה</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="179"/>
        <source>Import subjects</source>
        <translation>יבוא נושאים</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="180"/>
        <location filename="../importwizard.cpp" line="204"/>
        <source>Choose language</source>
        <translation>בחר שפה</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="38"/>
        <source>Save to database</source>
        <translation>שמור למסד הנתונים</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="79"/>
        <source>Add public talk&#x27;s subjects. Copy themes and paste below (Ctrl + V / cmd + V).
Number should be in the first column and theme in the second.</source>
        <translation>הוסף את נושאי הנאום פומבי. העתק נושאים והדבק למטה (Ctrl + V / cmd + V).
המספר צריך להיות בעמודה הראשונה והנושא בשנייה.</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="87"/>
        <source>Add songs. Copy all data to clipboard and paste below (Ctrl + V / cmd + V).
Number should be in the first column and theme in the second.</source>
        <translation>הוסף שירים. העתק את כל הנתונים ללוח והדבק למטה (Ctrl + V / cmd + V).
המספר צריך להיות בעמודה הראשונה והנושא בשנייה.</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="234"/>
        <source>date</source>
        <translation>תאריך</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="237"/>
        <source>number</source>
        <translation>מספר</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="240"/>
        <source>subject</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="243"/>
        <source>material</source>
        <translation>חומר</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="354"/>
        <source>setting</source>
        <translation>הגדרה</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="416"/>
        <source>study</source>
        <translation>שיעור</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="602"/>
        <source>Title</source>
        <translation>כותרת</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="748"/>
        <source>A public talk with the same number is already saved!
Do you want to discontinue the previous talk?

Scheduled talks will be moved to the To Do List.</source>
        <translation>נאום פומבית עם אותו מספר כבר נשמרת!
האם אתה רוצה להפסיק את הנאום הקודמת?
 
נאום מתוזמנות יועברו לרשימת המטלות.</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="751"/>
        <source>Previous talk: </source>
        <translation>נאום קודם: </translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="752"/>
        <source>New talk: </source>
        <translation>נאום חדש: </translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="873"/>
        <source>Public talk themes not found. Add themes and try again!</source>
        <translation>נושא של נאום פומבי לא נמצאו. הוסף נושא ונסה שוב!</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="900"/>
        <source> rows added</source>
        <translation> הוסיפו שורות</translation>
    </message>
</context>
<context>
    <name>lmmWorksheetRegEx</name>
    <message>
        <location filename="../lmmworksheetregex.ui" line="203"/>
        <source>Language Options</source>
        <translation>אפשרויות שפה</translation>
    </message>
</context>
<context>
    <name>lmmtalktypeedit</name>
    <message>
        <location filename="../lmmtalktypeedit.cpp" line="14"/>
        <source>Talk Name in the Workbook</source>
        <translation>שם הנאום בגיליון פעילות</translation>
    </message>
    <message>
        <location filename="../lmmtalktypeedit.cpp" line="16"/>
        <source>Meeting Item</source>
        <translation>פריט אסיפה</translation>
    </message>
    <message>
        <location filename="../lmmtalktypeedit.cpp" line="42"/>
        <source>Unknown</source>
        <comment>Unknown talk name</comment>
        <translation>לא ידוע</translation>
    </message>
    <message>
        <location filename="../lmmtalktypeedit.ui" line="14"/>
        <source>Talk Type Editor</source>
        <comment>dialog name</comment>
        <translation>עורך סוג השיחה</translation>
    </message>
</context>
<context>
    <name>logindialog</name>
    <message>
        <location filename="../logindialog.ui" line="33"/>
        <source>Username</source>
        <translation>שם משתמש</translation>
    </message>
    <message>
        <location filename="../logindialog.ui" line="50"/>
        <source>Password</source>
        <translation>סיסמה</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="317"/>
        <source>Select an assignment on the left to edit</source>
        <translation>בחר משימה משמאל לעריכה</translation>
    </message>
</context>
<context>
    <name>personsui</name>
    <message>
        <location filename="../personsui.ui" line="631"/>
        <source>Sister</source>
        <translation>אחות</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="619"/>
        <source>Brother</source>
        <translation>אח</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="645"/>
        <source>Servant</source>
        <translation>משרת</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="23"/>
        <source>Publishers</source>
        <translation>מבשרים</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="811"/>
        <source>General</source>
        <translation>כללי</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1133"/>
        <source>Assistant</source>
        <translation>עוזר</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1246"/>
        <source>Public talks</source>
        <translation>נאומים פומבים</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="958"/>
        <location filename="../personsui.ui" line="1239"/>
        <source>Chairman</source>
        <translation>יושב ראש</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1219"/>
        <source>Weekend Meeting</source>
        <translation>אסיפת סוף השבוע</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1303"/>
        <source>Watchtower reader</source>
        <translation>קרין המצפה</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="657"/>
        <source>Prayer</source>
        <translation>תפלה</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1177"/>
        <source>Cong. Bible Study reader</source>
        <translation>קרין שיעור המקרה הקהילתי</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1332"/>
        <source>Meeting for field ministry</source>
        <translation>מפגשי השירות</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="368"/>
        <location filename="../personsui.cpp" line="561"/>
        <source>First name</source>
        <translation>שם פרטי</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="404"/>
        <location filename="../personsui.cpp" line="560"/>
        <source>Last name</source>
        <translation>שם משפחה</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="535"/>
        <source>Phone</source>
        <translation>טלפון</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="599"/>
        <source>E-mail</source>
        <translation>דוא&apos;ל</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1388"/>
        <source>Assignment</source>
        <translation>משימה</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1505"/>
        <source>Current Study</source>
        <translation>השיעור הנוכחי</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1773"/>
        <location filename="../personsui.ui" line="1796"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1269"/>
        <source>Watchtower Study Conductor</source>
        <translation>מדריך שיעור המצפה</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="669"/>
        <source>Family Head</source>
        <translation>ראש משפחה</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="681"/>
        <source>Family member linked to</source>
        <translation>בן משפחה מקושר ל</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="921"/>
        <source>Midweek Meeting</source>
        <translation>אסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1092"/>
        <source>Only Auxiliary Classes</source>
        <translation>רק כיתות משניות</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1065"/>
        <source>Only Main Class</source>
        <translation>כיתה ראשונה בלבד</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="711"/>
        <source>Active</source>
        <translation>פעיל</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="982"/>
        <source>Treasures From God&#x27;s Word</source>
        <translation>אוצרות מדבר־אלוהים</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1037"/>
        <source>Initial Call</source>
        <translation>שיחה ראשונה</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="996"/>
        <source>Bible Reading</source>
        <translation>קריאת המקרא</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1099"/>
        <source>All Classes</source>
        <translation>כל הכיתות</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1119"/>
        <source>Bible Study</source>
        <translation>שיעור מקרא</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1170"/>
        <source>Congregation Bible Study</source>
        <translation>שיעור המקרא הקהילתי</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1163"/>
        <source>Living as Christians Talks</source>
        <translation>נאומים של לחיות כמשיחיים</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1112"/>
        <source>Return Visit</source>
        <translation>ביקור חוזר</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="295"/>
        <source>Personal Info</source>
        <translation>מידע אישי</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="723"/>
        <source>Host for Public Speakers</source>
        <translation>מארח לנואמים פומבים</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="471"/>
        <source>Mobile</source>
        <translation>נייד</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="780"/>
        <source>Details</source>
        <translation>פרטים</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="989"/>
        <source>Spiritual Gems</source>
        <translation>פנינים רוחניות</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1126"/>
        <source>Talk</source>
        <translation>נאום</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1143"/>
        <source>Discussion with Video</source>
        <translation>דיון עם וידאו</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1344"/>
        <source>History</source>
        <translation>היסטוריה</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1378"/>
        <source>date</source>
        <translation>תאריך</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1383"/>
        <source>no</source>
        <translation>מס&apos;</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1393"/>
        <source>Note</source>
        <translation>הערה</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1398"/>
        <source>Time</source>
        <translation>זמן</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1403"/>
        <source>Together</source>
        <comment>The column header text to show partner in student assignment</comment>
        <translation>יחד</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1412"/>
        <source>Studies</source>
        <translation>שיעורים</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1477"/>
        <source>Study</source>
        <translation>שיעור</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1482"/>
        <source>Date assigned</source>
        <translation>תאריך הוקצה</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1487"/>
        <source>Exercises</source>
        <translation>תרגילים</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1492"/>
        <source>Date completed</source>
        <translation>התאריך הושלם</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1672"/>
        <source>Unavailable</source>
        <translation>אינו זמין</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1706"/>
        <source>Start</source>
        <translation>התחלה</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1711"/>
        <source>End</source>
        <translation>סוֹף</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="131"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="181"/>
        <location filename="../personsui.ui" line="1642"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="390"/>
        <source>A person with the same name already exists: &#x27;%1&#x27;. Do you want to change the name?</source>
        <translation>אדם עם אותו שם כבר קיים: &apos;%1&apos;. האם אתה רוצה לשנות את השם?</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="611"/>
        <source>Remove student?</source>
        <translation>להסיר את התלמיד?</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="588"/>
        <source>Remove student</source>
        <translation>הסר את התלמיד</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="413"/>
        <source>%1 is scheduled for public talks! These talks will
be moved to the To Do List if you remove him as speaker.
Remove him as speaker?</source>
        <translation>%1 מתוכנן לנאום פומבי! הנאומים האלה כן
תועבר לרשימת המטלות אם תסיר אותו כנואם.
להסיר אותו כנואם?</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="602"/>
        <source>%1 is scheduled for public talks! These talks will
be moved to the To Do List if you remove the student.</source>
        <translation>%1 מתוכנן לנאום פומבי! השיחות האלה כן
תועבר לרשימת המטלות אם תסיר את התלמיד.</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="1085"/>
        <source>Remove study</source>
        <translation>הסר את השיעור</translation>
    </message>
</context>
<context>
    <name>printui</name>
    <message>
        <location filename="../printui.ui" line="1109"/>
        <source>Copy to the clipboard</source>
        <translation>העתק ללוח</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="1112"/>
        <source>Copy</source>
        <translation>עותק</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="235"/>
        <location filename="../printui.ui" line="326"/>
        <source>Schedule</source>
        <translation>לוח זמנים</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="14"/>
        <source>Print</source>
        <translation>הדפס</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="390"/>
        <source>Call List and Hospitality Schedule</source>
        <translation>לוח זמנים רשימת שיחות והכבסת אורחים</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="783"/>
        <source>Midweek Meeting Title</source>
        <translation>כותרת אסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="685"/>
        <source>Weekend Meeting Title</source>
        <translation>כותרת אסיפת סוף השבוע</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="793"/>
        <source>Show Section Titles</source>
        <translation>הצג כותרות קטע</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="764"/>
        <source>Opening Comments Title</source>
        <comment>See S-140</comment>
        <translation>דברי פתיחה</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="258"/>
        <source>Assignment Slips for Assistants</source>
        <translation>פתקים משימות לעוזרים</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="361"/>
        <source>Outgoing Speakers Schedules</source>
        <translation>לוחות זמנים של נואמים יוצאים</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="377"/>
        <source>Outgoing Speakers Assignments</source>
        <translation>משימות נואמים יוצאים</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="403"/>
        <source>Talks of Speakers</source>
        <translation>נומים של נואמים</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="535"/>
        <source>Territory Map Card</source>
        <translation>כרטיס מפת השטח</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="545"/>
        <source>Map and Addresses Sheets</source>
        <translation>מפה וגיליונות כתובות</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="698"/>
        <source>Concluding Comments Title</source>
        <comment>See S-140</comment>
        <translation>כותרת לדברי סיכום</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="824"/>
        <source>Show public talk revision date</source>
        <translation>הצג תאריך עדכון לנאומים פומבים</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="725"/>
        <source>Show duration</source>
        <translation>משך ההצגה</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="705"/>
        <source>Show time</source>
        <translation>הצג זמן</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="810"/>
        <source>Show Workbook Issue no.</source>
        <translation>הצג מספר הגיליון פעילות.</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="803"/>
        <source>Show Watchtower Issue no.</source>
        <translation>הצג גיליון מצפה</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="817"/>
        <source>Own congregation only</source>
        <translation>קהילה שלך בלבד</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="968"/>
        <source>Territory number(s)</source>
        <translation>מספר/ים שטח/ים</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="975"/>
        <source>Comma delimited; press Enter to refresh</source>
        <translation>תחום פסיקים; לחץ על Enter כדי לרענן</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="981"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="522"/>
        <source>Territory Record</source>
        <translation>שיא שטח</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="870"/>
        <source>Template</source>
        <translation>תבנית</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="896"/>
        <source>Paper Size</source>
        <translation>גודל נייר</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="958"/>
        <source>Print From Date</source>
        <translation>הדפסה מתאריך</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="916"/>
        <source>Print Thru Date</source>
        <translation>הדפס עד תאריך</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="1039"/>
        <source>Printing</source>
        <translation>הדפסה</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="1042"/>
        <location filename="../printui.ui" line="1077"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="248"/>
        <source>Assignment Slips</source>
        <translation>טפסים של משימות</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="718"/>
        <source>Share in Dropbox</source>
        <translation>שתף בדרופבוקס</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="278"/>
        <location filename="../printui.ui" line="345"/>
        <source>Worksheets</source>
        <translation>גיליוני פעילות</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="565"/>
        <source>Meetings for field ministry</source>
        <translation>מפגשי השירות</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="423"/>
        <source>Combination</source>
        <translation>שלוב</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="632"/>
        <source>Additional Options</source>
        <translation>אפשרויות נוספות</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="660"/>
        <source>Show Song Titles</source>
        <translation>הצג כותרות שירים</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="735"/>
        <source>Show Counsel Text</source>
        <translation>הצג טקסט של יועץ</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="474"/>
        <source>Outgoing Speakers Schedule</source>
        <translation>לוח נואמים יוצאים</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="268"/>
        <source>Print assigned only</source>
        <translation>הדפסה הוקצתה בלבד</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="373"/>
        <source>Copied to the clipboard. Paste to word processing program (Ctrl+V/Cmd+V)</source>
        <translation>הועתק ללוח. הדבק לתכנית לעיבוד תמלילים (Ctrl + V / Cmd + V)</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="420"/>
        <location filename="../printui.cpp" line="512"/>
        <location filename="../printui.cpp" line="1148"/>
        <source>file created</source>
        <translation>הקובץ נוצר</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="746"/>
        <source>Concluding Comments</source>
        <translation>דברי סיכום</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="983"/>
        <source>Invalid entry, sorry.</source>
        <translation>ערך לא חוקי, מצטער.</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="384"/>
        <location filename="../printui.cpp" line="432"/>
        <location filename="../printui.cpp" line="492"/>
        <source>Save file</source>
        <translation>שמור את הקובץ</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="494"/>
        <source>Territories</source>
        <translation>שטחים</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="356"/>
        <source>Custom...</source>
        <comment>pick custom paper size</comment>
        <translation>המותאם אישית...</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="588"/>
        <source>Converting %1 to JPG file</source>
        <translation>המרת %1 לקובץ JPG</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="207"/>
        <location filename="../printui.ui" line="461"/>
        <location filename="../printui.cpp" line="737"/>
        <source>Midweek Meeting</source>
        <translation>אסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="976"/>
        <source>New Custom Paper Size</source>
        <comment>title of dialog box</comment>
        <translation>גודל נייר מותאם אישית חדש</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="976"/>
        <source>Format: width x height. Width and Height can be in or mm. Example 210mm x 297mm</source>
        <translation>פורמט: רוחב x גובה. רוחב וגובה יכולים להיות באינץ או מ&quot;מ. דוגמה 210 מ&quot;מ x 297 מ&quot;מ</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="298"/>
        <location filename="../printui.ui" line="448"/>
        <location filename="../printui.cpp" line="738"/>
        <source>Weekend Meeting</source>
        <translation>אסיפת סוף השבוע</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="745"/>
        <source>Opening Comments</source>
        <translation>דברי פתיחה</translation>
    </message>
</context>
<context>
    <name>publicmeeting_controller</name>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="193"/>
        <source>From %1</source>
        <translation>מ-%1</translation>
    </message>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="221"/>
        <source>The destination date already has a talk scheduled.</source>
        <translation>בתאריך היעד כבר נקבעה נאום.</translation>
    </message>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="226"/>
        <source>Swap Talks</source>
        <comment>Button text</comment>
        <translation>החלף נאומים</translation>
    </message>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="227"/>
        <source>Cancel</source>
        <comment>Button text</comment>
        <translation>לבטל</translation>
    </message>
</context>
<context>
    <name>publictalkedit</name>
    <message>
        <location filename="../publictalkedit.ui" line="67"/>
        <source>Public Talk</source>
        <translation>נאום פומבי</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="257"/>
        <location filename="../publictalkedit.ui" line="592"/>
        <location filename="../publictalkedit.ui" line="681"/>
        <location filename="../publictalkedit.ui" line="881"/>
        <location filename="../publictalkedit.cpp" line="47"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="169"/>
        <location filename="../publictalkedit.ui" line="866"/>
        <location filename="../publictalkedit.cpp" line="48"/>
        <source>Speaker</source>
        <translation>נואם</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="144"/>
        <location filename="../publictalkedit.ui" line="328"/>
        <location filename="../publictalkedit.cpp" line="50"/>
        <source>Chairman</source>
        <translation>יושב ראש</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="232"/>
        <location filename="../publictalkedit.ui" line="617"/>
        <location filename="../publictalkedit.ui" line="642"/>
        <source>Song</source>
        <translation>שיר</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="411"/>
        <source>Watchtower Study</source>
        <translation>שיעור המצפה</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="472"/>
        <location filename="../publictalkedit.cpp" line="52"/>
        <source>Reader</source>
        <translation>קרין</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="542"/>
        <source>Source</source>
        <translation>מקור</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="285"/>
        <location filename="../publictalkedit.ui" line="932"/>
        <source>Move to different week</source>
        <translation>עוברים לשבוע אחר</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="118"/>
        <location filename="../publictalkedit.ui" line="906"/>
        <source>Send to To Do List</source>
        <translation>שלח לרשימת המטלות</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="311"/>
        <source>Clear Public Talk selections</source>
        <comment>Clear=Delete</comment>
        <translation>נקה את הבחירות בנאומים פומבים</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="567"/>
        <location filename="../publictalkedit.ui" line="658"/>
        <location filename="../publictalkedit.cpp" line="51"/>
        <source>Conductor</source>
        <translation>מדריך</translation>
    </message>
</context>
<context>
    <name>speakersui</name>
    <message>
        <location filename="../speakersui.ui" line="278"/>
        <source>qrc:/qml/CongregationMap.qml</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>publictalkedit</name>
    <message>
        <location filename="../publictalkedit.ui" line="497"/>
        <location filename="../publictalkedit.ui" line="671"/>
        <source>Service Talk</source>
        <translation>נאום שירות</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="759"/>
        <source>Outgoing Speakers This Weekend</source>
        <translation>נואמים יוצאים</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="833"/>
        <source>Add to Outgoing List</source>
        <translation>הוסף לרשימה יוצאת</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="876"/>
        <source>Start time</source>
        <translation>שעת התחלה</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="886"/>
        <source>Theme No.</source>
        <translation>נושא מס &apos;</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="979"/>
        <source>To Do List</source>
        <translation>רשימת מטלות</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="1019"/>
        <source>Add item to schedule</source>
        <translation>הוסף פריט לתזמון</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="1100"/>
        <source>Add Outgoing To Do item</source>
        <comment>Add OUT item</comment>
        <translation>הוסף פריט פעילויות יוצא</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="793"/>
        <location filename="../publictalkedit.ui" line="1045"/>
        <source>Remove item</source>
        <translation>הסר פריט</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="1071"/>
        <source>Add Incoming To Do item</source>
        <translation>הוסף פריט נכנסות לביצוע</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="207"/>
        <location filename="../publictalkedit.ui" line="871"/>
        <location filename="../publictalkedit.cpp" line="49"/>
        <source>Congregation</source>
        <translation>קהילה</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="89"/>
        <source>Watchtower Study Edition</source>
        <translation>מהדורה לימודית המצפה</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="256"/>
        <location filename="../publictalkedit.cpp" line="291"/>
        <source>From %1; speaker removed</source>
        <comment>From [scheduled date]; speaker removed</comment>
        <translation>מ - %1; הנואם הוסר</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="294"/>
        <source>From %1; speaker moved to %2</source>
        <comment>From [scheduled date]; speaker moved to [new congregation]</comment>
        <translation>מ - %1; הנואם עבר ל-%2</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="340"/>
        <location filename="../publictalkedit.cpp" line="375"/>
        <source>From %1; talk discontinued</source>
        <comment>From [scheduled date]; talk discontinued</comment>
        <translation>מ - %1; הנאום הופסקה</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="695"/>
        <source>In</source>
        <comment>Incoming</comment>
        <translation>נכנס</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="695"/>
        <source>Out</source>
        <comment>Outgoing</comment>
        <translation>יוצא</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="817"/>
        <source>The selected speaker has already public talk on this calendar month. Do you want to add?</source>
        <translation>הנואם הנבחר כבר יש נאום פומבי בחודש קלנדרי זה. האם אתה רוצה להוסיף?</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="946"/>
        <location filename="../publictalkedit.cpp" line="997"/>
        <location filename="../publictalkedit.cpp" line="1037"/>
        <source>The destination date already has a talk scheduled. What to do?</source>
        <translation>בתאריך היעד כבר נקבעה נאום. מה לעשות?</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="947"/>
        <location filename="../publictalkedit.cpp" line="998"/>
        <source>&amp;Swap Talks</source>
        <translation>&amp;והחלפת נאומים</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="948"/>
        <location filename="../publictalkedit.cpp" line="999"/>
        <location filename="../publictalkedit.cpp" line="1038"/>
        <source>&amp;Move other talk to To Do List</source>
        <translation>&amp;והעביר נאומים אחרים לרשימת המטלות</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="949"/>
        <location filename="../publictalkedit.cpp" line="1000"/>
        <location filename="../publictalkedit.cpp" line="1039"/>
        <source>&amp;Cancel</source>
        <translation>&amp;לבטל</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="966"/>
        <location filename="../publictalkedit.cpp" line="1012"/>
        <location filename="../publictalkedit.cpp" line="1048"/>
        <location filename="../publictalkedit.cpp" line="1057"/>
        <location filename="../publictalkedit.cpp" line="1079"/>
        <location filename="../publictalkedit.cpp" line="1112"/>
        <source>From %1</source>
        <translation>מ %1</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="1035"/>
        <source>Date Already Scheduled</source>
        <translation>התאריך כבר מתוזמן</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="1066"/>
        <source>Error</source>
        <translation>שגיאה</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="1066"/>
        <source>Cannot schedule this item until these fields are fixed: %1</source>
        <translation>לא ניתן לתזמן פריט זה עד לתיקון שדות אלה:%1</translation>
    </message>
</context>
<context>
    <name>reminders</name>
    <message>
        <location filename="../reminders.ui" line="14"/>
        <source>Reminders</source>
        <translation>תזכורות</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="58"/>
        <source>Date range</source>
        <translation>טווח תאריכים</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="159"/>
        <source>Send selected reminders</source>
        <translation>שלח תזכורות נבחרות</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="177"/>
        <source>From</source>
        <translation>מ</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="195"/>
        <source>To</source>
        <translation>ל</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="247"/>
        <source>Details</source>
        <translation>פרטים</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="298"/>
        <source>Date</source>
        <translation>תאריך</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="303"/>
        <source>Name</source>
        <translation>שם</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="313"/>
        <source>Subject</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="318"/>
        <source>Message</source>
        <translation>הודעה</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="308"/>
        <source>E-Mail</source>
        <translation>דוא&apos;ל</translation>
    </message>
    <message>
        <location filename="../reminders.cpp" line="93"/>
        <source>Email sending...</source>
        <translation>שליחת דוא&quot;ל ...</translation>
    </message>
    <message>
        <location filename="../reminders.cpp" line="94"/>
        <source>Cancel</source>
        <translation>ךבטל</translation>
    </message>
    <message>
        <location filename="../reminders.cpp" line="120"/>
        <source>Error sending e-mail</source>
        <translation>שגיאה בשליחת דוא&apos;ל</translation>
    </message>
</context>
<context>
    <name>schoolreminder</name>
    <message>
        <location filename="../schoolreminder.cpp" line="57"/>
        <source>Chairman</source>
        <translation>יושב ראש</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="60"/>
        <source>Counselor-Class II</source>
        <translation>יועץ-כתה ב&apos;</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="63"/>
        <source>Counselor-Class III</source>
        <translation>יועץ-כתה ד&apos;</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="66"/>
        <source>Prayer I</source>
        <translation>תפילה א&apos;</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="69"/>
        <source>Prayer II</source>
        <translation>תפילה ב&apos;</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="162"/>
        <source>Cancellation - Our Christian Life and Ministry Meeting Assignment</source>
        <translation>ביטול - משימת אסיפת אורח חיינו ושירותנו המשיחיים</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="162"/>
        <source>Our Christian Life and Ministry Meeting Assignment</source>
        <translation>משימת אסיפת אורח חיינו ושירותנו המשיחיים</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="182"/>
        <source>Name</source>
        <translation>שם</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="184"/>
        <source>Assignment</source>
        <translation>משימה</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="187"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="189"/>
        <source>Source Material</source>
        <translation>חומר מקור</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="203"/>
        <source>Assistant</source>
        <translation>עוזר</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="213"/>
        <location filename="../schoolreminder.cpp" line="215"/>
        <source>Study</source>
        <translation>שיעור</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="222"/>
        <source>Main hall</source>
        <translation>אולם ראשי</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="225"/>
        <source>Auxiliary classroom 1</source>
        <translation>כיתה משנה  א</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="228"/>
        <source>Auxiliary classroom 2</source>
        <translation>כיתה משנה  ב</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="231"/>
        <source>To be given in</source>
        <comment>Refer to main hall or aux. classroom. See S-89</comment>
        <translation>מקום הגשת המשימה</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="237"/>
        <source>Reader for Congregation Bible Study</source>
        <translation>קרין לשיעור המקרא הקהילתי</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="239"/>
        <source>Assistant to %1</source>
        <comment>%1 is student&apos;s name</comment>
        <translation>עוזר/ת ל-%1</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="244"/>
        <source>Cancellation</source>
        <translation>ביטול</translation>
    </message>
</context>
<context>
    <name>schoolresult</name>
    <message>
        <location filename="../schoolresult.ui" line="23"/>
        <source>Volunteer</source>
        <translation>מתנדב</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="33"/>
        <source>Study</source>
        <translation>שיעור</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="45"/>
        <source>Exercise Completed</source>
        <translation>התרגיל הושלם</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="52"/>
        <source>Next study:</source>
        <translation>שיעור הבא:</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="69"/>
        <source>Setting:</source>
        <comment>for sisters assignment</comment>
        <translation>סדור:</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="96"/>
        <source>OK</source>
        <translation>אשור</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="130"/>
        <source>Cancel</source>
        <translation>לבטל</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="137"/>
        <source>Completed</source>
        <translation>הושלם</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="151"/>
        <source>Timing:</source>
        <translation>תזמון:</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="59"/>
        <source>Bible highlights</source>
        <translation>עיקרי הפרקים</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="61"/>
        <source>reading</source>
        <translation>קריאה</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="65"/>
        <source>Assignment result</source>
        <translation>תוצאת משימה</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="130"/>
        <source>Do not assign the next study</source>
        <translation>אל תקצה את השיעור הבא</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="159"/>
        <location filename="../schoolresult.cpp" line="369"/>
        <source>Leave on current study</source>
        <translation>השאר בשיעור הנוכחי</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="239"/>
        <source>Select setting</source>
        <translation>בחר סדור</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="246"/>
        <source>Timing</source>
        <translation>תזמון</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="247"/>
        <source>The timing is empty. Save?</source>
        <translation>התזמון ריק. לשמור?</translation>
    </message>
</context>
<context>
    <name>schoolui</name>
    <message>
        <location filename="../schoolui.cpp" line="35"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="35"/>
        <location filename="../schoolui.cpp" line="36"/>
        <source>Source</source>
        <translation>מקור</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="41"/>
        <source>Class</source>
        <translation>כיתה</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="111"/>
        <source>Bible highlights:</source>
        <translation>עיקרי הפרקים</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="115"/>
        <source>No. 1:</source>
        <translation>מס&apos; 1:</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="119"/>
        <source>No. 2:</source>
        <translation>מס&apos; 2:</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="123"/>
        <source>No. 3:</source>
        <translation>מס&apos; 3:</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="127"/>
        <source>Reader:</source>
        <translation>קרין:</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="176"/>
        <source>No school</source>
        <translation>אין בית ספר</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="329"/>
        <source>Nothing to display</source>
        <translation>אין מה להציג</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="495"/>
        <source>No assignment has been made!</source>
        <translation>לא בוצעה משימה!</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="500"/>
        <source>Please import new school schedule from Watchtower Library (Settings-&gt;Theocratic Ministry School...)</source>
        <translation>אנא יבוא לוח זמנים חדש מ-Watchtower Library (הגדרות -&gt; בית הספר התיאוקרטי ...)</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="542"/>
        <source>Show Details...</source>
        <translation>הראה פרטים...</translation>
    </message>
</context>
<context>
    <name>speakersui</name>
    <message>
        <location filename="../speakersui.ui" line="14"/>
        <source>Congregations and Speakers</source>
        <translation>קהילות ונאומים</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="400"/>
        <source>Congregation...</source>
        <translation>קהילה...</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="371"/>
        <source>Speaker...</source>
        <translation>נואם...</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1332"/>
        <source>Toggle Talks Editable</source>
        <translation>החלף את הנאומים הניתנים לעריכה</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1300"/>
        <source>Add Multiple Talks</source>
        <translation>הוסף נאומים מרובים</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="95"/>
        <location filename="../speakersui.ui" line="136"/>
        <location filename="../speakersui.ui" line="180"/>
        <location filename="../speakersui.ui" line="246"/>
        <location filename="../speakersui.ui" line="1303"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="494"/>
        <source>Select a Congregation or Speaker</source>
        <translation>בחר קהילה או נואם</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="725"/>
        <source>Info</source>
        <translation>מידע</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="792"/>
        <source>Address</source>
        <translation>כתובת</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="865"/>
        <location filename="../speakersui.cpp" line="180"/>
        <source>Circuit</source>
        <translation>נפה</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="667"/>
        <location filename="../speakersui.ui" line="1685"/>
        <location filename="../speakersui.cpp" line="188"/>
        <source>Congregation</source>
        <translation>קהילה</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="92"/>
        <source>Speakers</source>
        <translation>נואמים</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="133"/>
        <source>Group by congregation</source>
        <translation>קבוצה לפי קהילה</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="177"/>
        <source>Group by circuit</source>
        <translation>קבץ לפי נפה</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="221"/>
        <source>Filter</source>
        <translation>לסנן</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="243"/>
        <source>Configure Filter</source>
        <translation>הגדר את המסנן</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="882"/>
        <source>Congregation Details</source>
        <translation>פרטי הקהילה</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="894"/>
        <source>Meeting Times</source>
        <translation>זמני אסיפות</translation>
    </message>
</context>
<context>
    <name>printui</name>
    <message>
        <source>buttonGroup_2</source>
        <translation>buttonGroup_2</translation>
    </message>
</context>
<context>
    <name>speakersui</name>
    <message>
        <location filename="../speakersui.ui" line="999"/>
        <location filename="../speakersui.ui" line="1169"/>
        <source>Mo</source>
        <translation>ב&apos;</translation>
    </message>
</context>
<context>
    <name>printui</name>
    <message>
        <source>buttonGroup</source>
        <translation>buttonGroup</translation>
    </message>
</context>
<context>
    <name>speakersui</name>
    <message>
        <location filename="../speakersui.ui" line="1004"/>
        <location filename="../speakersui.ui" line="1174"/>
        <source>Tu</source>
        <translation>ג&apos;</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1009"/>
        <location filename="../speakersui.ui" line="1179"/>
        <source>We</source>
        <translation>ד&apos;</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1014"/>
        <location filename="../speakersui.ui" line="1184"/>
        <source>Th</source>
        <translation>ה&apos;</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1019"/>
        <location filename="../speakersui.ui" line="1189"/>
        <source>Fr</source>
        <translation>ו&apos;</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1024"/>
        <location filename="../speakersui.ui" line="1194"/>
        <source>Sa</source>
        <translation>שבת</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1029"/>
        <location filename="../speakersui.ui" line="1199"/>
        <source>Su</source>
        <translation>א&apos;</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1363"/>
        <source>Personal Info</source>
        <translation>מידע אישי</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1786"/>
        <location filename="../speakersui.cpp" line="842"/>
        <source>First Name</source>
        <translation>שם פרטי</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1838"/>
        <source>Mobile</source>
        <translation>נייד</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1576"/>
        <location filename="../speakersui.cpp" line="843"/>
        <source>Last Name</source>
        <translation>שם משפחה</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1540"/>
        <source>Phone</source>
        <translation>טלפון</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1377"/>
        <source>Public Talks</source>
        <translation>נאומים פומבים</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1646"/>
        <source>E-mail</source>
        <translation>דוא&apos;ל</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1467"/>
        <source>Notes</source>
        <translation>הערות</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="196"/>
        <source>Speaker</source>
        <translation>נואם</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="370"/>
        <location filename="../speakersui.cpp" line="550"/>
        <source>Undefined</source>
        <translation>לא מוגדר</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="453"/>
        <location filename="../speakersui.cpp" line="458"/>
        <source>%1 Meeting Day/Time</source>
        <translation>יום / זמן האסיפה %1</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="583"/>
        <source>A speaker with the same name already exists: &#x27;%1&#x27;. Do you want to change the name?</source>
        <translation>נואם בשם זהה כבר קיים: &apos;%1&apos;. האם אתה רוצה לשנות את השם?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="715"/>
        <source>The congregation has speakers!</source>
        <translation>בקהילה יש נואמים!</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="720"/>
        <source>Remove the congregation?</source>
        <translation>להסיר את הקהילה?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="766"/>
        <source>Remove the speaker?</source>
        <translation>להסיר את הנואם?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="771"/>
        <source>The speaker is scheduled for talks! These talks will
be moved to the To Do List if you remove the speaker.</source>
        <translation>הנואם מתוכנן לנאומים! הנאומים האלה כן
תועבר לרשימת המטלות אם תסיר את הנואם.</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="835"/>
        <source>Missing Information</source>
        <translation>מידע חסר</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="835"/>
        <source>Select congregation first</source>
        <translation>בחר תחילה קהילה</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="868"/>
        <source>New Congregation</source>
        <translation>קהילה חדשה</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="936"/>
        <source>Add Talks</source>
        <translation>הוסף נאומים</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="936"/>
        <source>Enter talk numbers separated by commas or periods</source>
        <translation>הזן מספרי נאום המופרדים בפסיקים או בנקודות</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="972"/>
        <source>Change congregation to &#x27;%1&#x27;?</source>
        <translation>לשנות את הקהילה ל- &apos;%1&apos;?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="974"/>
        <source>The speaker is scheduled for outgoing talks. These talks will
be moved to the To Do List if you change the congregation.</source>
        <translation>הנואם מתוכנן לנאומים יוצאים. הנאומים האלה כן
תועבר לרשימת המטלות אם אתה משנה את הקהילה.</translation>
    </message>
</context>
<context>
    <name>startup</name>
    <message>
        <location filename="../startup.ui" line="35"/>
        <source>Start Page</source>
        <translation>עמוד פתיחה</translation>
    </message>
</context>
<context>
    <name>sync_cloud</name>
    <message>
        <location filename="../sync_cloud.cpp" line="240"/>
        <source>Version conflict: The cloud changes have been made with a newer version!</source>
        <translation>Versioristiriita: Muutokset pilvessä on tehty uudemmalla versiolla!</translation>
    </message>
    <message>
        <location filename="../sync_cloud.cpp" line="246"/>
        <source>Version conflict: The cloud data needs to be updated with the same version by an authorized user.</source>
        <translation>התנגשות גרסא: על משתמש מורשה לעדכן את נתוני הענן באותה גרסה.</translation>
    </message>
</context>
<context>
    <name>territorymanagement</name>
    <message>
        <location filename="../territorymanagement.ui" line="14"/>
        <source>Territories</source>
        <translation>שטחים</translation>
    </message>
</context></TS>