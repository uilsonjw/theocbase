<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bnt-chich">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Musapereke phunziro lotsatira</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Siyani phunziro lomweli</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Dongosolo losayenera</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Onjezani nthawi?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Mutu</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Kuchokera</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Wophunzira</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Wothandiza</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Zotsatira</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Zatha</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Wodzipereka</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Sankhani wodzipereka</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Phunziro Latsopano</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Zochita Zatha</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Phunziro Lotsatira</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Sankhani phunziro lotsatira</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Nthawi</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Mfundo</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Zambiri za Sukulu</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Yambani Nthawi</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Imitsani Nthawi</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Mtundu wa Makambirano</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Mtundu wa Makambirano</translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>No exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit overseer&#x27;s visit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit assembly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Regional convention</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Memorial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Other exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Mutu</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Kuchokera</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Wotsogolera</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Wokamba</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Wowelenga</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Mfundo</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>CHUMA CHOPEZEKA M&apos;MAWU A MULUNGU</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>KUPHUNZITSA MWALUSO MU UTUMIKI</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>MOYO WATHU WACHIKRISTU</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Tcheyamani</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Mphungu</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Nyimbo% 1 ndi Pemphero</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Nyimbo</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Wotsogolera</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Wowelenga</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Mawu Oyamba</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Pemphero</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Siyani phunziro lomweli</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Yambani Nthawi</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Imitsani Nthawi</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Onjezani nthawi?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Mutu</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Kuchokera</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Wophunzira</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Zotsatila</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Wamaliza</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Nthawi</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Phunziro Latsopano</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Zochita Zimene Zatha</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Phunziro Lotsatila</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Sankhani phunziro lotsatira</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Mfundo</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Wothandiza</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Wodzipereka</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>Nthawi</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>Abale okhaokha kapena Alongo Okhaokha</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Zambiri</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>kubwereza</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Ibwereze</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation>Dzina Lolowera</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Mawu A chinsinsi</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Lowani</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Dzina lolowera kapena Imelo</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Imelo</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Pangani akaunti</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>Bwezeretsani  Mawu a Chinsinsi</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>Adilesi ya imelo siinapezeke!</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Mwayiwala mawu achinsinsi olowera</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>Tsamba lachilolezo</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation>Mlungu woyambira</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Ndandanda</translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation>Zolembapo</translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation>Ndandanda ya Wokamba Nkhani Kwina</translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Sindikizani</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Text size</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Nyimbo ndi Pemphero</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Nyimbo% 1 ndi Pemphero</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>Nkhani ya Onse</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>PHUNZIRO la NSANJA YA OLONDA</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Nyimbo% 1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Wotsogolera</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Wowerenga</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Phone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>Dzina Loyamba</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Dzina Lomaliza</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>M&apos;bale</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Mlongo</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Waudindo</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Mutu wa Banja</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>Wachibale wokhudzana ndi</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Zambiri zamalumikizidwe</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Foni</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Imelo</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>Ma kalasi onse</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Kalasi Yoyamba Yokha</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Kalasi Yachiwiri Yokha</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Mutu wa Banja</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Wothandiza</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Zambiri</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>Osafooka</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Tcheyamani</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>Chuma chpezeka M&apos;Mawu a Mulungu</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Kuwerenga Baibulo</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Ulendo Woyamba</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Ulendo Wobwereza</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Phunziro la Baibulo</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>Moyo Wathu Wachikhristu</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Phunziro la Baibulo la Mpingo</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Wowelenga pa Phunziro la Baibulo la Mpingo</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>Wofalitsa watsopano</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Pemphero</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Wofalitsa</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Makhalidwe</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Tulukani</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Uthenga</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>Poyambira TheocBase</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Ndemanga</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>Kusinthidwa komaliza</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Ndandanda</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Wonetsani Nthawi</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Onetsani Kutalika kwake</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Mawonekedwe a zinthu</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Chilankhulo</translation>
    </message>
    <message>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Printing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Kugwirizana ...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Song</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>weeks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>Kusintha komweku kungapezeke pamakina anoi ndi mumtambo (% 1 mizera). Kodi mukufuna kusunga kusintha Kwapamakina ano?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Musapereke phunziro lotsatira</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>Osasankhidwa</translation>
    </message>
</context></TS>