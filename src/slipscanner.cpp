#include "slipscanner.h"
//#include <QDebug>

int SlipScanner::region::nextId = 0;

SlipScanner::SlipScanner(QWidget *parent, QString filename, bool forceScan, int regionAbsorptionMargin)
    : needFinalized(false), saveRegions(0), m_regionAbsorptionMargin(regionAbsorptionMargin), progressDialog(nullptr)
{
    bool showProgress = QOperatingSystemVersion::currentType() != QOperatingSystemVersion::IOS &&
            QOperatingSystemVersion::currentType() != QOperatingSystemVersion::Android;

    m_filename = filename;
    is4up = filename.contains(QRegularExpression("[_-]4[-]?up.", QRegularExpression::CaseInsensitiveOption)) || filename.contains(QRegularExpression("[_-]mu[-].", QRegularExpression::CaseInsensitiveOption));
    slipsPerPage = is4up ? 4 : 1;
    img.load(filename);
    pageWidth = img.width();
    pageHeight = img.height();
    if (img.isNull())
        return;

    sql_class *sql = &Singleton<sql_class>::Instance();

    region::nextId = 1;
    QFileInfo info(filename);
    settingsKey = filename + ":" + info.metadataChangeTime().toString();
    QString compiledSlips(sql->getSetting(settingsKey));
    if (!compiledSlips.isEmpty() && !compiledSlips.startsWith("v03;")) {
        forceScan = true; // rescan if older version
    }
    if (forceScan || compiledSlips.isEmpty()) {
        if (showProgress) {
            progressDialog = new QProgressDialog(QObject::tr("One-time Scanning of New Slip"), "", 0, 100, parent);
            progressDialog->setCancelButton(nullptr);
            progressDialog->setWindowModality(Qt::ApplicationModal);
            progressDialog->setAttribute(Qt::WA_DeleteOnClose);
            progressDialog->show();
            QCoreApplication::processEvents();
        }

        scan();

        if (showProgress) {
            progressDialog->close();
            delete (progressDialog);
        }
    } else {
        QStringList parts(compiledSlips.split(";"));
        layout = parts[1].toInt();
        QStringList txt(parts[2].split("|"));
        QStringList ck(parts[3].split("|"));
        QStringList ba(parts[4].split("|"));
        for (QString b : txt) {
            QStringList coords(b.split(","));
            if (coords.length() == 4)
                textBoxes.append(new region(coords[0].toInt(), coords[1].toInt(), coords[2].toInt(), coords[3].toInt(), regionAbsorptionMargin));
        }
        for (QString b : ck) {
            QStringList coords(b.split(","));
            if (coords.length() == 4)
                checkBoxes.append(new region(coords[0].toInt(), coords[1].toInt(), coords[2].toInt(), coords[3].toInt(), regionAbsorptionMargin));
        }
        for (QString b : ba) {
            QStringList coords(b.split(","));
            if (coords.length() == 4)
                blankAreas.append(new region(coords[0].toInt(), coords[1].toInt(), coords[2].toInt(), coords[3].toInt(), regionAbsorptionMargin));
        }
    }
}

SlipScanner::~SlipScanner()
{
    qDeleteAll(regions);
    regions.clear();
    qDeleteAll(textBoxes);
    textBoxes.clear();
    qDeleteAll(checkBoxes);
    checkBoxes.clear();
}

void SlipScanner::scan()
{
    // aquiring objects
    needFinalized = true;

    qDeleteAll(regions);
    regions.clear();
    potentialCheckBoxes.clear();

    region *current = new region(0, 0, 0, 0, m_regionAbsorptionMargin);
    bool currentValid(false);
    int progress(0);
    for (int y = 0; y < pageHeight; y++) {

        int newProgress(y * 100 / pageHeight);
        if (newProgress != progress) {
            progress = newProgress;
            if (progressDialog) {
                progressDialog->setValue(progress);
                QCoreApplication::processEvents();
            }
        }

        for (int x = 0; x < pageWidth; x++) {
            QColor pixel(img.pixel(x, y));
            int gray(pixel.red() + pixel.green() + pixel.blue());
            if (gray < colorThreshold) {
                // have data.  Create new object or expand previous?
                if (!currentValid) {
                    current->x1 = x;
                    current->x2 = x;
                    current->y1 = y;
                    current->y2 = y;
                    currentValid = true;
                } else
                    current->x2 = x;
            } else if (currentValid) {
                current->update();
                // back to being white.  Should existing object be absorbed or added to list?
                bool absorbed(false);
                for (region *other : regions) {
                    if (other->absorb(current)) {
                        absorbed = true;
                        break;
                    }
                }
                if (!absorbed) {
                    regions.append(current);
                    current = new region(0, 0, 0, 0, m_regionAbsorptionMargin);
                }
                currentValid = false;
            }
        }
    }

    // clean up overlapping objects to determine true sizes
    bool absorbed(true);
    while (absorbed) {
        absorbed = false;
        int objcnt(regions.count());
        for (int i1 = 0; i1 < objcnt; i1++) {
            region *o1 = regions[i1];
            for (int i2 = i1 + 1; i2 < objcnt; i2++) {
                region *o2 = regions[i2];
                if (o1->absorb(o2)) {
                    regions.removeAt(i2);
                    delete (o2);
                    objcnt--;
                    absorbed = true;
                }
            }
        }
    }

    // find title (largest area object)
    int maxarea(0);
    region *title(nullptr);
    for (region *o : regions) {
        int area(o->width * o->height);
        if (area > maxarea) {
            maxarea = area;
            title = o;
        }
    }

    usedPageX1 = pageWidth;
    usedPageX2 = 0;
    usedPageY1 = pageHeight;
    usedPageY2 = 0;
    for (region *o : regions) {
        if (o->x1 < usedPageX1)
            usedPageX1 = o->x1;
        if (o->x2 > usedPageX2)
            usedPageX2 = o->x2;
        if (o->y1 < usedPageY1)
            usedPageY1 = o->y1;
        if (o->y2 > usedPageY2)
            usedPageY2 = o->y2;
    }
    workingPageWidth = (usedPageX1 + (usedPageX2 - usedPageX1) / 2) * 2;
    workingPageHeight = (usedPageY1 + (usedPageY2 - usedPageY1) / 2) * 2;

    // find our boxes
    double titleHeight = title->height;
    double midPageThreshold(workingPageHeight / 18);
    //qDebug() << "x1 x2 y1 y2 titlecomp W/H ckboxW/H";
    for (region *o : regions) {
        /*
        qDebug() << o->x1 << o->x2 << o->y1 << o->y2
                 << ((titleHeight - o->height) / titleHeight)
                 << (o->width / o->height)
                 << fabs(1.0 - (double)o->width / (double)o->height);
        //o->applyToImg(&img);        
        */
        if (
                ((titleHeight - o->height) / titleHeight) > .8 /* shorter than half the title text */
                && (o->width / o->height) > 10 /* 2020-10-13 JMa: Changed 30 to 10. The text box may be quite short in some languages */
                && (!is4up || (abs(workingPageHeight / 2 - o->y1) > midPageThreshold)) /* not in the middle of the page */
        ) {
            textBoxes.append(new region(static_cast<int>(o->x1 + titleHeight / 5), o->x2, static_cast<int>(o->y1 - titleHeight), o->y1 - 1, m_regionAbsorptionMargin));
        } else if (fabs(1.0 - static_cast<double>(o->width) / static_cast<double>(o->height)) < .15) {
            // found box-like ratio.
            // Summarize contents
            int sectionPotential[3][3];
            for (int y = 0; y < 3; y++) {
                for (int x = 0; x < 3; x++) {
                    sectionPotential[x][y] = 0;
                    o->sectionSummary[x][y] = 0;
                }
            }
            int boxmargin = o->width / 6;
            int boxCutterX1(o->x1 + boxmargin);
            int boxCutterX2(o->x2 - boxmargin);
            int boxCutterY1(o->y1 + boxmargin);
            int boxCutterY2(o->y2 - boxmargin);
            for (int y = o->y1; y <= o->y2; y++) {
                for (int x = o->x1; x <= o->x2; x++) {
                    QColor pixel(img.pixel(x, y));
                    int gray(pixel.red() + pixel.green() + pixel.blue());
                    int sectionx(x > boxCutterX2 ? 2 : (x > boxCutterX1 ? 1 : 0));
                    int sectiony(y > boxCutterY2 ? 2 : (y > boxCutterY1 ? 1 : 0));
                    sectionPotential[sectionx][sectiony]++;
                    o->sectionSummary[sectionx][sectiony] += gray;
                }
            }
            for (int y = 0; y < 3; y++) {
                for (int x = 0; x < 3; x++) {
                    if (sectionPotential[x][y] > 0)
                        o->sectionSummary[x][y] /= sectionPotential[x][y];
                }
            }

            //qDebug() << "checkbox" << o->x1 << o->y1 << o->sectionSummary[0][0] << o->sectionSummary[1][0] << o->sectionSummary[2][0] << o->sectionSummary[0][1] << o->sectionSummary[1][1] << o->sectionSummary[2][1] << o->sectionSummary[0][2] << o->sectionSummary[1][2] << o->sectionSummary[2][2];

            // if the center is white enough, and corners dark enough, add it. We will judge more quality in a moment
            if (o->sectionSummary[1][1] > 600 && o->sectionSummary[0][0] < 400 && o->sectionSummary[2][0] < 400 && o->sectionSummary[0][2] < 400 && o->sectionSummary[2][2] < 400) {
                potentialCheckBoxes.append(o);
            }
        }
    }
}

void SlipScanner::FinalizeBoxes(int maxCheckboxes)
{
    int quadrantXSplit = is4up ? workingPageWidth / 2 : workingPageWidth;
    int quadrantYSplit = is4up ? workingPageHeight / 2 : workingPageHeight;
    // int midQuadrantXSplit = is4up ? (usedPageX2 - usedPageX1) / 4 : (usedPageX2 - usedPageX1) / 2;

    // pick checkboxes that are most closely related.
    qDeleteAll(checkBoxes);
    checkBoxes.clear();    
    if (potentialCheckBoxes.length() > 0) {
        int medianId(potentialCheckBoxes.length() / 2);
        //qDebug() << "medianId" << medianId;
        int checkboxCount(potentialCheckBoxes.length());
        //qDebug() << "checkboxCount" << checkboxCount;
        for (region *r : potentialCheckBoxes) {
            r->rating = 0;
        }
        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 3; x++) {
                // find median value
                for (region *r : potentialCheckBoxes) {
                    r->_ref = 0;
                    r->_x = x;
                    r->_y = y;
                }
                std::sort(potentialCheckBoxes.begin(), potentialCheckBoxes.end(), compareCheckboxArea);
                int medianVal(potentialCheckBoxes[medianId]->sectionSummary[x][y]);
                //qDebug() << "medianVal" << medianVal;
                //see which boxes are closest to the median
                for (region *r : potentialCheckBoxes) {
                    r->_ref = medianVal;
                }
                std::sort(potentialCheckBoxes.begin(), potentialCheckBoxes.end(), compareCheckboxArea);
                // finally, rate them
                for (int i = 0; i < checkboxCount; i++) {
                    potentialCheckBoxes[i]->rating++;
                }
            }
        }

        // sort by rating
        std::sort(potentialCheckBoxes.begin(), potentialCheckBoxes.end(), compareCheckboxRatingDec);

        // using the best boxes so far, sort again by proximity to other boxes
        QList<region *> bestBoxes;
        checkboxCount = maxCheckboxes * slipsPerPage * 4 / 3;
        if (checkboxCount > potentialCheckBoxes.length())
            checkboxCount = potentialCheckBoxes.length();
        for (int i = 0; i < checkboxCount; i++) {
            region *r = potentialCheckBoxes[i];
            r->rating = 0;
            r->quadrant = (r->x1 < quadrantXSplit ? 1 : 2) + (r->y1 < quadrantYSplit ? 0 : 2);
            bestBoxes.append(r);
        }
        for (region *c1 : bestBoxes) {
            c1->rating = 0;
            for (region *c2 : bestBoxes) {
                if (c1->quadrant == c2->quadrant) {
                    c1->rating += (c2->x1 - c1->x1) * (c2->x1 - c1->x1) + (c2->y1 - c1->y1) * (c2->y1 - c1->y1);
                }
            }
        }
        std::sort(bestBoxes.begin(), bestBoxes.end(), compareCheckboxRatingAsc);

        checkboxCount = maxCheckboxes * slipsPerPage;
        if (checkboxCount > potentialCheckBoxes.length())
            checkboxCount = potentialCheckBoxes.length();
        for (int i = 0; i < checkboxCount; i++) {
            region *r = bestBoxes[i];

            // checkboxes can't be inside the title
            int titleHeight = quadrantYSplit * 0.20;
            if ((r->y1 < quadrantYSplit && r->y1 < titleHeight) || (r->y1 > quadrantYSplit && r->y1 < quadrantYSplit + titleHeight))
                continue;

            checkBoxes.append(new region(r->x1, r->x2, r->y1, r->y2, m_regionAbsorptionMargin));
            //qDebug() << "selected" << r->x1 << r->y1 << r->rating << r->sectionSummary[0][0] << r->sectionSummary[1][0] << r->sectionSummary[2][0] << r->sectionSummary[0][1] << r->sectionSummary[1][1] << r->sectionSummary[2][1] << r->sectionSummary[0][2] << r->sectionSummary[1][2] << r->sectionSummary[2][2];
        }
    }

    // find blank area around the center of each slip
    for (int q = 0; q < slipsPerPage; q++) {
        int blankAreaXMin = 0;
        int blankAreaXMax = workingPageWidth;
        int blankAreaYMin = 0;
        int blankAreaYMax = workingPageHeight;

        for (region *o : regions) {
            if (o->x2 < (quadrantXSplit / 2 + q % 2 * quadrantXSplit) && o->x2 > blankAreaXMin)
                blankAreaXMin = o->x2;
            if (o->x1 > (quadrantXSplit / 2 + q % 2 * quadrantXSplit) && o->x1 < blankAreaXMax)
                blankAreaXMax = o->x1;
            if (o->y2 < (quadrantYSplit / 2 + q / 2 * quadrantYSplit) && o->y2 > blankAreaYMin)
                blankAreaYMin = o->y2;
            if (o->y1 > (quadrantYSplit / 2 + q / 2 * quadrantYSplit) && o->y1 < blankAreaYMax)
                blankAreaYMax = o->y1;
        }

        blankAreas.append(new region(blankAreaXMin, blankAreaXMax, blankAreaYMin, blankAreaYMax, m_regionAbsorptionMargin));
    }

    if (saveRegions) {
        for (region *o : regions) {
            for (int y = o->y1; y <= o->y2; y++) {
                for (int x = o->x1; x <= o->x2; x++) {
                    QColor pixel(img.pixel(x, y));
                    pixel.setRed(pixel.red() < 128 ? 128 : pixel.red());
                    img.setPixel(x, y, pixel.Rgb);
                }
            }
        }
        QString newfilename(m_filename.replace(".jpg", "_regions.jpg"));
        img.save(newfilename);
    }

    // sort the boxes as printui expects to see them    
    for (region *box : textBoxes) {
        box->quadrant = (box->x1 < quadrantXSplit ? 1 : 2) + (box->y1 < quadrantYSplit ? 0 : 2);
    }

    // sort by quadrant
    std::sort(textBoxes.begin(), textBoxes.end(), [](const region *a, const region *b) -> bool { return a->quadrant < b->quadrant; });        
    if (textBoxes.length() > 3 * slipsPerPage) {
        int boxesPerSlip = textBoxes.length() / slipsPerPage;
        for (int i = 0; i < slipsPerPage; i++) {            
            for (int j = 0; j < boxesPerSlip; j++) {
                region *box = textBoxes[j + i * boxesPerSlip];
                box->quadrant = (box->x1 < quadrantXSplit ? 1 : 2) + (box->y1 < quadrantYSplit ? 0 : 2);
                if (box->quadrant == 1 || box->quadrant == 3)
                    box->half = box->x1 - usedPageX1 < quadrantXSplit - box->x2 ? 0 : 1;
                else
                    box->half = box->x1 - quadrantXSplit < usedPageX2 - box->x2 ? 0 : 1;
            }
            // sort by y1
            std::sort(textBoxes.begin() + i * boxesPerSlip, textBoxes.begin() + (i + 1) * boxesPerSlip - 1, [](const region *a, const region *b) -> bool { return a->y1 < b->y1; });
            // sort by half but skip the three first boxes (name, assistant, date)
            std::sort(textBoxes.begin() + i * boxesPerSlip + 3, textBoxes.begin() + (i + 1) * boxesPerSlip, [](const region *a, const region *b) -> bool { return a->half < b->half; });
        }       
    }

    int row1colcnt(0);
    int row2colcnt(0);

    // sort checkboxes by x1
    std::sort(checkBoxes.begin(), checkBoxes.end(), [](const region *a, const region *b) -> bool { return a->x1 < b->x1; });

    int minX = checkBoxes.length() > 0 ? checkBoxes.first()->x1 : 0;
    int maxX = checkBoxes.length() > 0 ? checkBoxes.last()->x1 : 0;

    for (region *box : checkBoxes) {
        box->quadrant = (box->x1 < quadrantXSplit ? 1 : 2) + (box->y1 < quadrantYSplit ? 0 : 2);
        if (box->x1 < quadrantXSplit)
            box->half = abs(box->x1 - minX) < 10 ? 0 : 1; // left side
        else
            box->half = abs(box->x1 - maxX) < 10 ? 1 : 0; // right side
        if (box->half)
            row2colcnt++;
        else
            row1colcnt++;
    }
    std::sort(checkBoxes.begin(), checkBoxes.end(), compareBox);

    //qDebug() << "textBoxes" << textBoxes.length();
    //qDebug() << "checkBoxes" << checkBoxes.length();    
    layout = row1colcnt / slipsPerPage * 100 + row2colcnt / slipsPerPage;

    SaveCompiledSlips();
}

void SlipScanner::SaveCompiledSlips()
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    QString compiledSlips;

    compiledSlips = "v03;" + QVariant(layout).toString() + ";";
    bool isFirst(true);
    for (region *box : textBoxes) {
        if (isFirst)
            isFirst = false;
        else
            compiledSlips.append("|");
        compiledSlips.append(
                QString::number(box->x1) + "," + QString::number(box->x2) + "," + QString::number(box->y1) + "," + QString::number(box->y2));
    }
    compiledSlips.append(";");
    isFirst = true;
    for (region *box : checkBoxes) {
        if (isFirst)
            isFirst = false;
        else
            compiledSlips.append("|");
        compiledSlips.append(
                QString::number(box->x1) + "," + QString::number(box->x2) + "," + QString::number(box->y1) + "," + QString::number(box->y2));
    }
    compiledSlips.append(";");
    isFirst = true;
    for (region *box : blankAreas) {
        if (isFirst)
            isFirst = false;
        else
            compiledSlips.append("|");
        compiledSlips.append(
                QString::number(box->x1) + "," + QString::number(box->x2) + "," + QString::number(box->y1) + "," + QString::number(box->y2));
    }
    sql->saveSetting(settingsKey, compiledSlips);
    needFinalized = false;
}

bool compareCheckboxArea(const SlipScanner::region *a, const SlipScanner::region *b)
{
    return abs(a->_ref - a->sectionSummary[a->_x][a->_y]) < abs(b->_ref - b->sectionSummary[b->_x][b->_y]);
}

bool compareCheckboxRatingAsc(const SlipScanner::region *a, const SlipScanner::region *b)
{
    return a->rating < b->rating;
}

bool compareCheckboxRatingDec(const SlipScanner::region *a, const SlipScanner::region *b)
{
    return a->rating > b->rating;
}

bool compareBox(const SlipScanner::region *a, const SlipScanner::region *b)
{
    if (a->quadrant < b->quadrant)
        return true;
    if (a->quadrant > b->quadrant)
        return false;

    if (a->half < b->half)
        return true;
    if (a->half > b->half)
        return false;

    return a->y1 < b->y1;
}

SlipScanner::region::region(int x1, int x2, int y1, int y2, int absorptionMargin)
    : id(nextId++), x1(x1), x2(x2), y1(y1), y2(y2), absorptionMargin(absorptionMargin), quadrant(0), half(0), rating(0), _ref(0), _x(0), _y(0)
{
    memset(sectionSummary, 0, sizeof(sectionSummary));
    update();
}

bool SlipScanner::region::intersects(region *other)
{
    return other->x1outer <= x2outer && other->x2outer >= x1outer && other->y1outer <= y2outer && other->y2outer >= y1outer;
}

// returns true if other was absorbed
bool SlipScanner::region::absorb(region *other)
{
    if (intersects(other)) {
        if (other->x1 < x1)
            x1 = other->x1;
        if (other->x2 > x2)
            x2 = other->x2;
        if (other->y1 < y1)
            y1 = other->y1;
        if (other->y2 > y2)
            y2 = other->y2;
        update();
        return true;
    } else
        return false;
}

void SlipScanner::region::update()
{
    width = x2 - x1 + 1;
    height = y2 - y1 + 1;
    x1outer = x1 - absorptionMargin;
    x2outer = x2 + absorptionMargin;
    y1outer = y1 - absorptionMargin;
    y2outer = y2 + absorptionMargin;
}

void SlipScanner::region::applyToImg(QImage *img)
{
    for (int y = y1; y <= y2; y++) {
        for (int x = x1 - 1; x <= x2 + 1; x++) {
            QColor pixel(img->pixel(x, y));
            pixel.setRed(pixel.red() < 128 ? 128 : pixel.red());
            img->setPixel(x, y, pixel.Rgb);
        }
    }
}
