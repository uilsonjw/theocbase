import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.3
import QtQuick.Layouts 1.3
import net.theocbase.mobile 1.0

Rectangle {
    id: outgoingSpeakersModule
    color: "#f7f4f4"
    Layout.fillWidth: true
    Layout.preferredHeight: height
    height: 70*dpi + outModel.length * 40*dpi
    property date currentDate
    //property double dpi: 1

    function loadList(d) {
        currentDate = d
        outModel.loadList(currentDate)
    }

    OutgoingSpeakersModel {
        id: outModel
    }

    ColumnLayout {
        id: layout
        spacing: 0
        anchors.fill: parent

        Text {
            id: text5
            color: "#a9928b"
            text: qsTr("OUTGOING SPEAKERS")
            font.bold: true
            Layout.preferredHeight: 30*dpi
            Layout.fillWidth: true
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: app_info.fontsize

            Rectangle { color: "lightgrey"; anchors.top: parent.top; width: parent.width; height: app_info.linewidth }
        }

        ListView {
            model: outModel
            Layout.fillWidth: true
            Layout.fillHeight: true
            height: outModel ? outModel.length * 40*dpi : 0
            Layout.preferredHeight: height
            interactive: false
            delegate: ScheduleRowItem {
                width: outgoingSpeakersModule.width
                timeText.text: Qt.locale().dayName(date.getDay(), Locale.ShortFormat)
                timeboxColor: "#a9928b"
                themeText.text: theme == "" ? "" : theme + " (" + themeNo + ")"
                nameText1.text: speaker
                nameText2.text: "(" + congregation + ")"
                showTopLine: index == 0
            }
        }

        ScheduleRowItem {
            Layout.fillWidth: true
            timeboxColor: "transparent"
            timeText.text: ""            
            themeText.text: outModel.length > 0 ?
                                qsTr("%1 speakers away this weekend","", outModel.length).arg(outModel.length) :
                                qsTr("No speakers away this weekend")
            showTopLine: true
        }
    }
}
