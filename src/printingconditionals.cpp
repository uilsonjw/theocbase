#include "printingconditionals.h"
//#include <QtDebug>
//#include <QFile>
//#include <QTextStream>

printingconditionals::printingconditionals()
{
    this->ifthentag = "#";
    init();
}

printingconditionals::printingconditionals(QString ifthentag)
{
    this->ifthentag = ifthentag;
    init();
}

printingconditionals::~printingconditionals()
{
    DeleteAndClear(conditions);
}

void printingconditionals::init()
{
    iftag = ifthentag + "IF";
    elsetag = ifthentag + "ELSE" + ifthentag;
    elseiftag = ifthentag + "ELSEIF";
    endiftag = ifthentag + "ENDIF" + ifthentag;
}

QString printingconditionals::tokenize(QString html)
{
    int htmllen(html.length());
    QStringRef htmlref;
    int pos(0);
    int tokenIdx(-1);
    int thispos(-1);
    int len(-1);
    tokens token(tUnknown);
    bool computation(false);
    QList<condition *> operators;
    QList<QString> tokenOptions;
    QList<tokens> tokenTokens;
    QList<QString> operatorOptions;
    QList<tokens> operatorTokens;
    int compendpos(-1);

    tokenOptions.append(iftag);
    tokenTokens.append(tIf);
    tokenOptions.append(elsetag);
    tokenTokens.append(tElse);
    tokenOptions.append(elseiftag);
    tokenTokens.append(tElseIf);
    tokenOptions.append(endiftag);
    tokenTokens.append(tEndIf);

    operatorOptions.append("=");
    operatorTokens.append(tEq);
    //operatorOptions.append("!="); operatorTokens.append(tNeq); << without adding more complication, can't use this because ! starts/ends a variable
    operatorOptions.append("&gt;&lt;");
    operatorTokens.append(tNeq);
    operatorOptions.append("&lt;");
    operatorTokens.append(tLs);
    operatorOptions.append("&gt;");
    operatorTokens.append(tGr);
    operatorOptions.append("&lt;=");
    operatorTokens.append(tLsEq);
    operatorOptions.append("&gt;=");
    operatorTokens.append(tGrEq);
    operatorOptions.append("has");
    operatorTokens.append(tHas);
    operatorOptions.append("nothas");
    operatorTokens.append(tNhas);
    operatorOptions.append("hasnot");
    operatorTokens.append(tNhas);
    operatorOptions.append("empty");
    operatorTokens.append(tEmpty);
    operatorOptions.append("notempty");
    operatorTokens.append(tNempty);
    operatorOptions.append(" and ");
    operatorTokens.append(tAnd);
    operatorOptions.append(" or ");
    operatorTokens.append(tOr);

    for (;;) {
        len = 0;
        thispos = -1;
        computation = false;
        htmlref = html.midRef(pos, htmllen - pos);

        findoption(htmlref, tokenOptions, &thispos, &tokenIdx);
        if (thispos == -1)
            break; // no more conditions found, we're done

        thispos += pos;
        token = tokenTokens[tokenIdx];
        switch (token) {
        case tIf:
        case tElseIf:
            computation = true;
            break;
        default:
            break;
        }
        len = tokenOptions[tokenIdx].length();

        if (computation) {
            // search for end tag
            compendpos = html.indexOf(ifthentag, thispos + len + 1);
            if (compendpos == -1) {
                // since not found, skip this conditional
                pos = thispos + len;
                continue;
            }

            DeleteAndClear(operators);

            // loop thru ANDs, ORs, and other operators
            bool lastone(false);
            condition *op = new condition(); // 'op' for operator
            bool lasttokenwasandor = true;
            int startpos(thispos + len);
            do {
                QStringRef condtext(html.midRef(startpos, compendpos - startpos));
                int condPos(-1);
                int condIdx(-1);
                findoption(condtext, operatorOptions, &condPos, &condIdx);
                lastone = condPos == -1;
                tokens t(lastone ? tAnd : operatorTokens[condIdx]);
                bool tokenisandor(t == tAnd || t == tOr);
                if (lasttokenwasandor && tokenisandor) {
                    // if no operator provided, not empty is it
                    op->token = tNemptyDefault;
                    op->conditionpos = startpos + condPos;
                    op->conditionlen = 0;
                    operators.append(op);
                    op = new condition();
                }
                if (!lastone) {
                    op->token = t;
                    op->conditionpos = startpos + condPos;
                    op->conditionlen = operatorOptions[condIdx].length();
                    operators.append(op);
                    startpos = op->conditionend();
                }
                op = new condition();
                lasttokenwasandor = tokenisandor;
            } while (!lastone);

            delete (op);
        }

        pos = thispos;

        // replace the original text with token
        int offset(0);
        pastetoken(&html, token, pos, len, true, false, computation, &offset);

        if (computation) {
            const int oplen = operators.length();
            for (int i = 0; i < oplen; i++) {
                condition *op(operators[i]);
                pastetoken(&html, op->token, op->conditionpos, op->conditionlen, true, true, true, &offset);
            }
            pastetoken(&html, tEnd, compendpos, 1, false, true, false, &offset);
        } else {
            pos += len;
        }
        pos += offset;
        htmllen += offset;
    }

    DeleteAndClear(operators);

    return html.remove(QChar((char)tToBeRemoved));
}

void printingconditionals::findoption(QStringRef html, QList<QString> options, int *selPos, int *selIdx)
{
    int bestpos = -1;
    int bestlen = -1;
    int bestidx;
    int pos(-1);
    const int optCnt(options.count());
    for (int i = 0; i < optCnt; ++i) {
        QString opt(options[i]);
        int optlen(opt.length());
        pos = html.indexOf(opt, 0, Qt::CaseInsensitive);
        if (pos > -1 && (pos < bestpos || (pos == bestpos && optlen > bestlen) || bestpos == -1)) {
            bestpos = pos;
            bestlen = optlen;
            bestidx = i;
        }
    }
    if (bestpos > -1) {
        *selPos = bestpos;
        *selIdx = bestidx;
    } else {
        *selPos = -1;
    }
}

void printingconditionals::pastetoken(QString *html, tokens t, int pos, int len, bool includeid, bool cleanspacebefore, bool cleanspaceafter, int *offset)
{
    int start(pos + *offset);
    int end(start + len);
    if (cleanspacebefore) {
        while (html->at(start - 1) == QChar(' '))
            start--;
    }
    if (cleanspaceafter) {
        while (html->at(end) == QChar(' '))
            end++;
    }
    int need(len + (includeid ? 1 : 0));
    int have(end - start);
    while (have < need) {
        html->insert(start, QChar(' '));
        (*offset)++;
        end++;
        have++;
    }

    // do it
    if (includeid)
        (*html)[start++] = QChar((char)tToken);
    (*html)[start++] = QChar((char)t);
    while (start < end)
        (*html)[start++] = QChar((char)tToBeRemoved);
}

QString printingconditionals::compute(QString html)
{
    this->html = html;
    int htmlsize(html.size());
    results = "";
    results.reserve(htmlsize);
    _compute();
    _expandtoelement();
    _render();
    results.squeeze();
    return results;
}

void printingconditionals::_compute()
{
    qDeleteAll(conditions);
    conditions.clear();
    int pos(0);
    int tagpos(-1);
    condition *cond = new condition();
    bool computation(false);
    tokens comptoken(tUnknown);
    int comppos(-1);
    int compendpos(-1);
    compstates lastcompstate(sNotset);
    compstates nextcompstate(sNotset);

    // to make rendering easier, add a fake entry to cover the text before the first conditional
    cond->token = tIf;
    cond->conditionpos = 0;
    cond->conditionlen = 0;
    cond->compstate = sMatch;
    conditions.append(cond);
    cond = new condition();

    for (;;) {
        tagpos = html.indexOf(QChar((char)tToken), pos);
        if (tagpos == -1)
            break; // no more conditions found, we're done

        nextcompstate = sNotset; // not set means ignore me
        cond->token = (tokens)html[tagpos + 1].unicode();
        if (cond->token == tIf) {
            computation = true;
        } else if (cond->token == tElse) {
            computation = false; // we can calculate its state right here
            lastcompstate = lastcompstate == sMatch ? sNomatch : sMatch;
        } else if (cond->token == tElseIf) {
            if (lastcompstate == sMatch) { // treat this as an ELSE that isn't used
                computation = false; // we can calculate its state right here
                lastcompstate = sNomatch;
                nextcompstate = sMatch; // any ELSEs after me should likewise not take effect
            } else {
                computation = true;
            }
        } else if (cond->token == tEndIf) {
            computation = false; // we can calculate its state right here
            lastcompstate = sMatch; // for the purpose of rendering, keep text after ENDIF
        } else {
            pos = tagpos + 1;
            continue; // something bad has happened to our string
        }
        cond->conditionpos = tagpos;

        if (computation) {
            compendpos = html.indexOf(QChar((char)tEnd), pos);
            cond->conditionlen = compendpos - tagpos + 1;
            compstates thisstate(sNotset);
            bool ending(false);
            tokens andor(tUnknown);
            tokens op(tUnknown);
            QString var;
            QString comp;
            lastcompstate = sNotset;
            // loop thru all operators to arrive at final state
            while (!ending) {
                comppos = html.indexOf(QChar((char)tToken), tagpos + 2);
                ending = comppos == -1 || comppos > compendpos;
                if (ending) {
                    comptoken = tEnd;
                    comppos = compendpos;
                } else {
                    comptoken = (tokens)html[comppos + 1].unicode();
                }
                comp = html.mid(tagpos + 2, comppos - tagpos - 2).toLower();

                // finish up previous business
                switch (op) {
                case tEq:
                    thisstate = var == comp ? sMatch : sNomatch;
                    break;
                case tNeq:
                    thisstate = var != comp ? sMatch : sNomatch;
                    break;
                case tLs:
                    thisstate = var < comp ? sMatch : sNomatch;
                    break;
                case tGr:
                    thisstate = var > comp ? sMatch : sNomatch;
                    break;
                case tLsEq:
                    thisstate = var <= comp ? sMatch : sNomatch;
                    break;
                case tGrEq:
                    thisstate = var >= comp ? sMatch : sNomatch;
                    break;
                case tHas:
                    thisstate = var.contains(comp) ? sMatch : sNomatch;
                    break;
                case tNhas:
                    thisstate = !var.contains(comp) ? sMatch : sNomatch;
                    break;
                case tEmpty:
                    thisstate = var.isEmpty() ? sMatch : sNomatch;
                    break;
                case tNempty:
                    thisstate = !var.isEmpty() ? sMatch : sNomatch;
                    break;
                case tNemptyDefault:
                    //special case: operator comes first, so var is empty and must use 'comp'
                    thisstate = !comp.isEmpty() ? sMatch : sNomatch;
                    break;
                case tAnd:
                case tOr:
                case tUnknown:
                    break;
                default:
                    pos = tagpos + 1;
                    continue; // something bad has happened to our string
                }

                if (comptoken == tAnd || comptoken == tOr || comptoken == tEnd) {
                    // finish up cached and/or
                    if (andor == tUnknown) {
                        lastcompstate = thisstate;
                    } else if (andor == tAnd) {
                        lastcompstate = ((lastcompstate == sMatch) & (thisstate == sMatch)) ? sMatch : sNomatch;
                    } else if (andor == tOr) {
                        lastcompstate = ((lastcompstate == sMatch) | (thisstate == sMatch)) ? sMatch : sNomatch;
                    }
                    andor = comptoken;
                }
                tagpos = comppos;
                op = comptoken;
                var = comp;
            }

            pos = compendpos + 1;
        } else {
            pos = tagpos + 1;
            cond->conditionlen = 2;
        }

        cond->compstate = lastcompstate;
        conditions.append(cond);
        if (nextcompstate != sNotset)
            lastcompstate = nextcompstate;

        cond = new condition();
    }

    // to make rendering easier, add a fake entry to cover the text after the last conditional
    pos = conditions.last()->conditionend();
    if (pos < html.length()) {
        cond->conditionpos = html.length();
        cond->conditionlen = 0;
        cond->compstate = sNomatch;
        conditions.append(cond);
    } else {
        delete cond;
    }
}

void printingconditionals::_expandtoelement()
{
    condition *cond;
    QList<bool> levelExpanded;
    levelExpanded.append(false);
    const int ccnt(conditions.count() - 1); // loop up to but not including last fake entry
    for (int i = 1; i < ccnt; ++i) {
        cond = conditions[i];

        if (cond->token == tIf) {
            // if the IF gets expanded, do so for all parts of its logic
            int checkForEnd = cond->conditionend();
            while (html[checkForEnd] == QChar(' '))
                checkForEnd++;
            levelExpanded.append(html.midRef(checkForEnd, 2) == "</");
        }

        if (levelExpanded.count() > 0 && levelExpanded.last()) {
            // extend boundaries to include whole element

            // backup starting point
            while (html[cond->conditionpos] != QChar('<')) {
                cond->conditionpos--;
                cond->conditionlen++;
            }

            // extend ending point
            while (html[cond->conditionend()] != QChar('>'))
                cond->conditionlen++;
            cond->conditionlen++;

            //QStringRef x(html.midRef(cond->conditionpos, cond->conditionlen));
            //x.data();
        }

        if (cond->token == tEndIf && levelExpanded.count() > 0) {
            levelExpanded.removeLast();
        }
    }
}

void printingconditionals::_render()
{
    /*
    QFile file("C:\\test\\output.txt");
    if(QFile::exists(file.fileName())){
        QFile::remove(file.fileName());
    }
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);
    */

    condition *cond;
    condition *nextcond;
    QList<compstates *> nestedstates;
    compstates okrender(sNotset);
    const int ccnt(conditions.count() - 1); // loop up to but not including last fake entry
    for (int i = 0; i < ccnt; ++i) {
        cond = conditions[i];
        nextcond = conditions[i + 1];
        okrender = cond->compstate;

        /*
        out << "\r\n";
        switch (cond->token) {
        case tIf:
            out << "IF\r\n";
            break;
        case tElseIf:
            out << "ELSEIF\r\n";
            break;
        case tElse:
            out << "ELSE\r\n";
            break;
        case tEndIf:
            out << "ENDIF\r\n";
            break;
        default:
            out << "Unknown\r\n";
            break;
        }
        out << " start=" << cond->conditionpos << " len=" << cond->conditionlen << "\r\n";
        if (cond->conditionlen > 0) {
            int maxcharacters(250);
            int overage(cond->conditionlen - maxcharacters);
            if (overage > maxcharacters)
                overage = maxcharacters;
            if (overage > 0)
                out << " conditiontext=" << html.mid(cond->conditionpos, maxcharacters) << "(..)" << html.mid(cond->conditionend() - overage, overage) << "\r\n";
            else
                out << " conditiontext=" << html.mid(cond->conditionpos, cond->conditionlen) << "\r\n";
        }
        out << " Initial State=" << (okrender == sMatch ? "Render" : "Skip") << "\r\n";
        */

        // check for pulling up
        if (cond->token != tIf && nestedstates.count() > 0) {
            nestedstates.removeLast();
            // out << " pulling up" << "\r\n";
        }

        if (cond->token == tEndIf && nestedstates.count() > 0)
            okrender = *(nestedstates.last());

        // must be ok with all nesting if we are to render
        const int scnt(nestedstates.count());
        for (int i = 0; i < scnt; ++i) {
            if (*(nestedstates[i]) == sNomatch) {
                okrender = sNomatch;
                // out << " level " << i << " says SKIP" << "\r\n";
                break;
            }
        }

        // check for going deeper
        if (cond->token == tIf || cond->token == tElseIf || cond->token == tElse) {
            nestedstates.append(&cond->compstate);
            // out << " go deeper" << "\r\n";
        }

        QStringRef txt(html.midRef(cond->conditionend(), nextcond->conditionpos - cond->conditionend()));
        /*
        if (txt.length() > 0) {
            int maxcharacters(250);
            int overage(txt.length() - maxcharacters);
            if (overage > maxcharacters)
                overage = maxcharacters;
            if (txt.length() > maxcharacters)
                out << " txt to render =" << txt.mid(0, maxcharacters).toString() << "(..)" << txt.mid(txt.length() - overage, overage).toString() << "\r\n";
            else
                out << " txt to render =" << txt.toString() << "\r\n";
        }
        */
        if (okrender == sMatch) {
            results.append(txt);
            // out << " rendered" << "\r\n";

            // remove empty rows (most likely a result of conditionals in single columns)
            QRegularExpression rx("<tr[^>]*>\\s*</tr>");
            int startRow = -1;
            while ((startRow = results.indexOf(rx)) > -1) {
                QRegularExpressionMatch match = rx.match(results);
                int endRow = startRow + match.capturedLength();
                results = results.mid(0, startRow) + results.mid(endRow);
            }
            // remove rows with one empty columns (same cause)
            QRegularExpression rx2("<tr[^>]*>\\s*<td[^>]*>\\s*</td>\\s*</tr>");
            startRow = -1;
            while ((startRow = results.indexOf(rx2)) > -1) {
                QRegularExpressionMatch match = rx2.match(results);
                int endRow = startRow + match.capturedLength();
                results = results.mid(0, startRow) + results.mid(endRow);
            }
        } // else
        // out << " NOT rendered" << "\r\n";
    }

    /*
    file.close();

    QFile file2("C:\\test\\output.html");
    if(QFile::exists(file2.fileName())){
        QFile::remove(file2.fileName());
    }
    file2.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out2(&file2);
    out2 << results;
    file2.close();
    */
}

printingconditionals::condition::condition()
    : token(tUnknown), conditionpos(-1), conditionlen(0), compstate(sNotset)
{
}

int printingconditionals::condition::conditionend()
{
    return conditionpos + conditionlen;
}
