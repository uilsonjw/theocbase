/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import net.theocbase.mobile 1.0
import net.theocbase 1.0

Popup {
    id: printPage
    modal: true
    parent: Overlay.overlay
    x: (parent.width - width) / 2
    y: (parent.height - height) / 2
    implicitWidth:  350 * _dpi
    implicitHeight: 500 * _dpi
    padding: 0

    property date weekDate
    property real _dpi: typeof dpi == "undefined" ? 1 : dpi
    property var modelTemplates: printController.templateList
    property bool showBusyIndicator: false

    property bool canPrintMidweekSchedule: accessControl.user.hasPermission(PermissionRule.CanPrintMidweekMeetingSchedule)
    property bool canPrintMidweekSlips: accessControl.user.hasPermission(PermissionRule.CanPrintMidweekMeetingAssignmentSlips)
    property bool canPrintMidweekWorksheet: accessControl.user.hasPermission(PermissionRule.CanPrintMidweekMeetingWorksheets)
    property bool canPrintWeekendSchedule: accessControl.user.hasPermission(PermissionRule.CanPrintWeekendMeetingSchedule)
    property bool canPrintWeekendWorksheet: accessControl.user.hasPermission(PermissionRule.CanPrintWeekendMeetingWorksheets)
    property bool canPrintOutgoingSchedule: accessControl.user.hasPermission(PermissionRule.CanPrintSpeakersSchedule)
    property bool canPrintOutgoingSlips: accessControl.user.hasPermission(PermissionRule.CanPrintSpeakersAssignments)
    property bool canPrintHospitality: accessControl.user.hasPermission(PermissionRule.CanPrintHospitality)
    property bool canPrintPublicTalkList: accessControl.user.hasPermission(PermissionRule.CanPrintPublicTalkList)
    property bool canPrintCombination: accessControl.user.hasPermission(PermissionRule.CanPrintMidweekMeetingSchedule) &&
                                       accessControl.user.hasPermission(PermissionRule.CanPrintWeekendMeetingSchedule) &&
                                       accessControl.user.hasPermission(PermissionRule.CanPrintSpeakersSchedule) &&
                                       accessControl.user.hasPermission(PermissionRule.CanPrintHospitality)

    onOpened: {
        printController.initDate(weekDate)
        console.log(printController.fromDate)
        console.log(printController.toDate)
    }

    PrintController {
        id: printController
        fromDate: weekDate
        onTypeChanged: {
            modelTemplates = printController.templateList
            if (modelTemplates.length > 0) {
                printController.templateName = modelTemplates[0]
            }
        }
        onPreviewReady: {
            showBusyIndicator = false
        }
    }

    ToolBar {
        id: toolbar
        width: parent.width
        RowLayout {
            anchors.fill: parent
            ToolButton {
                icon.source: stack.depth > 1 ? "qrc:/back.svg" : "qrc:/cancel.svg"
                icon.color: "white"
                onClicked: {
                    if (stack.depth > 1)
                        stack.pop()
                    else
                        printPage.close()
                }
            }
            Label {
                text: qsTr("Print Options")
                horizontalAlignment: Qt.AlignHCenter
                Layout.fillWidth: true
                color: "white"
            }
            Item { Layout.fillWidth: true }
        }
    }

    StackView {
        id: stack
        anchors.fill: parent
        anchors.topMargin: toolbar.height
        initialItem: optionsPage
    }

    Component {
        id: optionsPage
        Page {
            implicitWidth: 400
            implicitHeight: 500

            ColumnLayout {
                anchors.fill: parent
                anchors.topMargin: 10 *_dpi
                spacing: 0
                RowTitle {
                    text: printController.fromDate.toLocaleDateString(Qt.locale(), Locale.ShortFormat) + " - " +
                          printController.toDate.toLocaleDateString(Qt.locale(), Locale.ShortFormat)
                }

                ListView {
                    id: listTypeGroup
                    width: contentWidth
                    Layout.preferredHeight: 120
                    Layout.alignment: Qt.AlignHCenter
                    Layout.preferredWidth: contentWidth
                    orientation: Qt.Horizontal
                    currentIndex: 0
                    onCurrentIndexChanged: {
                        switch(currentIndex) {
                        case 0:
                            radioMWSchedule.checked = true
                            break
                        case 1:
                            radioWESchedule.checked = true
                            break
                        case 2:
                            printController.templateType = TemplateData.CombinationTemplate
                        }
                    }

                    model: ListModel {
                        ListElement {
                            name: qsTr("Midweek Meeting")
                        }
                        ListElement {
                            name: qsTr("Weekend Meeting")
                        }
                        ListElement {
                            name: qsTr("Combination", "Print template")
                        }
                    }

                    delegate: Rectangle {
                        id: rectangle
                        height: 100 * _dpi
                        width: 100 * _dpi
                        color: "transparent"

                        Rectangle {
                            anchors.fill: parent
                            anchors.margins: 10
                            radius: 5
                            border.color: labelName.color
                            border.width: index === listTypeGroup.currentIndex ? 2 : 1
                            color: "transparent"
                        }

                        Label {
                            id: labelName
                            text: name
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                            anchors.fill: parent
                            anchors.margins: 10
                            wrapMode: Text.WordWrap
                        }
                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                listTypeGroup.currentIndex = index
                            }
                        }
                    }
                }

                ScrollView {
                    id: scrollView
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    contentWidth: availableWidth
                    clip: true
                    ColumnLayout {
                        width: stack.availableWidth - 20
                        spacing: 0
                        RadioButton {
                            id: radioMWSchedule
                            text: qsTr("Schedule")
                            visible: listTypeGroup.currentIndex == 0
                            checked: true
                            onCheckedChanged: if (checked)
                                                  printController.templateType = TemplateData.MidweekScheduleTemplate
                        }
                        RadioButton {
                            id: radioMWSlip
                            text: qsTr("Assignment Slips")
                            visible: listTypeGroup.currentIndex == 0
                            onCheckedChanged: if (checked)
                                                  printController.templateType = TemplateData.MidweekSlipTemplate
                        }

                        RadioButton {
                            id: radioMWWorkSheet
                            text: qsTr("Worksheets")
                            visible: listTypeGroup.currentIndex == 0
                            onCheckedChanged:  if (checked)
                                                   printController.templateType = TemplateData.MidweekWorksheetTemplate
                        }

                        RadioButton {
                            id: radioWESchedule
                            text: qsTr("Schedule")
                            visible: listTypeGroup.currentIndex == 1
                            onCheckedChanged: if (checked)
                                                  printController.templateType = TemplateData.PublicMeetingTemplate
                        }
                        RadioButton {
                            id: radioWEWorkSheet
                            text: qsTr("Worksheets")
                            visible: listTypeGroup.currentIndex == 1
                            onCheckedChanged: if (checked)
                                                  printController.templateType = TemplateData.PublicMeetingWorksheet
                        }
                        RadioButton {
                            id: radioOutgoingSchedule
                            text: qsTr("Outgoing Speakers Schedule")
                            visible: listTypeGroup.currentIndex == 1
                            onCheckedChanged: if (checked)
                                                  printController.templateType = TemplateData.OutgoingScheduleTemplate
                        }
                        RadioButton {
                            id: radioOutgoingSlips
                            text: qsTr("Outgoing Speakers Assignments")
                            visible: listTypeGroup.currentIndex == 1
                            onCheckedChanged: if (checked)
                                                  printController.templateType = TemplateData.OutgoingSlipTemplate
                        }
                        RadioButton {
                            id: radioHospitality
                            text: qsTr("Call List and Hospitality Schedule")
                            visible: listTypeGroup.currentIndex == 1
                            onCheckedChanged: if (checked)
                                                  printController.templateType = TemplateData.HospitalityTemplate
                        }
                        RadioButton {
                            id: radioTalksOfSpeakers
                            text: qsTr("Talks of Speakers")
                            visible: listTypeGroup.currentIndex == 1
                            onCheckedChanged: if (checked)
                                                  printController.templateType = TemplateData.TalksOfSpeakersTemplate
                        }
                    }
                }


                RowText {
                    arrow: true
                    title: qsTr("Template", "Print template")
                    text: printController.templateName
                    onClicked: stack.push(templatePage)
                }
                CheckBox {
                    id: checkBoxAssistantSlip
                    text: qsTr("Assignment Slips for Assistant")
                    visible: radioMWSlip.checked
                    checked: printController.assistantSlip
                    onToggled: printController.assistantSlip = checked
                }
                CheckBox {
                    id: checkBoxBlank
                    text: qsTr("Print Assigned Only", "Assignment slip printing")
                    visible: radioMWSlip.checked
                    checked: printController.assignedOnly
                    onToggled: printController.assignedOnly = checked
                }
                RowLayout {
                    Label {                        
                        text: qsTr("Text size")
                        font.pointSize: app_info.fontsize
                        Layout.leftMargin: 10 * _dpi
                    }

                    Slider {
                        Layout.fillWidth: true
                        from: -5
                        to: 5
                        value: printController.fontSize
                        stepSize: 1
                        onMoved: printController.fontSize = value
                    }
                }


                Button {
                    id: printButton
                    Layout.preferredWidth: 200 * _dpi
                    text: qsTr("Print")                    
                    Layout.alignment: Qt.AlignHCenter
                    enabled: listTypeGroup.currentIndex == 0 && (
                                 radioMWSchedule.checked && canPrintMidweekSchedule ||
                                 radioMWWorkSheet.checked && canPrintMidweekWorksheet ||
                                 radioMWSlip.checked && canPrintMidweekSlips) ||
                             listTypeGroup.currentIndex == 1 && (
                                 radioWESchedule.checked && canPrintWeekendSchedule ||
                                 radioWEWorkSheet.checked && canPrintWeekendWorksheet ||
                                 radioOutgoingSchedule.checked && canPrintOutgoingSchedule ||
                                 radioOutgoingSlips.checked && canPrintOutgoingSlips ||
                                 radioHospitality.checked && canPrintHospitality ||
                                 radioTalksOfSpeakers.checked && canPrintPublicTalkList) ||
                             listTypeGroup.currentIndex === 2 && canPrintCombination
                    onClicked: {
                        console.log(printController.templateName)
                        printPage.showBusyIndicator = true
                        printController.print()
                    }
                }
            }
        }
    }

    Component {
        id: templatePage
        Page {
            implicitWidth: 400
            implicitHeight: 500
            ListView {
                id: listTemplates
                model: printPage.modelTemplates
                anchors.fill: parent
                currentIndex: 0
                delegate: RadioDelegate {
                    width: stack.width
                    text: modelData
                    checked: printController.templateName == text
                    onToggled: {
                        console.log(checked + " " + modelData)
                        if (checked) {
                            printController.templateName = modelData
                        }
                    }
                }
            }
        }
    }

    BusyIndicator {
        id: busy
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.centerIn: parent.Center
        running: showBusyIndicator
    }
}
