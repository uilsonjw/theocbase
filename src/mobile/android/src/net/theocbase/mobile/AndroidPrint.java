/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.theocbase.mobile;

import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.ValueCallback;
import org.qtproject.qt5.android.bindings.QtActivity;
import org.qtproject.qt5.android.bindings.QtApplication;
import android.app.Activity;
import android.content.Intent;
import android.content.Context;
import android.os.Bundle;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import android.print.PrintAttributes;
import android.print.PrintAttributes.MediaSize;
import android.print.PrintDocumentAdapter;
import android.print.PrintManager;

public class AndroidPrint extends QtActivity {
    public static native void pageLoaded();
    private static Activity activity = null;
    private static Context mainContext;
    private static WebView mWebView;
    private static PrintAttributes.MediaSize mMediaSize;

    public AndroidPrint()
    {
        Log.d("AndroidPrint::AndroidPrint", "Constuctor");
    }

    public static void PrintHTML(Activity act, String htmlDoc, PrintAttributes.MediaSize mediaSize, int fSize)
    {
        final int fontSize = fSize;
        activity = act;
        mMediaSize = mediaSize;

        WebView webView = new WebView(act);
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
            @Override
            public void onPageFinished(WebView view, String url) {
                Log.d("Page finished loading " + url, "onpagefinished");
                if (fontSize == 0) {
                    pageLoaded();
                    createWebPrintJob(view);
                } else {
                    // adjust font size
                    String script = String.format("document.documentElement.style.fontSize = (parseInt(window.getComputedStyle" +
                        "(document.documentElement).getPropertyValue('font-size') )+%d).toString()+'px'", fontSize);                        
                    view.evaluateJavascript(script, new ValueCallback<String>() {
                        @Override
                        public void onReceiveValue(String s) {                            
                            pageLoaded();
                            createWebPrintJob(mWebView);
                        }
                    });
                }
            }
        });
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadDataWithBaseURL(null, htmlDoc, "text/HTML", "UTF-8", null);
        mWebView = webView;
    }

    private static void createWebPrintJob(WebView webView) {
           //create object of print manager in your device
           PrintManager printManager = (PrintManager) activity.getSystemService(Context.PRINT_SERVICE);

           //create object of print adapter
           PrintDocumentAdapter printAdapter = webView.createPrintDocumentAdapter();

           //provide name to your newly generated pdf file
           String jobName = "HTML Print";

           PrintAttributes.Builder builder = new PrintAttributes.Builder();
           builder.setMediaSize(mMediaSize);
           PrintAttributes.Margins margins = PrintAttributes.Margins.NO_MARGINS;
           builder.setMinMargins(margins);

           //open print dialog
           printManager.print(jobName, printAdapter, builder.build());
    }

    public static void PrintPDF(Activity act, String path, PrintAttributes.MediaSize mediaSize) {
        activity = act;
        mMediaSize = mediaSize;

        PrintManager printManager = (PrintManager) activity.getSystemService(Context.PRINT_SERVICE);
        PrintDocumentAdapter printAdapter = new PdfDocumentAdapter(activity, path);
        PrintAttributes.Builder builder = new PrintAttributes.Builder();
        builder.setMediaSize(mMediaSize);
        builder.setMinMargins(PrintAttributes.Margins.NO_MARGINS);
        printManager.print("PDF Print", printAdapter, builder.build());
    }
}


