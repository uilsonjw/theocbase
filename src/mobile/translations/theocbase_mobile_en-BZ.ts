<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en-BZ">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Noh asain di neks stodi point</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Kantinyu pahn di sayhn stodi point</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Rang dayta</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Sayv di taim?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Teem </translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Mateeryal</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Styoodent</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Helpa </translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Rizolt</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Finish</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Valantyaa</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Pik wahn valantyaa</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Stodi Point</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Eksasaiz Kompleet</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Neks Stodi Point</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Pik di neks stodi point</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Taimin</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Noats</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Deetaylz</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Staat di klak</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Stap di taim</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Setin</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Pik wan setin</translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>No exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit overseer&#x27;s visit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit assembly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Regional convention</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Memorial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Other exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Teem</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Mateeryal</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Kandokta</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Speeka</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Reeda</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Noats</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <comment>Page title</comment>
        <translation>Noats</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>CHREZHAZ FAHN DI WERD A GAAD</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>WERK HAAD FI DU BETA EENA FEEL SERVIS</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>LIV YU LAIF AZ WAHN KRISTYAN</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Chyaaman</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Kongsla</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Sang %1 ahn Pryaa</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Sang</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Kandokta</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Reeda</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Inchrodokshan</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Pryaa</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Kantinyu pahn di sayhn stodi point</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Staat di klak</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Stap di taim</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Sayv di taim</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Teem</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Mateeryal</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Styoodent</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Rizolt</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Kompleet</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Taimin</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Stodi Point</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Eksasaiz Kompleet</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Neks Stodi Point</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Pik di neks stodi point</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Noats</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Helpa</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Valantyaa</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>Stapwach</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>Di asistant noh fi bee sohnbadi fahn di opozit seks.</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Infamayshan</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>Swaip dong fi rifresh...</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Let goh fi rifresh...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation>Yooza naym</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Paaswerd</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Lag een</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Yooza Naym er Eemayl Ajres</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Eemayl</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Kriyayt Wahn Akonk</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>Set Wahn Nyoo Paaswerd</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>Da e-mayl ajres noh deh eena di daytabays!</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Klik ya if yu faget yu paaswerd</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>Lag Een Payj</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation>Week weh staat %1</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Skejul</translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation>Worksheets</translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation>Outgoing Speakers Schedule</translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Print</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Text size</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Sang ahn Pryaa</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Sang %1 ahn Pryaa</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>POBLIK TAAK</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>WACHTOWA STODI</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Sang %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Kandokta</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Reeda</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Phone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>Naym</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Taitl</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Brada</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Sista</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Servant</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Famili</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>Hoo da ih famili hed</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Kantak Infamayshan</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Foan</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Eemayl</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>Eni Klaasroom</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Onli di Mayn Klaasroom</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Onli di Agzilari Klaasroom</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Famili hed</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Helpa</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>Aktiv</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Chyaaman</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>Chrezhaz Fahn di Werd a Gaad</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Baibl Reedin</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Fers Kaal</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Ritern Vizit</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Baibl Stodi</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>Liv Yu Laif Az Wahn Kristyan Taak</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Kangrigayshan Baibl Stodi</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Kang. Baibl Stodi Reeda</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>Nyoo Poblisha</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Pryaa</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Poblishaz</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Opshanz</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Lag owt</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Verzhan</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>TheocBase Websait</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Feedbak</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>Laas bakop: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Skejul</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Shoa weh taim ih staat</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Shoa how lang</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Yooza Intafays</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Langwij</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Lag Een</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-mayl</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Naym</translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Printing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Di Sinkronaiz</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Song</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>Taimlain</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>Humoch week fi shoa bifoa di dayt weh yu pik</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>Humoch week fi shoa afta di dayt weh yu pik</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>week</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>Dehnya chaynjis yu ku fain eena yu divais ahn di klowd (%1 rows). Yu waahn kip di chaynjiz weh yu mek eena dis divais?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>Di infamayshan eena di klowd don riset. Di infamayshan pahn yu divais wahn geh riplays. Kantinyu?</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Noh asain di neks stodi point</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>Noh set</translation>
    </message>
</context></TS>