#include "printutil.h"
#include <QFile>
#include <QApplication>
#include <QtAndroid>
#include <QAndroidJniEnvironment>

PrintUtil* PrintUtil::mInstance = nullptr;

PrintUtil::PrintUtil(QObject *parent) : QObject(parent)
{
    mInstance = this;
}

void PrintUtil::printHTML(QString content, QPageLayout layout, int fontSizeFactory)
{
    QString sizeName = "";
    switch (layout.pageSize().id()) {
    case QPageSize::A4:
        sizeName = "ISO_A4";
        break;
    case QPageSize::A6:
        sizeName = "ISO_A6";
        break;
    case QPageSize::Letter:
        sizeName = "NA_LETTER";
        break;
    case QPageSize::Legal:
        sizeName = "NA_LEGAL";
        break;
    case QPageSize::Tabloid:
        sizeName = "NA_TABLOID";
        break;
    default:
        sizeName = "ISO_A4";
        break;
    }

    // Page size
    QAndroidJniObject mediaSize = QAndroidJniObject::getStaticObjectField("android/print/PrintAttributes$MediaSize",
                                                                          sizeName.toLocal8Bit().data(),
                                                                          "Landroid/print/PrintAttributes$MediaSize;");
    // Page orientation
    if (layout.orientation() == QPageLayout::Landscape) {
        mediaSize = mediaSize.callObjectMethod("asLandscape", "()Landroid/print/PrintAttributes$MediaSize;");
    }

    QAndroidJniObject javaControl;
    QtAndroid::runOnAndroidThreadSync([&javaControl, content, mediaSize, fontSizeFactory]() {
        javaControl.callStaticMethod<void>("net/theocbase/mobile/AndroidPrint",
                                           "PrintHTML",
                                           "(Landroid/app/Activity;Ljava/lang/String;Landroid/print/PrintAttributes$MediaSize;I)V",
                                           QtAndroid::androidActivity().object(),
                                           QAndroidJniObject::fromString(content).object<jstring>(),
                                           mediaSize.object(),
                                           fontSizeFactory);
    });
}

void PrintUtil::printPDF(QByteArray b, QPageLayout layout)
{
    QString sizeName = "";
    if (layout.pageSize().id() == QPageSize::A4)
        sizeName = "ISO_A4";
    else
        sizeName = "ISO_A6";

    // Page size
    QAndroidJniObject mediaSize = QAndroidJniObject::getStaticObjectField("android/print/PrintAttributes$MediaSize",
                                                                          sizeName.toLocal8Bit().data(),
                                                                          "Landroid/print/PrintAttributes$MediaSize;");

    mediaSize = mediaSize.callObjectMethod("asPortrait", "()Landroid/print/PrintAttributes$MediaSize;");

    QAndroidJniObject javaControl;
    QtAndroid::runOnAndroidThreadSync([&javaControl, b, mediaSize]() {
        QString tempDir = QStandardPaths::writableLocation(QStandardPaths::TempLocation);
        QFile file(tempDir + "/pdfprint.pdf");
        if (file.exists())
            file.remove();
        if (file.open(QIODevice::WriteOnly)) {
            file.write(b);
            file.close();
            QFileInfo info(file);
            QString fullPath = info.filePath();
            javaControl.callStaticMethod<void>("net/theocbase/mobile/AndroidPrint",
                                               "PrintPDF",
                                               "(Landroid/app/Activity;Ljava/lang/String;Landroid/print/PrintAttributes$MediaSize;)V",
                                               QtAndroid::androidActivity().object(),
                                               QAndroidJniObject::fromString(fullPath).object<jstring>(),
                                               mediaSize.object());
        }
    });
}

PrintUtil *PrintUtil::getInstance()
{
    if (!mInstance)
        mInstance = new PrintUtil();
    return mInstance;
}

void PrintUtil::showPrint(QByteArray data)
{
    Q_UNUSED(data);
}

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT void JNICALL
    Java_net_theocbase_mobile_AndroidPrint_pageLoaded(JNIEnv *env,
                                                 jobject obj)
{
    Q_UNUSED(env);
    Q_UNUSED(obj);
    emit PrintUtil::getInstance()->pageReady();
}
#ifdef __cplusplus
}
#endif
