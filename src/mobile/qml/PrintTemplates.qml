/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.12
import QtQuick.Controls 2.12
import QtGraphicalEffects 1.0
import Qt.labs.folderlistmodel 2.12
import Qt.labs.platform 1.1
import net.theocbase.mobile 1.0

Page {
    id: printTemplatesPage

    property string templatePath: StandardPaths.writableLocation(StandardPaths.AppDataLocation) + "/custom_templates"

    Connections {        
        target: shareUtils
        function onReceivedUrlChanged() { importFile() }
    }    
    Connections {
        target: Qt.application
        function onStateChanged(state) { if (state === Qt.ApplicationActive) importFile() }
    }
    MsgBox { id: _msg }
    // PrintController initialization is needed in order to create 'custom_template' folder if it does not exist
    PrintController {}
    function importFile() {
        if (shareUtils.receivedUrl !== "") {
            console.log("ready: " + shareUtils.receivedUrl)
            var fileName = shareUtils.receivedUrl
            fileName = fileName.substring(fileName.lastIndexOf('/')+1)
            if (/^.*(.pdf)$/.test(fileName)) {
                _msg.show(qsTr("Custom Templates"), "PDF file is not supported. Convert pdf to jpg and try again.")
            } else {
                var destFile = templatePath + "/" + fileName
                shareUtils.copyFile(shareUtils.receivedUrl, destFile)
            }
            shareUtils.receivedUrl = ""
        }
    }

    header: BaseToolbar {
        title: qsTr("Custom Templates")
        componentLeft: ToolButton {
            icon.source: "qrc:/back.svg"
            icon.color: "white"
            onClicked: stackView.pop()
        }
        componentRight: ToolButton {
            icon.source: "qrc:/import.svg"
            icon.color: "white"
            onClicked: {
                shareUtilsConnection.enabled = false
                shareUtils.openFile("*")
            }
        }
    }


    ListView {
        id: listView
        anchors.fill: parent
        model: FolderListModel {
            id: folderModel
            folder:  templatePath
            nameFilters: ["*.*"]
            Component.onCompleted: console.log(templatePath)
        }
        delegate: SwipeDelegate {
            text: model.fileName
            width: parent.width

            swipe.right: Rectangle {
                id: deleteBox
                color: "red"
                height: parent.height
                width: 80*dpi
                anchors.right: parent.right
                Image {
                    id: deleteIcon
                    anchors.fill: parent
                    anchors.margins: 5*dpi
                    source: "qrc:/delete.svg"
                    fillMode: Image.PreserveAspectFit
                    sourceSize: Qt.size(60,60)
                }
                ColorOverlay {
                    anchors.fill: deleteIcon
                    source: deleteIcon
                    color: "white"
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        shareUtils.deleteFile(model.filePath)
                    }
                }
            }
        }

    }
}
