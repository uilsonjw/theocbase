<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hi">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation>नाम</translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation>तारीख</translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>विषय</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>स्रोत</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>विद्यार्थी</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>सहायक</translation>
    </message>
    <message>
        <source>Result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>स्वयंसेवक</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>अगला गुण</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>समय</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>नोट्स</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>सैटिंग</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>सैटिंग चुनिए</translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>No exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit overseer&#x27;s visit</source>
        <translation>सर्किट निगरान का दौरा</translation>
    </message>
    <message>
        <source>Circuit assembly</source>
        <translation>सर्किट सम्मेलन</translation>
    </message>
    <message>
        <source>Regional convention</source>
        <translation>क्षेत्रीय अधिवेशन</translation>
    </message>
    <message>
        <source>Memorial</source>
        <translation>स्मारक</translation>
    </message>
    <message>
        <source>Other exception</source>
        <translation>अन्य खास मौका</translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation>%1 से शुरू होनेवाला हफ्ता</translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation>सभा के दिन</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>हफ्ते के बीच होनेवाली सभा</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>हफ्ते के आखिर में होनेवाली सभा</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>विषय</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>स्रोत</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>संचालक</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>वक्ता</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>पढ़नेवाला</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>नोट्स</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>अधिक जानकारी</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <comment>Page title</comment>
        <translation>नोट्स</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>पाएँ बाइबल का खज़ाना</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>बढ़ाएँ प्रचार में हुनर</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>जीएँ मसीहियों की तरह</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>चेयरमैन</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>सलाहकार</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>गीत %1 और प्रार्थना</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>गीत</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>संचालक</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>पढ़नेवाला</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>सभा की एक झलक</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>प्रार्थना</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation>मुख्य</translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation>अ1</translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation>अ2</translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation>समाप्ति के चंद शब्द</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>विषय</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>स्रोत</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>विद्यार्थी</translation>
    </message>
    <message>
        <source>Result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>समय</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>अभियास पूरा</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>अगला गुण</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>नोट्स</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>सहायक</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>स्वयंसेवक</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>गुण</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>अधिक जानकारी</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Login</source>
        <translation>लॉग-इन</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>लॉग-इन पेज</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation>%1 से शुरू होनेवाला हफ्ता</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation>बाहर जानेवाले वक्ता</translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>%1 वक्ता इस हफ्ते बाहर गए है</numerusform>
            <numerusform>%1 वक्ता इस हफ्ते बाहर गए है</numerusform>
        </translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>शेड्यूल</translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation>बाहर जानेवाले वक्ता का शेड्यूल</translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation>बाहर जानेवाले वक्ता के भाग</translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation>वक्ता के भाषण</translation>
    </message>
    <message>
        <source>Print</source>
        <translation>प्रिंट</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>हफ्ते के बीच होनेवाली सभा</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>हफ्ते के आखिर में होनेवाली सभा</translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation>कॉम्बिनेशन</translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Text size</source>
        <translation>अक्षरों का साइज़</translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>गीत और प्रार्थना</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>गीत %1 और प्रार्थना</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>जन भाषण</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>प्रहरीदुर्ग अध्ययन</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>गीत %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>संचालक</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>पढ़नेवाला</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>विषय</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>मंडली</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>वक्ता</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>मोबाईल</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>फोन</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>जानकारी</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>मेज़बान</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>जन भाषण</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>पहला नाम</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>उपनाम</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>भाई</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>बहन</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>सेवक</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>परिवार</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>इस परिवार का सदस्य</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>संपर्क जानकारी</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>फोन</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>सब क्लास</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>सिर्फ मुख्य क्लास</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>सिर्फ अतिरिक्त क्लास</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>परिवार का मुख्य</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>सहायक</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>जानकारी</translation>
    </message>
    <message>
        <source>Active</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>चेयरमैन</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>पाएँ बाइबल का खज़ाना</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>पढ़ने के लिए आयतें</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>पहली मुलाकात</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>वापसी भेंट</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>बाइबल अध्ययन</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>जीएँ मसीहियों की तरह भाग</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>मंडली का बाइबल अध्ययन</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>मंडली का बाइबल अध्ययन पढ़नेवाला</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>नया प्रचारक</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>प्रार्थना</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation>मेहमान वक्ता के लिए मेज़बान</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>मोबाईल</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>हफ्ते के बीच होनेवाली सभा</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>भाषण</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>हफ्ते के आखिर में होनेवाली सभा</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>प्रहरीदुर्ग अध्ययन संचालक</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation>प्रहरीदुर्ग अध्ययन पढ़नेवाला</translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation>अनमोल रत्न</translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation>सवाल-जवाब और वीडियो</translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>प्रचारक</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>सेटिंग्ज़</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation>जानकारी</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>वर्ज़न</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>TheocBase मुख्य पेज</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>शेड्यूल</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Language</source>
        <translation>भाषा</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>लॉग-इन</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>नाम</translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation>गीत विषय दिखाइए</translation>
    </message>
    <message>
        <source>Printing</source>
        <translation>प्रिंटिंग</translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>चेयरमैन</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>गीत</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation>हफ्ते के आखिर में होनेवाली सभा का चेयरमैन</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>गीत</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation>प्रहरीदुर्ग का गीत</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>प्रार्थना</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation>प्रहरीदुर्ग लेख</translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>लेख</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>विषय</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>संचालक</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>पढ़नेवाला</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>हफ्ते</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>अगला गुण न सौंपिए</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>सेट नहीं</translation>
    </message>
</context></TS>