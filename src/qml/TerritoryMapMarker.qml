import QtQuick 2.5
import QtLocation 5.6
import QtGraphicalEffects 1.0
import QtPositioning 5.8
import QtQuick.Controls 1.4

SimpleMarker {
    id: myMapMarker

    visible: false

    property int territoryId
    property int addressId
    property bool isTerritorySelected: false
    property int showMarker
    property var geocodeResult

    states: [
        State {
            name: "Unassigned"; when: territoryId == 0
            PropertyChanges { target: myMapMarker; opacity: 1; visible: true; color: 'black' }
        },
        State {
            name: "IsTerritorySelected"; when: isTerritorySelected && showMarker >= 1
            PropertyChanges { target: myMapMarker; opacity: 1; visible: true }
        },
        State {
            name: "IsNotSelected"; when: !isTerritorySelected && showMarker === 2
            PropertyChanges { target: myMapMarker; opacity: 0.5; visible: true }
        },
        State {
            name: "Hidden"; when: !isTerritorySelected && showMarker < 2
            PropertyChanges { target: myMapMarker; opacity: 0; visible: false }
        }
        ]

    markerMouseArea.onClicked: {
        if (mouse.button === Qt.RightButton) {
            contextMenu.show(myMapMarker, myMapMarker.territoryId, myMapMarker.addressId)
            return
        }

        if (addressId > 0)
        {
            if (territoryTreeModel.selectedTerritory === null ||
                    territoryTreeModel.selectedTerritory.territoryId !== territoryId)
            {
                var territoryIndex = territoryTreeModel.index(territoryId)
                territoryTreeModel.setSelectedTerritory(territoryIndex)
            }
            var addressIndex = addressModel.getAddressIndex(addressId)
            addressModel.setSelectedAddress(addressIndex)
        }
    }

    isMarkerDragTarget: map.editMode === 1
    markerMouseArea.onReleased: {
        if (map.editMode === 1) {
            addressModel.updateAddress(addressId, "POINT(" + coordinate.longitude + " " + coordinate.latitude + ")")
        }
    }

    Connections {
        target: parent
        function onRefreshTerritoryMapMarker(showMarkers) {
            showMarker = showMarkers
        }
    }

    Connections {
        target: territoryTreeModel
        function onSelectedChanged() {
            if (territoryTreeModel.selectedTerritory)
            {
                isTerritorySelected = territoryTreeModel.selectedTerritory.territoryId === territoryId ? true : false
            }
            else
                isTerritorySelected = false
        }
    }

    Connections {
        target: addressModel
        function onSelectedChanged() {
            if (addressModel.selectedAddress)
                isMarkerSelected = addressModel.selectedAddress.id === addressId ? true : false
            else
                isMarkerSelected = false
        }
    }

    Component.onCompleted: {
        if (territoryTreeModel.selectedTerritory)
        {
            isTerritorySelected = territoryTreeModel.selectedTerritory.territoryId === territoryId ? true : false
        }
        else
            isTerritorySelected = false
    }
}
