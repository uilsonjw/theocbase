<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ro">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation>Numele</translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation>Date</translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Nu aloca lecția următoare</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Continuă cu aceeași lecție</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Date incorecte</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Adaugi durata?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Sursa</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Cursant</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Asistent</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Rezultat</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Finalizat</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Voluntar</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Selectează un voluntar</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Lecția Curentă</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Exercițiu Finalizat</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Lecția Următoare</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Selectează lecția următoare</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Durata</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notițe</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Programul Şcolii</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Pornește cronometrul</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Oprește cronometrul</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Cadrul</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Selectează cadrul</translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>No exception</source>
        <translation>Nici o Excepție</translation>
    </message>
    <message>
        <source>Circuit overseer&#x27;s visit</source>
        <translation>Vizita Supraveghetorului de Circumscripție</translation>
    </message>
    <message>
        <source>Circuit assembly</source>
        <translation>Congres de Circumsripție</translation>
    </message>
    <message>
        <source>Regional convention</source>
        <translation>Congres Regional</translation>
    </message>
    <message>
        <source>Memorial</source>
        <translation>Comemorare</translation>
    </message>
    <message>
        <source>Other exception</source>
        <translation>Alte excepții</translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation>Săptămâna care începe la %1</translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation>Zile când sunt întruniri</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Întrunirea de pe parcursul săptămânii</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Întrunirea de la sfârșitul săptămânii</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Sursa</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Conducător</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Vorbitor</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Cititor</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notițe</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Detalii</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <comment>Page title</comment>
        <translation>Notițe</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>COMORI DIN CUVÂNTUL LUI DUMNEZEU</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>SĂ FIM MAI EFICIENȚI ÎN PREDICARE</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>VIAȚA DE CREȘTIN</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Președintele întrunirii</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Sfaturi</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Cântarea %1 și Rugăciunea</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Cântarea</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Conducător</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Cititor</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Cuvinte Introductive</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Rugăciune</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation>Importă Planificarea...</translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation>SP</translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation>A1</translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation>A2</translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation>Comentarii finale</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Continuă cu aceeași lecție</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Pornește cronometrul</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Oprește cronometrul</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Dorești să adaugi durata?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Sursa</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Cursant</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Rezultat</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Finalizat</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Durata</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Lecția Curentă</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Exercițiu Finalizat</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Lecția Următoare</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Selectează lecția următoare</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notițe</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Asistent</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Voluntar</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>Cronometru</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>Asistentul nu poate fi de sex diferit</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>Lecția la care lucrează</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Detalii</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>Trage pentru actualizare...</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Eliberează pentru actualizare...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation>Nume utilizator</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Parolă</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Conectare</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Utilizator sau Email</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Creează Cont</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>Resetează Parola</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>Adresa de e-mail n-a fost găsită!</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Am uitat parola</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>Pagina de conectare</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation>Săptămâna care începe la %1</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation>Vorbitorii Plecați</translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>Un vorbitor este plecat în acest sfârșit de săptămână</numerusform>
            <numerusform>%1 vorbitori plecați în acest sfârșit de săptămână</numerusform>
            <numerusform>%1 vorbitori plecați în acest sfârșit de săptămână</numerusform>
        </translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation>Nici un vorbitor plecat în acest sfârșit de săptămână</translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation>Tipărire Opțiuni</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Planificare</translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation>Foi de lucru</translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation>Programarea vorbitorilor trimiși</translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation>Responsabilitate Vorbitor Plecat</translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation>Lista numerelor de telefon și planificarea ospitalități</translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation>Discursurile Vorbitorilor</translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Imprimă</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Întrunirea de pe parcursul săptămânii</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Întrunirea de la sfârșitul săptămânii</translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation>Combinație</translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation>Fişa temei</translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation>Model</translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation>Fișa temei pentru cursant</translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation>Printează doar cele alocate</translation>
    </message>
    <message>
        <source>Text size</source>
        <translation>Mărimea textului</translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation>Model personalizat</translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Cântarea și Rugăciunea</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Cântarea %1 și Rugăciunea</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>DISCURSUL PUBLIC</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>STUDIUL TURNULUI DE VEGHE</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Cântarea %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Conducător</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Cititor</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation>Importă Turnul de Veghe...</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>Congregația</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Vorbitor</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Mobil</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Informații</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Gazdă</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>Discursul Public</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>Nume</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Prenume</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Frate</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Soră</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Numit</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Familie</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>Membrul familiei adăugat la</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Datele de contact</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>Toate clasele</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Numai la clasa principală</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Numai la clasele secundare</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Capul Familiei</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Asistent</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Informaţii</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>Activ</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Președintele întrunirii</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>Comori din Cuvântul lui Dumnezeu</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Citirea Bibliei</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Vizita Inițială</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Vizita Ulterioară</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Studiu Biblic</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>Viața de Creștin</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Studiul Bibliei in Congregație</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Cititor la Studiul Bibliei în Congregație</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>Vestitor nou</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Rugăciune</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation>Ospitalitate</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Mobil</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Întrunirea de pe parcursul săptămânii</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>Cuvântare</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Întrunirea de la sfârșitul săptămânii</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>Conducătorul Turnului de Veghe</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation>Cititor la Turnul de Veghe</translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation>Nestemate Spirituale</translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Vestitori</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation>Listă de selecție</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Setări</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Deconectare</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Informații</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Versiunea</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>Pagina principală TheocBase</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Trimite feedback</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>Ultima sincronizare: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Planificare</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Arată Ora</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Arată Durata</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Interfață utilizator</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Limbă</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Conectare</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Numele</translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation>Arată titlul cântărilor</translation>
    </message>
    <message>
        <source>Printing</source>
        <translation>Tipărire</translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation>Model personalizat</translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Sincronizare...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>Preşedintele Întruniri</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Cântarea</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation>Preşedintele Întruniri de la Sfârșitul de săptămână</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>Cântarea</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation>Cântari</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Rugăciune</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation>Ediția Turnului de Veghe</translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>Articolul</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>Conducător</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>Cititor</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>Interval de timp</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>Numărul de săptămâni până la data selectată</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>Numărul de săptămâni după data selectată</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>săptămâni</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation>Numărul de săptămâni selectate după repartizare</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>Datele cloud diferite de datele locale (%1 diferențe). Doriți să lăsați datele locale?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>Datele din cloud au fost șterse. Datele dvs. locale vor fi înlocuite. Continui?</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Nu aloca lecția următoare</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>Ne selectat</translation>
    </message>
</context></TS>