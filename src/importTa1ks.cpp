/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "importTa1ks.h"

importTa1ks::importTa1ks(QString directory)
{
    m_directory.setPath(directory);
    sql = &Singleton<sql_class>::Instance();
}

bool importTa1ks::TestBit(char A[], int k)
{
    return ((A[k / 8] & (1 << (k % 8))) != 0);
}

void importTa1ks::Go()
{
    sql->startTransaction();

    QString lastImport_start = QVariant(QDateTime::currentDateTime().toTime_t()).toString();

    QString filename = m_directory.absolutePath() + m_directory.separator() + "circuit.dat";
    importCircuits(filename);
    filename = m_directory.absolutePath() + m_directory.separator() + "cong.dat";
    bool imported = importCongregations(filename);
    filename = m_directory.absolutePath() + m_directory.separator() + "talk.dat";
    imported |= importTalks(filename);
    filename = m_directory.absolutePath() + m_directory.separator() + "speakers.dat";
    imported |= importSpeakers(filename);
    filename = m_directory.absolutePath() + m_directory.separator() + "xtend.dat";
    imported |= importSpeakersXtend(filename);
    filename = m_directory.absolutePath() + m_directory.separator() + "history.dat";
    imported |= importHistory(filename);

    if (imported) {
        sql->saveSetting("last_ta1ksimport_start", lastImport_start);
        sql->saveSetting("last_ta1ksimport_end", QVariant(QDateTime::currentDateTime().toTime_t()).toString());
    }

    QMessageBox::information(nullptr, QObject::tr("Import") + " Ta1ks", QObject::tr("Import Complete"));
    //sql->commitTransaction();
}

bool importTa1ks::importCircuits(QString filename)
{
    bool imported = false;

    // read circuit.dat-file
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "Could not open " << filename;
        return false;
    }

    CircuitData circuitData;
    while (file.read(reinterpret_cast<char *>(&circuitData), sizeof(CircuitData))) {
        circuitList.insert(circuitData.circuitID, circuitData.name);
        imported = true;
    }

    file.close();

    return imported;
}

bool importTa1ks::importCongregations(QString filename)
{
    bool imported = false;

    // read cong.dat-file
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "Could not open " << filename;
        return false;
    }

    ccongregation cc;
    CongData congData;

    while (file.read(reinterpret_cast<char *>(&congData), sizeof(CongData))) {
        if (congData.name[0] != 0) {
            ccongregation::congregation c = cc.getOrAddCongregation(win1252toUnicode(congData.name));
            int oldID = congData.congregationID;

            QStringList address = (QStringList() << win1252toUnicode(congData.address1) << win1252toUnicode(congData.address2));
            address.removeAll("");
            c.address = address.join(", ");

            if (circuitList.contains(congData.circuitID)) {
                c.circuit = circuitList.value(congData.circuitID);
            }

            auto &mtgtime_now = c.getPublicmeeting_now();
            auto &mtgtime_next = c.getPublicmeeting_next();

            QDate date = QDate::currentDate();
            if (date.year() % 2) {
                /* current year is odd */
                mtgtime_now.setMeetingday(congData.oddWeekDay ? congData.oddWeekDay : 7);
                mtgtime_now.setMeetingtime(time2QTime(congData.oddTime).toString("hh:mm"));
                mtgtime_next.setMeetingday(congData.evenWeekDay ? congData.evenWeekDay : 7);
                mtgtime_next.setMeetingtime(time2QTime(congData.evenTime).toString("hh:mm"));
            } else {
                /* current year is even */
                mtgtime_now.setMeetingday(congData.evenWeekDay ? congData.evenWeekDay : 7);
                mtgtime_now.setMeetingtime(time2QTime(congData.evenTime).toString("hh:mm"));
                mtgtime_next.setMeetingday(congData.oddWeekDay ? congData.oddWeekDay : 7);
                mtgtime_next.setMeetingtime(time2QTime(congData.oddTime).toString("hh:mm"));
            }

            sql_item s;

            if (mtgtime_now.getIsdirty() && mtgtime_now.getIsvalid()) {
                s.insert("mtg_day", mtgtime_now.getMeetingday());
                s.insert("mtg_time", mtgtime_now.getMeetingtime());
                sql->updateSql("congregationmeetingtimes", "id", QVariant(mtgtime_now.getId()).toString(), &s);
            }
            if (mtgtime_next.getIsdirty() && mtgtime_next.getIsvalid()) {
                s.clear();
                s.insert("mtg_day", mtgtime_next.getMeetingday());
                s.insert("mtg_time", mtgtime_next.getMeetingtime());
                sql->updateSql("congregationmeetingtimes", "id", QVariant(mtgtime_next.getId()).toString(), &s);
            }

            c.info.fromLatin1(congData.tel1);
            congOldIdToNewID.insert(oldID, c.id);
            c.save();

            imported = true;

            if (congData.coordinatorOfBOE != 0) {
                coordinatorList.append(congData.coordinatorOfBOE);
            }
            if (congData.secretary != 0) {
                secretaryList.append(congData.secretary);
            }
            if (congData.talkCoordinator != 0) {
                talkCoordinatorList.append(congData.talkCoordinator);
            }
        }
    }

    file.close();

    return imported;
}

bool importTa1ks::importSpeakers(QString filename)
{
    bool imported = false;

    // read speakers.dat-file
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "Could not open " << filename;
        return false;
    }

    int defaultlang = sql->getLanguageDefaultId();
    sql_item firstandlast;
    SpeakerData speakerData;

    int ta1ksUndefinedCongID = -1;

    while (file.read(reinterpret_cast<char *>(&speakerData), sizeof(SpeakerData))) {
        if (speakerData.firstName[0] != 0 && speakerData.lastName[0] != 0) {
            QString firstName = win1252toUnicode(speakerData.firstName);
            QString lastName = win1252toUnicode(speakerData.lastName);

            cpersons persons;
            person *p = NULL;

            QString speakername = firstName + " " + lastName;
            QString formatted = "firstname || ' ' || lastname";
            QString sqlcommand = QString("SELECT id FROM persons WHERE %1 = '%2'").arg(formatted, speakername);
            sql_items ps = sql->selectSql(sqlcommand);
            if (!ps.empty()) {
                p = persons.getPerson(ps[0].value("id").toInt());
            }

            if (!p) {
                // create new
                p = new person();
                p->setLastname(lastName);
                p->setFirstname(firstName);
                p->setGender(person::Male);
                persons.addPerson(p);
            }

            int speakerNewID = p->id();
            int speakerOldID = speakerData.speakerID;
            speakerOldIdToNewID.insert(speakerOldID, speakerNewID);

            if (congOldIdToNewID.contains(speakerData.congregationID)) {
                p->setCongregationid(congOldIdToNewID.value(speakerData.congregationID));
            } else {
                // if speaker is not assigned in Ta1ks to any congregation
                // add new congregation "Ta1ks-undefined" and add speaker to it
                if (ta1ksUndefinedCongID == -1) {
                    ccongregation cc;
                    ccongregation::congregation c = cc.getOrAddCongregation("Ta1ks-undefined");
                    ta1ksUndefinedCongID = c.id;
                }

                p->setCongregationid(ta1ksUndefinedCongID);
            }

            p->setPhone(speakerData.phone1);

            QStringList info = (QStringList()
                                << win1252toUnicode(speakerData.address1)
                                << win1252toUnicode(speakerData.address2));
            info.removeAll("");
            p->setInfo(info.join('\n'));

            p->setServant(speakerData.servant & 3); // 1 = ministerial servant; 2 = elder

            if (speakerData.servant & 32) {
                // suspended
                //qDebug() << "suspended speaker (Ta1ks-import)";
            }

            if (coordinatorList.contains(speakerData.speakerID)) {
                // Coordinator of the body of elders
                //qDebug() << "Coordinator of the body of elders (Ta1ks-import)";
            }

            if (secretaryList.contains(speakerData.speakerID)) {
                // Secretary
                //qDebug() << "Secretary (Ta1ks-import)";
            }

            if (talkCoordinatorList.contains(speakerData.speakerID)) {
                // Talk-Coordinator
                //qDebug() << "Talk-Coordinator (Ta1ks-import)";
            }

            // talks
            bool hasTalks = false;
            for (int talkOldID = 1; talkOldID < 257; talkOldID++) {
                if (TestBit(speakerData.talks, talkOldID - 1)) {
                    hasTalks = true;

                    // talk is selected
                    int talkNewID = talkOldIdToNewID.value(talkOldID, 0);
                    if (talkNewID > 0) {
                        sql_item speakertalk;
                        speakertalk.insert(":speaker_id", speakerNewID);
                        speakertalk.insert(":lang_id", defaultlang);
                        speakertalk.insert(":theme_id", talkNewID);
                        QVariant sp_talkID = sql->selectScalar("SELECT id FROM speaker_publictalks WHERE speaker_id = :speaker_id AND lang_id = :lang_id AND theme_id = :theme_id", &speakertalk);
                        if (sp_talkID.isNull()) {
                            sql_item s;
                            s.insert("speaker_id", speakerNewID);
                            s.insert("theme_id", talkNewID);
                            s.insert("lang_id", defaultlang);
                            sql->insertSql("speaker_publictalks", &s, "id");
                        }
                    } else {
                        qDebug() << "Could not find talk";
                    }
                }
            }

            int usefor = p->usefor();
            if (p->servant() && hasTalks) {
                usefor |= person::PublicTalk;
            }
            if (speakerData.privileges == 0) {
                // no privileges?
            }
            if (speakerData.privileges & 1) {
                qDebug() << "unknown usefor-bit (Ta1ks-import)";
            }
            if (speakerData.privileges & 2) {
                usefor |= person::PublicTalk; // outgoing speaker
            }
            if (speakerData.privileges & 4) {
                usefor |= person::Chairman;
            }
            if (speakerData.privileges & 8) {
                qDebug() << "unknown usefor-bit (Ta1ks-import)";
            }
            if ((speakerData.privileges & 16) && !(usefor & person::WtReader)) {
                usefor |= person::WtReader;
            }
            p->setUsefor(usefor);

            p->update();

            imported = true;
        }
    }

    file.close();

    return imported;
}

bool importTa1ks::importSpeakersXtend(QString filename)
{
    bool imported = false;

    // read xtend.dat-file
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "Could not open " << filename;
        return false;
    }

    SpeakerXtendData speakerXtendData;
    while (file.read(reinterpret_cast<char *>(&speakerXtendData), sizeof(SpeakerXtendData))) {
        int speakerNewID = speakerOldIdToNewID.value(speakerXtendData.speakerID, 0);
        if (speakerNewID > 0) {
            cpersons cp;
            person *p = cp.getPerson(speakerNewID);
            p->setEmail(speakerXtendData.email);

            // phone - add 2nd and 3rd phone number
            QStringList phoneNumbers = (QStringList() << p->phone()
                                                      << speakerXtendData.phone2
                                                      << speakerXtendData.phone3);
            phoneNumbers.removeAll("");
            p->setPhone(phoneNumbers.join("; "));

            // info - add remark field
            QStringList info = (QStringList() << p->info()
                                              << speakerXtendData.remark);
            info.removeAll("");
            p->setInfo(info.join('\n'));

            p->update();

            imported = true;
        }
    }

    file.close();

    return imported;
}

bool importTa1ks::importTalks(QString filename)
{
    bool imported = false;

    QFile file(filename);

    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "Could not open " << filename;
        return false;
    }

    int defaultlang = sql->getLanguageDefaultId();
    TalkData talkData;
    sql_item themeItem;

    while (file.read(reinterpret_cast<char *>(&talkData), sizeof(TalkData))) {
        if (talkData.number[0] != 0 && talkData.theme[0] != 0) {
            int oldID = talkData.talkID;

            int themeNumber = QString::fromLatin1(talkData.number).toInt();

            if (themeNumber) {
                QString themeName = win1252toUnicode(talkData.theme);
                QVariant ptID;

                // try theme name first
                themeItem.insert(":theme_name", themeName);
                themeItem.insert(":lang_id", defaultlang);
                ptID = sql->selectScalar("SELECT id FROM publictalks WHERE theme_name = :theme_name AND lang_id = :lang_id "
                                         "AND active",
                                         &themeItem);
                if (ptID.isNull()) {
                    themeItem.clear();
                    themeItem.insert(":theme_number", themeNumber);
                    themeItem.insert(":lang_id", defaultlang);

                    ptID = sql->selectScalar("SELECT id FROM publictalks WHERE theme_number = :theme_number AND lang_id = :lang_id "
                                             "AND active AND (discontinue_date IS NULL OR discontinue_date='')",
                                             &themeItem);
                }

                if (ptID.isNull()) {
                    // create new
                    sql_item sp;
                    sp.insert("theme_number", themeNumber);
                    sp.insert("theme_name", themeName);
                    sp.insert("lang_id", defaultlang);
                    int newID = sql->insertSql("publictalks", &sp, "id");

                    imported = true;

                    talkOldIdToNewID.insert(oldID, newID);
                } else {
                    talkOldIdToNewID.insert(oldID, ptID.toInt());
                }
            } else {
                qDebug() << "Could not import talk: " << talkData.number;
            }
        }
    }

    file.close();

    return imported;
}

bool importTa1ks::importOutSpeaker(QDate date, uint16_t spkrID, uint16_t talkID, uint16_t congID)
{
    bool imported = false;

    int speakerNewID = speakerOldIdToNewID.value(spkrID, 0);
    if (speakerNewID > 0) {
        int talkNewID = talkOldIdToNewID.value(talkID, 0);
        QVariant themeNumber = sql->selectScalar("select theme_number from publictalks where id = :talkNewID", talkNewID, 0);

        // the talk may be a discontinued talk but the theme was different initially and was replaced later
        // therefore we try to get the correct one by the date it was held
        int defaultlang = sql->getLanguageDefaultId();
        sql_item themeItem;
        themeItem.insert(":theme_number", themeNumber);
        themeItem.insert(":lang_id", defaultlang);
        QVariant themeId = sql->selectScalar("SELECT id FROM publictalks WHERE theme_number = :theme_number"
                                             " AND lang_id = :lang_id AND active"
                                             " AND (discontinue_date IS NULL OR discontinue_date = '' OR discontinue_date > '" + date.toString(Qt::ISODate) + "')"
                                             " AND (release_date IS NULL OR release_date = '' OR release_date <= '" + date.toString(Qt::ISODate) + "')", &themeItem);
        if (!themeId.isNull()) {
            talkNewID = themeId.toInt();
            int congNewID = congOldIdToNewID.value(congID);

            // update of existing rows
            //  - search for date & congregation
            //    update speaker & theme
            //  - rows that remain with old ta1ks-import time-stamp
            //    will be removed later
            sql_item outgoing;
            outgoing.insert(":date", date.toString(Qt::ISODate));
            outgoing.insert(":congregation_id", congNewID);
            QVariant outgoingID = sql->selectScalar("SELECT id FROM outgoing WHERE date = :date AND congregation_id = :congregation_id", &outgoing);

            if (!outgoingID.isNull()) {
                sql_item s;
                s.insert("id", outgoingID);
                s.insert("date", date);
                s.insert("speaker_id", speakerNewID);
                s.insert("congregation_id", congNewID);
                s.insert("theme_id", talkNewID);

                sql->updateSql("outgoing", "id", outgoingID.toString(), &s);

                imported = true;
            } else {
                cpublictalks cp;
                cptmeeting *currentMeeting = cp.getMeeting(date);
                cpoutgoing *out = cp.addOutgoingSpeaker(date, speakerNewID, talkNewID, congNewID);

                if (out) {
                    currentMeeting->save();
                    imported = true;
                }
            }
        }
    }

    return imported;
}

bool importTa1ks::importHistory(QString filename)
{
    bool imported = false;

    // read xtend.dat-file
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "Could not open " << filename;
        return false;
    }

    QDate refDate(1999, 12, 27);
    cpublictalks cp;

    HistoryData historyData;
    while (file.read(reinterpret_cast<char *>(&historyData), sizeof(HistoryData))) {
        QDate date = refDate.addDays(7 * historyData.date);

        cptmeeting *temp = cp.getMeeting(date);

        // in - speaker
        int speakerNewID = speakerOldIdToNewID.value(historyData.inSpeakerID, 0);
        if (speakerNewID > 0) {
            int inTalkNewID = talkOldIdToNewID.value(historyData.inTalkID, 0);
            QVariant themeNumber = sql->selectScalar("select theme_number from publictalks where id = :talkNewID", inTalkNewID, 0);
            if (!themeNumber.isNull()) {
                temp->theme = cp.getThemeByNumber(themeNumber.toInt(), date);
                temp->setSpeaker(cpersons::getPerson(speakerNewID));
                imported = true;
            }
        }

        // chairman
        int chairmanID = speakerOldIdToNewID.value(historyData.chairmanID, 0);
        if (chairmanID > 0) {
            temp->setChairman(cpersons::getPerson(chairmanID));
            imported = true;
        }

        // wt-reader
        int wtReaderID = speakerOldIdToNewID.value(historyData.wtReaderID, 0);
        if (wtReaderID > 0) {
            temp->setWtReader(cpersons::getPerson(wtReaderID));
            imported = true;
        }

        if (imported) {
            temp->save();
        }

        // out - speaker 1-6
        imported |= importOutSpeaker(date, historyData.outSpkr1ID, historyData.outTalk1ID, historyData.outCong1ID);
        imported |= importOutSpeaker(date, historyData.outSpkr2ID, historyData.outTalk2ID, historyData.outCong2ID);
        imported |= importOutSpeaker(date, historyData.outSpkr3ID, historyData.outTalk3ID, historyData.outCong3ID);
        imported |= importOutSpeaker(date, historyData.outSpkr4ID, historyData.outTalk4ID, historyData.outCong4ID);
        imported |= importOutSpeaker(date, historyData.outSpkr5ID, historyData.outTalk5ID, historyData.outCong5ID);
        imported |= importOutSpeaker(date, historyData.outSpkr6ID, historyData.outTalk6ID, historyData.outCong6ID);
    }

    file.close();

    // remove old data from last import
    if (imported) {
        QString lastImport_start = sql->getSetting("last_ta1ksimport_start", "-1");
        if (lastImport_start != "-1") {
            QString lastImport_end = sql->getSetting("last_ta1ksimport_end", "-1");

            sql->removeSql("publicmeeting", "time_stamp >= " + lastImport_start + " AND time_stamp <= " + lastImport_end);
            sql->removeSql("outgoing", "time_stamp >= " + lastImport_start + " AND time_stamp <= " + lastImport_end);
        }
    }

    return imported;
}

QTime importTa1ks::time2QTime(char *time)
{
    QString timestr = QString::fromLatin1(time).replace(" ", "").toLower();
    QString timefmt = timestr.indexOf(":") == 1 ? "h:m" : "hh:m"; //leading zero?
    timefmt += timestr.contains("m") ? "ap" : ""; //am,pm
    return QTime::fromString(timestr, timefmt);
}

QString importTa1ks::win1252toUnicode(std::string str)
{
    QTextCodec *codec = QTextCodec::codecForName("Windows-1250");
    QByteArray enc(str.c_str(), static_cast<int>(str.length()));
    return QString(codec->toUnicode(enc));
}
