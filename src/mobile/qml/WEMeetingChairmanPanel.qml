/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2019, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import net.theocbase.mobile 1.0

Page {
    id: chairmanEdit
    title: qsTr("Weekend Meeting Chairman", "Page title")
    property CPTMeeting meeting
    onMeetingChanged: {
        if (meeting)
            controller.date = meeting.date
    }
    header: BaseToolbar {
        title: chairmanEdit.title
        componentLeft: ToolButton {
            icon.source: "qrc:///back.svg"
            icon.color: "white"
            onClicked: {
                stackView.pop()
            }
        }
    }

    ScrollView {
        width: parent.width
        height: parent.height
        contentWidth: width

        ColumnLayout {
            id: layout            
            width: chairmanEdit.availableWidth

            // Chairman
            RowTitle {
                id: chairmanTitle
                text: qsTr("Chairman")
            }
            ComboBoxTable {
                Layout.fillWidth: true
                Layout.bottomMargin: 10
                currentText: meeting.chairman ? meeting.chairman.fullname : ""
                currentId: meeting.chairman ? meeting.chairman.id : -1
                col1Name: chairmanTitle.text
                onBeforeMenuShown: {
                    model = controller.brotherList(Publisher.Chairman)
                }
                onRowSelected: {
                    var chairman = CPersons.getPerson(id)
                    meeting.chairman = chairman
                    meeting.save()
                }
                PublicMeetingController { id: controller }
            }

            // Song
            RowTitle {
                text: qsTr("Song")
            }
            NumberSelector {
                Layout.fillWidth: true
                Layout.preferredHeight: height
                Layout.bottomMargin: 10*dpi
                maxValue: 151
                selectedValue: meeting.songTalk
                onSelectedValueChanged: {
                    if (selectedValue !== meeting.songTalk) {
                        meeting.songTalk = selectedValue
                        meeting.save()
                    }
                }
            }
        }
    }
}
